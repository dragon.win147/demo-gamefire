import { Code } from "@gameloot/sicbov2/sicbov2_code_pb";
import HandlerClientBase from "@gameloot/client-base-handler/build/HandlerClientBase";
import EventKey from "@gameloot/client-base-handler/build/EventKey";
import { Empty } from "google-protobuf/google/protobuf/empty_pb";
import { Playah } from "@gameloot/playah/playah_pb";

import { BenTauMessage, BenTauReply } from "@gameloot/bentau/bentau_pb";
import { Topic } from "@gameloot/topic/topic_pb";

import { Message as WhatAppMessage } from "@gameloot/whatsapp/whatsapp_pb";

import { MyHistoryClient as  HistoryClient} from "@gameloot/myhistory/MyHistoryServiceClientPb";
import { GetChannelHistoryReply, GetPersonalHistoryReply, GetChannelHistoryRequest, GetPersonalHistoryRequest } from "@gameloot/myhistory/myhistory_pb";

import { LeaderboardClient } from "@gameloot/leaderboard/LeaderboardServiceClientPb";
import { RankEarningReply, RankEarningRequest, DetailedLeader, Cutoff } from "@gameloot/leaderboard/leaderboard_pb";

import { FileField, ResourceField } from "@gameloot/conveyor/conveyor_pb";

import { SicboClient } from "@gameloot/sicbov2/Sicbov2ServiceClientPb";
import {
  BetDoubleReply,
  BetDoubleRequest,
  BetReply,
  BetRequest,
  Channel,
  Door,
  GetOthersReply,
  GetOthersRequest,
  LeaveRequest,
  Message,
  ReBetReply,
  ReBetRequest,
  State,
  Result,
  Event,
  DoorAndBetAmount,
  Status,
  ChannelHistoryMetadata,
  PersonalHistoryMetadata,
  Item,
  ChipLevels,
  StageTime,
  LeaveReply,
  JackpotHistoryMetadata,
  HealthCheckRequest, HealthCheckReply
} from "@gameloot/sicbov2/sicbov2_pb";

import { HandlerClientBenTauBase, CodeSocket } from "@gameloot/client-base-handler/build";
import { Timestamp } from "google-protobuf/google/protobuf/timestamp_pb";
import { AutoJoinReply } from '@gameloot/sicbov2/sicbov2_pb';

import { PotterClient } from "@gameloot/potter/PotterServiceClientPb";
import { ListCurrentJackpotsReply, ListCurrentJackpotsRequest, GetHistoryReply, GetHistoryRequest } from "@gameloot/potter/potter_pb";

export default class SicboModuleAdapter {
  static getAllRefs() {
    return {
      Code, HandlerClientBase, Empty, Playah,
      BenTauMessage, BenTauReply , Topic,
      HistoryClient, GetChannelHistoryReply, GetPersonalHistoryReply, GetChannelHistoryRequest, GetPersonalHistoryRequest,
      LeaderboardClient, RankEarningRequest, RankEarningReply, DetailedLeader, Cutoff,
      FileField, ResourceField, 
      SicboClient, 
      BetDoubleReply, BetDoubleRequest, BetReply, BetRequest, Channel, Door,
      GetOthersReply, GetOthersRequest, LeaveRequest, Message, ReBetReply, AutoJoinReply,
      ReBetRequest, State, Result, Event,  DoorAndBetAmount, Status, ChannelHistoryMetadata,
      PersonalHistoryMetadata, Item, ChipLevels, StageTime, LeaveReply,
      HandlerClientBenTauBase, CodeSocket, Timestamp, EventKey, ListCurrentJackpotsReply, GetHistoryReply, ListCurrentJackpotsRequest, GetHistoryRequest, PotterClient, JackpotHistoryMetadata,
      WhatAppMessage,
      HealthCheckRequest, HealthCheckReply
    };

    // let moduleAdapter = null;
    // const adapterName = "SicboAdapterImport"
    // try {
    //   moduleAdapter = require(adapterName)[adapterName];
    // } catch {
    //   //cc.log("new token get Chạy lúc gắn vào portal ");
    // }

    // if (moduleAdapter != null) {
    //   //duoc chay luc build
    //   return new moduleAdapter().getAllRefs();
    // }
    // let temp = cc.director.getScene().getChildByName(adapterName)?.getComponent(adapterName)?.getAllRefs();
    // return temp;
  }

  // getProtobuffRef(){
  //     return {Timestamp}
  // }

  // getConveyorRefs() {
  //     return { FileField, ResourceField};
  // }

  // getBenTauRefs() {
  //     return {BenTauMessage, BenTauReply , Topic, Empty};
  // }

  // getgamelootRefs() {
  //     return {HandlerClientBenTauBase, CodeSocket };
  // }

  // getHistoryRef(){
  //     return {HandlerClientBase, HistoryClient, GetChannelHistoryReply, GetPersonalHistoryReply, GetChannelHistoryRequest, GetPersonalHistoryRequest};
  // }
}
