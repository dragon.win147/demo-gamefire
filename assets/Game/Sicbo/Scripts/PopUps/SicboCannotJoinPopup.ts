// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboPortalAdapter from "../SicboPortalAdapter";
import SicboPopUpInstance from "../Managers/SicboPopUpInstance";
import { SicboScene } from "../Setting/SicboSetting";
import SicboText from '../Setting/SicboText';

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboCannotJoinPopup extends SicboPopUpInstance {
    @property(cc.Label)
    notIngameMsgLabel: cc.Label = null;

    @property(cc.Label)
    ingameMsgLabel: cc.Label = null;

    @property(cc.Node)
    notIngameNode: cc.Node = null;

    @property(cc.Node)
    ingameNode: cc.Node = null;

    onLoad() {
        super.onLoad();
        this.notIngameMsgLabel.string = SicboText.getCannotJoinMessageWhenNotIngame;
        this.ingameMsgLabel.string = SicboText.getCannotJoinMessageWhenIngame;
    }
    
    onShow(){
        let inGame: boolean = (cc.director.getScene().name != SicboScene.Loading);
        this.ingameNode.active = inGame;
        this.notIngameNode.active = !inGame;
    }

    leave(){
        this.close();
        SicboPortalAdapter.getInstance().leaveGame();
    }
}
