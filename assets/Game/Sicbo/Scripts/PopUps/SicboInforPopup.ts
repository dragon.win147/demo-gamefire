// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboController from "../Controllers/SicboController";
import SicboPopUpInstance from "../Managers/SicboPopUpInstance";
import SicboResourceManager from "../Managers/SicboResourceManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboInforPopup extends SicboPopUpInstance {
  @property(cc.ScrollView)
  scrollview: cc.ScrollView = null;

  @property(cc.Sprite)
  infoSprite: cc.Sprite = null;

  onLoad() {
    super.onLoad();
  }

  onShow() {
    // this.scrollview.scrollToTop(0);
    // this.infoSprite.spriteFrame = null;
    // let infoResources = SicboController.Instance.getGameInfo()
    // if(infoResources == null) return
    // infoResources.forEach((info) => {
    //   SicboResourceManager.loadRemoteImageWithFileField(this.infoSprite, info);
    // });
  }

  onButtonClose() {
    this.close();
  }
}
