import SicboModuleAdapter from "../../../../SicboModuleAdapter";

const { Code, Status, ChipLevels, StageTime } = SicboModuleAdapter.getAllRefs();

export enum SicboScene {
  Loading = "SicboLoadingScene",
  Main = "SicboScene",
}

export default class SicboSetting {
  public static USER_ANIM_DURATION = 0.77;
  static resourceLocalPaths: string[] = [];
  static resourceRemotePaths: string[] = [];
  static loadingBackgroundLocalPath: string = "";
  static loadingBackgroundOverrideProgressBar: boolean = false;

  static loadDataFromForge(
    onProgress: (current, total) => void = null,
    onComplete: () => void = null
  ) {
    if (onProgress) onProgress(0, 0);
    if (onComplete) onComplete();
  }

  private static _chipLevels: InstanceType<typeof ChipLevels> = null;
  public static get chipLevels(): InstanceType<typeof ChipLevels> {
    return SicboSetting._chipLevels;
  }

  private static _statusAndDuration: InstanceType<typeof StageTime> = null;
  public static get statusAndDuration(): InstanceType<typeof StageTime> {
    return SicboSetting._statusAndDuration;
  }

  public static getStatusDuration(status): number {
    switch (status) {
      case Status.WAITING:
        return SicboSetting._statusAndDuration.getWaiting();

      case Status.BETTING:
        return SicboSetting._statusAndDuration.getBetting();

      case Status.END_BET:
        return SicboSetting._statusAndDuration.getEndBet();

      case Status.RESULTING:
        return SicboSetting._statusAndDuration.getResulting();

      case Status.PAYING:
        return SicboSetting._statusAndDuration.getPaying();

      case Status.FINISHING:
        return SicboSetting._statusAndDuration.getFinishing();
      default:
        return 0;
    }
  }

  public static getChipLevelList() {
    return SicboSetting._chipLevels.getChipLevelList();
  }
  public static setChipAndStatusDurationConfig(
    chipLevels: InstanceType<typeof ChipLevels>,
    statusAndDuration: InstanceType<typeof StageTime>
  ) {
    SicboSetting._chipLevels = chipLevels;
    SicboSetting._statusAndDuration = statusAndDuration;
  }

  public static setStatusDurationConfig(
    statusAndDuration: InstanceType<typeof StageTime>
  ) {
    SicboSetting._statusAndDuration = statusAndDuration;
  }

  public static getSoundName(sound: SicboSound) {
    return SicboSound[sound];
    // switch (sound) {
    //   case SicboSound.BGM: return "bgm-main_game1";
    //   case SicboSound.SfxDealerShake: return "sfx-dice_shake_long";
    //   case SicboSound.SfxClick: return "SFX_ButtonTap";
    //   case SicboSound.SfxBet: return "sfx-bet";
    //   case SicboSound.SfxRush: return "sfx-rush";
    //   case SicboSound.SfxTictoc: return "sfx_tictoc";
    //   case SicboSound.SfxStopBetting: return "sfx-stop_betting1";
    //   case SicboSound.SfxResult: return "sfx-result";
    //   case SicboSound.SfxJackpotScale: return "sfx_jackpot_scale";
    //   case SicboSound.SfxJackpotFinal: return "sfx_jackpot_final";
    //   case SicboSound.SfxJackpotCoin: return "sfx-jackpot_coin1";
    //   case SicboSound.SfxWin: return "sfx-win";
    //   case SicboSound.SfxTotalWin: return "sfx-total_win2";
    //   case SicboSound.SfxMessage: return "sfx-message2";
    //   case SicboSound.SfxChipFly: return "sfx-bet";
    // }
  }

  public static getSoundDuration(sound: SicboSound) {
    switch (sound) {
      // case SicboSound.BGM: return "bgm-main_game1";
      // case SicboSound.SfxDealerShake: return "sfx-dice_shake_long";
      // case SicboSound.SfxClick: return "sfx-click2";
      // case SicboSound.SfxBet: return "sfx-bet";
      // case SicboSound.SfxRush: return "sfx-rush";
      // case SicboSound.SfxTictoc: return "sfx-tictoc2";
      // case SicboSound.SfxStopBetting: return "sfx-stop_betting1";
      // case SicboSound.SfxResult: return "sfx-result";
      // case SicboSound.SfxJackpotScale: return "sfx-jackpot_scale1";
      // case SicboSound.SfxJackpotFinal: return "sfx-jackpot_final1";
      // case SicboSound.SfxJackpotCoin: return "sfx-jackpot_coin1";
      // case SicboSound.SfxWin: return "sfx-win";
      // case SicboSound.SfxTotalWin: return "sfx-total_win2";
      // case SicboSound.SfxMessage: return "sfx-message";
      case SicboSound.SfxChipFly:
        return 0.134;

      default:
        //cc.log("BCF WARNING: NOT HAVE DURATION SETTING")
        return 0;
    }
  }
}

export enum SicboSound {
  BGM = 0,
  SfxDealerShake = 1,
  SfxClick = 2,
  SfxBet = 3,
  SfxRush = 4,
  SfxTictoc = 5,
  SfxStopBetting = 6,
  SfxResult = 7,
  SfxJackpotScale = 8,
  SfxJackpotFinal = 9,
  SfxJackpotCoin = 10,
  SfxWin = 11,
  SfxTotalWin = 12,
  SfxMessage = 13,
  SfxChipFly = 14,
}
