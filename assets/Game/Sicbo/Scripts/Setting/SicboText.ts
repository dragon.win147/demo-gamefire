// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboModuleAdapter from "../../../../SicboModuleAdapter";

const { Code } = SicboModuleAdapter.getAllRefs();

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboText {
  static get handlerDefaultErrMsg() {
    return "Có lỗi xảy ra xin vui lòng thử lại!";
  }

  static get sessionCopyMessage(): string {
    return "Sao Chép Thành Công Mã MD5";
  }

  static get loadingMsg(): string {
    return "Loading... {0}%";
  }

  static get lotsOfUserText(): string {
    return "999+";
  }

  static get sessionIdText() {
    return "#{0}";
  }

  static get leaderboardUpdateTime() {
    return "{0} phút";
  }

  static get noLotteryValue() {
    return "-";
  }

  static get sessionDataBetHistoryDetail() {
    return "#{0} ({1})";
  }

  static get sessionLastFormat() {
    return "Phiên gần đây nhất (#{0}) -";
  }

  static get valueLastFormat() {
    return "{0} {1}: ({2}-{3}-{4})";
  }

  static get getCannotJoinMessageWhenNotIngame(){
    return "Game Đang Tiến Hành Nâng Cấp Hệ Thống.\nQuý khách vui lòng quay lại sau!"
  }

  static get getCannotJoinMessageWhenIngame(){
    return "Xin lỗi quý khách, hiện tại hệ thống đang gặp sự cố.\nChúng tôi sẽ cố gắng khắc phục trong thời gian sớm nhất.\nChân thành xin lỗi vì sự bất tiện này."
  }


  public static getErrorMessage(errorCode) {
    switch (errorCode) {
      case Code.SICBO_BET_LIMIT_REACHED:
        // return "Quý Khách Đã Đặt Mức Cược Tối Đa Cho Tụ Này";
        return "Số Tiền Đặt Cược Không Thể Vượt Mức Quy Định Của Tụ!";

      case Code.SICBO_BET_NOT_ENOUGH_MONEY:
        return "Số Dư Của Quý Khách Không Đủ Để Đặt Cược";

      default:
        return null;
    }
  }

  public static get waitingRemainingTimeMsg() {
    return "Ván mới bắt đầu sau {0}s";
  }

  public static get kickMsg() {
    return "Xin Mời Quý Khách Đặt Cược\nĐể Tiếp Tục Tham Gia Bàn Chơi.";
  }

  public static get leaveBookingActiveMsg() {
    return "Quý Khách sẽ rời phòng sau khi ván chơi kết thúc.";
  }

  public static get leaveBookingDeactiveMsg() {
    return "Quý Khách sẽ tiếp tục chơi.";
  }
}
