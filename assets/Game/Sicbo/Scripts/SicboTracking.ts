// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

//SECTION - FIREBASE const
const FIREBASE_TRACKING_EVENT = "FIREBASE_TRACKING_EVENT";
const FIREBASE_BET_EVENT = "gp_sicbo_";//gp_sicbo_{action_name}
const FirebaseParamVersion = "Version";
const FirebaseParamSessionId = "SessionId";
//!SECTION


//SECTION - APPFLYER Const
const APPS_FLYER_TRACKING_EVENT = "APPS_FLYER_TRACKING_EVENT"
const AF_EVENT_BET = "af_placed_bet_sicbo"
const AF_EVENT_REBET = "af_placed_bet_sicbo"
const AF_EVENT_DOUBLEBET = "af_placed_bet_sicbo"

const AF_CURRENCY = "VND"

const AF_ParamPrice = "af_price"
const AF_ParamCurrency = "af_currency"
const AF_ParamSession = "event_number"
const AF_ParamDate = "event_date"
const AF_ParamTime = "event_time"
const AF_ParamTransactionId = "transaction_id"
//!SECTION


@ccclass
export default class SicboTracking {
  private static version: string;
  private static sesssionId: string;

  static setGameVersion(ver: string) {
    SicboTracking.version = ver;
  }

  static startSession(sesssionId: string) {
    SicboTracking.sesssionId = sesssionId;
  }

  
  static trackingBet(transactionId, amount) {
    SicboTracking.firebaseTrackingBet()
    SicboTracking.appflyerTrackingBet(transactionId, amount)
  }

  static trackingReBet(transactionId, amount) {
    SicboTracking.firebaseTrackingReBet()
    SicboTracking.appflyerTrackingReBet(transactionId, amount)
  }

  static trackingDoubleBet(transactionId, amount) {
    SicboTracking.firebaseTrackingDoubleBet();
    SicboTracking.appflyerTrackingDoubleBet(transactionId, amount)
  }

  //SECTION Appflyer
  static appflyerTrackingBet(transactionId, amount) {
    let eventName = AF_EVENT_BET;
    let eventValue = {};

    eventValue[AF_ParamPrice] = Math.abs(amount);
    eventValue[AF_ParamTransactionId] = transactionId;
    eventValue[AF_ParamCurrency] = AF_CURRENCY;

    SicboTracking.sendTrackingAppflyer(eventName, eventValue);
  }

  static appflyerTrackingReBet(transactionId, amount) {
    let eventName = AF_EVENT_REBET;
    let eventValue = {};

    eventValue[AF_ParamPrice] = Math.abs(amount);
    eventValue[AF_ParamTransactionId] = transactionId;
    eventValue[AF_ParamCurrency] = AF_CURRENCY;

    SicboTracking.sendTrackingAppflyer(eventName, eventValue);
  }

  static appflyerTrackingDoubleBet(transactionId, amount) {
    let eventName = AF_EVENT_DOUBLEBET;
    let eventValue = {};

    eventValue[AF_ParamPrice] = Math.abs(amount);
    eventValue[AF_ParamTransactionId] = transactionId;
    eventValue[AF_ParamCurrency] = AF_CURRENCY;

    SicboTracking.sendTrackingAppflyer(eventName, eventValue);
  }

  private static sendTrackingAppflyer(eventName, eventValue) {
    eventValue[AF_ParamSession] = SicboTracking.sesssionId;
    eventValue[AF_ParamDate] = SicboTracking.getCurrentDate();
    eventValue[AF_ParamTime] = SicboTracking.getCurrentTime();
    const evt = {
          eventType: "EVENT",
          eventName: eventName,
          eventValue: eventValue,
        };
      
    //send event
    cc.systemEvent.emit(APPS_FLYER_TRACKING_EVENT, evt);
  }
//!SECTION

  //SECTION Firebase
  static firebaseTrackingBet() {
    let eventName = FIREBASE_BET_EVENT + "bet";
    let params = {};
    SicboTracking.sendTrackingFirebase(eventName, params);
  }

  static firebaseTrackingReBet() {
    let eventName = FIREBASE_BET_EVENT + "re_bet";
    let params = {};
    SicboTracking.sendTrackingFirebase(eventName, params);
  }

  static firebaseTrackingDoubleBet() {
    let eventName = FIREBASE_BET_EVENT + "double_bet";
    let params = {};
    SicboTracking.sendTrackingFirebase(eventName, params);
  }

  private static sendTrackingFirebase(eventName, params) {
    params[FirebaseParamVersion] = SicboTracking.version;
    params[FirebaseParamSessionId] = SicboTracking.sesssionId;
    cc.systemEvent.emit(FIREBASE_TRACKING_EVENT, eventName, params);
  }
  
//!SECTION


  /**
   * get current date as DDMMYYYY
   */
  public static getCurrentDate(): string {
    const currentDate = new Date();

    // Get day, month, and year
    const day = currentDate.getDate().toString().padStart(2, "0");
    const month = (currentDate.getMonth() + 1).toString().padStart(2, "0"); // Month is zero-based
    const year = currentDate.getFullYear().toString();

    // Concatenate the date parts
    const formattedDate = day + month + year;

    return formattedDate;
  }
  /**
   * get current time as HH:MM
   */
  public static getCurrentTime(): string {
    const currentDate = new Date();

    // Get hours and minutes
    const hours = currentDate.getHours().toString().padStart(2, "0");
    const minutes = currentDate.getMinutes().toString().padStart(2, "0");

    // Concatenate the time parts
    const formattedTime = hours + ":" + minutes;

    return formattedTime;
  }
}
