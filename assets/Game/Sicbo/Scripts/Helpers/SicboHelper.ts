/* eslint-disable */

import SicboModuleAdapter from "../../../../SicboModuleAdapter";

const {
  Result,
  Item} = SicboModuleAdapter.getAllRefs();

export default class SicboHelper {
  //SECTION Old
  static getResultTypeName(sicboResultType: SicboResultType, upperAll: boolean = true): string {
    switch (sicboResultType) {
      case SicboResultType.TAI:
        return upperAll? "TÀI": "Tài";
      case SicboResultType.XIU:
        return upperAll? "XỈU": "Xỉu";
      case SicboResultType.BAO:
        return upperAll? "BÃO": "Bão";
    }
  }

  static getResultType(items: any[]): SicboResultType {
    let resultType = SicboResultType.TAI;
    if (items[0] == items[1] && items[1] == items[2]) {
      resultType = SicboResultType.BAO;
    } else {
      let totalValue = this.getResultTotalValue(items);
      if (totalValue > 3 && totalValue <= 10) {
        resultType = SicboResultType.XIU;
      }
    }
    return resultType;
  }

  static getResultBrightType(items: any[]): SicboResultType {
    let resultType = SicboResultType.TAI;
    let totalValue = this.getResultTotalValue(items);
    if (totalValue >= 3 && totalValue <= 10) {
      resultType = SicboResultType.XIU;
    }
    return resultType;
  }

  static getResultTotalValue(items: any[]): number {
    let totalValue = 0;
    items.forEach((item) => {
      totalValue += item as number;
    });
    return totalValue;
  }

  // static loadSicboDice(item: DICE, onComplete: (spriteFrame) => void) {
  //   if (item == DICE.DICE_UNSPECIFIED) item = DICE.ONE;
  //   let path = GameUtils.FormatString("Images/Game/Sicbo/Items/XucXac_{0}", item.toString());
  //   ResourceManager.loadAsset(path, cc.SpriteFrame, (spriteFrame: cc.SpriteFrame) => {
  //     if (onComplete) onComplete(spriteFrame);
  //   });
  // }
  //!SECTION

  static convertCoinTypeToValue(coinType: SicboCoinType) {
    switch (coinType) {
      case SicboCoinType.Coin100:
        return 100;
      case SicboCoinType.Coin200:
        return 200;
      case SicboCoinType.Coin500:
        return 500;
      case SicboCoinType.Coin1k:
        return 1000;
      case SicboCoinType.Coin2k:
        return 2000;
      case SicboCoinType.Coin5k:
        return 5000;
      case SicboCoinType.Coin10k:
        return 10000;
      case SicboCoinType.Coin20k:
        return 20000;
      case SicboCoinType.Coin50k:
        return 50000;
      case SicboCoinType.Coin100k:
        return 100000;
      case SicboCoinType.Coin200k:
        return 200000;
      case SicboCoinType.Coin500k:
        return 500000;
      case SicboCoinType.Coin1M:
        return 1000000;
      case SicboCoinType.Coin2M:
        return 2000000;
      case SicboCoinType.Coin5M:
        return 5000000;
      case SicboCoinType.Coin10M:
        return 10000000;
      case SicboCoinType.Coin20M:
        return 20000000;
      case SicboCoinType.Coin50M:
        return 50000000;
    }
    return 100;
  }

  static convertValueToCoinType(value: number) {
    switch (value) {
      case 100:
        return SicboCoinType.Coin100;
      case 200:
        return SicboCoinType.Coin200;
      case 500:
        return SicboCoinType.Coin500;
      case 1000:
        return SicboCoinType.Coin1k;
      case 2000:
        return SicboCoinType.Coin2k;
      case 5000:
        return SicboCoinType.Coin5k;
      case 10000:
        return SicboCoinType.Coin10k;
      case 20000:
        return SicboCoinType.Coin20k;
      case 50000:
        return SicboCoinType.Coin50k;
      case 100000:
        return SicboCoinType.Coin100k;
      case 200000:
        return SicboCoinType.Coin200k;
      case 500000:
        return SicboCoinType.Coin500k;
      case 1000000:
        return SicboCoinType.Coin1M;
      case 2000000:
        return SicboCoinType.Coin2M;
      case 5000000:
        return SicboCoinType.Coin5M;
      case 10000000:
        return SicboCoinType.Coin10M;
      case 20000000:
        return SicboCoinType.Coin20M;
      case 50000000:
        return SicboCoinType.Coin50M;
    }
  }


  static convertValueToChips(value: number, coinType: SicboCoinType[]) {
    if (value <= 0) return coinType;
    let chipMax = this.convertValueToChipMax(value);
    coinType[coinType.length] = chipMax;
    let valueTemp = value - this.convertCoinTypeToValue(chipMax);
    return this.convertValueToChips(valueTemp, coinType);
  }

  private static convertValueToChipMax(value: number) {
    if (value >= 50000000) return SicboCoinType.Coin50M;
    if (value >= 20000000) return SicboCoinType.Coin20M;
    if (value >= 10000000) return SicboCoinType.Coin10M;
    if (value >= 5000000) return SicboCoinType.Coin5M;
    if (value >= 2000000) return SicboCoinType.Coin2M;
    if (value >= 1000000) return SicboCoinType.Coin1M;
    if (value >= 500000) return SicboCoinType.Coin500k;
    if (value >= 200000) return SicboCoinType.Coin200k;
    if (value >= 100000) return SicboCoinType.Coin100k;
    if (value >= 50000) return SicboCoinType.Coin50k;
    if (value >= 20000) return SicboCoinType.Coin20k;
    if (value >= 10000) return SicboCoinType.Coin10k;
    if (value >= 5000) return SicboCoinType.Coin5k;
    if (value >= 2000) return SicboCoinType.Coin2k;
    if (value >= 1000) return SicboCoinType.Coin1k;
    if (value >= 500) return SicboCoinType.Coin500;
    if (value >= 200) return SicboCoinType.Coin200;

    return SicboCoinType.Coin100;
  }
  static convertLuckyNumToString(value: number): string {
    if (value < 10) return "00000" + value;
    if (value < 100) return "0000" + value;
    if (value < 1000) return "000" + value;
    if (value < 10000) return "00" + value;
    if (value < 100000) return "0" + value;
    return value.toString();
  }
  // static getAllBetTypeWithMinBet(
  //   value: number,
  //   channelType: CHANNEL_TYPE
  // ): number[] {
  //   let maxValue = this.getMaxBetWithMinBet(value, channelType);
  //   let values = this.getAllBuyIn().filter((x) => x >= value && x <= maxValue);
  //   return values;
  // }

  // static getAllBetTypeWithMinBetSicboMini(
  //   value: number,
  //   channelType: CHANNEL_TYPE
  // ): CoinType[] {
  //   let maxValue = this.getMaxBetWithMinBet(value, channelType);
  //   let values = this.getAllBuyInSicboMini().filter(
  //     (x) => x >= value && x <= maxValue
  //   );

  //   let ItemTypes: CoinType[] = [];
  //   values.forEach((value, index) => {
  //     let coinType = this.convertValueToItemType(value);

  //     ItemTypes[index] = coinType;
  //   });
  //   return ItemTypes;
  // }
  

  static convertSicboItemToString(item) {
    var ItemName = "";
    switch (item) {
      case Item.ONE:
        ItemName = "Một";
        break;
      case Item.TWO:
        ItemName = "Hai";
        break;
      case Item.THREE:
        ItemName = "Ba";
        break;
      case Item.FOUR:
        ItemName = "Bốn";
        break;
      case Item.FIVE:
        ItemName = "Năm";
        break;
      case Item.SIX:
        ItemName = "Sáu";
        break;
    }
    return ItemName;
  }

  static StatusToString(status):string{    
    let StatusString = [ 
      "STATUS_UNSPECIFIED",
      "WAITING",
      "BETTING",
      "WAITING_RESULT",
      "RESULTING",
      "SHOWING_JACKPOT",
      "SHOWING_RESULT",
      "PAYING",
      "FINISHING",
    ]
    return StatusString[status];
  }


  static getResultMessage() {
    let msg = "";
    // if (result) {
    //   let ItemWins = result.getItemsList();
    //   switch (result.getType()) {
    //     case Result.TYPE.JACKPOT:
    //     case Result.TYPE.PUT2POT:
    //     case Result.TYPE.NORMAL:
    //       ItemWins.forEach((Item, index) => {
    //         if (index == ItemWins.length - 1) {
    //           msg = msg + this.convertSicboItemToString(Item);
    //         } else {
    //           msg = msg + this.convertSicboItemToString(Item) + " ";
    //         }
    //       });
    //       break;
    //   }
    // }
    return msg;
  }

  static getResultJackpotMessage(result: InstanceType<typeof Result>) {
    let msg = "";
    if (result) {
      // switch (result.getType()) {
      //   case Result.TYPE.JACKPOT:
      //     msg = "Bão " + this.convertSicboItemToString(ItemWins[0]);
      //     break;
      // }
    }
    return msg;
  }

  static convertItemKeyToItems(key: string): number[] {
    return key.split(",").map((tmp) => parseInt(tmp));
  }


  static getMyJackPot(): number {
    let money = 0;
    // if (result && result.getWinnersList()) {
    //   result.getWinnersList().forEach((record) => {
    //     if (record != null && record.hasBet()) {
    //       let userId = record.getUserId();
    //       if (userId == UserManager.getInstance().userData.userId) {
    //         money = money + record.getAmountFromPot();
    //       }
    //     }
    //   });
    // }
    return money;
  }

  static convertItemsToItemKey(Items: any[]): string {
    let key = "";
    Items.forEach((Item, index) => {
      if (index == Items.length - 1) {
        key += Item;
      } else {
        key += Item + ",";
      }
    });
    return key;
  }

  //EX: let rs = calculateNumOfChipToMakeValue(15156000, chip_level);
  // rs.forEach((num: number, val: number) => {
  //   //console.log("val: " + val + " -> " + num);
  // });
  //
  //Result:   
  // [LOG]: "val: 5000000 -> 3" 
  // [LOG]: "val: 100000 -> 1" 
  // [LOG]: "val: 50000 -> 1" 
  // [LOG]: "val: 5000 -> 1" 
  // [LOG]: "val: 1000 -> 1" 
  static calculateNumOfChipToMakeValue(input: number, config: number[]){
    let resultMap = new Map<number, number>();
    let index = config.length - 1;    
    while(index>=0){       
        let val = config[index];
        let  num = Math.floor(input / val);        
        if(num>0){
            resultMap.set(val, num);
            input -= val * num;
        }
         index--;
    }
    return resultMap;
 }

 static convertItemToDiceSpriteName(item: any){
  let name: string = "SB_maintable_" + item;
  return name;
 }


 static convertCoinTypeToSpriteName(coinType: SicboCoinType, isDisable: boolean = false, isMini: boolean = false) {
  let spriteName = "SB_Main_Chip_";
  let disableSuffix = "_Disable";
  let miniSuffix = "_Mini";

  switch (coinType) {
    case SicboCoinType.Coin1k:
      spriteName += "1K";
      break;
    case SicboCoinType.Coin5k:
      spriteName += "5K";
      break;
    case SicboCoinType.Coin10k:
      spriteName += "10K";
      break;      
    case SicboCoinType.Coin50k:
      spriteName += "50K";
      break;
    case SicboCoinType.Coin100k:
      spriteName += "100K";
      break;      
    case SicboCoinType.Coin500k:
      spriteName += "500K";
      break;
    case SicboCoinType.Coin1M:
      spriteName += "1M";
      break;
    case SicboCoinType.Coin5M:
      spriteName += "5M";
      break;      
  }
  
  if(isDisable) return spriteName + disableSuffix;
  if(isMini) return spriteName + miniSuffix;
  return spriteName;
}

static approximatelyEqual(comparer: number, compareTo: number = 0, delta: number = .1): boolean{
  return comparer >= compareTo && (comparer - compareTo)<= delta;
}

 static doorToItems(door): any[]{//NOTE: THREE_KINDS will not be checked
  switch(door){
      // case Door.BAU_ONE: return [Item.BAU]; 
      // case Door.CUA_ONE: return [Item.CUA];
      // case Door.TOM_ONE: return [Item.TOM];
      // case Door.CA_ONE: return [Item.CA];
      // case Door.NAI_ONE: return [Item.NAI];
      // case Door.GA_ONE: return [Item.GA];

      // case Door.BAU_TOM: return [Item.BAU, Item.TOM];
      // case Door.BAU_CA: return [Item.BAU, Item.CA];
      // case Door.BAU_GA: return [Item.BAU, Item.GA];
      // case Door.BAU_CUA: return [Item.BAU, Item.CUA];
      // case Door.BAU_NAI: return [Item.BAU, Item.NAI];
      // case Door.TOM_CA: return [Item.TOM, Item.CA];
      // case Door.TOM_GA: return [Item.TOM, Item.GA];
      // case Door.TOM_CUA: return [Item.TOM, Item.CUA];
      // case Door.TOM_NAI: return [Item.TOM, Item.NAI];
      // case Door.CA_GA: return [Item.CA, Item.GA];
      // case Door.CA_CUA: return [Item.CA, Item.CUA];
      // case Door.CA_NAI: return [Item.CA, Item.NAI];
      // case Door.GA_CUA: return [Item.GA, Item.CUA];
      // case Door.GA_NAI: return [Item.GA, Item.NAI];
      // case Door.CUA_NAI: return [Item.CUA, Item.NAI];
  }
  return [];
}
 
 static changeParent(child: cc.Node, newParent:cc.Node){
    if(child.parent == newParent) return;
    let p = child.getPosition();
    let pos = SicboGameUtils.convertToOtherNode(child.parent, newParent, new cc.Vec3(p.x, p.y, 0));
    child.parent = newParent;
    child.setPosition(pos);    
  }

  static  fixString(input: string, maxLength = 15, checkChar: number = 7): string{    
   if(input.length<= maxLength) return input;
    let substr = input.substr(0, maxLength);
    let rs="";
    // if(input[maxLength + 1] == ' '||input[maxLength + 1] == '\n'){
    //   rs = substr;
    //   input = input.substr(maxLength);
    // }else
    {        
      for(let i = substr.length - 1; i >= 0; i--){
          if(substr[i] == ' '){
              rs = substr.substr(0, i);
              input = input.substr(i + 1);
              break;
          }
      }
      if(rs == ""){
          rs = substr;
          input = input.substr(maxLength);
      }
    }
    
    return rs + "\n" + SicboHelper.fixString(input);
  }
}




import { SicboResultType } from "../Enums/SicboResultType";
import { SicboCoinType } from "../RNGCommons/SicboCoinType";
import SicboGameUtils from '../RNGCommons/Utils/SicboGameUtils';

