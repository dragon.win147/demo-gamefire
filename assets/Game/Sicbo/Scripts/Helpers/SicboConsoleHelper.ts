// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboController from "../Controllers/SicboController";
import SicboGameUtils from "../RNGCommons/Utils/SicboGameUtils";


const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboConsoleHelper extends cc.Component {
    start () {
        if(window){
            window.packageJson = this.packageJson;
            window.show = SicboController.Instance.onShowEvent;
            window.hide = SicboController.Instance.onHideEvent;
        }
    }

    packageJson(){        
        let data = "";
        if (CC_BUILD) {
          if (SicboGameUtils.isBrowser()) {
            var text = window.localStorage.getItem("package");
            if (text) {
              data = JSON.parse(text);
              //console.log(data);
            }
          } else {
            // if (cc.sys.os == cc.sys.OS_ANDROID) {
            //   ResourceManager.loadAsset("android_app_info", cc.JsonAsset, (jsonAsset: cc.JsonAsset) => {
            //     //console.log(jsonAsset.json);
            //   });
            // } else if (cc.sys.os == cc.sys.OS_IOS) {
            //   ResourceManager.loadAsset("ios_app_info", cc.JsonAsset, (jsonAsset: cc.JsonAsset) => {
            //     //console.log(jsonAsset.json);
            //   });
            // }
          }
        }
    }
}
