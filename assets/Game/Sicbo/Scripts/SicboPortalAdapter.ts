// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboModuleAdapter from "../../../SicboModuleAdapter";

const { Playah, BenTauMessage, Topic, EventKey, GetHistoryReply } = SicboModuleAdapter.getAllRefs();

import SicboResourceManager from "./Managers/SicboResourceManager";

const { ccclass } = cc._decorator;

@ccclass
export default class SicboPortalAdapter {
  protected static instance: SicboPortalAdapter;
  static getInstance(): SicboPortalAdapter {
    if (!SicboPortalAdapter.instance) {
      SicboPortalAdapter.instance = new SicboPortalAdapter();
      SicboPortalAdapter.instance.init();
    }
    return SicboPortalAdapter.instance;
  }

  static destroy() {
    delete SicboPortalAdapter.instance;
    SicboPortalAdapter.instance = null;
  }

  //SECTION GLOBAL DATA
  static GLOBAL_DATA_NAME: string = "GlobalData";
  static globalComp = null;
  static getGlobalData(key: string): any {
    if (SicboPortalAdapter.globalComp == null) {
      let globalNode = cc.director.getScene().getChildByName(SicboPortalAdapter.GLOBAL_DATA_NAME);
      SicboPortalAdapter.globalComp = globalNode.getComponent(SicboPortalAdapter.GLOBAL_DATA_NAME);
    }
    return SicboPortalAdapter.globalComp.get(key);
  }

  //!SECTION

  //SECTION Connect Define
  static addressSanBay() {
    return SicboPortalAdapter.getGlobalData(EventKey.ADDRESS_SAN_BAY);
  }
  static addressBenTau() {
    return SicboPortalAdapter.getGlobalData(EventKey.ADDRESS_BEN_TAU);
  }
  static addressBenTauWS() {
    return SicboPortalAdapter.getGlobalData(EventKey.ADDRESS_BEN_TAU_WS);
  }
  static addressResources() {
    return SicboPortalAdapter.getGlobalData(EventKey.ADDRESS_RESOURCE);
  }
  //!SECTION

  static getBenTauHandler() {
    return SicboPortalAdapter.getGlobalData(EventKey.GET_BENTAU_HANDLER);
  }
  static getToken() {
    return SicboPortalAdapter.getGlobalData(EventKey.GET_TOKEN);
  }

  static getPotterHandler() {
    return SicboPortalAdapter.getGlobalData(EventKey.GET_POTTER_HANDLER);
  }

  isLogVersion: () => boolean = null;
  //SECTION UserManager
  //UserManager.getInstance().userData.userId
  getMyUserId: () => string = null;

  //UserManager.getInstance().getMyPlayah()
  getMyPlayAh: () => InstanceType<typeof Playah> = null;
  //!SECTION

  //SECTION MyAccountHandler
  //SicboUserItemComp, SicboLeaderBoardItemComp
  onGetBasePath: (id: number) => string = null; //basepath, avatar icon
  onGetFramePath: (id: number) => string = null; //frame

  //!SECTION

  getGameInfo: () => [] = null;
  //!SECTION

  //SECTION Portal
  //SicboUIComp
  showUserProfilePopUp: (playah: InstanceType<typeof Playah>, popupBackground: cc.SpriteFrame) => void = null;

  //SicboController
  leaveGame: () => void;

  //GameUtils.gameType = GameType.Sicbo;
  setGameType: () => void = null;

  //SicboMessageHandler
  //showLoading
  showHandlerLoading: (callback: Function) => void = null;
  //hideLoading
  hideHandlerLoading: () => void = null;
  //onShowErr
  onShowHandlerErr: (str: string, onClick?: () => void) => void = null;

  //onDisconnect
  onHandlerDisconnect: (CodeSocket, messError: string) => void = null;
  //!SECTION

  //SECTION Wallet
  getCurrentBalance: (callback?: (currentBalance: number) => void) => void;
  getLatestBalance: () => void;
  enableWallet: () => void;
  disableWallet: () => void;
  addMoney: (amount: number, transaction: number) => void;

  //!SECTION

  init() {
    this.registResourceManager();
    this.registUserManager();
    this.registMyAccountHandler();
    this.registPopup();
    this.registPortal();
    this.registMessageHandler();
    this.registWallet();
  }

  registResourceManager() {
    SicboResourceManager.addressResources = SicboPortalAdapter.addressResources();
  }

  registUserManager() {
    // BCF_EVENT.GET_USER_ID
    this.getMyUserId = () => {
      return SicboPortalAdapter.getGlobalData(EventKey.GET_PLAYAH).getUserId();
    };

    // BCF_EVENT.GET_PLAYAH
    this.getMyPlayAh = () => {
      return SicboPortalAdapter.getGlobalData(EventKey.GET_PLAYAH);
    };
  }

  registMyAccountHandler() {
    // BCF_EVENT.GET_ICON_PATH
    this.onGetBasePath = (id: number) => {
      return SicboPortalAdapter.getGlobalData(EventKey.GET_ICON_PATH).get(id);
    };

    // BCF_EVENT.GET_FRAME_PATH
    this.onGetFramePath = (id: number) => {
      return SicboPortalAdapter.getGlobalData(EventKey.GET_FRAME_PATH).get(id);
    };
  }

  registPopup() {
    //BCF_EVENT.GET_GAME_INFO_BY_TOPIC
    this.getGameInfo = () => {
      return SicboPortalAdapter.getGlobalData(EventKey.GET_GAME_INFO);
    }; //MarketPlace.getGameInfoByTopic(topic)};
  }

  registPortal() {
    // BCF_EVENT.OPEN_USER_INFO_POPUP

    this.showUserProfilePopUp = (playah: InstanceType<typeof Playah>, popupBackground: cc.SpriteFrame) => {
      cc.systemEvent.emit(EventKey.ON_OPEN_POPUP_INFO_EVENT, playah, popupBackground);
    };

    //BCF_EVENT.BACK_TO_PORTAL
    this.leaveGame = () => {
      cc.systemEvent.emit(EventKey.ON_BACK_TO_PORTAL_EVENT, Topic.SICBO);
    };
    // BCF_EVENT.SET_GAME_TYPE
    this.setGameType = () => {
      cc.systemEvent.emit(EventKey.ON_SET_GAME_TYPE_EVENT, Topic.SICBO);
    };

    this.isLogVersion = () => {
      return SicboPortalAdapter.getGlobalData(EventKey.IS_LOG_VERSION);
    };
  }

  registMessageHandler() {
    // BCF_EVENT.HANDLER_SHOW_LOADING_POPUP
    this.showHandlerLoading = (callback: Function) => {
      cc.systemEvent.emit(EventKey.ON_OPEN_POPUP_LOADING_EVENT, callback);
    };

    // BCF_EVENT.HANDLER_HIDE_LOADING_POPUP
    this.hideHandlerLoading = () => {
      cc.systemEvent.emit(EventKey.ON_CLOSE_POPUP_LOADING_EVENT);
    };

    // BCF_EVENT.HANDLER_SHOW_ERR_POPUP
    this.onShowHandlerErr = (str: string, onClick?: () => void) => {
      cc.systemEvent.emit(EventKey.ON_OPEN_POPUP_WARNING_EVENT, str, onClick);
    };

    this.onHandlerDisconnect = (CodeSocket, messError: string) => {
      cc.systemEvent.emit(EventKey.ON_DISCONNECT_EVENT, CodeSocket, false, messError);
    };
  }

  registWallet() {
    // BCF_EVENT.WALLET_GET_CURRENT_BALANCE
    this.getCurrentBalance = (callback?: (currentBalance: number) => void) => {
      cc.log("registWallet GET_CURRENT_BALANCE_EVENT");
      cc.systemEvent.emit(EventKey.GET_CURRENT_BALANCE_EVENT, callback);
      // return SicboPortalAdapter.getGlobalData(EventKey.GET_CURRENT_BALANCE_EVENT)
    };

    // BCF_EVENT.WALLET_GET_LATEST_BALANCE
    this.getLatestBalance = () => {
      cc.systemEvent.emit(EventKey.ON_GET_LATEST_BALANCE_EVENT);
    };

    // BCF_EVENT.WALLET_ENABLE
    this.enableWallet = () => {
      cc.systemEvent.emit(EventKey.ON_ENABLE_WALLET_EVENT, Topic.SICBO);
    };

    // BCF_EVENT.WALLET_DISABLE
    this.disableWallet = () => {
      cc.systemEvent.emit(EventKey.ON_DISABLE_WALLET_EVENT, Topic.SICBO);
    };

    // BCF_EVENT.WALLET_ADD_MONEY
    this.addMoney = (amount: number, transaction: number) => {
      cc.systemEvent.emit(EventKey.ON_ADD_CURRENT_BALANCE_EVENT, amount, transaction);
    };
  }

  // getBundleInfo(callback?: (bundleInfo) => void) {
  //   cc.systemEvent.emit(EventKey.GET_BUNDLE_INFO, Topic.SICBO, callback);
  // }
}
