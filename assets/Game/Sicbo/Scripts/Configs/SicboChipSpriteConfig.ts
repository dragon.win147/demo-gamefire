// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboChipSpriteConfig extends cc.Component {

    @property([cc.SpriteFrame])
    normalChips: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    disableChips: cc.SpriteFrame[] = [];


    @property([cc.SpriteFrame])
    miniChips: cc.SpriteFrame[] = [];

    private static instance: SicboChipSpriteConfig = null;
    static get Instance(): SicboChipSpriteConfig {
      return SicboChipSpriteConfig.instance;
    }

    onLoad(){
        SicboChipSpriteConfig.instance = this;
    }

    onDestroy() {
        delete SicboChipSpriteConfig.instance;
        SicboChipSpriteConfig.instance = null;
      }
    

    getNormalChipSprite(spriteName: string): cc.SpriteFrame {
        let rs: cc.SpriteFrame = null;
        SicboChipSpriteConfig.Instance.normalChips.forEach(element => {
            if(element.name == spriteName){
                rs = element;
            }
        });
        return rs;
    }

    getDisableChipSprite(spriteName: string): cc.SpriteFrame {
        let rs: cc.SpriteFrame = null;
        SicboChipSpriteConfig.Instance.disableChips.forEach(element => {
            if(element.name == spriteName){
                rs = element;
            }
        });
        return rs;
    }

    getMiniChipSprite(spriteName: string): cc.SpriteFrame {
        let rs: cc.SpriteFrame = null;
        SicboChipSpriteConfig.Instance.miniChips.forEach(element => {
            if(element.name == spriteName){
                rs = element;
            }
        });
        return rs;
    }
}
