// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboDiceConfig extends cc.Component {

    @property([cc.SpriteFrame])
    diceSprites: cc.SpriteFrame[] = [];

    private static instance: SicboDiceConfig = null;
    static get Instance(): SicboDiceConfig {
      return SicboDiceConfig.instance;
    }

    onLoad(){
        SicboDiceConfig.instance = this;
    }

    onDestroy() {
        delete SicboDiceConfig.instance;
        SicboDiceConfig.instance = null;
      }
    

    getDice(diceName: string): cc.SpriteFrame {
        let rs: cc.SpriteFrame = null;
        SicboDiceConfig.Instance.diceSprites.forEach(element => {
            if(element.name == diceName){
                rs = element;
            }
        });
        return rs;
    }
}
