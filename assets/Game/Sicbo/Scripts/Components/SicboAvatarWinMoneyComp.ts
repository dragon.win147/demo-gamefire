// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboMoneyFormatComp from "../RNGCommons/SicboMoneyFormatComp";
import SicboSkeletonUtils from "../RNGCommons/Utils/SicboSkeletonUtils";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboAvatarWinMoneyComp extends cc.Component {
    @property(sp.Skeleton)
    animationBack: sp.Skeleton = null;

    @property(sp.Skeleton)
    animationFront: sp.Skeleton = null;

    @property(SicboMoneyFormatComp)
    money: SicboMoneyFormatComp = null;

    backAnimation = "win_avata";
    frontAnimation = "win_avata2";
    
    private showTime = 1;
    private outTime =  .5;

    isShowing: boolean = false;

    onLoad(){
        this.animationBack.node.active = false;
        this.animationFront.node.active = false;
        this.isShowing = false;
    }
  
    show(money: number){   
        if(this.isShowing) return;
        this.isShowing = true;
        this.animationBack.node.active = true;
        this.animationFront.node.active = true;
        this.money.setMoney(money, true);        

        SicboSkeletonUtils.setAnimation(this.animationBack, this.backAnimation, false);
        SicboSkeletonUtils.setAnimation(this.animationFront, this.frontAnimation, false);
        this.money.node.opacity = 255;

        let self = this;
        cc.tween(self.money.node)
            .delay(self.showTime)
            .to(self.outTime, {opacity: 0})
            .call(()=>self.isShowing = false)
            .start();
    }  

    onDestroy(){
        cc.Tween.stopAllByTarget(this.money.node);
    }
}
