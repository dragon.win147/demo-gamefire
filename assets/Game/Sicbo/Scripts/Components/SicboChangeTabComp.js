var customLog = function (logString) {
  cc.log("SicboChangeTabComponent - " + logString);
};

import SicboSoundManager from "../Managers/SicboSoundManager"
cc.Class({
  extends: cc.Component,

  properties: {
    timeMinute: 0,
    closeGameEvent: { default: [], type: cc.Component.EventHandler },
  },

  onDestroy() {
    cc.game.off(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
    cc.game.off(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad() {
    this.timeEnterBg = 0;
    
    cc.game.on(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
    cc.game.on(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
  },


  onChangeTabHide() {
    let onmessage = function (g) {
      let a,
        b,
        c,
        d = Date.now(),
        e = g.data[0],
        f = function () {
          clearTimeout(a);
          b = Date.now();
          c = b - d;
          d = b;
          postMessage(c);
          a = setTimeout(f, e);
        };
      a = setTimeout(f, e);
    };
    let onmessageFunction = `onmessage = ${onmessage.toString().replace(/\n/g, "")}`;
      if (!cc.sys.isNative && !window.webWorker) {
        this.checkTimerEnterBg();
        // cc.game.pause();
        this.onPauseMusic();
        window.webWorker = new Worker(URL.createObjectURL(new Blob([onmessageFunction], { type: "text/javascript" })));
        window.webWorker.onmessage = function (event) {
          cc.director.mainLoop();
          // TweenMax.ticker.tick()
        };
        window.webWorker.postMessage([Math.floor(cc.game.getFrameRate())]);
      }
    },

  onChangeTabShow() {
    if (!cc.sys.isNative && window.webWorker) {
      cc.game.resume();
      this.onResumeMusic();
      this.timeEnterBg = 0;
      clearInterval(this.timeInterval);
      window.webWorker.terminate();
      delete window.webWorker;
    }
  },

  onPauseMusic: function () {
    SicboSoundManager.getInstance().pauseAllEffects();
    SicboSoundManager.getInstance().setMusicVolume(0, false);
    SicboSoundManager.getInstance().setSfxVolume(0, false);
  },

  onResumeMusic: function () {
    SicboSoundManager.getInstance().resumeAllEffects();
    SicboSoundManager.getInstance().loadConfigFromFromStorage();
  },

  checkTimerEnterBg: function () {
    if (this.timeMinute <= 0) return;
    this.timeInterval = setInterval(
      function () {
        this.timeEnterBg++;
        if (this.timeEnterBg >= this.timeMinute * 60) {
          this.closeGameEvent.forEach((element) => {
            element.emit();
          });
          this.timeEnterBg = 0;
          clearInterval(this.timeInterval);
        }
      }.bind(this),
      1000
    );
  },

  start() {},
});
