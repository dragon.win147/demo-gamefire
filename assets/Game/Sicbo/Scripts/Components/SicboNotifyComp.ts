// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { SicboSound } from "../Setting/SicboSetting";
import SicboController from "../Controllers/SicboController";
import SicboSkeletonUtils from "../RNGCommons/Utils/SicboSkeletonUtils";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboNotifyComp extends cc.Component {
    //SMALL NOTI
    @property(cc.Node)
    smallNotiRoot: cc.Node = null;
    @property(cc.Label)
    smallNotiMessage: cc.Label = null;
    skipAllNextNoti: boolean = false;

    //LARGE NOTI
    @property(sp.Skeleton)
    largeNotiSkeleton: sp.Skeleton = null;
    @property(cc.Sprite)
    largeNotiMessage: cc.Sprite = null;

    @property(cc.SpriteFrame)
    bettingMessageSprite: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    stopBettingMessageSprite: cc.SpriteFrame = null;

    //RESULT NOTI
    @property(sp.Skeleton)
    resultNotiSkeleton: sp.Skeleton = null;
    @property(cc.Label)
    resultNotiMessage: cc.Label = null;

    onLoad(){
        this.smallNotiRoot.opacity = 0;
        this.largeNotiSkeleton.node.opacity = 0;        
    }

    //SECTION SMALL
    showSmallNoti(message: string, enterTime: number = .5, showTime: number = 2, exitTime: number = .5, skipAllNextNoti: boolean = false){        
        if(this.skipAllNextNoti) return;
        let self = this;
        cc.Tween.stopAllByTarget(this.smallNotiRoot);
        self.smallNotiMessage.string = message;
        self.smallNotiRoot.active = true;
        cc.tween(self.smallNotiRoot)
            .to(enterTime, {opacity: 255, scale: 1})
            .delay(showTime)
            .to(exitTime, {opacity: 150})
            .call(
                ()=>{
                    self.skipAllNextNoti = false;
                    self.smallNotiRoot.active = false;
                    self.smallNotiRoot.scale = 2;
                }
            )
            .start();
        this.skipAllNextNoti = skipAllNextNoti;
    }

    updateSmallNotiMessage(message: string){
        this.smallNotiMessage.string = message;
    }

    forceHideSmallNoti(){
        cc.Tween.stopAllByTarget(this.smallNotiRoot);
        this.smallNotiRoot.opacity = 150;
        this.smallNotiRoot.active = false;
        this.smallNotiRoot.scale = 2;
        this.skipAllNextNoti = false;
    }
    //!SECTION

    //SECTION LARGE
    showLargeNoti(enterTime: number = 0, showTime: number = 2.5, exitTime: number = .5){        
        cc.Tween.stopAllByTarget(this.largeNotiSkeleton.node);       
        
        SicboSkeletonUtils.setAnimation(this.largeNotiSkeleton, "noti_2");

        this.largeNotiSkeleton.node.opacity = 255;
        cc.tween(this.largeNotiSkeleton.node)
            .to(enterTime, {opacity: 255})
            .delay(showTime)
            .to(exitTime, {opacity: 0})
            .start();            

        SicboController.Instance.playSfx(SicboSound.SfxMessage);
    }

    showBettingMessage(){
        this.largeNotiMessage.spriteFrame = this.bettingMessageSprite;
        this.showLargeNoti();
    }

    showStopBettingMessage(){
        this.largeNotiMessage.spriteFrame = this.stopBettingMessageSprite;
        this.showLargeNoti();
    }

    forceHideLargeNoti(){
        cc.Tween.stopAllByTarget(this.largeNotiSkeleton.node);     
        this.largeNotiSkeleton.node.opacity = 0;
    }
    //!SECTION
  
     //SECTION RESULT
     showResultNoti(message: string, enterTime: number = .34, showTime: number = 2.5, exitTime: number = .34){        
        cc.Tween.stopAllByTarget(this.resultNotiSkeleton.node);       
        
        this.resultNotiMessage.string = message;
        SicboSkeletonUtils.setAnimation(this.resultNotiSkeleton, "in");

        this.resultNotiSkeleton.node.opacity = 255;
        cc.tween(this.resultNotiSkeleton.node)
            .delay(enterTime)
            .call(()=>{
                SicboSkeletonUtils.setAnimation(this.resultNotiSkeleton, "loop", true);
            })
            .delay(showTime)
            .call(()=>{
                SicboSkeletonUtils.setAnimation(this.resultNotiSkeleton, "out", false);
            })
            .delay(exitTime)
            .call(()=>{
                this.resultNotiSkeleton.node.opacity = 0;
            })
            .start();            

        this.resultNotiMessage.node.opacity = 0;
        cc.tween(this.resultNotiMessage.node)
            .delay(enterTime/2)
            .to(enterTime/2,  {opacity: 255})
            .delay(showTime)
            .to(exitTime,  {opacity: 0})
            .start();            

        // SicboController.Instance.playSfx(SicboSound.SfxMessage);
    }


    forceHideResultNoti(){
        cc.Tween.stopAllByTarget(this.resultNotiSkeleton.node);     
        this.resultNotiSkeleton.node.opacity = 0;
    }
    //!SECTION

    onDestroy(){
        cc.Tween.stopAllByTarget(this.smallNotiRoot);
        cc.Tween.stopAllByTarget(this.largeNotiSkeleton.node);        
        cc.Tween.stopAllByTarget(this.resultNotiSkeleton.node);        
        cc.Tween.stopAllByTarget(this.resultNotiMessage.node);        
    }
}
