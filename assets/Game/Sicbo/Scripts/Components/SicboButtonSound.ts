// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboSetting, { SicboSound } from "../Setting/SicboSetting";
import SicboController from "../Controllers/SicboController";
import SicboSoundManager from "../Managers/SicboSoundManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboButtonSound extends cc.Component {
    @property({type: cc.Enum(SicboSound)})
    sound: SicboSound = SicboSound.SfxClick;

    onLoad () {
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; 
        clickEventHandler.component = "SicboButtonSound";
        clickEventHandler.handler = "callback";

        var button = this.node.getComponent(cc.Button);
        if(button)
            button.clickEvents.push(clickEventHandler);

        var toggle = this.node.getComponent(cc.Toggle);
        if(toggle)
            toggle.clickEvents.push(clickEventHandler);

    }

    callback () {
        SicboSoundManager.getInstance().play(SicboSetting.getSoundName(this.sound));
    }

    // update (dt) {}
}
