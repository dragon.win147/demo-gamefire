import SicboModuleAdapter from "../../../../SicboModuleAdapter";

const {
  Door,
  State,
  DoorAndBetAmount,
  Status} = SicboModuleAdapter.getAllRefs();



const { ccclass, property } = cc._decorator;
import SicboUIComp from "./SicboUIComp";
import SicboHelper from "../Helpers/SicboHelper";
import SicboCoinItemComp, { SicboCoinItemCompData } from "./Items/SicboCoinItemComp";
import SicboBetSlotItemComp from "./Items/SicboBetSlotItemComp";


import { SicboSound } from "../Setting/SicboSetting";
import ISicboState from "./States/ISicboState";
import SicboController from "../Controllers/SicboController";
import SicboScrollViewWithButton from '../RNGCommons/UIComponents/SicboScrollViewWithButton';
import SicboGameUtils from "../RNGCommons/Utils/SicboGameUtils";
import { SicboCoinType } from "../RNGCommons/SicboCoinType";



@ccclass
export default class SicboBetComp extends cc.Component implements ISicboState{ 
  sicboUIComp:SicboUIComp = null;

  @property(cc.ScrollView)
  chipsScrollView: cc.ScrollView = null;
  currentSelectedChip: SicboCoinItemComp = null;

  @property(cc.Prefab)
  largeChipPrefab:cc.Prefab = null;

  @property(cc.Button)
  rebetButton: cc.Button = null;
  
  @property(cc.Button)
  doubleBetButton: cc.Button = null;

  @property(cc.Node)
  betSlotsNode:cc.Node = null;
  allBetSlots: SicboBetSlotItemComp[] = [];

  @property(cc.Node)
  miniChipRoot:cc.Node = null;
  miniChipPool:cc.NodePool = null;

  @property(cc.Node)
  loseChipTarget:cc.Node = null;

  @property(cc.Node)
  winChipPos:cc.Node = null;

  private _disableChip: boolean = false;

  private chip_data = [];
  private chipItems : SicboCoinItemComp[] = [];
//ANCHOR chip stack pos config
  chipStackPosConfig = {
    anyTrippleCloneSpacing: 30,
    singleCloneSpacing: 3,
    smallCloneSpacing: 40,
    bigCloneSpacing: -40,
    cloneSpacing: 4,
    chipSpacing: 4,

    chipStackOffsetRight: new cc.Vec2( -27, 25),
    chipStackOffsetLeft: new cc.Vec2( 27, 25),

    anyTripple : [
      {
          pos: new cc.Vec2( -84, -34),
          height: 10,
      },
      {
          pos: new cc.Vec2( -56, -33),
          height: 10,
      }
    ], 

    trippleLeft : [
      {
        pos: new cc.Vec2( -82, -8),
        height: 10,
      },
    ],

    trippleRight : [
      {
        pos: new cc.Vec2( 82, -8),
        height: 10,
      },
    ], 

    small : [
      {
          pos: new cc.Vec2( -75.5, -50),
          height: 10,
      },
      {
          pos: new cc.Vec2( -46, -51),
          height: 10,
      },
      // {
      //     pos: new cc.Vec2( -54, -74.5),
      //     height: 5,
      // }
   ], 

   big : [
    {
      pos: new cc.Vec2(76.5, -51),
      height: 10,
    },
    {
      pos: new cc.Vec2(47, -50),
      height: 10,
    },
   
    // {
    //     pos: new cc.Vec2( 54.5, -74.5),
    //     height: 5,
    // }
  ], 

  four_sum : [
    {
      pos: new cc.Vec2( -20, -23.5), //-44.5),
      height: 10,
    }, 
  ],

  five_sum : [
    {
      pos: new cc.Vec2( -20, -23.5), //-44.5),
      height: 10,
    }, 
  ],


  six_sum : [
    {
      pos: new cc.Vec2( -23, -23.5), //-44.5),
      height: 10,
    }, 
  ],


  seven_sum : [
    {
      pos: new cc.Vec2( -20, -23.5), //-44.5),
      height: 10,
    }, 
  ],


  eight_sum : [
    {
      pos: new cc.Vec2( -20, -23.5), //-44.5),
      height: 10,
    }, 
  ],

  nine_sum : [
    {
      pos: new cc.Vec2( -19, -23.5), //-44.5),
      height: 10,
    }, 
  ],

  ten_sum : [
    {
      pos: new cc.Vec2( -19, -23.5), //-44.5),
      height: 10,
    }, 
  ],


  eleven_sum : [
    {
      pos: new cc.Vec2( -18, -23.5), //-44.5),
      height: 10,
    }, 
  ],


  twelve_sum : [
    {
      pos: new cc.Vec2( -18, -23.5), //-44.5),
      height: 10,
    }, 
  ],


  thirteen_sum : [
    {
      pos: new cc.Vec2( -16.5, -23.5), //-44.5),
      height: 10,
    }, 
  ],

  fourteen_sum : [
    {
      pos: new cc.Vec2( -16, -23.5), //-44.5),
      height: 10,
    }, 
  ],

  fifteen_sum : [
    {
      pos: new cc.Vec2( -15.5, -23.5), //-44.5),
      height: 10,
    }, 
  ],


  sixteen_sum : [
    {
      pos: new cc.Vec2( -15.5, -23.5), //-44.5),
      height: 10,
    }, 
  ],


  seventeen_sum : [
    {
      pos: new cc.Vec2( -14.5, -23.5), //-44.5),
      height: 10,
    }, 
  ],

  single : [
    {
        pos: new cc.Vec2( -57.5, -22),
        height: 10,
    },
    // {
    //     pos: new cc.Vec2( -29.5, -21),
    //     height: 5,
    // }
  ], 
 
}
  
  onLoad(){
    this.sicboUIComp = this.node.parent.getComponentInChildren(SicboUIComp);

    this.allBetSlots = this.betSlotsNode.getComponentsInChildren(SicboBetSlotItemComp);    

    this.miniChipPool = new cc.NodePool();        
  }

  getDoor(door):SicboBetSlotItemComp{
    return this.allBetSlots.find(x=>x.door == door);
  }
  
  //SECTION Chip scroll view
  createChipItems(data: number[]) {
    this.chip_data = data;
    this.createItemChipCoins(data);
    this.selectChip(this.chipItems[0], false);
    this.chipsScrollView.scrollToTopLeft();
    this.chipsScrollView.getComponent(SicboScrollViewWithButton)?.checkScrollViewButton();
  }

  
  public createItemChipCoins(data: number[]) {
    data.forEach((chipValue) => {
      this.createItemCoin(chipValue);
    });
  }

  createItemCoin(chipValue: number) {    
    let coinType = SicboHelper.convertValueToCoinType(chipValue);
    var newItem = SicboGameUtils.createItemFromPrefab(SicboCoinItemComp, this.largeChipPrefab, this.chipsScrollView.content);

    let itemData = new SicboCoinItemCompData();
    itemData.coinType = coinType;    
    itemData.onClick = (sicboCoinItemComp) => {
      this.selectChip(sicboCoinItemComp);
    };
    newItem.setData(itemData);  
    newItem.isDisable = this.disableChip;  
    this.chipItems.push(newItem);
  }
  
  selectChip(sicboCoinItemComp: SicboCoinItemComp, playSfx: boolean = true){
    if(this.disableChip) return;
    if(this.currentSelectedChip!=null) this.currentSelectedChip.selectEff(false);
    this.currentSelectedChip = sicboCoinItemComp;
    this.currentSelectedChip.selectEff(true);
    //TODO: change sound khi bam chip binh thuong va disable
    if(playSfx)
      SicboController.Instance.playSfx(SicboSound.SfxClick);
    
  }

  getSelectedChipType():SicboCoinType{
    return this.currentSelectedChip.data.coinType;
  }

  onClickChipScrollNext(){
    let numOfItem = this.chipsScrollView.content.childrenCount;
    let spacing = this.chipsScrollView.content.width/numOfItem;

    let offset = this.chipsScrollView.getScrollOffset();
    let maxOffset = this.chipsScrollView.getMaxScrollOffset();

    this.chipsScrollView.scrollToPercentHorizontal((Math.abs(offset.x) + spacing)/maxOffset.x, .5);
  }

  onClickChipScrollPre(){
    let numOfItem = this.chipsScrollView.content.childrenCount;
    let spacing = this.chipsScrollView.content.width/numOfItem;

    let offset = this.chipsScrollView.getScrollOffset();
    let maxOffset = this.chipsScrollView.getMaxScrollOffset();

    this.chipsScrollView.scrollToPercentHorizontal((Math.abs(offset.x) - spacing)/maxOffset.x, .5);
  }

  scrollToInsideView(chip: cc.Node){
    this.chipsScrollView.stopAutoScroll();
    let pos = SicboGameUtils.convertToOtherNode(chip, this.chipsScrollView.node);
    if(pos.x>=-230 && pos.x<=200) return;// Inside view

    let offset = this.chipsScrollView.getScrollOffset();
    let maxOffset = this.chipsScrollView.getMaxScrollOffset();

    this.chipsScrollView.scrollToPercentHorizontal((Math.abs(offset.x) + pos.x)/maxOffset.x);
    this.chipsScrollView.getComponent(SicboScrollViewWithButton)?.checkScrollViewButton();
  }
  //!SECTION

  //#region BET IN TABLE
    /** 
    * Bam vao o dat cuoc tren ban
		*/
    onClickBetSlotItem(betSlotItemComp: SicboBetSlotItemComp){
      if(this.disableChip) return;
      this.scrollToInsideView(this.currentSelectedChip.node);
      this.sicboUIComp.myUserBet(betSlotItemComp.door, this.currentSelectedChip.data.coinType);
      // this.putMiniChipOnTableImmediately(this.currentSelectedChip.data.coinType, betSlotItemComp);
    }

  createItemCoinMini(coinType: SicboCoinType) {    
    let newItem:SicboCoinItemComp = null;    
    if (this.miniChipPool.size() > 0) { 
      newItem = this.miniChipPool.get().getComponent("SicboCoinItemComp");
    } else {
      newItem = SicboGameUtils.createItemFromPrefab(SicboCoinItemComp, this.largeChipPrefab, this.chipsScrollView.content);
    }
    newItem.node.parent = this.miniChipRoot;
    newItem.node.scale = 1;
    newItem.node.opacity = 255;

    let itemData = new SicboCoinItemCompData();
    itemData.coinType = coinType;    
    itemData.isMini = true;
    newItem.setData(itemData);
    
    // newItem.rotation = -this.randomRange(0, 360);

    //ANCHOR minichip Scale
    // newItem.isOnTable = true;
    return newItem;
  }

  recycleItemCoinMini(item: cc.Node){
    item.opacity = 255;
    this.miniChipPool.put(item);    
    cc.Tween.stopAllByTarget(item);
  }

  getRandomPos(betSlotItemComp: SicboBetSlotItemComp):cc.Vec2{
    let betArea = betSlotItemComp.miniChipArea;
    let rs = cc.Vec2.ZERO;
    
    let offset = cc.Vec2.ZERO;// new cc.Vec2(20, 20);
    if(betSlotItemComp.door == Door.BIG) offset = new cc.Vec2(-22, 0); //NOTE for layer fix when throw chip
    if(betSlotItemComp.door == Door.SMALL) offset = new cc.Vec2(22, 0); //NOTE for layer fix when throw chip

    let halfW = (betArea.width)/2;
    let halfH = (betArea.height)/2;
    rs = new cc.Vec2(
        this.randomRange(-halfW, halfW) + offset.x,
        this.randomRange(-halfH, halfH) + offset.y
    );
            
    rs.add(betArea.getPosition());

    let rsV3 = SicboGameUtils.convertToOtherNode(betArea, this.miniChipRoot, new cc.Vec3(rs.x, rs.y, 0));
    rs.x = rsV3.x;
    rs.y = rsV3.y;
    return rs;
  }

  randomRange(min:number, max:number):number{
    return Math.random() * (max - min) + min;
  }

  getMiniChipStaticPos(betSlot: SicboBetSlotItemComp){
    let chipSpacing = this.chipStackPosConfig.chipSpacing;

    let count = betSlot.miniChipsOnSlot.length;     
    let col = 0;
    let row = count; 


    // if (betSlot.door == Door.ANY_TRIPLE) {
    if (this.getChipStackMaxCol(betSlot.door)>1) {
        let maxHeight0 = this.getChipStackHeight(betSlot.door, 0);
        if(count >= maxHeight0){
          col = 1;
          row = count - maxHeight0;
        }
    }
    let startPos = this.getChipStackStartPos(betSlot.door, col);
    let y = startPos.y;
    y += row * chipSpacing;

    return SicboGameUtils.convertToOtherNode2(betSlot.miniChipArea, this.miniChipRoot, new cc.Vec2(startPos.x, y));
  }

  //ANCHOR  putMiniChipOnTableImmediately
  putMiniChipOnTableImmediately(coinType: SicboCoinType, betSlotItemComp: SicboBetSlotItemComp){ 
    let chip = this.createItemCoinMini(coinType);    

    chip.node.setPosition(this.getRandomPos(betSlotItemComp));
    if(this.isStaticSlot(betSlotItemComp.door)){
      chip.node.setPosition(this.getMiniChipStaticPos(betSlotItemComp));
    }else{
      chip.node.setPosition(this.getRandomPos(betSlotItemComp));
    }
    this.changeParent(chip.node, betSlotItemComp.miniChipArea);
    betSlotItemComp.addMiniChipToSlot(chip);
    this.removeRedundancyChip(betSlotItemComp);
  }

  putMiniChipOnTableImmediatelyByValue(totalValue: number, betSlotItemComp: SicboBetSlotItemComp){
    if(betSlotItemComp == null){
      //cc.log("putMiniChipOnTableImmediatelyByValue NO DOOR FOUND");
      return;
    }
    let parsedCoin = SicboHelper.calculateNumOfChipToMakeValue(totalValue, this.chip_data);
    
    parsedCoin.forEach((num: number, val: number) => {
      let coinType = SicboHelper.convertValueToCoinType(val);
      for (let i = 0; i < num; i++) {
        this.putMiniChipOnTableImmediately(coinType, betSlotItemComp);
      }
    });
  }

  //ANCHOR  throwMiniChipOnTable
  throwMiniChipOnTableByValue(totalValue: number, from:cc.Node = null, betSlotItemComp: SicboBetSlotItemComp){  
    if(betSlotItemComp == null){
      //cc.log("throwMiniChipOnTableByValue NO DOOR FOUND");
      return;
    }  

    let parsedCoin = SicboHelper.calculateNumOfChipToMakeValue(totalValue, this.chip_data);
    
    parsedCoin.forEach((num: number, val: number) => {
      // //console.log("val: " + val + " -> " + num);
      let coinType = SicboHelper.convertValueToCoinType(val);
      for (let i = 0; i < num; i++) {
        this.throwMiniChipOnTable(coinType, from, betSlotItemComp);
      }
    });
  }

  throwMiniChipOnTable(coinType: SicboCoinType, from:cc.Node = null, betSlotItemComp: SicboBetSlotItemComp){
    if(from == null) from = this.currentSelectedChip.node;

    let chip = this.createItemCoinMini(coinType);    

    let startPos = SicboGameUtils.convertToOtherNode(from, betSlotItemComp.miniChipArea);

    let targetPos =  this.getMiniChipStaticPos(betSlotItemComp)
    let randomPos =  this.getRandomPos(betSlotItemComp);  

    targetPos = SicboGameUtils.convertToOtherNode2(chip.node.parent, betSlotItemComp.miniChipArea, targetPos);
    randomPos = SicboGameUtils.convertToOtherNode2(chip.node.parent, betSlotItemComp.miniChipArea, randomPos);

    chip.node.parent = betSlotItemComp.miniChipArea;
    chip.node.setPosition(startPos);   
    // chip.node.setSiblingIndex
    betSlotItemComp.addMiniChipToSlot(chip);  

    let chipThrowDuration = 0.9;
    let fadeOutDelay = 0.5;
    if(betSlotItemComp.door != Door.BIG && betSlotItemComp.door != Door.SMALL){
      randomPos = cc.Vec2.ZERO;
      fadeOutDelay = 0;
    }  

    chip.node.scale = 1.2;
    chip.node.opacity = 255;

    cc.tween(chip.node)
    .to(chipThrowDuration/3, {x:randomPos.x, y:randomPos.y, scale: 1})
    .delay(fadeOutDelay)
    .to(chipThrowDuration/3, {opacity: 0, x:targetPos.x, y:targetPos.y})
    //.to(0, {x:targetPos.x, y:targetPos.y})
    .to(chipThrowDuration/3, {opacity: 255})
    .call(
      ()=>{
        this.removeRedundancyChip(betSlotItemComp);
      }
    )
    .start();
  
  }


  private changeParent(child: cc.Node, newParent:cc.Node){
    SicboHelper.changeParent(child, newParent);
  }
  //#endregion

  //SECTION WIN - LOSE
  //ANCHOR chip to lose hole animation
  playLoseAnimation(winDoors: any[], duration: number = .75){        
    let tweenParams = {
      position : cc.Vec3.ZERO,
      // scale : 0,
    }
    this.allBetSlots.forEach(betSlot => {
        if(winDoors.find(x=>x == betSlot.door) == null){
        let lostChips = betSlot.miniChipsOnSlot;
        
        let self = this;
        self.tweenChipToAnotherNode(self.loseChipTarget, lostChips
          , duration, 0, tweenParams
          , true,  (chip:SicboCoinItemComp)=>{
            //ANCHOR SOUND
            SicboController.Instance.playSfx(SicboSound.SfxChipFly);
            self.recycleItemCoinMini(chip.node);                        
        });
        betSlot.clearMiniChipOnSlot();
      }
    });
  }

  //ANCHOR stack chip into column
  playChipStackAnimation(winDoors: any[], duration: number = .5){
    //cc.log(duration);
    let chipSpacing = this.chipStackPosConfig.chipSpacing;

    this.allBetSlots.forEach(betSlot => {
      if(this.isStaticSlot(betSlot.door)) return;
      if(winDoors.find(x=>x == betSlot.door) != null){
          let chips = betSlot.miniChipsOnSlot;      
          betSlot.chipColCount = 0;

          let count = 0;
          let maxCol = this.getChipStackMaxCol(betSlot.door);

          for (let col = 0; col < maxCol; col++) {
            betSlot.chipColCount++;
            
            let maxHeight = this.getChipStackHeight(betSlot.door, col);
            let startPos = this.getChipStackStartPos(betSlot.door, col);
            let offSet = cc.Vec2.ZERO;//this.getChipStackOffset(betSlot.door);

            let x = startPos.x;
            for (let h = 0; h < maxHeight; h++) {
              if(chips.length<=0) break;

              let y = startPos.y;
              y += h*chipSpacing;

              let idx = Math.floor(Math.random() * chips.length);
              let chip = chips[idx];
              chips.splice(idx, 1);
              
              count++;
              chip.node.setSiblingIndex(count);

              cc.tween(chip.node)
                .to(duration , {x: x + offSet.x, y: y + offSet.y})
                .call(
                  ()=>{          
                    //ANCHOR SOUND
                    SicboController.Instance.playSfx(SicboSound.SfxChipFly);                          
                    cc.Tween.stopAllByTarget(chip.node);                        
                  }
                )
                .start();
              }
            }

            for (let i = 0; i < chips.length; i++) {
              let chip = chips[i];
              this.recycleItemCoinMini(chip.node);                        
            }
            // betSlot.miniChipsOnSlot = cachedMiniChip;
          }
        }
      );
    }

    getChipStackConfig(door: any):any{
      switch (door) {
        case Door.ANY_TRIPLE: 
          return this.chipStackPosConfig.anyTripple;
  
        case Door.BIG:
          return this.chipStackPosConfig.big;
  
        case Door.SMALL:
          return this.chipStackPosConfig.small;
  
        case Door.ONE_TRIPLE: case Door.TWO_TRIPLE: case Door.THREE_TRIPLE:
          return this.chipStackPosConfig.trippleLeft;

        case Door.FOUR_TRIPLE: case Door.FIVE_TRIPLE: case Door.SIX_TRIPLE:
          return this.chipStackPosConfig.trippleRight;

        case  Door.ONE_SINGLE: case Door.TWO_SINGLE: case Door.THREE_SINGLE: case  Door.FOUR_SINGLE: case Door.FIVE_SINGLE: case Door.SIX_SINGLE:
          return this.chipStackPosConfig.single; 
        case Door.FOUR_SUM: return this.chipStackPosConfig.four_sum; 
        case Door.FIVE_SUM: return this.chipStackPosConfig.five_sum; 
        case Door.SIX_SUM: return this.chipStackPosConfig.six_sum; 
        case Door.SEVEN_SUM: return this.chipStackPosConfig.seven_sum; 
        case Door.EIGHT_SUM: return this.chipStackPosConfig.eight_sum; 
        case Door.NINE_SUM: return this.chipStackPosConfig.nine_sum; 
        case Door.TEN_SUM: return this.chipStackPosConfig.ten_sum; 
        case Door.ELEVEN_SUM: return this.chipStackPosConfig.eleven_sum; 
        case Door.TWELVE_SUM: return this.chipStackPosConfig.twelve_sum; 
        case Door.THIRTEEN_SUM: return this.chipStackPosConfig.thirteen_sum; 
        case Door.FOURTEEN_SUM: return this.chipStackPosConfig.fourteen_sum; 
        case Door.FIFTEEN_SUM: return this.chipStackPosConfig.fifteen_sum; 
        case Door.SIXTEEN_SUM: return this.chipStackPosConfig.sixteen_sum; 
        case Door.SEVENTEEN_SUM: return this.chipStackPosConfig.seventeen_sum; 
      }
    }
    getChipStackStartPos(door: any, col: number){
      return this.getChipStackConfig(door)[col].pos;
    }
  
    getChipStackOffset(door: any){
      return this.isRightChipStack(door)? this.chipStackPosConfig.chipStackOffsetRight: this.chipStackPosConfig.chipStackOffsetLeft;
    }

    getChipStackHeight(door: any, col: number){
      return this.getChipStackConfig(door)[col].height;
    }
  
    getChipStackMaxCol(door: any){
      return this.getChipStackConfig(door).length;
    }

    getCloneSpacing(door: any):number{
      switch (door) {
        case Door.ANY_TRIPLE:
          return this.chipStackPosConfig.anyTrippleCloneSpacing;
        case Door.ONE_SINGLE: case Door.TWO_SINGLE: case Door.THREE_SINGLE:case Door.FOUR_SINGLE: case Door.FIVE_SINGLE: case Door.SIX_SINGLE:
          return this.chipStackPosConfig.singleCloneSpacing;
        case Door.BIG: return this.chipStackPosConfig.bigCloneSpacing;
        case Door.SMALL: return this.chipStackPosConfig.smallCloneSpacing;
      }       
      
      return this.isRightChipStack(door)?-this.chipStackPosConfig.cloneSpacing:this.chipStackPosConfig.cloneSpacing;
    }

    isStaticSlot(door: any){
      return true;
      switch (door) {
        case Door.ANY_TRIPLE:
        case Door.ONE_TRIPLE: case Door.TWO_TRIPLE: case Door.THREE_TRIPLE:case Door.FOUR_TRIPLE: case Door.FIVE_TRIPLE: case Door.SIX_TRIPLE:
          return true;     
      }       
      return false;
    }

    isRightChipStack(door: any) : boolean{
      switch (door) {
        case Door.FOUR_TRIPLE: case Door.FIVE_TRIPLE: case Door.SIX_TRIPLE: case Door.BIG:
          return true;     
      }       
      return false;
    }

    getCloneStackSibling(door: any):number{
      return this.isRightChipStack(door)?1:0;
    }

    //ANCHOR return won chips to slot    
    dealerReturnWinChip(winDoors: any[], onFinish:(door: any)=>void = null){
      return this.dealerReturnChipOneByOne(winDoors, .05, .5, onFinish);
    }    

    dealerReturnFullStackChip(winDoors: any[], duration:number, onFinish:(door: any)=>void = null){
      this.allBetSlots.forEach(betSlot => {
        if(winDoors.find(x=>x == betSlot.door) != null){      
          let offset = this.getChipStackOffset(betSlot.door);
          let cloneChips = cc.instantiate(betSlot.miniChipArea);
          let spacing = this.getCloneSpacing(betSlot.door);
          cloneChips.parent = this.winChipPos;
          cloneChips.position = new cc.Vec3(offset.x, offset.y, 0);
          betSlot.setStackChipRoot(cloneChips);

          this.changeParent(cloneChips, betSlot.node);
          cloneChips.setSiblingIndex(this.getCloneStackSibling(betSlot.door));

          cc.tween(cloneChips)
          .to(duration , {position: new cc.Vec3(offset.x + spacing, 0, 0)})
          .call(()=>{
            if(onFinish) onFinish(betSlot.door);
          })
          .start();
        }
      });
    }

    private dealerReturnChipOneByOne(winDoors: any[], delayBetweenChipTime: number = .05, chipFlyTime: number = .5, onFinish:(door: any)=>void = null){
      let maxEffectTime = 0;

      this.allBetSlots.forEach(betSlot => {
        if(winDoors.find(x=>x == betSlot.door) != null){      
          let offset = this.getChipStackOffset(betSlot.door);
          let cloneChips = cc.instantiate(betSlot.miniChipArea);
          let spacing = this.getCloneSpacing(betSlot.door);
          cloneChips.parent = betSlot.node;
          cloneChips.position = new cc.Vec3(offset.x + spacing, 0, 0);
          betSlot.setStackChipRoot(cloneChips);

          // this.changeParent(cloneChips, betSlot.node);
          cloneChips.setSiblingIndex(this.getCloneStackSibling(betSlot.door));

          let chipsInStack = cloneChips.children;
          let delay = delayBetweenChipTime;
          let startPos = SicboGameUtils.convertToOtherNode(this.winChipPos, cloneChips);
          maxEffectTime = Math.max(maxEffectTime, chipsInStack.length * delay + chipFlyTime);

          for (let i = 0; i < chipsInStack.length; i++) {            
            let chip = chipsInStack[i];
            let chipPos = chip.position;
            
            chip.position = startPos;
            chip.active = false;

            cc.tween(this.node)
              .delay(i * delay)                 
              .call(
                ()=>{                  
                  chip.active = true;
                  cc.tween(chip).to(chipFlyTime , {position: chipPos})
                    .call(()=>{
                      //ANCHOR SOUND
                      SicboController.Instance.playSfx(SicboSound.SfxChipFly);
                      if(i == chipsInStack.length - 1){
                        if(onFinish) onFinish(betSlot.door);
                      }
                    })
                    .start();
                }
              )              
              .start();
          }          
        }
      });

      return maxEffectTime;
    }


  //ANCHOR chip to win player animation
  playPayingAnimation(winDoor: any, user: cc.Node, wonAmount: number, duration:number = 1){
    let tweenParams = {
      useBezier: true,
      position : cc.Vec3.ZERO,
      // scale : 0,
    }

    let betSlot = this.getDoor(winDoor);

    let winChips = this.getChipForPaying(wonAmount);
    let tweenChips:SicboCoinItemComp[] = [];
    winChips.forEach(coinType => {
      let chipComp = this.createItemCoinMini(coinType);
      chipComp.node.parent = betSlot.miniChipArea;
      chipComp.node.position = cc.Vec3.ZERO;

      tweenChips.push(chipComp);
    });
  
    let self = this;
    let delayBetweenChipTime = .1;
    self.tweenChipToAnotherNode(user, tweenChips
        , duration, delayBetweenChipTime, tweenParams
        , true,  (chip:SicboCoinItemComp)=>{
          self.recycleItemCoinMini(chip.node);                      
        });
    this.clearMiniChipOnSlot(betSlot);
  }

  clearMiniChipOnSlot(betSlot: SicboBetSlotItemComp){
    betSlot.miniChipsOnSlot.forEach(chip => {
      this.recycleItemCoinMini(chip.node);   
    });
    betSlot.clearMiniChipOnSlot();

    betSlot.miniChipsOnStack.forEach(chip => {
      this.recycleItemCoinMini(chip.node);
    });
    betSlot.deleteStackChipRoot();
  }


  destroyAllMiniChip(){
    for (let i = 0; i < this.allBetSlots.length; i++) {   
      let betSlot = this.allBetSlots[i];
      betSlot.miniChipsOnSlot.forEach(chip => {
        chip.node?.destroy();   
      });
      betSlot.clearMiniChipOnSlot();
  
      betSlot.miniChipsOnStack.forEach(chip => {
        chip.node?.destroy();  
      });
      betSlot.deleteStackChipRoot();
    }
    this.miniChipPool.clear();
  }

  getChipForPaying(wonValue:number):SicboCoinType[]{
    if(wonValue<= 5000){
      return [SicboCoinType.Coin1k, SicboCoinType.Coin1k, SicboCoinType.Coin1k];
    }else if(wonValue<= 20000){
      return [SicboCoinType.Coin5k, SicboCoinType.Coin5k, SicboCoinType.Coin5k];
    }else if(wonValue<= 100000){
      return [SicboCoinType.Coin100k];
    }else if(wonValue<= 1000000){
      return [SicboCoinType.Coin1M];
    }else{
      return [SicboCoinType.Coin5M, SicboCoinType.Coin5M, SicboCoinType.Coin5M];
    }
  }


    removeRedundancyChip(betSlot: SicboBetSlotItemComp){
      let mergeFrom = -1;
      if(this.isStaticSlot(betSlot.door)){
        if (this.getChipStackMaxCol(betSlot.door)>1) {
          mergeFrom = this.getChipStackHeight(betSlot.door, 0);
        }else{
          mergeFrom = 0;
        }

        mergeFrom += 5;
      }

      let rc = betSlot.getRedundancyChip(this.isStaticSlot(betSlot.door), mergeFrom);
      rc.forEach(chip => {
        this.recycleItemCoinMini(chip.node);         
      });      
    }    
    
    tweenChipToAnotherNode(targetNode: cc.Node, chipList: SicboCoinItemComp[]
                          , duration: number, delayBetweenChipTime: number = 0, tweenParams:any
                          , changeParentToTargetNode: boolean = true, onComplete: (chip: SicboCoinItemComp) => void){
      for (let i = 0; i < chipList.length; i++) {
        let chip = chipList[i];
        let cachedParent = chip.node.parent;

        if(chip.node.parent == null) {
          //cc.log("Fail to tweenChipToAnotherNode");
          continue;
        }

        this.changeParent(chip.node, targetNode);      
        
        let tween:cc.Tween = null;
        if(tweenParams.useBezier){
          let startPos = new cc.Vec2(chip.node.position.x, chip.node.position.y);
          let endPos = new cc.Vec2(tweenParams.position.x, tweenParams.position.y);
          tween =  cc.tween(chip.node).delay(i * delayBetweenChipTime).bezierTo(duration, startPos, new cc.Vec2(startPos.x, startPos.y + (endPos.y- startPos.y)*1), endPos);
        }else{
          tween = cc.tween(chip.node).delay(i * delayBetweenChipTime).to(duration , tweenParams);
        }
       
        tween.call(
          ()=>{
            if(changeParentToTargetNode == false) 
              this.changeParent(chip.node, cachedParent);   
            if(onComplete != null) onComplete(chip);                        
          }
        )
        .start();
      }
    }

    //ANCHOR Tween    
    tween(node:cc.Node, targetNode: cc.Node, tweenParams:any, onComplete: () => void){
      let duration = tweenParams.duration || 0;
      let delay = tweenParams.delay || 0;
      let targetPos = tweenParams.positionInTargetNode || cc.Vec2.ZERO;    
      let bezierPoint = tweenParams.bezierPoint || cc.Vec2.ZERO;    

      targetPos = SicboGameUtils.convertToOtherNode2(node.parent, targetNode, targetPos);
  
      let tween:cc.Tween = null;
      
      let startPos = new cc.Vec2(node.position.x, node.position.y);
      let endPos = targetPos;
      if(tweenParams.useBezier){
        tween =  cc.tween(node).delay(delay).bezierTo(duration, startPos, bezierPoint, endPos);
      }else{
        tween = cc.tween(node).delay(delay).to(duration , {x: endPos.x, y: endPos.y});
      }
      
      tween.call(
        ()=>{          
          onComplete && onComplete();                        
        }
      )
      .start();
    }
  //!SECTION
  
  //SECTION update bet number in table  
  updateMyUserBetOnTable(userBetAmounts: InstanceType<typeof DoorAndBetAmount>[]) {
    userBetAmounts.forEach(betAmount => {
      let door = this.getDoor(betAmount.getDoor());
      door?.setMyTotalBet(betAmount.getBetAmount());
    });
  }

  updateTotalBetOnTable(totalBetAmounts: InstanceType<typeof DoorAndBetAmount>[]) {
    totalBetAmounts.forEach(betAmount => {
      let door = this.getDoor(betAmount.getDoor());
      door?.setTotalBet(betAmount.getBetAmount());
    });
  }

  //!SECTION

  //SECTION ISicboState
  exitState(status: any): void {  
    if(status == Status.PAYING){
      for (let i = 0; i < this.allBetSlots.length; i++) {
        this.allBetSlots[i].clearMiniChipOnSlot();
        this.allBetSlots[i].resetSlot();
      }
    }
  }

  updateState(state: InstanceType<typeof State>){
    if(state.getStatus() == Status.BETTING || state.getStatus() == Status.END_BET || state.getStatus() == Status.RESULTING|| state.getStatus() == Status.PAYING){
      this.updateTotalBetOnTable(state.getDoorsAndBetAmountsList());
      this.updateMyUserBetOnTable(state.getDoorsAndPlayerBetAmountsList());
    }

    this.setDoubleBetActive(state.getAllowBetDouble());
    this.setRebetActive(state.getAllowReBet());
  }

  startState(state: InstanceType<typeof State>, totalDuration: number): void {   
    let status = state.getStatus();

    this.disableChip = status != Status.BETTING;
    
    if(status != Status.WAITING && status != Status.FINISHING){
      state.getDoorsAndBetAmountsList().forEach(dab => {
        let doorComp = this.getDoor(dab.getDoor());
        if(doorComp.miniChipsOnSlot.length == 0)
          this.putMiniChipOnTableImmediatelyByValue(dab.getBetAmount(), doorComp);
      });
    }

    if(status == Status.WAITING||status == Status.FINISHING){
      for (let i = 0; i < this.allBetSlots.length; i++) {        
        this.allBetSlots[i].resetSlot();
      }
    }
  }

  //ANCHOR disable chip
  public get disableChip(): boolean {
    return this._disableChip;
  }
  public set disableChip(value: boolean) {
    this._disableChip = value;
    this.chipItems.forEach(chip => {
      chip.isDisable = value;
      if(!value){
        if(this.currentSelectedChip!=null) 
          this.selectChip(this.currentSelectedChip, false);
        else 
          this.selectChip(this.chipItems[0], false);
      }
    });
  }

  //Quick bet
  setDoubleBetActive(isActive: boolean){
    this.doubleBetButton.interactable = isActive;
  }

  setRebetActive(isActive: boolean){
    this.rebetButton.interactable = isActive;
  }

  //!SECTION
  onDestroy(){
    cc.Tween.stopAllByTarget(this.node);
  }

  onHide(): void {
    cc.Tween.stopAllByTarget(this.node);

    for (let i = 0; i < this.allBetSlots.length; i++) {        
      this.allBetSlots[i].resetSlot();
      // this.clearMiniChipOnSlot(this.allBetSlots[i]);
    }
    this.destroyAllMiniChip();
  }
}
