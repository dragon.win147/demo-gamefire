// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property, executeInEditMode} = cc._decorator;

@ccclass
@executeInEditMode
export default class ScaleNodeComp extends cc.Component {
    @property()
    maxWidth: number = 20;

    @property()
    maxScale: number = 1;

    update (dt) {
        this.updateScale();
    }

    updateScale(){
        if(this.node.width<=0) this.node.scale = this.maxScale;

        let scale = Math.min(this.maxScale, this.maxWidth * 1.0/ this.node.width);
        this.node.scale = scale;
    }
}
