import SicboHelper from "../../../Sicbo/Scripts/Helpers/SicboHelper";
import SicboItemComp from "./Items/SicboItemComp";

const { ccclass, property } = cc._decorator;
@ccclass
export default class SicboLatestResultComp extends cc.Component {
  @property(cc.Label)
  sumLabel: cc.Label = null;

  @property(cc.Label)
  typeLabel: cc.Label = null;

  @property([SicboItemComp])
  items: SicboItemComp[] = [];

  @property(cc.Node)
  fxNode: cc.Node = null;

  onLoad() {
    this.playFx(false);
  }

  set active(isActive: boolean) {
    this.items.forEach((item, index) => {
      item.node.scale = isActive ? 1 : 0;
    });
    this.typeLabel.node.parent.active = isActive;
  }

  setData(items: any[], isAnimated: boolean = true) {
    let self = this;
    items.forEach((item, index) => {
      self.items[index].setData(items[index], isAnimated);
    });

    self.sumLabel.string = SicboHelper.getResultTotalValue(items).toString();
    let typeResult = SicboHelper.getResultType(items);
    self.typeLabel.string = SicboHelper.getResultTypeName(typeResult);
  }

  onDestroy() {
    let self = this;
    self.items.forEach((item, index) => {
      if (self.items[index].node) cc.Tween.stopAllByTarget(self.items[index].node.parent);
    });
  }

  playFx(isPlay: boolean = true) {
    this.fxNode.active = isPlay;
  }
}
