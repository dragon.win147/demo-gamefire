// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboModuleAdapter from "../../../../SicboModuleAdapter";

const { Playah } = SicboModuleAdapter.getAllRefs();
import SicboUserItemComp from "./Items/SicboUserItemComp";
import SicboPortalAdapter from "../SicboPortalAdapter";
import SicboSetting from "../Setting/SicboSetting";
import SicboText from "../Setting/SicboText";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboUserComp extends cc.Component {
  //MY USER UI
  @property(SicboUserItemComp)
  myUserItem: SicboUserItemComp = null;

  @property(SicboUserItemComp)
  otherUserGroup: SicboUserItemComp = null;
  @property(cc.Label)
  remainingUsersLabel: cc.Label = null;

  @property(cc.Node)
  usersInRoomNode: cc.Node = null;
  usersInRoom: SicboUserItemComp[] = [];

  remainingPlayersCount: number = 0;
  private usersCachePos: cc.Vec2[] = [];

  private isUserInRoomAnimPlaying = false;
  private userCompAnimMap: Map<SicboUserItemComp, number> = new Map<SicboUserItemComp, number>(); //co data - co slot

  private cacheDisplayPlayers: InstanceType<typeof Playah>[];

  onLoad() {
    this.usersInRoom = this.usersInRoomNode.getComponentsInChildren(SicboUserItemComp);
    for (let i = 0; i < this.usersInRoom.length; i++) {
      this.usersCachePos.push(this.usersInRoom[i].node.getPosition());
      this.userCompAnimMap.set(this.usersInRoom[i], null);
    }
    this.setMyUserItemUI();
  }

  setMyUserItemUI() {
    let playah = SicboPortalAdapter.getInstance().getMyPlayAh();
    this.myUserItem.setData(playah);
  }

  // getMyUserItemComp(): SicboUserItemComp{
  //     return this.myUserItem;
  // }

  findUser(userId: string): SicboUserItemComp {
    let user: SicboUserItemComp = this.getUserInRoom(userId);

    if (user == null) {
      let myUserId = SicboPortalAdapter.getInstance().getMyUserId();
      if (myUserId == userId) user = this.myUserItem;
      else user = this.otherUserGroup;
    }
    return user;
  }

  getUserInRoom(userId: string): SicboUserItemComp {
    return this.usersInRoom.find((x) => x.data != null && x.data.userId == userId);
  }

  getMyUser() {
    return this.myUserItem;
  }

  getOtherUserGroup() {
    return this.otherUserGroup;
  }

  playBetAnimation(user: SicboUserItemComp, slotNode: cc.Node) {
    user.shakeBet(slotNode);
  }

  updateUsersInRoom(displayPlayers: InstanceType<typeof Playah>[], remainingPlayersCount: number) {
    this.remainingPlayersCount = Math.max(remainingPlayersCount, 0);
    this.otherUserGroup.node.active = remainingPlayersCount > 0;
    if (remainingPlayersCount > 999) this.remainingUsersLabel.string = SicboText.lotsOfUserText;
    else this.remainingUsersLabel.string = remainingPlayersCount.toString();

    if (this.isSameUserData(displayPlayers)) return;
    if (this.isUserInRoomAnimPlaying) return;

    this.cacheDisplayPlayers = displayPlayers;

    this.playUserInRoomAnimation(displayPlayers);
  }

  private playUserInRoomAnimation(displayPlayers: InstanceType<typeof Playah>[]) {
    let animDuration = SicboSetting.USER_ANIM_DURATION;
    let self = this;

    let animCount = 0;
    this.userCompAnimMap.forEach((targetIndex: number, userComp: SicboUserItemComp) => {
      this.userCompAnimMap.set(userComp, null);
    });

    //console.error("-------------", displayPlayers.length)
    // for (let i = 0; i < displayPlayers.length; i++) {console.error(displayPlayers[i].getUserId(), displayPlayers[i].getProfile().getDisplayName())}
    //user da co - co slot moi
    for (let i = 0; i < displayPlayers.length; i++) {
      let userId = displayPlayers[i].getUserId();
      let userComp = this.usersInRoom.find((x) => x.getUserId() == userId);
      if (userComp != null) {
        //console.error("anim", userId, displayPlayers[i].getProfile().getDisplayName(), i)
        self.userCompAnimMap.set(userComp, i);
        if (this.isUseLinearAnim(userComp.getChatIndex(), i)) {
          cc.tween(userComp.node).to(animDuration, { x: self.usersCachePos[i].x, y: self.usersCachePos[i].y }, { easing: "cubicInOut" }).start();
        } else {
          let startPos = new cc.Vec2(userComp.node.x, userComp.node.position.y);
          let endPos = new cc.Vec2(self.usersCachePos[i].x, self.usersCachePos[i].y);
          cc.tween(userComp.node).bezierTo(animDuration, startPos, new cc.Vec2(0, -170), endPos).start();
        }
        userComp.setData(displayPlayers[i], i);
        animCount++;
      }
    }

    let freeComps: SicboUserItemComp[] = [];
    this.userCompAnimMap.forEach((i: number, userComp: SicboUserItemComp) => {
      if (i == null) freeComps.push(userComp);
    });

    //NO PLAYAH COMP
    for (let i = 0; i < freeComps.length; i++) {
      freeComps[i].setData();
    }

    cc.tween(this.node)
      .delay(animDuration / 2)
      .call(() => {
        //NEW Playah
        for (let i = 0; i < displayPlayers.length; i++) {
          let userId = displayPlayers[i].getUserId();
          let userComp = self.usersInRoom.find((x) => x.getUserId() == userId);
          if (userComp == null) {
            //console.error("set", userId, displayPlayers[i].getProfile().getDisplayName(), i)

            userComp = freeComps.pop();
            if (userComp != null) {
              userComp.setData(displayPlayers[i], i);

              cc.tween(userComp.node).to(0, { x: self.usersCachePos[i].x, y: self.usersCachePos[i].y }).start();
            }
          }
        }
      })
      .start();

    //ANIM DELAY
    if (animCount > 0) {
      self.isUserInRoomAnimPlaying = true;
      cc.tween(this.node)
        .delay(animDuration)
        .call(() => {
          self.isUserInRoomAnimPlaying = false;
        })
        .start();
    } else {
      self.isUserInRoomAnimPlaying = false;
    }
  }

  isSameUserData(displayPlayers: InstanceType<typeof Playah>[]) {
    return false;

    // if(this.cacheDisplayPlayers == null || this.cacheDisplayPlayers.length == 0) return false;
    // if(this.cacheDisplayPlayers.length != displayPlayers.length) return false;

    // for (let i = 0; i < displayPlayers.length; i++) {
    //     if(this.cacheDisplayPlayers[i].getUserId() != displayPlayers[i].getUserId()) return false;
    // }
    // return true;
  }

  isUseLinearAnim(myIndex: number, otherIndex: number) {
    let isSameSide = Math.floor(myIndex / 3) == Math.floor(otherIndex / 3);
    let isSameRow = myIndex % 3 == otherIndex % 3;

    return isSameSide || isSameRow;
  }
}
