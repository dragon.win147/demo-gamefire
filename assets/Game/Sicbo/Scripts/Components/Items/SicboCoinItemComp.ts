// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboChipSpriteConfig from "../../Configs/SicboChipSpriteConfig";
import SicboHelper from "../../Helpers/SicboHelper";
import { SicboCoinType } from "../../RNGCommons/SicboCoinType";

const {ccclass, property} = cc._decorator;


export class SicboCoinItemCompData {
    public coinType: SicboCoinType = SicboCoinType.Coin100;    
    public onClick: (sicboCoinItemComp: SicboCoinItemComp) => void = null;
    public isMini: boolean = false;
}


@ccclass
export default class SicboCoinItemComp extends cc.Component {    
  @property(cc.Sprite)
  icon: cc.Sprite = null;

  @property(cc.Sprite)
  iconDisable: cc.Sprite = null;

  @property(cc.Node)
  showEffect: cc.Node = null;

  data: SicboCoinItemCompData = null;

//   set isOnTable(value: boolean) {    //
//     this.node.setScale(value? new cc.Vec2(.25, .22): cc.Vec2.ONE);
//   }

  onLoad(){
    // var interval = 5;
    // // Time of repetition
    // var repeat = 4;
    // // Start delay
    // var delay = 2;
    // let self = this;
    // let isDisable = false;
    // self.isDisable = isDisable;
    // this.schedule(function() {
    //     // Here `this` is referring to the component
    //     isDisable = !isDisable;
    //     self.isDisable = isDisable;        
    //     //cc.log("DKM", isDisable);
    // }, interval, repeat, delay);
  }

  set isDisable(disable: boolean) {    
    this.icon.node.active = !disable;
    this.iconDisable.node.active = disable;    
    if(disable) {
        this.showEffect.active = false;
        this.node.scale = 1;
    }
  }

  set rotation(value: number){
    this.icon.node.angle = -value;
  }

    setData(data: SicboCoinItemCompData) {
        this.data = data;
        let spriteName = SicboHelper.convertCoinTypeToSpriteName(data.coinType, false, this.data.isMini);
        let spriteNameDisable = SicboHelper.convertCoinTypeToSpriteName(data.coinType, true);
        let self = this;
        let spriteFrame : cc.SpriteFrame = null;
        if(self.data.isMini)
            spriteFrame = SicboChipSpriteConfig.Instance.getMiniChipSprite(spriteName);
        else
            spriteFrame = SicboChipSpriteConfig.Instance.getNormalChipSprite(spriteName);
        self.icon.spriteFrame = spriteFrame;


        if(this.data.isMini){
            let button = this.getComponent(cc.Button);
            button.enabled = false;
            this.iconDisable.node.active = false;
        }else{
            self.iconDisable.spriteFrame = SicboChipSpriteConfig.Instance.getDisableChipSprite(spriteNameDisable);
            this.iconDisable.node.active = true;
        }
    }


    public onClickItem() {     
        if(this.data.onClick){
            this.data.onClick(this);
        }
    }

    selectEff(isSelect:boolean){
        cc.tween(this.node).to(.05, 
            {
                // y: isSelect?25:0, 
                scale: isSelect?1.1:1
            })
            .start();
        this.showEffect.active = isSelect;
    }

    onDestroy(){
        cc.Tween.stopAllByTarget(this.node);
    }
}
