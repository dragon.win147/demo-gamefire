
const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboItemComp extends cc.Component {
  @property(cc.Sprite)
  iconSprite: cc.Sprite = null;  

  @property([cc.SpriteFrame])
  itemIconSprite: cc.SpriteFrame[] = [];

  setData(item, isAnimated: boolean = false){
    let self = this;
    
    cc.tween(self.node)
    .to(isAnimated ? .5 : 0, { scale: 0 })
    .call(
      () => {
        let index = item as number;
        self.iconSprite.spriteFrame = self.itemIconSprite[index - 1];
      }
    )
    .to(isAnimated ? 0.5 : 0, { scale: 1 })
    .start();
  }

  onDestroy(){
    cc.Tween.stopAllByTarget(this.node);
  }
}
