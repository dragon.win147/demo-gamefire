// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboModuleAdapter from "../../../../../SicboModuleAdapter";

const {
  Door} = SicboModuleAdapter.getAllRefs();
import SicboSetting from "../../Setting/SicboSetting";
import SicboHelper from "../../Helpers/SicboHelper";
import SicboMoneyFormatComp from "../../RNGCommons/SicboMoneyFormatComp";
import SicboBetComp from "../SicboBetComp";
import SicboCoinItemComp, { SicboCoinItemCompData } from "./SicboCoinItemComp";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboBetSlotItemComp extends cc.Component {

    @property({ type: cc.Enum(Door) })
    door = Door.ONE_SINGLE;

    @property(SicboBetComp)
    sicboBetComp: SicboBetComp = null;

    @property(cc.Node)
    myBetBgNode: cc.Node = null;

    @property(SicboMoneyFormatComp)
    myBetMoneyComp: SicboMoneyFormatComp = null;

    @property(cc.Node)
    totalBetBgNode: cc.Node = null;

    @property(SicboMoneyFormatComp)
    totalBetMoneyComp: SicboMoneyFormatComp = null;

    @property(cc.Node)
    miniChipArea: cc.Node = null;

    private stackChipRoot: cc.Node = null;
    maxMiniChipOnSlot = 12 + 10 + 5;
    chipColCount = 0;

    lastMyBetAmount = 0;
    lastTotalBetAmount = 0;

    private _miniChipsOnSlot: SicboCoinItemComp[] = [];

    public get miniChipsOnSlot(): SicboCoinItemComp[] {
        //FIXME kiem tra lai loi bi sai _miniChipsOnSlot
        this._miniChipsOnSlot = this.miniChipArea.getComponentsInChildren(SicboCoinItemComp);
        return this._miniChipsOnSlot;
    }

    // public set miniChipsOnSlot(value: SicboCoinItemComp[]) {
    //     this._miniChipsOnSlot = value;
    // }

    public addMiniChipToSlot(chip: SicboCoinItemComp) {
        this._miniChipsOnSlot.push(chip);
    }

    public getRedundancyChip(isStaticSlot: boolean, mergeFrom: number): SicboCoinItemComp[] {
        if (isStaticSlot) {
            let totalValue = 0;
            let chips = this.miniChipsOnSlot;
            for (let i = mergeFrom; i < chips.length; i++) {
                totalValue += SicboHelper.convertCoinTypeToValue(chips[i].getComponent(SicboCoinItemComp)?.data.coinType);
            }
            let newChips = SicboHelper.calculateNumOfChipToMakeValue(totalValue, SicboSetting.getChipLevelList());

            newChips.forEach((num: number, val: number) => {
                // //console.log("val: " + val + " -> " + num);
                let coinType = SicboHelper.convertValueToCoinType(val);
                for (let i = 0; i < num; i++) {
                    let itemData = new SicboCoinItemCompData();
                    itemData.coinType = coinType;
                    itemData.isMini = true;
                    chips[mergeFrom].getComponent(SicboCoinItemComp).setData(itemData);
                    mergeFrom++;
                }
            });
            return this._miniChipsOnSlot.splice(mergeFrom + newChips.values.length);
        } else {
            return this._miniChipsOnSlot.splice(0, this._miniChipsOnSlot.length - this.maxMiniChipOnSlot);
        }
    }

    public clearMiniChipOnSlot() {
        this._miniChipsOnSlot.length = 0;
    }

    public setStackChipRoot(stackChipRoot: cc.Node) {
        this.stackChipRoot = stackChipRoot;
    }

    public deleteStackChipRoot() {
        if (this.stackChipRoot != null)
            this.stackChipRoot.destroy();
        this.stackChipRoot = null;
    }

    public get miniChipsOnStack(): SicboCoinItemComp[] {
        if (this.stackChipRoot != null)
            return this.stackChipRoot.getComponentsInChildren(SicboCoinItemComp);
        return [];
    }


    onLoad() {
        this.registerClickEvent();
        this.resetSlot();
    }

    resetSlot() {
        this.setTotalBet(0);
        this.setMyTotalBet(0);
    }

    setMyTotalBet(value: number) {
        if (value > 0) {
            this.myBetBgNode.active = true;
            this.myBetBgNode.width = this.myBetMoneyComp.node.width + 20 * .8;//NOTE layout not work 

            this.myBetMoneyComp.node.active = true;
            this.myBetMoneyComp.setMoney(value);
            if (this.lastMyBetAmount != value) {
                this.playBetAmountEffect(this.myBetMoneyComp.node);
            }
        } else {
            this.myBetBgNode.active = false;
            this.myBetMoneyComp.node.active = false;
        }
        this.lastMyBetAmount = value;
    }

    setTotalBet(value: number) {
        if (value > 0) {
            this.totalBetMoneyComp.node.active = true;
            this.totalBetBgNode.active = true;
            this.totalBetMoneyComp.setMoney(value);
            if (this.lastTotalBetAmount != value) {
                this.playBetAmountEffect(this.totalBetMoneyComp.node);
            }
        } else {
            this.totalBetBgNode.active = false;
            this.totalBetMoneyComp.node.active = false;
        }
        this.lastTotalBetAmount = value;
    }

    playBetAmountEffect(node: cc.Node) {
        node.scale = 2;
        node.opacity = 0;
        cc.tween(node)
            .to(.2, {
                scale: 1,
                opacity: 255
            })
            .start();
    }

    private registerClickEvent() {
        let button = this.getComponent(cc.Button);
        let clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        clickEventHandler.component = "SicboBetSlotItemComp";// This is the code file name
        clickEventHandler.handler = "onClickBetSlotItem";
        button.clickEvents.push(clickEventHandler);
    }

    onClickBetSlotItem() {
        this.sicboBetComp.onClickBetSlotItem(this);
    }

    onDestroy() {
        if (this.stackChipRoot != null) cc.Tween.stopAllByTarget(this.stackChipRoot);
        if (this.myBetMoneyComp.node != null) cc.Tween.stopAllByTarget(this.myBetMoneyComp.node);
        if (this.totalBetMoneyComp.node != null) cc.Tween.stopAllByTarget(this.totalBetMoneyComp.node);
    }

}
