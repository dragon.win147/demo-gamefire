// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboModuleAdapter from "../../../../../SicboModuleAdapter";

const { Playah } = SicboModuleAdapter.getAllRefs();
import SicboMoneyFormatComp from "../../RNGCommons/SicboMoneyFormatComp";
import SicboPortalAdapter from "../../SicboPortalAdapter";
import SicboAvatarWinMoneyComp from "../SicboAvatarWinMoneyComp";
import SicboUIComp from "../SicboUIComp";
import SicboResourceManager from "../../Managers/SicboResourceManager";
import SicboUserItemEffect from "../SicboUserItemEffect";
import SicboSetting from "../../Setting/SicboSetting";

const { ccclass, property } = cc._decorator;

export class SicboUserItemCompData {
  public rawPlayah: InstanceType<typeof Playah>;
  public userId: string;
  public avatarId: string;
  public framePath: string;
  public nickName: string;
  public balance: number = 0;
  public chatIndex: number = 0;
}

@ccclass
export default class SicboUserItemComp extends cc.Component {
  @property(cc.Sprite)
  iconSprite: cc.Sprite = null;

  @property(cc.Sprite)
  frame: cc.Sprite = null;

  @property(cc.Label)
  nickNameLabel: cc.Label = null;

  @property(SicboMoneyFormatComp)
  userMoneyComp: SicboMoneyFormatComp = null;

  @property(cc.Node)
  betShakeNode: cc.Node = null;

  @property(SicboAvatarWinMoneyComp)
  winMoneyEffect: SicboAvatarWinMoneyComp = null;

  cachedPos: cc.Vec2;

  public data: SicboUserItemCompData = null;

  onLoad() {
    this.cachedPos = this.betShakeNode?.getPosition();
  }

  updateUI() {
    if (this.data.userId != "") {
      this.node.active = true;
      this.setIconUI(this.data.avatarId);
      this.setFrame(this.data.framePath);
      this.setNickNameUI(this.data.nickName);
      this.setMoneyUI(this.data.balance);
    }
  }

  setData(playah: InstanceType<typeof Playah> = null, chatIndex: number = -1) {
    if (this.data == null) this.data = new SicboUserItemCompData();
    let userId = "";
    let basePath = "";
    let framePath = "";
    let nickName = "";
    let balance = 0;

    if (playah) {
      userId = playah.getUserId();
      basePath = SicboPortalAdapter.getInstance().onGetBasePath(playah.getAvatar()?.getBaseId());
      framePath = SicboPortalAdapter.getInstance().onGetFramePath(playah.getAvatar()?.getFrameId());
      nickName = playah.getProfile()?.getDisplayName();
      balance = playah.getWallet() ? playah.getWallet()?.getCurrentBalance() : 0;
    }

    this.data.rawPlayah = playah;
    this.data.userId = userId;
    this.data.avatarId = basePath;
    this.data.framePath = framePath;
    this.data.nickName = nickName;
    this.data.balance = balance;
    this.data.chatIndex = chatIndex;

    this.updateUI();

    let animDuration = SicboSetting.USER_ANIM_DURATION / 2;
    if (playah != null) this.node.getComponent(SicboUserItemEffect)?.fadeIn(animDuration);
    else this.node.getComponent(SicboUserItemEffect)?.fadeOut(animDuration);
  }

  getUserId() {
    return this.data?.userId;
  }

  getChatIndex() {
    return this.data?.chatIndex;
  }

  setMoney(money: number) {
    this.data.balance = money;
    this.setMoneyUI(money, true);
  }

  private setIconUI(avatarId: string) {
    // SicboResourceManager.loadRemoteImageWithPath(this.iconSprite, avatarId);
  }

  private setFrame(framePath: string) {
    // if (this.frame) SicboResourceManager.loadRemoteImageWithPath(this.frame, framePath);
  }

  private setNickNameUI(nickName: string) {
    this.nickNameLabel.string = nickName;
  }

  private setMoneyUI(money: number, isEffect: boolean = false) {
    money = Math.max(money, 0);

    if (isEffect) this.userMoneyComp.runMoneyTo(money);
    else this.userMoneyComp.setMoney(money);
  }

  shakeBet(target: cc.Node, distance: number = 15) {
    this.betShakeNode.setPosition(this.cachedPos);

    let tweenNode = this.betShakeNode;
    let point: cc.Vec2 = cc.Vec2.ZERO;

    let targetPoint = target.convertToWorldSpaceAR(cc.Vec2.ZERO);
    let tweenNodePoint = tweenNode.convertToWorldSpaceAR(cc.Vec2.ZERO);

    cc.Vec2.subtract(point, targetPoint, tweenNodePoint);
    point = point.normalize().mul(distance);

    let eff = cc.tween(tweenNode).to(0.1, { x: point.x + this.cachedPos.x, y: point.y + this.cachedPos.y }); //, { easing: 'elasticInOut'}
    let effB = cc
      .tween(tweenNode)
      .to(0.1, { x: this.cachedPos.x, y: this.cachedPos.y })
      .call(() => {
        this.betShakeNode.setPosition(this.cachedPos);
      });
    eff.then(effB).start();
  }

  showWinMoney(amount: number) {
    this.winMoneyEffect?.show(amount);
  }

  onDestroy() {
    cc.Tween.stopAllByTarget(this.betShakeNode);
  }

  onClickAvatar() {
    SicboUIComp.Instance.showUserProfilePopUp(this.data.rawPlayah);
  }
}
