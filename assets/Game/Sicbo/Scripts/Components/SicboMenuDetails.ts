// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboModuleAdapter from "../../../../SicboModuleAdapter";
const { State, Status } = SicboModuleAdapter.getAllRefs();
import SicboMessageHandler from "../SicboMessageHandler";
import SicboController from "../Controllers/SicboController";
import SicboUIManager from "../Managers/SicboUIManager";
import SicboInforPopup from "../PopUps/SicboInforPopup";
import SicboSoundManager from "../Managers/SicboSoundManager";
import ISicboState from "./States/ISicboState";
import SicboUIComp from "./SicboUIComp";
import SicboText from "../Setting/SicboText";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboMenuDetails extends cc.Component implements ISicboState {
  //MENU DETAIL
  isMenuDetailShowed: boolean = false;
  lockMenuDetail: boolean = false;

  @property(cc.Node)
  menuDetailRoot: cc.Node = null;
  @property(cc.Node)
  menuDetail: cc.Node = null;

  @property(cc.Node)
  bgSoundInactiveNode: cc.Node = null;

  @property(cc.Node)
  effectSoundInactiveNode: cc.Node = null;

  @property(cc.Node)
  outsideClickNode: cc.Node = null;

  @property(cc.Node)
  leaveBookingActiveNode: cc.Node = null;

  @property(cc.Node)
  leaveBookingDeactiveNode: cc.Node = null;

  private totalBet: number = 0;
  private curSessionId: number = 0;

  private isBookingLeave: boolean = false;
  private bookingLeaveSessionId: number = 0;

  start() {
    this.loadMenuBackgroundSoundStatus();
    this.loadMenuSoundFxStatus();
  }

  onLeave() {
    this.onClickShowMenuDetail();
    SicboController.Instance.sendCheckServerAvailable();
    if (this.isBookingLeave) {
      this.setLeaveBookingStatus(false, this.curSessionId);
      return;
    }

    if (this.totalBet > 0) {
      this.setLeaveBookingStatus(true, this.curSessionId);
      return;
    }

    this.leave();
  }
  private leave() {
    SicboMessageHandler.getInstance().sendLeaveRequest(() => {
      SicboController.Instance?.leaveGame();
    });
  }

  setLeaveBookingStatus(isActive: boolean, leaveSession: number) {
    this.bookingLeaveSessionId = leaveSession;

    this.isBookingLeave = isActive;
    this.leaveBookingActiveNode.active = isActive;
    this.leaveBookingDeactiveNode.active = !isActive;

    let msg = isActive ? SicboText.leaveBookingActiveMsg : SicboText.leaveBookingDeactiveMsg;
    SicboUIComp.Instance.showNotifyMessage(msg);
  }

  exitState(status: any): void {
    // if(this.isBookingLeave && this.totalBet> 0)
    //   this.leave();
  }
  startState(state: InstanceType<typeof State>, totalDuration: number): void {
    this.curSessionId = state.getTableSessionId();
    if (!this.isBookingLeave) return;

    if (this.curSessionId != this.bookingLeaveSessionId) {
      this.leave();
      return;
    }

    if (state.getStatus() == Status.WAITING) {
      this.leave();
    }
  }

  updateState(state: InstanceType<typeof State>): void {
    this.totalBet = state.getSessionPlayerBettedAmount();
  }

  onHide(): void {}

  onClickButtonRule() {
    SicboUIManager.getInstance().openPopup(SicboInforPopup, SicboUIManager.SicboInforPopup);
    this.onClickShowMenuDetail();
  }

  onClickShowMenuDetail() {
    if (this.lockMenuDetail) return;

    this.lockMenuDetail = true;

    this.isMenuDetailShowed = !this.isMenuDetailShowed;

    if (this.isMenuDetailShowed == true) {
      //tween out
      this.menuDetailRoot.active = this.isMenuDetailShowed;
      this.outsideClickNode.active = this.isMenuDetailShowed;
      cc.tween(this.menuDetail)
        .to(0.3, { position: new cc.Vec3(0, 0, 0) })
        .call(() => {
          this.lockMenuDetail = false;
        })
        .start();
    } else {
      //tween in
      cc.tween(this.menuDetail)
        .to(0.1, { position: new cc.Vec3(10, 0, 0) })
        .to(0.5, { position: new cc.Vec3(-720, 0, 0) })
        .call(() => {
          this.menuDetailRoot.active = this.isMenuDetailShowed;
          this.outsideClickNode.active = this.isMenuDetailShowed;
          this.lockMenuDetail = false;
        })
        .start();
    }
  }

  loadMenuBackgroundSoundStatus() {
    let vol = SicboSoundManager.getInstance().getMusicVolumeFromStorage();
    this.bgSoundInactiveNode.active = vol == 0;
  }

  loadMenuSoundFxStatus() {
    let vol = SicboSoundManager.getInstance().getSfxVolumeFromStorage();
    this.effectSoundInactiveNode.active = vol == 0;
  }

  onClickMenuBackgroundSound() {
    let vol = SicboSoundManager.getInstance().getMusicVolumeFromStorage();
    if (vol > 0) {
      vol = 0;
    } else {
      vol = 1;
    }
    SicboSoundManager.getInstance().setMusicVolume(vol);
    this.bgSoundInactiveNode.active = vol == 0;
  }

  onClickMenuSoundFx() {
    let vol = SicboSoundManager.getInstance().getSfxVolumeFromStorage();
    if (vol > 0) {
      vol = 0;
    } else {
      vol = 1;
    }
    SicboSoundManager.getInstance().setSfxVolume(vol);
    this.effectSoundInactiveNode.active = vol == 0;
  }
}
