import SicboModuleAdapter from "../../../../SicboModuleAdapter";

const { EventKey, AutoJoinReply } = SicboModuleAdapter.getAllRefs();

import SicboMessageHandler from "../SicboMessageHandler";
import SicboResourceManager from "../Managers/SicboResourceManager";
import SicboSetting from "../Setting/SicboSetting";
import SicboLoadingBackgroundAnimation from "./SicboLoadingBackgroundAnimation";
import SicboGameUtils from "../RNGCommons/Utils/SicboGameUtils";
import SicboText from "../Setting/SicboText";

const { ccclass, property } = cc._decorator;
@ccclass
export default class SicboLoadingScene extends cc.Component {
  @property(SicboLoadingBackgroundAnimation)
  loadingAnimCmp: SicboLoadingBackgroundAnimation = null;

  @property(cc.ProgressBar)
  sprProgress: cc.ProgressBar = null;

  @property(cc.Label)
  loadingLabel: cc.Label = null;

  private _percent: number;
  private _nameScene: string = "SicboScene";
  private _callBack: () => void = null;
  onLoad() {
    cc.systemEvent.on(
      EventKey.ON_DISCONNECT_EVENT,
      () => {
        //cc.log("Loading Scene ON_RECONNECTED");
        if (this._nameScene != "") {
          this.setDataLoadScene(this._nameScene, this._callBack);
        }
      },
      this
    );
  }

  start() {
    this.setDataLoadScene(this._nameScene, this._callBack);
  }

  onDestroy() {
    this._nameScene = "";
    this._callBack = null;
  }

  private setLoadingText(percent: number) {
    if (this.loadingLabel == null) {
      return;
    }
    percent = Math.ceil(Math.min(Math.max(percent * 100, 0), 100));
    if (percent >= this._percent) {
      this._percent = percent;
      this.loadingLabel.string = SicboGameUtils.FormatString(SicboText.loadingMsg, percent);
    }
  }

  setDataLoadScene(nameScene: string, callBack: () => void = null) {
    const self = this;
    this._callBack = callBack;

    self.onLoadProgress(SicboSetting.loadingBackgroundOverrideProgressBar);
    self._percent = 0;

    if (this.loadingAnimCmp == null) onSetData(null);
    else {
      this.loadingAnimCmp.OnBeginLoadResources = () => {
        onSetData(this.loadingAnimCmp);
      };
      self.onSetProgress(0, SicboSetting.loadingBackgroundOverrideProgressBar, this.loadingAnimCmp);
    }

    var onSetData = (backgroundAnimation: SicboLoadingBackgroundAnimation) => {
      let resourceLocalPaths = SicboSetting.resourceLocalPaths;
      let resourceRemotePaths = SicboSetting.resourceRemotePaths;

      let totalResourceLocal = resourceLocalPaths.length;
      let completedResourceLocal: number = 0;

      let totalResourceRemote = resourceRemotePaths.length;
      let completedResourceRemote: number = 0;

      let totalSceneResource: number = 100;
      let completedScene: number = 0;

      let totalData = 100;
      let completedLoadData: number = 0;

      var onPreLoadComplete = () => {
        let finish = () => {
          SicboMessageHandler.getInstance().sendAutoJoinRequest((autoJoinReply: InstanceType<typeof AutoJoinReply>) => {
            let channel = autoJoinReply.getChannel();
            SicboSetting.setChipAndStatusDurationConfig(channel.getChipLevels(), channel.getStatusAndDuration());
            SicboResourceManager.loadScene(nameScene, function () {
              if (callBack) {
                callBack();
              }
            });
          });
        };

        if (backgroundAnimation == null) finish();
        else {
          backgroundAnimation.playFinishAnim(finish);
        }
      };
      var onProgress = (completedCount, totalCount) => {
        var percent = 0;
        if (totalCount > 0) {
          percent = (100 * completedCount) / totalCount;
          var progress = percent / 100;
          self.onSetProgress(progress, SicboSetting.loadingBackgroundOverrideProgressBar, this.loadingAnimCmp);
        }
        if (percent == 100) {
          if (onPreLoadComplete) onPreLoadComplete();
        }
      };
      var onProgressLoadData = (completedCount, totalCount) => {
        //cc.log("onProgressLoadData.. ", completedCount, totalCount);
        totalData = totalCount;
        completedLoadData = completedCount;
        onProgress(
          completedScene + completedResourceLocal + completedResourceRemote + completedLoadData,
          totalData + totalSceneResource + totalResourceLocal + totalResourceRemote
        );
      };

      var onProgressResourceLocal = (completedCount, totalCount) => {
        totalResourceLocal = totalCount;
        completedResourceLocal = completedCount;
        onProgress(
          completedScene + completedResourceLocal + completedResourceRemote + completedLoadData,
          totalData + totalSceneResource + totalResourceLocal + totalResourceRemote
        );
      };

      var onProgressResourceRemote = (completedCount, totalCount) => {
        totalResourceRemote = totalCount;
        completedResourceRemote = completedCount;
        onProgress(
          completedScene + completedResourceLocal + completedResourceRemote + completedLoadData,
          totalData + totalSceneResource + totalResourceLocal + totalResourceRemote
        );
      };

      var onProgressScene = (completedCount, totalCount) => {
        totalSceneResource = totalCount;
        completedScene = completedCount;
        onProgress(
          completedScene + completedResourceLocal + completedResourceRemote + completedLoadData,
          totalData + totalSceneResource + totalResourceLocal + totalResourceRemote
        );
      };

      this.scheduleOnce(function () {
        let onLoadDataComplete = () => {
          SicboResourceManager.preloadScene(nameScene, onProgressScene, () => {});
          SicboResourceManager.preloadResourceLocal(resourceLocalPaths, onProgressResourceLocal, function () {});
          SicboResourceManager.preloadResourceRemote(resourceRemotePaths, onProgressResourceRemote, function () {});
        };
        if (SicboSetting) {
          SicboSetting.loadDataFromForge(onProgressLoadData, onLoadDataComplete);
        } else {
          onProgressLoadData(0, 0);
          onLoadDataComplete();
        }
      }, 0.3);
    };
  }

  onLoadProgress(override: boolean = false) {
    if (override) {
      this.sprProgress.node.active = false;
    }
  }

  onSetProgress(progress: number, override: boolean = false, loadingAnimCmp: SicboLoadingBackgroundAnimation) {
    this.setLoadingText(progress);
    if (override && loadingAnimCmp) {
      loadingAnimCmp.onSetProgress(progress);
    } else {
      if (this.sprProgress && progress > this.sprProgress.progress) {
        this.sprProgress.progress = progress;
      }
    }
  }
}
