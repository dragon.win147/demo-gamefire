import SicboModuleAdapter from "../../../../SicboModuleAdapter";

const { Playah, State, Status, ListCurrentJackpotsReply } = SicboModuleAdapter.getAllRefs();

import SicboSetting, { SicboSound } from "../Setting/SicboSetting";
import SicboUserItemComp from "./Items/SicboUserItemComp";
import SicboUserComp from "./SicboUserComp";
import SicboBetComp from "./SicboBetComp";

import SicboController from "../Controllers/SicboController";

import SicboNotifyComp from "./SicboNotifyComp";
import SicboHelper from "../Helpers/SicboHelper";
import SicboSessionInfoComp from "./SicboSessionInfoComp";
import SicboClockStateComp from "./States/SicboClockStateComp";
import SicboDealerStateComp from "./States/SicboDealerStateComp";
import SicboResultStateComp from "./States/SicboResultStateComp";
import SicboJackpotStateComp from "./States/SicboJackpotStateComp";
import SicboNotifyStateComp from "./States/SicboNotifyStateComp";
import SicboUIManager from "../Managers/SicboUIManager";
import SicboPortalAdapter from "../SicboPortalAdapter";
import SicboResourceManager from "../Managers/SicboResourceManager";
import SicboGameUtils from "../RNGCommons/Utils/SicboGameUtils";
import { SicboCoinType } from "../RNGCommons/SicboCoinType";
import SicboText from "../Setting/SicboText";
import SicboMenuDetails from "./SicboMenuDetails";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboUIComp extends cc.Component {
  private static instance: SicboUIComp = null;
  static get Instance(): SicboUIComp {
    return SicboUIComp.instance;
  }

  //GameBetUIComp {
  //OPEN SCENE FADE OUT EFF
  @property(cc.Sprite)
  private effectShow: cc.Sprite = null;

  //USER COMP
  @property(SicboUserComp)
  private userComp: SicboUserComp = null;

  @property(cc.Button)
  private chatButton: cc.Button = null;

  @property(cc.Node)
  private shakeBetTarget: cc.Node = null;

  @property(cc.SpriteFrame)
  private userProfileBackground: cc.SpriteFrame = null;
  //BET COMP
  @property(cc.Node)
  private betCompNode: cc.Node = null;
  private betComp: SicboBetComp = null;

  @property(SicboSessionInfoComp)
  private sessionComp: SicboSessionInfoComp = null;

  @property(SicboMenuDetails)
  private menuDetails: SicboMenuDetails = null;

  //ANCHOR State UI
  @property(SicboClockStateComp)
  private clock: SicboClockStateComp = null;

  @property(SicboDealerStateComp)
  private dealer: SicboDealerStateComp = null;

  @property(SicboResultStateComp)
  private result: SicboResultStateComp = null;

  @property(SicboJackpotStateComp)
  private jackpot: SicboJackpotStateComp = null;

  @property(SicboNotifyStateComp)
  private message: SicboNotifyStateComp = null;

  @property(SicboNotifyComp)
  notifyComp: SicboNotifyComp = null;

  @property(cc.Node)
  private chatRoot: cc.Node = null;

  //!SECTION
  private stateStatus = Status.STATUS_UNSPECIFIED;

  onLoad() {
    SicboUIComp.instance = this;
    this.betComp = this.betCompNode.getComponent(SicboBetComp);
    this.onLoadJackpot();
    this.onSubscribeJackpot();
  }

  onDestroy() {
    this.onUnSubscribeJackpot();
    delete SicboUIComp.instance;
    SicboUIComp.instance = null;
  }

  update(dt) {
    // super.update(dt);
  }

  start() {
    this.effectShow.node.active = true;
    this.createChipItems(SicboSetting.getChipLevelList());
    this.showEffectLoadingGame();
    this.preloadInfoSpites();
  }

  preloadInfoSpites() {
    // let infoResources = SicboController.Instance.getGameInfo();
    // infoResources.forEach((info) => {
    //   SicboResourceManager.loadRemoteImageWithFileField(null, info);
    // });
  }

  //SECTION BET
  myUserBet(door, coinType: SicboCoinType) {
    if (this.stateStatus != Status.BETTING) return;

    let coinNumber = SicboHelper.convertCoinTypeToValue(coinType);
    SicboController.Instance.sendBet(door, coinType, () => {
      this.playUserBet(SicboPortalAdapter.getInstance().getMyUserId(), door, coinNumber);
      SicboController.Instance.playSfx(SicboSound.SfxBet);
    });
  }

  onClickReBet() {
    SicboController.Instance.sendReBet((dabs) => {
      let myUserId = SicboPortalAdapter.getInstance().getMyUserId();
      dabs.forEach((dab) => {
        this.playUserBet(myUserId, dab.getDoor(), dab.getBetAmount());
      });
      SicboController.Instance.playSfx(SicboSound.SfxBet);
    });
  }

  onClickDoubleBet() {
    SicboController.Instance.sendBetDouble((dabs) => {
      let myUserId = SicboPortalAdapter.getInstance().getMyUserId();
      dabs.forEach((dab) => {
        this.playUserBet(myUserId, dab.getDoor(), dab.getBetAmount());
      });
      SicboController.Instance.playSfx(SicboSound.SfxBet);
    });
  }
  //!SECTION

  setChatEnable(isEnable: boolean) {
    this.chatButton.node.active = isEnable;
  }

  //ANCHOR effect loading game
  showEffectLoadingGame() {
    cc.tween(this.effectShow.node)
      .delay(0.3)
      .to(1, {
        opacity: 0,
      })
      .call(() => (this.effectShow.node.active = false))
      .start();
  }

  //ANCHOR SicboBetComp
  createChipItems(chip_level: number[]) {
    this.betComp.createChipItems(chip_level);
  }

  //!SECTION

  //ANCHOR SicboUserComp
  findUserComp(userId: string): SicboUserItemComp {
    let user = this.userComp.getUserInRoom(userId);
    if (SicboGameUtils.isNullOrUndefined(user)) {
      let myUserId = SicboPortalAdapter.getInstance().getMyUserId();
      if (userId == myUserId) user = this.userComp.getMyUser();
      else user = this.userComp.getOtherUserGroup();
    }
    return user;
  }

  myUserComp() {
    return this.userComp.getMyUser();
  }

  updateUsersInRoom(displayPlayers: InstanceType<typeof Playah>[], remainingPlayersCount: number) {
    this.userComp.updateUsersInRoom(displayPlayers, remainingPlayersCount);
  }

  playUserBet(userId: string, door, chip: number) {
    let userUI = this.userComp.findUser(userId);

    let doorComp = this.betComp.getDoor(door);
    this.userComp.playBetAnimation(userUI, this.shakeBetTarget);
    this.betComp.throwMiniChipOnTableByValue(chip, userUI.betShakeNode, doorComp);
  }

  changeMyUserBalance(newBalance: number) {
    this.userComp.getMyUser().setMoney(newBalance);
  }

  //ANCHOR State UI
  exitState(status) {
    this.betComp.exitState(status);
    this.clock.exitState(status);
    this.dealer.exitState(status);
    this.result.exitState(status);
    this.jackpot.exitState(status);
    this.message.exitState(status);
    this.sessionComp.exitState(status);
    this.menuDetails.exitState(status);
  }

  startState(state: InstanceType<typeof State>, stateDuration: number) {
    //cc.log("SICBO STATE", state.getStatus(), stateDuration);
    this.betComp.startState(state, stateDuration);
    this.clock.startState(state, stateDuration);
    this.dealer.startState(state, stateDuration);
    this.result.startState(state, stateDuration);
    this.jackpot.startState(state, stateDuration);
    this.message.startState(state, stateDuration);
    this.sessionComp.startState(state, stateDuration);
    this.menuDetails.startState(state, stateDuration);

    this.stateStatus = state.getStatus();
  }

  updateState(state: InstanceType<typeof State>) {
    this.betComp.updateState(state);
    this.clock.updateState(state);
    this.dealer.updateState(state);
    this.result.updateState(state);
    this.jackpot.updateState(state);
    this.message.updateState(state);
    this.sessionComp.updateState(state);
    this.menuDetails.updateState(state);
  }

  onHide() {
    this.betComp.onHide();
    this.clock.onHide();
    this.dealer.onHide();
    this.result.onHide();
    this.jackpot.onHide();
    this.message.onHide();
    this.sessionComp.onHide();
    this.menuDetails.onHide();
  }
  protected onLoadJackpot() {
    // SicboPortalAdapter.getPotterHandler().getListCurrentJackpotsRequest((res: InstanceType<typeof ListCurrentJackpotsReply>) => {
    //   this.jackpot.handleResponseJackpot(res);
    // });
  }
  protected onSubscribeJackpot() {
    // SicboPortalAdapter.getPotterHandler().onSubscribeJackpot("SicboUIComp", (res: InstanceType<typeof ListCurrentJackpotsReply>) => {
    //   this.jackpot.handleResponseJackpot(res);
    // });
  }

  protected onUnSubscribeJackpot() {
    // SicboPortalAdapter.getPotterHandler().onUnSubscribeJackpot("SicboUIComp");
  }

  //ANCHOR show Error message
  showNotifyMessage(message: string) {
    this.notifyComp.showSmallNoti(message, 0.1, 2, 0.1);
  }

  showErrMessage(errCode, err: string) {
    // if (errCode == Code.SICBO_BET_FAILED ||
    //   errCode == Code.SICBO_CANNOT_REBET ||
    //   errCode == Code.SICBO_CANNOT_BET_DOUBLE) return;

    let message = SicboText.getErrorMessage(errCode);
    if (message == null) {
      message = errCode + " - " + err;
      //cc.log("SICBO ERROR", message);
    } else {
      this.showNotifyMessage(message);
    }
  }

  //!SECTION

  //SECTION user profile
  showUserProfilePopUp(playah: InstanceType<typeof Playah>) {
    if (playah == null) return;
    SicboPortalAdapter.getInstance().showUserProfilePopUp(playah, this.userProfileBackground);
  }
  //!SECTION
}
