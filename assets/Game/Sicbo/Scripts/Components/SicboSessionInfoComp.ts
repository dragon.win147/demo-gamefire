// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import SicboModuleAdapter from "../../../../SicboModuleAdapter";

const {
  State,
  Status} = SicboModuleAdapter.getAllRefs();

import SicboSetting from "../Setting/SicboSetting";
import SicboMoneyFormatComp from "../RNGCommons/SicboMoneyFormatComp";
import SicboGameUtils from "../RNGCommons/Utils/SicboGameUtils";
import ISicboState from "./States/ISicboState";
import SicboText from '../Setting/SicboText';

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboSessionInfoComp extends cc.Component implements ISicboState{

  @property(cc.Label)
  sessionIdLabel: cc.Label = null;

  @property(SicboMoneyFormatComp)
  totalBetLabel: SicboMoneyFormatComp = null;

  @property(cc.Label)
  diceLabel: cc.Label = null;

  @property(cc.Node)
  md5Desc: cc.Node = null;

  @property(cc.Node)
  keyDesc: cc.Node = null;

  @property(cc.Label)
  md5Label: cc.Label = null;

  @property(cc.Node)
  copyNotiNode: cc.Node = null;

  @property(cc.Node)
  md5Mask: cc.Node = null;

  @property(cc.Node)
  md5ParentNode: cc.Node = null;

  @property(cc.Layout)
  layout: cc.Layout = null;

  @property(cc.Animation)
  shinnyAnimation: cc.Animation = null;

  @property(cc.Color)
  md5Color: cc.Color = cc.Color.WHITE;

  @property(cc.Color)
  keyColor: cc.Color = cc.Color.RED;

  md5Cache: string = null;
  // private MD5_LENGTH = 32;
  private MD5_MASK_WIDTH = 310;
  private KEY_WIDTH = 275;
  private MD5_WIDTH = 295;
  // private MD5_MASK_LEFT_POS = -26.5;
  // private MD5_MASK_RIGHT_POS = this.MD5_MASK_LEFT_POS + this.MD5_MASK_WIDTH;
  private md5NewStartTime = .7;
  private keyNewStartTime = .6;

  MD5_STR = "MD5 :"
  KEY_STR = "KEY :"

  onLoad(){
    this.shinnyAnimation.node.active = false;
    this.MD5_MASK_WIDTH = this.md5Mask.width;
  }

  setMaskAnchor(isLeft: boolean = true){
    // this.md5Mask.y = 0;
    if(isLeft){
      this.md5Mask.anchorX = 0;
      // this.md5Mask.x = this.MD5_MASK_LEFT_POS;
      this.md5ParentNode.setPosition(cc.Vec2.ZERO);
    }else{
      this.md5Mask.anchorX = 1;
      // this.md5Mask.x = this.MD5_MASK_RIGHT_POS;
      this.md5ParentNode.setPosition(cc.Vec2.ZERO);
      this.md5ParentNode.x = -this.MD5_MASK_WIDTH;
    }
  }


  exitState(status): void {
  }

  startState(state: InstanceType<typeof State>, totalDuration: number): void {
    this.setSessionId(state.getTableSessionId().toString());
    let status = state.getStatus();

    switch (status) {
      // case Status.WAITING:
      //   this.setNewMd5(state.getPrevMd5Result(), state.getMd5Result(), elapseTime);
      //   break;

      case Status.BETTING: case Status.END_BET:
        this.md5Cache = state.getMd5Result();
        this.setMd5Style();
        this.updateMd5Label(this.md5Cache);
        break;

      // case Status.RESULTING:
      //   this.setNewKey(state.getMd5Result(), state.getKeyResult(), elapseTime);
      //   break;

      case Status.PAYING: case Status.FINISHING:
        this.md5Cache = state.getKeyResult();
        this.updateMd5Label(this.md5Cache);
        this.setKeyStyle();
        break; 
    }
  }

  updateState(state: InstanceType<typeof State>): void {
    this.setTotalBet(state.getSessionPlayerBettedAmount());

    let status = state.getStatus();
    let elapseTime = state.getStageTime()/1000;

    switch (status) {
      case Status.WAITING:
        this.setNewMd5(state.getPrevKeyResult(), state.getMd5Result(), elapseTime);
        break;

      case Status.RESULTING:
        this.setNewKey(state.getMd5Result(), state.getKeyResult(), elapseTime);
        break;
    }
  }

  onHide(): void {
    this.md5Label.node.width = 0;
    // cc.Tween.stopAllByTarget(this.copyNotiNode);
    // cc.Tween.stopAllByTarget(this.md5Mask);
    // cc.Tween.stopAllByTarget(this.layout);    
    cc.Tween.stopAllByTarget(this.node);    
  }

  setSessionId(sessionId: string) {
    this.sessionIdLabel.string = SicboGameUtils.FormatString(SicboText.sessionIdText, sessionId);
  }
  setTotalBet(totalBet: number) {
    this.totalBetLabel.setMoney(totalBet);
  }


  setNewMd5(md5PrevRaw: string, md5NewRaw: string, elapseTime: number){
    if(elapseTime>this.md5NewStartTime){   
      this.md5Cache = md5NewRaw;   
      this.updateMd5Label(this.md5Cache);
      this.setMd5Style();
    }else if(elapseTime < this.md5NewStartTime){
      this.md5Cache = md5PrevRaw;
      this.setKeyStyle();
      this.updateMd5Label(this.md5Cache);
    }else{
      let self = this;
      cc.tween(self.node)
        // .delay(self.md5NewStartTime - elapseTime)
        .call(
          ()=>{
            self.md5Cache = md5NewRaw;             
            self.setMd5Style();
            self.setMaskAnchor(true);
            self.playLeftToRight();
            self.updateMd5Label(self.md5Cache);
          }
        ).start();   
    } 
  }

  setNewKey(md5NewRaw: string, keyNewRaw: string, elapseTime: number){
    if(elapseTime>this.keyNewStartTime){   
      this.md5Cache = keyNewRaw;   
      this.updateMd5Label(this.md5Cache);
      this.setKeyStyle();
    }else if(elapseTime < this.keyNewStartTime){
      this.md5Cache = md5NewRaw;      
      this.updateMd5Label(this.md5Cache);
      this.setMd5Style();
    }else{
      let self = this;
      cc.tween(self.node)
        // .delay(self.keyNewStartTime - elapseTime)
        .call(
          ()=>{
            self.md5Cache = keyNewRaw;   
            self.updateMd5Label(self.md5Cache);
            self.setKeyStyle();
            self.setMaskAnchor(false);
            self.playRightToLeft();
          }
        ).start();   
    } 
  }


  private setKeyStyle(){
    this.md5Desc.active = false;
    this.keyDesc.active = true;
    this.md5Label.node.color = this.keyColor;
    this.md5Label.node.width = this.KEY_WIDTH;
    this.diceLabel.node.active = true;
  }

  private setMd5Style(){
    this.md5Desc.active = true;
    this.keyDesc.active = false;
    this.md5Label.node.color = this.md5Color;
    this.md5Label.node.width = this.MD5_WIDTH;
    this.diceLabel.node.active = false;

  }

  private updateMd5Label(rawStr: string){
    let [diceStr, md5Str] = this.parseMd5(rawStr);   
    this.md5Label.string = md5Str;
    this.diceLabel.string = diceStr;
  }

  private playRightToLeft(){
    // this.setMaskAnchor(false);
    this.playMd5Anim(true);
    this.shinnyAnimation.node.active = true;
    this.shinnyAnimation.play("rightToLeft");
  }

  private playLeftToRight(){
    // this.setMaskAnchor(true);
    this.playMd5Anim();
    this.shinnyAnimation.node.active = true;
    this.shinnyAnimation.play("leftToRight");
  }


  playMd5Anim(animLayout: boolean = false, animTime: number = 1.15){
    this.md5Mask.width = 5;
    cc.tween(this.md5Mask)
      .to(animTime, {width: this.MD5_MASK_WIDTH})
      .start();


    if(!animLayout) {
      this.layout.spacingX = 0;
      return;
    }
    this.layout.spacingX = this.MD5_MASK_WIDTH - 5;
    cc.tween(this.layout)
    .to(animTime, {spacingX: 0})
    .start();
  }

  /**
   * 
   * @param md5Raw 
   * @returns [diceStr, md5Str]
   */
  parseMd5(md5Raw: string){
    // md5Raw = md5Raw.replace(":", "");
    let md5Str = "";
    let diceStr = "";

    let temp = md5Raw.indexOf("]");
    if(temp < 0){
      md5Str = md5Raw;
      diceStr = "\n";
    }else{
      md5Str = md5Raw.substr(temp + 1);
      diceStr = md5Raw.substr(0, temp + 1);
    }
    return [diceStr, md5Str];
  }

  onClickCopy() {
    SicboGameUtils.copyToClipboard(this.md5Cache);
    this.playCopyNoti();
  }

  playCopyNoti(){
    cc.Tween.stopAllByTarget(this.copyNotiNode);
    cc.tween(this.copyNotiNode)
      .to(.2, {scale: 1, opacity: 255})      
      .delay(1)
      .to(.2, {scale:0, opacity: 0})
      .start();
  }

  onDestroy(){
    cc.Tween.stopAllByTarget(this.copyNotiNode);
    cc.Tween.stopAllByTarget(this.md5Mask);
    cc.Tween.stopAllByTarget(this.layout);    
    cc.Tween.stopAllByTarget(this.node);    

  }
}
