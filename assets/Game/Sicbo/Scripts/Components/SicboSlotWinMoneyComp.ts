// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboMoneyFormatComp from "../RNGCommons/SicboMoneyFormatComp";
import SicboSkeletonUtils from "../RNGCommons/Utils/SicboSkeletonUtils";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboSlotWinMoneyComp extends cc.Component {
    @property(sp.Skeleton)
    animation: sp.Skeleton = null;

    @property(SicboMoneyFormatComp)
    money: SicboMoneyFormatComp = null;

    showAnimation = "win_in";
    idleAnimation = "win_loop";
    hideAnimation = "win_out";

    private inTime = 0;
    private idleTime = .5;
    private outTime =  5.5 - 1;

    private elapseTime = 0;

    isShow:boolean = false;
    isIdle:boolean = false;
    isHide:boolean = false;   

    show(money: number, elapseTime: number = 0){   
        this.node.opacity = 0;
        // //cc.log("DKM", elapseTime)    ;
        this.money.setMoney(money);
        this.elapseTime = elapseTime;
    }

    hide(){
        this.node.opacity = 255;
        SicboSkeletonUtils.setAnimation(this.animation, this.hideAnimation, false, this.elapseTime - this.outTime);
        this.isHide = true;
    }

    update(dt){
        this.elapseTime+=dt;
        
        if(this.elapseTime>this.outTime && this.isHide == false){
            this.node.opacity = 255;
            SicboSkeletonUtils.setAnimation(this.animation, this.hideAnimation, false, this.elapseTime - this.outTime);
            this.isHide = true;
        }else if(this.elapseTime>this.idleTime && this.isIdle == false){
            this.node.opacity = 255;
            SicboSkeletonUtils.setAnimation(this.animation, this.idleAnimation, true, this.elapseTime - this.idleTime);
            this.isIdle = true;
        }else if(this.elapseTime>this.inTime && this.isShow == false){
            this.node.opacity = 255;
            SicboSkeletonUtils.setAnimation(this.animation, this.showAnimation, false, this.elapseTime - this.inTime);
            this.isShow = true;
        }

        // if(this.elapseTime> this.outTime + 1) this.node.destroy();
    }
}
