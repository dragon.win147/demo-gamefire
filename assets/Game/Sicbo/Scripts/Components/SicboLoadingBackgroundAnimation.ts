import SicboGameUtils from '../RNGCommons/Utils/SicboGameUtils';
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboLoadingBackgroundAnimation extends cc.Component { 
  public OnBeginLoadResources: Function = null;
  public OnCompleteLoadResources: Function = null;

  start() {
      this.OnBeginLoadResources && this.OnBeginLoadResources();
  }

  public playFinishAnim(onPlayCompleted: Function) {
    onPlayCompleted && onPlayCompleted();
  }

  

  public onSetProgress(fill: number) {}
}
