// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboUserItemEffect extends cc.Component {
    @property([cc.Node])
    nodeArr: cc.Node[] = [];

    protected onLoad(): void {
        for (let i = 0; i < this.nodeArr.length; i++) {
            this.nodeArr[i].opacity = 0;
        }
    }

    fadeIn(duration: number){
        // console.error("Fade In");
        for (let i = 0; i < this.nodeArr.length; i++) {
            cc.tween(this.nodeArr[i])
                .to(duration, {opacity: 255})
                .start();
        }
    }

    fadeOut(duration: number){
        // console.error("Fade Out");

        for (let i = 0; i < this.nodeArr.length; i++) {
            cc.tween(this.nodeArr[i])
                .to(duration, {opacity: 0})
                .start();
        }
    }
}
