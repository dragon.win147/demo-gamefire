// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboModuleAdapter from "../../../../../SicboModuleAdapter";

const {
  State,
  Status} = SicboModuleAdapter.getAllRefs();
import { SicboSound } from "../../Setting/SicboSetting";
import SicboController from "../../Controllers/SicboController";
import SicboSkeletonUtils from "../../RNGCommons/Utils/SicboSkeletonUtils";
import ISicboState from "./ISicboState";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboClockStateComp extends cc.Component implements ISicboState {
  @property(cc.Label)
  countDownText: cc.Label = null;

  @property(sp.Skeleton)
  ringAnimation: sp.Skeleton = null;

  RING_TIME: number = 5;

  elapseTime: number = 0;
  totalDuration: number = 0;

  // lastRemainingTime = -1;
  isRushSfxPlaying = false;
  isClockPaused: boolean = true;

  onLoad() {
    // this.node.active = false;
    this.stopAnimation();
  }

  exitState(status): void {
    this.stopAnimation();
    // this.node.active = false;
    // this.lastRemainingTime = -1;
    this.isRushSfxPlaying = false;

    if (status == Status.BETTING) {
      // SicboController.Instance.playSfx(SicboSound.SfxStopBetting);
      SicboController.Instance.stopSfx(SicboSound.SfxRush);
    }
  }

  updateState(state: InstanceType<typeof State>) {
    let status = state.getStatus();
    this.elapseTime = state.getStageTime();

    if (status != Status.BETTING && status != Status.PAYING) return;
    let remainingTime = Math.max(Math.ceil((this.totalDuration - this.elapseTime) / 1000), 0);

    this.countDownText.string = remainingTime.toString();
    if (status == Status.BETTING && remainingTime <= this.RING_TIME) {
      // if(remainingTime == 0){
      //     this.stopAnimation();
      //     return;
      // }
      if (this.isRushSfxPlaying == false) {
        SicboController.Instance.playSfx(SicboSound.SfxRush, true);
        this.isRushSfxPlaying = true;
      }

      if (this.isClockPaused) {
        this.isClockPaused = false;
        // this.ringAnimation.animation = "animation";
        SicboSkeletonUtils.setAnimation(this.ringAnimation, "animation", true);
      }

      if (this.elapseTime % 1000 == 0) {
        // //cc.log("PlaySfx SfxTictoc");
        // SicboController.Instance.playSfx(SicboSound.SfxTictoc);
        // this.lastRemainingTime = remainingTime;
        cc.tween(this.countDownText.node).to(0.1, { scale: 2 }).to(0.1, { scale: 1 }).start();
      }
    }
  }

  startState(state: InstanceType<typeof State>, totalDuration: number): void {
    let status = state.getStatus();
    let elapseTime = state.getStageTime() / 1000;

    this.elapseTime = elapseTime * 1000;
    this.totalDuration = totalDuration;

    switch (status) {
      case Status.BETTING: case Status.PAYING:
        // this.node.active = true;
        break;

      default:
        // this.node.active = false;
        this.stopAnimation();
        break;
    }
  }

  stopAnimation() {
    SicboSkeletonUtils.setAnimation(this.ringAnimation, "idle", true);

    this.isClockPaused = true;

    this.countDownText.node.scale = 1;
    this.countDownText.string = "0";
    
    // this.ringAnimation.setBonesToSetupPose();
  }

  onDestroy() {
    cc.Tween.stopAllByTarget(this.countDownText.node);
  }

  onHide(): void {
    cc.Tween.stopAllByTarget(this.countDownText.node);
    this.stopAnimation();    

    this.isRushSfxPlaying = false;
    SicboController.Instance.stopSfx(SicboSound.SfxRush);
  }
}
