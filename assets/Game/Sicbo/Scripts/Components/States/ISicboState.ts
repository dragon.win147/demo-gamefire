
import SicboModuleAdapter from "../../../../../SicboModuleAdapter";

const {
  State} = SicboModuleAdapter.getAllRefs();


export default interface ISicboState {
    exitState(status):void;
    startState(state: InstanceType<typeof State>, totalDuration: number):void;
    updateState(state: InstanceType<typeof State>):void;
    onHide():void;
  }
  