// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


import SicboModuleAdapter from "../../../../../SicboModuleAdapter";

const {
  State,
  Status} = SicboModuleAdapter.getAllRefs();
import SicboGameUtils from "../../RNGCommons/Utils/SicboGameUtils";
import SicboSkeletonUtils from "../../RNGCommons/Utils/SicboSkeletonUtils";
import ISicboState from "./ISicboState";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboDealerStateComp extends cc.Component implements ISicboState {
    @property(sp.Skeleton)
    dealerSkeleton: sp.Skeleton = null;

    private skins = ["default"];
    private idleArr = ["idle"];

    // private shakeAnimation = "shake";
    // private openAnimation = "open";
    private betAnimation = "bet";
    
    status = Status.STATUS_UNSPECIFIED;
    onLoad(){
        this.nextIdleAnimation();
        this.setRandomSkin();        
        // SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.openAnimation, false, 100);
    }

    exitState(status): void {}

    updateState(state: InstanceType<typeof State>){}

    startState(state: InstanceType<typeof State>, totalDuration: number): void {
        this.status = state.getStatus();
        let elapseTime = state.getStageTime()/1000;
        switch (this.status) {
            // case Status.WAITING:                
            //     SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.shakeAnimation, false, elapseTime);
            //     SicboController.Instance.playSfx(SicboSound.SfxDealerShake);
            //     break;

            case Status.BETTING:
                SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.betAnimation, false, elapseTime);
                break;
                        
            // case Status.SHOWING_RESULT: 
            //     SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.openAnimation, true);
            default:
                // SicboSkeletonUtils.setAnimation(this.dealerSkeleton, "Waiting Idle", false, elapseTime/1000);
                break;
        }
        
    }

    update(dt){                
        if(this.isAnimationComplete() && this.status!= Status.RESULTING) 
            this.nextIdleAnimation();            
    }

    nextIdleAnimation(){
        let idx = SicboGameUtils.getRandomInt(this.idleArr.length, 0);
        SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.idleArr[idx]); 
        //cc.log("Dealer Idle");
    }

    isAnimationComplete(){
        let trackEntry = this.dealerSkeleton.getCurrent(0) as sp.spine.TrackEntry;
        // //cc.log("[track %s][isComplete %s]end.", trackEntry.trackIndex, trackEntry.isComplete());
        return trackEntry.isComplete();
    }

    setRandomSkin(){
        let idx = SicboGameUtils.getRandomInt(this.skins.length, 0);
        this.dealerSkeleton.setSkin(this.skins[idx]);
    }

    // setMixAnimation (anim1, anim2, mixTime = 0.5) {
    //     this.dealerSkeleton.setMix(anim1, anim2, mixTime);
    //     this.dealerSkeleton.setMix(anim2, anim1, mixTime);
    // }

    onHide(): void {    
        this.nextIdleAnimation();
        this.setRandomSkin();        
        // SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.openAnimation, false, 100);
    }
}
