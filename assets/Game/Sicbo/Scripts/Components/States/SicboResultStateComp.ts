// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboModuleAdapter from "../../../../../SicboModuleAdapter";

const { GetChannelHistoryReply, Door, State, Result, Status, ChannelHistoryMetadata } = SicboModuleAdapter.getAllRefs();

import { SicboSound } from "../../Setting/SicboSetting";
import SicboDiceConfig from "../../Configs/SicboDiceConfig";
import SicboController from "../../Controllers/SicboController";
import SicboHelper from "../../Helpers/SicboHelper";

import SicboBetComp from "../SicboBetComp";
import SicboLatestResultComp from "../SicboLatestResultComp";
import SicboNotifyComp from "../SicboNotifyComp";
import SicboSlotWinMoneyComp from "../SicboSlotWinMoneyComp";
import SicboUIComp from "../SicboUIComp";
import ISicboState from "./ISicboState";
import SicboPortalAdapter from "../../SicboPortalAdapter";
import SicboGameUtils from "../../RNGCommons/Utils/SicboGameUtils";
import SicboSkeletonUtils from "../../RNGCommons/Utils/SicboSkeletonUtils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboResultStateComp extends cc.Component implements ISicboState {
  @property(cc.Node)
  betCompNode: cc.Node = null;
  private betComp: SicboBetComp = null;

  @property(cc.Node)
  winFxShinnyNode: cc.Node = null;

  @property(cc.Animation)
  chenAnimation: cc.Animation = null;

  @property(cc.Node)
  chenSmallRoot: cc.Node = null;

  @property(cc.Node)
  chenZoomInRoot: cc.Node = null;

  @property([cc.Sprite])
  diceSprites: cc.Sprite[] = [];

  @property(SicboLatestResultComp)
  latestResultComp: SicboLatestResultComp = null;

  @property(sp.Skeleton)
  dealerSkeleton: sp.Skeleton = null;

  @property(cc.Prefab)
  slotWinMoneySmallPrefab: cc.Prefab = null;

  @property(cc.Prefab)
  slotWinMoneyLargePrefab: cc.Prefab = null;

  @property(SicboNotifyComp)
  notifyComp: SicboNotifyComp = null;

  private winFxSkeletons: sp.Skeleton[] = [];
  private winFxShinnySkeletons: sp.Skeleton[] = [];
  private isWinFxPlaying = false;
  private winFxTween: cc.Tween = null;

  private curSessionId: number;
  private result: InstanceType<typeof Result>;
  private wonDoors: any[] = [];
  private wonDoorsFiltered: any[] = [];
  private items: any[] = [];

  private chenOpenAnim = "ChenOpen";
  private chenIdleAnim = "ChenIdle";
  // private chenCloseAnim = "ChenClose";
  private chenRollDiceAnim = "chenRollDice";

  //NOTE startTime tinh tu khi state bat dau
  //ANCHOR Status.RESULTING start time
  private chenRollDiceStartTime = 0.1;
  private chenRollDiceSfxStartTime = this.chenRollDiceStartTime + 0.2;
  private chenRollDiceSfxStopTime = this.chenRollDiceSfxStartTime + 0.8;

  private chenOpenStartTime = 0.5; //NOTE 1;
  private chenOpenSfxStartTime = this.chenOpenStartTime + 1.5;
  // private chenCloseStartTime = 0;
  private chenIdleStartTime = this.chenOpenStartTime + 5;
  private dicesFlyToLatestResultStartTime = this.chenIdleStartTime + 0.7;
  private doorWinFxShinnyStartTime = this.chenIdleStartTime + 0.5;
  private doorWinFxStartTime = this.chenIdleStartTime + +2.1;

  //ANCHOR Status.PAYING start time
  private chipLoseStartTime = 0;
  private chipStackStartTime = 1;
  private returnChipStartTime = 3 - 1;
  private payingStartTime = 6 - 1;

  private isPayingPlaying: boolean = false;

  //other user group key
  private otherGroupKey = "otherGroupKey";

  winMoneyAtSlots: SicboSlotWinMoneyComp[] = [];

  onLoad() {
    this.betComp = this.betCompNode.getComponent(SicboBetComp);

    this.winFxShinnySkeletons = this.winFxShinnyNode.getComponentsInChildren(sp.Skeleton);
    this.winFxSkeletons = this.winFxShinnySkeletons;

    this.hideWinFx();
  }

  //SECTION ISicboState
  exitState(status): void {
    this.isPayingPlaying = false;
  }

  updateState(state: InstanceType<typeof State>): void {
    if (this.isPayingPlaying) return;
    let status = state.getStatus();
    let elapseTime = state.getStageTime() / 1000;

    if (status == Status.PAYING && elapseTime >= 0.1) {
      //NOTE server delay 0.1s for generate result
      this.setResult(state.getResult());
      //cc.log("updateState", this.result.toObject());
      this.playPayingAnimation(elapseTime);
      this.isPayingPlaying = true;
    }
  }

  private setResult(result: InstanceType<typeof Result>) {
    if (SicboGameUtils.isNullOrUndefined(result)) return;
    //cc.log("RESULT1", result.hasPlayerBetTx(), result.toObject());
    this.result = result;
    this.items = result.getItemsList().sort();
    this.wonDoors = result.getDoorsList();

    this.wonDoorsFiltered = this.wonDoors.filter(function (elem, index, self) {
      return index === self.indexOf(elem);
    });

    this.setDiceResult();
  }

  startState(state: InstanceType<typeof State>, totalDuration: number): void {
    this.curSessionId = state.getTableSessionId();
    let status = state.getStatus();
    let elapseTime = state.getStageTime() / 1000;
    this.setResult(state.getResult());
    //console.error(SicboHelper.StatusToString(status), elapseTime);
    switch (status) {
      case Status.WAITING:
        SicboHelper.changeParent(this.chenAnimation.node, this.chenZoomInRoot);
        this.chenRollDice(elapseTime);
        this.chenRollDiceSfx(elapseTime);
        this.chenRollDiceSfx(elapseTime, false);

        this.hideWinFx();
        this.latestResultComp.playFx(false);

        this.destroyWinMoneyAtDoor();
        break;
      case Status.RESULTING:
        this.setDiceResult();

        SicboHelper.changeParent(this.chenAnimation.node, this.chenZoomInRoot);
        this.chenOpen(elapseTime);
        this.chenOpenSfx(elapseTime);
        this.chenIdle(elapseTime);

        this.dicesFlyToLatestResult(elapseTime);

        this.playWinFxShinny(elapseTime);
        this.playWinFx(elapseTime);
        break;

      case Status.PAYING:
        // this.destroyWinMoneyAtDoor();
        // this.statisticComp.showHideSoiCauNewEffect(true);
        SicboHelper.changeParent(this.chenAnimation.node, this.chenSmallRoot);

        this.chenIdle(100);

        ////console.log("BCF PAYING WinnersList", elapseTime, state.getResult()?.getWinnersList());
        // //console.log("BCF PAYING Receipt", elapseTime, state.getResult()?.getPlayerBetTx());

        this.playWinFx(100);
        this.latestResultComp.playFx();

        // this.playChipInDoorAnimation(elapseTime);
        this.playLoseAnimation(elapseTime);
        this.playChipStackAnimation(elapseTime);

        this.dealerReturnWinChip(elapseTime);
        // this.playPayingAnimation(elapseTime);  //NOTE server delay 0.1s for generate result
        break;
      // case Status.FINISHING:
      //   return;//FIXME
      // this.chenClose(elapseTime);
      // this.statisticComp.showHideSoiCauNewEffect(false);

      default:
        this.hideWinFx();
        this.latestResultComp.playFx(false);
    }
  }

  //!SECTION

  //SECTION Chen
  setDiceResult() {
    //cc.log("DICES", this.items);
    for (let i = 0; i < this.items.length; i++) {
      let spriteName = SicboHelper.convertItemToDiceSpriteName(this.items[i]);
      let self = this;
      self.diceSprites[i].spriteFrame = SicboDiceConfig.Instance.getDice(spriteName);
    }
  }

  chenRollDice(elapseTime: number = 0) {
    if (elapseTime > this.chenRollDiceStartTime) {
      this.chenAnimation.play(this.chenRollDiceAnim, elapseTime - this.chenRollDiceStartTime);
      // this.playDealerOpenAnimation();
    } else {
      cc.tween(this.node)
        .delay(this.chenRollDiceStartTime - elapseTime)
        .call(() => {
          // this.playDealerOpenAnimation();
          this.chenAnimation.play(this.chenRollDiceAnim);
        })
        .start();
    }
  }

  chenRollDiceSfx(elapseTime: number = 0, isPlay: boolean = true) {
    let time = isPlay ? this.chenRollDiceSfxStartTime : this.chenRollDiceSfxStopTime;
    if (elapseTime > time) {
      if (isPlay) SicboController.Instance.playSfx(SicboSound.SfxDealerShake);
      else SicboController.Instance.stopSfx(SicboSound.SfxDealerShake);
    } else {
      cc.tween(this.node)
        .delay(time - elapseTime)
        .call(() => {
          if (isPlay) SicboController.Instance.playSfx(SicboSound.SfxDealerShake);
          else SicboController.Instance.stopSfx(SicboSound.SfxDealerShake);
        })
        .start();
    }
  }

  chenOpen(elapseTime: number = 0) {
    if (elapseTime > this.chenOpenStartTime) {
      this.chenAnimation.play(this.chenOpenAnim, elapseTime - this.chenOpenStartTime);
      // this.playDealerOpenAnimation();
    } else {
      cc.tween(this.node)
        .delay(this.chenOpenStartTime - elapseTime)
        .call(() => {
          // this.playDealerOpenAnimation();
          this.chenAnimation.play(this.chenOpenAnim);
        })
        .start();
    }
  }

  chenOpenSfx(elapseTime: number = 0) {
    let self = this;
    if (elapseTime > this.chenOpenSfxStartTime) {
    } else {
      cc.tween(self.node)
        .delay(self.chenOpenSfxStartTime - elapseTime)
        .call(() => {
          SicboController.Instance.playSfx(SicboSound.SfxResult);
          self.notifyComp.showResultNoti(self.getResultText(self.result.getItemsList()));
        })
        .start();
    }
  }

  getResultText(items: any[]) {
    let total = SicboHelper.getResultTotalValue(items).toString();
    let typeResult = SicboHelper.getResultType(items);
    let typeName = SicboHelper.getResultTypeName(typeResult);
    return total + " điểm - " + typeName;
  }

  // private playDealerOpenAnimation(){
  //   SicboSkeletonUtils.setAnimation(this.dealerSkeleton, "open", true);
  //   //cc.log("Dealer Open");
  // }

  chenIdle(elapseTime: number = 0) {
    if (elapseTime > this.chenIdleStartTime) {
      this.chenAnimation.play(this.chenIdleAnim, elapseTime - this.chenIdleStartTime);
    } else
      cc.tween(this.node)
        .delay(this.chenIdleStartTime - elapseTime)
        .call(() => {
          this.chenAnimation.play(this.chenIdleAnim);
          // SoundManager.getInstance().playEffLocalName(SoundDefine.Sicbo_LidClose, SoundDefine.Sicbo);
        })
        .start();
  }

  // chenClose(elapseTime: number = 0){
  //   if(elapseTime>this.chenCloseStartTime)
  //     this.chenAnimation.play(this.chenCloseAnim, elapseTime - this.chenCloseStartTime);
  //   else
  //     cc.tween(this.node)
  //       .delay(this.chenCloseStartTime - elapseTime)
  //       .call(
  //         ()=>{
  //           this.chenAnimation.play(this.chenCloseAnim);
  //           // SoundManager.getInstance().playEffLocalName(SoundDefine.Sicbo_LidClose, SoundDefine.Sicbo);
  //         }
  //       ).start();
  // }

  dicesFlyToLatestResult(elapseTime: number = 0) {
    if (elapseTime > this.dicesFlyToLatestResultStartTime) {
      // this.statisticComp.reloadLatestRecordWithAnimation(this.result.getItemsList());
      // this.statisticComp.showHideSoiCauNewEffect(true, .5);
      // SicboUIComp.Instance.getHistory(true);
      // SicboController.Instance.playSfx(SicboSound.SfxMessage);
    } else {
      let self = this;

      cc.tween(self.node)
        .delay(self.dicesFlyToLatestResultStartTime - elapseTime)
        .call(() => {
          //cc.log("dicesFlyToLatestResultStartTime", self.dicesFlyToLatestResultStartTime);

          let diceFlyDuration = 0.5;
          for (let i = 0; i < self.diceSprites.length; i++) {
            let diceOrigin = self.diceSprites[i].node;
            let dice = cc.instantiate(diceOrigin);
            dice.setParent(diceOrigin.parent);
            dice.setPosition(diceOrigin.getPosition());
            dice.scale = 0.165;

            SicboHelper.changeParent(dice, self.chenZoomInRoot);
            let targetPos = self.latestResultComp.items[i].node.getPosition();
            targetPos = SicboGameUtils.convertToOtherNode2(self.latestResultComp.items[i].node, self.chenZoomInRoot);
            cc.tween(dice)
              .to(diceFlyDuration, {
                x: targetPos.x,
                y: targetPos.y,
                scale: 0.2,
              })
              .call(() => {
                dice.destroy();
              })
              .start();
          }

          cc.tween(self.node)
            .delay(diceFlyDuration)
            .call(() => {
              self.latestResultComp.setData(self.result.getItemsList());
              self.latestResultComp.playFx();
              // self.statisticComp.showHideSoiCauNewEffect(true, .5);
              SicboController.Instance.playSfx(SicboSound.SfxMessage);
              // SicboUIComp.Instance.getHistory(true);
            })
            .start();
        })
        .start();
    }
  }
  //!SECTION

  //SECTION Door WinFX
  playWinFx(elapseTime: number = 0) {
    if (this.wonDoors.length <= 0) return;
    if (this.isWinFxPlaying) return;

    if (elapseTime > this.doorWinFxStartTime) {
      this.winFx();
    } else {
      let self = this;
      self.winFxTween?.stop();
      self.winFxTween = cc
        .tween(this.winFxShinnyNode)
        .delay(this.doorWinFxStartTime - elapseTime)
        .call(() => {
          this.winFx();
        })
        .start();
    }
  }

  private winFx() {
    this.isWinFxPlaying = true;
    for (let index = 0; index < this.winFxSkeletons.length; index++) {
      if (this.wonDoorsFiltered[index] != null) {
        SicboSkeletonUtils.setAnimation(this.winFxSkeletons[index], this.wonDoorsFiltered[index].toString() + "b", true);
        this.winFxSkeletons[index].node.active = true;
      } else {
        this.winFxSkeletons[index].node.active = false;
      }
    }
  }

  playWinFxShinny(elapseTime: number) {
    let self = this;
    if (elapseTime > self.doorWinFxShinnyStartTime) {
      self.winFxShinny(elapseTime - self.doorWinFxShinnyStartTime);
    } else {
      cc.tween(this.node)
        .delay(this.doorWinFxShinnyStartTime - elapseTime)
        .call(() => {
          self.winFxShinny();
        })
        .start();
    }
  }
  private winFxShinny(startTime: number = 0) {
    let self = this;

    for (let index = 0; index < self.winFxShinnySkeletons.length; index++) {
      if (self.wonDoorsFiltered[index] != null) {
        SicboSkeletonUtils.setAnimation(self.winFxShinnySkeletons[index], this.wonDoorsFiltered[index].toString(), false, startTime);
        self.winFxShinnySkeletons[index].node.active = true;
      } else {
        self.winFxShinnySkeletons[index].node.active = false;
      }
    }
  }

  hideWinFx() {
    this.winFxTween?.stop();

    cc.Tween.stopAllByTarget(this.winFxShinnyNode);
    for (let index = 0; index < this.winFxSkeletons.length; index++) {
      this.winFxSkeletons[index].node.active = false;
    }

    for (let index = 0; index < this.winFxShinnySkeletons.length; index++) {
      this.winFxShinnySkeletons[index].node.active = false;
    }
    this.isWinFxPlaying = false;
    this.wonDoors = [];

    this.winMoneyAtSlots.forEach((element) => {
      element.hide();
    });
  }
  //!SECTION

  //SECTION other betComp FX
  //ANCHOR playLoseAnimation
  playLoseAnimation(elapseTime: number) {
    let self = this;
    self.elapseTimeCheck(
      elapseTime,
      self.chipLoseStartTime,
      () => {
        self.betComp.playLoseAnimation(self.wonDoorsFiltered, 0);
      },
      () => {
        let duration = 0.5;
        self.betComp.playLoseAnimation(self.wonDoorsFiltered, duration);
      }
    );
  }

  playChipStackAnimation(elapseTime: number) {
    let self = this;
    self.elapseTimeCheck(
      elapseTime,
      self.chipStackStartTime,
      () => {
        self.betComp.playChipStackAnimation(self.wonDoorsFiltered, 0);
      },
      () => {
        let duration = 0.5;
        self.betComp.playChipStackAnimation(self.wonDoorsFiltered, duration);
      }
    );
  }

  //ANCHOR dealerReturnWinChip
  dealerReturnWinChip(elapseTime: number) {
    let userWinAmount = this.getMyUserWinAmount();
    ////console.log("BCF userWinAmount", userWinAmount);
    let self = this;
    let delayShowInMoney = 0;
    if (elapseTime > this.returnChipStartTime) {
      this.betComp.dealerReturnFullStackChip(this.wonDoorsFiltered, 0);
      delayShowInMoney = 0;
    } else {
      cc.tween(this.node)
        .delay(this.returnChipStartTime - elapseTime)
        .call(() => {
          delayShowInMoney = self.betComp.dealerReturnWinChip(self.wonDoorsFiltered) + 0.5;

          cc.tween(this.node)
            .delay(delayShowInMoney)
            .call(() => {
              userWinAmount.forEach((amount: number, door) => {
                self.showWinMoneyAtDoor(amount, door, elapseTime);
              });
              if (userWinAmount.size > 0) SicboController.Instance.playSfx(SicboSound.SfxWin);
            })
            .start();
        })
        .start();
    }
  }

  //ANCHOR playPayingAnimation
  playPayingAnimation(elapseTime: number) {
    //cc.log("SICBO playPayingAnimation", this.result.toObject());
    if (elapseTime > this.payingStartTime) {
      //NOTE k play animation cong tien o avatar khi user vao luc co result, do tien da duoc update o server
      this.result.getWinnersList().forEach((record) => {
        let user = SicboUIComp.Instance.findUserComp(record.getUserId());

        let payingMoney = record.getAmount();
        this.betComp.playPayingAnimation(record.getBet().getDoor(), user.node, payingMoney, 0);
      });
    } else {
      let self = this;
      cc.tween(this.node)
        .delay(this.payingStartTime - elapseTime)
        .call(() => {
          self.result.getWinnersList().forEach((record) => {
            let user = SicboUIComp.Instance.findUserComp(record.getUserId());
            self.betComp.playPayingAnimation(record.getBet().getDoor(), user.node, record.getAmount());
          });

          let delayShowAvatarWinMoney = 1.05;

          let payingMoney = self.result.hasPlayerBetTx() ? self.result.getPlayerBetTx().getAmount() : 0; //from chip
          let transaction = self.result.hasPlayerBetTx() ? self.result.getPlayerBetTx().getLastTxId() : 0;

          if (self.result.hasLotteryWinner() && SicboPortalAdapter.getInstance().getMyUserId() == self.result.getLotteryWinner().getUserId()) {
            //from jackpot
            payingMoney += self.result.getLotteryWinner().getAmount();
            let jackpotTransaction = self.result.hasPlayerJackpotTx() ? self.result.getPlayerJackpotTx().getLastTxId() : 0;
            transaction = Math.max(transaction, jackpotTransaction);
          }

          self.payMoneyForMyUser(payingMoney, transaction, delayShowAvatarWinMoney);

          let wonAmountFiltered = self.filterWinAmount(self.result.getWinnersList(), self.result.getLotteryWinner());
          wonAmountFiltered.forEach((amount: number, userId: string) => {
            let user = SicboUIComp.Instance.findUserComp(userId);
            if (self.result.hasLotteryWinner() && userId == self.result.getLotteryWinner().getUserId())
              //from jackpot
              amount += self.result.getLotteryWinner().getAmount();

            cc.tween(this.node)
              .delay(delayShowAvatarWinMoney)
              .call(() => {
                user.showWinMoney(amount);
              })
              .start();
          });
        })
        .start();
    }
  }

  //ANCHOR filterWinAmount
  filterWinAmount(records: InstanceType<typeof Result.Record>[], jackpotWinner) {
    let result: Map<string, number> = new Map<string, number>();
    let myUserId = SicboPortalAdapter.getInstance().getMyUserId();

    if (jackpotWinner != null) {
      let jackpotUserId = jackpotWinner.getUserId();
      if (myUserId != jackpotUserId) {
        if (!result.has(jackpotUserId)) result.set(jackpotUserId, 0);
      }
    }

    records.forEach((record) => {
      let userId = null;
      if (record.getOthers()) userId = this.otherGroupKey;
      else {
        userId = record.getUserId();
      }

      if (userId != myUserId) {
        if (!result.has(userId)) result.set(userId, 0);

        result.set(userId, result.get(userId) + record.getAmount());
      }
    });

    return result;
  }

  private elapseTimeCheck(elapseTime: number, startTime: number, onPassedElapseTime: () => void, onNOTPassElapseTime: () => void) {
    if (elapseTime > startTime) {
      if (SicboHelper.approximatelyEqual(elapseTime - startTime, 0, 0.1)) {
        onNOTPassElapseTime && onNOTPassElapseTime();
      } else {
        onPassedElapseTime && onPassedElapseTime();
      }
    } else {
      cc.tween(this.node)
        .delay(Math.max(startTime - elapseTime, 0))
        .call(() => {
          onNOTPassElapseTime && onNOTPassElapseTime();
        })
        .start();
    }
  }

  payMoneyForMyUser(amount: number, transaction, delay: number = 0) {
    if (amount == 0) return;
    let user = SicboUIComp.Instance.myUserComp();
    cc.tween(this.node)
      .delay(delay)
      .call(() => {
        SicboController.Instance.addMyUserMoney(amount, transaction);
        //cc.log("SICBO RECEIPT", amount);
        user.showWinMoney(amount);
        SicboController.Instance.playSfx(SicboSound.SfxTotalWin);
      })
      .start();
  }

  //ANCHOR showWinMoneyAtDoor
  getMyUserWinAmount(): Map<any, number> {
    let winnerList = this.result.getWinnersList();
    let rs: Map<any, number> = new Map<any, number>();
    let userId = SicboPortalAdapter.getInstance().getMyUserId();

    winnerList.forEach((record) => {
      if (record.getUserId() == userId) {
        let door = record.getBet().getDoor();
        if (rs.has(door)) {
          rs.set(door, record.getAmount() + rs.get(door));
        } else {
          rs.set(door, record.getAmount());
        }
      }
    });
    return rs;
  }

  showWinMoneyAtDoor(money: number, door, elapseTime: number = 0) {
    let d = this.betComp.getDoor(door);
    let prefab: cc.Prefab = this.slotWinMoneySmallPrefab;
    switch (door) {
      case Door.ANY_TRIPLE:
      case Door.BIG:
      case Door.SMALL:
      case Door.ONE_SINGLE:
      case Door.TWO_TRIPLE:
      case Door.THREE_TRIPLE:
      case Door.FOUR_TRIPLE:
      case Door.FIVE_TRIPLE:
      case Door.SIX_TRIPLE:
      case Door.ONE_SINGLE:
      case Door.TWO_SINGLE:
      case Door.THREE_SINGLE:
      case Door.FOUR_SINGLE:
      case Door.FIVE_SINGLE:
      case Door.SIX_SINGLE:
        prefab = this.slotWinMoneyLargePrefab;
        break;
    }

    let comp = SicboGameUtils.createItemFromPrefab(SicboSlotWinMoneyComp, prefab, d.node);
    comp.show(money, elapseTime);
    this.winMoneyAtSlots.push(comp);
  }

  destroyWinMoneyAtDoor() {
    this.winMoneyAtSlots.forEach((element) => {
      element.node?.destroy();
    });
    this.winMoneyAtSlots.length = 0;
  }
  //!SECTION

  updateResultFistTime(records: InstanceType<typeof GetChannelHistoryReply.Record>[]) {
    if (SicboGameUtils.isNullOrUndefined(records)) {
      this.latestResultComp.active = false;
      return;
    }

    let lastRecord = records[0];
    // if(this.updateLastResult == false)
    //   lastRecord = records[1];

    if (SicboGameUtils.isNullOrUndefined(lastRecord)) {
      this.latestResultComp.active = false;
      return;
    }

    this.latestResultComp.active = true;
    let data = ChannelHistoryMetadata.deserializeBinary(lastRecord.getMetadata_asU8());
    let items = data.getItemsList().sort();
    this.latestResultComp.setData(items, false);
  }

  onDestroy() {
    cc.Tween.stopAllByTarget(this.node);
    cc.Tween.stopAllByTarget(this.winFxShinnyNode);
  }

  onHide(): void {
    SicboController.Instance.stopSfx(SicboSound.SfxDealerShake);
    cc.Tween.stopAllByTarget(this.node);
    cc.Tween.stopAllByTarget(this.winFxShinnyNode);
    // this.chenClose(100);
    this.hideWinFx();
    this.destroyWinMoneyAtDoor();
    this.isPayingPlaying = false;
  }
}
