// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import SicboModuleAdapter from "../../../../../SicboModuleAdapter";

const {
  State,
  Status} = SicboModuleAdapter.getAllRefs();

import SicboSetting, { SicboSound } from "../../Setting/SicboSetting";
import SicboController from "../../Controllers/SicboController";
import SicboHelper from "../../Helpers/SicboHelper";
import SicboGameUtils from "../../RNGCommons/Utils/SicboGameUtils";
import SicboNotifyComp from "../SicboNotifyComp";
import ISicboState from "./ISicboState";
import SicboText from '../../Setting/SicboText';

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboNotifyStateComp extends cc.Component implements ISicboState{
    @property(SicboNotifyComp)
    notifyComp: SicboNotifyComp = null;

    elapseTime: number = 0;
    totalDuration: number = 0;

    isWaiting: boolean = false;

    exitState(status): void {
      if(status == Status.FINISHING)
        this.notifyComp.forceHideSmallNoti();
      this.isWaiting = false;
    }

    updateState(state: InstanceType<typeof State>): void {
      if(this.isWaiting == false) return;

      this.elapseTime = state.getStageTime();
      let remainingTime = Math.max(Math.ceil((this.totalDuration - this.elapseTime)/ 1000), 0);      
      this.notifyComp.updateSmallNotiMessage(SicboGameUtils.FormatString(SicboText.waitingRemainingTimeMsg, remainingTime));      
    }

    startState(state: InstanceType<typeof State>, totalDuration: number): void {
      let status = state.getStatus();
      let elapseTime = state.getStageTime()/1000;
      
      this.elapseTime = elapseTime * 1000;
      this.totalDuration = totalDuration;      
        switch (status) {
            case Status.FINISHING: 
              if(SicboSetting.getStatusDuration(Status.FINISHING)<= 1*1000) return;
              this.isWaiting = true;
              let remainingTime = Math.max(Math.floor((this.totalDuration - this.elapseTime)/ 1000), 0);              
              let showTime = Math.max(Math.min(totalDuration - 1, totalDuration - this.elapseTime - 1), 0);
              this.notifyComp.showSmallNoti(SicboGameUtils.FormatString(SicboText.waitingRemainingTimeMsg, remainingTime), .5, showTime, .5, true);
              SicboController.Instance.playSfx(SicboSound.SfxMessage);
              break;
            case Status.END_BET:              
              if(SicboHelper.approximatelyEqual(this.elapseTime, 0, 100))
                this.notifyComp.showStopBettingMessage();
              break;
            case Status.BETTING:              
              if(SicboHelper.approximatelyEqual(this.elapseTime, 0, 100))
                this.notifyComp.showBettingMessage();
              break;
            default:
              break;
          }
    }
    
    onHide(): void {
      this.notifyComp.forceHideLargeNoti();
      this.notifyComp.forceHideResultNoti();
      this.notifyComp.forceHideSmallNoti();
    }
}
