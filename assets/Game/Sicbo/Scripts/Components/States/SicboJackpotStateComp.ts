// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboModuleAdapter from "../../../../../SicboModuleAdapter";

const {
  Topic,
  State,
  Result,
  Status,
  ListCurrentJackpotsReply
} = SicboModuleAdapter.getAllRefs();

import { SicboSound } from "../../Setting/SicboSetting";
import SicboController from "../../Controllers/SicboController";
import SicboHelper from "../../Helpers/SicboHelper";
import ISicboState from "./ISicboState";
import SicboPortalAdapter from '../../SicboPortalAdapter';
import SicboGameUtils from "../../RNGCommons/Utils/SicboGameUtils";
import SicboSkeletonUtils from "../../RNGCommons/Utils/SicboSkeletonUtils";
import SicboMoneyFormatComp from "../../RNGCommons/SicboMoneyFormatComp";
const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboJackpotStateComp extends cc.Component implements ISicboState {
  //ANCHOR jackpot stand
  @property(cc.Node)
  jackpotStandNode: cc.Node = null;

  @property(sp.Skeleton)
  jackPotStandSkeleton: sp.Skeleton = null;

  @property(SicboMoneyFormatComp)
  jackpotStandValueLabel: SicboMoneyFormatComp = null;

  //ANCHOR my jackpot animation
  @property(sp.Skeleton)
  myJackpotFrontSkeleton: sp.Skeleton = null;

  @property(sp.Skeleton)
  myJackpotBackSkeleton: sp.Skeleton = null;

  @property(cc.Prefab)
  particlePrefab: cc.Prefab = null;

  @property(SicboMoneyFormatComp)
  jackpotRandomLabel: SicboMoneyFormatComp = null;

  @property(SicboMoneyFormatComp)
  jackpotNumberLabel: SicboMoneyFormatComp = null;

  @property(cc.Label)
  otherWinnerNameLabel: cc.Label = null;
  private jackpotAnimationStartTime = 0;
  // private jackpotAnimationEndTime = this.jackpotAnimationStartTime + 4;

  private isJackpotPlaying = false;
  private jackPotStandPlayingAnim: string;

  private jackpotScaleSfxStartTime: number = this.jackpotAnimationStartTime;
  private jackpotScaleSfxStopTime: number = this.jackpotScaleSfxStartTime + 1.5;
  private jackpotFinalSfxStartTime: number = this.jackpotScaleSfxStartTime + 1.5;

  private particleDelayTime: number = 1.8;

  private result: InstanceType<typeof Result>;
  private particleInstance: cc.Node = null;


  private jackpotValue: number = 0;

  private setResult(result: InstanceType<typeof Result>) {
    if (SicboGameUtils.isNullOrUndefined(result)) return;
    this.result = result;
  }

  onLoad() {
    this.setMyJackpotAnimActive(false);
    //this.jackpotStandNode.active = true;
    this.playJackpotStandAnimation(true);
    this.setOtherWinnerActive(false);
  }

  exitState(status): void {
    this.isJackpotPlaying = false;

    // if(status != Status.RESULTING && status != Status.PAYING) {
    if (status != Status.PAYING) {
      this.setMyJackpotAnimActive(false); //NOTE: do anim jackpot hoi dai
      //this.jackpotStandNode.active = true;
    }

    this.stopParticleFX();
    this.playJackpotStandAnimation(true);
  }

  updateState(state: InstanceType<typeof State>): void {
    if (this.isJackpotPlaying) return;
    let status = state.getStatus();
    let elapseTime = state.getStageTime() / 1000;

    if (this.isUpdateJackpotValue(status) || this.jackpotStandValueLabel.moneyValue == 0) {
        // this.updateJackpot(state.getPot() || 0);// tạm ẩn không cập nhật theo state
        this.updateJackpot(this.jackpotValue || 0);// tạm ẩn không cập nhật theo state
    }
    
    if (SicboGameUtils.isNullOrUndefined(this.result)) return;
    if (!this.result.hasLotteryWinner()) return;

    // if(status == Status.RESULTING && elapseTime >= this.jackpotAnimationStartTime){
    if (status == Status.PAYING && elapseTime >= this.jackpotAnimationStartTime) {
      this.isJackpotPlaying = true;

      this.otherWinnerNameLabel.string = this.result.getLotteryWinner().getDisplayName();
      if (SicboPortalAdapter.getInstance().getMyUserId() == this.result.getLotteryWinner().getUserId()) {
        this.setMyJackpotAnimActive(true);
        this.playParticleFx(elapseTime - this.jackpotAnimationStartTime);
        //this.jackpotStandNode.active = false;

        SicboSkeletonUtils.setAnimation(this.myJackpotFrontSkeleton, "animation", false, elapseTime - this.jackpotAnimationStartTime);
        SicboSkeletonUtils.setAnimation(this.myJackpotBackSkeleton, "animation", false, elapseTime - this.jackpotAnimationStartTime);
        this.myJackpotBackSkeleton.setCompleteListener((_) => {
          this?.setMyJackpotAnimActive(false);
        });

        // let self = this;
        // cc.tween(self.node)
        //     .delay(self.jackpotAnimationEndTime - elapseTime)
        //     .call(()=>self?.setMyJackpotAnimActive(false))
        //     .start();

        this.jackpotNumberLabel.setMoney(0);
        this.jackpotNumberLabel.runMoneyTo(this.result.getLotteryWinner().getAmount(), 3);

        this.setOtherWinnerActive(false);

        this.myJackpotSfx(SicboSound.SfxJackpotScale, elapseTime, this.jackpotScaleSfxStartTime);
        this.myJackpotSfx(SicboSound.SfxJackpotScale, elapseTime, this.jackpotScaleSfxStopTime, false);
        this.myJackpotSfx(SicboSound.SfxJackpotFinal, elapseTime, this.jackpotFinalSfxStartTime);

        //this.jackpotStandNode.active = true;
        this.setOtherWinnerActive(true);
        this.playJackpotStandAnimation(false);
      } else {
        //Other user win jackpot
        this.setMyJackpotAnimActive(false);
        //this.jackpotStandNode.active = true;
        this.setOtherWinnerActive(true);
        this.playJackpotStandAnimation(false);
      }
    }
  }

  isUpdateJackpotValue(status){
    switch(status){
      case Status.PAYING:
      case Status.END_BET:
      case Status.RESULTING:
        return false
      
      default: return true;
    }
  }

  handleResponseJackpot(res: InstanceType<typeof ListCurrentJackpotsReply>) {
    let getJackpotsList = res.getJackpotsList();
    // let mapJackpot = new Map<Topic, ListCurrentJackpotsReply.Jackpot[]>();
    getJackpotsList.forEach((jackpot) => {
      let topic = jackpot.getTopic();
      if (topic == Topic.SICBO) {
        let jackpots: InstanceType<typeof ListCurrentJackpotsReply.Jackpot>[] = [jackpot];
        let amountJackpot = jackpots[0].getAmount();
        // this.updateJackpot(amountJackpot);
        this.jackpotValue = amountJackpot;
        return;
      }
    });
  }

  updateJackpot(value: number) {
    //cc.log("updateJackpot " + value);
    this.jackpotStandValueLabel.runMoneyTo(value, .2);
    this.jackpotRandomLabel.runMoneyTo(value, .2);
  }

  myJackpotSfx(sfxName: SicboSound, elapseTime: number = 0, time: number, isPlay: boolean = true) {
    let self = this;

    if (elapseTime > time) {
      if (isPlay) {
        //qua tgian thi skip
        if (SicboHelper.approximatelyEqual(elapseTime, time)) SicboController.Instance.playSfx(sfxName);
      } else SicboController.Instance.stopSfx(sfxName);
    } else {
      cc.tween(self.node)
        .delay(time - elapseTime)
        .call(() => {
          if (isPlay) SicboController.Instance.playSfx(sfxName);
          else SicboController.Instance.stopSfx(sfxName);
        })
        .start();
    }
  }

  startState(state: InstanceType<typeof State>, totalDuration: number): void {
    this.setResult(state.getResult());
    let status = state.getStatus();
    // if(status != Status.RESULTING && status != Status.PAYING)
    if (status != Status.PAYING) this.setOtherWinnerActive(false);

    // if( status == Status.PAYING && this.jackpotStandValueLabel.moneyValue != 0) return;
    // this.jackpotStandValueLabel.setMoney(state.getPot()||0);
    // this.jackpotRandomLabel.setMoney(state.getPot()||0);
  }

  private setMyJackpotAnimActive(isActive: boolean = true) {
    this.myJackpotFrontSkeleton.node.active = isActive;
    this.myJackpotBackSkeleton.node.active = isActive;

    if (!isActive) this.stopParticleFX();
  }

  private playParticleFx(elapseTime: number) {
    this.stopParticleFX();

    this.particleInstance = cc.instantiate(this.particlePrefab);
    this.particleInstance.setParent(this.node);
    this.particleInstance.setPosition(cc.Vec2.ZERO);

    let particles: cc.ParticleSystem3D[] = this.particleInstance.getComponentsInChildren(cc.ParticleSystem3D);
    for (let i = 0; i < particles.length; i++) {
      let particle = particles[i];
      particle.startDelay.constant = Math.max(this.particleDelayTime - elapseTime, 0);
    }
  }

  private stopParticleFX() {
    if (this.particleInstance) this.particleInstance.destroy();
  }

  onHide(): void {
    if (this.node != null) cc.Tween.stopAllByTarget(this.node);
    this.setMyJackpotAnimActive(false);
    //this.jackpotStandNode.active = true;
    this.isJackpotPlaying = false;
    this.jackPotStandPlayingAnim = null;

    this.setOtherWinnerActive(false);

    SicboController.Instance.stopSfx(SicboSound.SfxJackpotFinal);
    SicboController.Instance.stopSfx(SicboSound.SfxJackpotScale);
  }

  private setOtherWinnerActive(active: boolean) {
    this.otherWinnerNameLabel.node.parent.active = active;
    this.jackpotStandValueLabel.node.active = !active;
  }

  private playJackpotStandAnimation(isIdle: boolean) {
    let anim = "no_hu";
    if (isIdle) {
      anim = "idle";
    }

    if (this.jackPotStandPlayingAnim == anim) return;
    this.jackPotStandPlayingAnim = anim;
    SicboSkeletonUtils.setAnimation(this.jackPotStandSkeleton, anim, true);
  }

  onDestroy() {
    if (this.node != null) cc.Tween.stopAllByTarget(this.node);
  }
}
