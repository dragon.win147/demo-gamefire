import SicboDropDownItemComp from "./SicboBaseDropDownItemComp";
import SicboDropDownOptionDataComp from "./SicboBaseDropDownOptionDataComp";

import Vec3 = cc.Vec3;

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboBaseDropDownComp extends cc.Component {
  private view = [];
  private loadObject: boolean = false;
  private template: cc.Node = null;
  private labelCaption: cc.Label = null;
  private spriteCaption: cc.Sprite = null;
  private labelItem: cc.Label = null;
  private spriteItem: cc.Sprite = null;
  protected arrow: cc.Node = null
  protected blackCover: cc.Node = null
  protected dropDownBackGround: cc.Node;
  // extra thing you wanna do when Show()
  protected extraShow: Function = () => { }
  protected extraHide: Function = () => { }
  protected popup: cc.Node;

  protected clickFunc: [] = [];

  public optionDatas: SicboDropDownOptionDataComp[] = [];

  private _dropDown: cc.Node;
  private validTemplate: boolean = false;
  private items: SicboDropDownItemComp[] = [];
  private isShow: boolean = false;

  private _selectedIndex: number = -1;
  private get selectedIndex(): number {
    return this._selectedIndex;
  }
  private set selectedIndex(value: number) {
    this._selectedIndex = value;
    this.refreshShownValue();
  }

  public addOptionDatas(optionDatas: SicboDropDownOptionDataComp[]) {
    optionDatas &&
      optionDatas.forEach((data) => {
        this.optionDatas.push(data);
      });
    this.refreshShownValue();
  }

  public clearOptionDatas() {
    cc.js.clear(this.optionDatas);
    this.refreshShownValue();
  }

  public resetSelectIndex() {
    this.selectedIndex = 0
  }

  public setSelectIndex(int: number) {
    this.selectedIndex = int
  }
  public show() {
    this.blackCover.parent = this.node.parent;
    this.blackCover.width = 4000;
    this.blackCover.height = 4000;
    this.blackCover.setPosition(new Vec3(0, 0, 0));
    this.blackCover.active = true;
    this.blackCover.setSiblingIndex(this.node.parent.childrenCount - 2)
    if (!this.validTemplate) {
      this.setUpTemplate();
      if (!this.validTemplate) {
        return;
      }
    }
    this.isShow = true;
    this._dropDown = this.createDropDownList(this.template);
    this._dropDown.name = "DropDownList";
    this._dropDown.active = true;
    this._dropDown.setParent(this.template.parent);
    this._dropDown.setSiblingIndex(0)
    this.extraShow();

    let itemTemplate = this._dropDown.getComponentInChildren<SicboDropDownItemComp>(SicboDropDownItemComp);
    let content = itemTemplate.node.parent;
    itemTemplate.node.active = true;

    cc.js.clear(this.items);

    for (let i = 0, len = this.optionDatas.length; i < len; i++) {
      let data = this.optionDatas[i];
      let item: SicboDropDownItemComp = this.addItem(data, i == this.selectedIndex, itemTemplate, this.items);
      if (!item) {
        continue;
      }
      item.toggle.isChecked = i == this.selectedIndex;
      item.toggle.node.on("toggle", this.onSelectedItem, this);
      // if(i == this.selectedIndex){
      //     this.onSelectedItem(item.toggle);
      // }
    }
    itemTemplate.node.active = false;

    content.height = itemTemplate.node.height * this.optionDatas.length;
  }

  private addItem(data: SicboDropDownOptionDataComp, selected: boolean, itemTemplate: SicboDropDownItemComp, DropDownItemComps: SicboDropDownItemComp[]): SicboDropDownItemComp {
    let item = this.createItem(itemTemplate);
    item.node.setParent(itemTemplate.node.parent);
    item.node.active = true;
    item.node.name = `item_${this.items.length + data.optionString ? data.optionString : ""}`;
    if (item.toggle) {
      item.toggle.isChecked = false;
    }
    if (item.label) {
      item.label.string = data.optionString;
    }
    if (item.sprite) {
      item.sprite.spriteFrame = data.optionSf;
      item.sprite.enabled = data.optionSf != undefined;
    }
    this.items.push(item);
    return item;
  }

  public hide() {
    this.blackCover.parent = this.node;
    this.blackCover.active = false;
    this.extraHide();
    this.isShow = false;
    if (this._dropDown != undefined) {
      this.delayedDestroyDropdownList(0.15);
    }
  }

  private async delayedDestroyDropdownList(delay: number) {
    // await WaitUtil.waitForSeconds(delay);
    // wait delay;
    for (let i = 0, len = this.items.length; i < len; i++) {
      if (this.items[i] != undefined) this.destroyItem(this.items[i]);
    }
    cc.js.clear(this.items);
    if (this._dropDown != undefined) this.destroyDropDownList(this._dropDown);
    this._dropDown = undefined;
  }

  private destroyItem(item) { }

  // 设置模板，方便后面item
  private setUpTemplate() {
    this.validTemplate = false;

    if (!this.template) {
      cc.error("The dropdown template is not assigned. The template needs to be assigned and must have a child GameObject with a Toggle component serving as the item");
      return;
    }
    this.template.active = true;
    let itemToggle: cc.Toggle = this.template.getComponentInChildren<cc.Toggle>(cc.Toggle);
    this.validTemplate = true;
    // 一些判断
    if (!itemToggle || itemToggle.node == this.template) {
      this.validTemplate = false;
      cc.error("The dropdown template is not valid. The template must have a child Node with a Toggle component serving as the item.");
    } else if (this.labelItem != undefined && !this.labelItem.node.isChildOf(itemToggle.node)) {
      this.validTemplate = false;
      cc.error("The dropdown template is not valid. The Item Label must be on the item Node or children of it.");
    } else if (this.spriteItem != undefined && !this.spriteItem.node.isChildOf(itemToggle.node)) {
      this.validTemplate = false;
      cc.error("The dropdown template is not valid. The Item Sprite must be on the item Node or children of it.");
    }

    if (!this.validTemplate) {
      this.template.active = false;
      return;
    }
    let item = itemToggle.node.addComponent<SicboDropDownItemComp>(SicboDropDownItemComp);
    item.label = this.labelItem;
    item.sprite = this.spriteItem;
    item.toggle = itemToggle;
    item.node = itemToggle.node;

    this.template.active = false;
    this.validTemplate = true;
  }

  // 刷新显示的选中信息
  private refreshShownValue() {
    if (this.optionDatas.length <= 0) {
      return;
    }
    let data = this.optionDatas[this.clamp(this.selectedIndex, 0, this.optionDatas.length - 1)];
    if (this.labelCaption) {
      if (data && data.optionString) {
        this.labelCaption.string = data.optionString;
      } else {
        this.labelCaption.string = "";
      }
    }
    if (this.spriteCaption) {
      if (data && data.optionSf) {
        this.spriteCaption.spriteFrame = data.optionSf;
      } else {
        this.spriteCaption.spriteFrame = undefined;
      }
      this.spriteCaption.enabled = this.spriteCaption.spriteFrame != undefined;
    }
  }

  protected createDropDownList(template: cc.Node): cc.Node {
    return cc.instantiate(template);
  }

  protected destroyDropDownList(dropDownList: cc.Node) {
    dropDownList.destroy();
  }

  protected createItem(itemTemplate: SicboDropDownItemComp): SicboDropDownItemComp {
    let newItem = cc.instantiate(itemTemplate.node);
    return newItem.getComponent<SicboDropDownItemComp>(SicboDropDownItemComp);
  }

  /** 当toggle被选中 */
  private onSelectedItem(toggle: cc.Toggle) {
    let parent = toggle.node.parent;
    for (let i = 0; i < parent.childrenCount; i++) {
      if (parent.children[i] == toggle.node) {
        // Subtract one to account for template child.
        this.optionDatas[i - 1]?.clickFunc(i - 1);
        this.selectedIndex = i - 1;
        break;
      }
    }
    this.hide();
  }

  private onClick() {
    if (!this.isShow) {
      this.show();
    } else {
      this.hide();
    }
  }

  onLoad() {
    if (this.loadObject == false) {
      this.view = [];
      this.load_all_object(this.node, "");
    }
    this.template = this.view["Template"];
    this.labelCaption = this.view["LabelCaption"].getComponent(cc.Label);
    this.spriteCaption = this.view["SpriteCaption"].getComponent(cc.Sprite);
    this.labelItem = this.view["Template/view/content/item/Label"].getComponent(cc.Label);
    this.spriteItem = this.view["Template/view/content/item/Sprite"].getComponent(cc.Sprite);
    this.blackCover = this.view["BlackCover"]
    this.arrow = this.view["Arrow"]
    this.popup = cc.find("UIManager/Popup");
    var blackCoverEvent = new cc.Component.EventHandler();
    blackCoverEvent.target = this.node; // This node is the node to which your event handler code component belongs
    blackCoverEvent.component = "SicboBaseDropDownComp"; // This is the code file name
    blackCoverEvent.handler = "clickBlackCover";
    this.dropDownBackGround = this.view["Background"]
    this.blackCover.getComponent(cc.Button).clickEvents.push(blackCoverEvent)
    // let dropDownBackGroundEvent = new cc.Component.EventHandler();
    // dropDownBackGroundEvent.target = this.node; // This node is the node to which your event handler code component belongs
    // dropDownBackGroundEvent.component = "BaseDropDownComp"; // This is the code file name
    // dropDownBackGroundEvent.handler = "onClick";
    // this.dropDownBackGround.addComponent(cc.Button).clickEvents.push(dropDownBackGroundEvent)
  }
  load_all_object(root, path) {
    for (let i = 0; i < root.childrenCount; i++) {
      this.view[path + root.children[i].name] = root.children[i];
      this.load_all_object(root.children[i], path + root.children[i].name + "/");
    }
  }

  start() {
    this.template.active = false;
    this.refreshShownValue();
  }

  onEnable() {
    this.node.on("touchend", this.onClick, this);
  }

  onDisable() {
    this.node.off("touchend", this.onClick, this);
  }

  private clamp(value: number, min: number, max: number): number {
    if (value < min) return min;
    if (value > max) return max;
    return value;
  }

  private clickBlackCover() {
    console.log(`clickBtn`)
    this.hide();
  }
}
