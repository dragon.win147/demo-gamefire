const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboCutOffComp extends cc.Component {
    @property({ type: cc.Node })
    dropDown: cc.Node = null
    @property({ type: cc.SpriteFrame })
    day: cc.SpriteFrame = null
    @property({ type: cc.SpriteFrame })
    week: cc.SpriteFrame = null
    @property({ type: cc.SpriteFrame })
    month: cc.SpriteFrame = null
    @property({ type: cc.SpriteFrame })
    lifeTime: cc.SpriteFrame = null
}
