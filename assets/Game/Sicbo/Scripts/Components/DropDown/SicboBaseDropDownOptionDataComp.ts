const { ccclass, property } = cc._decorator;

export default class SicboBaseDropDownOptionDataComp {
  constructor(obj: { optionString, optionSf, clickFunc }) {
    const { optionString, optionSf, clickFunc } = obj;
    this.optionString = optionString
    this.optionSf = optionSf
    this.clickFunc = clickFunc
  }
  public optionString: string = "";
  public optionSf: cc.SpriteFrame = null;

  clickFunc: Function = null
}
