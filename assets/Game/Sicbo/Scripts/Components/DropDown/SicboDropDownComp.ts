import SicboBaseDropDownComp from "./SicboBaseDropDownComp";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboSicboDropDownComp extends SicboBaseDropDownComp {
    // you can add extra setting to dropDown here
    onLoad(): void {
        super.onLoad();
        this.extraShow = () => {
            this.arrow.angle = 180;
        };
        this.extraHide = () => {
            this.arrow.angle = 0
        };
    }
}
