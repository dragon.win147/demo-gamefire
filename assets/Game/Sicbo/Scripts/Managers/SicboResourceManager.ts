

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboResourceManager {
  static addressResources: string = "";

  public setAddressResources(addressResources: string){
    SicboResourceManager.addressResources = addressResources;
  }

  public static loadSpineFromForgeAsObj(rf: any, onComplete: (result: sp.SkeletonData) => void) {
    let image = "";
    let json = "";
    let atlas = "";
    let namePng = "";

    rf.filesList.forEach((file) => {
      switch (file.type) {
        case "image/png":
          image = file.path;
          namePng = file.name;
          break;
        case "application/json":
          json = file.path;
          break;
        case "text/plain; charset=utf-8":
          atlas = file.path;
          break;
      }
    });

    image = SicboResourceManager.addressResources + image;
    json = SicboResourceManager.addressResources + json;
    atlas = SicboResourceManager.addressResources + atlas;

    this.loadRemoteSpine(image, json, atlas, namePng, onComplete);
  }

  public static loadRemoteSpine(image: string, json: string, atlas: string, namePNG: string, onComplete: (result: sp.SkeletonData) => void) {
    cc.assetManager.loadRemote(image, (error, texture) => {
      cc.assetManager.loadAny({ url: atlas, type: "txt" }, (error, atlasJson) => {
        cc.assetManager.loadAny({ url: json, type: "txt" }, (error, spineJson) => {
          if (error) {
            //cc.log(error);
            return;
          }
          var asset = new sp.SkeletonData();
          asset.skeletonJson = spineJson;
          asset.atlasText = atlasJson;
          asset.textures = [texture];
          asset.textureNames = [namePNG];
          if (onComplete) onComplete(asset);
        });
      });
    });
  }


  public static loadRemoteImageWithFileField(spr: cc.Sprite, fileField: any, onComplete: () => void = null) {
    if(spr)
      spr.spriteFrame = null;
    this.loadRemoteImageByFileFieldAsObj(fileField, (spriteFrame) => {
      if (spr) {
        spr.spriteFrame = spriteFrame;
        if (onComplete) onComplete();
      }
    });
  }

  public static loadRemoteImageByFileFieldAsObj(fileField: any, onComplete: (result) => void) {
    if (fileField) {
      let remoteUrl = SicboResourceManager.addressResources + fileField.path;
      cc.assetManager.loadRemote(remoteUrl, function (err, result) {
        ////cc.log("url: "+remoteUrl);
        if (err) {
          //cc.error(err);
          if (onComplete) onComplete(null);
          return;
        }
        let res = new cc.SpriteFrame(result);
        if (onComplete) onComplete(res);
      });
    } else {
      if (onComplete) onComplete(null);
    }
  }

  
  public static loadRemoteImageWithPath(spr: cc.Sprite, path: string, onComplete: () => void = null, resetFrame: boolean = false) {
    if (resetFrame) spr.spriteFrame = null;

    this.loadImageFromForge(path, (spriteFrame) => {
      if (spr) {
        spr.spriteFrame = spriteFrame;
        if (onComplete) onComplete();
      }
    });
  }

  public static loadImageFromForge(path: string, onComplete: (result) => void) {
    if (path == "" || path == "undefined" || path === undefined) {
      if (onComplete) onComplete(null);
      return;
    }
    let remoteUrl = SicboResourceManager.addressResources + path;
    cc.assetManager.loadRemote(remoteUrl, function (err, result) {
      ////cc.log("url: "+remoteUrl);
      if (err) {
        ///LogManager.error(err);
        if (onComplete) onComplete(null);
        return;
      }
      let res = new cc.SpriteFrame(result);
      if (onComplete) onComplete(res);
    });
  }

  public static preloadScene(nameScene: string, onProgress: (finish: number, total: number, item: cc.AssetManager.RequestItem) => void = null, onComplete: () => void = null) {
    cc.director.preloadScene(nameScene, onProgress, function () {
      if (cc.sys.isNative) {
        // cc.loader.releaseAll();
      }
      if (onComplete) onComplete();
    });
  }

  public static preloadResourceLocal(
    paths: string[],
    onProgress: (finish: number, total: number, item: cc.AssetManager.RequestItem) => void = null,
    onComplete: () => void = null
  ) {
    if (paths.length == 0) {
      if (onComplete) onComplete();
      return;
    }
    cc.resources.preload(paths, onProgress, function (err, items) {
      if (err) {
        //cc.error(err);
        if (onComplete) onComplete();
        return;
      }
      if (onComplete) onComplete();
    });
  }

  public static preloadResourceRemote(
    paths: string[],
    onProgress: (finish: number, total: number, item: cc.AssetManager.RequestItem) => void = null,
    onComplete: () => void = null
  ) {
    if (paths.length == 0) {
      if (onComplete) onComplete();
      return;
    }
    let finish = 0;
    let total = paths.length;
    paths.forEach((path) => {
      path = SicboResourceManager.addressResources + path;
      cc.assetManager.loadRemote(path, function (err, item) {
        if (err) {
          //cc.error(err);
        }
        finish++;
        if (onProgress) onProgress(finish, total, item);
        if (finish == total) {
          if (onComplete) onComplete();
        }
      });
    });
  }

  public static loadScene(sceneName: string, callback: () => void = null) {
    // SoundManager.getInstance().stopAllEffects();
    cc.director.loadScene(sceneName, function (err, asset) {
      if (err) {
        //cc.error(err);
        //TODO : handle error load scene : suggest user reload page and force reload
        // GameUtils.showPopupNotice({ title: i18n.t("common.notice"), text: i18n.t("common.reloadPage") }, function () {
        //   window.location.reload(true);
        // });
      }
      if (callback) callback();
    });
  }
}
