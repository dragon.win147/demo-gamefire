// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboPopUpInstance from "./SicboPopUpInstance";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboUIManager extends cc.Component {
  private static _instance: SicboUIManager = null;

  public static SicboInforPopup: string = "SicboInforPopup";
  public static SicboCannotJoinPopup: string = "SicboCannotJoinPopup";

  @property(cc.Node)
  popupRoot: cc.Node = null;

  @property([cc.Prefab])
  popupPrefabs: cc.Prefab[] = [];

  private popupMap: Map<string, cc.Prefab> = new Map<string, cc.Prefab>();

  private _popupInstances: Map<string, SicboPopUpInstance> = new Map<string, SicboPopUpInstance>();
  private _currentPopup: SicboPopUpInstance;
  private _currentPopupName: string;

  static getInstance(): SicboUIManager {
    if (SicboUIManager._instance == null) {
      let canvas = cc.director.getScene().getChildByName("Canvas");
      let node = canvas.getChildByName("SicboUIManager");
      SicboUIManager._instance = node.getComponent(SicboUIManager);
    }
    return SicboUIManager._instance;
  }

  onLoad() {
    this.init();
  }

  init() {
    this.popupPrefabs.forEach((popupPrefab) => {
      if(popupPrefab){
        this.popupMap.set(popupPrefab.name, popupPrefab);
      }
    });
  }

  public openPopup(type: typeof SicboPopUpInstance, name: string, data = null, onComplete: (instance: SicboPopUpInstance) => void = null, parent: cc.Node = null) {
    let self = SicboUIManager._instance;
    parent = parent ? parent : self.popupRoot;
    if (name == self._currentPopupName && self._currentPopup.node) {
      if (onComplete) onComplete(self._currentPopup);
      return;
    }
    self._currentPopupName = name;
    self.getPopupInstance(
      type,
      name,
      (instance) => {
        if (self._currentPopup == instance) {
          if (onComplete) onComplete(self._currentPopup);
          return;
        }
        self._currentPopup = instance;
        instance.open(data);
        instance._close = () => {
          self._currentPopup = null;
          self._currentPopupName = "";
        };
        if (onComplete) onComplete(self._currentPopup);
      },
      parent
    );
  }

  public hidePopup(name: string) {
    let self = SicboUIManager._instance;
    self._popupInstances.get(name)?.hidePopup();
  }

  public getPopupInstance(type: typeof SicboPopUpInstance, name: string, onComplete: (instance: SicboPopUpInstance) => void, parent: cc.Node = null) {
    let self = SicboUIManager._instance;
    const _parent = parent == null ? this.node : parent;
    if (self._popupInstances.has(name)) {
      let popup = self._popupInstances.get(name);
      popup.node.parent = null;
      _parent.addChild(popup.node);
      onComplete(popup);
    } else {
      this.loadPopUpPrefab(type, name, onComplete, _parent);
    }
  }

  private loadPopUpPrefab(type: typeof SicboPopUpInstance, name: string, onComplete: (instance: SicboPopUpInstance) => void, parent: cc.Node = null) {
    let self = SicboUIManager._instance;
    let prefab = this.popupMap.get(name);
    const newNode = cc.instantiate(prefab) as unknown as cc.Node;
    const instance = newNode.getComponent(type);
    parent.addChild(newNode);
    self._popupInstances.set(name, instance);
    onComplete(instance);
  }

  onDestroy() {
    delete SicboUIManager._instance;
    SicboUIManager._instance = null;
  }
}
