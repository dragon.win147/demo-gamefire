// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import SicboModuleAdapter from "../..../../../../../SicboModuleAdapter";

const {
  EventKey,
} = SicboModuleAdapter.getAllRefs();
import SicboLocalStorageManager from "./SicboLocalStorageManager";
import SicboSoundItem from "./SicboSoundItem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboSoundManager extends cc.Component {
  private static instance: SicboSoundManager = null;
  soundMap: Map<string, SicboSoundItem> = new Map<string, SicboSoundItem>();


  private KEY_EFFECT_VOLUME: string = EventKey.KEY_EFFECT_VOLUME;
  private KEY_EFFECT: string = EventKey.KEY_EFFECT;
  private KEY_MUSIC_VOLUME: string = EventKey.KEY_MUSIC_VOLUME;
  private KEY_MUSIC: string = EventKey.KEY_MUSIC;
  
  private isEnableMusic: boolean = true;
  private isEnableSfx: boolean = true;
  private musicVolume: number = 1;
  private sfxVolume: number = 1;

  static getInstance(): SicboSoundManager {
    if (SicboSoundManager.instance == null) {
      let canvas = cc.director.getScene().getChildByName("Canvas");
      let node = canvas.getChildByName("SicboSoundManager");
      SicboSoundManager.instance = node.getComponent(SicboSoundManager);
    }
    return SicboSoundManager.instance;
  }

  onLoad(){
    this.init();
    this.loadConfigFromFromStorage();
  }

  init(){
    let sounds = this.node.getComponentsInChildren(SicboSoundItem);
    for (let i = 0; i < sounds.length; i++) {
      this.soundMap.set(sounds[i].node.name, sounds[i]);
    }
  }

  play(soundName: string, from: number = 0, autoStopDelay: number = -1){
    if(this.soundMap.has(soundName)){
      this.soundMap.get(soundName).play(from, autoStopDelay);
    }
  }

  stop(soundName: string){
    if(this.soundMap.has(soundName)){
      this.soundMap.get(soundName).stop();
    }
  }

  private loadConfigFromFromStorage() {
    this.isEnableMusic = this.getMusicStatusFromStorage();
    this.isEnableSfx = this.getSfxStatusFromStorage();
    this.musicVolume = this.getMusicVolumeFromStorage();
    this.sfxVolume = this.getSfxVolumeFromStorage();

    this.setMusicVolume(this.musicVolume);
    this.setSfxVolume(this.sfxVolume);
  }

  setMusicStatus(isOn: boolean){
    this.isEnableMusic = isOn;
    cc.audioEngine.setMusicVolume(this.MusicVolume);
    this.saveMusicStatusToStorage(isOn);
    cc.systemEvent.emit(EventKey.ON_SOUND_BG_EVENT, isOn);
  }

  setMusicVolume(volume: number, isSave: boolean = true){
    this.musicVolume = volume;
    cc.audioEngine.setMusicVolume(volume);
    if(!isSave) return; 
    
    this.saveMusicVolumeToStorage(volume);
    let isOn = volume > 0;
    this.saveMusicStatusToStorage(isOn);
    cc.systemEvent.emit(EventKey.ON_SOUND_BG_EVENT, isOn);
  }

  get MusicVolume(){
    return this.isEnableMusic? this.musicVolume: 0;
  }

  setSfxVolume(volume: number, isSave: boolean = true){
    this.sfxVolume = volume;
    cc.audioEngine.setEffectsVolume(volume);
    if(!isSave) return;
    
    this.saveSfxVolumeToStorage(volume);
    let isOn = this.sfxVolume > 0;
    this.saveSfxStatusToStorage(isOn);
    cc.systemEvent.emit(EventKey.ON_SOUND_EFF_EVENT, isOn);
  }

  setSfxStatus(isOn: boolean){
    this.isEnableSfx = isOn;
    cc.audioEngine.setEffectsVolume(this.SfxVolume);
    this.saveSfxStatusToStorage(isOn);
    cc.systemEvent.emit(EventKey.ON_SOUND_EFF_EVENT, isOn);
  }

  get SfxVolume(){
    return this.isEnableSfx? this.sfxVolume: 0;
  }

  //SECTION Storage
  getMusicStatusFromStorage(): boolean {
    return SicboLocalStorageManager.internalGetBoolean(this.KEY_MUSIC, true);
  }

  saveMusicStatusToStorage(isOn: boolean){
    SicboLocalStorageManager.internalSaveBoolean(this.KEY_MUSIC, isOn);
  }

  getMusicVolumeFromStorage(): number {
    return Number.parseFloat(SicboLocalStorageManager.internalGetString(this.KEY_MUSIC_VOLUME, "1"));
  }

  saveMusicVolumeToStorage(volume: number): void {
    SicboLocalStorageManager.internalSaveString(this.KEY_MUSIC_VOLUME, volume.toString());
  }

  getSfxStatusFromStorage(): boolean {
    return SicboLocalStorageManager.internalGetBoolean(this.KEY_EFFECT, true);
  }

  saveSfxStatusToStorage(isOn: boolean){
    SicboLocalStorageManager.internalSaveBoolean(this.KEY_EFFECT, isOn);
  }

  getSfxVolumeFromStorage(): number {
    return Number.parseFloat(SicboLocalStorageManager.internalGetString(this.KEY_EFFECT_VOLUME, "1"));
  }
  
  saveSfxVolumeToStorage(volume: number): void {
    SicboLocalStorageManager.internalSaveString(this.KEY_EFFECT_VOLUME, volume.toString());
  }
  //!SECTION

  
  public pauseAllEffects(): void {
    cc.audioEngine.pauseAllEffects();
  }

  public resumeAllEffects(): void {
    cc.audioEngine.resumeAllEffects();
  }
  
  onDestroy(){
    cc.audioEngine.stopAllEffects();
    
    delete SicboSoundManager.instance;
    SicboSoundManager.instance = null;
  }
}
