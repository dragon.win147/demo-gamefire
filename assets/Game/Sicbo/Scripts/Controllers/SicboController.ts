import SicboModuleAdapter from "../../../../SicboModuleAdapter";

const {
  Code,
  Topic,
  State,
  Event,
  DoorAndBetAmount,
  Status,

  WhatAppMessage,
  EventKey,
  AutoJoinReply,
} = SicboModuleAdapter.getAllRefs();

import SicboUIComp from "../Components/SicboUIComp";
import SicboHelper from "../Helpers/SicboHelper";

import SicboMessageHandler from "../SicboMessageHandler";
import SicboSetting, { SicboSound } from "../Setting/SicboSetting";
import SicboSoundManager from "../Managers/SicboSoundManager";
import SicboPortalAdapter from "../SicboPortalAdapter";
import { SicboCoinType } from "../RNGCommons/SicboCoinType";
import SicboHandleEventShowHideComp from "../RNGCommons/SicboHandleEventShowHideComp";
import SicboTracking from "../SicboTracking";
import SicboUIManager from "../Managers/SicboUIManager";

const { ccclass } = cc._decorator;

@ccclass
export default class SicboController extends cc.Component {
  private static instance: SicboController = null;
  static get Instance(): SicboController {
    return SicboController.instance;
  }

  private stateStatus = Status.STATUS_UNSPECIFIED;
  private isKicked: boolean = false;
  private handleEventShowHideComp: SicboHandleEventShowHideComp = null;
  private lastTickTime = -100;
  private lastSession = 0;
  onLoad() {
    SicboController.instance = this;

    if (!SicboController.Instance.handleEventShowHideComp) SicboController.Instance.handleEventShowHideComp = SicboController.Instance.addComponent(SicboHandleEventShowHideComp); //Device home button
    SicboController.Instance.handleEventShowHideComp.listenEvent(
      () => SicboController.Instance.onHideEvent(),
      () => SicboController.Instance.onShowEvent()
    );

    cc.systemEvent.on(
      EventKey.ON_PING_ERROR,
      (code, isRetry, messError) => {
        SicboMessageHandler.IS_WS_DISCONNECTED = true;
        SicboController.Instance.isDisconnected = true;
        SicboController.Instance.onHideEvent();
        //cc.warn("EventKey.ON_DISCONNECT_EVENT", code, isRetry, messError)
      },
      this
    );
    cc.systemEvent.on(
      EventKey.ON_RECONNECTED_EVENT,
      () => {
        SicboMessageHandler.IS_WS_DISCONNECTED = false;
        SicboController.Instance.isDisconnected = false;

        SicboUIManager.getInstance().hidePopup(SicboUIManager.SicboCannotJoinPopup);
        SicboController.Instance.onShowEvent();
      },
      this
    );
  }

  start() {
    SicboMessageHandler.getInstance().receiveState = SicboController.Instance.onReceiveState;
    SicboMessageHandler.getInstance().onErrorCustom = SicboController.Instance.onError;

    cc.systemEvent.on(EventKey.ON_ADD_BALANCE_EVENT, SicboController.Instance.onBalanceAdded);
    cc.systemEvent.on(EventKey.ON_UPDATE_BALANCE_EVENT, SicboController.Instance.onBalanceUpdated);

    SicboPortalAdapter.getInstance().getLatestBalance();

    SicboController.Instance.onBalanceUpdated();

    SicboController.Instance.playBgm();

    // SicboPortalAdapter.getInstance().getBundleInfo(bundleInfo=>{
    //   SicboTracking.setGameVersion(bundleInfo?.version);
    // })
  }

  onBalanceUpdated() {
    SicboPortalAdapter.getInstance().getCurrentBalance((balance) => {
      SicboUIComp.Instance.changeMyUserBalance(balance);
    });
  }

  onBalanceAdded() {
    SicboPortalAdapter.getInstance().getCurrentBalance((balance) => {
      SicboUIComp.Instance.changeMyUserBalance(balance);
    });
  }

  onEnable() {
    SicboPortalAdapter.getInstance().enableWallet();

    let chanelID = SicboMessageHandler.getInstance().getChannelId();

    this.handleNotEnoughMoneyOnJointGame(Math.min(...SicboSetting.getChipLevelList()));
  }

  onDisable() {
    SicboPortalAdapter.getInstance().disableWallet();
  }

  onDestroy() {
    cc.systemEvent.off(EventKey.ON_ADD_BALANCE_EVENT, SicboController.Instance.onBalanceAdded);
    cc.systemEvent.off(EventKey.ON_UPDATE_BALANCE_EVENT, SicboController.Instance.onBalanceUpdated);
    SicboMessageHandler.getInstance().onDestroy();
    SicboPortalAdapter.destroy();

    SicboController.Instance.unscheduleAllCallbacks();
    delete SicboController.instance;
    SicboController.instance = null;
  }

  appHiding: boolean = false;
  appResuming: boolean = false;
  isDisconnected: boolean = false;

  onHideEvent() {
    //cc.log("SICBO onHideEvent");
    SicboUIComp.Instance.onHide();
    SicboController.Instance.appHiding = true;
    SicboController.Instance.appResuming = false;
  }

  onShowEvent() {
    //cc.log("SICBO onShowEvent");
    SicboController.Instance.stateStatus = Status.STATUS_UNSPECIFIED;
    SicboController.Instance.appHiding = false;
    SicboController.Instance.appResuming = true;

    if (SicboController.Instance.isKicked) return;

    SicboPortalAdapter.getInstance().getLatestBalance();

    SicboMessageHandler.getInstance().onClearAllMsg();
    SicboMessageHandler.getInstance().sendAutoJoinRequest(() => {
      SicboController.Instance.onBalanceUpdated();
    });
  }

  //ANCHOR My User Wallet
  addMyUserMoney(amount: number, transaction: number) {
    SicboPortalAdapter.getInstance().addMoney(amount, transaction);
  }

  //SECTION Send message
  sendCheckServerAvailable() {
    //Work around for checking server available
    SicboMessageHandler.getInstance().sendHealthCheck((res) => {});
  }

  sendBet(door, coinType: SicboCoinType, onSuccess: () => void = null) {
    let self = this;
    let coinNumber = SicboHelper.convertCoinTypeToValue(coinType);
    self.checkWalletLocalForBetting(coinNumber, (enoughMoney) => {
      if (enoughMoney) {
        SicboMessageHandler.getInstance().sendBetRequest(door, coinNumber, (res) => {
          self.addMyUserMoney(res.getTx().getAmount(), res.getTx().getLastTxId());
          onSuccess && onSuccess();
        });
      } else {
        cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, Topic.SICBO, coinNumber);
        // self.onError(Code.SICBO_BET_NOT_ENOUGH_MONEY, "");
      }
    });
  }

  sendReBet(onSuccess: (dabs: InstanceType<typeof DoorAndBetAmount>[]) => void = null) {
    SicboMessageHandler.getInstance().sendRebetRequest((res) => {
      SicboController.Instance.addMyUserMoney(res.getTx().getAmount(), res.getTx().getLastTxId());
      onSuccess && onSuccess(res.getDoorsAndBetAmountsList());
    });
  }

  sendBetDouble(onSuccess: (dabs: InstanceType<typeof DoorAndBetAmount>[]) => void = null) {
    SicboMessageHandler.getInstance().sendBetDoubleRequest((res) => {
      SicboController.Instance.addMyUserMoney(res.getTx().getAmount(), res.getTx().getLastTxId());
      onSuccess && onSuccess(res.getDoorsAndBetAmountsList());
    });
  }

  checkWalletLocalForBetting(amount: number, callback: (enoughMoney: boolean) => void) {
    SicboPortalAdapter.getInstance().getCurrentBalance((balance) => {
      callback && callback(amount <= balance);
    });
  }
  //!SECTION

  //SECTION Receive message from MessageHandler
  onError(errCode, err: string) {
    //cc.log("SICBO ERROR", errCode, err);
    SicboUIComp.Instance?.showErrMessage(errCode, err);
  }

  onReceiveState(state: InstanceType<typeof State>) {
    SicboSetting.setStatusDurationConfig(state.getSessionStatesTime());
    SicboUIComp.Instance.updateUsersInRoom(state.getDisplayPlayersList(), state.getRemainingPlayersCount());

    let myUserId = SicboPortalAdapter.getInstance().getMyUserId();
    state.getEventsList().forEach((e) => {
      let payloadCase = e.getPayloadCase();
      switch (payloadCase) {
        case Event.PayloadCase.BET:
          if (e.getUserId() == myUserId) {
            //cc.error("SICBO Event.PayloadCase.BET broadcast self userid");
          } else SicboUIComp.Instance.playUserBet(e.getUserId(), e.getBet().getDoor(), e.getBet().getChip());
          break;

        //TODO: xu ly kick, join, ... .
        case Event.PayloadCase.KICKED:
          if (e.hasKicked() && e.getUserId() == myUserId) {
            SicboController.Instance.isKicked = true;
          }
          break;
        default:
          break;
      }
    });

    if (SicboController.Instance.isKicked) return;
    if (SicboController.Instance.appHiding) return;
    if (SicboController.Instance.isDisconnected) return;
    // if(SicboController.Instance.appResuming){
    //   if(state.getStatus() == Status.PAYING){
    //     SicboMessageHandler.getInstance().sendLeaveRequest(() => {
    //       SicboController.Instance.leaveGame();
    //     });
    //   }
    // }

    if (SicboController.Instance.lastSession != state.getSessionId()) {
      SicboController.Instance.startNewSession(state.getSessionId());
      //cc.log("ZZZZZZ startNewSession", SicboController.Instance.stateStatus, SicboController.Instance.lastTickTime);
    }

    if ((SicboController.Instance.stateStatus != state.getStatus() && SicboController.Instance.isValidTick(state.getTickTime())) || SicboController.Instance.appResuming) {
      SicboController.Instance.exitState(SicboController.Instance.stateStatus);
      SicboController.Instance.startState(state, SicboSetting.getStatusDuration(state.getStatus()));
      SicboController.Instance.lastTickTime = state.getTickTime();
    }
    SicboController.Instance.stateStatus = state.getStatus();
    SicboController.Instance.updateState(state);

    SicboController.Instance.appResuming = false;
  }

  exitState(status) {
    SicboUIComp.Instance.exitState(status);
  }

  updateState(state: InstanceType<typeof State>) {
    SicboUIComp.Instance.updateState(state);
  }

  startState(state: InstanceType<typeof State>, stateDuration: number) {
    if (stateDuration == 0) {
      //cc.log("SKIP STATE DURATION = 0", state.getStatus().toString());
      return;
    }
    SicboUIComp.Instance.startState(state, stateDuration);
  }

  startNewSession(newSession) {
    SicboController.Instance.lastSession = newSession;
    SicboController.Instance.lastTickTime = -100;
    SicboTracking.startSession(newSession);
  }

  isValidTick(curTick): boolean {
    //cc.log("ZZZZZZ isValidTick", curTick, SicboController.Instance.lastTickTime)
    return true;
    // return curTick >= SicboController.Instance.lastTickTime;
  }
  //!SECTION

  //SECTION Sound
  playBgm() {
    SicboSoundManager.getInstance().play(SicboSetting.getSoundName(SicboSound.BGM));
  }

  playSfx(sound: SicboSound, loop: boolean = false) {
    SicboSoundManager.getInstance().play(SicboSetting.getSoundName(sound));
  }

  stopSfx(sound: SicboSound) {
    SicboSoundManager.getInstance().stop(SicboSetting.getSoundName(sound));
  }
  //!SECTION
  leaveGame() {
    // SicboPortalAdapter.getInstance().leaveGame();
    cc.director.loadScene("LoginScene");
  }

  getGameInfo() {
    // let games = SicboPortalAdapter.getInstance().getGameInfo() as any[];
    // let res: [] = [];
    // games.forEach((game) => {
    //   if (game.topic == Topic.SICBO) {
    //     res = game.info.filesList;
    //   }
    // });
    // return res;
  }

  handleNotEnoughMoneyOnJointGame(coinNumber: number) {
    this.checkWalletLocalForBetting(coinNumber, (enoughMoney) => {
      if (!enoughMoney) cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_JOINING_GAME, Topic.SICBO, coinNumber);
    });
  }
}
