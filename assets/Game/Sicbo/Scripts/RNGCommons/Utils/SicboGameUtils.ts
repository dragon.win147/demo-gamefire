import SicboNative from "../../SicboNative";
import SicboPortalAdapter from '../../SicboPortalAdapter';

export default class SicboGameUtils {
  p
  public static createClickBtnEvent(node: cc.Node, componentName: string, handlerEvent: string, custom: string = "") {
    const clickEventHandler = new cc.Component.EventHandler();
    clickEventHandler.target = node; //This node is the node to which your event handler code component belongs
    clickEventHandler.component = componentName; //This is the code file name
    clickEventHandler.handler = handlerEvent;
    clickEventHandler.customEventData = custom;
    return clickEventHandler;
  }

  public static convertToOtherNode(fromNode: cc.Node, targetNode: cc.Node, pos: cc.Vec3 = null) {
    let parent = fromNode;
    if (fromNode.parent) parent = fromNode.parent;
    let worldSpace = parent.convertToWorldSpaceAR(fromNode.position);
    if (pos) {
      worldSpace = fromNode.convertToWorldSpaceAR(pos);
    }
    return targetNode.convertToNodeSpaceAR(worldSpace);
  }

  public static convertToOtherNode2(fromNode: cc.Node, targetNode: cc.Node, pos: cc.Vec2 = null) {
    let parent = fromNode;
    if (fromNode.parent) parent = fromNode.parent;
    let worldSpace = parent.convertToWorldSpaceAR(fromNode.getPosition());
    if (pos) {
      worldSpace = fromNode.convertToWorldSpaceAR(pos);
    }
    return targetNode.convertToNodeSpaceAR(worldSpace);
  }

  public static rectContainsPoint(rect: cc.Rect, point: cc.Vec2) {
    const result = point.x >= rect.origin.x && point.x <= rect.origin.x + rect.size.width && point.y >= rect.origin.y && point.y <= rect.origin.y + rect.size.height;
    return result;
  }

  public static createItemFromNode<T>(item: new () => T, node: cc.Node, parentNode: cc.Node = null, insert: boolean = false): T {
    if (!node) return null;
    let parent = parentNode ? parentNode : node.parent;
    let newNode = cc.instantiate(node);

    if (insert) {
      parent.insertChild(newNode, 0);
    } else {
      parent.addChild(newNode);
    }
    let newItem = newNode.getComponent(item);
    newItem.node.active = true;
    node.active = false;
    return newItem;
  }

  public static cloneItemFromNode<T>(item: new () => T, node: cc.Node): T {
    if (!node) return null;
    let newNode = cc.instantiate(node);

    let newItem = newNode.getComponent(item);
    newItem.node.active = true;
    node.active = false;
    return newItem;
  }

  public static createItemFromPrefab<T>(item: new () => T, prefab: cc.Prefab, parentNode: cc.Node): T {
    if (!prefab) return null;
    let node = cc.instantiate(prefab);
    parentNode.addChild(node);
    let newItem = node.getComponent(item);
    newItem.node.active = true;
    return newItem;
  }

  public static createNodeFromPrefab<T>(prefab: cc.Prefab, parentNode: cc.Node): cc.Node {
    if (!prefab) return null;
    let node = cc.instantiate(prefab);
    parentNode.addChild(node);
    return node;
  }

  public static formatDate(date: Date, format: string): string {
    format = format.replace("%Y", date.getFullYear().toString());
    format = format.replace("%m", date.getMonth().toString().length < 2 ? "0" + date.getMonth().toString() : date.getMonth().toString());
    format = format.replace("%d", date.getDay().toString().length < 2 ? "0" + date.getDay().toString() : date.getDay().toString());
    format = format.replace("%h", date.getHours().toString().length < 2 ? "0" + date.getHours().toString() : date.getHours().toString());
    format = format.replace("%m", date.getMinutes().toString().length < 2 ? "0" + date.getMinutes().toString() : date.getMinutes().toString());
    format = format.replace("%s", date.getSeconds().toString().length < 2 ? "0" + date.getSeconds().toString() : date.getSeconds().toString());
    return format;
  }

  public static formatMoneyNumberUser(money: number): string {
    let s = "$ " + this.formatMoneyNumber(money);
    return s;
  }
  public static formatMoneyNumber(money: number): string {
    let sign = 1;
    let value = money;
    if (money < 0) {
      sign = -1;
      value = value * -1;
    }

    var format = "";
    if (value >= 1000000000.0) {
      value /= 1000000000.0;
      format = "B";
    } else if (value >= 1000000.0) {
      value /= 1000000.0;
      format = "M";
    } else if (value >= 1000.0) {
      value /= 1000.0;
      format = "K";
    }

    value = (Math.floor(value * 100 + 0.00000001) / 100) * sign;
    return value + format;
  }

  public static numberWithCommasMoney(number) {
    let s = "$ " + this.numberWithCommas(number);
    return s;
  }

  public static numberWithCommas(number) {
    if (number) {
      var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
      result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      return result[1] !== "00" ? result.join(",") : result[0];
    }
    return "0";
  }

  public static numberWithCommasV2(number) {
    if (number) {
      var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
      result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");

      return result[1] !== "00" ? result.join(",") : result[0];
    }
    return "0";
  }

  public static numberWithDot(number) {
    if (number) {
      var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
      result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");

      return result[1] !== "00" ? result.join(",") : result[0];
    }
    return "0";
  }

  public static formatMoneyWithRange(money: number, shortenStart: number = 100000000): string {
    let rs = "";
    if (money >= shortenStart) rs = SicboGameUtils.formatMoneyNumber(money);
    else rs = SicboGameUtils.numberWithCommas(money);
    return rs;
  }

  public static roundNumber(num: number, roundMin: number = 1000): number {
    let temp = parseInt((num / roundMin).toString());
    if (num % roundMin != 0) temp += 1;
    return temp * roundMin;
  }

  public static getRandomInt(max: number, min: number = 0): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  public static randomInsideCircle(R: number = 1): cc.Vec2 {
    let r = R * Math.sqrt(Math.random());
    let theta = Math.random() * 2 * Math.PI;
    let x = r * Math.cos(theta);
    let y = r * Math.sin(theta);
    return new cc.Vec2(x, y);
  }

  
  public static playAnimOnce(skeleton: sp.Skeleton, toAnimation: string, backAnimation: string, onCompleted: Function = null) {
    if (skeleton == null || skeleton == undefined) return;
    // if (skeleton.getCurrent(0) != null && skeleton.getCurrent(0).animation.name == toAnimation) {
    //   return;
    // }
    skeleton.setCompleteListener((trackEntry: sp.spine.TrackEntry) => {
      if (backAnimation != "" && backAnimation != null) skeleton.setAnimation(0, backAnimation, true);
      if (trackEntry.animation && trackEntry.animation.name == toAnimation) onCompleted && onCompleted();
    });
    skeleton.setAnimation(0, toAnimation, false);
  }
  
  public static setContentLabelAutoSize(label: cc.Label, content: string) {
    if (content.length > 30) {
      label.node.width = (30 * label.fontSize) / 2;
      label.overflow = cc.Label.Overflow.RESIZE_HEIGHT;
    } else {
      label.overflow = cc.Label.Overflow.NONE;
    }
    label.string = content;
  }

  static FormatString(str: string, ...val: any[]) {
    for (let index = 0; index < val.length; index++) {
      str = str.replace(`{${index}}`, val[index].toString());
    }
    return str;
  }

  static setParentTemp(nameTempNode: string = "Temp", node: cc.Node, parentOfTemp: cc.Node, zIndex: number = 90) {
    let temp = parentOfTemp.getChildByName(nameTempNode);
    if (!temp) {
      temp = new cc.Node(nameTempNode);
      parentOfTemp.addChild(temp);
      temp.zIndex = zIndex;
      parentOfTemp.sortAllChildren();
    }
    let widget = node.getComponent(cc.Widget);
    if (widget) widget.enabled = false;

    let startPos = SicboGameUtils.convertToOtherNode(node.parent, temp, node.position);
    node.setParent(temp);
    node.position = startPos;
    node.zIndex = 0;
  }


  public static isPlatformIOS() {
    return cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD;
  }

  public static isBrowser() {
    return cc.sys.isBrowser;
  }

  public static isNative() {
    return cc.sys.isNative;
  }

  public static isLogVersion() {
    return SicboPortalAdapter.getInstance().isLogVersion();
  }

  public static isNullOrUndefined(object) {
    return object == null || object == undefined;
  }

  public static copyToClipboard(str: string) {
    switch (cc.sys.platform) {
      case cc.sys.ANDROID:
      case cc.sys.IPHONE:
      case cc.sys.IPAD:
        SicboNative.getInstance().call("copyToClipBoard", str);
        break;
      case cc.sys.DESKTOP_BROWSER:
      case cc.sys.MOBILE_BROWSER:
        const el = document.createElement("textarea");
        el.value = str;
        document.body.appendChild(el);
        el.select();
        document.execCommand("copy");
        document.body.removeChild(el);

        //cc.log("copy to clipboard");
        break;
    }
  }
}
