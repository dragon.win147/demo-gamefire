
export default class SicboSkeletonUtils {
  static setAnimation(skeleton: sp.Skeleton, animationName: string = "animation", loop: boolean = false, startTime: number = 0, timeScale: number = 1) {
    if (!skeleton) return;
    let state = skeleton.setAnimation(0, animationName, loop) as sp.spine.TrackEntry;
    if (state) state.animationStart = startTime;
    skeleton.timeScale = timeScale;
  }
}
