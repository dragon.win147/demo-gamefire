const { ccclass, property } = cc._decorator;
export enum SicboMoneyFormatType {
  None = 0, // 123456
  Common = 1, // 123.456
  Number = 2, // 10M
  Config = 3, // num / configConvert
  Dot = 4,
}

@ccclass
export default class SicboMoneyFormatComp extends cc.Component {
  formatLabel: cc.Label = null;

  @property({ type: cc.Enum(SicboMoneyFormatType) })
  formatType: SicboMoneyFormatType = SicboMoneyFormatType.Number;

  @property
  prefix: string = "";

  private _moneyValue: number = 0;

  get moneyValue() {
    return this._moneyValue;
  }

  set moneyValue(value) {
    this._moneyValue = value;
    this.doFormat();
  }

  onLoad() {
    this.formatLabel = this.getComponent(cc.Label);
    this.doFormat();
  }

  onDestroy() {
    if (this.t) this.t.stop();
  }

  setMoney(money, isPrefix = false) {
    if (isPrefix) {
      this.prefix = money > 0 ? "+" : "";
    }
    if (this.t) this.t.stop();
    this.moneyValue = money;
  }
  setColor(color) {
    this.node.color = color;
  }

  private t: cc.Tween;
  runMoneyTo(money: number, duration: number = 0.5) {
    if (money == this.moneyValue) return;
    var obj = { current: this.moneyValue };
    if (this.t) this.t.stop();
    this.t = cc
      .tween(obj)
      .to(
        duration,
        { current: money },
        {
          progress: (start, end, current, ratio) => {
            this.moneyValue = Math.floor(current);
            return start + (end - start) * ratio;
          },
        }
      )
      .call((current) => (this.moneyValue = money))
      .start();
  }

  doFormat() {
    if (this.formatLabel) {
      let formattedString = "";
      let moneyFormat = Math.abs(this.moneyValue);
      if (this.formatType === SicboMoneyFormatType.None) {
        formattedString = this.moneyValue.toString();
      } else if (this.formatType === SicboMoneyFormatType.Config) {
        //  formattedString = SicboGameUtils.formatByConfigConvert(moneyFormat);
      } else if (this.formatType === SicboMoneyFormatType.Number) {
        formattedString = SicboGameUtils.formatMoneyNumber(moneyFormat);
      } else if (this.formatType === SicboMoneyFormatType.Common) {
        formattedString = SicboGameUtils.numberWithCommas(moneyFormat);
      } else if (this.formatType === SicboMoneyFormatType.Dot) {
        formattedString = SicboGameUtils.numberWithDot(moneyFormat);
      }
      if (this.moneyValue < 0) formattedString = "-" + formattedString;
      this.formatLabel.string = this.prefix + formattedString; // this.moneyValue;
    }
  }
}

import SicboGameUtils from "./Utils/SicboGameUtils";

