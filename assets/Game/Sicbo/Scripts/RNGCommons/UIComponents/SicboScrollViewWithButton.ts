// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
const ScrollEvent = cc.ScrollView.EventType;
@ccclass
export default class SicboScrollViewWithButton extends cc.Component {
  scrollView:cc.ScrollView = null;

  @property(cc.Button)
  preButton: cc.Button = null;

  @property(cc.Button)
  nextButton: cc.Button = null;

  @property()
  leftThreshold: number = 1;

  @property()
  rightThreshold: number = 2;

  onLoad(){        
      this.scrollView = this.getComponent(cc.ScrollView);
      this.init();
  } 

  //SECTION Scroll Pre/next button
  init() {
    var scrollViewEventHandler = new cc.Component.EventHandler();
    scrollViewEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
    scrollViewEventHandler.component = "SicboScrollViewWithButton"; // This is the code file name
    scrollViewEventHandler.handler = "callback";
    // scrollViewEventHandler.customEventData = "";    

    this.scrollView.scrollEvents.push(scrollViewEventHandler);       
    this.checkScrollViewButton(); 
}


  callback(scrollView: cc.ScrollView, eventType: cc.ScrollView.EventType, customEventData) {    
    switch (eventType) {
      case cc.ScrollView.EventType.SCROLL_ENDED:            
        this.checkScrollViewButton();
        break;         
    }
  }

  checkScrollViewButton(){
    let offSetX = this.scrollView.getScrollOffset().x;
    let halfContentW = this.scrollView.content.getContentSize().width/2;

    //cc.log("SCROLL END", offSetX, halfContentW);
    let isLeft = Math.abs(offSetX) <= this.leftThreshold;
    let isRight = offSetX <= -halfContentW + this.rightThreshold;
    
    this.preButton.interactable = !isLeft;

    this.nextButton.interactable = !isRight;

  }
  //!SECTION
}
