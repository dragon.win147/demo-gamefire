// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboLoading extends cc.Component {

    @property([cc.Node])
    activesOnStart: cc.Node[] = [];

    @property([cc.Node])
    deactivesOnStart: cc.Node[] = [];
   

    @property([cc.Node])
    activesOnFinish: cc.Node[] = [];

    @property([cc.Node])
    deactivesOnFinish: cc.Node[] = [];

    @property([cc.Component.EventHandler])
    onStartEvents: cc.Component.EventHandler[] = [];

    @property([cc.Component.EventHandler])
    onFinishEvents: cc.Component.EventHandler[] = []; 
    
    startLoading(){
        this.setActiveNodes(this.activesOnStart, true);
        this.setActiveNodes(this.deactivesOnStart, false);
        this.runEvents(this.onStartEvents);
    }

    finishLoading(){
        this.setActiveNodes(this.activesOnFinish, true);
        this.setActiveNodes(this.deactivesOnFinish, false);
        this.runEvents(this.onFinishEvents);
    }

    setActiveNodes(nodes: cc.Node[], isActive: boolean = true){
        for (let i = 0; i < nodes.length; i++) {
            nodes[i].active = isActive;            
        }
    }

    runEvents(events: cc.Component.EventHandler[]){
        for (let i = 0; i < events.length; i++) {
            events[i].emit([events[i].customEventData]);            
        }
    }
}
