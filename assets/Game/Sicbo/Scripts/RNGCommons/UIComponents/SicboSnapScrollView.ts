// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
const ScrollEvent = cc.ScrollView.EventType;
@ccclass
export default class SicboSnapScrollView extends cc.Component {
    scrollView:cc.ScrollView = null;
    @property
    numOfItemInView:number = 4;
    contentStartPosX: number = 0;

    preScrollEvent = ScrollEvent.SCROLL_BEGAN;
    snapActive: boolean = false;

    private get contentPosX(){
        return this.scrollView.getContentPosition().x;
    }

    onLoad(){        
        this.scrollView = this.getComponent(cc.ScrollView);
        this.contentStartPosX = this.contentPosX;
        this.init();
        this.registerEvent();
    }

    init() {
        var scrollViewEventHandler = new cc.Component.EventHandler();
        scrollViewEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        scrollViewEventHandler.component = "SicboSnapScrollView"; // This is the code file name
        scrollViewEventHandler.handler = "callback";
        // scrollViewEventHandler.customEventData = "";    
     
        this.scrollView.scrollEvents.push(scrollViewEventHandler);        
    }

    
    private registerEvent() {        
        // this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this, true);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnded, this, true);
        this.node.on(cc.Node.EventType.MOUSE_WHEEL, this.onMouseWheel, this, true);
    }

    private unregisterEvent() {        
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this, true);        
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnded, this, true);        
        this.node.off(cc.Node.EventType.MOUSE_WHEEL, this.onMouseWheel, this, true);
    }

    private onTouchBegan(){
        // //cc.log("DKM", "onTouchBegan");
    }

    private onTouchEnded(){
        // //cc.log("DKM", "onTouchEnded");
        this.snapActive = true;
    }

    // private onTouchCancelled(){
    //     //cc.log("DKM", "onTouchCancelled");
    // }

    private onMouseWheel(){        
        // //cc.log("DKM", "onMouseWheel");
        this.snapActive = true;
    }

    // update (dt) {}


    callback(scrollView: cc.ScrollView, eventType: cc.ScrollView.EventType, customEventData) {
        this.preScrollEvent = eventType;
        switch (eventType) {
          case cc.ScrollView.EventType.SCROLL_ENDED:            
            this.onScrollEnd()
            break;
          case cc.ScrollView.EventType.AUTOSCROLL_ENDED_WITH_THRESHOLD:            
            // //cc.log("DKM", "AUTOSCROLL_ENDED_WITH_THRESHOLD");
            break;
          case cc.ScrollView.EventType.SCROLL_BEGAN:
            // //cc.log("DKM", "SCROLL_BEGAN");
            break;
          case cc.ScrollView.EventType.SCROLLING:
            // //cc.log("DKM", "SCROLLING");
            break;
        }
    }

    onScrollEnd(){
        this.snap();        
        this.snapActive = false;
    }    

    snap(){
        if(this.snapActive == false) return;
        let numOfItem = this.scrollView.content.childrenCount;
        let spacing = this.scrollView.content.width/numOfItem;
        let curPosX = this.contentPosX;

        let itemIndex = Math.abs(Math.round((curPosX - this.contentStartPosX)/spacing) 
                        - Math.round((numOfItem - this.numOfItemInView)/2));
      
        let percent = itemIndex * (numOfItem/(numOfItem - this.numOfItemInView))/numOfItem;
    
        this.scrollView.scrollToPercentHorizontal(percent, .5);
        //cc.log("Snap", itemIndex);
        // this.scrollView.scrollToPercentHorizontal(1 * 2/numOfItem, .5);
    }

    onDestroy(){
        cc.Tween.stopAllByTarget(this.node);
        this.unregisterEvent();
    }
}
