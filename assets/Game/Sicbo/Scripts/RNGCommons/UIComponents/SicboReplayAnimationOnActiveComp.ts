// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboSkeletonUtils from "../Utils/SicboSkeletonUtils";



const {ccclass, property} = cc._decorator;

@ccclass
export default class SicboReplayAnimationOnActiveComp extends cc.Component {

    @property(sp.Skeleton)
    skeleton: sp.Skeleton = null;

    @property(cc.Animation)
    animation: cc.Animation = null;

    @property
    animationName: string =  "";

    @property
    loop: boolean = true;

    onEnable(){
        if(this.skeleton != null)
            SicboSkeletonUtils.setAnimation(this.skeleton, this.animationName, this.loop);
        
        if(this.animation != null)
            this.animation.play(this.animationName, 0);
    }
}
