// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import EventTouch = cc.Event.EventTouch;
const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboPageViewLoopComp extends cc.Component {

    @property(cc.Node)
    private content: cc.Node = null;
    @property([cc.Node])
    private pages: cc.Node[] = [];

    @property
    private distanceBetweenPage: number = 677 + 16;
    @property
    private moveTime: number = 0.1;

    private currentPage: cc.Node = null;
    private idCurrentPage: number = 0;
    private xOriginalPageNode: number = 0;
    private isPageMoving: boolean = false;
    private nextPage: cc.Node = null;
    private nextID: number = 0;

    onLoad() {
        this.pages.forEach((page) => {
            page.active = false;
        })
        this.currentPage = this.pages[0];
        this.idCurrentPage = 0;
        this.currentPage.active = true;
        // this.nextPage = this.pages[1];
        this.xOriginalPageNode = this.currentPage.x;
        // this.distanceBetweenPage += this.xOriginalPageNode;
        // this.nextPage.parent = this.currentPage;
        // this.nextPage.x = this.distanceBetweenPage;
        this.setUpOnTouchPage();
    }

    setUpOnTouchPage() {
        this.pages.forEach((page) => {
            // page.on(
            //     "touchstart",
            //     (e: EventTouch) => {
            //         if (this.isPageMoving) return;
            //         if (e.target.name == this.currentPage.name) {
            //             this.xPageNode = this.currentPage.x;
            //         }
            //     },
            //     this
            // );

            page.on(
                "touchmove",
                (e: EventTouch) => {
                    if (this.isPageMoving) return;
                    if (e.target.name == this.currentPage.name) {
                        this.currentPage.x = this.currentPage.x + e.getDeltaX();
                        if (this.currentPage.x > this.distanceBetweenPage + this.xOriginalPageNode) {
                            this.currentPage.x = this.distanceBetweenPage + this.xOriginalPageNode;
                        } else if (this.currentPage.x < -this.distanceBetweenPage + this.xOriginalPageNode) {
                            this.currentPage.x = -this.distanceBetweenPage + this.xOriginalPageNode;
                        }
                        if (this.currentPage.x > this.xOriginalPageNode) {
                            this.showNextByPage(true);
                        } else {
                            this.showNextByPage(false);
                        }
                    }

                },
                this
            );

            page.on(
                "touchcancel",
                (e: cc.Event.EventTouch) => {
                    if (this.isPageMoving) return;
                    if (e.target.name == this.currentPage.name) {
                        //cc.log("this.current.x " + this.currentPage.x + " " + ((-this.distanceBetweenPage) / 2 + this.xOriginalPageNode));
                        if (this.currentPage.x > this.distanceBetweenPage / 2 + this.xOriginalPageNode) {
                            this.movePage(false);
                        } else if (this.currentPage.x < (-this.distanceBetweenPage) / 2 + this.xOriginalPageNode) {
                            this.movePage(true);
                        } else {
                            this.moveBackToOriginal();
                        }
                    }
                },
                this
            );

            page.on(
                "touchend",
                (e: cc.Event.EventTouch) => {
                    if (this.isPageMoving) return;
                    if (e.target.name == this.currentPage.name) {
                        //cc.log("this.current.x " + this.currentPage.x + " " + ((-this.distanceBetweenPage) / 2 + this.xOriginalPageNode));
                        if (this.currentPage.x > this.distanceBetweenPage / 2 + this.xOriginalPageNode) {
                            this.movePage(false);
                        } else if (this.currentPage.x < (-this.distanceBetweenPage) / 2 + this.xOriginalPageNode) {
                            this.movePage(true);
                        } else {
                            this.moveBackToOriginal();
                        }
                    }
                },
                this
            );
        })

    }
    private movePage(isMoveLeft: boolean) {
        this.isPageMoving = true;
        cc.Tween.stopAllByTarget(this.currentPage);
        cc.tween(this.currentPage)
            .to(
                this.moveTime,
                { x: isMoveLeft ? -this.distanceBetweenPage + this.xOriginalPageNode : this.distanceBetweenPage + this.xOriginalPageNode }
            )
            .call(() => {
                this.pages.forEach((page, index) => {
                    if (index != this.idCurrentPage) {
                        page.parent = this.content;
                        page.active = false;
                    }

                });
                this.idCurrentPage = this.nextID;
                this.nextPage.active = true;
                let tempPage = this.currentPage;
                this.currentPage = this.nextPage;
                this.nextPage = tempPage;
                this.nextPage.parent = this.currentPage;
                this.currentPage.x = this.xOriginalPageNode;
                this.nextPage.x = this.xOriginalPageNode + this.distanceBetweenPage;
                this.isPageMoving = false;
            })
            .start();
    }

    moveToFirstPage(){
        this.pages.forEach((page) => {
            page.parent = this.content;
            page.active = false;
        })
        this.currentPage = this.pages[0];
        this.idCurrentPage = 0;
        this.currentPage.active = true;
        this.currentPage.x = 0;
        this.xOriginalPageNode = this.currentPage.x;
    }

    private moveBackToOriginal() {
        this.isPageMoving = true;
        cc.Tween.stopAllByTarget(this.currentPage);
        cc.tween(this.currentPage)
            .to(
                this.moveTime,
                { x: this.xOriginalPageNode }
            )
            .call(() => {
                this.isPageMoving = false;
            })
            .start();

    }

    private showNextByPage(isLeft: boolean = true) {
        this.pages.forEach((page, index) => {
            if (index != this.idCurrentPage) {
                page.parent = this.content;
                page.active = false;
            }

        });

        if (isLeft) {
            let id = this.idCurrentPage - 1;
            if (id < 0) {
                id += this.pages.length;
            }
            this.nextID = id;
            this.pages[id].parent = this.currentPage;
            this.pages[id].x = -this.distanceBetweenPage;
            this.pages[id].active = true;
            this.nextPage = this.pages[id];
        } else {
            let id = this.idCurrentPage + 1;
            if (id > this.pages.length - 1) {
                id -= this.pages.length;
            }
            this.nextID = id;
            this.pages[id].parent = this.currentPage;
            this.pages[id].x = this.distanceBetweenPage;
            this.pages[id].active = true;
            this.nextPage = this.pages[id];
        }
    }
    public onNextPage() {
        this.showNextByPage(false);
        this.movePage(true);
    }
    public onPrePage() {
        this.showNextByPage(true);
        this.movePage(false);
    }
}
