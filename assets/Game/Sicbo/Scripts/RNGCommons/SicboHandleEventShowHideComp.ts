const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboHandleEventShowHideComp extends cc.Component {
  private onHide: () => void = null;
  private onShow: () => void = null;

  onLoad() {
    cc.game.on(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
    cc.game.on(cc.game.EVENT_SHOW,  this.onChangeTabShow, this);
  }
  
  protected onChangeTabHide() {
    cc.game.pause();
    if (this.onHide) this.onHide();
  }

  protected onChangeTabShow() {
      cc.game.resume();
      if (this.onShow) this.onShow();
  }


  public listenEvent(onHide: () => void, onShow: () => void) {
    this.onHide = onHide;
    this.onShow = onShow;
  }
  onDestroy() {
    cc.game.off(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
    cc.game.off(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
  }
}
