import SicboModuleAdapter from "../../../SicboModuleAdapter";

const {
  Code,
  Empty,
  BenTauMessage,
  Topic,
  SicboClient,
  BetDoubleReply,
  BetDoubleRequest,
  BetReply,
  BetRequest,
  GetOthersReply,
  GetOthersRequest,
  LeaveRequest,
  Message,
  ReBetReply,
  ReBetRequest,
  State,
  HandlerClientBenTauBase,
  AutoJoinReply, LeaveReply,
  HealthCheckRequest, HealthCheckReply,
  EventKey
} = SicboModuleAdapter.getAllRefs();


import SicboPortalAdapter from './SicboPortalAdapter';
import SicboSetting, { SicboScene } from './Setting/SicboSetting';
import SicboText from './Setting/SicboText';
import SicboUIManager from "./Managers/SicboUIManager";
import SicboCannotJoinPopup from "./PopUps/SicboCannotJoinPopup";
import SicboTracking from "./SicboTracking";
export default class SicboMessageHandler extends HandlerClientBenTauBase {
  //#region  Instance

  protected static instance: SicboMessageHandler;
  private client: InstanceType<typeof SicboClient> = null;
  public static IS_WS_DISCONNECTED: boolean = false;  

  public receiveState: (state: InstanceType<typeof State>) => void = null;
  public onErrorCustom: (errCode, err: string) => void = null;
  public static benTauHandler = null;

  static getInstance(): SicboMessageHandler {
    if (!SicboMessageHandler.instance) {
      SicboMessageHandler.instance = new SicboMessageHandler(SicboPortalAdapter.getBenTauHandler());
      SicboMessageHandler.instance.curChannelId = "sicbov2";
      SicboMessageHandler.instance.client = new SicboClient(SicboPortalAdapter.addressBenTau(), {}, null);
      SicboMessageHandler.instance.topic = Topic.SICBO;
      SicboMessageHandler.instance.onSubscribe();
    }
    return SicboMessageHandler.instance;
  }

  static destroy() {
    if (SicboMessageHandler.instance) {
      SicboMessageHandler.instance.onUnSubscribe();
      SicboMessageHandler.instance.onClearAllMsg();
    }
    SicboMessageHandler.instance = null;
  }

  private curChannelId: string = "sicbov2";

  private get metaData() {
    return {
      Authorization: "Bearer " + SicboPortalAdapter.getToken(),
    };
  }

  protected showLoading(callback: Function) {
    SicboPortalAdapter.getInstance().showHandlerLoading(callback);
  }

  protected hideLoading() {
    SicboPortalAdapter.getInstance().hideHandlerLoading();
  }

  protected onShowErr(str: string, onClick?: () => void) {
    //cc.log("SICBO onShowErr", str);
    if(cc.director.getScene().name == SicboScene.Loading){
      SicboUIManager.getInstance().openPopup(SicboCannotJoinPopup, SicboUIManager.SicboCannotJoinPopup, null);
    }else{
      SicboPortalAdapter.getInstance().onShowHandlerErr(SicboText.handlerDefaultErrMsg, onClick);
    }
  }

  protected onDisconnect(CodeSocket, messError: string) {
    SicboPortalAdapter.getInstance().onHandlerDisconnect(CodeSocket, messError);
  }

  getChannelId(): string {
    return this.curChannelId;
  }

  closeHandler() {
    if (SicboMessageHandler.instance) SicboMessageHandler.instance.onUnSubscribe();
    delete SicboMessageHandler.instance;
    SicboMessageHandler.instance = null;
  }

  onDestroy() {
    this.closeHandler();
  }
  //#endregion
  //SECTION implement BaseHandler
  onReceive(msg: InstanceType<typeof BenTauMessage>) {
    super.onReceive(msg);
    let message = Message.deserializeBinary(msg.getPayload_asU8());
    switch (message.getPayloadCase()) {
      case Message.PayloadCase.STATE:
        let state = message.getState();
        if (this.receiveState) this.receiveState(state);
        break;
      default:
        break;
    }
  }

  //#endregion
  handleCustomError(errCode, err: string) {
    if (this.isServerDieCode(errCode)){
        SicboUIManager.getInstance().openPopup(SicboCannotJoinPopup, SicboUIManager.SicboCannotJoinPopup, null);
    }else
        if (this.onErrorCustom) this.onErrorCustom(errCode, err);
}

  private isServerDieCode(errCode): boolean{
    if(SicboMessageHandler.IS_WS_DISCONNECTED || (!window.navigator || !window.navigator.onLine)) return false;
      return (errCode == Code.SICBO_CANNOT_JOIN 
          || errCode == Code.SICBO_REQUEST_TIMEOUT 
          || errCode == Code.UNAVAILABLE 
          || errCode == Code.SICBO_SESSION_NOT_FOUND
          || errCode == Code.SICBO_ACTOR_NOT_FOUND
          || errCode == Code.ABORTED
          || errCode == Code.UNAUTHENTICATED
          || errCode == Code.UNKNOWN)
  }

  //!SECTION

  //ANCHOR Bet
  private isNotEnoughMoney(errCode): boolean{
    return (errCode == Code.SICBO_BET_NOT_ENOUGH_MONEY 
        || errCode == Code.SICBO_REBET_NOT_ENOUGH_MONEY 
        || errCode == Code.SICBO_BET_DOUBLE_NOT_ENOUGH_MONEY)
  }

  sendBetRequest(door, chip: number, response: (response: InstanceType<typeof BetReply>) => void = null) {
    var self = this;

    let request = new BetRequest();
    request.setDoor(door);
    request.setChip(chip);

    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.bet(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
        //cc.log("SICBO BetReply", res?.getTx());
        SicboTracking.trackingBet(res?.getTx()?.getLastTxId(), res?.getTx()?.getAmount());
      });
    };

    this.onSendRequest(msgId, sendRequest, response, (err, code)=>{
      if(self.isNotEnoughMoney(code)){
        cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, SicboMessageHandler.instance.topic, chip);
      }else{
          self?.handleCustomError(code, err);
      }
      // self?.handleCustomError(code, err);
    }, false);
  }

  //ANCHOR Rebet
  sendRebetRequest(response: (response: InstanceType<typeof ReBetReply>) => void = null) {
    var self = this;
    let request = new ReBetRequest();
    var msgId = self.getUniqueId();

    let sendRequest = () => {
      self.client.rebet(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
        //cc.log("SICBO ReBetReply", res?.getTx());
        SicboTracking.trackingReBet(res?.getTx()?.getLastTxId(), res?.getTx()?.getAmount());

      });
    };

    this.onSendRequest(msgId, sendRequest, response, (err, code)=>{
      if(self.isNotEnoughMoney(code)){
        cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, SicboMessageHandler.instance.topic, 0);
      }else{
          self?.handleCustomError(code, err);
      }
      // self?.handleCustomError(code, err);
    }, false);
  }

  //ANCHOR Double bet
  sendBetDoubleRequest(response: (response: InstanceType<typeof BetDoubleReply>) => void = null) {
    var self = this;
    let request = new BetDoubleRequest();
    var msgId = self.getUniqueId();

    let sendRequest = () => {
      self.client.betDouble(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
        //cc.log("SICBO BetDoubleReply", res?.getTx());
        SicboTracking.trackingDoubleBet(res?.getTx()?.getLastTxId(), res?.getTx()?.getAmount());

      });
    };

    this.onSendRequest(msgId, sendRequest, response, (err, code)=>{
      if(self.isNotEnoughMoney(code)){
        cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, SicboMessageHandler.instance.topic, 0);
      }else{
          self?.handleCustomError(code, err);
      }
      // self?.handleCustomError(code, err);
    }, false);
  }

  //ANCHOR AutoJoin
  sendAutoJoinRequest(response: (response: InstanceType<typeof AutoJoinReply>) => void) {
    var self = this;
    let request = new Empty();

    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.autoJoin(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };

    this.onSendRequest(msgId, sendRequest, response, (err, code)=>{
      self?.handleCustomError(code, err);
    }, false);
  }
  //ANCHOR Leave
  sendLeaveRequest(response: (response: InstanceType<typeof LeaveReply>) => void) {
    var self = this;
    let request = new LeaveRequest();

    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.leave(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };

    this.onSendRequest(msgId, sendRequest, response, (err, code)=>{
      self?.handleCustomError(code, err);
    }, false);
  }

  //ANCHOR get Others
  sendGetOthersRequest(from: number, limit: number, response: (response: InstanceType<typeof GetOthersReply>) => void) {
    var self = this;
    let request = new GetOthersRequest();
    request.setCursor(from);
    request.setLimit(limit);

    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.getOthers(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };

    this.onSendRequest(msgId, sendRequest, response, (err, code)=>{
      self?.handleCustomError(code, err);
    }, false);
  }

    //ANCHOR - Health check
    sendHealthCheck(response: (response: InstanceType<typeof HealthCheckReply>) => void){
      let self = this
      let request = new HealthCheckRequest();

      var msgId = self.getUniqueId();
      let sendRequest = () => {
          self.client.healthCheck(request, self.metaData, (err, res) => {
              self.onSendReply(err, res, msgId);
          });
      };

      this.onSendRequest(msgId, sendRequest, response,  (err, code)=>{
          self?.handleCustomError(code, err);
      }, false);
  }

}


