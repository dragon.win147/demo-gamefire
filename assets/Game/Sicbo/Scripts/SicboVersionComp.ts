// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SicboPortalAdapter from "./SicboPortalAdapter";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SicboVersionComp extends cc.Component {
  @property(cc.Label)
  private verLabel: cc.Label = null;
  // version = "1.1.0"
  start() {
    this.node.active = SicboPortalAdapter.getInstance().isLogVersion();
    // SicboPortalAdapter.getInstance().getBundleInfo(bundleInfo=>{
    //   this.verLabel.string = bundleInfo?.version;
    // })
  }

  // update (dt) {}
}
