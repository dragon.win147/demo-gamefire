import NotifyInstance from "../Base/NotifyInstance";
import Vec3 = cc.Vec3;
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
export class TextNotifyData {
  public onClick: () => void = null;
  public msg: string = "";
  public duration: number = 2;
  public position: Vec3 = null;
  constructor(msg: string, duration: number = 2, position: Vec3 = new Vec3(0, 0, 0), onClick: () => void = null) {
    this.msg = msg;
    this.duration = duration;
    this.position = position;
    this.onClick = onClick;
  }
}

@ccclass
export default class TextNotify extends NotifyInstance {
  @property(cc.Label)
  content: cc.Label = null;

  @property(cc.Node)
  rootNode: cc.Node = null;

  @property(cc.SpriteFrame)
  listSprFrame: cc.SpriteFrame[] = [];

  private data: TextNotifyData = null;
  protected onShow(data) {
    this.data = data;
    let notifyData = data as TextNotifyData;
    this.content.string = notifyData.msg;
    cc.Tween.stopAllByTarget(this.node);
    cc.tween(this.node)
      .delay(notifyData.duration)
      .call(() => {
        this.closeInstance();
      })
      .start();

    // let urlFont = "Fonts/MyriadPro-Regular";
    // cc.resources.load(urlFont, cc.Font, function (err, font) {
    //     if (!err && font) {
    //       this.content.font = font;
    //     }
    //   }.bind(this)
    // );
    this.content.node.color = new cc.Color(255, 255, 255);
    this.rootNode.getComponent(cc.Layout).paddingTop = 14;
    this.rootNode.getComponent(cc.Layout).paddingBottom = 14;
    this.rootNode.getComponent(cc.Sprite).spriteFrame = this.listSprFrame[0];
  }

  protected afterShow() {}

  protected beforeShow() {}

  protected beforeClose() {}

  protected afterClose() {}

  public onClick() {
    if (this.data) {
      if (this.data.onClick) this.data.onClick();
    }
  }
}
