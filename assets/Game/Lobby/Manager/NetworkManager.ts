import { Topic } from "@gameloot/topic/topic_pb";
import PortalText from "../Utils/PortalText";
import GameDefine from "../Define/GameDefine";
import { CodeSocket, GameType } from "../Utils/EnumGame";
import BenTauHandler from "../Network/BenTauHandler";
import IdentityHandler from "../Network/IdentityHandler";
import StabilizeConnection from "../Network/StabilizeConnection";
import WalletHandler from "../Network/WalletHandler";
import LayerHelper from "../Utils/LayerHelper";
import GameUtils from "../Utils/GameUtils";
import EventBusManager from "../Event/EventBusManager";
import EventManager from "../Event/EventManager";
import UIManager from "./UIManager";
import { TextNotifyData } from "../Manager/TextNotify";
import TextNotifyDisconnect from "../Manager/TextNotifyDisconnect";
import LoadingPopupSystem from "../Popup/LoadingPopupSystem";
import RetryPopupSystem from "../Popup/RetryPopupSystem";
import WarningPopupSystem from "../Popup/WarningPopupSystem";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NetworkManager extends cc.Component {
  @property(TextNotifyDisconnect)
  textNotifyDisconnect: TextNotifyDisconnect = null;

  @property(RetryPopupSystem)
  retryPopupSystem: RetryPopupSystem = null;

  onLoad() {
    cc.game.addPersistRootNode(this.node);
    this.node.zIndex = LayerHelper.netWorkManager;
    let resourceLocalPaths: string[] = [
      // chỉ thêm chứ không xóa nha
    ];
    cc.resources.preload(resourceLocalPaths, cc.Prefab, (err, prefab) => {
      if (err) {
        return;
      }
    });
  }
  start() {
    EventManager.on(
      EventManager.ON_DISCONNECT,
      (code: number = 0, isRetry: boolean = false, messError: string) => {
        cc.log("ON_DISCONNECT code = " + code + " isRetry = " + isRetry);
        // if (UserManager.getInstance().isLogin) {
        //   this.backToPortal(code, isRetry, messError);
        // }
        UIManager.getInstance().openPopupSystem(WarningPopupSystem, GameDefine.WarningPopupSystem, messError);
      },
      this
    );

    EventManager.on(
      EventManager.ON_PING_FAIL,
      () => {
        cc.log("Ping Fail");
        let duration = 3;
        let textData: TextNotifyData = new TextNotifyData(PortalText.POR_NOTIFY_PING_FAIL, duration);

        if (this.textNotifyDisconnect) {
          this.textNotifyDisconnect.node.active = true;
          this.textNotifyDisconnect.onShow(textData);
        }

        this.scheduleOnce(() => {
          if (BenTauHandler.getInstance().client) {
            UIManager.getInstance()
              .openPopupSystemV2(LoadingPopupSystem, GameDefine.LoadingPopupSystem, true, null, null)
              .then(() => {
                cc.log("onClose ON_PING_FAIL ");
                // BenTauHandler.getInstance().client?.close(CodeSocket.RETRY, "PING FAIL");
                BenTauHandler.getInstance().client?.reConnect(CodeSocket.RETRY, "PING FAIL");
                EventBusManager.onPingError();
              });
          }
        }, duration);
      },
      this
    );

    EventManager.on(
      EventManager.ON_PING_OK,
      () => {
        cc.log("Ping OK");
      },
      this
    );

    // cc.game.on(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
    // cc.game.on(cc.game.EVENT_SHOW, this.onChangeTabShow, this);

    EventManager.on(
      EventManager.ON_RECONNECTED,
      () => {
        WalletHandler.getInstance().onMyWalletRequest((response) => {});
      },
      this
    );

    EventManager.on(
      EventManager.ON_SHOW_RETRY_POPUP,
      () => {
        cc.log("this.textNotifyDisconnect " + this.textNotifyDisconnect);
        if (this.textNotifyDisconnect) {
          this.textNotifyDisconnect.node.active = false;
        }
        if (this.retryPopupSystem) {
          this.retryPopupSystem.node.active = true;
          this.retryPopupSystem.onShow();
        }
      },
      this
    );

    if (!GameUtils.isStatusPing) {
      EventManager.fire(EventManager.ON_PING_FAIL);
    }
  }

  // protected onChangeTabHide() {
  //   cc.log("ON_HIDE_GAME ");
  //   EventManager.fire(EventManager.ON_HIDE_GAME);
  // }

  // protected onChangeTabShow() {
  //   cc.log("ON_SHOW_GAME");
  //   EventManager.fire(EventManager.ON_SHOW_GAME);
  //   if (GameUtils.isNative() && UserManager.getInstance().isLogin) {
  //     WalletHandler.getInstance().onMyWalletRequest((response) => {
  //       //EventManager.fire(EventManager.onUpdateBalance, response.getWallet().getCurrentBalance());
  //     });
  //   }
  // }

  /**
   * Ví dụ cách sử dụng để lấy bến tàu và token
   *let networkHandler = cc.director.getScene().getChildByName("NetworkManager");
    let bentauHandle = networkHandler.getComponent("NetworkManager").getBenTauHandler();
    SicboMessageHandler.benTauHandler = bentauHandle;
   * @returns 
   */

  getBenTauHandler() {
    return BenTauHandler.getInstance();
  }

  getToken() {
    return IdentityHandler.getInstance().token;
  }

  onDestroy() {
    // cc.game.off(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
    // cc.game.off(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
  }
}
