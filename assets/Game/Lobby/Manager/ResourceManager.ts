import GameDefine from "../../../Common/Scripts/Defines/GameDefine";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ResourceManager {
  public static get bundle(): cc.AssetManager.Bundle {
    return cc.assetManager.getBundle(GameDefine.PortalBundle);
  }

  public static loadPrefab(path: string, onComplete: (prefab: cc.Prefab) => void = null) {
    cc.resources.load(path, cc.Prefab, function (err, prefab: cc.Prefab) {
      if (err) {
        cc.log(err);
        return;
      }
      if (onComplete) onComplete(prefab);
    });
  }

  public static loadPrefabWithErrorHandler(path: string): Promise<cc.Asset | cc.Asset[]> {
    return new Promise<cc.Asset | cc.Asset[]>((resolve, reject) => {
      cc.resources.load(path, function (err, prefab) {
        if (err) {
          cc.log(err);
          reject(err);
          return;
        }

        resolve(prefab);
      });
    });
  }

  public static loadSound(path: string, onComplete: (sound: cc.AudioClip) => void = null) {
    cc.resources.load(path, cc.AudioClip, function (err, sound: cc.AudioClip) {
      if (err) {
        cc.log(err);
        return;
      }
      if (onComplete) onComplete(sound);
    });
  }
}
