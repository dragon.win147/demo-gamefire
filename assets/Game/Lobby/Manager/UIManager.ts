export class PopupConfigData {
  public popupType: typeof PopupInstance;
  public popupName: string;

  constructor(type: typeof PopupInstance, name: string) {
    this.popupType = type;
    this.popupName = name;
  }
}

export class PopupSystemConfigData {
  public popupType: typeof PopupSystemInstance;
  public popupName: string;

  constructor(type: typeof PopupSystemInstance, name: string) {
    this.popupType = type;
    this.popupName = name;
  }
}

export class BundlePreloadData {
  public topicConst: number = -1;
  public listPopupPaths: string[] = [];

  constructor(topicConst, listPaths: string[]) {
    this.topicConst = topicConst;
    this.listPopupPaths = listPaths;
  }
}

export class LocalBundlePreload {
  public bundleName: string;
  constructor(name: string) {
    this.bundleName = name;
  }
}

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIManager extends cc.Component {
  static _instance: UIManager;

  static getInstance(): UIManager {
    if (!UIManager._instance) {
      let node = new cc.Node("UIManager");
      UIManager._instance = node.addComponent(UIManager);
      let self = UIManager._instance;

      let canvas = cc.director.getScene().getChildByName("Canvas");
      let size = canvas.getContentSize();

      cc.game.addPersistRootNode(self.node);
      self.node.zIndex = LayerHelper.uiManager;
      //self.setWidget(self.node, self.node.parent);
      // cc.log("UIManager");

      self._persistPopupRoot = new cc.Node("Popup");
      self._persistPopupRoot.setContentSize(size);
      self._persistPopupRoot.setParent(self.node);
      self._persistPopupRoot.setPosition(cc.Vec3.ZERO);
      self.setWidget(self._persistPopupRoot, self.node);

      self._persisNotifyRoot = new cc.Node("Notify");
      self._persisNotifyRoot.setContentSize(size);
      self._persisNotifyRoot.setParent(self.node);
      self._persisNotifyRoot.setPosition(cc.Vec3.ZERO);
      self.setWidget(self._persisNotifyRoot, self.node);

      self._persistPopupSystemRoot = new cc.Node("PopupSystem");
      self._persistPopupSystemRoot.setContentSize(size);
      self._persistPopupSystemRoot.setParent(self.node);
      self._persistPopupSystemRoot.setPosition(cc.Vec3.ZERO);
      self.setWidget(self._persistPopupSystemRoot, self.node);

      // EventManager.on(
      //   EventManager.ON_SCREEN_CHANGE_SIZE,
      //   () => {
      //     self.updateSize();
      //   },
      //   self
      // );

      cc.systemEvent.on(EventKey.ON_SCREEN_CHANGE_SIZE_EVENT, self.updateSize, self);
    }
    return UIManager._instance;
  }

  protected onDestroy(): void {
    cc.systemEvent.off(EventKey.ON_SCREEN_CHANGE_SIZE_EVENT, this.updateSize, this);
  }

  private updateSize() {
    let self = UIManager._instance;
    if (self && self.node) {
      let canvas = cc.director.getScene().getChildByName("Canvas");
      if (canvas) {
        let size = canvas.getContentSize();

        self.node.setContentSize(size);
        self.node.setPosition(canvas.getPosition());

        self._persistPopupRoot.setContentSize(size);
        self._persistPopupRoot.setParent(self.node);
        self._persistPopupRoot.setPosition(cc.Vec3.ZERO);

        self._persisNotifyRoot.setContentSize(size);
        self._persisNotifyRoot.setParent(self.node);
        self._persisNotifyRoot.setPosition(cc.Vec3.ZERO);

        self._persistPopupSystemRoot.setContentSize(size);
        self._persistPopupSystemRoot.setParent(self.node);
        self._persistPopupSystemRoot.setPosition(cc.Vec3.ZERO);
      }
    }
  }

  private setWidget(node: cc.Node, parent: cc.Node) {
    let widget = node.getComponent(cc.Widget);
    if (widget == null) widget = node.addComponent(cc.Widget);
    widget.target = parent;
    widget.isAlignLeft = true;
    widget.isAbsoluteRight = true;
    widget.isAbsoluteTop = true;
    widget.isAlignBottom = true;
    widget.left = 0;
    widget.top = 0;
    widget.bottom = 0;
    widget.right = 0;
  }

  private _persisNotifyRoot: cc.Node = null;
  private _persistPopupSystemRoot: cc.Node = null;
  private _persistPopupRoot: cc.Node = null;
  private paymentTopUpNode: cc.Node = null;
  private paymentWithDrawNode: cc.Node = null;

  public get PaymentTopUpObject() {
    return this.paymentTopUpNode;
  }

  public get PaymentWithDrawObject() {
    return this.paymentWithDrawNode;
  }

  public setPaymentTopUpObject(node: cc.Node) {
    this.paymentTopUpNode = node;
  }

  public setPaymentWithDrawObject(node: cc.Node) {
    this.paymentWithDrawNode = node;
  }

  public get LayerUpPopup() {
    return this._persistPopupRoot;
  }

  public init() {
    // cc.log("UIManager...... init");
  }
  _currentScreen: ScreenInstance;
  public get currentScreen() {
    return this._currentScreen;
  }
  public set currentScreen(value) {
    this._currentScreen = value;
  }

  @property(cc.Node)
  screenTran: cc.Node = null;

  private _screenInstances: Map<string, ScreenInstance> = new Map<string, ScreenInstance>();

  _currentHeader: ElementInstance;
  _currentElementName: string;
  public get currentElement() {
    return this._currentHeader;
  }
  public set currentElement(value) {
    this._currentHeader = value;
  }

  @property(cc.Node)
  elementTran: cc.Node = null;

  private _elementInstances: Map<string, ElementInstance> = new Map<string, ElementInstance>();
  _currentPopup: PopupInstance;
  _currentPopupName: string;
  public get currentPopup() {
    return this._currentPopup;
  }
  public set currentPopup(value) {
    this._currentPopup = value;
  }

  @property(cc.Node)
  popupTran: cc.Node = null;

  private _popupInstances: Map<string, PopupInstance> = new Map<string, PopupInstance>();

  _currentNotify: PopupInstance;
  _currentNotifyName: string;

  public get currentNotify() {
    return this._currentNotify;
  }
  public set currentNotify(value) {
    this._currentNotify = value;
  }

  @property(cc.Node)
  notifyTran: cc.Node = null;

  private _notifyInstances: Map<string, NotifyInstance> = new Map<string, NotifyInstance>();

  _currentPopupSystem: PopupSystemInstance;
  _currentPopupSystemName: string;
  public get currentPopupSystem() {
    return this._currentPopupSystem;
  }
  public set currentPopupSystem(value) {
    this._currentPopupSystem = value;
  }

  public get currentPopupSystemName() {
    return this._currentPopupSystemName;
  }

  @property(cc.Node)
  popupSystemTran: cc.Node = null;
  private _currentLoading: number = 0;
  private _popupSystemInstances: Map<string, PopupSystemInstance> = new Map<string, PopupSystemInstance>();

  public openElement(type: typeof ElementInstance, name: string, data = null, parent: cc.Node = null, onComplete: (instance) => void = null): void {
    let self = UIManager._instance;

    self._currentElementName = name;
    parent = parent ? parent : self.elementTran;
    self.getElementInstance(
      type,
      name,
      (instance) => {
        if (self.currentElement == instance) {
          return self.currentElement;
        }

        self.currentElement = instance;
        instance.open(data);
        instance._close = () => {
          self.currentElement = null;
          this._currentElementName = "";
        };
        if (onComplete) onComplete(instance);
      },
      parent
    );
  }

  public closeElement(type: typeof ElementInstance, name: string) {
    let self = UIManager._instance;

    self.getElementInstance(type, name, (instance) => {
      instance.close();
    });
  }

  public getElementInstance(type: typeof ElementInstance, name: string, onComplete: (instance) => void, parent: cc.Node = null) {
    let self = UIManager._instance;
    const _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
    if (!self._elementInstances) self._elementInstances = new Map<string, ElementInstance>();
    if (self._elementInstances.has(name) && self._elementInstances.get(name) != null && self._elementInstances.get(name).node != null) {
      let element = self._elementInstances.get(name);
      element.node.parent = null;
      _parent.addChild(element.node);
      onComplete(element);
    } else {
      ResourceManager.loadPrefab("Prefabs/Element/" + name, function (prefab) {
        const newNode = cc.instantiate(prefab) as unknown as cc.Node;
        const instance = newNode.getComponent(type);
        _parent.addChild(newNode);
        self._elementInstances.set(name, instance);

        onComplete(instance);
      });
    }
  }

  public openScreen(type: typeof ScreenInstance, name: string, data = null, parent: cc.Node = null) {
    let self = UIManager._instance;
    parent = parent ? parent : self.screenTran;
    self.getSceneInstance(
      type,
      name,
      (instance) => {
        if (self.currentScreen != null) {
          self.currentScreen.close();
        }
        self.currentScreen = instance;

        instance.open(data);
      },
      parent
    );
  }

  public getSceneInstance(type: typeof ScreenInstance, name: string, onComplete: (instance: ScreenInstance) => void, parent: cc.Node = null) {
    let self = UIManager._instance;
    const _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
    if (!self._screenInstances) self._screenInstances = new Map<string, ScreenInstance>();
    if (self._screenInstances.has(name) && self._screenInstances.get(name) != null && self._screenInstances.get(name).node != null) {
      let scene = self._screenInstances.get(name);
      scene.node.parent = null;
      _parent.addChild(scene.node);
      onComplete(scene);
    } else {
      ResourceManager.loadPrefab("Prefabs/Screen/" + name, function (prefab) {
        var newNode = cc.instantiate(prefab) as unknown as cc.Node;
        var instance = newNode.getComponent(type);
        _parent.addChild(newNode);
        self._screenInstances.set(name, instance);

        onComplete(instance);
      });
    }
  }

  public preloadListPopup(listPopups: PopupConfigData[], onProgress: (finish: number, total: number, item: PopupInstance) => void = null, onComplete: () => void = null) {
    if (listPopups.length == 0) {
      if (onComplete) onComplete();
      return;
    }
    let finish = 0;
    let total = listPopups.length;
    listPopups.forEach((data) => {
      this.preLoadPopup(data.popupType, data.popupName, (err, item: PopupInstance) => {
        if (err) {
          // cc.log(err);
        }
        finish++;
        if (onProgress) {
          onProgress(finish, total, item);
        }
        if (finish == total) {
          if (onComplete) onComplete();
        }
      });
    });
  }

  public preLoadPopup(type: typeof PopupInstance, name: string, callback: (err, item: PopupInstance) => void = null) {
    let self = UIManager._instance;
    if (!self._popupInstances) self._popupInstances = new Map<string, PopupInstance>();
    if (self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null) {
      if (callback) {
        callback(null, self._popupInstances.get(name));
      }
      return;
    } else {
      ResourceManager.loadPrefab("Prefabs/Popup/" + name, function (prefab) {
        const newNode = cc.instantiate(prefab) as unknown as cc.Node;
        const instance = newNode.getComponent(type);
        self._popupInstances.set(name, instance);
        if (callback) {
          callback(null, self._popupInstances.get(name));
        }
      });
    }
  }

  public preloadListPopupSystem(
    listPopups: PopupSystemConfigData[],
    onProgress: (finish: number, total: number, item: PopupSystemInstance) => void = null,
    onComplete: () => void = null
  ) {
    if (listPopups.length == 0) {
      if (onComplete) onComplete();
      return;
    }
    let finish = 0;
    let total = listPopups.length;
    listPopups.forEach((data) => {
      this.preLoadPopupSystem(data.popupType, data.popupName, (err, item: PopupSystemInstance) => {
        if (err) {
          // cc.log(err);
        }
        finish++;
        if (onProgress) {
          onProgress(finish, total, item);
        }
        if (finish == total) {
          if (onComplete) onComplete();
        }
      });
    });
  }

  public preLoadPopupSystem(type: typeof PopupSystemInstance, name: string, callback: (err, item: PopupSystemInstance) => void = null) {
    let self = UIManager._instance;

    if (!self._popupSystemInstances) self._popupSystemInstances = new Map<string, PopupSystemInstance>();
    if (self._popupSystemInstances.has(name) && self._popupSystemInstances.get(name) != null && self._popupSystemInstances.get(name).node != null) {
      if (callback) {
        callback(null, self._popupSystemInstances.get(name));
      }
      return;
    } else {
      ResourceManager.loadPrefab("Prefabs/PopupSystem/" + name, function (prefab) {
        const newNode = cc.instantiate(prefab) as unknown as cc.Node;
        const instance = newNode.getComponent(type);
        self._popupSystemInstances.set(name, instance);
        if (callback) {
          callback(null, self._popupSystemInstances.get(name));
        }
      });
    }
  }

  public openPopup(
    type: typeof PopupInstance,
    name: string,
    data = null,
    onComplete: (instance: PopupInstance) => void = null,
    parent: cc.Node = null,
    showLoadingPopUp: boolean = true
  ) {
    let self = UIManager._instance;
    parent = parent ? parent : self.popupTran;
    if (name == self._currentPopupName && self._currentPopup.node) {
      if (onComplete) onComplete(self._currentPopup);
      return;
    }
    self._currentPopupName = name;
    self.getPopupInstance(
      type,
      name,
      (instance) => {
        if (self.currentPopup == instance) {
          if (onComplete) onComplete(self.currentPopup);
          return;
        }

        self.currentPopup = instance;
        instance.open(data);
        instance._close = () => {
          self.currentPopup = null;
          self._currentPopupName = "";
        };
        if (onComplete) onComplete(self.currentPopup);
      },
      parent,
      showLoadingPopUp
    );
  }

  public openPopupWithErrorHandler(type: typeof PopupInstance, name: string, data = null, parent: cc.Node = null, showLoadingPopUp: boolean = true) {
    let self = UIManager._instance;
    parent = parent ? parent : self.popupTran;
    if (name == self._currentPopupName && self._currentPopup != null && self._currentPopup.node) {
      return new Promise<PopupInstance>((resolve) => {
        resolve(self._currentPopup);
      });
    }

    self._currentPopupName = name;
    return self
      .getPopupInstanceWithError(type, name, parent, showLoadingPopUp)
      .then((instance: PopupInstance) => {
        if (self.currentPopup == instance) {
          return new Promise<PopupInstance>((resolve) => {
            resolve(self.currentPopup);
          });
        }

        self.currentPopup = instance;
        instance.open(data);
        instance._close = () => {
          self.currentPopup = null;
          self._currentPopupName = "";
        };

        return instance;
      })
      .catch((error) => {
        if (showLoadingPopUp) {
          self._currentPopupName = "";
          UIManager.getInstance().closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
        }
        // UIManager.getInstance().openPopupSystem(WarningPopupSystem, GameDefine.WarningPopupSystem, PortalText.POR_MESS_NETWORK_DISCONNECT);
      });
  }

  public hasPopupInstance(name: string) {
    return UIManager._instance._popupInstances.has(name);
  }

  public closePopup(type: typeof PopupInstance, name: string) {
    let self = UIManager._instance;
    self.getPopupInstance(type, name, (instance) => {
      instance.closeInstance();
    });
  }

  public getPopupInstance(type: typeof PopupInstance, name: string, onComplete: (instance: PopupInstance) => void, parent: cc.Node = null, showLoadingPopUp: boolean = true) {
    let self = UIManager._instance;
    const _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
    if (!self._popupInstances) self._popupInstances = new Map<string, PopupInstance>();
    if (self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null) {
      let popup = self._popupInstances.get(name);
      popup.node.parent = null;
      _parent.addChild(popup.node);
      onComplete(popup);
    } else {
      if (showLoadingPopUp) {
        self.openPopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem, true, null, null, (popupLoading) => {
          this.loadPopUpPrefab(type, name, onComplete, _parent);
        });
      } else {
        this.loadPopUpPrefab(type, name, onComplete, _parent);
      }
    }
  }

  public async getPopupInstanceWithError(type: typeof PopupInstance, name: string, parent: cc.Node = null, showLoadingPopUp: boolean = true) {
    let self = UIManager._instance;
    const _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
    if (!self._popupInstances) self._popupInstances = new Map<string, PopupInstance>();
    if (self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null) {
      let popup = self._popupInstances.get(name);
      popup.node.parent = null;
      _parent.addChild(popup.node);
      return new Promise<PopupInstance>((resolve) => {
        resolve(popup);
      });
    } else {
      if (showLoadingPopUp) {
        await self.openPopupSystemV2(LoadingPopupSystem, GameDefine.LoadingPopupSystem, true, null, null);
        return this.loadPopUpPrefabWithErrorHandler(type, name, _parent);
      } else {
        return this.loadPopUpPrefabWithErrorHandler(type, name, _parent);
      }
    }
  }

  private loadPopUpPrefab(type: typeof PopupInstance, name: string, onComplete: (instance: PopupInstance) => void, parent: cc.Node = null) {
    let self = UIManager._instance;
    ResourceManager.loadPrefab("Prefabs/Popup/" + name, function (prefab) {
      const newNode = cc.instantiate(prefab) as unknown as cc.Node;
      const instance = newNode.getComponent(type);
      parent.addChild(newNode);
      self._popupInstances.set(name, instance);
      self.closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
      onComplete(instance);
    });
  }

  private loadPopUpPrefabWithErrorHandler(type: typeof PopupInstance, name: string, parent: cc.Node = null) {
    let self = UIManager._instance;
    return ResourceManager.loadPrefabWithErrorHandler("Prefabs/Popup/" + name).then(function (prefab) {
      const newNode = cc.instantiate(prefab) as unknown as cc.Node;
      const instance = newNode.getComponent(type);
      parent.addChild(newNode);
      self._popupInstances.set(name, instance);
      self.closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
      return instance;
    });
  }

  public openNotify(type: typeof NotifyInstance, name: string, data = null, parent: cc.Node = null) {
    let self = UIManager._instance;
    parent = parent ? parent : self.notifyTran;
    if (name == self._currentNotifyName) return;
    self._currentNotifyName = name;
    self.getNotifyInstance(
      type,
      name,
      (instance) => {
        if (self.currentNotify == instance) return;

        self.currentNotify = instance;
        instance.open(data);
        instance._close = () => {
          self.currentNotify = null;
          self._currentNotifyName = "";
        };
      },
      parent
    );
  }

  public getNotifyInstance(type: typeof NotifyInstance, name: string, onComplete: (instance: NotifyInstance) => void, parent: cc.Node = null) {
    let self = UIManager._instance;
    let _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
    if (name == GameDefine.TextNotify) _parent = self._persisNotifyRoot;

    if (!self._notifyInstances) self._notifyInstances = new Map<string, NotifyInstance>();
    if (self._notifyInstances.has(name) && self._notifyInstances.get(name) != null && self._notifyInstances.get(name).node != null) {
      let notify = self._notifyInstances.get(name);
      if (name == GameDefine.TextNotify) {
      } else {
        notify.node.parent = null;
        _parent.addChild(notify.node);
      }
      onComplete(notify);
    } else {
      ResourceManager.loadPrefab("Prefabs/Notify/" + name, function (prefab) {
        const newNode = cc.instantiate(prefab) as unknown as cc.Node;
        const instance = newNode.getComponent(type);
        _parent.addChild(newNode);
        self._notifyInstances.set(name, instance);
        onComplete(instance);
      });
    }
  }

  public openPopupSystem(
    type: typeof PopupSystemInstance,
    name: string,
    data = null,
    onYes: () => void = null,
    onNo: () => void = null,
    onComplete: (instance: PopupSystemInstance) => void = null,
    parent: cc.Node = null
  ) {
    let self = UIManager._instance;
    // self.popupSystemTran = cc.director.getScene().getComponentInChildren(MiniGameManager)?.popupSystemTran;
    parent = parent ? parent : self.popupSystemTran;

    if (name == GameDefine.LoadingPopupSystem) {
      self._currentLoading++;
    }
    if (name == self._currentPopupSystemName) {
      return;
    }
    if (self._currentPopupSystem && self._currentPopupSystem.node) {
      if (onComplete) onComplete(self._currentPopupSystem);
      return;
    }
    self._currentPopupSystemName = name;
    self.getPopupSystemInstance(
      type,
      name,
      (instance) => {
        if (self._currentPopupSystem == instance) {
          if (onComplete) onComplete(self._currentPopupSystem);
          return;
        }

        self._currentPopupSystem = instance;
        instance.open(data, onYes, onNo);
        instance._close = () => {
          self._currentPopupSystem = null;
          self._currentPopupSystemName = "";
        };
        if (onComplete) onComplete(instance);
      },
      parent
    );
  }

  public openPopupSystemV2(
    type: typeof PopupSystemInstance,
    name: string,
    data = null,
    onYes: () => void = null,
    onNo: () => void = null,
    parent: cc.Node = null
  ): Promise<PopupSystemInstance> {
    return new Promise<PopupSystemInstance>((resolve) => {
      let self = UIManager._instance;
      // self.popupSystemTran = cc.director.getScene().getComponentInChildren(MiniGameManager)?.popupSystemTran;
      parent = parent ? parent : self.popupSystemTran;

      if (name == GameDefine.LoadingPopupSystem) {
        self._currentLoading++;
      }

      if (name == self._currentPopupSystemName && self._currentPopupSystem && self._currentPopupSystem.node) {
        resolve(self._currentPopupSystem);
        return;
      }

      self._currentPopupSystemName = name;
      self.getPopupSystemInstance(
        type,
        name,
        (instance) => {
          if (self._currentPopupSystem == instance) {
            resolve(self._currentPopupSystem);
            return;
          }

          self._currentPopupSystem = instance;
          instance.open(data, onYes, onNo);
          instance._close = () => {
            self._currentPopupSystem = null;
            self._currentPopupSystemName = "";
          };
          resolve(instance);
        },
        parent
      );
    });
  }

  public closeAllPopupSystem() {
    let self = UIManager._instance;
    self._popupSystemInstances.forEach((p) => {
      if (p != null && p.node != null) p.closeInstance();
      self._currentLoading = 0;
      self._currentPopupSystemName = "";
    });
  }

  public closeAllPopup(shouldCloseProfilePopup: boolean = true) {
    let self = UIManager._instance;
    self._popupInstances.forEach((p, name) => {
      if (!shouldCloseProfilePopup) {
      }
      if (p != null && p.node != null) p.closeInstance();
      self._currentPopupName = "";
    });
    self.closeAllPopupSystem();
  }

  closePaymentPopup() {
    if (this.paymentWithDrawNode) {
      if (this.paymentWithDrawNode.active) this.paymentWithDrawNode.active = false;
    }
    if (this.paymentTopUpNode) {
      if (this.paymentTopUpNode.active) this.paymentTopUpNode.active = false;
    }
  }

  public closePopupSystem(type: typeof PopupSystemInstance, name: string) {
    let self = UIManager._instance;
    if (name == GameDefine.LoadingPopupSystem) {
      self._currentLoading--;
      if (self._currentLoading < 0) self._currentLoading = 0;
    }

    self.getPopupSystemInstance(type, name, (instance) => {
      if (self._currentLoading == 0) {
        instance.closeInstance();
        self._currentPopupSystemName = "";
      }
    });
  }

  public getPopupSystemInstance(type: typeof PopupSystemInstance, name: string, onComplete: (instance: PopupSystemInstance) => void, parent: cc.Node = null) {
    let self = UIManager._instance;
    let _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;

    _parent = self._persistPopupSystemRoot;

    if (!self._popupSystemInstances) self._popupSystemInstances = new Map<string, PopupSystemInstance>();
    if (self._popupSystemInstances.has(name) && self._popupSystemInstances.get(name) != null && self._popupSystemInstances.get(name).node != null) {
      let popup = self._popupSystemInstances.get(name);
      popup.node.parent = null;
      _parent.addChild(popup.node);
      onComplete(popup);
    } else {
      ResourceManager.loadPrefab("Prefabs/PopupSystem/" + name, function (prefab) {
        const newNode = cc.instantiate(prefab) as unknown as cc.Node;
        const instance = newNode.getComponent(type);
        _parent.addChild(newNode);
        self._popupSystemInstances.set(name, instance);

        onComplete(instance);
      });
    }
  }

  public openActionElementPopup(type: typeof PopupInstance, name: string, data = null, onComplete: (instance: PopupSystemInstance) => void = null, parent: cc.Node = null) {
    // cc.log("open action popup");
    let self = UIManager._instance;
    parent = parent ? parent : self.popupTran;
    // if (name == self._currentPopupName) {
    //   if (onComplete) onComplete(self._currentPopup);
    //   return;
    // }
    self._currentPopupName = name;
    self.getActionElementPopupInstance(
      type,
      name,
      (instance) => {
        if (self.currentPopup == instance) {
          if (onComplete) onComplete(self.currentPopup);
          return;
        }

        self.currentPopup = instance;
        instance.open(data);
        instance._close = () => {
          self.currentPopup = null;
          self._currentPopupName = "";
        };
        if (onComplete) onComplete(self.currentPopup);
      },
      parent
    );
  }

  public closeActionElementPopup(type: typeof PopupInstance, name: string) {
    let self = UIManager._instance;
    self.getActionElementPopupInstance(type, name, (instance) => {
      instance.closeInstance();
    });
  }

  public getActionElementPopupInstance(type: typeof PopupInstance, name: string, onComplete: (instance: PopupInstance) => void, parent: cc.Node = null) {
    let self = UIManager._instance;
    const _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
    if (!self._popupInstances) self._popupInstances = new Map<string, PopupInstance>();
    if (self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null) {
      let popup = self._popupInstances.get(name);
      popup.node.parent = null;
      _parent.addChild(popup.node);
      onComplete(popup);
    } else {
      // self.openPopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem, true, null, null, (popupLoading) => {
      ResourceManager.loadPrefab("Prefabs/Popup/" + name, function (prefab) {
        const newNode = cc.instantiate(prefab) as unknown as cc.Node;
        const instance = newNode.getComponent(type);
        _parent.addChild(newNode);
        self._popupInstances.set(name, instance);
        // self.closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
        onComplete(instance);
      });
      // });
    }
  }

  private textTempParent: cc.Node = null;
  private uiTempParent: cc.Node = null;
  private effTempParent: cc.Node = null;
  public setParentText(node: cc.Node, parent: cc.Node) {
    if (!this.textTempParent) {
      this.textTempParent = new cc.Node("textTemp");
    } else {
      this.textTempParent = new cc.Node("textTemp");
    }
  }
  public setParentUI(node: cc.Node) {
    let parent = new cc.Node("ui");
  }
  public setParentEff(node: cc.Node) {
    let parent = new cc.Node("eff");
  }
}

import ScreenInstance from "../Base/ScreenInstance";
import PopupInstance from "../Base/PopupInstance";
import PopupSystemInstance from "../Base/PopupSystemInstance";
import ElementInstance from "../Base/ElementInstance";
import GameDefine from "./../Define/GameDefine";
import LoadingPopupSystem from "../Popup/LoadingPopupSystem";
import NotifyInstance from "../Base/NotifyInstance";
import LayerHelper from "../Utils/LayerHelper";
import ResourceManager from "./ResourceManager";
import EventKey from "@gameloot/client-base-handler/build/EventKey";
