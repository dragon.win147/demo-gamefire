import NotifyInstance from "../Base/NotifyInstance";
import Vec3 = cc.Vec3;
import { TextNotifyData } from "./TextNotify";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
// export class TextNotifyData {
//   public onClick: () => void = null;
//   public msg: string = "";
//   public duration: number = 2;
//   public position: Vec3 = null;
//   constructor(msg: string, duration: number = 2, position: Vec3 = new Vec3(0, 0, 0), onClick: () => void = null) {
//     this.msg = msg;
//     this.duration = duration;
//     this.position = position;
//     this.onClick = onClick;
//   }
// }

@ccclass
export default class TextNotifyDisconnect extends NotifyInstance {
  @property(cc.Label)
  content: cc.Label = null;

  @property(cc.Node)
  rootNode: cc.Node = null;

  @property(cc.SpriteFrame)
  listSprFrame: cc.SpriteFrame[] = [];

  private data: TextNotifyData = null;
  public onShow(data = null) {
    this.node.active = true;
    this.data = data;
    let notifyData = data as TextNotifyData;
    if (notifyData.msg != "") this.content.string = notifyData.msg;
    this.node.stopAllActions();
    this.node.runAction(
      cc.sequence(
        cc.delayTime(notifyData.duration),
        cc.callFunc(() => {
          this.onHide();
        })
      )
    );
  }

  protected afterShow() {}

  protected beforeShow() {}

  protected beforeClose() {}

  protected afterClose() {}

  public onHide() {
    this.node.active = false;
  }

  public onClick() {
    if (this.data) {
      if (this.data.onClick) this.data.onClick();
    }
  }
}
