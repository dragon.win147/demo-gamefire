import { BenTauMessage } from "@gameloot/bentau/bentau_pb";
import { Topic } from "@gameloot/topic/topic_pb";
import { MyAccountClient } from "@gameloot/myaccount/MyaccountServiceClientPb";
import {
  Avatar,
  Gender,
  Level,
  ListBasesReply,
  ListBasesRequest,
  ListLevelsReply,
  ListLevelsRequest,
  ListMyAvatarsReply,
  ListMyAvatarsRequest,
  ProfileReply,
  UpdateAvatarReply,
  UpdateAvatarRequest,
  UpdateProfileReply,
  UpdateProfileRequest,
} from "@gameloot/myaccount/myaccount_pb";
import { Code } from "@gameloot/myaccount/myaccount_code_pb";
import { Playah } from "@gameloot/playah/playah_pb";
import { HandlerClientBenTauBase } from "@gameloot/client-base-handler/build";
import { Empty } from "google-protobuf/google/protobuf/empty_pb";
import PortalText from "../Utils/PortalText";
import ConnectDefine from "../Define/ConnectDefine";
import GameDefine from "../Define/GameDefine";
import { CodeSocket } from "../Utils/EnumGame";
import EventManager from "../Event/EventManager";
import UIManager from "../Manager/UIManager";
import TextNotify, { TextNotifyData } from "../Manager/TextNotify";
import LoadingPopupSystem from "../Popup/LoadingPopupSystem";
import WarningPopupSystem from "../Popup/WarningPopupSystem";
import BenTauHandler from "./BenTauHandler";
import IdentityHandler from "./IdentityHandler";

export default class MyAccountHandler extends HandlerClientBenTauBase {
  protected static instance: MyAccountHandler;

  static getInstance(): MyAccountHandler {
    if (!MyAccountHandler.instance) {
      MyAccountHandler.instance = new MyAccountHandler(BenTauHandler.getInstance());
      MyAccountHandler.instance.client = new MyAccountClient(ConnectDefine.addressSanBay, {}, null);
      MyAccountHandler.instance.topic = Topic.MY_ACCOUNT_LEVEL_UP;
      MyAccountHandler.instance.onSubscribe();
    }
    return MyAccountHandler.instance;
  }

  static destroy() {
    if (MyAccountHandler.instance) MyAccountHandler.instance.onUnSubscribe();
    MyAccountHandler.instance = null;
  }
  private client: MyAccountClient = null;
  private bases: Map<number, string> = new Map<number, string>();
  private frames: Map<number, string> = new Map<number, string>();
  private playah: Playah = null;

  get Frames() {
    return this.frames;
  }
  get Bases() {
    return this.bases;
  }
  get Playah() {
    return this.playah;
  }

  private get metaData() {
    return {
      Authorization: "Bearer " + IdentityHandler.getInstance().token,
    };
  }

  protected showLoading(callback: Function) {
    UIManager.getInstance()
      .openPopupSystemV2(LoadingPopupSystem, GameDefine.LoadingPopupSystem, false, null, null)
      .then(() => {
        callback();
      });
  }

  protected hideLoading() {
    UIManager.getInstance().closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
  }

  protected onShowErr(str: string, onClick?: () => void) {
    UIManager.getInstance().openPopupSystem(WarningPopupSystem, GameDefine.WarningPopupSystem, str, onClick, onClick);
  }

  protected onDisconnect(CodeSocket: CodeSocket, messError: string) {
    EventManager.fire(EventManager.ON_DISCONNECT, CodeSocket, false, messError);
  }

  onGetBasePath(id: number): string {
    return this.bases.get(id);
  }

  onGetFramePath(id: number): string {
    return this.frames.get(id);
  }

  handleErr: () => void = null;
  onGetAvatarList(response: (response: ListMyAvatarsReply) => void = null) {
    //cc.log("onGetAvatarList");
    let self = this;
    let request = new ListMyAvatarsRequest();
    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.listMyAvatars(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };
    this.onSendRequest(
      msgId,
      sendRequest,
      (res: ListMyAvatarsReply) => {
        res.getBasesList().forEach((base) => {
          this.bases.set(base.getId(), base.getPath());
        });
        res.getFramesList().forEach((frame) => {
          this.frames.set(frame.getId(), frame.getPath());
        });
        if (response) response(res);
      },
      () => {
        //cc.log("Err");
        if (response) response(null);
      },
      false
    );
  }

  onGetAvatarListWhenSignUp(gender: Gender, random: boolean = true, limit: number = 10, response: (response: ListBasesReply) => void = null) {
    let self = this;
    let request = new ListBasesRequest();
    request.setGender(gender);
    request.setRandom(random);
    request.setLimit(limit);
    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.listBases(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };
    this.onSendRequest(
      msgId,
      sendRequest,
      (res: ListBasesReply) => {
        this.frames.set(res.getDefaultFrame().getId(), res.getDefaultFrame().getPath());
        res.getBasesList().forEach((base) => {
          this.bases.set(base.getId(), base.getPath());
        });

        response(res);
      },
      null,
      false
    );
  }

  getProfile(response: (response: ProfileReply) => void) {
    //cc.log("getProfile " + IdentityHandler.getInstance().token);

    let request = new Empty();
    let self = this;
    var msgId = self.getUniqueId();
    let metaData = { Authorization: "Bearer " + IdentityHandler.getInstance().token };

    let sendRequest = () => {
      self.client.me(request, metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };

    this.onSendRequest(
      msgId,
      sendRequest,
      (res: ProfileReply) => {
        //cc.log("getProfile done ");
        if (res == null || !res?.hasProfile() || !res?.hasAvatar()) {
          // UIManager.getInstance().openUpdateNickNamePopup();
        } else {
          // UserManager.getInstance().setUserData(res);
          let playah = new Playah();
          playah.setUserId(res.getSafeId());
          playah.setProfile(res.getProfile());
          playah.setAvatar(res.getAvatar());
          self.playah = playah;
        }
        if (response) {
          response(res);
        }
      },
      (err, code: Code) => {
        // if (code != Code.UNKNOWN) {
        //   UIManager.getInstance().openUpdateNickNamePopup();
        // }
      },
      false
    );
  }

  handleCustomError(errCode: Code, err: string) {
    if (errCode == Code.UNKNOWN) return;
    if (this.handleErr) this.handleErr();
    //cc.log("handleCustomError " + "errCode" + err);
    let errString = "[" + errCode + "] ";
    //cc.log("errcode: ", errCode);
    switch (errCode) {
      case Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_REQUIRE_LENGTH:
        errString = PortalText.POR_NOTIFY_NICKNAME_TOO_SHORT;
        break;
      case Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_OVER_LENGTH:
        errString = PortalText.POR_NOTIFY_NICKNAME_TOO_LONG;
        break;
      case Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_START_SPACE_CHAR:
        errString = PortalText.POR_NOTIFY_NICKNAME_INVALID_PREFIX;
        break;
      case Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_SAME_USERNAME:
        errString = PortalText.POR_NOTIFY_NICKNAME_SAME_USERNAME;
        break;
      case Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_HAS_PROFANITY:
        errString = PortalText.POR_NOTIFY_NICKNAME_PROFANITY;
        break;
      case Code.MY_ACCOUNT_INVALID_DISPLAY_NAME:
        errString = PortalText.POR_NOTIFY_NICKNAME_TOO_SHORT;
        break;
      case Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_HAS_INVALID_CHARACTER:
        errString = PortalText.POR_NOTIFY_NICKNAME_HAS_BANNED_CHARACTER;
        break;
    }
    UIManager.getInstance().openNotify(TextNotify, GameDefine.TextNotify, new TextNotifyData(errString, 2));
  }
  onReceive(msg: BenTauMessage) {
    let message = Level.deserializeBinary(msg.getPayload_asU8());
    let level = message.getLevel();
    EventManager.fire(EventManager.UPDATE_BADGE_LEVEL_UP, level);
    // UIManager.getInstance().openAnimationLevelUpPopup(level);
  }
}
