export default class BenTauHandler {
  protected static instance: BenTauHandler;

  static getInstance(): BenTauHandler {
    if (!BenTauHandler.instance) {
      BenTauHandler.instance = new BenTauHandler();
      BenTauHandler.instance.handUpdateTimeOut();
    }
    return BenTauHandler.instance;
  }

  public client: WSClient = null;

  private openLoading(callback?: Function) {
    UIManager.getInstance()
      .openPopupSystemV2(LoadingPopupSystem, GameDefine.LoadingPopupSystem, false, null, null)
      .then(() => {
        callback();
      });
  }

  private closeLoading() {
    UIManager.getInstance().closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
  }

  private openRetry() {
    EventManager.fire(EventManager.ON_SHOW_RETRY_POPUP);
  }

  private _initComplete: () => void = null;
  private _timerIntervalUpdate = null;
  private _start: boolean = false;
  static destroy() {
    //cc.log("destroy");
    let self = BenTauHandler.instance;
    if (!self?.client) return;
    self.client.close(CodeSocket.FORCE_CLOSE, "");
    clearInterval(self._timerIntervalUpdate);
    BenTauHandler.instance._start = false;
    BenTauHandler.instance = null;
  }

  private subscribes: Map<Topic, Map<string, Function>> = new Map<Topic, Map<string, Function>>();
  private msgsSend: Map<string, RequestMessage> = new Map<string, RequestMessage>();
  private keydefault: string = "keydefault";

  onSubscribe(topic: Topic, onReceive: (msg: BenTauMessage) => void, key: string = ""): void {
    if (key == "") {
      key = this.keydefault;
    }
    if (this.subscribes.has(topic)) {
      let e = this.subscribes.get(topic);
      e.set(key, onReceive);
      this.subscribes.set(topic, e);
    } else {
      let e = new Map<string, Function>();
      e.set(key, onReceive);
      this.subscribes.set(topic, e);
    }
  }

  onUnSubscribe(topic: Topic, key: string = ""): void {
    if (key == "") {
      key = this.keydefault;
    }
    if (this.subscribes.has(topic)) {
      let e = this.subscribes.get(topic);
      if (e.has(key)) {
        e.delete(key);
        this.subscribes.set(topic, e);
      }
    }
  }

  init(onComplete: () => void = null) {
    //cc.log("init betau WS");
    let self = BenTauHandler.instance;
    self._initComplete = onComplete;

    this.openLoading(() => {
      //cc.log("Connecting to BenTau");
      if (!self?.client) {
        self.client = new WSClient();
      }
      self.client.open(
        ConnectDefine.addressBenTauWS,
        self.handleOpen.bind(this),
        self.onMessage.bind(this),
        self.onClose.bind(this),
        self.onError.bind(this),
        self.onReconnect.bind(this)
      );
    });
  }

  openNewWebsocket() {
    // console.log("openNewWebsocket");
    let self = BenTauHandler.instance;
    if (!self?.client) {
      self.client = new WSClient();
    }
    self.client.open(
      ConnectDefine.addressBenTauWS,
      self.handleReconnected.bind(this),
      self.onMessage.bind(this),
      self.onClose.bind(this),
      self.onError.bind(this),
      self.onReconnect.bind(this)
    );
  }

  cleanConnection() {}
  private countReconnect: number = 0;
  private MAX_COUNT_RECONNECT: number = 3;
  doReconnect(onReconnectingFunc: Function = null, failedReconnectFunc: Function = null) {
    //cc.log("Reconnecting with api verifyTokenReconnect ");
    // onReconnectingFunc();
    this.countReconnect++;
    IdentityHandler.getInstance().verifyTokenReconnect(
      () => {
        BenTauHandler.instance.openNewWebsocket();
      },
      (msgError, code) => {
        cc.log("msgError = " + msgError);
        cc.log("code = " + code);
        //cc.log("countReconnect = " + this.countReconnect + " " + this.MAX_COUNT_RECONNECT);
        if (code == Code.UNKNOWN) {
          // failedReconnectFunc(); // reconnect lại
          if (this.countReconnect < this.MAX_COUNT_RECONNECT) {
            this.doReconnect();
          } else {
            this.countReconnect = 0;
            this.closeLoading();
            this.openRetry();
          }
        } else {
          let messError = PortalText.POR_MESS_ACCOUNT_LOGIN_OTHER_DEVICE;
          EventManager.fire(EventManager.ON_DISCONNECT, CodeSocket.KOTIC, false, messError);
        }
      }
    );
  }

  send(
    topic: Topic,
    channelId: string,
    payload: Uint8Array,
    reply: (msgReply) => void = null,
    onError: (msgError: string, code: Code) => void = null,
    loadingBg = true,
    loading = true
  ): string {
    if (!BenTauHandler.instance?.client) return null;
    loading = reply != null && loading;
    let request = new BenTauMessage();
    request.setTopic(topic);
    request.setChannelId(channelId);

    var msgId = BenTauHandler.instance.getUniqueId();
    request.setMsgId(msgId);

    // Put  to BenTauMessage
    request.setPayload(payload);

    let sendRequest = () => {
      ////cc.log("sendRequest.........", msgId);
      BenTauHandler.instance.client.send(request.serializeBinary());
    };

    let msg = new RequestMessage();
    msg.msgId = msgId;
    msg.request = sendRequest;
    msg.response = reply;
    msg.onError = onError;
    msg.onTimeOut = (msgIdTimeOut) => this.onMsgTimeOut(msgIdTimeOut);
    msg.loading = loading;
    msg.loadingBg = loadingBg;

    this.sendRequest(msg);
    return msgId;
  }

  private sendRequest(msg: RequestMessage) {
    //chỉ những msg nào chờ response mới cache lại
    if (msg.response != null) this.msgsSend.set(msg.msgId, msg);
    if (msg.loading) {
      this.openLoading(() => {
        msg.request();
      });
    } else {
      msg.request();
    }
  }

  onReceiveMsgReply(msgReply: BenTauMessage): void {
    let status = msgReply.getStatus();
    if (status) {
      let code = status.getCode();
      let err = status.getMessage();

      let msgId = msgReply.getMsgId();
      let msgRequest: RequestMessage = null;
      let self = BenTauHandler.getInstance();
      if (self.msgsSend.has(msgId)) {
        msgRequest = self.msgsSend.get(msgId);
        self.msgsSend.delete(msgId);
      }
      if (msgRequest) {
        if (msgRequest.loading) {
          self.closeLoading();
        }
        switch (code) {
          case Code.OK:
            if (msgRequest.response) msgRequest.response(msgReply);
            break;
          default:
            // request fail
            if (msgRequest.onError) msgRequest.onError(err);
            break;
        }
      }
    }
  }

  private handUpdateTimeOut() {
    let dt = 500;
    BenTauHandler.instance._timerIntervalUpdate = setInterval(() => this.updateTimeOut(dt), dt);
  }

  private updateTimeOut(dt) {
    if (!BenTauHandler.instance?._start) return;
    this.msgsSend.forEach((msg) => {
      msg.update(dt);
    });
    StabilizeConnection.update(dt);
  }

  private onMsgTimeOut(msgId) {
    if (this.msgsSend.has(msgId)) {
      let msgTimeOut = this.msgsSend.get(msgId);
      this.msgsSend.delete(msgId);
      if (msgTimeOut.loading) {
        this.closeLoading();
      }
      if (msgTimeOut.onError) {
        msgTimeOut.onError("Request timeout", Code.UNKNOWN);
      }
    }
  }

  protected getUniqueId(): string {
    var d = new Date().getTime(); //Timestamp
    var d2 = (performance && performance.now && performance.now() * 1000) || 0; //Time in microseconds since page-load or 0 if unsupported
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
      var r = Math.random() * 16; //random number between 0 and 16
      if (d > 0) {
        //Use timestamp until depleted
        r = (d + r) % 16 | 0;
        d = Math.floor(d / 16);
      } else {
        //Use microseconds since page-load if supported
        r = (d2 + r) % 16 | 0;
        d2 = Math.floor(d2 / 16);
      }
      return (c === "x" ? r : (r & 0x3) | 0x8).toString(16);
    });
  }

  private handleOpen() {
    if (!BenTauHandler.instance?.client) return;
    this.closeLoading();
    if (BenTauHandler.instance._initComplete) BenTauHandler.instance._initComplete();
    BenTauHandler.instance._start = true;
    StabilizeConnection.send();
  }

  private handleReconnected() {
    if (!BenTauHandler.instance?.client) return;
    //cc.log("Reconnected to BenTau");
    UIManager.getInstance().closeAllPopup(false);
    EventManager.fire(EventManager.ON_RECONNECTED);
    //reconnect event for card games
    cc.systemEvent.emit("ON_RECONNECTED");
    EventBusManager.onFireReconnected();
    BenTauHandler.instance._start = true;
    StabilizeConnection.send();
  }

  private onMessage(msg: BenTauMessage) {
    if (!BenTauHandler.instance?.client) return;
    let topic = msg.getTopic();
    StabilizeConnection.setTimeOutMsg();
    if (this.subscribes.has(topic)) {
      let e = this.subscribes.get(topic);
      e.forEach((f, key) => {
        f(msg);
      });
      this.onReceiveMsgReply(msg);
    }
  }

  onError(errCode, err) {
    // if (!BenTauHandler.instance?.client) return;
    EventManager.fire(EventManager.ON_DISCONNECT, CodeSocket.FORCE_CLOSE);
  }

  private onClose(code: number, reason: string) {
    //cc.log("Bentau ws onClose " + code + " " + reason);
    EventManager.fire(EventManager.ON_DISCONNECT, code);
  }

  private onReconnect(code: number, reason: string) {
    //cc.log("Bentau ws onReconnect " + code + " " + reason);
    BenTauHandler.instance.doReconnect();
  }
}

import ConnectDefine from "../Define/ConnectDefine";
import UIManager from "../Manager/UIManager";
import GameDefine from "../Define/GameDefine";
import LoadingPopupSystem from "../Popup/LoadingPopupSystem";
import WSClient from "../Network/WebSocketClient/WSClient";
import StabilizeConnection from "./StabilizeConnection";
import EventManager from "../Event/EventManager";
import { CodeSocket } from "../Utils/EnumGame";
import IdentityHandler from "./IdentityHandler";
import { BenTauMessage } from "@gameloot/bentau/bentau_pb";
import { Topic } from "@gameloot/topic/topic_pb";
import { Code } from "@marketplace/rpc/code_pb";
import { RequestMessage } from "@gameloot/client-base-handler/build";
import EventBusManager from "../Event/EventBusManager";
import PortalText from "../Utils/PortalText";
