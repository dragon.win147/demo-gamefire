import { HandlerClientBenTauBase } from "@gameloot/client-base-handler";

export default class WalletHandler extends HandlerClientBenTauBase {
  protected static instance: WalletHandler;

  private client: MyWalletClient = null;

  public onReceiveSendEvent: (userId: string, sendMessage: string) => void = null;

  public onReceiveJoinEvent: (evt: Event) => void = null;

  public onReceiveLeaveEvent: (evt: Event) => void = null;

  public onHandleWithDrawFromVaultErr: () => void = null;

  public onHandleWithDrawFromVaultSucceed: (res) => void = null;

  public onVerifyOTPFalse: () => void = null;

  private gamePlayings: Map<Topic, number> = new Map<Topic, number>();

  private listTransactionID: number[] = [];
  private max: number = 50;

  private _currentBalance: number = 0;
  private _currentVaultBalance: number = 0;
  private _currentMainBalance: number = 0;
  private _currentPromotionBalance: number = 0;
  private _xOTPId: string;
  private OTPId: string;
  private _timeRecallOTP: number;
  public get currentBalance(): number {
    return this._currentBalance;
  }
  private setCurrentMainBalance(balance: number) {
    this._currentMainBalance = balance;
  }
  public get CurrentMainBalance(): number {
    return this._currentMainBalance;
  }
  public get CurrentPromotionBalance(): number {
    return this._currentPromotionBalance;
  }
  private setCurrentPromotionBalance(balance: number) {
    this._currentPromotionBalance = balance;
  }

  private setCurrentVaultBalance(balance: number) {
    this._currentVaultBalance = balance;
  }
  public get currentVaultBalance(): number {
    return this._currentVaultBalance;
  }
  private _currentTransaction: number = 0;
  public get currentTransaction(): number {
    return this._currentTransaction;
  }

  static getInstance(): WalletHandler {
    if (!WalletHandler.instance) {
      // cc.log("new getInstance(): WalletHandler");
      WalletHandler.instance = new WalletHandler(BenTauHandler.getInstance());
      WalletHandler.instance.client = new MyWalletClient(ConnectDefine.addressSanBay, {}, null);
      WalletHandler.instance.topic = Topic.MY_WALLET;
      WalletHandler.instance.onSubscribe();
    }
    return WalletHandler.instance;
  }

  static destroy() {
    if (WalletHandler.instance) WalletHandler.instance.onUnSubscribe();
    WalletHandler.instance = null;
  }

  public curChannelId: string;

  private get metaData() {
    return {
      Authorization: "Bearer " + IdentityHandler.getInstance().token,
    };
  }

  private get metaDataWithOTP() {
    return {
      Authorization: "Bearer " + IdentityHandler.getInstance().token,
      ["x-otp-id"]: this._xOTPId,
      ["x-otp"]: this.OTPId,
    };
  }

  public setOTPID(id: string) {
    this.OTPId = id;
  }

  protected showLoading(callback: Function) {
    UIManager.getInstance()
      .openPopupSystemV2(LoadingPopupSystem, GameDefine.LoadingPopupSystem, false, null, null)
      .then(() => {
        callback();
      });
  }

  protected hideLoading() {
    UIManager.getInstance().closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
  }

  protected onShowErr(str: string, onClick?: () => void) {
    UIManager.getInstance().openPopupSystem(WarningPopupSystem, GameDefine.WarningPopupSystem, str, onClick, onClick);
  }

  protected onDisconnect(CodeSocket: CodeSocket, messError: string) {
    EventManager.fire(EventManager.ON_DISCONNECT, CodeSocket, false, messError);
  }

  onMyWalletRequest(response: (response: MeReply) => void = null) {
    let request = new google_protobuf_empty_pb.Empty();
    let self = this;
    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.me(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };

    this.onSendRequest(
      msgId,
      sendRequest,
      (res: MeReply) => {
        let walletAmount = res.getWallet().getCurrentBalance();
        let vaultWalletAmount = res.getWallet().getBalanceInVault();
        let mainBalance = res.getWallet().getCurrentMainBalance();
        let promotionBalance = res.getWallet().getCurrentPromotionBalance();
        let transaction = res.getLastTxId();
        this.setCurrentBalance(walletAmount, transaction);
        this.setCurrentVaultBalance(vaultWalletAmount);
        this.setCurrentMainBalance(mainBalance);
        this.setCurrentPromotionBalance(promotionBalance);
        response(res);
      },
      null,
      false
    );
  }

  putToVault(amount: number, response: (response: PutToVaultReply) => void = null) {
    let request = new PutToVaultRequest();
    request.setAmount(amount);
    let self = this;
    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.putToVault(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };

    this.onSendRequest(
      msgId,
      sendRequest,
      (res: PutToVaultReply) => {
        let walletAmount = res.getWallet().getCurrentBalance();
        let vaultWalletAmount = res.getWallet().getBalanceInVault();
        let mainBalance = res.getWallet().getCurrentMainBalance();
        let promotionBalance = res.getWallet().getCurrentPromotionBalance();
        let transaction = res.getChange().getLastTxId();
        this.setCurrentBalance(walletAmount, transaction);
        this.setCurrentVaultBalance(vaultWalletAmount);
        this.setCurrentMainBalance(mainBalance);
        this.setCurrentPromotionBalance(promotionBalance);
        response(res);
      },
      null,
      false
    );
  }

  onReceive(msg: BenTauMessage) {
    super.onReceive(msg);
    const change = Change.deserializeBinary(msg.getPayload_asU8());
    let topicReceive = change.getTopic();
    //console.log("wallet topic " + topicReceive + " amount change " + change.getAmount() + " amount new " + change.getNewBalance() + " " + change.getHappenedAt());
    if (!this.gamePlayings.has(topicReceive)) {
      this.addMoney(change.getAmount(), change.getLastTxId());
    }
  }

  onEnableGame(topic: Topic) {
    if (!this.gamePlayings.has(topic)) {
      //cc.log("Wallet onEnableGame ............." + topic);
      this.gamePlayings.set(topic, 0);
    }
  }

  onDisableGame(topic: Topic) {
    if (this.gamePlayings.has(topic)) {
      //cc.log("Wallet onDisableGame ............." + topic);
      this.gamePlayings.delete(topic);
    }
    if (this.gamePlayings.size == 0) {
      this.onMyWalletRequest(() => {});
    }
  }

  onReConnect(onComplete: () => void = null): void {
    //BenTauWSHandler.getInstance().onReConnect();
  }

  private setCurrentBalance(value: number, transaction: number) {
    //cc.log("Update setCurrentBalance " + value + " " + transaction);
    this._currentBalance = value;
    this._currentTransaction = transaction;
    EventManager.fire(EventManager.onUpdateBalance, this._currentBalance);
    EventBusManager.onFireUpdateBalance(this._currentBalance);
  }

  //TODO lay balance theo revision
  public getLatestBalance(callback = null) {
    // cc.log("callback 1 " + (callback != null))
    this.onMyWalletRequest((resp) => {
      // cc.log("callback " + (callback != null))
      if (callback) {
        callback(resp);
      }
    });
  }

  public addMoney(amount: number, transaction: number) {
    let allowAddMoney = true;
    //cc.log("wallet " + amount + " " + transaction);
    this.listTransactionID.forEach((saveID) => {
      if (saveID == transaction) {
        allowAddMoney = false;
        return;
      }
    });
    if (allowAddMoney) {
      if (transaction > 0) {
        // cheat ==0 for FISH temp
        if (transaction <= this._currentTransaction) return;
        this.listTransactionID.push(transaction);
      }
      this._currentBalance += amount;
      EventManager.fire(EventManager.onUpdateAddBalance, amount);
      EventBusManager.onFireAddBalance(amount, this._currentBalance);
      if (this.listTransactionID.length > this.max) {
        this.listTransactionID.shift();
      }
    }
  }

  handleCustomError(errCode: Code, err: string) {
    //cc.log("handleCustomError " + errCode);
    if (errCode == Code.OK) return;
    if (errCode == Code.UNKNOWN) return;
    let messError = "";
    switch (errCode) {
      case Code.INSUFFICIENT_BALANCE:
        messError = PortalText.POR_MESS_INSUFFICIENT_BALANCE;
        break;
      default:
        messError = errCode + " - " + err;
        break;
    }
    UIManager.getInstance().openPopupSystem(WarningPopupSystem, GameDefine.WarningPopupSystem, messError, () => {});
  }
}
import ConnectDefine from "../Define/ConnectDefine";
import IdentityHandler from "./IdentityHandler";
import { MeReply, GetHistoryRequest, GetHistoryReply, PutToVaultReply, PutToVaultRequest, WithdrawFromVaultReply, WithdrawFromVaultRequest } from "@gameloot/mywallet/mywallet_pb";
import { MyWalletClient } from "@gameloot/mywallet/MywalletServiceClientPb";
import EventManager from "../Event/EventManager";
import { BenTauMessage } from "@gameloot/bentau/bentau_pb";
import { Topic } from "@gameloot/topic/topic_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import { Code } from "@gameloot/mywallet/mywallet_code_pb";
import UIManager from "../Manager/UIManager";
import WarningPopupSystem from "../Popup/WarningPopupSystem";
import BenTauHandler from "./BenTauHandler";
import LoadingPopupSystem from "../Popup/LoadingPopupSystem";
import { CodeSocket } from "../Utils/EnumGame";
import GameDefine from "../Define/GameDefine";
import EventBusManager from "../Event/EventBusManager";
import PortalText from "../Utils/PortalText";
import { Change } from "@gameloot/mywallet/mywallet_change_pb";
import TextNotify, { TextNotifyData } from "../Manager/TextNotify";
import GameUtils from "../Utils/GameUtils";
