import { Ping } from "@gameloot/bentau/bentau_pb";
import { Topic } from "@gameloot/topic/topic_pb";
import GameUtils from "../Utils/GameUtils";
import TimeUtils from "../Utils/TimeUtils";
import EventManager from "../Event/EventManager";
import BenTauHandler from "./BenTauHandler";
import EventBusManager from "../Event/EventBusManager";
export default class StabilizeConnection {
  public static TIME_TO_PING = 5000;
  public static TIME_TO_PONG = 3000;

  private static pingDict: Map<string, number> = new Map<string, number>();

  private static ping = null;
  private static currentTime = 0;
  private static start: boolean = false;

  public static update(dt) {
    if (!this.start) return;
    if (!GameUtils.isStatusPing) return;
    this.currentTime += dt;
    if (this.currentTime > this.TIME_TO_PONG * 1.5) {
      this.currentTime = 0;
      StabilizeConnection.onPingFailure(this.currentTime);
    }
  }

  public static send() {
    this.clear();
    this.start = true;
    this.pingDict.clear();

    let ping = new Ping();
    ping.setTopic(GameUtils.gameTopic);
    let msgId = BenTauHandler.getInstance().send(Topic.PING, "", ping.serializeBinary());
    if (msgId == null) {
      this.continue();
      return;
    }

    BenTauHandler.getInstance().onSubscribe(Topic.PONG, (msg) => {
      this.onReceive(msg.getMsgId());
    });
    let time = TimeUtils.getCurrentTimeByMillisecond();
    if (msgId != null) {
      this.pingDict.set(msgId, time);
    }
  }

  private static onReceive(msgId: string) {
    if (this.pingDict.has(msgId)) {
      let preTimePing = this.pingDict.get(msgId);
      let pingTime = TimeUtils.getCurrentTimeByMillisecond() - preTimePing;
      this.pingDict.delete(msgId);

      // if (GameUtils.isLogVersion()) {
      //   CheatManager.getInstance().open(PingCheat, "PingCheat", pingTime);
      // }

      EventBusManager.onPingTime(pingTime);
      this.onPingOk(pingTime);
    }
  }

  public static clear() {
    GameUtils.isStatusPing = true;
    BenTauHandler.getInstance().onUnSubscribe(Topic.PONG);
    clearTimeout(this.ping);
    this.ping = null;
    this.start = false;
    this.currentTime = 0;
  }

  private static continue() {
    if (!GameUtils.isStatusPing) return;
    this.ping = setTimeout(() => this.send(), this.TIME_TO_PING);
  }

  private static onPingOk(pingTime: number) {
    this.continue();
    GameUtils.isStatusPing = false;
    EventManager.fire(EventManager.ON_PING_OK);
  }

  private static onPingFailure(pingTime: number) {
    cc.log("PING FAILED:" + pingTime + "ms");
    GameUtils.isStatusPing = false;
    EventManager.fire(EventManager.ON_PING_FAIL);
  }

  public static setTimeOutMsg() {
    this.currentTime = 0;
  }
}
