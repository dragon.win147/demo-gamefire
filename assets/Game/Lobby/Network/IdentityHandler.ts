// import UserManager from "../Managers/UserManager";
import { Empty } from "google-protobuf/google/protobuf/empty_pb";
import { MyIDClient } from "@greyhole/myid/MyidServiceClientPb";
import * as grpcWeb from "grpc-web";
import { CreateAccessTokenReply, CreateAccessTokenRequest, MeReply, SignInReply, SignInV2Request, TokenInfo } from "@greyhole/myid/myid_pb";
import { Code } from "@greyhole/myid/myid_code_pb";
import HandlerClientBase from "@gameloot/client-base-handler/build/HandlerClientBase";

import UIManager from "../Manager/UIManager";
import GameDefine from "../Define/GameDefine";
import TextNotify, { TextNotifyData } from "../Manager/TextNotify";
import ConnectDefine from "../Define/ConnectDefine";
import EventManager from "../Event/EventManager";
import { CodeSocket } from "../Utils/EnumGame";
import LoadingPopupSystem from "../Popup/LoadingPopupSystem";
import WarningPopupSystem from "../Popup/WarningPopupSystem";
import PortalText from "../Utils/PortalText";

const { ccclass, property } = cc._decorator;

@ccclass
export default class IdentityHandler extends HandlerClientBase {
  protected static instance: IdentityHandler;
  private static readonly deviceIdKey = "DEVICE_ID";
  public onHandleChangePassSucceed: (res) => void = null;
  public onHandleLoginSucceed: (res) => void = null;
  public onVerifyOTPFalse: () => void = null;
  public backtoLoginScene: () => void = null;

  static getInstance(): IdentityHandler {
    if (!IdentityHandler.instance) {
      IdentityHandler.instance = new IdentityHandler();
      IdentityHandler.instance.client = new MyIDClient(ConnectDefine.addressSanBay, {}, null);
    }
    return IdentityHandler.instance;
  }

  static destroy() {
    IdentityHandler.instance = null;
  }
  public getNewCaptcha: () => void = null;

  public closeLoginPopup: () => void = null;
  public token: string;
  private get metaData() {
    return {
      Authorization: "Bearer " + IdentityHandler.getInstance().token,
    };
  }

  protected showLoading(callback: Function) {
    UIManager.getInstance()
      .openPopupSystemV2(LoadingPopupSystem, GameDefine.LoadingPopupSystem, false, null, null)
      .then(() => {
        callback();
      });
  }

  protected hideLoading() {
    UIManager.getInstance().closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
  }

  protected onShowErr(str: string, onClick?: () => void) {
    let code = parseInt(str.split("-")[0]) as Code;
    if (!isNaN(code)) {
      let messError = "";
      switch (code) {
        case Code.INTERNAL:
        case Code.UNAVAILABLE:
          messError = PortalText.POR_MESS_INTERNAL;
          UIManager.getInstance().openPopupSystem(WarningPopupSystem, GameDefine.WarningPopupSystem, messError, onClick, onClick);
          return;
      }
    }

    UIManager.getInstance().openPopupSystem(WarningPopupSystem, GameDefine.WarningPopupSystem, str, onClick, onClick);
  }

  protected onDisconnect(CodeSocket: CodeSocket, messError: string) {
    EventManager.fire(EventManager.ON_DISCONNECT, CodeSocket, false, messError);
  }

  private client: MyIDClient = null;

  onLogin(username: string, password: string, response: (response: SignInReply) => void, onErr: () => void = null) {
    console.log("onLogin " + username + "," + password);
    // this.onHandleLoginSucceed = response;
    let request = new SignInV2Request();
    let myId = new SignInV2Request.MyID();
    myId.setUsername(username);
    myId.setPassword(password);
    request.setMyId(myId);
    request.setDeviceId("");
    request.setDeviceName("");
    let self = this;
    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.signInV2(request, self.metaData, (err: grpcWeb.RpcError, res: SignInReply) => {
        if (err && onErr) onErr();
        cc.log("res", res?.toObject());
        // if (res?.toObject().hasOwnProperty('confirmOtp')) {

        // if (res?.toObject().confirmOtp) {
        //   if (res.getConfirmOtp().getWaiting() > 0) {
        //     let message = GameUtils.FormatString(PortalText.POR_NOTIFY_WARNING_RESET_OTP, res.getConfirmOtp().getWaiting());
        //     UIManager.getInstance().openNotify(TextNotify, GameDefine.TextNotify, new TextNotifyData(message));
        //     self.onSendReply(err, res, msgId);
        //     return;
        //   }
        //   let data = new RequireOTPData();
        //   data.username = username;
        //   data.password = password;
        //   data.requireOTPType = REQUIREOTP.LOGIN;
        //   data.otpId = res.getConfirmOtp().getSuccess().getId();
        //   data.timeRecallOTP = res.getConfirmOtp().getSuccess().getExpiry();

        //   UIManager.getInstance().openPopupWithErrorHandler(InputOTPPopup, "InputOTPPopup", data, UIManager.getInstance().LayerUpPopup, false);
        //   self.onSendReply(err, res, msgId);
        //   return;
        // }
        self.handleResponseLogin(err, res?.getTokenInfo(), msgId, username);
      });
    };
    this.onSendRequest(
      msgId,
      sendRequest,
      (res: SignInReply) => {
        if (res?.toObject().confirmOtp) {
          return;
        }
        response && response(res);
      },
      null,
      true
    );
  }

  private handleResponseLogin(err: grpcWeb.RpcError, tokenInfo: TokenInfo, msgId: string, username: string) {
    cc.log("handleResponseLogin");
    if (err != null) {
      // console.log("err " + err);
    } else {
      // Todo in request logic
      if (tokenInfo) {
        this.token = tokenInfo.getAccessToken();
        // UserManager.getInstance().userData.displayName = "";
        // UserManager.getInstance().userData.userId = tokenInfo.getSafeId();
        // UserManager.getInstance().saveInfoLogin(tokenInfo.getIdToken());
        // UserManager.getInstance().setUserName(tokenInfo.getUsername());
        // console.log("AccessToken " + this.token);
        // console.log("IdToken " + tokenInfo.getIdToken());
        // cc.director.loadScene("SplashScene", () => {
        //   MarketPlace.showPromotionPopup = true;
        // });
      } else {
        //TODO : fix tạm thời cho trường hợp native vì 'grpcWeb.Error' trên native return null
        let error: grpcWeb.RpcError = {
          code: 2,
          metadata: null,
          name: "",
          message: "",
        };
        err = error;
      }
    }
    this.onSendReply(err, tokenInfo, msgId);
  }

  getAccessToken(idToken: string, response: (response: CreateAccessTokenReply) => void) {
    let request = new CreateAccessTokenRequest();
    request.setIdToken(idToken);

    let self = this;
    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.createAccessToken(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };
    this.onSendRequest(
      msgId,
      sendRequest,
      (res: CreateAccessTokenReply) => {
        this.token = res.getAccessToken();
        cc.log("this.token", this.token);
        // UserManager.getInstance().saveInfoLogin(idToken);
        response(res);
      },
      null,
      false
    );
  }

  verifyAccessToken(token: string, response: (response: MeReply) => void, onError: (msgError: string, code: Code) => void = null) {
    let request = new Empty();

    let self = this;
    var msgId = self.getUniqueId();
    let sendRequest = () => {
      self.client.me(request, self.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };
    this.onSendRequest(msgId, sendRequest, response, onError, false, false, 5);
  }

  verifyTokenReconnect(onSuccess: Function = null, onError: (msgError: string, code: any) => void = null) {
    // cc.log("verifyTokenReconnect token = " + this.token);
    if (this.token != "") {
      this.verifyAccessToken(
        this.token,
        (response) => {
          cc.log("verifyTokenReconnect success");
          onSuccess && onSuccess();
        },
        (msgError: string, code: Code) => {
          cc.log("verifyTokenReconnect onFail");
          onError && onError(msgError, code);
        }
      );
    }
  }

  me(response: (response: MeReply) => void) {
    let request = new Empty();

    let self = this;
    var msgId = self.getUniqueId();

    let sendRequest = () => {
      self.client.me(request, this.metaData, (err, res) => {
        self.onSendReply(err, res, msgId);
      });
    };
    this.onSendRequest(msgId, sendRequest, response, null, false);
  }

  handleCustomError(errCode: number, err: string) {
    if (errCode == Code.UNKNOWN) return;
    if (errCode == Code.OK) return;
    let messError = "";
    UIManager.getInstance().openNotify(TextNotify, GameDefine.TextNotify, new TextNotifyData(messError, 2));
  }
}
