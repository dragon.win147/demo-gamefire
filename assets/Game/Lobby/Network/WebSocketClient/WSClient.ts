// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import WSClient_Android from "./WSClient_Android";
import WSClient_WebAndIOS from "./WSClient_WebAndIOS";

const { ccclass, property } = cc._decorator;

@ccclass
export default class WSClient {
  public ws_android_client: WSClient_Android = new WSClient_Android();
  public ws_webios_client: WSClient_WebAndIOS = new WSClient_WebAndIOS();
  public open(address, onOpen, onMessage, onClose, onError, onReconnect) {
    cc.log("Open new WS");
    if (cc.sys.platform == cc.sys.ANDROID) {
      this.ws_android_client.open(address, onOpen, onMessage, onClose, onError, onReconnect);
    } else {
      this.ws_webios_client.open(address, onOpen, onMessage, onClose, onError, onReconnect);
    }
  }

  public send(data) {
    if (cc.sys.platform == cc.sys.ANDROID) {
      this.ws_android_client.send(data);
    } else {
      this.ws_webios_client.send(data);
    }
  }

  public close(code: number, reason: string) {
    cc.log("WSClient close ");
    if (cc.sys.platform == cc.sys.ANDROID) {
      this.ws_android_client.closeWS(code, reason);
    } else {
      this.ws_webios_client.closeWS(code, reason);
    }
  }

  public reConnect(code: number, reason: string) {
    cc.log("WSClient reConnect ");
    if (cc.sys.platform == cc.sys.ANDROID) {
      this.ws_android_client.reConnect(code, reason);
    } else {
      this.ws_webios_client.reConnect(code, reason);
    }
  }
}
