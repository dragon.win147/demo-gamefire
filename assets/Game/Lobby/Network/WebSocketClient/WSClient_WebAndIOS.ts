import { CodeSocket } from "../../Utils/EnumGame";
import IdentityHandler from "../../Network/IdentityHandler";
import { BenTauMessage } from "@gameloot/bentau/bentau_pb";

const { ccclass, property } = cc._decorator;

@ccclass
export default class WSClient_WebAndIOS extends cc.Component {
  private onClose;
  private onReconnect;
  private websocket: WebSocket = null;
  private isError: boolean = false;
  public open(address, onOpen, onMessage, onClose, onError, onReconnect) {
    this.isError = false;
    this;
    try {
      this.websocket = new WebSocket(address, [IdentityHandler.getInstance().token]);
    } catch (error) {
      const code = CodeSocket.FORCE_CLOSE;
      const reason = "Đường truyền mạng không ổn định!";
      onError && onError(code, reason);
    }
    var self = this;
    this.websocket.binaryType = "arraybuffer";
    this.onClose = onClose;
    this.onReconnect = onReconnect;
    this.websocket.onopen = function () {
      onOpen && onOpen();
    };

    this.websocket.onmessage = function (mgs) {
      var buffer = new Uint8Array(mgs.data);
      var benTauMessage = BenTauMessage.deserializeBinary(buffer);
      // Parse data
      onMessage && onMessage(benTauMessage);
    };

    this.websocket.onclose = function (message: CloseEvent) {
      // Hiện tại không cần xử lý case này, case này là hệ thông tự đóng, còn các trường hợp manual close thì đã xử lý.
      //cc.log("System onClose code " + self.isError + " " + message.code + " message " + message.reason + " " + self.idWS);
    };

    this.websocket.onerror = function (message: CloseEvent) {
      self.isError = true;
      const code = message.code;
      const reason = message.reason;
      //cc.log("onerror code " + message.code + " message " + message.reason + " " + self.idWS);
      // onError && onError(code, reason);
    };
  }

  public send(data) {
    if (this.websocket.readyState == WebSocket.OPEN) this.websocket.send(data);
  }

  public reConnect(code: number, reason: string) {
    // //cc.log("reConnect = " + code + " reason = " + reason + " idWS " + this.idWS + " isOpen " + this.websocket.readyState);
    this.onReconnect && this.onReconnect(code, reason);
  }

  public closeWS(code: number, reason: string) {
    //cc.log("closeWS = " + code + " reason = " + reason + " idWS " + this.idWS + " isOpen " + this.websocket.readyState);
    if (this.websocket == null || this.websocket.readyState == WebSocket.CLOSED) return;
    this.websocket.close(CodeSocket.FORCE_CLOSE);
  }
}
