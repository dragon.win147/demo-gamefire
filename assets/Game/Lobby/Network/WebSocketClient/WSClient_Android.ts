import IdentityHandler from "../../Network/IdentityHandler";
import { BenTauMessage } from "@gameloot/bentau/bentau_pb";

const { ccclass, property } = cc._decorator;

@ccclass
export default class WSClient_Android extends cc.Component {
  private onOpenCb: Function;
  private onMessageCb: Function;
  private onCloseCb: Function;
  private onErrorCb: Function;
  private onReconnect: Function;
  private isOpen = false;
  private isError = false;
  public open(address, onOpen, onMessage, onClose, onError, onReconnect) {
    console.log("WSClient_Android::receive");
    this.onOpenCb = onOpen;
    this.onMessageCb = onMessage;
    this.onCloseCb = onClose;
    this.onErrorCb = onError;
    this.onReconnect = onReconnect;
    this.isOpen = true;
    this.isError = false;
    // Native.getInstance().call("createWebSocketClient", address, IdentityHandler.getInstance().token);
    cc.systemEvent.on("WSClient_Android_open", () => {
      this.onOpenCb && this.onOpenCb();
    });

    cc.systemEvent.on("WSClient_Android_message", (base64_string) => {
      var data = Uint8Array.from(atob(base64_string), (c) => c.charCodeAt(0));
      var benTauMessage = BenTauMessage.deserializeBinary(data);
      this.onMessageCb && this.onMessageCb(benTauMessage);
    });

    cc.systemEvent.on("WSClient_Android_close", (code, reason) => {
      // Hiện tại không cần xử lý case này, case này là hệ thông tự đóng, còn các trường hợp manual close thì đã xử lý.
      cc.log("WSClient_Android_close " + code + " " + reason);
      this.isOpen = false;
      this.offEvent();
    });

    cc.systemEvent.on("WSClient_Android_error", (msg) => {
      cc.log("WSClient_Android_error " + msg);
      this.offEvent();
      this.isError = false;
      // this.onErrorCb && this.onErrorCb(msg);
    });
  }
  public send(data) {
    cc.log("WSClient_Android send");
    var base64_string = btoa(String.fromCharCode.apply(null, new Uint8Array(data)));
    jsb.reflection.callStaticMethod("org/cocos2dx/javascript/NativeWebSocketClient", "send", "(Ljava/lang/String;)V", base64_string);
  }

  public closeWS(code: number, reason: string) {
    this.offEvent();
    if (!this.isOpen) return;
    this.isOpen = false;
    this.onCloseCb && this.onCloseCb(code, reason);
    jsb.reflection.callStaticMethod("org/cocos2dx/javascript/NativeWebSocketClient", "close", "()V");
  }

  public reConnect(code: number, reason: string) {
    // cc.log("reConnect = " + code + " reason = " + reason + " idWS " + this.idWS + " isOpen " + this.websocket.readyState);
    this.onReconnect && this.onReconnect(code, reason);
  }

  public offEvent() {
    cc.systemEvent.off("WSClient_Android_open");
    cc.systemEvent.off("WSClient_Android_message");
    cc.systemEvent.off("WSClient_Android_close");
    cc.systemEvent.off("WSClient_Android_error");
  }
}
