import EventManager from "../Event/EventManager";
import PopupSystemInstance from "../Base/PopupSystemInstance";

const { ccclass, property } = cc._decorator;

@ccclass
export default class RetryPopupSystem extends PopupSystemInstance {
  @property(cc.Label)
  content: cc.Label = null;

  @property(cc.Integer)
  limitReconnectTime: number = 3;

  @property(cc.Integer)
  timeToReconnect: number = 3;

  @property(cc.Integer)
  timeDelayConfirm: number = 3;
  public onShow(data = null) {
    this.node.active = true;
  }
  public onClickCancel() {
    cc.log("onClickCancel");
    EventManager.fire(EventManager.ON_DISCONNECT, 0, false);
    this.onHide();
  }

  public onClickRetry() {
    cc.log("onClickRetry");
    EventManager.fire(EventManager.ON_PING_FAIL, 0, true);
    this.onHide();
  }

  public onHide() {
    this.node.active = false;
  }

  protected afterShow() {}

  protected beforeShow() {}

  protected beforeClose() {}

  protected afterClose() {}
}
