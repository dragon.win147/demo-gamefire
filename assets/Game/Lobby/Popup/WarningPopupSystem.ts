// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import PopupSystemInstance from "../Base/PopupSystemInstance";

const { ccclass, property } = cc._decorator;

@ccclass
export default class WarningPopupSystem extends PopupSystemInstance {
  @property(cc.Label)
  content: cc.Label = null;

  protected onShow(data) {
    this.content.string = data;
  }

  protected afterShow() {}

  protected beforeShow() {}

  protected beforeClose() {}

  protected afterClose() {}
}
