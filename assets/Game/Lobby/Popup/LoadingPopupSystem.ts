import PopupSystemInstance from "../Base/PopupSystemInstance";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LoadingPopupSystem extends PopupSystemInstance {
  protected onShow(data) {
    // cc.error("show loading popup system");
    if (this.background) {
      this.background.node.active = false;
      cc.Tween.stopAllByTarget(this.background.node);
      cc.tween(this.background.node)
        .delay(1)
        .call(() => {
          this.background.node.active = true;
        })
        .start();
    }
  }

  protected afterShow() {}

  protected beforeShow() {}

  protected beforeClose() {}

  protected afterClose() {}

  onDisable() {
    cc.Tween.stopAllByTarget(this.background.node);
  }
}
