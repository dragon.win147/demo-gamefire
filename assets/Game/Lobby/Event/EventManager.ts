// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import HookEvent from "./HookEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class EventManager {
  public static onLogout: string = "onLogout";
  public static onCloseAllMiniGame: string = "onCloseAllMiniGame";
  public static onLoginSuccess: string = "onLoginSuccess";
  public static onGetProfile: string = "onGetProfile";
  public static onGetProfileDone: string = "onGetProfileDone";
  public static onChangeProfile: string = "onChangeProfile";
  public static onHandleEventAfterLoginSuccess: string = "onHandleEventAfterLoginSuccess";
  public static onCheckCurrentChannel: string = "onCheckCurrentChannel";
  public static ON_AUTO_LOGIN_FAIL = "OnAutoLogicFail";
  public static ON_HIDE_GAME: string = "onHideGame";
  public static ON_SHOW_GAME: string = "onShowGame";
  public static ON_SHOW_RETRY_POPUP: string = "onShowRetryPopup";
  public static ON_LOAD_JACKPOT: string = "onLoadJackpot";
  public static ON_CHECK_NEW_MAIL: string = "checkNewMail";
  public static ON_CHECK_CLAIM_REWARD: string = "checkClaimReward";
  public static ON_SHOW_ERROR_WHEN_DOMAIN_DIE: string = "ON_SHOW_ERROR_WHEN_DOMAIN_DIE";
  //Wallet event
  public static onUpdateAddBalance: string = "onUpdateAddBalance";
  public static onUpdateBalance: string = "onUpdateBalance";
  // public static onUpdateFakeBalance: string = "onUpdateFakeBalance";

  // Game state event
  public static ON_MAIN_GAME = "OnMainGame";
  public static ON_FREE_GAME = "OnFreeGame";
  public static ON_BONUS_GAME = "OnBonusGame";
  public static ON_BONUS_SPIN_GAME = "OnBonusSpinGame";

  // UI Slot event
  public static ON_PRESS_SPIN = "OnSpin";
  public static ON_FREE_SPIN = "OnFreeSpin";
  public static ON_BONUS_SPIN = "OnBonusSpin";
  public static ON_BONUS_GAME_START = "OnBonusGameStart";
  public static ON_PRESS_STOP_IMMEDIATELY = "OnStopImmediately";
  public static ON_PRESSED_TOGGLE_FAST_SPIN = "OnToggleFastSpin";
  public static ON_PRESSED_INCREASE_BET = "OnIncreaseBet";
  public static ON_PRESSED_DECREASE_BET = "OnDecreaseBet";
  public static ON_PRESSED_INCREASE_LINE = "OnIncreaseLine";
  public static ON_PRESSED_DECREASE_LINE = "OnDecreaseLine";
  public static ON_PRESSED_CLOSE = "OnPressClose";
  public static ON_UPDATE_FREESPIN_COUNT = "OnUpdateFreeSpinCount";
  public static ON_UPDATE_BONUSSPIN_COUNT = "OnUpdateBonusSpinCount";
  public static ON_SHOW_WIN_AMOUNT = "OnShowWinAmount";
  public static ON_SHOW_WALLET_AMOUNT = "OnShowWalletAmount";
  public static ON_TRIGGER_AUTO_SPIN = "OnTriggerAutoSpin";
  public static ON_HIDE_FREE_SPIN = "OnHideFreeSpin";
  public static ON_WALLET_SLOT = "OnWalletSlot";

  // Info events
  public static ON_OPEN_INFO = "OnOpenInfo";
  public static ON_CLOSE_INFO = "OnCloseInfo";
  public static ON_CHANGED_INFO = "OnChangedInfo";

  // History events
  public static ON_OPEN_HISTORY = "OnOpenHistory";
  public static ON_CLOSE_HISTORY = "OnCloseHistory";

  // Mission events
  public static ON_CLOSE_EVENT_POPUP = "OnCloseEventPopup";
  public static ON_SHOW_MISSION = "OnShowMission";
  // JACKPOT History events
  public static ON_OPEN_JACKPOT_HISTORY = "OnOpenJackpotHistory";
  public static ON_CLOSE_JACKPOT_HISTORY = "OnCloseJackpotHistory";

  // Leaderboard detail events
  public static ON_OPEN_LEADERBOARD_DETAIL = "OnOpenLeaderboardDetail";
  public static ON_CLOSE_LEADERBOARD_DETAIL = "OnCloseLeaderboardDetail";

  // Logic events
  public static ON_GAME_START = "OnGameStarted";
  public static ON_GAME_BACK = "OnGameBack";
  public static ON_DOUBLE_START = "OnDoubleStarted";
  public static ON_BET_CHANGED = "OnBetChanged";
  public static ON_LINE_CHANGED = "OnLineChanged";
  public static ON_TOTAL_BET_CHANGED = "OnTotalBetChanged";
  public static ON_RECEIVED_SLOT_DATA = "OnReceivedSlotData";
  public static ON_RECEIVED_BONUS_SPIN_DATA = "OnReceivedBonusSpinData";
  public static ON_HAS_BONUS_SPIN = "OnHasBonusSpin";
  public static ON_FINISH_BONUS_GAME = "OnFinishBonusGame";
  public static ON_RECEIVED_BONUS_DATA = "OnReceivedBonusData";
  public static ON_RECEIVED_BONUS_RESULT_DATA = "OnReceivedBonusResultData";
  public static ON_SLOT_MACHINE_COLUMN_BOUNCE = "OnSlotMachineColumnBounce";
  public static ON_SLOT_MACHINE_STATE_CHANGED = "OnSlotMachineStateChanged";
  public static ON_SLOT_MACHINE_STOPPED = "OnSlotMachineStopped";
  public static ON_SHOW_LINES = "OnShowLines";
  public static ON_END_BONUS_SPIN_TURN = "OnEndBonusSpinTurn"; // Kết thúc hoàn toàn 1 lượt quay bonus
  public static ON_END_SPIN_TURN = "OnEndSpinTurn"; // Kết thúc hoàn toàn 1 lượt quay
  public static ON_END_FREE_SPIN_TURN = "OnEndFreeSpinTurn"; // Kết thúc hoàn toàn 1 lượt quay
  public static ON_JACKPOT_CHANGED = "OnJackpotChanged";
  public static ON_ACTIVATED_TENSION_SPIN = "OnActivatedTensionSpin";
  public static ON_SHOW_TENSION_SPIN = "OnShowTensionSpin";
  public static ON_SHOW_SPECIAL_WIN = "OnShowSpecialWin";
  public static COLUMN_STOPPED = "OnColumnStopped";
  public static ON_SPINABLE = "OnSpinable";
  public static UPDATE_AVATAR_FRAME = "updateAvatarFrame";
  public static HUB_MINI_GAME_LOCATION = "hubMiniGameLocation";
  public static UPDATE_BADGE_LEVEL_UP = "UpdateBadgeLevelUp";
  private static event_mapping = {};

  //WS
  public static ON_RECONNECTED = "OnReconnect";
  public static ON_DISCONNECT = "OnDisconnect";
  public static ON_PING_FAIL = "OnPingFail";
  public static ON_PING_OK = "OnPingOk";
  public static ON_RECONNECTING = "OnReconnecting";

  public static ON_SUBSCRIBE_AMOUNT_TXMN = "ON_SUBSCRIBE_AMOUNT_TXMN";

  // public static ON_SCREEN_CHANGE_SIZE = "OnScreenChangeSize";

  public static ON_CHANGE_WEB_MOBILE_NODE_TO_HIGHEST = "OnChangeWebMobileNodeToHighest";

  public static getEventCallbackLength(evt: string) {
    if (this.event_mapping[evt] == undefined) {
      return 0;
    }

    return this.event_mapping[evt].length;
  }

  public static on(evt: string, cb: Function, attachedObject: cc.Component, identity: string = "") {
    evt += identity != "" ? "_" + identity : "";
    if (this.event_mapping[evt] == undefined || this.event_mapping[evt] == null) {
      this.event_mapping[evt] = [];
    }
    this.event_mapping[evt].push(cb);

    attachedObject &&
      attachedObject.addComponent(HookEvent).init(() => {
        this.off(evt, cb);
      });
  }

  public static off(evt: string, callback: Function = null, identity: string = "") {
    evt += identity != "" ? "_" + identity : "";
    if (this.event_mapping[evt] == undefined) {
      return;
    }
    var cbs: Function[] = this.event_mapping[evt];
    if (callback != null) this.event_mapping[evt] = cbs.filter((cb) => callback != cb);
    else this.event_mapping[evt] = [];
  }

  public static fire(evt: string, args1 = null, args2 = null, args3 = null, args4 = null, args5 = null, identity: string = "") {
    evt += identity != "" ? "_" + identity : "";
    if (this.event_mapping[evt] == undefined || this.event_mapping[evt] == null) {
      return;
    }

    var cbs: Function[] = this.event_mapping[evt];
    cbs.forEach((cb) => cb(args1, args2, args3, args4, args5));
  }
}
