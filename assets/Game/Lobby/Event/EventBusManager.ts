import { Topic } from "@gameloot/topic/topic_pb";
import GameDefine from "../Define/GameDefine";
import WalletHandler from "../Network/WalletHandler";
import GameUtils from "../Utils/GameUtils";
import UIManager from "../Manager/UIManager";
import LoadingPopupSystem from "../Popup/LoadingPopupSystem";
import WarningPopupSystem from "../Popup/WarningPopupSystem";
import EventKey from "@gameloot/client-base-handler/build/EventKey";
import TextNotify, { TextNotifyData } from "../Manager/TextNotify";

import PortalText from "../Utils/PortalText";
import EventManager from "../Event/EventManager";

const { ccclass, property } = cc._decorator;
/**
 * Cách bắn event từ project cảu game qua
 * Sample : 
 * cc.systemEvent.emit(EventKey.ON_OPEN_POPUP_WARNING_EVENT, "Đây là thông báo lỗi", () => {
      cc.log("open warning popup success");
    });
 * 
 *  cc.systemEvent.emit(EventKey.ON_OPEN_POPUP_LOADING_EVENT, () => {
      cc.log("open loading popup success");
    },0);
 * 
 *  *  cc.systemEvent.emit(EventKey.ON_GET_CURRENT_BALANCE_EVENT, (currentBalance) => {
      cc.log("Current balance = " + currentBalance);
    });
 * 
 * 
 */
@ccclass
export default class EventBusManager {
  public static onRegisterEvent() {
    //#region ======== popup system ========
    EventBusManager.removeAllEvent();
    cc.systemEvent.on(EventKey.ON_OPEN_POPUP_DISCONNECT_EVENT, (code, callback) => {});

    cc.systemEvent.on(EventKey.ON_OPEN_POPUP_WARNING_EVENT, (msg, callback) => {
      UIManager.getInstance().openPopupSystem(WarningPopupSystem, GameDefine.WarningPopupSystem, msg);
      callback && callback();
    });
    //#region ======== popup loading ========
    cc.systemEvent.on(EventKey.ON_OPEN_POPUP_LOADING_EVENT, (callback) => {
      UIManager.getInstance()
        .openPopupSystemV2(LoadingPopupSystem, GameDefine.LoadingPopupSystem, false, null, null)
        .then(() => {
          callback && callback();
        });
    });
    cc.systemEvent.on(EventKey.ON_CLOSE_POPUP_LOADING_EVENT, () => {
      UIManager.getInstance().closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
    });

    //#region ======== balance ========
    cc.systemEvent.on(EventKey.ON_ADD_CURRENT_BALANCE_EVENT, (amount: number, transaction: number) => {
      WalletHandler.getInstance().addMoney(amount, transaction);
    });

    cc.systemEvent.on(EventKey.GET_CURRENT_BALANCE_EVENT, (callback) => {
      cc.log("GET_CURRENT_BALANCE_EVENT");
      let currentBalance = WalletHandler.getInstance().currentBalance;
      callback && callback(currentBalance);
    });

    cc.systemEvent.on(EventKey.ON_ENABLE_WALLET_EVENT, (topic: number, callback) => {
      // console.error(EventKey.ON_ENABLE_WALLET_EVENT, topic);
      WalletHandler.getInstance().onEnableGame(topic);
      callback && callback();
    });

    cc.systemEvent.on(EventKey.ON_DISABLE_WALLET_EVENT, (topic: number, callback) => {
      // console.error(EventKey.ON_DISABLE_WALLET_EVENT, topic);

      WalletHandler.getInstance().onDisableGame(topic);
      callback && callback();
    });

    cc.systemEvent.on(EventKey.ON_GET_LATEST_BALANCE_EVENT, (callback) => {
      // console.error(EventKey.ON_GET_LATEST_BALANCE_EVENT);
      WalletHandler.getInstance().getLatestBalance(callback);
    });

    //#region ======== event system ========
    cc.systemEvent.on(EventKey.ON_REMOVE_ALL_EVENT, () => {
      this.removeAllEvent();
    });
    cc.systemEvent.on(EventKey.ON_REMOVE_EVENT, (keyEvent) => {
      this.removeEvent(keyEvent);
    });
    cc.systemEvent.on(EventKey.ON_DISCONNECT_EVENT, (code, isRetry, msg) => {
      EventManager.fire(EventManager.ON_DISCONNECT, code, isRetry, msg);
    });

    // cc.systemEvent.on(EventKey.UPDATE_STATE_TXMN, (state) => {
    //   if (MiniGameManager.getInstance().countDownSicboMiniComp) MiniGameManager.getInstance().countDownSicboMiniComp.updateState(state);
    // });

    cc.systemEvent.on(EventKey.ON_SET_GAME_TYPE_EVENT, (topic) => {
      if ((topic as Topic) == Topic.BAU_CUA) GameUtils.gameTopic = topic;
    });

    cc.systemEvent.on(EventKey.ON_SHOW_TEXT_NOTIFY, (notifyText, time: number = 2) => {
      UIManager.getInstance().openNotify(TextNotify, GameDefine.TextNotify, new TextNotifyData(notifyText, time));
    });
    cc.systemEvent.on(EventKey.CG_SHOW_TEXT_NOTIFTY_INVETED, () => {
      UIManager.getInstance().openNotify(TextNotify, GameDefine.TextNotify, new TextNotifyData("Đã gửi lời mời tới người chơi khác!"));
    });

    cc.systemEvent.on("CG_REPORT_RESULT", (rs: number) => {
      let textMessage = "";
      switch (rs) {
        case 0: //success
          textMessage = PortalText.POR_CARD_REPORT_SUCCESSED;
          break;
        case 1: //Anonymous cannot report
          textMessage = PortalText.POR_CARD_REPORT_ANONYMOUS;
          break;
        case 2: //Reported
          textMessage = PortalText.POR_CARD_REPORT_FAILED_REPORTED;
          break;
        case 3: //Cannot report, not enough player
          textMessage = PortalText.POR_CARD_REPORT_FAILED_NOT_ENOUGH_PLAYER;
          break;
      }

      UIManager.getInstance().openNotify(TextNotify, GameDefine.TextNotify, new TextNotifyData(textMessage));
    });

    //-=============
  }

  //#region ======== network ========
  public static onPingTime(ping: number = 0) {
    cc.systemEvent.emit(EventKey.ON_PING_TIME, ping);
  }

  public static onPingError() {
    cc.systemEvent.emit(EventKey.ON_PING_ERROR);
  }

  public static onFireDisconnect(msg: string = "") {
    cc.systemEvent.emit(EventKey.ON_DISCONNECT_EVENT, msg);
  }

  public static onFireReconnected(msg: string = "") {
    cc.systemEvent.emit(EventKey.ON_RECONNECTED_EVENT, msg);
  }

  //#region ======== balance ========
  public static onFireUpdateBalance(balance) {
    cc.systemEvent.emit(EventKey.ON_UPDATE_BALANCE_EVENT, balance);
  }

  public static onFireAddBalance(amount, balance) {
    cc.systemEvent.emit(EventKey.ON_ADD_BALANCE_EVENT, amount, balance);
  }

  public static removeAllEvent() {
    cc.log("removeAllEvent");
    var propNames = Object.getOwnPropertyNames(EventKey);
    propNames.forEach(function (propName) {
      cc.systemEvent.off(propName);
    });
    cc.systemEvent.off(EventKey.ON_OPEN_POPUP_DISCONNECT_EVENT);
    cc.systemEvent.off(EventKey.ON_OPEN_POPUP_CHAT_EVENT);
    cc.systemEvent.off(EventKey.ON_OPEN_POPUP_INFO_EVENT);
    cc.systemEvent.off(EventKey.ON_SOUND_BG_EVENT);
    cc.systemEvent.off(EventKey.ON_SOUND_EFF_EVENT);
    cc.systemEvent.off(EventKey.ON_REMOVE_ALL_EVENT);
    cc.systemEvent.off(EventKey.ON_REMOVE_EVENT);
    cc.systemEvent.off(EventKey.ON_CLOSE_POPUP_LOADING_EVENT);
    cc.systemEvent.off(EventKey.ON_OPEN_POPUP_LOADING_EVENT);
    cc.systemEvent.off(EventKey.ON_OPEN_POPUP_WARNING_EVENT);
    cc.systemEvent.off(EventKey.ON_ADD_CURRENT_BALANCE_EVENT);
    cc.systemEvent.off(EventKey.ON_ENABLE_WALLET_EVENT);
    cc.systemEvent.off(EventKey.ON_DISABLE_WALLET_EVENT);
    cc.systemEvent.off(EventKey.LISTEN_CHAT_CARD);
    cc.systemEvent.off(EventKey.LISTEN_CHAT_TABLE);
    cc.systemEvent.off(EventKey.LISTEN_CHAT_LODE);
    cc.systemEvent.off(EventKey.LISTEN_CHAT_MINI);
    cc.systemEvent.off(EventKey.SHOW_CHAT_ERR_LODE);
    cc.systemEvent.off(EventKey.SHOW_CHAT_ERR_MINI);
    cc.systemEvent.off(EventKey.SEND_CHAT_MINI);
    cc.systemEvent.off(EventKey.TURN_ON_COUNT_DOWN_TXMN);
    cc.systemEvent.off(EventKey.TURN_OFF_COUNT_DOWN_TXMN);
    cc.systemEvent.off(EventKey.UPDATE_STATE_TXMN);
    cc.systemEvent.off(EventKey.GET_CURRENT_BALANCE_EVENT);
    cc.systemEvent.off(EventKey.ON_GET_LATEST_BALANCE_EVENT);
    cc.systemEvent.off(EventKey.ON_DISCONNECT_EVENT);
    cc.systemEvent.off(EventKey.GET_EMOJI);
    cc.systemEvent.off(EventKey.GET_HISTORY_CHAT_MINI);
    cc.systemEvent.off(EventKey.SETTING_CHAT);
    cc.systemEvent.off(EventKey.SETTING_CHAT_MINI);
    cc.systemEvent.off(EventKey.ON_SET_GAME_TYPE_EVENT);
    cc.systemEvent.off(EventKey.ON_BACK_TO_PORTAL_EVENT);

    //Card games
    cc.systemEvent.off(EventKey.ON_BACK_TO_CARD_LOBBY_EVENT);
    cc.systemEvent.off(EventKey.ON_SHOW_TEXT_NOTIFY);
    cc.systemEvent.off(EventKey.CG_SHOW_TEXT_NOTIFTY_INVETED);
    cc.systemEvent.off(EventKey.CG_OPEN_SETTING_POPUP);
    cc.systemEvent.off(EventKey.CG_OPEN_INFO_POPUP);
    cc.systemEvent.off(EventKey.CG_GET_CHAT_SAMPLE);
    cc.systemEvent.off(EventKey.CG_OPEN_CONFIRM_START_GAME);
    cc.systemEvent.off(EventKey.CG_OPEN_SHARE_TABLE_POPUP);
    cc.systemEvent.off(EventKey.CG_PLAY_SFX_CLICK_BUTTON);
    cc.systemEvent.off(EventKey.ON_SHOW_EVENT_POPUP_FOR_TXMN);
    cc.systemEvent.off(EventKey.ON_GET_CHAT_ENABLE);
    cc.systemEvent.off(EventKey.ON_GET_MINI_CHAT_ENABLE);
    cc.systemEvent.off(EventKey.ON_OPEN_GIFT_CODE_HISTORY);
    cc.systemEvent.off(EventKey.ON_PING_TIME);
    cc.systemEvent.off("ON_SET_POS_MINI_GAME_HUB_FOR_CARD_GAME");
    cc.systemEvent.off("CG_REPORT_RESULT");
    cc.systemEvent.off("FIREBASE_TRACKING_EVENT");
  }

  public static removeEvent(keyEvent: string) {
    cc.systemEvent.off(keyEvent);
  }
}
