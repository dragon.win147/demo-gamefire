// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class HookEvent extends cc.Component {
  private hookEvent: Function = null;

  public init(hookEvent: Function) {
    this.hookEvent = hookEvent;
  }

  onDestroy() {
    this.hookEvent && this.hookEvent();
  }
}
