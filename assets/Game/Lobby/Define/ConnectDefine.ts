export default class ConnectDefine {
  public static fbAppId = "";
  public static addressConfig: string[] = ["https://firebasestorage.googleapis.com/v0/b/gameeagles-66e75.appspot.com/o/config_stg_default.json?alt=media"];
  public static addressURL: string = "https://play.henxui.fun/";
  public static addressURLAndParameter: string = "https://bentau.henxui.fun";
  public static addressSanBay: string = "https://sanbay.henxui.fun";
  public static addressBenTau: string = "https://bentau.henxui.fun";
  public static addressBenTauWS: string = "wss://bentau.henxui.fun/gameloot.bentau.BenTau/WS";
  public static addressResources: string = "https://hose.henxui.fun";
}
