export default class GameDefine {
  //#region ======== ON/OFF ========
  public static CC_LOG_ENABLE: boolean = true;
  public static LOG_VERSION_ENABLE: boolean = false;
  public static IS_PROD_VERSION: boolean = false;

  //#endregion

  //#region ======== Notify ========
  public static TextNotify: string = "TextNotify";
  public static NotifyPopup: string = "NotifyPopup";

  //#endregion

  public static ConfirmPopupSystem: string = "ConfirmPopupSystem";
  public static LoadingPopupSystem: string = "LoadingPopupSystem";
  public static WarningPopupSystem: string = "WarningPopupSystem";
  public static RetryPopupSystem: string = "RetryPopupSystem";
  //#endregion

  //#end
}
