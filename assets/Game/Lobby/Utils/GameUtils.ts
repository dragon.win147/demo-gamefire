import { GameType, typeScene } from "../Utils/EnumGame";
import { Timestamp } from "google-protobuf/google/protobuf/timestamp_pb";
import GameDefine from "../Define/GameDefine";
import { Topic } from "@gameloot/topic/topic_pb";
import TextNotify, { TextNotifyData } from "../Manager/TextNotify";
import UIManager from "../Manager/UIManager";
import PortalText from "../Utils/PortalText";

export enum Badge {
  gold = "_gold",
  platinum = "_platinum",
  diamond = "_diamond",
  ruby = "_ruby",
  emerald = "_emerald",
}
export default class GameUtils {
  public static IS_DEV: boolean = false;
  public static count: number = 0;
  public static gameType: GameType = GameType.BauCua;
  public static gameTopic: Topic = Topic.TINA;
  public static isStatusPing: boolean = true;
  public static isMainScene: boolean = true;
  public static isFirstLoadGame: boolean = true;
  public static deviceName: string = "";
  public static deviceID: string = "";
  public static utmSource: string = "";
  private static readonly deviceIdKey = "DEVICE_ID";

  public static createClickBtnEvent(node: cc.Node, componentName: string, handlerEvent: string, custom: string = "") {
    const clickEventHandler = new cc.Component.EventHandler();
    clickEventHandler.target = node; //This node is the node to which your event handler code component belongs
    clickEventHandler.component = componentName; //This is the code file name
    clickEventHandler.handler = handlerEvent;
    clickEventHandler.customEventData = custom;
    return clickEventHandler;
  }

  public static getUrlParameter(name, url) {
    name = name.replace("/[[]/", "\\[").replace("/[]]/", "\\]");
    let regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    let results = regex.exec(url);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  public static removeParameterUrl(sParam, sourceURL): string {
    var rtn = sourceURL.split("?")[0],
      param,
      params_arr = [],
      queryString = sourceURL.indexOf("?") !== -1 ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
      params_arr = queryString.split("&");
      for (var i = params_arr.length - 1; i >= 0; i -= 1) {
        param = params_arr[i].split("=")[0];
        if (param === sParam) {
          params_arr.splice(i, 1);
        }
      }
      if (params_arr.length) rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
  }

  public static removeAllParameterUrl(sourceURL) {
    var rtn = sourceURL.split("?")[0],
      param,
      params_arr = [],
      queryString = sourceURL.indexOf("?") !== -1 ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
      params_arr = queryString.split("&");
      for (var i = params_arr.length - 1; i >= 0; i -= 1) {
        param = params_arr[i].split("=")[0];
        // if (param === sParam) {
        params_arr.splice(i, 1);
        // }
      }
      if (params_arr.length) rtn = rtn + "?" + params_arr.join("&");
    }
    window.history.replaceState(null, null, rtn);
    // return rtn;
  }

  public static convertToOtherNode(fromNode: cc.Node, targetNode: cc.Node, pos: cc.Vec3 = null) {
    let parent = fromNode;
    if (fromNode.parent) parent = fromNode.parent;
    let worldSpace = parent.convertToWorldSpaceAR(fromNode.position);
    if (pos) {
      worldSpace = fromNode.convertToWorldSpaceAR(pos);
    }
    return targetNode.convertToNodeSpaceAR(worldSpace);
  }

  public static convertToOtherNode2(fromNode: cc.Node, targetNode: cc.Node, pos: cc.Vec2 = null) {
    let parent = fromNode;
    if (fromNode.parent) parent = fromNode.parent;
    let worldSpace = parent.convertToWorldSpaceAR(fromNode.getPosition());
    if (pos) {
      worldSpace = fromNode.convertToWorldSpaceAR(pos);
    }
    return targetNode.convertToNodeSpaceAR(worldSpace);
  }

  public static rectContainsPoint(rect: cc.Rect, point: cc.Vec2) {
    const result = point.x >= rect.origin.x && point.x <= rect.origin.x + rect.size.width && point.y >= rect.origin.y && point.y <= rect.origin.y + rect.size.height;
    return result;
  }

  public static createItemFromNode<T>(item: new () => T, node: cc.Node, parentNode: cc.Node = null, insert: boolean = false): T {
    if (!node) return null;
    let parent = parentNode ? parentNode : node.parent;
    let newNode = cc.instantiate(node);

    if (insert) {
      parent.insertChild(newNode, 0);
    } else {
      parent.addChild(newNode);
    }
    let newItem = newNode.getComponent(item);
    newItem.node.active = true;
    node.active = false;
    return newItem;
  }

  public static cloneItemFromNode<T>(item: new () => T, node: cc.Node): T {
    if (!node) return null;
    let newNode = cc.instantiate(node);

    let newItem = newNode.getComponent(item);
    newItem.node.active = true;
    node.active = false;
    return newItem;
  }

  public static createItemFromPrefab<T>(item: new () => T, prefab: cc.Prefab, parentNode: cc.Node): T {
    if (!prefab) return null;
    let node = cc.instantiate(prefab);
    parentNode.addChild(node);
    let newItem = node.getComponent(item);
    newItem.node.active = true;
    return newItem;
  }

  public static createNodeFromPrefab<T>(prefab: cc.Prefab, parentNode: cc.Node): cc.Node {
    if (!prefab) return null;
    let node = cc.instantiate(prefab);
    parentNode.addChild(node);
    return node;
  }

  public static formatDate(date: Date, format: string): string {
    format = format.replace("%Y", date.getFullYear().toString());

    let month = date.getMonth() + 1;
    format = format.replace("%M", month.toString().length < 2 ? "0" + month.toString() : month.toString());
    format = format.replace("%d", date.getDate().toString().length < 2 ? "0" + date.getDate().toString() : date.getDate().toString());
    format = format.replace("%h", date.getHours().toString().length < 2 ? "0" + date.getHours().toString() : date.getHours().toString());
    format = format.replace("%m", date.getMinutes().toString().length < 2 ? "0" + date.getMinutes().toString() : date.getMinutes().toString());
    format = format.replace("%s", date.getSeconds().toString().length < 2 ? "0" + date.getSeconds().toString() : date.getSeconds().toString());
    return format;
  }

  public static formatMoreDate(startDate: Date, endDate: Date, format: string): string {
    format = format.replace("%Y1", startDate.getFullYear().toString());
    let monthFrom = startDate.getMonth() + 1;
    format = format.replace("%M1", monthFrom.toString().length < 2 ? "0" + monthFrom.toString() : monthFrom.toString());
    format = format.replace("%d1", startDate.getDate().toString().length < 2 ? "0" + startDate.getDate().toString() : startDate.getDate().toString());
    format = format.replace("%h1", startDate.getHours().toString().length < 2 ? "0" + startDate.getHours().toString() : startDate.getHours().toString());
    format = format.replace("%m1", startDate.getMinutes().toString().length < 2 ? "0" + startDate.getMinutes().toString() : startDate.getMinutes().toString());
    // end date
    format = format.replace("%Y2", endDate.getFullYear().toString());
    let monthTo = startDate.getMonth() + 1;
    format = format.replace("%M2", monthTo.toString().length < 2 ? "0" + monthTo.toString() : monthTo.toString());
    format = format.replace("%d2", endDate.getDate().toString().length < 2 ? "0" + endDate.getDate().toString() : endDate.getDate().toString());
    format = format.replace("%h2", endDate.getHours().toString().length < 2 ? "0" + endDate.getHours().toString() : endDate.getHours().toString());
    format = format.replace("%m2", endDate.getMinutes().toString().length < 2 ? "0" + endDate.getMinutes().toString() : endDate.getMinutes().toString());
    return format;
  }

  public static formatMoneyNumberMyUser(money: number): string {
    let s = "$ " + this.numberWithCommas(money);
    return s;
  }

  public static formatMoneyNumberMyUserNotIcon(money: number): string {
    return this.numberWithCommas(money);
  }

  public static formatMoneyNumberUser(money: number): string {
    let s = "$ " + this.formatMoneyNumber(money);
    return s;
  }
  public static formatMoneyNumber(money: number): string {
    let sign = 1;
    let value = money;
    if (money < 0) {
      sign = -1;
      value = value * -1;
    }

    var format = "";
    if (value >= 1000000000.0) {
      value /= 1000000000.0;
      format = "B";
    } else if (value >= 1000000.0) {
      value /= 1000000.0;
      format = "M";
    } else if (value >= 1000.0) {
      value /= 1000.0;
      format = "K";
    }

    value = (Math.floor(value * 100 + 0.00000001) / 100) * sign;
    return value + format;
  }

  public static formatMoneyNumber_v2(money: number): string {
    let sign = 1;
    let value = money;
    if (money < 0) {
      sign = -1;
      value = value * -1;
    }

    var format = "";
    if (value >= 1000000000.0) {
      value /= 1000000000.0;
      format = " Tỉ";
    } else if (value >= 1000000.0) {
      value /= 1000000.0;
      format = " Triệu";
    } else if (value >= 1000.0) {
      value /= 1000.0;
      format = " Ngàn";
    }

    value = (Math.floor(value * 100 + 0.00000001) / 100) * sign;
    return value + format;
  }

  public static numberWithCommasMoney(number) {
    let s = "$ " + this.numberWithCommas(number);
    return s;
  }

  public static numberWithCommas(number) {
    if (number) {
      var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
      result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      return result[1] !== "00" ? result.join(",") : result[0];
    }
    return "0";
  }

  public static numberWithCommasV2(number) {
    if (number) {
      var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
      result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");

      return result[1] !== "00" ? result.join(",") : result[0];
    }
    return "0";
  }

  public static numberWithDot(number) {
    if (number) {
      var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
      result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");

      return result[1] !== "00" ? result.join(",") : result[0];
    }
    return "0";
  }

  public static formatMoneyWithRange(money: number, shortenStart: number = 100000000): string {
    let rs = "";
    if (money >= shortenStart) rs = GameUtils.formatMoneyNumber(money);
    else rs = GameUtils.numberWithCommas(money);
    return rs;
  }

  public static roundNumber(num: number, roundMin: number = 1000): number {
    let temp = parseInt((num / roundMin).toString());
    if (num % roundMin != 0) temp += 1;
    return temp * roundMin;
  }

  public static getRandomInt(max: number, min: number = 0): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  public static randomInsideCircle(R: number = 1): cc.Vec2 {
    let r = R * Math.sqrt(Math.random());
    let theta = Math.random() * 2 * Math.PI;
    let x = r * Math.cos(theta);
    let y = r * Math.sin(theta);
    return new cc.Vec2(x, y);
  }

  public static getRandomFloat(max) {
    return Math.random() * max;
  }

  public static playAnimOnce(skeleton: sp.Skeleton, toAnimation: string, backAnimation: string, onCompleted: Function = null) {
    if (skeleton == null || skeleton == undefined) return;
    // if (skeleton.getCurrent(0) != null && skeleton.getCurrent(0).animation.name == toAnimation) {
    //   return;
    // }
    skeleton.setCompleteListener((trackEntry: sp.spine.TrackEntry) => {
      if (backAnimation != "" && backAnimation != null) skeleton.setAnimation(0, backAnimation, true);
      if (trackEntry.animation && trackEntry.animation.name == toAnimation) onCompleted && onCompleted();
    });
    skeleton.setAnimation(0, toAnimation, false);
  }

  public static sum(array: number[]) {
    if (array.length <= 0) return 0;
    return array.reduce((total, val) => total + val);
  }

  public static setContentLabelAutoSize(label: cc.Label, content: string) {
    if (content.length > 30) {
      label.node.width = (30 * label.fontSize) / 2;
      label.overflow = cc.Label.Overflow.RESIZE_HEIGHT;
    } else {
      label.overflow = cc.Label.Overflow.NONE;
    }
    label.string = content;
  }

  static FormatString(str: string, ...val: any[]) {
    for (let index = 0; index < val.length; index++) {
      str = str.replace(`{${index}}`, val[index].toString());
    }
    return str;
  }

  static setPlayEffect(nodeEff: cc.Node) {
    if (!nodeEff) return;
    nodeEff.getComponentsInChildren(cc.ParticleSystem3D).forEach((x) => {
      x.stop();
      x.play();
    });
    nodeEff.getComponentsInChildren(cc.Animation).forEach((x) => {
      x.stop();
      x.play();
    });
    nodeEff.getComponentsInChildren(sp.Skeleton).forEach((x) => {
      x.setAnimation(0, x.defaultAnimation, x.loop);
    });
  }

  static setParentTemp(nameTempNode: string = "Temp", node: cc.Node, parentOfTemp: cc.Node, zIndex: number = 90) {
    let temp = parentOfTemp.getChildByName(nameTempNode);
    if (!temp) {
      temp = new cc.Node(nameTempNode);
      parentOfTemp.addChild(temp);
      temp.zIndex = zIndex;
      parentOfTemp.sortAllChildren();
    }
    let widget = node.getComponent(cc.Widget);
    if (widget) widget.enabled = false;

    let startPos = GameUtils.convertToOtherNode(node.parent, temp, node.position);
    node.setParent(temp);
    node.position = startPos;
    node.zIndex = 0;
  }

  static getParentTemp(nameTempNode: string = "Temp", parentOfTemp: cc.Node, zIndex: number = 80) {
    let temp = parentOfTemp.getChildByName(nameTempNode);
    if (!temp) {
      temp = new cc.Node(nameTempNode);
      parentOfTemp.addChild(temp);
      temp.zIndex = zIndex;
    }
    return temp;
  }

  static getTimeSince(timeStamp: Timestamp) {
    let msPerMinute = 60 * 1000;
    let msPerHour = msPerMinute * 60;
    let msPerDay = msPerHour * 24;
    let msPerMonth = msPerDay * 30;
    let msPerYear = msPerDay * 365;

    let elapsed = Date.now() - timeStamp.toDate().getTime();
    cc.log("elapsed", elapsed);
    if (elapsed < msPerMinute) {
      return Math.round(elapsed / 1000) + " giây trước";
    } else if (elapsed < msPerHour) {
      return Math.round(elapsed / msPerMinute) + " phút trước";
    } else if (elapsed < msPerDay) {
      return Math.round(elapsed / msPerHour) + " giờ trước";
    } else if (elapsed < msPerMonth) {
      return "khoảng " + Math.round(elapsed / msPerDay) + " ngày trước";
    } else if (elapsed < msPerYear) {
      return "khoảng " + Math.round(elapsed / msPerMonth) + " tháng trước";
    } else {
      return "khoảng " + Math.round(elapsed / msPerYear) + " năm trước";
    }
  }

  public static getKeyByValue(object, value) {
    let key = undefined;
    object.forEach((v, k) => {
      if (value == v) {
        key = k;
      }
    });
    return key;
  }

  public static isPlatformIOS() {
    return cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD;
  }

  public static isBrowser() {
    return cc.sys.isBrowser;
  }

  public static isNative() {
    return cc.sys.isNative;
  }

  public static isAndroid() {
    return cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID;
  }

  public static isIOS() {
    return cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS;
  }

  public static isLogVersion() {
    return GameDefine.LOG_VERSION_ENABLE;
  }

  public static isNullOrUndefined(object) {
    return object == null || object == undefined;
  }
  public static convertLotteryNumberToString(lotteryNumber: number): string {
    let lotteryNumberString = lotteryNumber.toString();
    let remainZeroNumber = 6 - lotteryNumberString.length;
    for (let i = 0; i < remainZeroNumber; i++) {
      lotteryNumberString = "0" + lotteryNumberString;
    }
    return lotteryNumberString;
  }

  public static isWebMobile() {
    return cc.sys.isBrowser && cc.sys.isMobile;
  }

  public static getDeviceId() {
    let id = cc.sys.localStorage.getItem(GameUtils.deviceIdKey);

    if (id == null || id == "") {
      let rawId =
        cc.sys.os +
        cc.sys.osMainVersion +
        cc.sys.language +
        cc.sys.languageCode +
        cc.sys.osVersion +
        cc.sys.isNative +
        cc.sys.windowPixelResolution.height +
        cc.sys.platform +
        cc.sys.windowPixelResolution.width +
        cc.sys.isMobile +
        cc.sys.now();

      id = btoa(rawId);
      cc.sys.localStorage.setItem(GameUtils.deviceIdKey, id);
    }
    GameUtils.deviceID = id;
  }
  public static getPlatformName(): string {
    if (GameUtils.isWebMobile()) {
      return "WEBM";
    } else if (cc.sys.isBrowser) {
      return "WEBPC";
    } else if (cc.sys.os == cc.sys.OS_ANDROID) {
      return "ANDROID";
    } else if (cc.sys.os == cc.sys.OS_IOS) {
      return "IOS";
    } else return "UNKNOWN";
  }

  private static qrToString(qrcode) {
    let rs = "";
    for (let row = 0; row < qrcode.getModuleCount(); row++) {
      for (let col = 0; col < qrcode.getModuleCount(); col++) {
        rs = rs + (qrcode.isDark(row, col) ? "1" : "0");
      }
    }
    return rs;
  }
}
