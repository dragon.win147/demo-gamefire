export const enum GameType {
  LandingPage = 0,
  BauCua = 1,
  TLMN = 2,
  Sicbo = 3,
  TienLenMN = 4,
  Tala = 6,
  Fishing = 7,
  ThanTai = 8,
  Loading = 9,
  ChienThan = 10,
  MauBinh = 11,
  XocDia = 12,
  LoDe = 13,
  Poker = 14,
}
export const enum typeScene {
  release = 0,
  lobby = 1,
  loadingScene = 2,
  home = 3,
}
export enum TypeLoadingChangeScene {
  NORMAL = 0,
  SLOT = 1,
  BET = 2,
  CARD = 3,
}

export enum TypeChatHeo {
  NONE = -1,
  HEO = 0,

  CHAT_HEO = 1,
  CHAT_DOI_HEO = 2,
  DOI_HEO_DE = 3,
  DE_DOI_HEO = 4,
  DOI_HEO = 5,
  BA_HEO = 6,

  BA_DOI_THONG = 7,
  BA_DOI_THONG_DE = 8,
  DE_BA_DOI_THONG = 9,
  CHAT_BA_DOI_THONG = 10,

  TU_QUY = 11,
  TU_QUY_DE = 12,
  DE_TU_QUY = 13,
  CHAT_TU_QUY = 14,

  BON_DOI_THONG = 15,
  BON_DOI_THONG_DE = 16,
  DE_BON_DOI_THONG = 17,
}

export enum SoundCard {
  DEAL = 0,
  HIT = 1,
  CHAT = 2,
  BI_CHAT = 3,
  WIN = 4,
  LOSE = 5,
  REFUND = 6,
  LOST_MONEY = 7,
  MONEY_CUT_PIG = 8,
  AUTO_WIN = 9,
  AUTO_LOSE = 10,
}

export enum CodeSocket {
  FORCE_CLOSE = 4999,
  KOTIC = 1001,
  AUTO_CLOSE = 1000,
  RETRY = 3006,
}
