import { Timestamp } from "google-protobuf/google/protobuf/timestamp_pb";

export default class TimeUtils {
  static getTime1970(): Timestamp {
    let timestamp = new Timestamp();
    timestamp.fromDate(new Date(1970, 0, 1));
    return timestamp;
  }

  static getTimeCurrentDate(): Timestamp {
    let timestamp = new Timestamp();
    timestamp.fromDate(new Date());
    return timestamp;
  }

  static getTimeFromDate(day: number): Timestamp {
    let timestamp = new Timestamp();
    let today = new Date();
    let fromDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() - day, today.getHours(), today.getMinutes());
    timestamp.fromDate(fromDay);
    return timestamp;
  }

  static getCurrentTimeByMillisecond() {
    return new Date().getTime();
  }

  static getTimeWithDate(year: number, month: number = 1, day: number = 1, hour: number = 0, minute: number = 0): Timestamp {
    let timestamp = new Timestamp();
    timestamp.fromDate(new Date(year, month - 1, day, hour, minute));
    return timestamp;
  }

  static parseTimeToString(date: Date, separateChar: string = "\n") {
    function pad(n) {
      return n < 10 ? "0" + n : n;
    }
    if (!date) date = new Date();
    let year = pad(date.getDate()) + "/" + pad(date.getMonth() + 1) + "/" + date.getFullYear();
    let minute = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
    let hour = date.getHours() + ":" + minute;
    return year + separateChar + hour;
  }

  static parseTimeToDateNotHour(date: Date) {
    function pad(n) {
      return n < 10 ? "0" + n : n;
    }
    if (!date) date = new Date();
    let year = pad(date.getDate()) + "/" + pad(date.getMonth() + 1) + "/" + date.getFullYear();
   
    return year;
  }

  static parseTimeToHourNotDate(date: Date) {
    function pad(n) {
      return n < 10 ? "0" + n : n;
    }
    if (!date) date = new Date();
    let minute = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
    let hour = date.getHours() + "h" + minute;
    return hour;
  }

  static parseTimeToString_v2(date: Date, separateChar: string = " ") {
    function pad(n) {
      return n < 10 ? "0" + n : n;
    }
    if (!date) date = new Date();
    let year = pad(date.getDate()) + "/" + pad(date.getMonth() + 1) + "/" + date.getFullYear();
    let minute = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
    let hour = (date.getHours() > 9 ? date.getHours() : "0" + date.getHours()) + ":" + minute;
    return year + separateChar + hour;
  }
  static parseTimeToString_v3(date: Date) {
    function pad(n) {
      return n < 10 ? "0" + n : n;
    }
    if (!date) date = new Date();
    let year = pad(date.getDate()) + "-" + pad(date.getMonth() + 1) + "-" + date.getFullYear();
    let minute = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
    let hour = date.getHours() + ":" + minute;
    return hour + " " + year;
  }
  static parseTimeToString_v4(date: Date) {
    let dateString = "";
    switch (date.getDay()) {
      case 0:
        dateString += "CN";
        break;
      case 1:
        dateString += "T2";
        break;
      case 2:
        dateString += "T3";
        break;
      case 3:
        dateString += "T4";
        break;
      case 4:
        dateString += "T5";
        break;
      case 5:
        dateString += "T6";
        break;
      case 6:
        dateString += "T7";
        break;
    }
    dateString += "/" + date.getDate() + "-" + (date.getMonth() + 1) + "[" + date.getHours().toString().padStart(2, "0") + ":" + date.getMinutes().toString().padStart(2, "0") + ":" + date.getSeconds().toString().padStart(2, "0") + "]";
    return dateString;
  }
  static parseTimeToString_v5(date: Date) {
    let dateString = "";
    switch (date.getDay()) {
      case 0:
        dateString += "CN";
        break;
      case 1:
        dateString += "T2";
        break;
      case 2:
        dateString += "T3";
        break;
      case 3:
        dateString += "T4";
        break;
      case 4:
        dateString += "T5";
        break;
      case 5:
        dateString += "T6";
        break;
      case 6:
        dateString += "T7";
        break;
    }
    dateString += "-" + date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear().toString().substring(2) + "[" + date.getHours().toString().padStart(2, "0") + ":" + date.getMinutes().toString().padStart(2, "0") + "]";
    return dateString;
  }
  static parseTimeToStringDMY(date: Date) {
    function pad(n) {
      return n < 10 ? "0" + n : n;
    }
    if (!date) date = new Date();
    let year = pad(date.getDate()) + "/" + pad(date.getMonth() + 1) + "/" + date.getFullYear();
    return year;
  }

  static parseTimeToStringDMY_2(date: Date) {
    function pad(n) {
      return n < 10 ? "0" + n : n;
    }
    if (!date) date = new Date();
    let year = pad(date.getDate()) + "/" + pad(date.getMonth() + 1);
    return year;
  }

  static getMillisecond(date: Date) {
    return date.getTime();
  }

  static getSeconds(date: Date) {
    return (date.getTime() - date.getTime() % 1000) / 1000;
  }
}
