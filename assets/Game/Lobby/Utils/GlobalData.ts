import { Topic } from "@gameloot/topic/topic_pb";
import EventKey from "@gameloot/client-base-handler/build/EventKey";
import ConnectDefine from "../Define/ConnectDefine";
import BenTauHandler from "../Network/BenTauHandler";
import IdentityHandler from "../Network/IdentityHandler";
import MyAccountHandler from "../Network/MyAccountHandler";
// import SoundManager from "../Managers/SoundManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GlobalData extends cc.Component {
  map: Map<string, any> = null;

  onLoad() {
    this.map = new Map<string, any>();
    cc.game.addPersistRootNode(this.node);
  }

  set(key, val) {
    this.map.set(key, val);
  }

  get(key) {
    return this.map.get(key);
  }

  clear() {
    this.map.clear();
  }

  setBundleData() {
    this.set(EventKey.ADDRESS_BEN_TAU, ConnectDefine.addressBenTau);
    this.set(EventKey.ADDRESS_BEN_TAU_WS, ConnectDefine.addressBenTauWS);
    this.set(EventKey.ADDRESS_SAN_BAY, ConnectDefine.addressSanBay);
    this.set(EventKey.ADDRESS_RESOURCE, ConnectDefine.addressResources);
    this.set(EventKey.FB_APP_ID, ConnectDefine.fbAppId);

    this.set(EventKey.GET_BENTAU_HANDLER, BenTauHandler.getInstance());
    // this.set(EventKey.GET_POTTER_HANDLER, PotterHandler.getInstance());
    // this.set(EventKey.GET_MARKETPLACE_HANDLER, MarketPlaceHandler.getInstance());
    // this.set(EventKey.GET_LEADER_BOARD_HANDLER, LeaderBoardHandler.getInstance());
    // this.set(EventKey.GET_SOUND_MANAGER, SoundManager.getInstance());
    this.set(EventKey.GET_TOKEN, IdentityHandler.getInstance().token);
    // this.set(EventKey.IS_LOG_VERSION, GameUtils.isLogVersion());

    this.set(EventKey.GET_FRAME_PATH, MyAccountHandler.getInstance().Frames);
    this.set(EventKey.GET_ICON_PATH, MyAccountHandler.getInstance().Bases);
    this.set(EventKey.GET_PLAYAH, MyAccountHandler.getInstance().Playah);
    // this.set(EventKey.GET_GAME_INFO, MarketPlace.games);
    cc.log("setBundleData Playah ", MyAccountHandler.getInstance().Playah.toObject());
  }

  setPaymentBundle(bundle: cc.AssetManager.Bundle) {
    this.set(EventKey.PAYMENT_BUNDLE, bundle);
  }

  setTopHuBetLevel(topic: Topic, gameBetLevel: number) {
    var mapData: Map<Topic, number> = this.get(EventKey.TOP_HU_BET_LEVEL);
    if (mapData) {
      mapData.set(topic, gameBetLevel);
    } else {
      this.set(EventKey.TOP_HU_BET_LEVEL, new Map<Topic, number>().set(topic, gameBetLevel));
    }
  }
  clearTopHuBetLevel(topic: Topic = null) {
    if (topic) {
      var mapData: Map<Topic, number> = this.get(EventKey.TOP_HU_BET_LEVEL);
      if (mapData && mapData.has(topic)) {
        mapData.delete(topic);
      }
    } else {
      this.set(EventKey.TOP_HU_BET_LEVEL, null);
    }
  }
}
