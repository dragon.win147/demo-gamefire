// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class ScreenInstance extends cc.Component {
  _animation: cc.Animation;
  _open = false;
  _data;

  onLoad() {
    this._animation = this.getComponent(cc.Animation);
    this.node.active = false;
  }

  public open(data) {
    this.beforeShow();
    this._open = true;
    this.node.active = true;
    this._data = data;
    this.onShow(data);
    if (this._animation != null) {
      this._animation.play("Show");
    } else {
      this.showDone();
    }
  }

  public close() {
    this._open = false;
    this.beforeClose();
    if (this._animation != null) {
      this._animation.play("Hide");
    } else {
      this.closeDone();
    }
  }

  //#region Call From Animation Event
  public showDone() {
    this.afterShow();
  }

  public closeDone() {
    if (this._open == false) {
      this.node.active = false;
      this.afterClose();
    }
  }
  //#endregion

  protected onShow(data) {}

  protected afterShow() {}

  protected beforeShow() {}

  protected beforeClose() {}

  protected afterClose() {}
}
