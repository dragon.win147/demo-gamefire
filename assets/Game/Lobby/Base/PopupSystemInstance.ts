// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import PopupInstance from "./PopupInstance";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PopupSystemInstance extends PopupInstance {
  onLoad() {
    // if (!this.background) this.background = this.node.getChildByName("darkBackground").getComponent(cc.Widget);
    // if (this.background) {
    //   this.background.isAlignLeft = true;
    //   this.background.isAbsoluteRight = true;
    //   this.background.isAbsoluteTop = true;
    //   this.background.isAlignBottom = true;
    //   this.background.left = -500;
    //   this.background.top = -500;
    //   this.background.bottom = -500;
    //   this.background.right = -500;
    //   this.background.target = cc.director.getScene().getChildByName("Canvas");
    // }
    // this._animation = this.getComponent(cc.Animation);
    this.node.active = false;
  }
}
