// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class ElementInstance extends cc.Component {
  _animation: cc.Animation;
  _open = false;
  _data;

  _onYes: () => void;
  _onNo: () => void;
  _close: () => void;

  onLoad() {
    this._animation = this.getComponent(cc.Animation);
    this.node.active = true;
  }

  public open(data, onYes: () => void = null, onNo: () => void = null) {
    this.beforeShow();
    this._open = true;
    this.node.active = true;
    this._data = data;
    this._onYes = onYes;
    this._onNo = onNo;
    this.onShow(data);
    if (this._animation != null) {
      this._animation.play("Show");
    } else {
      this.showDone();
    }
  }

  public close() {
    this._open = false;
    this._close();
    this.beforeClose();
    if (this._animation != null) {
      this._animation.play("Hide");
    } else {
      this.closeDone();
    }
  }

  public onYes() {
    if (this._onYes) {
      this._onYes();
    }
    this.close();
  }

  public onNo() {
    if (this._onNo) {
      this._onNo();
    }
    this.close();
  }

  //#region Call From Animation Event
  public showDone() {
    this.afterShow();
  }

  public closeDone() {
    if (this._open == false) {
      this.node.active = false;
      this.afterClose();
    }
  }

  //#endregion

  protected onShow(data) {}

  protected afterShow() {}

  protected beforeShow() {}

  protected beforeClose() {}

  protected afterClose() {}
}
