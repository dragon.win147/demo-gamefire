// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import BenTauHandler from "./../Network/BenTauHandler";
import IdentityHandler from "./../Network/IdentityHandler";
import MyAccountHandler from "./../Network/MyAccountHandler";
import { ProfileReply } from "@gameloot/myaccount/myaccount_pb";
import EventBusManager from "./../Event/EventBusManager";
const { ccclass, property } = cc._decorator;

@ccclass
export default class LoginGame extends cc.Component {
  start() {
    EventBusManager.onRegisterEvent();
  }

  onClickLogin() {
    this.sendLogin("dev111", "123123");
  }

  private sendLogin(userName: string, password: string) {
    let self = this;
    IdentityHandler.getInstance().onLogin(
      userName,
      password,
      (response) => {
        cc.log("onLoginSuccess", response);
        MyAccountHandler.getInstance().onGetAvatarList((res) => {
          self.getProfile(() => {
            if (MyAccountHandler.getInstance().Playah) {
              BenTauHandler.getInstance().init(() => {
                let GLOBAL_DATA_NAME: string = "GlobalData";
                let globalNode = cc.director.getScene().getChildByName(GLOBAL_DATA_NAME);
                let globalComp = globalNode.getComponent(GLOBAL_DATA_NAME);
                globalComp.setBundleData();
                cc.director.loadScene("SicboLoadingScene");
              });
            }
          });
        });
      },
      null
    );
  }

  public getProfile(onSuccess: () => void = null) {
    let self = this;
    MyAccountHandler.getInstance().getProfile((profile: ProfileReply) => {
      if (!self) return;
      if (onSuccess) onSuccess();
    });
  }
}
