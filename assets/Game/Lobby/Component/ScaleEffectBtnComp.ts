
const {ccclass, property} = cc._decorator;

@ccclass
export default class ScaleEffectBtnComp extends cc.Component {

    @property(cc.Boolean)
    playOnAwake: boolean = true;
  
    @property(cc.Float)
    delayFirst: number = 0;

    start () {
        if(this.playOnAwake){
            this.scaleEffect()
        }

        this.node.on("click",()=>{
            cc.Tween.stopAllByTarget(this.node)
        },this);
    }

    scaleEffect(){
        let duration = 0.2;
        let child = this.node;
        let scale: cc.Vec3 = cc.Vec3.ZERO;
         this.node.getScale(scale);
        let offset = 0.1;
        cc.tween(child)
          .sequence(
            cc.tween(child).delay(this.delayFirst),
            cc.tween(child).to(duration, { scaleX: scale.x + offset }),
            cc.tween(child).parallel(cc.tween(child).to(duration, { scale: scale.x }), cc.tween(child).to(duration, { scaleY:scale.y + offset })),
            cc.tween(child).to(duration, { scale: scale.x })
          )
          .repeatForever()
          .start();
    }

    protected onDestroy(): void {
        cc.Tween.stopAllByTarget(this.node);
    }

}
