"use strict";
cc._RF.push(module, '9a88d1UPeNGFLkf8Qr5yCoY', 'SicboTracking');
// Game/Sicbo_New/Scripts/SicboTracking.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
//SECTION - FIREBASE const
var FIREBASE_TRACKING_EVENT = "FIREBASE_TRACKING_EVENT";
var FIREBASE_BET_EVENT = "gp_sicbo_"; //gp_sicbo_{action_name}
var FirebaseParamVersion = "Version";
var FirebaseParamSessionId = "SessionId";
//!SECTION
//SECTION - APPFLYER Const
var APPS_FLYER_TRACKING_EVENT = "APPS_FLYER_TRACKING_EVENT";
var AF_EVENT_BET = "af_placed_bet_sicbo";
var AF_EVENT_REBET = "af_placed_bet_sicbo";
var AF_EVENT_DOUBLEBET = "af_placed_bet_sicbo";
var AF_CURRENCY = "VND";
var AF_ParamPrice = "af_price";
var AF_ParamCurrency = "af_currency";
var AF_ParamSession = "event_number";
var AF_ParamDate = "event_date";
var AF_ParamTime = "event_time";
var AF_ParamTransactionId = "transaction_id";
//!SECTION
var SicboTracking = /** @class */ (function () {
    function SicboTracking() {
    }
    SicboTracking_1 = SicboTracking;
    SicboTracking.setGameVersion = function (ver) {
        SicboTracking_1.version = ver;
    };
    SicboTracking.startSession = function (sesssionId) {
        SicboTracking_1.sesssionId = sesssionId;
    };
    SicboTracking.trackingBet = function (transactionId, amount) {
        SicboTracking_1.firebaseTrackingBet();
        SicboTracking_1.appflyerTrackingBet(transactionId, amount);
    };
    SicboTracking.trackingReBet = function (transactionId, amount) {
        SicboTracking_1.firebaseTrackingReBet();
        SicboTracking_1.appflyerTrackingReBet(transactionId, amount);
    };
    SicboTracking.trackingDoubleBet = function (transactionId, amount) {
        SicboTracking_1.firebaseTrackingDoubleBet();
        SicboTracking_1.appflyerTrackingDoubleBet(transactionId, amount);
    };
    //SECTION Appflyer
    SicboTracking.appflyerTrackingBet = function (transactionId, amount) {
        var eventName = AF_EVENT_BET;
        var eventValue = {};
        eventValue[AF_ParamPrice] = Math.abs(amount);
        eventValue[AF_ParamTransactionId] = transactionId;
        eventValue[AF_ParamCurrency] = AF_CURRENCY;
        SicboTracking_1.sendTrackingAppflyer(eventName, eventValue);
    };
    SicboTracking.appflyerTrackingReBet = function (transactionId, amount) {
        var eventName = AF_EVENT_REBET;
        var eventValue = {};
        eventValue[AF_ParamPrice] = Math.abs(amount);
        eventValue[AF_ParamTransactionId] = transactionId;
        eventValue[AF_ParamCurrency] = AF_CURRENCY;
        SicboTracking_1.sendTrackingAppflyer(eventName, eventValue);
    };
    SicboTracking.appflyerTrackingDoubleBet = function (transactionId, amount) {
        var eventName = AF_EVENT_DOUBLEBET;
        var eventValue = {};
        eventValue[AF_ParamPrice] = Math.abs(amount);
        eventValue[AF_ParamTransactionId] = transactionId;
        eventValue[AF_ParamCurrency] = AF_CURRENCY;
        SicboTracking_1.sendTrackingAppflyer(eventName, eventValue);
    };
    SicboTracking.sendTrackingAppflyer = function (eventName, eventValue) {
        eventValue[AF_ParamSession] = SicboTracking_1.sesssionId;
        eventValue[AF_ParamDate] = SicboTracking_1.getCurrentDate();
        eventValue[AF_ParamTime] = SicboTracking_1.getCurrentTime();
        var evt = {
            eventType: "EVENT",
            eventName: eventName,
            eventValue: eventValue,
        };
        //send event
        cc.systemEvent.emit(APPS_FLYER_TRACKING_EVENT, evt);
    };
    //!SECTION
    //SECTION Firebase
    SicboTracking.firebaseTrackingBet = function () {
        var eventName = FIREBASE_BET_EVENT + "bet";
        var params = {};
        SicboTracking_1.sendTrackingFirebase(eventName, params);
    };
    SicboTracking.firebaseTrackingReBet = function () {
        var eventName = FIREBASE_BET_EVENT + "re_bet";
        var params = {};
        SicboTracking_1.sendTrackingFirebase(eventName, params);
    };
    SicboTracking.firebaseTrackingDoubleBet = function () {
        var eventName = FIREBASE_BET_EVENT + "double_bet";
        var params = {};
        SicboTracking_1.sendTrackingFirebase(eventName, params);
    };
    SicboTracking.sendTrackingFirebase = function (eventName, params) {
        params[FirebaseParamVersion] = SicboTracking_1.version;
        params[FirebaseParamSessionId] = SicboTracking_1.sesssionId;
        cc.systemEvent.emit(FIREBASE_TRACKING_EVENT, eventName, params);
    };
    //!SECTION
    /**
     * get current date as DDMMYYYY
     */
    SicboTracking.getCurrentDate = function () {
        var currentDate = new Date();
        // Get day, month, and year
        var day = currentDate.getDate().toString().padStart(2, "0");
        var month = (currentDate.getMonth() + 1).toString().padStart(2, "0"); // Month is zero-based
        var year = currentDate.getFullYear().toString();
        // Concatenate the date parts
        var formattedDate = day + month + year;
        return formattedDate;
    };
    /**
     * get current time as HH:MM
     */
    SicboTracking.getCurrentTime = function () {
        var currentDate = new Date();
        // Get hours and minutes
        var hours = currentDate.getHours().toString().padStart(2, "0");
        var minutes = currentDate.getMinutes().toString().padStart(2, "0");
        // Concatenate the time parts
        var formattedTime = hours + ":" + minutes;
        return formattedTime;
    };
    var SicboTracking_1;
    SicboTracking = SicboTracking_1 = __decorate([
        ccclass
    ], SicboTracking);
    return SicboTracking;
}());
exports.default = SicboTracking;

cc._RF.pop();