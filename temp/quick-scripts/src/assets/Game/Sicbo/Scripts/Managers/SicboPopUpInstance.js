"use strict";
cc._RF.push(module, 'fe47feNq/RId4+IXrYWCKIs', 'SicboPopUpInstance');
// Game/Sicbo_New/Scripts/Managers/SicboPopUpInstance.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboPopUpInstance = /** @class */ (function (_super) {
    __extends(SicboPopUpInstance, _super);
    function SicboPopUpInstance() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.background = null;
        _this.panel = null;
        _this.normalPanelSize = 1;
        _this.scaleWhenOpenAndClosePanelSize = 1.05;
        _this._open = false;
        return _this;
    }
    SicboPopUpInstance.prototype.onDestroy = function () {
        this.unscheduleAllCallbacks();
    };
    SicboPopUpInstance.prototype.onLoad = function () {
        var _a;
        if (!this.background)
            this.background = (_a = this.node.getChildByName("darkBackground")) === null || _a === void 0 ? void 0 : _a.getComponent(cc.Widget);
        if (this.background) {
            this.background.isAlignLeft = true;
            this.background.isAbsoluteRight = true;
            this.background.isAbsoluteTop = true;
            this.background.isAlignBottom = true;
            this.background.left = -500;
            this.background.top = -500;
            this.background.bottom = -500;
            this.background.right = -500;
            this.background.target = cc.director.getScene().getChildByName("Canvas");
        }
        this._animation = this.getComponent(cc.Animation);
        this.node.active = false;
    };
    SicboPopUpInstance.prototype.showPopup = function () {
        var _this = this;
        if (this.panel != null) {
            this.panel.scale = 0;
            this.panel.opacity = 0;
            this.node.active = true;
            cc.Tween.stopAllByTarget(this.panel);
            cc.tween(this.panel)
                .to(0, { scale: 0, opacity: 0 })
                .to(0.2, { scale: this.normalPanelSize * this.scaleWhenOpenAndClosePanelSize, opacity: 255 })
                .to(0.1, { scale: this.normalPanelSize })
                .call(function () {
                _this.showDone();
            }).start();
        }
        else {
            this.node.active = true;
            if (this._animation != null) {
                this._animation.stop();
                this._animation.play("Show");
            }
            else {
                this.showDone();
            }
        }
    };
    SicboPopUpInstance.prototype.hidePopup = function () {
        var _this = this;
        if (this.panel != null) {
            cc.Tween.stopAllByTarget(this.panel);
            cc.tween(this.panel)
                .to(0.2, { scale: this.normalPanelSize * this.scaleWhenOpenAndClosePanelSize, opacity: 0 }).call(function () {
                _this.node.active = false;
                _this.closeDone();
            }).start();
        }
        else {
            if (this._animation != null) {
                this._animation.stop();
                this._animation.play("Hide");
            }
            else {
                this.closeDone();
            }
        }
    };
    SicboPopUpInstance.prototype.open = function (data, onYes, onNo) {
        if (onYes === void 0) { onYes = null; }
        if (onNo === void 0) { onNo = null; }
        this.beforeShow();
        this._open = true;
        this.showPopup();
        this._data = data;
        this._onYes = onYes;
        this._onNo = onNo;
        this.onShow(data);
    };
    SicboPopUpInstance.prototype.closeInstance = function () {
        if (!this._open)
            return;
        this._open = false;
        if (this._close)
            this._close();
        this.beforeClose();
        this.hidePopup();
    };
    SicboPopUpInstance.prototype.close = function () {
        this._open = false;
        if (this._close)
            this._close();
        this.beforeClose();
        this.hidePopup();
    };
    SicboPopUpInstance.prototype.onYes = function () {
        if (this._onYes) {
            this._onYes();
        }
        this.close();
    };
    SicboPopUpInstance.prototype.onNo = function () {
        if (this._onNo) {
            this._onNo();
        }
        this.close();
    };
    //#region Call From Animation Event
    SicboPopUpInstance.prototype.showDone = function () {
        this.afterShow();
    };
    SicboPopUpInstance.prototype.closeDone = function () {
        if (this._open == false) {
            this.node.active = false;
            this.afterClose();
        }
    };
    //#endregion
    SicboPopUpInstance.prototype.onShow = function (data) { };
    SicboPopUpInstance.prototype.afterShow = function () { };
    SicboPopUpInstance.prototype.beforeShow = function () { };
    SicboPopUpInstance.prototype.beforeClose = function () { };
    SicboPopUpInstance.prototype.afterClose = function () { };
    __decorate([
        property(cc.Widget)
    ], SicboPopUpInstance.prototype, "background", void 0);
    __decorate([
        property(cc.Node)
    ], SicboPopUpInstance.prototype, "panel", void 0);
    __decorate([
        property
    ], SicboPopUpInstance.prototype, "normalPanelSize", void 0);
    __decorate([
        property
    ], SicboPopUpInstance.prototype, "scaleWhenOpenAndClosePanelSize", void 0);
    SicboPopUpInstance = __decorate([
        ccclass
    ], SicboPopUpInstance);
    return SicboPopUpInstance;
}(cc.Component));
exports.default = SicboPopUpInstance;

cc._RF.pop();