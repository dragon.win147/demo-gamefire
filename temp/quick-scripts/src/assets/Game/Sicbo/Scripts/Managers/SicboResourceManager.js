"use strict";
cc._RF.push(module, '87163Pl1uZLeLywpKJGF7u4', 'SicboResourceManager');
// Game/Sicbo_New/Scripts/Managers/SicboResourceManager.ts

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboResourceManager = /** @class */ (function () {
    function SicboResourceManager() {
    }
    SicboResourceManager_1 = SicboResourceManager;
    SicboResourceManager.prototype.setAddressResources = function (addressResources) {
        SicboResourceManager_1.addressResources = addressResources;
    };
    SicboResourceManager.loadSpineFromForgeAsObj = function (rf, onComplete) {
        var image = "";
        var json = "";
        var atlas = "";
        var namePng = "";
        rf.filesList.forEach(function (file) {
            switch (file.type) {
                case "image/png":
                    image = file.path;
                    namePng = file.name;
                    break;
                case "application/json":
                    json = file.path;
                    break;
                case "text/plain; charset=utf-8":
                    atlas = file.path;
                    break;
            }
        });
        image = SicboResourceManager_1.addressResources + image;
        json = SicboResourceManager_1.addressResources + json;
        atlas = SicboResourceManager_1.addressResources + atlas;
        this.loadRemoteSpine(image, json, atlas, namePng, onComplete);
    };
    SicboResourceManager.loadRemoteSpine = function (image, json, atlas, namePNG, onComplete) {
        cc.assetManager.loadRemote(image, function (error, texture) {
            cc.assetManager.loadAny({ url: atlas, type: "txt" }, function (error, atlasJson) {
                cc.assetManager.loadAny({ url: json, type: "txt" }, function (error, spineJson) {
                    if (error) {
                        //cc.log(error);
                        return;
                    }
                    var asset = new sp.SkeletonData();
                    asset.skeletonJson = spineJson;
                    asset.atlasText = atlasJson;
                    asset.textures = [texture];
                    asset.textureNames = [namePNG];
                    if (onComplete)
                        onComplete(asset);
                });
            });
        });
    };
    SicboResourceManager.loadRemoteImageWithFileField = function (spr, fileField, onComplete) {
        if (onComplete === void 0) { onComplete = null; }
        if (spr)
            spr.spriteFrame = null;
        this.loadRemoteImageByFileFieldAsObj(fileField, function (spriteFrame) {
            if (spr) {
                spr.spriteFrame = spriteFrame;
                if (onComplete)
                    onComplete();
            }
        });
    };
    SicboResourceManager.loadRemoteImageByFileFieldAsObj = function (fileField, onComplete) {
        if (fileField) {
            var remoteUrl = SicboResourceManager_1.addressResources + fileField.path;
            cc.assetManager.loadRemote(remoteUrl, function (err, result) {
                ////cc.log("url: "+remoteUrl);
                if (err) {
                    //cc.error(err);
                    if (onComplete)
                        onComplete(null);
                    return;
                }
                var res = new cc.SpriteFrame(result);
                if (onComplete)
                    onComplete(res);
            });
        }
        else {
            if (onComplete)
                onComplete(null);
        }
    };
    SicboResourceManager.loadRemoteImageWithPath = function (spr, path, onComplete, resetFrame) {
        if (onComplete === void 0) { onComplete = null; }
        if (resetFrame === void 0) { resetFrame = false; }
        if (resetFrame)
            spr.spriteFrame = null;
        this.loadImageFromForge(path, function (spriteFrame) {
            if (spr) {
                spr.spriteFrame = spriteFrame;
                if (onComplete)
                    onComplete();
            }
        });
    };
    SicboResourceManager.loadImageFromForge = function (path, onComplete) {
        if (path == "" || path == "undefined" || path === undefined) {
            if (onComplete)
                onComplete(null);
            return;
        }
        var remoteUrl = SicboResourceManager_1.addressResources + path;
        cc.assetManager.loadRemote(remoteUrl, function (err, result) {
            ////cc.log("url: "+remoteUrl);
            if (err) {
                ///LogManager.error(err);
                if (onComplete)
                    onComplete(null);
                return;
            }
            var res = new cc.SpriteFrame(result);
            if (onComplete)
                onComplete(res);
        });
    };
    SicboResourceManager.preloadScene = function (nameScene, onProgress, onComplete) {
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        cc.director.preloadScene(nameScene, onProgress, function () {
            if (cc.sys.isNative) {
                // cc.loader.releaseAll();
            }
            if (onComplete)
                onComplete();
        });
    };
    SicboResourceManager.preloadResourceLocal = function (paths, onProgress, onComplete) {
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (paths.length == 0) {
            if (onComplete)
                onComplete();
            return;
        }
        cc.resources.preload(paths, onProgress, function (err, items) {
            if (err) {
                //cc.error(err);
                if (onComplete)
                    onComplete();
                return;
            }
            if (onComplete)
                onComplete();
        });
    };
    SicboResourceManager.preloadResourceRemote = function (paths, onProgress, onComplete) {
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (paths.length == 0) {
            if (onComplete)
                onComplete();
            return;
        }
        var finish = 0;
        var total = paths.length;
        paths.forEach(function (path) {
            path = SicboResourceManager_1.addressResources + path;
            cc.assetManager.loadRemote(path, function (err, item) {
                if (err) {
                    //cc.error(err);
                }
                finish++;
                if (onProgress)
                    onProgress(finish, total, item);
                if (finish == total) {
                    if (onComplete)
                        onComplete();
                }
            });
        });
    };
    SicboResourceManager.loadScene = function (sceneName, callback) {
        if (callback === void 0) { callback = null; }
        // SoundManager.getInstance().stopAllEffects();
        cc.director.loadScene(sceneName, function (err, asset) {
            if (err) {
                //cc.error(err);
                //TODO : handle error load scene : suggest user reload page and force reload
                // GameUtils.showPopupNotice({ title: i18n.t("common.notice"), text: i18n.t("common.reloadPage") }, function () {
                //   window.location.reload(true);
                // });
            }
            if (callback)
                callback();
        });
    };
    var SicboResourceManager_1;
    SicboResourceManager.addressResources = "";
    SicboResourceManager = SicboResourceManager_1 = __decorate([
        ccclass
    ], SicboResourceManager);
    return SicboResourceManager;
}());
exports.default = SicboResourceManager;

cc._RF.pop();