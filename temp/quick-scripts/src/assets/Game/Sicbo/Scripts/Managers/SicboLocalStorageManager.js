"use strict";
cc._RF.push(module, '05744F843tEyLYeZw5Aw4tK', 'SicboLocalStorageManager');
// Game/Sicbo_New/Scripts/Managers/SicboLocalStorageManager.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboLocalStorageManager = /** @class */ (function () {
    function SicboLocalStorageManager() {
    }
    SicboLocalStorageManager.internalGetString = function (key, defaultValue) {
        if (defaultValue === void 0) { defaultValue = ""; }
        var isValue = cc.sys.localStorage.getItem(key);
        return isValue == undefined ? defaultValue : isValue;
    };
    SicboLocalStorageManager.internalSaveString = function (key, strValue) {
        if (strValue === void 0) { strValue = ""; }
        if (strValue == undefined) {
            cc.sys.localStorage.removeItem(key);
        }
        else {
            cc.sys.localStorage.setItem(key, strValue);
        }
    };
    SicboLocalStorageManager.internalSaveBoolean = function (key, defaultValue) {
        if (defaultValue === void 0) { defaultValue = true; }
        if (defaultValue == undefined) {
            cc.sys.localStorage.removeItem(key);
        }
        else {
            cc.sys.localStorage.setItem(key, defaultValue);
        }
    };
    SicboLocalStorageManager.internalGetBoolean = function (key, defaultValue) {
        if (defaultValue === void 0) { defaultValue = true; }
        var isValue = cc.sys.localStorage.getItem(key);
        if (isValue == undefined) {
            isValue = defaultValue;
        }
        if (typeof isValue === "string") {
            return isValue === "true";
        }
        else {
            return isValue;
        }
    };
    SicboLocalStorageManager = __decorate([
        ccclass
    ], SicboLocalStorageManager);
    return SicboLocalStorageManager;
}());
exports.default = SicboLocalStorageManager;

cc._RF.pop();