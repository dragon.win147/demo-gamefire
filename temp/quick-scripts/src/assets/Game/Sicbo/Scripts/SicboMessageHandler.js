"use strict";
cc._RF.push(module, 'dacc33J/ZdEZpZw5bNV0ZB+', 'SicboMessageHandler');
// Game/Sicbo_New/Scripts/SicboMessageHandler.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Code = _a.Code, Empty = _a.Empty, BenTauMessage = _a.BenTauMessage, Topic = _a.Topic, SicboClient = _a.SicboClient, BetDoubleReply = _a.BetDoubleReply, BetDoubleRequest = _a.BetDoubleRequest, BetReply = _a.BetReply, BetRequest = _a.BetRequest, GetOthersReply = _a.GetOthersReply, GetOthersRequest = _a.GetOthersRequest, LeaveRequest = _a.LeaveRequest, Message = _a.Message, ReBetReply = _a.ReBetReply, ReBetRequest = _a.ReBetRequest, State = _a.State, HandlerClientBenTauBase = _a.HandlerClientBenTauBase, AutoJoinReply = _a.AutoJoinReply, LeaveReply = _a.LeaveReply, HealthCheckRequest = _a.HealthCheckRequest, HealthCheckReply = _a.HealthCheckReply, EventKey = _a.EventKey;
var SicboPortalAdapter_1 = require("./SicboPortalAdapter");
var SicboSetting_1 = require("./Setting/SicboSetting");
var SicboText_1 = require("./Setting/SicboText");
var SicboUIManager_1 = require("./Managers/SicboUIManager");
var SicboCannotJoinPopup_1 = require("./PopUps/SicboCannotJoinPopup");
var SicboTracking_1 = require("./SicboTracking");
var SicboMessageHandler = /** @class */ (function (_super) {
    __extends(SicboMessageHandler, _super);
    function SicboMessageHandler() {
        //#region  Instance
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.client = null;
        _this.receiveState = null;
        _this.onErrorCustom = null;
        _this.curChannelId = "sicbov2";
        return _this;
    }
    SicboMessageHandler.getInstance = function () {
        if (!SicboMessageHandler.instance) {
            SicboMessageHandler.instance = new SicboMessageHandler(SicboPortalAdapter_1.default.getBenTauHandler());
            SicboMessageHandler.instance.curChannelId = "sicbov2";
            SicboMessageHandler.instance.client = new SicboClient(SicboPortalAdapter_1.default.addressBenTau(), {}, null);
            SicboMessageHandler.instance.topic = Topic.SICBO;
            SicboMessageHandler.instance.onSubscribe();
        }
        return SicboMessageHandler.instance;
    };
    SicboMessageHandler.destroy = function () {
        if (SicboMessageHandler.instance) {
            SicboMessageHandler.instance.onUnSubscribe();
            SicboMessageHandler.instance.onClearAllMsg();
        }
        SicboMessageHandler.instance = null;
    };
    Object.defineProperty(SicboMessageHandler.prototype, "metaData", {
        get: function () {
            return {
                Authorization: "Bearer " + SicboPortalAdapter_1.default.getToken(),
            };
        },
        enumerable: false,
        configurable: true
    });
    SicboMessageHandler.prototype.showLoading = function (callback) {
        SicboPortalAdapter_1.default.getInstance().showHandlerLoading(callback);
    };
    SicboMessageHandler.prototype.hideLoading = function () {
        SicboPortalAdapter_1.default.getInstance().hideHandlerLoading();
    };
    SicboMessageHandler.prototype.onShowErr = function (str, onClick) {
        //cc.log("SICBO onShowErr", str);
        if (cc.director.getScene().name == SicboSetting_1.SicboScene.Loading) {
            SicboUIManager_1.default.getInstance().openPopup(SicboCannotJoinPopup_1.default, SicboUIManager_1.default.SicboCannotJoinPopup, null);
        }
        else {
            SicboPortalAdapter_1.default.getInstance().onShowHandlerErr(SicboText_1.default.handlerDefaultErrMsg, onClick);
        }
    };
    SicboMessageHandler.prototype.onDisconnect = function (CodeSocket, messError) {
        SicboPortalAdapter_1.default.getInstance().onHandlerDisconnect(CodeSocket, messError);
    };
    SicboMessageHandler.prototype.getChannelId = function () {
        return this.curChannelId;
    };
    SicboMessageHandler.prototype.closeHandler = function () {
        if (SicboMessageHandler.instance)
            SicboMessageHandler.instance.onUnSubscribe();
        delete SicboMessageHandler.instance;
        SicboMessageHandler.instance = null;
    };
    SicboMessageHandler.prototype.onDestroy = function () {
        this.closeHandler();
    };
    //#endregion
    //SECTION implement BaseHandler
    SicboMessageHandler.prototype.onReceive = function (msg) {
        _super.prototype.onReceive.call(this, msg);
        var message = Message.deserializeBinary(msg.getPayload_asU8());
        switch (message.getPayloadCase()) {
            case Message.PayloadCase.STATE:
                var state = message.getState();
                if (this.receiveState)
                    this.receiveState(state);
                break;
            default:
                break;
        }
    };
    //#endregion
    SicboMessageHandler.prototype.handleCustomError = function (errCode, err) {
        if (this.isServerDieCode(errCode)) {
            SicboUIManager_1.default.getInstance().openPopup(SicboCannotJoinPopup_1.default, SicboUIManager_1.default.SicboCannotJoinPopup, null);
        }
        else if (this.onErrorCustom)
            this.onErrorCustom(errCode, err);
    };
    SicboMessageHandler.prototype.isServerDieCode = function (errCode) {
        if (SicboMessageHandler.IS_WS_DISCONNECTED || (!window.navigator || !window.navigator.onLine))
            return false;
        return (errCode == Code.SICBO_CANNOT_JOIN
            || errCode == Code.SICBO_REQUEST_TIMEOUT
            || errCode == Code.UNAVAILABLE
            || errCode == Code.SICBO_SESSION_NOT_FOUND
            || errCode == Code.SICBO_ACTOR_NOT_FOUND
            || errCode == Code.ABORTED
            || errCode == Code.UNAUTHENTICATED
            || errCode == Code.UNKNOWN);
    };
    //!SECTION
    //ANCHOR Bet
    SicboMessageHandler.prototype.isNotEnoughMoney = function (errCode) {
        return (errCode == Code.SICBO_BET_NOT_ENOUGH_MONEY
            || errCode == Code.SICBO_REBET_NOT_ENOUGH_MONEY
            || errCode == Code.SICBO_BET_DOUBLE_NOT_ENOUGH_MONEY);
    };
    SicboMessageHandler.prototype.sendBetRequest = function (door, chip, response) {
        if (response === void 0) { response = null; }
        var self = this;
        var request = new BetRequest();
        request.setDoor(door);
        request.setChip(chip);
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.bet(request, self.metaData, function (err, res) {
                var _a, _b;
                self.onSendReply(err, res, msgId);
                //cc.log("SICBO BetReply", res?.getTx());
                SicboTracking_1.default.trackingBet((_a = res === null || res === void 0 ? void 0 : res.getTx()) === null || _a === void 0 ? void 0 : _a.getLastTxId(), (_b = res === null || res === void 0 ? void 0 : res.getTx()) === null || _b === void 0 ? void 0 : _b.getAmount());
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            if (self.isNotEnoughMoney(code)) {
                cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, SicboMessageHandler.instance.topic, chip);
            }
            else {
                self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
            }
            // self?.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR Rebet
    SicboMessageHandler.prototype.sendRebetRequest = function (response) {
        if (response === void 0) { response = null; }
        var self = this;
        var request = new ReBetRequest();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.rebet(request, self.metaData, function (err, res) {
                var _a, _b;
                self.onSendReply(err, res, msgId);
                //cc.log("SICBO ReBetReply", res?.getTx());
                SicboTracking_1.default.trackingReBet((_a = res === null || res === void 0 ? void 0 : res.getTx()) === null || _a === void 0 ? void 0 : _a.getLastTxId(), (_b = res === null || res === void 0 ? void 0 : res.getTx()) === null || _b === void 0 ? void 0 : _b.getAmount());
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            if (self.isNotEnoughMoney(code)) {
                cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, SicboMessageHandler.instance.topic, 0);
            }
            else {
                self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
            }
            // self?.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR Double bet
    SicboMessageHandler.prototype.sendBetDoubleRequest = function (response) {
        if (response === void 0) { response = null; }
        var self = this;
        var request = new BetDoubleRequest();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.betDouble(request, self.metaData, function (err, res) {
                var _a, _b;
                self.onSendReply(err, res, msgId);
                //cc.log("SICBO BetDoubleReply", res?.getTx());
                SicboTracking_1.default.trackingDoubleBet((_a = res === null || res === void 0 ? void 0 : res.getTx()) === null || _a === void 0 ? void 0 : _a.getLastTxId(), (_b = res === null || res === void 0 ? void 0 : res.getTx()) === null || _b === void 0 ? void 0 : _b.getAmount());
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            if (self.isNotEnoughMoney(code)) {
                cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, SicboMessageHandler.instance.topic, 0);
            }
            else {
                self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
            }
            // self?.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR AutoJoin
    SicboMessageHandler.prototype.sendAutoJoinRequest = function (response) {
        var self = this;
        var request = new Empty();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.autoJoin(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR Leave
    SicboMessageHandler.prototype.sendLeaveRequest = function (response) {
        var self = this;
        var request = new LeaveRequest();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.leave(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR get Others
    SicboMessageHandler.prototype.sendGetOthersRequest = function (from, limit, response) {
        var self = this;
        var request = new GetOthersRequest();
        request.setCursor(from);
        request.setLimit(limit);
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.getOthers(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR - Health check
    SicboMessageHandler.prototype.sendHealthCheck = function (response) {
        var self = this;
        var request = new HealthCheckRequest();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.healthCheck(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
        }, false);
    };
    SicboMessageHandler.IS_WS_DISCONNECTED = false;
    SicboMessageHandler.benTauHandler = null;
    return SicboMessageHandler;
}(HandlerClientBenTauBase));
exports.default = SicboMessageHandler;

cc._RF.pop();