"use strict";
cc._RF.push(module, 'be40et5SA5E16BUkW/4QWUD', 'SicboController');
// Game/Sicbo/Scripts/Controllers/SicboController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Code = _a.Code, Topic = _a.Topic, State = _a.State, Event = _a.Event, DoorAndBetAmount = _a.DoorAndBetAmount, Status = _a.Status, WhatAppMessage = _a.WhatAppMessage, EventKey = _a.EventKey, AutoJoinReply = _a.AutoJoinReply;
var SicboUIComp_1 = require("../Components/SicboUIComp");
var SicboHelper_1 = require("../Helpers/SicboHelper");
var SicboMessageHandler_1 = require("../SicboMessageHandler");
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboSoundManager_1 = require("../Managers/SicboSoundManager");
var SicboPortalAdapter_1 = require("../SicboPortalAdapter");
var SicboHandleEventShowHideComp_1 = require("../RNGCommons/SicboHandleEventShowHideComp");
var SicboTracking_1 = require("../SicboTracking");
var SicboUIManager_1 = require("../Managers/SicboUIManager");
var ccclass = cc._decorator.ccclass;
var SicboController = /** @class */ (function (_super) {
    __extends(SicboController, _super);
    function SicboController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.stateStatus = Status.STATUS_UNSPECIFIED;
        _this.isKicked = false;
        _this.handleEventShowHideComp = null;
        _this.lastTickTime = -100;
        _this.lastSession = 0;
        _this.appHiding = false;
        _this.appResuming = false;
        _this.isDisconnected = false;
        return _this;
    }
    SicboController_1 = SicboController;
    Object.defineProperty(SicboController, "Instance", {
        get: function () {
            return SicboController_1.instance;
        },
        enumerable: false,
        configurable: true
    });
    SicboController.prototype.onLoad = function () {
        SicboController_1.instance = this;
        if (!SicboController_1.Instance.handleEventShowHideComp)
            SicboController_1.Instance.handleEventShowHideComp = SicboController_1.Instance.addComponent(SicboHandleEventShowHideComp_1.default); //Device home button
        SicboController_1.Instance.handleEventShowHideComp.listenEvent(function () { return SicboController_1.Instance.onHideEvent(); }, function () { return SicboController_1.Instance.onShowEvent(); });
        cc.systemEvent.on(EventKey.ON_PING_ERROR, function (code, isRetry, messError) {
            SicboMessageHandler_1.default.IS_WS_DISCONNECTED = true;
            SicboController_1.Instance.isDisconnected = true;
            SicboController_1.Instance.onHideEvent();
            //cc.warn("EventKey.ON_DISCONNECT_EVENT", code, isRetry, messError)
        }, this);
        cc.systemEvent.on(EventKey.ON_RECONNECTED_EVENT, function () {
            SicboMessageHandler_1.default.IS_WS_DISCONNECTED = false;
            SicboController_1.Instance.isDisconnected = false;
            SicboUIManager_1.default.getInstance().hidePopup(SicboUIManager_1.default.SicboCannotJoinPopup);
            SicboController_1.Instance.onShowEvent();
        }, this);
    };
    SicboController.prototype.start = function () {
        SicboMessageHandler_1.default.getInstance().receiveState = SicboController_1.Instance.onReceiveState;
        SicboMessageHandler_1.default.getInstance().onErrorCustom = SicboController_1.Instance.onError;
        cc.systemEvent.on(EventKey.ON_ADD_BALANCE_EVENT, SicboController_1.Instance.onBalanceAdded);
        cc.systemEvent.on(EventKey.ON_UPDATE_BALANCE_EVENT, SicboController_1.Instance.onBalanceUpdated);
        SicboPortalAdapter_1.default.getInstance().getLatestBalance();
        SicboController_1.Instance.onBalanceUpdated();
        SicboController_1.Instance.playBgm();
        // SicboPortalAdapter.getInstance().getBundleInfo(bundleInfo=>{
        //   SicboTracking.setGameVersion(bundleInfo?.version);
        // })
    };
    SicboController.prototype.onBalanceUpdated = function () {
        SicboPortalAdapter_1.default.getInstance().getCurrentBalance(function (balance) {
            SicboUIComp_1.default.Instance.changeMyUserBalance(balance);
        });
    };
    SicboController.prototype.onBalanceAdded = function () {
        SicboPortalAdapter_1.default.getInstance().getCurrentBalance(function (balance) {
            SicboUIComp_1.default.Instance.changeMyUserBalance(balance);
        });
    };
    SicboController.prototype.onEnable = function () {
        SicboPortalAdapter_1.default.getInstance().enableWallet();
        var chanelID = SicboMessageHandler_1.default.getInstance().getChannelId();
        this.handleNotEnoughMoneyOnJointGame(Math.min.apply(Math, SicboSetting_1.default.getChipLevelList()));
    };
    SicboController.prototype.onDisable = function () {
        SicboPortalAdapter_1.default.getInstance().disableWallet();
    };
    SicboController.prototype.onDestroy = function () {
        cc.systemEvent.off(EventKey.ON_ADD_BALANCE_EVENT, SicboController_1.Instance.onBalanceAdded);
        cc.systemEvent.off(EventKey.ON_UPDATE_BALANCE_EVENT, SicboController_1.Instance.onBalanceUpdated);
        SicboMessageHandler_1.default.getInstance().onDestroy();
        SicboPortalAdapter_1.default.destroy();
        SicboController_1.Instance.unscheduleAllCallbacks();
        delete SicboController_1.instance;
        SicboController_1.instance = null;
    };
    SicboController.prototype.onHideEvent = function () {
        //cc.log("SICBO onHideEvent");
        SicboUIComp_1.default.Instance.onHide();
        SicboController_1.Instance.appHiding = true;
        SicboController_1.Instance.appResuming = false;
    };
    SicboController.prototype.onShowEvent = function () {
        //cc.log("SICBO onShowEvent");
        SicboController_1.Instance.stateStatus = Status.STATUS_UNSPECIFIED;
        SicboController_1.Instance.appHiding = false;
        SicboController_1.Instance.appResuming = true;
        if (SicboController_1.Instance.isKicked)
            return;
        SicboPortalAdapter_1.default.getInstance().getLatestBalance();
        SicboMessageHandler_1.default.getInstance().onClearAllMsg();
        SicboMessageHandler_1.default.getInstance().sendAutoJoinRequest(function () {
            SicboController_1.Instance.onBalanceUpdated();
        });
    };
    //ANCHOR My User Wallet
    SicboController.prototype.addMyUserMoney = function (amount, transaction) {
        SicboPortalAdapter_1.default.getInstance().addMoney(amount, transaction);
    };
    //SECTION Send message
    SicboController.prototype.sendCheckServerAvailable = function () {
        //Work around for checking server available
        SicboMessageHandler_1.default.getInstance().sendHealthCheck(function (res) { });
    };
    SicboController.prototype.sendBet = function (door, coinType, onSuccess) {
        if (onSuccess === void 0) { onSuccess = null; }
        var self = this;
        var coinNumber = SicboHelper_1.default.convertCoinTypeToValue(coinType);
        self.checkWalletLocalForBetting(coinNumber, function (enoughMoney) {
            if (enoughMoney) {
                SicboMessageHandler_1.default.getInstance().sendBetRequest(door, coinNumber, function (res) {
                    self.addMyUserMoney(res.getTx().getAmount(), res.getTx().getLastTxId());
                    onSuccess && onSuccess();
                });
            }
            else {
                cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, Topic.SICBO, coinNumber);
                // self.onError(Code.SICBO_BET_NOT_ENOUGH_MONEY, "");
            }
        });
    };
    SicboController.prototype.sendReBet = function (onSuccess) {
        if (onSuccess === void 0) { onSuccess = null; }
        SicboMessageHandler_1.default.getInstance().sendRebetRequest(function (res) {
            SicboController_1.Instance.addMyUserMoney(res.getTx().getAmount(), res.getTx().getLastTxId());
            onSuccess && onSuccess(res.getDoorsAndBetAmountsList());
        });
    };
    SicboController.prototype.sendBetDouble = function (onSuccess) {
        if (onSuccess === void 0) { onSuccess = null; }
        SicboMessageHandler_1.default.getInstance().sendBetDoubleRequest(function (res) {
            SicboController_1.Instance.addMyUserMoney(res.getTx().getAmount(), res.getTx().getLastTxId());
            onSuccess && onSuccess(res.getDoorsAndBetAmountsList());
        });
    };
    SicboController.prototype.checkWalletLocalForBetting = function (amount, callback) {
        SicboPortalAdapter_1.default.getInstance().getCurrentBalance(function (balance) {
            callback && callback(amount <= balance);
        });
    };
    //!SECTION
    //SECTION Receive message from MessageHandler
    SicboController.prototype.onError = function (errCode, err) {
        var _a;
        //cc.log("SICBO ERROR", errCode, err);
        (_a = SicboUIComp_1.default.Instance) === null || _a === void 0 ? void 0 : _a.showErrMessage(errCode, err);
    };
    SicboController.prototype.onReceiveState = function (state) {
        SicboSetting_1.default.setStatusDurationConfig(state.getSessionStatesTime());
        SicboUIComp_1.default.Instance.updateUsersInRoom(state.getDisplayPlayersList(), state.getRemainingPlayersCount());
        var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
        state.getEventsList().forEach(function (e) {
            var payloadCase = e.getPayloadCase();
            switch (payloadCase) {
                case Event.PayloadCase.BET:
                    if (e.getUserId() == myUserId) {
                        //cc.error("SICBO Event.PayloadCase.BET broadcast self userid");
                    }
                    else
                        SicboUIComp_1.default.Instance.playUserBet(e.getUserId(), e.getBet().getDoor(), e.getBet().getChip());
                    break;
                //TODO: xu ly kick, join, ... .
                case Event.PayloadCase.KICKED:
                    if (e.hasKicked() && e.getUserId() == myUserId) {
                        SicboController_1.Instance.isKicked = true;
                    }
                    break;
                default:
                    break;
            }
        });
        if (SicboController_1.Instance.isKicked)
            return;
        if (SicboController_1.Instance.appHiding)
            return;
        if (SicboController_1.Instance.isDisconnected)
            return;
        // if(SicboController.Instance.appResuming){
        //   if(state.getStatus() == Status.PAYING){
        //     SicboMessageHandler.getInstance().sendLeaveRequest(() => {
        //       SicboController.Instance.leaveGame();
        //     });
        //   }
        // }
        if (SicboController_1.Instance.lastSession != state.getSessionId()) {
            SicboController_1.Instance.startNewSession(state.getSessionId());
            //cc.log("ZZZZZZ startNewSession", SicboController.Instance.stateStatus, SicboController.Instance.lastTickTime);
        }
        if ((SicboController_1.Instance.stateStatus != state.getStatus() && SicboController_1.Instance.isValidTick(state.getTickTime())) || SicboController_1.Instance.appResuming) {
            SicboController_1.Instance.exitState(SicboController_1.Instance.stateStatus);
            SicboController_1.Instance.startState(state, SicboSetting_1.default.getStatusDuration(state.getStatus()));
            SicboController_1.Instance.lastTickTime = state.getTickTime();
        }
        SicboController_1.Instance.stateStatus = state.getStatus();
        SicboController_1.Instance.updateState(state);
        SicboController_1.Instance.appResuming = false;
    };
    SicboController.prototype.exitState = function (status) {
        SicboUIComp_1.default.Instance.exitState(status);
    };
    SicboController.prototype.updateState = function (state) {
        SicboUIComp_1.default.Instance.updateState(state);
    };
    SicboController.prototype.startState = function (state, stateDuration) {
        if (stateDuration == 0) {
            //cc.log("SKIP STATE DURATION = 0", state.getStatus().toString());
            return;
        }
        SicboUIComp_1.default.Instance.startState(state, stateDuration);
    };
    SicboController.prototype.startNewSession = function (newSession) {
        SicboController_1.Instance.lastSession = newSession;
        SicboController_1.Instance.lastTickTime = -100;
        SicboTracking_1.default.startSession(newSession);
    };
    SicboController.prototype.isValidTick = function (curTick) {
        //cc.log("ZZZZZZ isValidTick", curTick, SicboController.Instance.lastTickTime)
        return true;
        // return curTick >= SicboController.Instance.lastTickTime;
    };
    //!SECTION
    //SECTION Sound
    SicboController.prototype.playBgm = function () {
        SicboSoundManager_1.default.getInstance().play(SicboSetting_1.default.getSoundName(SicboSetting_1.SicboSound.BGM));
    };
    SicboController.prototype.playSfx = function (sound, loop) {
        if (loop === void 0) { loop = false; }
        SicboSoundManager_1.default.getInstance().play(SicboSetting_1.default.getSoundName(sound));
    };
    SicboController.prototype.stopSfx = function (sound) {
        SicboSoundManager_1.default.getInstance().stop(SicboSetting_1.default.getSoundName(sound));
    };
    //!SECTION
    SicboController.prototype.leaveGame = function () {
        // SicboPortalAdapter.getInstance().leaveGame();
        cc.director.loadScene("LoginScene");
    };
    SicboController.prototype.getGameInfo = function () {
        // let games = SicboPortalAdapter.getInstance().getGameInfo() as any[];
        // let res: [] = [];
        // games.forEach((game) => {
        //   if (game.topic == Topic.SICBO) {
        //     res = game.info.filesList;
        //   }
        // });
        // return res;
    };
    SicboController.prototype.handleNotEnoughMoneyOnJointGame = function (coinNumber) {
        this.checkWalletLocalForBetting(coinNumber, function (enoughMoney) {
            if (!enoughMoney)
                cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_JOINING_GAME, Topic.SICBO, coinNumber);
        });
    };
    var SicboController_1;
    SicboController.instance = null;
    SicboController = SicboController_1 = __decorate([
        ccclass
    ], SicboController);
    return SicboController;
}(cc.Component));
exports.default = SicboController;

cc._RF.pop();