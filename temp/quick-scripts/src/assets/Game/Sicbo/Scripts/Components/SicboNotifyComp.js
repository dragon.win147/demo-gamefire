"use strict";
cc._RF.push(module, '74b43csqj1LFYozQkxS+7XD', 'SicboNotifyComp');
// Game/Sicbo_New/Scripts/Components/SicboNotifyComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboController_1 = require("../Controllers/SicboController");
var SicboSkeletonUtils_1 = require("../RNGCommons/Utils/SicboSkeletonUtils");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboNotifyComp = /** @class */ (function (_super) {
    __extends(SicboNotifyComp, _super);
    function SicboNotifyComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //SMALL NOTI
        _this.smallNotiRoot = null;
        _this.smallNotiMessage = null;
        _this.skipAllNextNoti = false;
        //LARGE NOTI
        _this.largeNotiSkeleton = null;
        _this.largeNotiMessage = null;
        _this.bettingMessageSprite = null;
        _this.stopBettingMessageSprite = null;
        //RESULT NOTI
        _this.resultNotiSkeleton = null;
        _this.resultNotiMessage = null;
        return _this;
    }
    SicboNotifyComp.prototype.onLoad = function () {
        this.smallNotiRoot.opacity = 0;
        this.largeNotiSkeleton.node.opacity = 0;
    };
    //SECTION SMALL
    SicboNotifyComp.prototype.showSmallNoti = function (message, enterTime, showTime, exitTime, skipAllNextNoti) {
        if (enterTime === void 0) { enterTime = .5; }
        if (showTime === void 0) { showTime = 2; }
        if (exitTime === void 0) { exitTime = .5; }
        if (skipAllNextNoti === void 0) { skipAllNextNoti = false; }
        if (this.skipAllNextNoti)
            return;
        var self = this;
        cc.Tween.stopAllByTarget(this.smallNotiRoot);
        self.smallNotiMessage.string = message;
        self.smallNotiRoot.active = true;
        cc.tween(self.smallNotiRoot)
            .to(enterTime, { opacity: 255, scale: 1 })
            .delay(showTime)
            .to(exitTime, { opacity: 150 })
            .call(function () {
            self.skipAllNextNoti = false;
            self.smallNotiRoot.active = false;
            self.smallNotiRoot.scale = 2;
        })
            .start();
        this.skipAllNextNoti = skipAllNextNoti;
    };
    SicboNotifyComp.prototype.updateSmallNotiMessage = function (message) {
        this.smallNotiMessage.string = message;
    };
    SicboNotifyComp.prototype.forceHideSmallNoti = function () {
        cc.Tween.stopAllByTarget(this.smallNotiRoot);
        this.smallNotiRoot.opacity = 150;
        this.smallNotiRoot.active = false;
        this.smallNotiRoot.scale = 2;
        this.skipAllNextNoti = false;
    };
    //!SECTION
    //SECTION LARGE
    SicboNotifyComp.prototype.showLargeNoti = function (enterTime, showTime, exitTime) {
        if (enterTime === void 0) { enterTime = 0; }
        if (showTime === void 0) { showTime = 2.5; }
        if (exitTime === void 0) { exitTime = .5; }
        cc.Tween.stopAllByTarget(this.largeNotiSkeleton.node);
        SicboSkeletonUtils_1.default.setAnimation(this.largeNotiSkeleton, "noti_2");
        this.largeNotiSkeleton.node.opacity = 255;
        cc.tween(this.largeNotiSkeleton.node)
            .to(enterTime, { opacity: 255 })
            .delay(showTime)
            .to(exitTime, { opacity: 0 })
            .start();
        SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxMessage);
    };
    SicboNotifyComp.prototype.showBettingMessage = function () {
        this.largeNotiMessage.spriteFrame = this.bettingMessageSprite;
        this.showLargeNoti();
    };
    SicboNotifyComp.prototype.showStopBettingMessage = function () {
        this.largeNotiMessage.spriteFrame = this.stopBettingMessageSprite;
        this.showLargeNoti();
    };
    SicboNotifyComp.prototype.forceHideLargeNoti = function () {
        cc.Tween.stopAllByTarget(this.largeNotiSkeleton.node);
        this.largeNotiSkeleton.node.opacity = 0;
    };
    //!SECTION
    //SECTION RESULT
    SicboNotifyComp.prototype.showResultNoti = function (message, enterTime, showTime, exitTime) {
        var _this = this;
        if (enterTime === void 0) { enterTime = .34; }
        if (showTime === void 0) { showTime = 2.5; }
        if (exitTime === void 0) { exitTime = .34; }
        cc.Tween.stopAllByTarget(this.resultNotiSkeleton.node);
        this.resultNotiMessage.string = message;
        SicboSkeletonUtils_1.default.setAnimation(this.resultNotiSkeleton, "in");
        this.resultNotiSkeleton.node.opacity = 255;
        cc.tween(this.resultNotiSkeleton.node)
            .delay(enterTime)
            .call(function () {
            SicboSkeletonUtils_1.default.setAnimation(_this.resultNotiSkeleton, "loop", true);
        })
            .delay(showTime)
            .call(function () {
            SicboSkeletonUtils_1.default.setAnimation(_this.resultNotiSkeleton, "out", false);
        })
            .delay(exitTime)
            .call(function () {
            _this.resultNotiSkeleton.node.opacity = 0;
        })
            .start();
        this.resultNotiMessage.node.opacity = 0;
        cc.tween(this.resultNotiMessage.node)
            .delay(enterTime / 2)
            .to(enterTime / 2, { opacity: 255 })
            .delay(showTime)
            .to(exitTime, { opacity: 0 })
            .start();
        // SicboController.Instance.playSfx(SicboSound.SfxMessage);
    };
    SicboNotifyComp.prototype.forceHideResultNoti = function () {
        cc.Tween.stopAllByTarget(this.resultNotiSkeleton.node);
        this.resultNotiSkeleton.node.opacity = 0;
    };
    //!SECTION
    SicboNotifyComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.smallNotiRoot);
        cc.Tween.stopAllByTarget(this.largeNotiSkeleton.node);
        cc.Tween.stopAllByTarget(this.resultNotiSkeleton.node);
        cc.Tween.stopAllByTarget(this.resultNotiMessage.node);
    };
    __decorate([
        property(cc.Node)
    ], SicboNotifyComp.prototype, "smallNotiRoot", void 0);
    __decorate([
        property(cc.Label)
    ], SicboNotifyComp.prototype, "smallNotiMessage", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboNotifyComp.prototype, "largeNotiSkeleton", void 0);
    __decorate([
        property(cc.Sprite)
    ], SicboNotifyComp.prototype, "largeNotiMessage", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], SicboNotifyComp.prototype, "bettingMessageSprite", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], SicboNotifyComp.prototype, "stopBettingMessageSprite", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboNotifyComp.prototype, "resultNotiSkeleton", void 0);
    __decorate([
        property(cc.Label)
    ], SicboNotifyComp.prototype, "resultNotiMessage", void 0);
    SicboNotifyComp = __decorate([
        ccclass
    ], SicboNotifyComp);
    return SicboNotifyComp;
}(cc.Component));
exports.default = SicboNotifyComp;

cc._RF.pop();