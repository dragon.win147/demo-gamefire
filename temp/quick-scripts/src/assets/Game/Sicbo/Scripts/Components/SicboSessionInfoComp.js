"use strict";
cc._RF.push(module, '0c13aRWN9ZJ6bYY0gIRCgM2', 'SicboSessionInfoComp');
// Game/Sicbo_New/Scripts/Components/SicboSessionInfoComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), State = _a.State, Status = _a.Status;
var SicboMoneyFormatComp_1 = require("../RNGCommons/SicboMoneyFormatComp");
var SicboGameUtils_1 = require("../RNGCommons/Utils/SicboGameUtils");
var SicboText_1 = require("../Setting/SicboText");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboSessionInfoComp = /** @class */ (function (_super) {
    __extends(SicboSessionInfoComp, _super);
    function SicboSessionInfoComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sessionIdLabel = null;
        _this.totalBetLabel = null;
        _this.diceLabel = null;
        _this.md5Desc = null;
        _this.keyDesc = null;
        _this.md5Label = null;
        _this.copyNotiNode = null;
        _this.md5Mask = null;
        _this.md5ParentNode = null;
        _this.layout = null;
        _this.shinnyAnimation = null;
        _this.md5Color = cc.Color.WHITE;
        _this.keyColor = cc.Color.RED;
        _this.md5Cache = null;
        // private MD5_LENGTH = 32;
        _this.MD5_MASK_WIDTH = 310;
        _this.KEY_WIDTH = 275;
        _this.MD5_WIDTH = 295;
        // private MD5_MASK_LEFT_POS = -26.5;
        // private MD5_MASK_RIGHT_POS = this.MD5_MASK_LEFT_POS + this.MD5_MASK_WIDTH;
        _this.md5NewStartTime = .7;
        _this.keyNewStartTime = .6;
        _this.MD5_STR = "MD5 :";
        _this.KEY_STR = "KEY :";
        return _this;
    }
    SicboSessionInfoComp.prototype.onLoad = function () {
        this.shinnyAnimation.node.active = false;
        this.MD5_MASK_WIDTH = this.md5Mask.width;
    };
    SicboSessionInfoComp.prototype.setMaskAnchor = function (isLeft) {
        if (isLeft === void 0) { isLeft = true; }
        // this.md5Mask.y = 0;
        if (isLeft) {
            this.md5Mask.anchorX = 0;
            // this.md5Mask.x = this.MD5_MASK_LEFT_POS;
            this.md5ParentNode.setPosition(cc.Vec2.ZERO);
        }
        else {
            this.md5Mask.anchorX = 1;
            // this.md5Mask.x = this.MD5_MASK_RIGHT_POS;
            this.md5ParentNode.setPosition(cc.Vec2.ZERO);
            this.md5ParentNode.x = -this.MD5_MASK_WIDTH;
        }
    };
    SicboSessionInfoComp.prototype.exitState = function (status) {
    };
    SicboSessionInfoComp.prototype.startState = function (state, totalDuration) {
        this.setSessionId(state.getTableSessionId().toString());
        var status = state.getStatus();
        switch (status) {
            // case Status.WAITING:
            //   this.setNewMd5(state.getPrevMd5Result(), state.getMd5Result(), elapseTime);
            //   break;
            case Status.BETTING:
            case Status.END_BET:
                this.md5Cache = state.getMd5Result();
                this.setMd5Style();
                this.updateMd5Label(this.md5Cache);
                break;
            // case Status.RESULTING:
            //   this.setNewKey(state.getMd5Result(), state.getKeyResult(), elapseTime);
            //   break;
            case Status.PAYING:
            case Status.FINISHING:
                this.md5Cache = state.getKeyResult();
                this.updateMd5Label(this.md5Cache);
                this.setKeyStyle();
                break;
        }
    };
    SicboSessionInfoComp.prototype.updateState = function (state) {
        this.setTotalBet(state.getSessionPlayerBettedAmount());
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        switch (status) {
            case Status.WAITING:
                this.setNewMd5(state.getPrevKeyResult(), state.getMd5Result(), elapseTime);
                break;
            case Status.RESULTING:
                this.setNewKey(state.getMd5Result(), state.getKeyResult(), elapseTime);
                break;
        }
    };
    SicboSessionInfoComp.prototype.onHide = function () {
        this.md5Label.node.width = 0;
        // cc.Tween.stopAllByTarget(this.copyNotiNode);
        // cc.Tween.stopAllByTarget(this.md5Mask);
        // cc.Tween.stopAllByTarget(this.layout);    
        cc.Tween.stopAllByTarget(this.node);
    };
    SicboSessionInfoComp.prototype.setSessionId = function (sessionId) {
        this.sessionIdLabel.string = SicboGameUtils_1.default.FormatString(SicboText_1.default.sessionIdText, sessionId);
    };
    SicboSessionInfoComp.prototype.setTotalBet = function (totalBet) {
        this.totalBetLabel.setMoney(totalBet);
    };
    SicboSessionInfoComp.prototype.setNewMd5 = function (md5PrevRaw, md5NewRaw, elapseTime) {
        if (elapseTime > this.md5NewStartTime) {
            this.md5Cache = md5NewRaw;
            this.updateMd5Label(this.md5Cache);
            this.setMd5Style();
        }
        else if (elapseTime < this.md5NewStartTime) {
            this.md5Cache = md5PrevRaw;
            this.setKeyStyle();
            this.updateMd5Label(this.md5Cache);
        }
        else {
            var self_1 = this;
            cc.tween(self_1.node)
                // .delay(self.md5NewStartTime - elapseTime)
                .call(function () {
                self_1.md5Cache = md5NewRaw;
                self_1.setMd5Style();
                self_1.setMaskAnchor(true);
                self_1.playLeftToRight();
                self_1.updateMd5Label(self_1.md5Cache);
            }).start();
        }
    };
    SicboSessionInfoComp.prototype.setNewKey = function (md5NewRaw, keyNewRaw, elapseTime) {
        if (elapseTime > this.keyNewStartTime) {
            this.md5Cache = keyNewRaw;
            this.updateMd5Label(this.md5Cache);
            this.setKeyStyle();
        }
        else if (elapseTime < this.keyNewStartTime) {
            this.md5Cache = md5NewRaw;
            this.updateMd5Label(this.md5Cache);
            this.setMd5Style();
        }
        else {
            var self_2 = this;
            cc.tween(self_2.node)
                // .delay(self.keyNewStartTime - elapseTime)
                .call(function () {
                self_2.md5Cache = keyNewRaw;
                self_2.updateMd5Label(self_2.md5Cache);
                self_2.setKeyStyle();
                self_2.setMaskAnchor(false);
                self_2.playRightToLeft();
            }).start();
        }
    };
    SicboSessionInfoComp.prototype.setKeyStyle = function () {
        this.md5Desc.active = false;
        this.keyDesc.active = true;
        this.md5Label.node.color = this.keyColor;
        this.md5Label.node.width = this.KEY_WIDTH;
        this.diceLabel.node.active = true;
    };
    SicboSessionInfoComp.prototype.setMd5Style = function () {
        this.md5Desc.active = true;
        this.keyDesc.active = false;
        this.md5Label.node.color = this.md5Color;
        this.md5Label.node.width = this.MD5_WIDTH;
        this.diceLabel.node.active = false;
    };
    SicboSessionInfoComp.prototype.updateMd5Label = function (rawStr) {
        var _a = this.parseMd5(rawStr), diceStr = _a[0], md5Str = _a[1];
        this.md5Label.string = md5Str;
        this.diceLabel.string = diceStr;
    };
    SicboSessionInfoComp.prototype.playRightToLeft = function () {
        // this.setMaskAnchor(false);
        this.playMd5Anim(true);
        this.shinnyAnimation.node.active = true;
        this.shinnyAnimation.play("rightToLeft");
    };
    SicboSessionInfoComp.prototype.playLeftToRight = function () {
        // this.setMaskAnchor(true);
        this.playMd5Anim();
        this.shinnyAnimation.node.active = true;
        this.shinnyAnimation.play("leftToRight");
    };
    SicboSessionInfoComp.prototype.playMd5Anim = function (animLayout, animTime) {
        if (animLayout === void 0) { animLayout = false; }
        if (animTime === void 0) { animTime = 1.15; }
        this.md5Mask.width = 5;
        cc.tween(this.md5Mask)
            .to(animTime, { width: this.MD5_MASK_WIDTH })
            .start();
        if (!animLayout) {
            this.layout.spacingX = 0;
            return;
        }
        this.layout.spacingX = this.MD5_MASK_WIDTH - 5;
        cc.tween(this.layout)
            .to(animTime, { spacingX: 0 })
            .start();
    };
    /**
     *
     * @param md5Raw
     * @returns [diceStr, md5Str]
     */
    SicboSessionInfoComp.prototype.parseMd5 = function (md5Raw) {
        // md5Raw = md5Raw.replace(":", "");
        var md5Str = "";
        var diceStr = "";
        var temp = md5Raw.indexOf("]");
        if (temp < 0) {
            md5Str = md5Raw;
            diceStr = "\n";
        }
        else {
            md5Str = md5Raw.substr(temp + 1);
            diceStr = md5Raw.substr(0, temp + 1);
        }
        return [diceStr, md5Str];
    };
    SicboSessionInfoComp.prototype.onClickCopy = function () {
        SicboGameUtils_1.default.copyToClipboard(this.md5Cache);
        this.playCopyNoti();
    };
    SicboSessionInfoComp.prototype.playCopyNoti = function () {
        cc.Tween.stopAllByTarget(this.copyNotiNode);
        cc.tween(this.copyNotiNode)
            .to(.2, { scale: 1, opacity: 255 })
            .delay(1)
            .to(.2, { scale: 0, opacity: 0 })
            .start();
    };
    SicboSessionInfoComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.copyNotiNode);
        cc.Tween.stopAllByTarget(this.md5Mask);
        cc.Tween.stopAllByTarget(this.layout);
        cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.Label)
    ], SicboSessionInfoComp.prototype, "sessionIdLabel", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboSessionInfoComp.prototype, "totalBetLabel", void 0);
    __decorate([
        property(cc.Label)
    ], SicboSessionInfoComp.prototype, "diceLabel", void 0);
    __decorate([
        property(cc.Node)
    ], SicboSessionInfoComp.prototype, "md5Desc", void 0);
    __decorate([
        property(cc.Node)
    ], SicboSessionInfoComp.prototype, "keyDesc", void 0);
    __decorate([
        property(cc.Label)
    ], SicboSessionInfoComp.prototype, "md5Label", void 0);
    __decorate([
        property(cc.Node)
    ], SicboSessionInfoComp.prototype, "copyNotiNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboSessionInfoComp.prototype, "md5Mask", void 0);
    __decorate([
        property(cc.Node)
    ], SicboSessionInfoComp.prototype, "md5ParentNode", void 0);
    __decorate([
        property(cc.Layout)
    ], SicboSessionInfoComp.prototype, "layout", void 0);
    __decorate([
        property(cc.Animation)
    ], SicboSessionInfoComp.prototype, "shinnyAnimation", void 0);
    __decorate([
        property(cc.Color)
    ], SicboSessionInfoComp.prototype, "md5Color", void 0);
    __decorate([
        property(cc.Color)
    ], SicboSessionInfoComp.prototype, "keyColor", void 0);
    SicboSessionInfoComp = __decorate([
        ccclass
    ], SicboSessionInfoComp);
    return SicboSessionInfoComp;
}(cc.Component));
exports.default = SicboSessionInfoComp;

cc._RF.pop();