"use strict";
cc._RF.push(module, 'bfe0b8BCa9KTrKHNTCEyGMw', 'SicboBaseDropDownComp');
// Game/Sicbo_New/Scripts/Components/DropDown/SicboBaseDropDownComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboBaseDropDownItemComp_1 = require("./SicboBaseDropDownItemComp");
var Vec3 = cc.Vec3;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboBaseDropDownComp = /** @class */ (function (_super) {
    __extends(SicboBaseDropDownComp, _super);
    function SicboBaseDropDownComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.view = [];
        _this.loadObject = false;
        _this.template = null;
        _this.labelCaption = null;
        _this.spriteCaption = null;
        _this.labelItem = null;
        _this.spriteItem = null;
        _this.arrow = null;
        _this.blackCover = null;
        // extra thing you wanna do when Show()
        _this.extraShow = function () { };
        _this.extraHide = function () { };
        _this.clickFunc = [];
        _this.optionDatas = [];
        _this.validTemplate = false;
        _this.items = [];
        _this.isShow = false;
        _this._selectedIndex = -1;
        return _this;
    }
    Object.defineProperty(SicboBaseDropDownComp.prototype, "selectedIndex", {
        get: function () {
            return this._selectedIndex;
        },
        set: function (value) {
            this._selectedIndex = value;
            this.refreshShownValue();
        },
        enumerable: false,
        configurable: true
    });
    SicboBaseDropDownComp.prototype.addOptionDatas = function (optionDatas) {
        var _this = this;
        optionDatas &&
            optionDatas.forEach(function (data) {
                _this.optionDatas.push(data);
            });
        this.refreshShownValue();
    };
    SicboBaseDropDownComp.prototype.clearOptionDatas = function () {
        cc.js.clear(this.optionDatas);
        this.refreshShownValue();
    };
    SicboBaseDropDownComp.prototype.resetSelectIndex = function () {
        this.selectedIndex = 0;
    };
    SicboBaseDropDownComp.prototype.setSelectIndex = function (int) {
        this.selectedIndex = int;
    };
    SicboBaseDropDownComp.prototype.show = function () {
        this.blackCover.parent = this.node.parent;
        this.blackCover.width = 4000;
        this.blackCover.height = 4000;
        this.blackCover.setPosition(new Vec3(0, 0, 0));
        this.blackCover.active = true;
        this.blackCover.setSiblingIndex(this.node.parent.childrenCount - 2);
        if (!this.validTemplate) {
            this.setUpTemplate();
            if (!this.validTemplate) {
                return;
            }
        }
        this.isShow = true;
        this._dropDown = this.createDropDownList(this.template);
        this._dropDown.name = "DropDownList";
        this._dropDown.active = true;
        this._dropDown.setParent(this.template.parent);
        this._dropDown.setSiblingIndex(0);
        this.extraShow();
        var itemTemplate = this._dropDown.getComponentInChildren(SicboBaseDropDownItemComp_1.default);
        var content = itemTemplate.node.parent;
        itemTemplate.node.active = true;
        cc.js.clear(this.items);
        for (var i = 0, len = this.optionDatas.length; i < len; i++) {
            var data = this.optionDatas[i];
            var item = this.addItem(data, i == this.selectedIndex, itemTemplate, this.items);
            if (!item) {
                continue;
            }
            item.toggle.isChecked = i == this.selectedIndex;
            item.toggle.node.on("toggle", this.onSelectedItem, this);
            // if(i == this.selectedIndex){
            //     this.onSelectedItem(item.toggle);
            // }
        }
        itemTemplate.node.active = false;
        content.height = itemTemplate.node.height * this.optionDatas.length;
    };
    SicboBaseDropDownComp.prototype.addItem = function (data, selected, itemTemplate, DropDownItemComps) {
        var item = this.createItem(itemTemplate);
        item.node.setParent(itemTemplate.node.parent);
        item.node.active = true;
        item.node.name = "item_" + (this.items.length + data.optionString ? data.optionString : "");
        if (item.toggle) {
            item.toggle.isChecked = false;
        }
        if (item.label) {
            item.label.string = data.optionString;
        }
        if (item.sprite) {
            item.sprite.spriteFrame = data.optionSf;
            item.sprite.enabled = data.optionSf != undefined;
        }
        this.items.push(item);
        return item;
    };
    SicboBaseDropDownComp.prototype.hide = function () {
        this.blackCover.parent = this.node;
        this.blackCover.active = false;
        this.extraHide();
        this.isShow = false;
        if (this._dropDown != undefined) {
            this.delayedDestroyDropdownList(0.15);
        }
    };
    SicboBaseDropDownComp.prototype.delayedDestroyDropdownList = function (delay) {
        return __awaiter(this, void 0, void 0, function () {
            var i, len;
            return __generator(this, function (_a) {
                // await WaitUtil.waitForSeconds(delay);
                // wait delay;
                for (i = 0, len = this.items.length; i < len; i++) {
                    if (this.items[i] != undefined)
                        this.destroyItem(this.items[i]);
                }
                cc.js.clear(this.items);
                if (this._dropDown != undefined)
                    this.destroyDropDownList(this._dropDown);
                this._dropDown = undefined;
                return [2 /*return*/];
            });
        });
    };
    SicboBaseDropDownComp.prototype.destroyItem = function (item) { };
    // 设置模板，方便后面item
    SicboBaseDropDownComp.prototype.setUpTemplate = function () {
        this.validTemplate = false;
        if (!this.template) {
            cc.error("The dropdown template is not assigned. The template needs to be assigned and must have a child GameObject with a Toggle component serving as the item");
            return;
        }
        this.template.active = true;
        var itemToggle = this.template.getComponentInChildren(cc.Toggle);
        this.validTemplate = true;
        // 一些判断
        if (!itemToggle || itemToggle.node == this.template) {
            this.validTemplate = false;
            cc.error("The dropdown template is not valid. The template must have a child Node with a Toggle component serving as the item.");
        }
        else if (this.labelItem != undefined && !this.labelItem.node.isChildOf(itemToggle.node)) {
            this.validTemplate = false;
            cc.error("The dropdown template is not valid. The Item Label must be on the item Node or children of it.");
        }
        else if (this.spriteItem != undefined && !this.spriteItem.node.isChildOf(itemToggle.node)) {
            this.validTemplate = false;
            cc.error("The dropdown template is not valid. The Item Sprite must be on the item Node or children of it.");
        }
        if (!this.validTemplate) {
            this.template.active = false;
            return;
        }
        var item = itemToggle.node.addComponent(SicboBaseDropDownItemComp_1.default);
        item.label = this.labelItem;
        item.sprite = this.spriteItem;
        item.toggle = itemToggle;
        item.node = itemToggle.node;
        this.template.active = false;
        this.validTemplate = true;
    };
    // 刷新显示的选中信息
    SicboBaseDropDownComp.prototype.refreshShownValue = function () {
        if (this.optionDatas.length <= 0) {
            return;
        }
        var data = this.optionDatas[this.clamp(this.selectedIndex, 0, this.optionDatas.length - 1)];
        if (this.labelCaption) {
            if (data && data.optionString) {
                this.labelCaption.string = data.optionString;
            }
            else {
                this.labelCaption.string = "";
            }
        }
        if (this.spriteCaption) {
            if (data && data.optionSf) {
                this.spriteCaption.spriteFrame = data.optionSf;
            }
            else {
                this.spriteCaption.spriteFrame = undefined;
            }
            this.spriteCaption.enabled = this.spriteCaption.spriteFrame != undefined;
        }
    };
    SicboBaseDropDownComp.prototype.createDropDownList = function (template) {
        return cc.instantiate(template);
    };
    SicboBaseDropDownComp.prototype.destroyDropDownList = function (dropDownList) {
        dropDownList.destroy();
    };
    SicboBaseDropDownComp.prototype.createItem = function (itemTemplate) {
        var newItem = cc.instantiate(itemTemplate.node);
        return newItem.getComponent(SicboBaseDropDownItemComp_1.default);
    };
    /** 当toggle被选中 */
    SicboBaseDropDownComp.prototype.onSelectedItem = function (toggle) {
        var _a;
        var parent = toggle.node.parent;
        for (var i = 0; i < parent.childrenCount; i++) {
            if (parent.children[i] == toggle.node) {
                // Subtract one to account for template child.
                (_a = this.optionDatas[i - 1]) === null || _a === void 0 ? void 0 : _a.clickFunc(i - 1);
                this.selectedIndex = i - 1;
                break;
            }
        }
        this.hide();
    };
    SicboBaseDropDownComp.prototype.onClick = function () {
        if (!this.isShow) {
            this.show();
        }
        else {
            this.hide();
        }
    };
    SicboBaseDropDownComp.prototype.onLoad = function () {
        if (this.loadObject == false) {
            this.view = [];
            this.load_all_object(this.node, "");
        }
        this.template = this.view["Template"];
        this.labelCaption = this.view["LabelCaption"].getComponent(cc.Label);
        this.spriteCaption = this.view["SpriteCaption"].getComponent(cc.Sprite);
        this.labelItem = this.view["Template/view/content/item/Label"].getComponent(cc.Label);
        this.spriteItem = this.view["Template/view/content/item/Sprite"].getComponent(cc.Sprite);
        this.blackCover = this.view["BlackCover"];
        this.arrow = this.view["Arrow"];
        this.popup = cc.find("UIManager/Popup");
        var blackCoverEvent = new cc.Component.EventHandler();
        blackCoverEvent.target = this.node; // This node is the node to which your event handler code component belongs
        blackCoverEvent.component = "SicboBaseDropDownComp"; // This is the code file name
        blackCoverEvent.handler = "clickBlackCover";
        this.dropDownBackGround = this.view["Background"];
        this.blackCover.getComponent(cc.Button).clickEvents.push(blackCoverEvent);
        // let dropDownBackGroundEvent = new cc.Component.EventHandler();
        // dropDownBackGroundEvent.target = this.node; // This node is the node to which your event handler code component belongs
        // dropDownBackGroundEvent.component = "BaseDropDownComp"; // This is the code file name
        // dropDownBackGroundEvent.handler = "onClick";
        // this.dropDownBackGround.addComponent(cc.Button).clickEvents.push(dropDownBackGroundEvent)
    };
    SicboBaseDropDownComp.prototype.load_all_object = function (root, path) {
        for (var i = 0; i < root.childrenCount; i++) {
            this.view[path + root.children[i].name] = root.children[i];
            this.load_all_object(root.children[i], path + root.children[i].name + "/");
        }
    };
    SicboBaseDropDownComp.prototype.start = function () {
        this.template.active = false;
        this.refreshShownValue();
    };
    SicboBaseDropDownComp.prototype.onEnable = function () {
        this.node.on("touchend", this.onClick, this);
    };
    SicboBaseDropDownComp.prototype.onDisable = function () {
        this.node.off("touchend", this.onClick, this);
    };
    SicboBaseDropDownComp.prototype.clamp = function (value, min, max) {
        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    };
    SicboBaseDropDownComp.prototype.clickBlackCover = function () {
        console.log("clickBtn");
        this.hide();
    };
    SicboBaseDropDownComp = __decorate([
        ccclass
    ], SicboBaseDropDownComp);
    return SicboBaseDropDownComp;
}(cc.Component));
exports.default = SicboBaseDropDownComp;

cc._RF.pop();