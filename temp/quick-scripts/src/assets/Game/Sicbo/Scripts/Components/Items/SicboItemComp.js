"use strict";
cc._RF.push(module, '3de95q9UaVJCpt8GjaCsJP7', 'SicboItemComp');
// Game/Sicbo_New/Scripts/Components/Items/SicboItemComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboItemComp = /** @class */ (function (_super) {
    __extends(SicboItemComp, _super);
    function SicboItemComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.iconSprite = null;
        _this.itemIconSprite = [];
        return _this;
    }
    SicboItemComp.prototype.setData = function (item, isAnimated) {
        if (isAnimated === void 0) { isAnimated = false; }
        var self = this;
        cc.tween(self.node)
            .to(isAnimated ? .5 : 0, { scale: 0 })
            .call(function () {
            var index = item;
            self.iconSprite.spriteFrame = self.itemIconSprite[index - 1];
        })
            .to(isAnimated ? 0.5 : 0, { scale: 1 })
            .start();
    };
    SicboItemComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.Sprite)
    ], SicboItemComp.prototype, "iconSprite", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], SicboItemComp.prototype, "itemIconSprite", void 0);
    SicboItemComp = __decorate([
        ccclass
    ], SicboItemComp);
    return SicboItemComp;
}(cc.Component));
exports.default = SicboItemComp;

cc._RF.pop();