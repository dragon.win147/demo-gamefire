"use strict";
cc._RF.push(module, '0f8aessYx5KyrSSK6sS50QC', 'SicboLoadingScene');
// Game/Sicbo/Scripts/Components/SicboLoadingScene.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), EventKey = _a.EventKey, AutoJoinReply = _a.AutoJoinReply;
var SicboMessageHandler_1 = require("../SicboMessageHandler");
var SicboResourceManager_1 = require("../Managers/SicboResourceManager");
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboLoadingBackgroundAnimation_1 = require("./SicboLoadingBackgroundAnimation");
var SicboGameUtils_1 = require("../RNGCommons/Utils/SicboGameUtils");
var SicboText_1 = require("../Setting/SicboText");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboLoadingScene = /** @class */ (function (_super) {
    __extends(SicboLoadingScene, _super);
    function SicboLoadingScene() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.loadingAnimCmp = null;
        _this.sprProgress = null;
        _this.loadingLabel = null;
        _this._nameScene = "SicboScene";
        _this._callBack = null;
        return _this;
    }
    SicboLoadingScene.prototype.onLoad = function () {
        var _this = this;
        cc.systemEvent.on(EventKey.ON_DISCONNECT_EVENT, function () {
            //cc.log("Loading Scene ON_RECONNECTED");
            if (_this._nameScene != "") {
                _this.setDataLoadScene(_this._nameScene, _this._callBack);
            }
        }, this);
    };
    SicboLoadingScene.prototype.start = function () {
        this.setDataLoadScene(this._nameScene, this._callBack);
    };
    SicboLoadingScene.prototype.onDestroy = function () {
        this._nameScene = "";
        this._callBack = null;
    };
    SicboLoadingScene.prototype.setLoadingText = function (percent) {
        if (this.loadingLabel == null) {
            return;
        }
        percent = Math.ceil(Math.min(Math.max(percent * 100, 0), 100));
        if (percent >= this._percent) {
            this._percent = percent;
            this.loadingLabel.string = SicboGameUtils_1.default.FormatString(SicboText_1.default.loadingMsg, percent);
        }
    };
    SicboLoadingScene.prototype.setDataLoadScene = function (nameScene, callBack) {
        var _this = this;
        if (callBack === void 0) { callBack = null; }
        var self = this;
        this._callBack = callBack;
        self.onLoadProgress(SicboSetting_1.default.loadingBackgroundOverrideProgressBar);
        self._percent = 0;
        if (this.loadingAnimCmp == null)
            onSetData(null);
        else {
            this.loadingAnimCmp.OnBeginLoadResources = function () {
                onSetData(_this.loadingAnimCmp);
            };
            self.onSetProgress(0, SicboSetting_1.default.loadingBackgroundOverrideProgressBar, this.loadingAnimCmp);
        }
        var onSetData = function (backgroundAnimation) {
            var resourceLocalPaths = SicboSetting_1.default.resourceLocalPaths;
            var resourceRemotePaths = SicboSetting_1.default.resourceRemotePaths;
            var totalResourceLocal = resourceLocalPaths.length;
            var completedResourceLocal = 0;
            var totalResourceRemote = resourceRemotePaths.length;
            var completedResourceRemote = 0;
            var totalSceneResource = 100;
            var completedScene = 0;
            var totalData = 100;
            var completedLoadData = 0;
            var onPreLoadComplete = function () {
                var finish = function () {
                    SicboMessageHandler_1.default.getInstance().sendAutoJoinRequest(function (autoJoinReply) {
                        var channel = autoJoinReply.getChannel();
                        SicboSetting_1.default.setChipAndStatusDurationConfig(channel.getChipLevels(), channel.getStatusAndDuration());
                        SicboResourceManager_1.default.loadScene(nameScene, function () {
                            if (callBack) {
                                callBack();
                            }
                        });
                    });
                };
                if (backgroundAnimation == null)
                    finish();
                else {
                    backgroundAnimation.playFinishAnim(finish);
                }
            };
            var onProgress = function (completedCount, totalCount) {
                var percent = 0;
                if (totalCount > 0) {
                    percent = (100 * completedCount) / totalCount;
                    var progress = percent / 100;
                    self.onSetProgress(progress, SicboSetting_1.default.loadingBackgroundOverrideProgressBar, _this.loadingAnimCmp);
                }
                if (percent == 100) {
                    if (onPreLoadComplete)
                        onPreLoadComplete();
                }
            };
            var onProgressLoadData = function (completedCount, totalCount) {
                //cc.log("onProgressLoadData.. ", completedCount, totalCount);
                totalData = totalCount;
                completedLoadData = completedCount;
                onProgress(completedScene + completedResourceLocal + completedResourceRemote + completedLoadData, totalData + totalSceneResource + totalResourceLocal + totalResourceRemote);
            };
            var onProgressResourceLocal = function (completedCount, totalCount) {
                totalResourceLocal = totalCount;
                completedResourceLocal = completedCount;
                onProgress(completedScene + completedResourceLocal + completedResourceRemote + completedLoadData, totalData + totalSceneResource + totalResourceLocal + totalResourceRemote);
            };
            var onProgressResourceRemote = function (completedCount, totalCount) {
                totalResourceRemote = totalCount;
                completedResourceRemote = completedCount;
                onProgress(completedScene + completedResourceLocal + completedResourceRemote + completedLoadData, totalData + totalSceneResource + totalResourceLocal + totalResourceRemote);
            };
            var onProgressScene = function (completedCount, totalCount) {
                totalSceneResource = totalCount;
                completedScene = completedCount;
                onProgress(completedScene + completedResourceLocal + completedResourceRemote + completedLoadData, totalData + totalSceneResource + totalResourceLocal + totalResourceRemote);
            };
            _this.scheduleOnce(function () {
                var onLoadDataComplete = function () {
                    SicboResourceManager_1.default.preloadScene(nameScene, onProgressScene, function () { });
                    SicboResourceManager_1.default.preloadResourceLocal(resourceLocalPaths, onProgressResourceLocal, function () { });
                    SicboResourceManager_1.default.preloadResourceRemote(resourceRemotePaths, onProgressResourceRemote, function () { });
                };
                if (SicboSetting_1.default) {
                    SicboSetting_1.default.loadDataFromForge(onProgressLoadData, onLoadDataComplete);
                }
                else {
                    onProgressLoadData(0, 0);
                    onLoadDataComplete();
                }
            }, 0.3);
        };
    };
    SicboLoadingScene.prototype.onLoadProgress = function (override) {
        if (override === void 0) { override = false; }
        if (override) {
            this.sprProgress.node.active = false;
        }
    };
    SicboLoadingScene.prototype.onSetProgress = function (progress, override, loadingAnimCmp) {
        if (override === void 0) { override = false; }
        this.setLoadingText(progress);
        if (override && loadingAnimCmp) {
            loadingAnimCmp.onSetProgress(progress);
        }
        else {
            if (this.sprProgress && progress > this.sprProgress.progress) {
                this.sprProgress.progress = progress;
            }
        }
    };
    __decorate([
        property(SicboLoadingBackgroundAnimation_1.default)
    ], SicboLoadingScene.prototype, "loadingAnimCmp", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], SicboLoadingScene.prototype, "sprProgress", void 0);
    __decorate([
        property(cc.Label)
    ], SicboLoadingScene.prototype, "loadingLabel", void 0);
    SicboLoadingScene = __decorate([
        ccclass
    ], SicboLoadingScene);
    return SicboLoadingScene;
}(cc.Component));
exports.default = SicboLoadingScene;

cc._RF.pop();