"use strict";
cc._RF.push(module, '006d0fFKAxOp7IdxGpB1jDr', 'SicboUserItemComp');
// Game/Sicbo/Scripts/Components/Items/SicboUserItemComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboUserItemCompData = void 0;
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var Playah = SicboModuleAdapter_1.default.getAllRefs().Playah;
var SicboMoneyFormatComp_1 = require("../../RNGCommons/SicboMoneyFormatComp");
var SicboPortalAdapter_1 = require("../../SicboPortalAdapter");
var SicboAvatarWinMoneyComp_1 = require("../SicboAvatarWinMoneyComp");
var SicboUIComp_1 = require("../SicboUIComp");
var SicboUserItemEffect_1 = require("../SicboUserItemEffect");
var SicboSetting_1 = require("../../Setting/SicboSetting");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboUserItemCompData = /** @class */ (function () {
    function SicboUserItemCompData() {
        this.balance = 0;
        this.chatIndex = 0;
    }
    return SicboUserItemCompData;
}());
exports.SicboUserItemCompData = SicboUserItemCompData;
var SicboUserItemComp = /** @class */ (function (_super) {
    __extends(SicboUserItemComp, _super);
    function SicboUserItemComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.iconSprite = null;
        _this.frame = null;
        _this.nickNameLabel = null;
        _this.userMoneyComp = null;
        _this.betShakeNode = null;
        _this.winMoneyEffect = null;
        _this.data = null;
        return _this;
    }
    SicboUserItemComp.prototype.onLoad = function () {
        var _a;
        this.cachedPos = (_a = this.betShakeNode) === null || _a === void 0 ? void 0 : _a.getPosition();
    };
    SicboUserItemComp.prototype.updateUI = function () {
        if (this.data.userId != "") {
            this.node.active = true;
            this.setIconUI(this.data.avatarId);
            this.setFrame(this.data.framePath);
            this.setNickNameUI(this.data.nickName);
            this.setMoneyUI(this.data.balance);
        }
    };
    SicboUserItemComp.prototype.setData = function (playah, chatIndex) {
        var _a, _b, _c, _d, _e, _f;
        if (playah === void 0) { playah = null; }
        if (chatIndex === void 0) { chatIndex = -1; }
        if (this.data == null)
            this.data = new SicboUserItemCompData();
        var userId = "";
        var basePath = "";
        var framePath = "";
        var nickName = "";
        var balance = 0;
        if (playah) {
            userId = playah.getUserId();
            basePath = SicboPortalAdapter_1.default.getInstance().onGetBasePath((_a = playah.getAvatar()) === null || _a === void 0 ? void 0 : _a.getBaseId());
            framePath = SicboPortalAdapter_1.default.getInstance().onGetFramePath((_b = playah.getAvatar()) === null || _b === void 0 ? void 0 : _b.getFrameId());
            nickName = (_c = playah.getProfile()) === null || _c === void 0 ? void 0 : _c.getDisplayName();
            balance = playah.getWallet() ? (_d = playah.getWallet()) === null || _d === void 0 ? void 0 : _d.getCurrentBalance() : 0;
        }
        this.data.rawPlayah = playah;
        this.data.userId = userId;
        this.data.avatarId = basePath;
        this.data.framePath = framePath;
        this.data.nickName = nickName;
        this.data.balance = balance;
        this.data.chatIndex = chatIndex;
        this.updateUI();
        var animDuration = SicboSetting_1.default.USER_ANIM_DURATION / 2;
        if (playah != null)
            (_e = this.node.getComponent(SicboUserItemEffect_1.default)) === null || _e === void 0 ? void 0 : _e.fadeIn(animDuration);
        else
            (_f = this.node.getComponent(SicboUserItemEffect_1.default)) === null || _f === void 0 ? void 0 : _f.fadeOut(animDuration);
    };
    SicboUserItemComp.prototype.getUserId = function () {
        var _a;
        return (_a = this.data) === null || _a === void 0 ? void 0 : _a.userId;
    };
    SicboUserItemComp.prototype.getChatIndex = function () {
        var _a;
        return (_a = this.data) === null || _a === void 0 ? void 0 : _a.chatIndex;
    };
    SicboUserItemComp.prototype.setMoney = function (money) {
        this.data.balance = money;
        this.setMoneyUI(money, true);
    };
    SicboUserItemComp.prototype.setIconUI = function (avatarId) {
        // SicboResourceManager.loadRemoteImageWithPath(this.iconSprite, avatarId);
    };
    SicboUserItemComp.prototype.setFrame = function (framePath) {
        // if (this.frame) SicboResourceManager.loadRemoteImageWithPath(this.frame, framePath);
    };
    SicboUserItemComp.prototype.setNickNameUI = function (nickName) {
        this.nickNameLabel.string = nickName;
    };
    SicboUserItemComp.prototype.setMoneyUI = function (money, isEffect) {
        if (isEffect === void 0) { isEffect = false; }
        money = Math.max(money, 0);
        if (isEffect)
            this.userMoneyComp.runMoneyTo(money);
        else
            this.userMoneyComp.setMoney(money);
    };
    SicboUserItemComp.prototype.shakeBet = function (target, distance) {
        var _this = this;
        if (distance === void 0) { distance = 15; }
        this.betShakeNode.setPosition(this.cachedPos);
        var tweenNode = this.betShakeNode;
        var point = cc.Vec2.ZERO;
        var targetPoint = target.convertToWorldSpaceAR(cc.Vec2.ZERO);
        var tweenNodePoint = tweenNode.convertToWorldSpaceAR(cc.Vec2.ZERO);
        cc.Vec2.subtract(point, targetPoint, tweenNodePoint);
        point = point.normalize().mul(distance);
        var eff = cc.tween(tweenNode).to(0.1, { x: point.x + this.cachedPos.x, y: point.y + this.cachedPos.y }); //, { easing: 'elasticInOut'}
        var effB = cc
            .tween(tweenNode)
            .to(0.1, { x: this.cachedPos.x, y: this.cachedPos.y })
            .call(function () {
            _this.betShakeNode.setPosition(_this.cachedPos);
        });
        eff.then(effB).start();
    };
    SicboUserItemComp.prototype.showWinMoney = function (amount) {
        var _a;
        (_a = this.winMoneyEffect) === null || _a === void 0 ? void 0 : _a.show(amount);
    };
    SicboUserItemComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.betShakeNode);
    };
    SicboUserItemComp.prototype.onClickAvatar = function () {
        SicboUIComp_1.default.Instance.showUserProfilePopUp(this.data.rawPlayah);
    };
    __decorate([
        property(cc.Sprite)
    ], SicboUserItemComp.prototype, "iconSprite", void 0);
    __decorate([
        property(cc.Sprite)
    ], SicboUserItemComp.prototype, "frame", void 0);
    __decorate([
        property(cc.Label)
    ], SicboUserItemComp.prototype, "nickNameLabel", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboUserItemComp.prototype, "userMoneyComp", void 0);
    __decorate([
        property(cc.Node)
    ], SicboUserItemComp.prototype, "betShakeNode", void 0);
    __decorate([
        property(SicboAvatarWinMoneyComp_1.default)
    ], SicboUserItemComp.prototype, "winMoneyEffect", void 0);
    SicboUserItemComp = __decorate([
        ccclass
    ], SicboUserItemComp);
    return SicboUserItemComp;
}(cc.Component));
exports.default = SicboUserItemComp;

cc._RF.pop();