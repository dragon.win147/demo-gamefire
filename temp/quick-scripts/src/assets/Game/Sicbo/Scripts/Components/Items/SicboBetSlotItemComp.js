"use strict";
cc._RF.push(module, '4b3e3sv9j1M85/v8+NJ1Mth', 'SicboBetSlotItemComp');
// Game/Sicbo_New/Scripts/Components/Items/SicboBetSlotItemComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var Door = SicboModuleAdapter_1.default.getAllRefs().Door;
var SicboSetting_1 = require("../../Setting/SicboSetting");
var SicboHelper_1 = require("../../Helpers/SicboHelper");
var SicboMoneyFormatComp_1 = require("../../RNGCommons/SicboMoneyFormatComp");
var SicboBetComp_1 = require("../SicboBetComp");
var SicboCoinItemComp_1 = require("./SicboCoinItemComp");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboBetSlotItemComp = /** @class */ (function (_super) {
    __extends(SicboBetSlotItemComp, _super);
    function SicboBetSlotItemComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.door = Door.ONE_SINGLE;
        _this.sicboBetComp = null;
        _this.myBetBgNode = null;
        _this.myBetMoneyComp = null;
        _this.totalBetBgNode = null;
        _this.totalBetMoneyComp = null;
        _this.miniChipArea = null;
        _this.stackChipRoot = null;
        _this.maxMiniChipOnSlot = 12 + 10 + 5;
        _this.chipColCount = 0;
        _this.lastMyBetAmount = 0;
        _this.lastTotalBetAmount = 0;
        _this._miniChipsOnSlot = [];
        return _this;
    }
    Object.defineProperty(SicboBetSlotItemComp.prototype, "miniChipsOnSlot", {
        get: function () {
            //FIXME kiem tra lai loi bi sai _miniChipsOnSlot
            this._miniChipsOnSlot = this.miniChipArea.getComponentsInChildren(SicboCoinItemComp_1.default);
            return this._miniChipsOnSlot;
        },
        enumerable: false,
        configurable: true
    });
    // public set miniChipsOnSlot(value: SicboCoinItemComp[]) {
    //     this._miniChipsOnSlot = value;
    // }
    SicboBetSlotItemComp.prototype.addMiniChipToSlot = function (chip) {
        this._miniChipsOnSlot.push(chip);
    };
    SicboBetSlotItemComp.prototype.getRedundancyChip = function (isStaticSlot, mergeFrom) {
        var _a;
        if (isStaticSlot) {
            var totalValue = 0;
            var chips_1 = this.miniChipsOnSlot;
            for (var i = mergeFrom; i < chips_1.length; i++) {
                totalValue += SicboHelper_1.default.convertCoinTypeToValue((_a = chips_1[i].getComponent(SicboCoinItemComp_1.default)) === null || _a === void 0 ? void 0 : _a.data.coinType);
            }
            var newChips = SicboHelper_1.default.calculateNumOfChipToMakeValue(totalValue, SicboSetting_1.default.getChipLevelList());
            newChips.forEach(function (num, val) {
                // //console.log("val: " + val + " -> " + num);
                var coinType = SicboHelper_1.default.convertValueToCoinType(val);
                for (var i = 0; i < num; i++) {
                    var itemData = new SicboCoinItemComp_1.SicboCoinItemCompData();
                    itemData.coinType = coinType;
                    itemData.isMini = true;
                    chips_1[mergeFrom].getComponent(SicboCoinItemComp_1.default).setData(itemData);
                    mergeFrom++;
                }
            });
            return this._miniChipsOnSlot.splice(mergeFrom + newChips.values.length);
        }
        else {
            return this._miniChipsOnSlot.splice(0, this._miniChipsOnSlot.length - this.maxMiniChipOnSlot);
        }
    };
    SicboBetSlotItemComp.prototype.clearMiniChipOnSlot = function () {
        this._miniChipsOnSlot.length = 0;
    };
    SicboBetSlotItemComp.prototype.setStackChipRoot = function (stackChipRoot) {
        this.stackChipRoot = stackChipRoot;
    };
    SicboBetSlotItemComp.prototype.deleteStackChipRoot = function () {
        if (this.stackChipRoot != null)
            this.stackChipRoot.destroy();
        this.stackChipRoot = null;
    };
    Object.defineProperty(SicboBetSlotItemComp.prototype, "miniChipsOnStack", {
        get: function () {
            if (this.stackChipRoot != null)
                return this.stackChipRoot.getComponentsInChildren(SicboCoinItemComp_1.default);
            return [];
        },
        enumerable: false,
        configurable: true
    });
    SicboBetSlotItemComp.prototype.onLoad = function () {
        this.registerClickEvent();
        this.resetSlot();
    };
    SicboBetSlotItemComp.prototype.resetSlot = function () {
        this.setTotalBet(0);
        this.setMyTotalBet(0);
    };
    SicboBetSlotItemComp.prototype.setMyTotalBet = function (value) {
        if (value > 0) {
            this.myBetBgNode.active = true;
            this.myBetBgNode.width = this.myBetMoneyComp.node.width + 20 * .8; //NOTE layout not work 
            this.myBetMoneyComp.node.active = true;
            this.myBetMoneyComp.setMoney(value);
            if (this.lastMyBetAmount != value) {
                this.playBetAmountEffect(this.myBetMoneyComp.node);
            }
        }
        else {
            this.myBetBgNode.active = false;
            this.myBetMoneyComp.node.active = false;
        }
        this.lastMyBetAmount = value;
    };
    SicboBetSlotItemComp.prototype.setTotalBet = function (value) {
        if (value > 0) {
            this.totalBetMoneyComp.node.active = true;
            this.totalBetBgNode.active = true;
            this.totalBetMoneyComp.setMoney(value);
            if (this.lastTotalBetAmount != value) {
                this.playBetAmountEffect(this.totalBetMoneyComp.node);
            }
        }
        else {
            this.totalBetBgNode.active = false;
            this.totalBetMoneyComp.node.active = false;
        }
        this.lastTotalBetAmount = value;
    };
    SicboBetSlotItemComp.prototype.playBetAmountEffect = function (node) {
        node.scale = 2;
        node.opacity = 0;
        cc.tween(node)
            .to(.2, {
            scale: 1,
            opacity: 255
        })
            .start();
    };
    SicboBetSlotItemComp.prototype.registerClickEvent = function () {
        var button = this.getComponent(cc.Button);
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        clickEventHandler.component = "SicboBetSlotItemComp"; // This is the code file name
        clickEventHandler.handler = "onClickBetSlotItem";
        button.clickEvents.push(clickEventHandler);
    };
    SicboBetSlotItemComp.prototype.onClickBetSlotItem = function () {
        this.sicboBetComp.onClickBetSlotItem(this);
    };
    SicboBetSlotItemComp.prototype.onDestroy = function () {
        if (this.stackChipRoot != null)
            cc.Tween.stopAllByTarget(this.stackChipRoot);
        if (this.myBetMoneyComp.node != null)
            cc.Tween.stopAllByTarget(this.myBetMoneyComp.node);
        if (this.totalBetMoneyComp.node != null)
            cc.Tween.stopAllByTarget(this.totalBetMoneyComp.node);
    };
    __decorate([
        property({ type: cc.Enum(Door) })
    ], SicboBetSlotItemComp.prototype, "door", void 0);
    __decorate([
        property(SicboBetComp_1.default)
    ], SicboBetSlotItemComp.prototype, "sicboBetComp", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetSlotItemComp.prototype, "myBetBgNode", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboBetSlotItemComp.prototype, "myBetMoneyComp", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetSlotItemComp.prototype, "totalBetBgNode", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboBetSlotItemComp.prototype, "totalBetMoneyComp", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetSlotItemComp.prototype, "miniChipArea", void 0);
    SicboBetSlotItemComp = __decorate([
        ccclass
    ], SicboBetSlotItemComp);
    return SicboBetSlotItemComp;
}(cc.Component));
exports.default = SicboBetSlotItemComp;

cc._RF.pop();