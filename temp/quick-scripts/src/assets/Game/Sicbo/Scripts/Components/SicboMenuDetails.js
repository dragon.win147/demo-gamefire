"use strict";
cc._RF.push(module, 'd485akhFDxBa4C0XZwtUD/c', 'SicboMenuDetails');
// Game/Sicbo_New/Scripts/Components/SicboMenuDetails.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), State = _a.State, Status = _a.Status;
var SicboMessageHandler_1 = require("../SicboMessageHandler");
var SicboController_1 = require("../Controllers/SicboController");
var SicboUIManager_1 = require("../Managers/SicboUIManager");
var SicboInforPopup_1 = require("../PopUps/SicboInforPopup");
var SicboSoundManager_1 = require("../Managers/SicboSoundManager");
var SicboUIComp_1 = require("./SicboUIComp");
var SicboText_1 = require("../Setting/SicboText");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboMenuDetails = /** @class */ (function (_super) {
    __extends(SicboMenuDetails, _super);
    function SicboMenuDetails() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //MENU DETAIL
        _this.isMenuDetailShowed = false;
        _this.lockMenuDetail = false;
        _this.menuDetailRoot = null;
        _this.menuDetail = null;
        _this.bgSoundInactiveNode = null;
        _this.effectSoundInactiveNode = null;
        _this.outsideClickNode = null;
        _this.leaveBookingActiveNode = null;
        _this.leaveBookingDeactiveNode = null;
        _this.totalBet = 0;
        _this.curSessionId = 0;
        _this.isBookingLeave = false;
        _this.bookingLeaveSessionId = 0;
        return _this;
    }
    SicboMenuDetails.prototype.start = function () {
        this.loadMenuBackgroundSoundStatus();
        this.loadMenuSoundFxStatus();
    };
    SicboMenuDetails.prototype.onLeave = function () {
        this.onClickShowMenuDetail();
        SicboController_1.default.Instance.sendCheckServerAvailable();
        if (this.isBookingLeave) {
            this.setLeaveBookingStatus(false, this.curSessionId);
            return;
        }
        if (this.totalBet > 0) {
            this.setLeaveBookingStatus(true, this.curSessionId);
            return;
        }
        this.leave();
    };
    SicboMenuDetails.prototype.leave = function () {
        SicboMessageHandler_1.default.getInstance().sendLeaveRequest(function () {
            var _a;
            (_a = SicboController_1.default.Instance) === null || _a === void 0 ? void 0 : _a.leaveGame();
        });
    };
    SicboMenuDetails.prototype.setLeaveBookingStatus = function (isActive, leaveSession) {
        this.bookingLeaveSessionId = leaveSession;
        this.isBookingLeave = isActive;
        this.leaveBookingActiveNode.active = isActive;
        this.leaveBookingDeactiveNode.active = !isActive;
        var msg = isActive ? SicboText_1.default.leaveBookingActiveMsg : SicboText_1.default.leaveBookingDeactiveMsg;
        SicboUIComp_1.default.Instance.showNotifyMessage(msg);
    };
    SicboMenuDetails.prototype.exitState = function (status) {
        // if(this.isBookingLeave && this.totalBet> 0)
        //   this.leave();
    };
    SicboMenuDetails.prototype.startState = function (state, totalDuration) {
        this.curSessionId = state.getTableSessionId();
        if (!this.isBookingLeave)
            return;
        if (this.curSessionId != this.bookingLeaveSessionId) {
            this.leave();
            return;
        }
        if (state.getStatus() == Status.WAITING) {
            this.leave();
        }
    };
    SicboMenuDetails.prototype.updateState = function (state) {
        this.totalBet = state.getSessionPlayerBettedAmount();
    };
    SicboMenuDetails.prototype.onHide = function () { };
    SicboMenuDetails.prototype.onClickButtonRule = function () {
        SicboUIManager_1.default.getInstance().openPopup(SicboInforPopup_1.default, SicboUIManager_1.default.SicboInforPopup);
        this.onClickShowMenuDetail();
    };
    SicboMenuDetails.prototype.onClickShowMenuDetail = function () {
        var _this = this;
        if (this.lockMenuDetail)
            return;
        this.lockMenuDetail = true;
        this.isMenuDetailShowed = !this.isMenuDetailShowed;
        if (this.isMenuDetailShowed == true) {
            //tween out
            this.menuDetailRoot.active = this.isMenuDetailShowed;
            this.outsideClickNode.active = this.isMenuDetailShowed;
            cc.tween(this.menuDetail)
                .to(0.3, { position: new cc.Vec3(0, 0, 0) })
                .call(function () {
                _this.lockMenuDetail = false;
            })
                .start();
        }
        else {
            //tween in
            cc.tween(this.menuDetail)
                .to(0.1, { position: new cc.Vec3(10, 0, 0) })
                .to(0.5, { position: new cc.Vec3(-720, 0, 0) })
                .call(function () {
                _this.menuDetailRoot.active = _this.isMenuDetailShowed;
                _this.outsideClickNode.active = _this.isMenuDetailShowed;
                _this.lockMenuDetail = false;
            })
                .start();
        }
    };
    SicboMenuDetails.prototype.loadMenuBackgroundSoundStatus = function () {
        var vol = SicboSoundManager_1.default.getInstance().getMusicVolumeFromStorage();
        this.bgSoundInactiveNode.active = vol == 0;
    };
    SicboMenuDetails.prototype.loadMenuSoundFxStatus = function () {
        var vol = SicboSoundManager_1.default.getInstance().getSfxVolumeFromStorage();
        this.effectSoundInactiveNode.active = vol == 0;
    };
    SicboMenuDetails.prototype.onClickMenuBackgroundSound = function () {
        var vol = SicboSoundManager_1.default.getInstance().getMusicVolumeFromStorage();
        if (vol > 0) {
            vol = 0;
        }
        else {
            vol = 1;
        }
        SicboSoundManager_1.default.getInstance().setMusicVolume(vol);
        this.bgSoundInactiveNode.active = vol == 0;
    };
    SicboMenuDetails.prototype.onClickMenuSoundFx = function () {
        var vol = SicboSoundManager_1.default.getInstance().getSfxVolumeFromStorage();
        if (vol > 0) {
            vol = 0;
        }
        else {
            vol = 1;
        }
        SicboSoundManager_1.default.getInstance().setSfxVolume(vol);
        this.effectSoundInactiveNode.active = vol == 0;
    };
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "menuDetailRoot", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "menuDetail", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "bgSoundInactiveNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "effectSoundInactiveNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "outsideClickNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "leaveBookingActiveNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "leaveBookingDeactiveNode", void 0);
    SicboMenuDetails = __decorate([
        ccclass
    ], SicboMenuDetails);
    return SicboMenuDetails;
}(cc.Component));
exports.default = SicboMenuDetails;

cc._RF.pop();