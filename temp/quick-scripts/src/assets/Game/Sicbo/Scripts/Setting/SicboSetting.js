"use strict";
cc._RF.push(module, '59f0d6hAmZPSZ8S1RAl3kdw', 'SicboSetting');
// Game/Sicbo_New/Scripts/Setting/SicboSetting.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboSound = exports.SicboScene = void 0;
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Code = _a.Code, Status = _a.Status, ChipLevels = _a.ChipLevels, StageTime = _a.StageTime;
var SicboScene;
(function (SicboScene) {
    SicboScene["Loading"] = "SicboLoadingScene";
    SicboScene["Main"] = "SicboScene";
})(SicboScene = exports.SicboScene || (exports.SicboScene = {}));
var SicboSetting = /** @class */ (function () {
    function SicboSetting() {
    }
    SicboSetting.loadDataFromForge = function (onProgress, onComplete) {
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (onProgress)
            onProgress(0, 0);
        if (onComplete)
            onComplete();
    };
    Object.defineProperty(SicboSetting, "chipLevels", {
        get: function () {
            return SicboSetting._chipLevels;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboSetting, "statusAndDuration", {
        get: function () {
            return SicboSetting._statusAndDuration;
        },
        enumerable: false,
        configurable: true
    });
    SicboSetting.getStatusDuration = function (status) {
        switch (status) {
            case Status.WAITING:
                return SicboSetting._statusAndDuration.getWaiting();
            case Status.BETTING:
                return SicboSetting._statusAndDuration.getBetting();
            case Status.END_BET:
                return SicboSetting._statusAndDuration.getEndBet();
            case Status.RESULTING:
                return SicboSetting._statusAndDuration.getResulting();
            case Status.PAYING:
                return SicboSetting._statusAndDuration.getPaying();
            case Status.FINISHING:
                return SicboSetting._statusAndDuration.getFinishing();
            default:
                return 0;
        }
    };
    SicboSetting.getChipLevelList = function () {
        return SicboSetting._chipLevels.getChipLevelList();
    };
    SicboSetting.setChipAndStatusDurationConfig = function (chipLevels, statusAndDuration) {
        SicboSetting._chipLevels = chipLevels;
        SicboSetting._statusAndDuration = statusAndDuration;
    };
    SicboSetting.setStatusDurationConfig = function (statusAndDuration) {
        SicboSetting._statusAndDuration = statusAndDuration;
    };
    SicboSetting.getSoundName = function (sound) {
        return SicboSound[sound];
        // switch (sound) {
        //   case SicboSound.BGM: return "bgm-main_game1";
        //   case SicboSound.SfxDealerShake: return "sfx-dice_shake_long";
        //   case SicboSound.SfxClick: return "SFX_ButtonTap";
        //   case SicboSound.SfxBet: return "sfx-bet";
        //   case SicboSound.SfxRush: return "sfx-rush";
        //   case SicboSound.SfxTictoc: return "sfx_tictoc";
        //   case SicboSound.SfxStopBetting: return "sfx-stop_betting1";
        //   case SicboSound.SfxResult: return "sfx-result";
        //   case SicboSound.SfxJackpotScale: return "sfx_jackpot_scale";
        //   case SicboSound.SfxJackpotFinal: return "sfx_jackpot_final";
        //   case SicboSound.SfxJackpotCoin: return "sfx-jackpot_coin1";
        //   case SicboSound.SfxWin: return "sfx-win";
        //   case SicboSound.SfxTotalWin: return "sfx-total_win2";
        //   case SicboSound.SfxMessage: return "sfx-message2";
        //   case SicboSound.SfxChipFly: return "sfx-bet";
        // }
    };
    SicboSetting.getSoundDuration = function (sound) {
        switch (sound) {
            // case SicboSound.BGM: return "bgm-main_game1";
            // case SicboSound.SfxDealerShake: return "sfx-dice_shake_long";
            // case SicboSound.SfxClick: return "sfx-click2";
            // case SicboSound.SfxBet: return "sfx-bet";
            // case SicboSound.SfxRush: return "sfx-rush";
            // case SicboSound.SfxTictoc: return "sfx-tictoc2";
            // case SicboSound.SfxStopBetting: return "sfx-stop_betting1";
            // case SicboSound.SfxResult: return "sfx-result";
            // case SicboSound.SfxJackpotScale: return "sfx-jackpot_scale1";
            // case SicboSound.SfxJackpotFinal: return "sfx-jackpot_final1";
            // case SicboSound.SfxJackpotCoin: return "sfx-jackpot_coin1";
            // case SicboSound.SfxWin: return "sfx-win";
            // case SicboSound.SfxTotalWin: return "sfx-total_win2";
            // case SicboSound.SfxMessage: return "sfx-message";
            case SicboSound.SfxChipFly:
                return 0.134;
            default:
                //cc.log("BCF WARNING: NOT HAVE DURATION SETTING")
                return 0;
        }
    };
    SicboSetting.USER_ANIM_DURATION = 0.77;
    SicboSetting.resourceLocalPaths = [];
    SicboSetting.resourceRemotePaths = [];
    SicboSetting.loadingBackgroundLocalPath = "";
    SicboSetting.loadingBackgroundOverrideProgressBar = false;
    SicboSetting._chipLevels = null;
    SicboSetting._statusAndDuration = null;
    return SicboSetting;
}());
exports.default = SicboSetting;
var SicboSound;
(function (SicboSound) {
    SicboSound[SicboSound["BGM"] = 0] = "BGM";
    SicboSound[SicboSound["SfxDealerShake"] = 1] = "SfxDealerShake";
    SicboSound[SicboSound["SfxClick"] = 2] = "SfxClick";
    SicboSound[SicboSound["SfxBet"] = 3] = "SfxBet";
    SicboSound[SicboSound["SfxRush"] = 4] = "SfxRush";
    SicboSound[SicboSound["SfxTictoc"] = 5] = "SfxTictoc";
    SicboSound[SicboSound["SfxStopBetting"] = 6] = "SfxStopBetting";
    SicboSound[SicboSound["SfxResult"] = 7] = "SfxResult";
    SicboSound[SicboSound["SfxJackpotScale"] = 8] = "SfxJackpotScale";
    SicboSound[SicboSound["SfxJackpotFinal"] = 9] = "SfxJackpotFinal";
    SicboSound[SicboSound["SfxJackpotCoin"] = 10] = "SfxJackpotCoin";
    SicboSound[SicboSound["SfxWin"] = 11] = "SfxWin";
    SicboSound[SicboSound["SfxTotalWin"] = 12] = "SfxTotalWin";
    SicboSound[SicboSound["SfxMessage"] = 13] = "SfxMessage";
    SicboSound[SicboSound["SfxChipFly"] = 14] = "SfxChipFly";
})(SicboSound = exports.SicboSound || (exports.SicboSound = {}));

cc._RF.pop();