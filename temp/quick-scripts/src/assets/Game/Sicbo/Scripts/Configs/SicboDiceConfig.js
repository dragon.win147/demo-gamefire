"use strict";
cc._RF.push(module, 'e61d21/C2tHo40Kx1X8zgZT', 'SicboDiceConfig');
// Game/Sicbo_New/Scripts/Configs/SicboDiceConfig.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboDiceConfig = /** @class */ (function (_super) {
    __extends(SicboDiceConfig, _super);
    function SicboDiceConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.diceSprites = [];
        return _this;
    }
    SicboDiceConfig_1 = SicboDiceConfig;
    Object.defineProperty(SicboDiceConfig, "Instance", {
        get: function () {
            return SicboDiceConfig_1.instance;
        },
        enumerable: false,
        configurable: true
    });
    SicboDiceConfig.prototype.onLoad = function () {
        SicboDiceConfig_1.instance = this;
    };
    SicboDiceConfig.prototype.onDestroy = function () {
        delete SicboDiceConfig_1.instance;
        SicboDiceConfig_1.instance = null;
    };
    SicboDiceConfig.prototype.getDice = function (diceName) {
        var rs = null;
        SicboDiceConfig_1.Instance.diceSprites.forEach(function (element) {
            if (element.name == diceName) {
                rs = element;
            }
        });
        return rs;
    };
    var SicboDiceConfig_1;
    SicboDiceConfig.instance = null;
    __decorate([
        property([cc.SpriteFrame])
    ], SicboDiceConfig.prototype, "diceSprites", void 0);
    SicboDiceConfig = SicboDiceConfig_1 = __decorate([
        ccclass
    ], SicboDiceConfig);
    return SicboDiceConfig;
}(cc.Component));
exports.default = SicboDiceConfig;

cc._RF.pop();