"use strict";
cc._RF.push(module, '59266c742JCBY4DW0XLZp9n', 'SicboMoneyFormatComp');
// Game/Sicbo_New/Scripts/RNGCommons/SicboMoneyFormatComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboMoneyFormatType = void 0;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboMoneyFormatType;
(function (SicboMoneyFormatType) {
    SicboMoneyFormatType[SicboMoneyFormatType["None"] = 0] = "None";
    SicboMoneyFormatType[SicboMoneyFormatType["Common"] = 1] = "Common";
    SicboMoneyFormatType[SicboMoneyFormatType["Number"] = 2] = "Number";
    SicboMoneyFormatType[SicboMoneyFormatType["Config"] = 3] = "Config";
    SicboMoneyFormatType[SicboMoneyFormatType["Dot"] = 4] = "Dot";
})(SicboMoneyFormatType = exports.SicboMoneyFormatType || (exports.SicboMoneyFormatType = {}));
var SicboMoneyFormatComp = /** @class */ (function (_super) {
    __extends(SicboMoneyFormatComp, _super);
    function SicboMoneyFormatComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.formatLabel = null;
        _this.formatType = SicboMoneyFormatType.Number;
        _this.prefix = "";
        _this._moneyValue = 0;
        return _this;
    }
    Object.defineProperty(SicboMoneyFormatComp.prototype, "moneyValue", {
        get: function () {
            return this._moneyValue;
        },
        set: function (value) {
            this._moneyValue = value;
            this.doFormat();
        },
        enumerable: false,
        configurable: true
    });
    SicboMoneyFormatComp.prototype.onLoad = function () {
        this.formatLabel = this.getComponent(cc.Label);
        this.doFormat();
    };
    SicboMoneyFormatComp.prototype.onDestroy = function () {
        if (this.t)
            this.t.stop();
    };
    SicboMoneyFormatComp.prototype.setMoney = function (money, isPrefix) {
        if (isPrefix === void 0) { isPrefix = false; }
        if (isPrefix) {
            this.prefix = money > 0 ? "+" : "";
        }
        if (this.t)
            this.t.stop();
        this.moneyValue = money;
    };
    SicboMoneyFormatComp.prototype.setColor = function (color) {
        this.node.color = color;
    };
    SicboMoneyFormatComp.prototype.runMoneyTo = function (money, duration) {
        var _this = this;
        if (duration === void 0) { duration = 0.5; }
        if (money == this.moneyValue)
            return;
        var obj = { current: this.moneyValue };
        if (this.t)
            this.t.stop();
        this.t = cc
            .tween(obj)
            .to(duration, { current: money }, {
            progress: function (start, end, current, ratio) {
                _this.moneyValue = Math.floor(current);
                return start + (end - start) * ratio;
            },
        })
            .call(function (current) { return (_this.moneyValue = money); })
            .start();
    };
    SicboMoneyFormatComp.prototype.doFormat = function () {
        if (this.formatLabel) {
            var formattedString = "";
            var moneyFormat = Math.abs(this.moneyValue);
            if (this.formatType === SicboMoneyFormatType.None) {
                formattedString = this.moneyValue.toString();
            }
            else if (this.formatType === SicboMoneyFormatType.Config) {
                //  formattedString = SicboGameUtils.formatByConfigConvert(moneyFormat);
            }
            else if (this.formatType === SicboMoneyFormatType.Number) {
                formattedString = SicboGameUtils_1.default.formatMoneyNumber(moneyFormat);
            }
            else if (this.formatType === SicboMoneyFormatType.Common) {
                formattedString = SicboGameUtils_1.default.numberWithCommas(moneyFormat);
            }
            else if (this.formatType === SicboMoneyFormatType.Dot) {
                formattedString = SicboGameUtils_1.default.numberWithDot(moneyFormat);
            }
            if (this.moneyValue < 0)
                formattedString = "-" + formattedString;
            this.formatLabel.string = this.prefix + formattedString; // this.moneyValue;
        }
    };
    __decorate([
        property({ type: cc.Enum(SicboMoneyFormatType) })
    ], SicboMoneyFormatComp.prototype, "formatType", void 0);
    __decorate([
        property
    ], SicboMoneyFormatComp.prototype, "prefix", void 0);
    SicboMoneyFormatComp = __decorate([
        ccclass
    ], SicboMoneyFormatComp);
    return SicboMoneyFormatComp;
}(cc.Component));
exports.default = SicboMoneyFormatComp;
var SicboGameUtils_1 = require("./Utils/SicboGameUtils");

cc._RF.pop();