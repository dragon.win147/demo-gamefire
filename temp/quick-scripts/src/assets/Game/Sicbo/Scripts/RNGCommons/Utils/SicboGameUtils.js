"use strict";
cc._RF.push(module, '1f0d3UXOC1Aw5uyi75k9JZH', 'SicboGameUtils');
// Game/Sicbo_New/Scripts/RNGCommons/Utils/SicboGameUtils.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SicboNative_1 = require("../../SicboNative");
var SicboPortalAdapter_1 = require("../../SicboPortalAdapter");
var SicboGameUtils = /** @class */ (function () {
    function SicboGameUtils() {
    }
    SicboGameUtils.createClickBtnEvent = function (node, componentName, handlerEvent, custom) {
        if (custom === void 0) { custom = ""; }
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = node; //This node is the node to which your event handler code component belongs
        clickEventHandler.component = componentName; //This is the code file name
        clickEventHandler.handler = handlerEvent;
        clickEventHandler.customEventData = custom;
        return clickEventHandler;
    };
    SicboGameUtils.convertToOtherNode = function (fromNode, targetNode, pos) {
        if (pos === void 0) { pos = null; }
        var parent = fromNode;
        if (fromNode.parent)
            parent = fromNode.parent;
        var worldSpace = parent.convertToWorldSpaceAR(fromNode.position);
        if (pos) {
            worldSpace = fromNode.convertToWorldSpaceAR(pos);
        }
        return targetNode.convertToNodeSpaceAR(worldSpace);
    };
    SicboGameUtils.convertToOtherNode2 = function (fromNode, targetNode, pos) {
        if (pos === void 0) { pos = null; }
        var parent = fromNode;
        if (fromNode.parent)
            parent = fromNode.parent;
        var worldSpace = parent.convertToWorldSpaceAR(fromNode.getPosition());
        if (pos) {
            worldSpace = fromNode.convertToWorldSpaceAR(pos);
        }
        return targetNode.convertToNodeSpaceAR(worldSpace);
    };
    SicboGameUtils.rectContainsPoint = function (rect, point) {
        var result = point.x >= rect.origin.x && point.x <= rect.origin.x + rect.size.width && point.y >= rect.origin.y && point.y <= rect.origin.y + rect.size.height;
        return result;
    };
    SicboGameUtils.createItemFromNode = function (item, node, parentNode, insert) {
        if (parentNode === void 0) { parentNode = null; }
        if (insert === void 0) { insert = false; }
        if (!node)
            return null;
        var parent = parentNode ? parentNode : node.parent;
        var newNode = cc.instantiate(node);
        if (insert) {
            parent.insertChild(newNode, 0);
        }
        else {
            parent.addChild(newNode);
        }
        var newItem = newNode.getComponent(item);
        newItem.node.active = true;
        node.active = false;
        return newItem;
    };
    SicboGameUtils.cloneItemFromNode = function (item, node) {
        if (!node)
            return null;
        var newNode = cc.instantiate(node);
        var newItem = newNode.getComponent(item);
        newItem.node.active = true;
        node.active = false;
        return newItem;
    };
    SicboGameUtils.createItemFromPrefab = function (item, prefab, parentNode) {
        if (!prefab)
            return null;
        var node = cc.instantiate(prefab);
        parentNode.addChild(node);
        var newItem = node.getComponent(item);
        newItem.node.active = true;
        return newItem;
    };
    SicboGameUtils.createNodeFromPrefab = function (prefab, parentNode) {
        if (!prefab)
            return null;
        var node = cc.instantiate(prefab);
        parentNode.addChild(node);
        return node;
    };
    SicboGameUtils.formatDate = function (date, format) {
        format = format.replace("%Y", date.getFullYear().toString());
        format = format.replace("%m", date.getMonth().toString().length < 2 ? "0" + date.getMonth().toString() : date.getMonth().toString());
        format = format.replace("%d", date.getDay().toString().length < 2 ? "0" + date.getDay().toString() : date.getDay().toString());
        format = format.replace("%h", date.getHours().toString().length < 2 ? "0" + date.getHours().toString() : date.getHours().toString());
        format = format.replace("%m", date.getMinutes().toString().length < 2 ? "0" + date.getMinutes().toString() : date.getMinutes().toString());
        format = format.replace("%s", date.getSeconds().toString().length < 2 ? "0" + date.getSeconds().toString() : date.getSeconds().toString());
        return format;
    };
    SicboGameUtils.formatMoneyNumberUser = function (money) {
        var s = "$ " + this.formatMoneyNumber(money);
        return s;
    };
    SicboGameUtils.formatMoneyNumber = function (money) {
        var sign = 1;
        var value = money;
        if (money < 0) {
            sign = -1;
            value = value * -1;
        }
        var format = "";
        if (value >= 1000000000.0) {
            value /= 1000000000.0;
            format = "B";
        }
        else if (value >= 1000000.0) {
            value /= 1000000.0;
            format = "M";
        }
        else if (value >= 1000.0) {
            value /= 1000.0;
            format = "K";
        }
        value = (Math.floor(value * 100 + 0.00000001) / 100) * sign;
        return value + format;
    };
    SicboGameUtils.numberWithCommasMoney = function (number) {
        var s = "$ " + this.numberWithCommas(number);
        return s;
    };
    SicboGameUtils.numberWithCommas = function (number) {
        if (number) {
            var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
            result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return result[1] !== "00" ? result.join(",") : result[0];
        }
        return "0";
    };
    SicboGameUtils.numberWithCommasV2 = function (number) {
        if (number) {
            var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
            result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return result[1] !== "00" ? result.join(",") : result[0];
        }
        return "0";
    };
    SicboGameUtils.numberWithDot = function (number) {
        if (number) {
            var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
            result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return result[1] !== "00" ? result.join(",") : result[0];
        }
        return "0";
    };
    SicboGameUtils.formatMoneyWithRange = function (money, shortenStart) {
        if (shortenStart === void 0) { shortenStart = 100000000; }
        var rs = "";
        if (money >= shortenStart)
            rs = SicboGameUtils.formatMoneyNumber(money);
        else
            rs = SicboGameUtils.numberWithCommas(money);
        return rs;
    };
    SicboGameUtils.roundNumber = function (num, roundMin) {
        if (roundMin === void 0) { roundMin = 1000; }
        var temp = parseInt((num / roundMin).toString());
        if (num % roundMin != 0)
            temp += 1;
        return temp * roundMin;
    };
    SicboGameUtils.getRandomInt = function (max, min) {
        if (min === void 0) { min = 0; }
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };
    SicboGameUtils.randomInsideCircle = function (R) {
        if (R === void 0) { R = 1; }
        var r = R * Math.sqrt(Math.random());
        var theta = Math.random() * 2 * Math.PI;
        var x = r * Math.cos(theta);
        var y = r * Math.sin(theta);
        return new cc.Vec2(x, y);
    };
    SicboGameUtils.playAnimOnce = function (skeleton, toAnimation, backAnimation, onCompleted) {
        if (onCompleted === void 0) { onCompleted = null; }
        if (skeleton == null || skeleton == undefined)
            return;
        // if (skeleton.getCurrent(0) != null && skeleton.getCurrent(0).animation.name == toAnimation) {
        //   return;
        // }
        skeleton.setCompleteListener(function (trackEntry) {
            if (backAnimation != "" && backAnimation != null)
                skeleton.setAnimation(0, backAnimation, true);
            if (trackEntry.animation && trackEntry.animation.name == toAnimation)
                onCompleted && onCompleted();
        });
        skeleton.setAnimation(0, toAnimation, false);
    };
    SicboGameUtils.setContentLabelAutoSize = function (label, content) {
        if (content.length > 30) {
            label.node.width = (30 * label.fontSize) / 2;
            label.overflow = cc.Label.Overflow.RESIZE_HEIGHT;
        }
        else {
            label.overflow = cc.Label.Overflow.NONE;
        }
        label.string = content;
    };
    SicboGameUtils.FormatString = function (str) {
        var val = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            val[_i - 1] = arguments[_i];
        }
        for (var index = 0; index < val.length; index++) {
            str = str.replace("{" + index + "}", val[index].toString());
        }
        return str;
    };
    SicboGameUtils.setParentTemp = function (nameTempNode, node, parentOfTemp, zIndex) {
        if (nameTempNode === void 0) { nameTempNode = "Temp"; }
        if (zIndex === void 0) { zIndex = 90; }
        var temp = parentOfTemp.getChildByName(nameTempNode);
        if (!temp) {
            temp = new cc.Node(nameTempNode);
            parentOfTemp.addChild(temp);
            temp.zIndex = zIndex;
            parentOfTemp.sortAllChildren();
        }
        var widget = node.getComponent(cc.Widget);
        if (widget)
            widget.enabled = false;
        var startPos = SicboGameUtils.convertToOtherNode(node.parent, temp, node.position);
        node.setParent(temp);
        node.position = startPos;
        node.zIndex = 0;
    };
    SicboGameUtils.isPlatformIOS = function () {
        return cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD;
    };
    SicboGameUtils.isBrowser = function () {
        return cc.sys.isBrowser;
    };
    SicboGameUtils.isNative = function () {
        return cc.sys.isNative;
    };
    SicboGameUtils.isLogVersion = function () {
        return SicboPortalAdapter_1.default.getInstance().isLogVersion();
    };
    SicboGameUtils.isNullOrUndefined = function (object) {
        return object == null || object == undefined;
    };
    SicboGameUtils.copyToClipboard = function (str) {
        switch (cc.sys.platform) {
            case cc.sys.ANDROID:
            case cc.sys.IPHONE:
            case cc.sys.IPAD:
                SicboNative_1.default.getInstance().call("copyToClipBoard", str);
                break;
            case cc.sys.DESKTOP_BROWSER:
            case cc.sys.MOBILE_BROWSER:
                var el = document.createElement("textarea");
                el.value = str;
                document.body.appendChild(el);
                el.select();
                document.execCommand("copy");
                document.body.removeChild(el);
                //cc.log("copy to clipboard");
                break;
        }
    };
    return SicboGameUtils;
}());
exports.default = SicboGameUtils;

cc._RF.pop();