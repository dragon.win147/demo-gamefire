"use strict";
cc._RF.push(module, '0abe3eaLONMMLpQQLBqkbDy', 'SicboSkeletonUtils');
// Game/Sicbo_New/Scripts/RNGCommons/Utils/SicboSkeletonUtils.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SicboSkeletonUtils = /** @class */ (function () {
    function SicboSkeletonUtils() {
    }
    SicboSkeletonUtils.setAnimation = function (skeleton, animationName, loop, startTime, timeScale) {
        if (animationName === void 0) { animationName = "animation"; }
        if (loop === void 0) { loop = false; }
        if (startTime === void 0) { startTime = 0; }
        if (timeScale === void 0) { timeScale = 1; }
        if (!skeleton)
            return;
        var state = skeleton.setAnimation(0, animationName, loop);
        if (state)
            state.animationStart = startTime;
        skeleton.timeScale = timeScale;
    };
    return SicboSkeletonUtils;
}());
exports.default = SicboSkeletonUtils;

cc._RF.pop();