"use strict";
cc._RF.push(module, 'c2455M+uvpIcpqPa+DVali+', 'SicboLoading');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboLoading.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboLoading = /** @class */ (function (_super) {
    __extends(SicboLoading, _super);
    function SicboLoading() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.activesOnStart = [];
        _this.deactivesOnStart = [];
        _this.activesOnFinish = [];
        _this.deactivesOnFinish = [];
        _this.onStartEvents = [];
        _this.onFinishEvents = [];
        return _this;
    }
    SicboLoading.prototype.startLoading = function () {
        this.setActiveNodes(this.activesOnStart, true);
        this.setActiveNodes(this.deactivesOnStart, false);
        this.runEvents(this.onStartEvents);
    };
    SicboLoading.prototype.finishLoading = function () {
        this.setActiveNodes(this.activesOnFinish, true);
        this.setActiveNodes(this.deactivesOnFinish, false);
        this.runEvents(this.onFinishEvents);
    };
    SicboLoading.prototype.setActiveNodes = function (nodes, isActive) {
        if (isActive === void 0) { isActive = true; }
        for (var i = 0; i < nodes.length; i++) {
            nodes[i].active = isActive;
        }
    };
    SicboLoading.prototype.runEvents = function (events) {
        for (var i = 0; i < events.length; i++) {
            events[i].emit([events[i].customEventData]);
        }
    };
    __decorate([
        property([cc.Node])
    ], SicboLoading.prototype, "activesOnStart", void 0);
    __decorate([
        property([cc.Node])
    ], SicboLoading.prototype, "deactivesOnStart", void 0);
    __decorate([
        property([cc.Node])
    ], SicboLoading.prototype, "activesOnFinish", void 0);
    __decorate([
        property([cc.Node])
    ], SicboLoading.prototype, "deactivesOnFinish", void 0);
    __decorate([
        property([cc.Component.EventHandler])
    ], SicboLoading.prototype, "onStartEvents", void 0);
    __decorate([
        property([cc.Component.EventHandler])
    ], SicboLoading.prototype, "onFinishEvents", void 0);
    SicboLoading = __decorate([
        ccclass
    ], SicboLoading);
    return SicboLoading;
}(cc.Component));
exports.default = SicboLoading;

cc._RF.pop();