"use strict";
cc._RF.push(module, 'b5d6bc/eZdIpaukMTL7PNWm', 'SicboSnapScrollView');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboSnapScrollView.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ScrollEvent = cc.ScrollView.EventType;
var SicboSnapScrollView = /** @class */ (function (_super) {
    __extends(SicboSnapScrollView, _super);
    function SicboSnapScrollView() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scrollView = null;
        _this.numOfItemInView = 4;
        _this.contentStartPosX = 0;
        _this.preScrollEvent = ScrollEvent.SCROLL_BEGAN;
        _this.snapActive = false;
        return _this;
    }
    Object.defineProperty(SicboSnapScrollView.prototype, "contentPosX", {
        get: function () {
            return this.scrollView.getContentPosition().x;
        },
        enumerable: false,
        configurable: true
    });
    SicboSnapScrollView.prototype.onLoad = function () {
        this.scrollView = this.getComponent(cc.ScrollView);
        this.contentStartPosX = this.contentPosX;
        this.init();
        this.registerEvent();
    };
    SicboSnapScrollView.prototype.init = function () {
        var scrollViewEventHandler = new cc.Component.EventHandler();
        scrollViewEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        scrollViewEventHandler.component = "SicboSnapScrollView"; // This is the code file name
        scrollViewEventHandler.handler = "callback";
        // scrollViewEventHandler.customEventData = "";    
        this.scrollView.scrollEvents.push(scrollViewEventHandler);
    };
    SicboSnapScrollView.prototype.registerEvent = function () {
        // this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this, true);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnded, this, true);
        this.node.on(cc.Node.EventType.MOUSE_WHEEL, this.onMouseWheel, this, true);
    };
    SicboSnapScrollView.prototype.unregisterEvent = function () {
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this, true);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnded, this, true);
        this.node.off(cc.Node.EventType.MOUSE_WHEEL, this.onMouseWheel, this, true);
    };
    SicboSnapScrollView.prototype.onTouchBegan = function () {
        // //cc.log("DKM", "onTouchBegan");
    };
    SicboSnapScrollView.prototype.onTouchEnded = function () {
        // //cc.log("DKM", "onTouchEnded");
        this.snapActive = true;
    };
    // private onTouchCancelled(){
    //     //cc.log("DKM", "onTouchCancelled");
    // }
    SicboSnapScrollView.prototype.onMouseWheel = function () {
        // //cc.log("DKM", "onMouseWheel");
        this.snapActive = true;
    };
    // update (dt) {}
    SicboSnapScrollView.prototype.callback = function (scrollView, eventType, customEventData) {
        this.preScrollEvent = eventType;
        switch (eventType) {
            case cc.ScrollView.EventType.SCROLL_ENDED:
                this.onScrollEnd();
                break;
            case cc.ScrollView.EventType.AUTOSCROLL_ENDED_WITH_THRESHOLD:
                // //cc.log("DKM", "AUTOSCROLL_ENDED_WITH_THRESHOLD");
                break;
            case cc.ScrollView.EventType.SCROLL_BEGAN:
                // //cc.log("DKM", "SCROLL_BEGAN");
                break;
            case cc.ScrollView.EventType.SCROLLING:
                // //cc.log("DKM", "SCROLLING");
                break;
        }
    };
    SicboSnapScrollView.prototype.onScrollEnd = function () {
        this.snap();
        this.snapActive = false;
    };
    SicboSnapScrollView.prototype.snap = function () {
        if (this.snapActive == false)
            return;
        var numOfItem = this.scrollView.content.childrenCount;
        var spacing = this.scrollView.content.width / numOfItem;
        var curPosX = this.contentPosX;
        var itemIndex = Math.abs(Math.round((curPosX - this.contentStartPosX) / spacing)
            - Math.round((numOfItem - this.numOfItemInView) / 2));
        var percent = itemIndex * (numOfItem / (numOfItem - this.numOfItemInView)) / numOfItem;
        this.scrollView.scrollToPercentHorizontal(percent, .5);
        //cc.log("Snap", itemIndex);
        // this.scrollView.scrollToPercentHorizontal(1 * 2/numOfItem, .5);
    };
    SicboSnapScrollView.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
        this.unregisterEvent();
    };
    __decorate([
        property
    ], SicboSnapScrollView.prototype, "numOfItemInView", void 0);
    SicboSnapScrollView = __decorate([
        ccclass
    ], SicboSnapScrollView);
    return SicboSnapScrollView;
}(cc.Component));
exports.default = SicboSnapScrollView;

cc._RF.pop();