"use strict";
cc._RF.push(module, 'ae64eHPPBxHrqdoGoFIjgEU', 'SicboScrollViewWithButton');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboScrollViewWithButton.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ScrollEvent = cc.ScrollView.EventType;
var SicboScrollViewWithButton = /** @class */ (function (_super) {
    __extends(SicboScrollViewWithButton, _super);
    function SicboScrollViewWithButton() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scrollView = null;
        _this.preButton = null;
        _this.nextButton = null;
        _this.leftThreshold = 1;
        _this.rightThreshold = 2;
        return _this;
        //!SECTION
    }
    SicboScrollViewWithButton.prototype.onLoad = function () {
        this.scrollView = this.getComponent(cc.ScrollView);
        this.init();
    };
    //SECTION Scroll Pre/next button
    SicboScrollViewWithButton.prototype.init = function () {
        var scrollViewEventHandler = new cc.Component.EventHandler();
        scrollViewEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        scrollViewEventHandler.component = "SicboScrollViewWithButton"; // This is the code file name
        scrollViewEventHandler.handler = "callback";
        // scrollViewEventHandler.customEventData = "";    
        this.scrollView.scrollEvents.push(scrollViewEventHandler);
        this.checkScrollViewButton();
    };
    SicboScrollViewWithButton.prototype.callback = function (scrollView, eventType, customEventData) {
        switch (eventType) {
            case cc.ScrollView.EventType.SCROLL_ENDED:
                this.checkScrollViewButton();
                break;
        }
    };
    SicboScrollViewWithButton.prototype.checkScrollViewButton = function () {
        var offSetX = this.scrollView.getScrollOffset().x;
        var halfContentW = this.scrollView.content.getContentSize().width / 2;
        //cc.log("SCROLL END", offSetX, halfContentW);
        var isLeft = Math.abs(offSetX) <= this.leftThreshold;
        var isRight = offSetX <= -halfContentW + this.rightThreshold;
        this.preButton.interactable = !isLeft;
        this.nextButton.interactable = !isRight;
    };
    __decorate([
        property(cc.Button)
    ], SicboScrollViewWithButton.prototype, "preButton", void 0);
    __decorate([
        property(cc.Button)
    ], SicboScrollViewWithButton.prototype, "nextButton", void 0);
    __decorate([
        property()
    ], SicboScrollViewWithButton.prototype, "leftThreshold", void 0);
    __decorate([
        property()
    ], SicboScrollViewWithButton.prototype, "rightThreshold", void 0);
    SicboScrollViewWithButton = __decorate([
        ccclass
    ], SicboScrollViewWithButton);
    return SicboScrollViewWithButton;
}(cc.Component));
exports.default = SicboScrollViewWithButton;

cc._RF.pop();