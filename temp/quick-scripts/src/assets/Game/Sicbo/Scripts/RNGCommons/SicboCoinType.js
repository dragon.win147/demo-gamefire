"use strict";
cc._RF.push(module, '061d9v/VcxP57DrdIBH+JNm', 'SicboCoinType');
// Game/Sicbo_New/Scripts/RNGCommons/SicboCoinType.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboCoinType = void 0;
var SicboCoinType;
(function (SicboCoinType) {
    SicboCoinType[SicboCoinType["Coin100"] = 1] = "Coin100";
    SicboCoinType[SicboCoinType["Coin200"] = 2] = "Coin200";
    SicboCoinType[SicboCoinType["Coin500"] = 3] = "Coin500";
    SicboCoinType[SicboCoinType["Coin1k"] = 4] = "Coin1k";
    SicboCoinType[SicboCoinType["Coin2k"] = 5] = "Coin2k";
    SicboCoinType[SicboCoinType["Coin5k"] = 6] = "Coin5k";
    SicboCoinType[SicboCoinType["Coin10k"] = 7] = "Coin10k";
    SicboCoinType[SicboCoinType["Coin20k"] = 8] = "Coin20k";
    SicboCoinType[SicboCoinType["Coin50k"] = 9] = "Coin50k";
    SicboCoinType[SicboCoinType["Coin100k"] = 10] = "Coin100k";
    SicboCoinType[SicboCoinType["Coin200k"] = 11] = "Coin200k";
    SicboCoinType[SicboCoinType["Coin500k"] = 12] = "Coin500k";
    SicboCoinType[SicboCoinType["Coin1M"] = 13] = "Coin1M";
    SicboCoinType[SicboCoinType["Coin2M"] = 14] = "Coin2M";
    SicboCoinType[SicboCoinType["Coin5M"] = 15] = "Coin5M";
    SicboCoinType[SicboCoinType["Coin10M"] = 16] = "Coin10M";
    SicboCoinType[SicboCoinType["Coin20M"] = 17] = "Coin20M";
    SicboCoinType[SicboCoinType["Coin50M"] = 18] = "Coin50M";
})(SicboCoinType = exports.SicboCoinType || (exports.SicboCoinType = {}));

cc._RF.pop();