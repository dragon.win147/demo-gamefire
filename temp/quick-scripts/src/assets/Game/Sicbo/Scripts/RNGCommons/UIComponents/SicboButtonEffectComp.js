"use strict";
cc._RF.push(module, '6ec2fHROdxCUYcyFdDUo+rc', 'SicboButtonEffectComp');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboButtonEffectComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboButtonEffectComp = /** @class */ (function (_super) {
    __extends(SicboButtonEffectComp, _super);
    function SicboButtonEffectComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.button = null;
        _this.toggle = null;
        _this.target = null;
        return _this;
    }
    SicboButtonEffectComp.prototype.onLoad = function () {
        this.button = this.node.getComponent(cc.Button);
        this.toggle = this.node.getComponent(cc.Toggle);
        if (this.target == null)
            this.target = this.node;
    };
    SicboButtonEffectComp.prototype.start = function () {
        this.registerEvent();
    };
    // update (dt) {}
    SicboButtonEffectComp.prototype.registerEvent = function () {
        this.node.on(cc.Node.EventType.MOUSE_ENTER, this.onMouseEnter, this, true);
        this.node.on(cc.Node.EventType.MOUSE_LEAVE, this.onMouseLeave, this, true);
    };
    SicboButtonEffectComp.prototype.unregisterEvent = function () {
        this.node.off(cc.Node.EventType.MOUSE_ENTER, this.onMouseEnter, this, true);
        this.node.off(cc.Node.EventType.MOUSE_LEAVE, this.onMouseLeave, this, true);
    };
    SicboButtonEffectComp.prototype.onMouseEnter = function (_, __) {
        if (this.button != null && this.button.interactable)
            this.target.scale = 1.05;
        if (this.toggle != null && this.toggle.interactable)
            this.target.scale = 1.05;
    };
    SicboButtonEffectComp.prototype.onMouseLeave = function (event, captureListeners) {
        this.target.scale = 1;
    };
    SicboButtonEffectComp.prototype.onDestroy = function () {
        this.unregisterEvent();
    };
    __decorate([
        property(cc.Node)
    ], SicboButtonEffectComp.prototype, "target", void 0);
    SicboButtonEffectComp = __decorate([
        ccclass
    ], SicboButtonEffectComp);
    return SicboButtonEffectComp;
}(cc.Component));
exports.default = SicboButtonEffectComp;

cc._RF.pop();