"use strict";
cc._RF.push(module, '685belZ3YxFwotjWM3/Npnm', 'SicboResultType');
// Game/Sicbo_New/Scripts/Enums/SicboResultType.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboResultType = void 0;
var SicboResultType;
(function (SicboResultType) {
    SicboResultType[SicboResultType["TAI"] = 0] = "TAI";
    SicboResultType[SicboResultType["XIU"] = 1] = "XIU";
    SicboResultType[SicboResultType["BAO"] = 2] = "BAO";
})(SicboResultType = exports.SicboResultType || (exports.SicboResultType = {}));

cc._RF.pop();