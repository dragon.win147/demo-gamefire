"use strict";
cc._RF.push(module, 'e77afE0OfJPKIudqWWOfMBP', 'IdentityHandler');
// Game/Lobby/Network/IdentityHandler.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// import UserManager from "../Managers/UserManager";
var empty_pb_1 = require("google-protobuf/google/protobuf/empty_pb");
var MyidServiceClientPb_1 = require("@greyhole/myid/MyidServiceClientPb");
var myid_pb_1 = require("@greyhole/myid/myid_pb");
var myid_code_pb_1 = require("@greyhole/myid/myid_code_pb");
var HandlerClientBase_1 = require("@gameloot/client-base-handler/build/HandlerClientBase");
var UIManager_1 = require("../Manager/UIManager");
var GameDefine_1 = require("../Define/GameDefine");
var TextNotify_1 = require("../Manager/TextNotify");
var ConnectDefine_1 = require("../Define/ConnectDefine");
var EventManager_1 = require("../Event/EventManager");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var WarningPopupSystem_1 = require("../Popup/WarningPopupSystem");
var PortalText_1 = require("../Utils/PortalText");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var IdentityHandler = /** @class */ (function (_super) {
    __extends(IdentityHandler, _super);
    function IdentityHandler() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.onHandleChangePassSucceed = null;
        _this.onHandleLoginSucceed = null;
        _this.onVerifyOTPFalse = null;
        _this.backtoLoginScene = null;
        _this.getNewCaptcha = null;
        _this.closeLoginPopup = null;
        _this.client = null;
        return _this;
    }
    IdentityHandler_1 = IdentityHandler;
    IdentityHandler.getInstance = function () {
        if (!IdentityHandler_1.instance) {
            IdentityHandler_1.instance = new IdentityHandler_1();
            IdentityHandler_1.instance.client = new MyidServiceClientPb_1.MyIDClient(ConnectDefine_1.default.addressSanBay, {}, null);
        }
        return IdentityHandler_1.instance;
    };
    IdentityHandler.destroy = function () {
        IdentityHandler_1.instance = null;
    };
    Object.defineProperty(IdentityHandler.prototype, "metaData", {
        get: function () {
            return {
                Authorization: "Bearer " + IdentityHandler_1.getInstance().token,
            };
        },
        enumerable: false,
        configurable: true
    });
    IdentityHandler.prototype.showLoading = function (callback) {
        UIManager_1.default.getInstance()
            .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, false, null, null)
            .then(function () {
            callback();
        });
    };
    IdentityHandler.prototype.hideLoading = function () {
        UIManager_1.default.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
    };
    IdentityHandler.prototype.onShowErr = function (str, onClick) {
        var code = parseInt(str.split("-")[0]);
        if (!isNaN(code)) {
            var messError = "";
            switch (code) {
                case myid_code_pb_1.Code.INTERNAL:
                case myid_code_pb_1.Code.UNAVAILABLE:
                    messError = PortalText_1.default.POR_MESS_INTERNAL;
                    UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, messError, onClick, onClick);
                    return;
            }
        }
        UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, str, onClick, onClick);
    };
    IdentityHandler.prototype.onDisconnect = function (CodeSocket, messError) {
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, CodeSocket, false, messError);
    };
    IdentityHandler.prototype.onLogin = function (username, password, response, onErr) {
        if (onErr === void 0) { onErr = null; }
        console.log("onLogin " + username + "," + password);
        // this.onHandleLoginSucceed = response;
        var request = new myid_pb_1.SignInV2Request();
        var myId = new myid_pb_1.SignInV2Request.MyID();
        myId.setUsername(username);
        myId.setPassword(password);
        request.setMyId(myId);
        request.setDeviceId("");
        request.setDeviceName("");
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.signInV2(request, self.metaData, function (err, res) {
                if (err && onErr)
                    onErr();
                cc.log("res", res === null || res === void 0 ? void 0 : res.toObject());
                // if (res?.toObject().hasOwnProperty('confirmOtp')) {
                // if (res?.toObject().confirmOtp) {
                //   if (res.getConfirmOtp().getWaiting() > 0) {
                //     let message = GameUtils.FormatString(PortalText.POR_NOTIFY_WARNING_RESET_OTP, res.getConfirmOtp().getWaiting());
                //     UIManager.getInstance().openNotify(TextNotify, GameDefine.TextNotify, new TextNotifyData(message));
                //     self.onSendReply(err, res, msgId);
                //     return;
                //   }
                //   let data = new RequireOTPData();
                //   data.username = username;
                //   data.password = password;
                //   data.requireOTPType = REQUIREOTP.LOGIN;
                //   data.otpId = res.getConfirmOtp().getSuccess().getId();
                //   data.timeRecallOTP = res.getConfirmOtp().getSuccess().getExpiry();
                //   UIManager.getInstance().openPopupWithErrorHandler(InputOTPPopup, "InputOTPPopup", data, UIManager.getInstance().LayerUpPopup, false);
                //   self.onSendReply(err, res, msgId);
                //   return;
                // }
                self.handleResponseLogin(err, res === null || res === void 0 ? void 0 : res.getTokenInfo(), msgId, username);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            if (res === null || res === void 0 ? void 0 : res.toObject().confirmOtp) {
                return;
            }
            response && response(res);
        }, null, true);
    };
    IdentityHandler.prototype.handleResponseLogin = function (err, tokenInfo, msgId, username) {
        cc.log("handleResponseLogin");
        if (err != null) {
            // console.log("err " + err);
        }
        else {
            // Todo in request logic
            if (tokenInfo) {
                this.token = tokenInfo.getAccessToken();
                // UserManager.getInstance().userData.displayName = "";
                // UserManager.getInstance().userData.userId = tokenInfo.getSafeId();
                // UserManager.getInstance().saveInfoLogin(tokenInfo.getIdToken());
                // UserManager.getInstance().setUserName(tokenInfo.getUsername());
                // console.log("AccessToken " + this.token);
                // console.log("IdToken " + tokenInfo.getIdToken());
                // cc.director.loadScene("SplashScene", () => {
                //   MarketPlace.showPromotionPopup = true;
                // });
            }
            else {
                //TODO : fix tạm thời cho trường hợp native vì 'grpcWeb.Error' trên native return null
                var error = {
                    code: 2,
                    metadata: null,
                    name: "",
                    message: "",
                };
                err = error;
            }
        }
        this.onSendReply(err, tokenInfo, msgId);
    };
    IdentityHandler.prototype.getAccessToken = function (idToken, response) {
        var _this = this;
        var request = new myid_pb_1.CreateAccessTokenRequest();
        request.setIdToken(idToken);
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.createAccessToken(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            _this.token = res.getAccessToken();
            cc.log("this.token", _this.token);
            // UserManager.getInstance().saveInfoLogin(idToken);
            response(res);
        }, null, false);
    };
    IdentityHandler.prototype.verifyAccessToken = function (token, response, onError) {
        if (onError === void 0) { onError = null; }
        var request = new empty_pb_1.Empty();
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.me(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, onError, false, false, 5);
    };
    IdentityHandler.prototype.verifyTokenReconnect = function (onSuccess, onError) {
        if (onSuccess === void 0) { onSuccess = null; }
        if (onError === void 0) { onError = null; }
        // cc.log("verifyTokenReconnect token = " + this.token);
        if (this.token != "") {
            this.verifyAccessToken(this.token, function (response) {
                cc.log("verifyTokenReconnect success");
                onSuccess && onSuccess();
            }, function (msgError, code) {
                cc.log("verifyTokenReconnect onFail");
                onError && onError(msgError, code);
            });
        }
    };
    IdentityHandler.prototype.me = function (response) {
        var _this = this;
        var request = new empty_pb_1.Empty();
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.me(request, _this.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, null, false);
    };
    IdentityHandler.prototype.handleCustomError = function (errCode, err) {
        if (errCode == myid_code_pb_1.Code.UNKNOWN)
            return;
        if (errCode == myid_code_pb_1.Code.OK)
            return;
        var messError = "";
        UIManager_1.default.getInstance().openNotify(TextNotify_1.default, GameDefine_1.default.TextNotify, new TextNotify_1.TextNotifyData(messError, 2));
    };
    var IdentityHandler_1;
    IdentityHandler.deviceIdKey = "DEVICE_ID";
    IdentityHandler = IdentityHandler_1 = __decorate([
        ccclass
    ], IdentityHandler);
    return IdentityHandler;
}(HandlerClientBase_1.default));
exports.default = IdentityHandler;

cc._RF.pop();