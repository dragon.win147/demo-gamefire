"use strict";
cc._RF.push(module, 'c3b1dBt+lRCVqiRYWgQe6dy', 'StabilizeConnection');
// Game/Lobby/Network/StabilizeConnection.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var bentau_pb_1 = require("@gameloot/bentau/bentau_pb");
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var GameUtils_1 = require("../Utils/GameUtils");
var TimeUtils_1 = require("../Utils/TimeUtils");
var EventManager_1 = require("../Event/EventManager");
var BenTauHandler_1 = require("./BenTauHandler");
var EventBusManager_1 = require("../Event/EventBusManager");
var StabilizeConnection = /** @class */ (function () {
    function StabilizeConnection() {
    }
    StabilizeConnection.update = function (dt) {
        if (!this.start)
            return;
        if (!GameUtils_1.default.isStatusPing)
            return;
        this.currentTime += dt;
        if (this.currentTime > this.TIME_TO_PONG * 1.5) {
            this.currentTime = 0;
            StabilizeConnection.onPingFailure(this.currentTime);
        }
    };
    StabilizeConnection.send = function () {
        var _this = this;
        this.clear();
        this.start = true;
        this.pingDict.clear();
        var ping = new bentau_pb_1.Ping();
        ping.setTopic(GameUtils_1.default.gameTopic);
        var msgId = BenTauHandler_1.default.getInstance().send(topic_pb_1.Topic.PING, "", ping.serializeBinary());
        if (msgId == null) {
            this.continue();
            return;
        }
        BenTauHandler_1.default.getInstance().onSubscribe(topic_pb_1.Topic.PONG, function (msg) {
            _this.onReceive(msg.getMsgId());
        });
        var time = TimeUtils_1.default.getCurrentTimeByMillisecond();
        if (msgId != null) {
            this.pingDict.set(msgId, time);
        }
    };
    StabilizeConnection.onReceive = function (msgId) {
        if (this.pingDict.has(msgId)) {
            var preTimePing = this.pingDict.get(msgId);
            var pingTime = TimeUtils_1.default.getCurrentTimeByMillisecond() - preTimePing;
            this.pingDict.delete(msgId);
            // if (GameUtils.isLogVersion()) {
            //   CheatManager.getInstance().open(PingCheat, "PingCheat", pingTime);
            // }
            EventBusManager_1.default.onPingTime(pingTime);
            this.onPingOk(pingTime);
        }
    };
    StabilizeConnection.clear = function () {
        GameUtils_1.default.isStatusPing = true;
        BenTauHandler_1.default.getInstance().onUnSubscribe(topic_pb_1.Topic.PONG);
        clearTimeout(this.ping);
        this.ping = null;
        this.start = false;
        this.currentTime = 0;
    };
    StabilizeConnection.continue = function () {
        var _this = this;
        if (!GameUtils_1.default.isStatusPing)
            return;
        this.ping = setTimeout(function () { return _this.send(); }, this.TIME_TO_PING);
    };
    StabilizeConnection.onPingOk = function (pingTime) {
        this.continue();
        GameUtils_1.default.isStatusPing = false;
        EventManager_1.default.fire(EventManager_1.default.ON_PING_OK);
    };
    StabilizeConnection.onPingFailure = function (pingTime) {
        cc.log("PING FAILED:" + pingTime + "ms");
        GameUtils_1.default.isStatusPing = false;
        EventManager_1.default.fire(EventManager_1.default.ON_PING_FAIL);
    };
    StabilizeConnection.setTimeOutMsg = function () {
        this.currentTime = 0;
    };
    StabilizeConnection.TIME_TO_PING = 5000;
    StabilizeConnection.TIME_TO_PONG = 3000;
    StabilizeConnection.pingDict = new Map();
    StabilizeConnection.ping = null;
    StabilizeConnection.currentTime = 0;
    StabilizeConnection.start = false;
    return StabilizeConnection;
}());
exports.default = StabilizeConnection;

cc._RF.pop();