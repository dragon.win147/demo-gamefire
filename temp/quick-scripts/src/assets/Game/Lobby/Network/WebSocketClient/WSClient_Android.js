"use strict";
cc._RF.push(module, '635a5J16TxEbru5tJQ+1qFu', 'WSClient_Android');
// Game/Lobby/Utils/WebSocketClient/WSClient_Android.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var bentau_pb_1 = require("@gameloot/bentau/bentau_pb");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var WSClient_Android = /** @class */ (function (_super) {
    __extends(WSClient_Android, _super);
    function WSClient_Android() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.isOpen = false;
        _this.isError = false;
        return _this;
    }
    WSClient_Android.prototype.open = function (address, onOpen, onMessage, onClose, onError, onReconnect) {
        var _this = this;
        console.log("WSClient_Android::receive");
        this.onOpenCb = onOpen;
        this.onMessageCb = onMessage;
        this.onCloseCb = onClose;
        this.onErrorCb = onError;
        this.onReconnect = onReconnect;
        this.isOpen = true;
        this.isError = false;
        // Native.getInstance().call("createWebSocketClient", address, IdentityHandler.getInstance().token);
        cc.systemEvent.on("WSClient_Android_open", function () {
            _this.onOpenCb && _this.onOpenCb();
        });
        cc.systemEvent.on("WSClient_Android_message", function (base64_string) {
            var data = Uint8Array.from(atob(base64_string), function (c) { return c.charCodeAt(0); });
            var benTauMessage = bentau_pb_1.BenTauMessage.deserializeBinary(data);
            _this.onMessageCb && _this.onMessageCb(benTauMessage);
        });
        cc.systemEvent.on("WSClient_Android_close", function (code, reason) {
            // Hiện tại không cần xử lý case này, case này là hệ thông tự đóng, còn các trường hợp manual close thì đã xử lý.
            cc.log("WSClient_Android_close " + code + " " + reason);
            _this.isOpen = false;
            _this.offEvent();
        });
        cc.systemEvent.on("WSClient_Android_error", function (msg) {
            cc.log("WSClient_Android_error " + msg);
            _this.offEvent();
            _this.isError = false;
            // this.onErrorCb && this.onErrorCb(msg);
        });
    };
    WSClient_Android.prototype.send = function (data) {
        cc.log("WSClient_Android send");
        var base64_string = btoa(String.fromCharCode.apply(null, new Uint8Array(data)));
        jsb.reflection.callStaticMethod("org/cocos2dx/javascript/NativeWebSocketClient", "send", "(Ljava/lang/String;)V", base64_string);
    };
    WSClient_Android.prototype.closeWS = function (code, reason) {
        this.offEvent();
        if (!this.isOpen)
            return;
        this.isOpen = false;
        this.onCloseCb && this.onCloseCb(code, reason);
        jsb.reflection.callStaticMethod("org/cocos2dx/javascript/NativeWebSocketClient", "close", "()V");
    };
    WSClient_Android.prototype.reConnect = function (code, reason) {
        // cc.log("reConnect = " + code + " reason = " + reason + " idWS " + this.idWS + " isOpen " + this.websocket.readyState);
        this.onReconnect && this.onReconnect(code, reason);
    };
    WSClient_Android.prototype.offEvent = function () {
        cc.systemEvent.off("WSClient_Android_open");
        cc.systemEvent.off("WSClient_Android_message");
        cc.systemEvent.off("WSClient_Android_close");
        cc.systemEvent.off("WSClient_Android_error");
    };
    WSClient_Android = __decorate([
        ccclass
    ], WSClient_Android);
    return WSClient_Android;
}(cc.Component));
exports.default = WSClient_Android;

cc._RF.pop();