"use strict";
cc._RF.push(module, '6e60bbi441AiJnCRgu1b/F1', 'MyAccountHandler');
// Game/Lobby/Network/MyAccountHandler.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var MyaccountServiceClientPb_1 = require("@gameloot/myaccount/MyaccountServiceClientPb");
var myaccount_pb_1 = require("@gameloot/myaccount/myaccount_pb");
var myaccount_code_pb_1 = require("@gameloot/myaccount/myaccount_code_pb");
var playah_pb_1 = require("@gameloot/playah/playah_pb");
var build_1 = require("@gameloot/client-base-handler/build");
var empty_pb_1 = require("google-protobuf/google/protobuf/empty_pb");
var PortalText_1 = require("../Utils/PortalText");
var ConnectDefine_1 = require("../Define/ConnectDefine");
var GameDefine_1 = require("../Define/GameDefine");
var EventManager_1 = require("../Event/EventManager");
var UIManager_1 = require("../Manager/UIManager");
var TextNotify_1 = require("../Manager/TextNotify");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var WarningPopupSystem_1 = require("../Popup/WarningPopupSystem");
var BenTauHandler_1 = require("./BenTauHandler");
var IdentityHandler_1 = require("./IdentityHandler");
var MyAccountHandler = /** @class */ (function (_super) {
    __extends(MyAccountHandler, _super);
    function MyAccountHandler() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.client = null;
        _this.bases = new Map();
        _this.frames = new Map();
        _this.playah = null;
        _this.handleErr = null;
        return _this;
    }
    MyAccountHandler.getInstance = function () {
        if (!MyAccountHandler.instance) {
            MyAccountHandler.instance = new MyAccountHandler(BenTauHandler_1.default.getInstance());
            MyAccountHandler.instance.client = new MyaccountServiceClientPb_1.MyAccountClient(ConnectDefine_1.default.addressSanBay, {}, null);
            MyAccountHandler.instance.topic = topic_pb_1.Topic.MY_ACCOUNT_LEVEL_UP;
            MyAccountHandler.instance.onSubscribe();
        }
        return MyAccountHandler.instance;
    };
    MyAccountHandler.destroy = function () {
        if (MyAccountHandler.instance)
            MyAccountHandler.instance.onUnSubscribe();
        MyAccountHandler.instance = null;
    };
    Object.defineProperty(MyAccountHandler.prototype, "Frames", {
        get: function () {
            return this.frames;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MyAccountHandler.prototype, "Bases", {
        get: function () {
            return this.bases;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MyAccountHandler.prototype, "Playah", {
        get: function () {
            return this.playah;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MyAccountHandler.prototype, "metaData", {
        get: function () {
            return {
                Authorization: "Bearer " + IdentityHandler_1.default.getInstance().token,
            };
        },
        enumerable: false,
        configurable: true
    });
    MyAccountHandler.prototype.showLoading = function (callback) {
        UIManager_1.default.getInstance()
            .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, false, null, null)
            .then(function () {
            callback();
        });
    };
    MyAccountHandler.prototype.hideLoading = function () {
        UIManager_1.default.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
    };
    MyAccountHandler.prototype.onShowErr = function (str, onClick) {
        UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, str, onClick, onClick);
    };
    MyAccountHandler.prototype.onDisconnect = function (CodeSocket, messError) {
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, CodeSocket, false, messError);
    };
    MyAccountHandler.prototype.onGetBasePath = function (id) {
        return this.bases.get(id);
    };
    MyAccountHandler.prototype.onGetFramePath = function (id) {
        return this.frames.get(id);
    };
    MyAccountHandler.prototype.onGetAvatarList = function (response) {
        var _this = this;
        if (response === void 0) { response = null; }
        //cc.log("onGetAvatarList");
        var self = this;
        var request = new myaccount_pb_1.ListMyAvatarsRequest();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.listMyAvatars(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            res.getBasesList().forEach(function (base) {
                _this.bases.set(base.getId(), base.getPath());
            });
            res.getFramesList().forEach(function (frame) {
                _this.frames.set(frame.getId(), frame.getPath());
            });
            if (response)
                response(res);
        }, function () {
            //cc.log("Err");
            if (response)
                response(null);
        }, false);
    };
    MyAccountHandler.prototype.onGetAvatarListWhenSignUp = function (gender, random, limit, response) {
        var _this = this;
        if (random === void 0) { random = true; }
        if (limit === void 0) { limit = 10; }
        if (response === void 0) { response = null; }
        var self = this;
        var request = new myaccount_pb_1.ListBasesRequest();
        request.setGender(gender);
        request.setRandom(random);
        request.setLimit(limit);
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.listBases(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            _this.frames.set(res.getDefaultFrame().getId(), res.getDefaultFrame().getPath());
            res.getBasesList().forEach(function (base) {
                _this.bases.set(base.getId(), base.getPath());
            });
            response(res);
        }, null, false);
    };
    MyAccountHandler.prototype.getProfile = function (response) {
        //cc.log("getProfile " + IdentityHandler.getInstance().token);
        var request = new empty_pb_1.Empty();
        var self = this;
        var msgId = self.getUniqueId();
        var metaData = { Authorization: "Bearer " + IdentityHandler_1.default.getInstance().token };
        var sendRequest = function () {
            self.client.me(request, metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            //cc.log("getProfile done ");
            if (res == null || !(res === null || res === void 0 ? void 0 : res.hasProfile()) || !(res === null || res === void 0 ? void 0 : res.hasAvatar())) {
                // UIManager.getInstance().openUpdateNickNamePopup();
            }
            else {
                // UserManager.getInstance().setUserData(res);
                var playah = new playah_pb_1.Playah();
                playah.setUserId(res.getSafeId());
                playah.setProfile(res.getProfile());
                playah.setAvatar(res.getAvatar());
                self.playah = playah;
            }
            if (response) {
                response(res);
            }
        }, function (err, code) {
            // if (code != Code.UNKNOWN) {
            //   UIManager.getInstance().openUpdateNickNamePopup();
            // }
        }, false);
    };
    MyAccountHandler.prototype.handleCustomError = function (errCode, err) {
        if (errCode == myaccount_code_pb_1.Code.UNKNOWN)
            return;
        if (this.handleErr)
            this.handleErr();
        //cc.log("handleCustomError " + "errCode" + err);
        var errString = "[" + errCode + "] ";
        //cc.log("errcode: ", errCode);
        switch (errCode) {
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_REQUIRE_LENGTH:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_TOO_SHORT;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_OVER_LENGTH:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_TOO_LONG;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_START_SPACE_CHAR:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_INVALID_PREFIX;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_SAME_USERNAME:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_SAME_USERNAME;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_HAS_PROFANITY:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_PROFANITY;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_TOO_SHORT;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_HAS_INVALID_CHARACTER:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_HAS_BANNED_CHARACTER;
                break;
        }
        UIManager_1.default.getInstance().openNotify(TextNotify_1.default, GameDefine_1.default.TextNotify, new TextNotify_1.TextNotifyData(errString, 2));
    };
    MyAccountHandler.prototype.onReceive = function (msg) {
        var message = myaccount_pb_1.Level.deserializeBinary(msg.getPayload_asU8());
        var level = message.getLevel();
        EventManager_1.default.fire(EventManager_1.default.UPDATE_BADGE_LEVEL_UP, level);
        // UIManager.getInstance().openAnimationLevelUpPopup(level);
    };
    return MyAccountHandler;
}(build_1.HandlerClientBenTauBase));
exports.default = MyAccountHandler;

cc._RF.pop();