"use strict";
cc._RF.push(module, 'f97e6MRpsVAiYn+ZLhBPZJS', 'GameDefine');
// Portal/Common/Scripts/Defines/GameDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameDefine = /** @class */ (function () {
    function GameDefine() {
    }
    GameDefine.Sicbo = "Sicbo";
    GameDefine.XocDia = "XocDia";
    GameDefine.BauCua = "BauCua";
    GameDefine.Phom = "Phom";
    GameDefine.TienLenMienNam = "TienLenMienNam";
    GameDefine.MauBinh = "MauBinh";
    //#region ======== ON/OFF ========
    GameDefine.CC_LOG_ENABLE = true;
    // Tạm sửa đẻ quay video xong mở lại
    GameDefine.LOG_VERSION_ENABLE = false; // Use For Drawcall/Version
    GameDefine.IS_PROD_VERSION = false;
    //#region ======== scene ========
    GameDefine.SicboScene = "SicboScene";
    GameDefine.XocDiaScene = "XocDiaScene";
    GameDefine.ThanTaiScene = "ThanTaiScene";
    GameDefine.ChienThanScene = "ChienThanScene";
    GameDefine.LoDeScene = "LoDeScene";
    GameDefine.SplashGame = "SplashScene";
    GameDefine.MainScene = "MainScene";
    GameDefine.LobbyTLMNScene = "TLMNLobbyScene";
    GameDefine.BauCuaScene = "BauCuaScene";
    GameDefine.LoadingChangeScene = "LoadingChangeScene";
    GameDefine.MauBinhScene = "MauBinhLobbyScene";
    GameDefine.PokerScene = "PokerLobbyScene";
    GameDefine.PhomScene = "PhomLobbyScene";
    GameDefine.LobbyCardGame = "LobbyCardGame";
    GameDefine.KBDDScene = "KBDDScene";
    GameDefine.KBDDLobbyScene = "KBDDLobbyScene";
    //#endregion
    //#region ======== element ========
    GameDefine.PortalBundle = "PortalResources";
    GameDefine.LobbyCardBundle = "LobbyCardBundle";
    GameDefine.PaymentBundle = "Payment";
    //#endregion
    //#region ======== element ========
    GameDefine.HeaderMainGameElement = "HeaderMainGameElement";
    //#endregion
    //#region ======== popup ========
    GameDefine.ProfilePopup = "ProfilePopup";
    GameDefine.InfoGamePopup = "InfoGamePopup";
    GameDefine.NotifyPopup = "NotifyPopup";
    GameDefine.ChatPopup = "ChatPopupNew";
    GameDefine.CardGameChatPopup = "CardGameChatPopup";
    GameDefine.TableGameChatPopup = "TableGameChatPopup";
    GameDefine.LoginPopup = "LoginPopup";
    GameDefine.ChatSettingPopup = "ChatSettingPopup";
    GameDefine.Confirm2ButtonPopup = "Confirm2ButtonPopup";
    GameDefine.CreateTablePopup = "CreateTablePopup";
    GameDefine.LeaderBoardPopup = "LeaderBoardPopup";
    // public static PhomLeaderBoardPopup: string = "PhomVinhDanhPopup";
    GameDefine.AllPlayerInTablePopup = "AllPlayerInTablePopup";
    GameDefine.UserProfilePopup = "UserProfilePopup";
    GameDefine.UserProfilePopupNew = "UserProfilePopupNew";
    GameDefine.UpdateNickNamePopup = "UpdateNickNamePopup";
    GameDefine.ChangeAvatarPopup = "ChangeAvatarPopup";
    GameDefine.SettingPopup = "SettingPopup";
    GameDefine.ThanTaiSettingPopup = "ThanTaiSettingPopup";
    GameDefine.PasswordPopup = "PasswordPopup";
    GameDefine.WalletPopup = "WalletPopup";
    GameDefine.ShareTablePopup = "ShareTablePopup";
    GameDefine.BetHistoryPopup = "BetHistoryPopup";
    GameDefine.ChangeLocationPopup = "ChangeLocationPopup";
    GameDefine.ChangeMailPopup = "ChangeMailPopup";
    GameDefine.ChangePasswordPopup = "ChangePasswordPopup";
    GameDefine.ChangePhonePopup = "ChangePhonePopup";
    GameDefine.LogoutPopup = "LogoutPopup";
    GameDefine.MainSettingPopup = "MainSettingPopupV2";
    GameDefine.VerifyEmailSuccessPopup = "VerifyEmailSuccessPopup";
    GameDefine.GlobalInviteSettingPopup = "GlobalInviteSettingPopup";
    GameDefine.GlobalInviteNotify = "NotifyInviteGlobal";
    GameDefine.PortalHistoryPopup = "PortalHistoryPopup";
    GameDefine.PortalVipGuidePopup = "PortalVipGuidePopup";
    GameDefine.PortalMessagePopup = "PortalMessagePopup";
    GameDefine.DeleteMessagePopup = "DeleteMessagePopup";
    GameDefine.PortalReferralPopup = "PortalReferralPopup";
    GameDefine.TLMNBetHistoryPopup = "TLMNHistoryPopup";
    GameDefine.PhomHistoryPopup = "PhomHistoryPopup";
    GameDefine.CardGameSettingPopup = "CardGameSettingPopup";
    GameDefine.MauBinhHistoryPopup = "MauBinhHistoryPopup";
    GameDefine.CardGameInfoGamePopup = "CardGameInfoGamePopup";
    GameDefine.CustomAvatarPopup = "CustomAvatarPopupNew";
    GameDefine.EventPopup = "EventPopup";
    GameDefine.PortalLeaderBoardPopup = "PortalLeaderBoardPopupV2";
    GameDefine.TopHuBigPopup = "TopHuBigPopup";
    GameDefine.GiftCodeHistoryPopup = "GiftCodeHistoryPopup";
    GameDefine.AnimationLevelUpPopup = "AnimationLevelUpPopup";
    GameDefine.RewardPopup = "RewardPopup";
    GameDefine.ConfirmVaultPopup = "ConfirmVaultPopup";
    GameDefine.MissionGuidePopup = "MissionGuidePopup";
    GameDefine.TXMNPromotionPopup = "TXMNPromotionPopup";
    GameDefine.TopupPromotionPopup = "TopupPromotionPopup";
    GameDefine.CardGamePromotionPopup = "CardGamePromotionPopup";
    GameDefine.AFFCupEventPromotionPopup = "AFFCupEventPromotionPopup";
    GameDefine.SpringRotationPromotionPopup = "SpringRotationPromotionPopup";
    GameDefine.LoDeGuidePopup = "LoDeGuidePopup";
    GameDefine.LoDeLeaderBoardPopup = "LoDeLeaderBoardPopup";
    GameDefine.LoDeStatisticPopup = "LoDeStatisticPopup";
    GameDefine.LoDeBetHistoryPopup = "LoDeBetHistoryPopup";
    GameDefine.LoDeBetHistoryDetailPopup = "LoDeBetHistoryDetailPopup";
    GameDefine.ListCsAccountPopup = "ListCsAccountPopup";
    GameDefine.EventGoingPopup = "EventGoingPopup";
    GameDefine.GiftCodeNotifyPopup = "GiftCodeNotifyPopup";
    GameDefine.GiftCodePopup = "GiftCodePopup";
    //#endregion
    //#region WorldCupEvent
    GameDefine.PortalWorldCupEventPopup = "PortalWorldCupEventPopup";
    GameDefine.WorldCupEventPromotionPopup = "WorldCupEventPromotionPopup";
    GameDefine.WorldCupEventPopupGuide = "WorldCupEvent/WCEventPopupGuide";
    GameDefine.WorldCupEventPopupHistory = "WorldCupEvent/WCEventPopupHistory";
    GameDefine.WorldCupEventPopupLeaderboard = "WorldCupEvent/WCEventPopupLeaderboard";
    //#endregion
    //#region AFFCupEvent
    GameDefine.PortalAFFCupEventPopup = "PortalAFFCupEventPopup";
    GameDefine.AFFCupEventPopupGuide = "AFFCupEvent/AFFEventPopupGuide";
    GameDefine.AFFCupEventPopupHistory = "AFFCupEvent/AFFEventPopupHistory";
    GameDefine.AFFCupEventPopupLeaderboard = "AFFCupEvent/AFFEventPopupLeaderboard";
    //#endregion
    //#region AFFCupEvent
    GameDefine.PortalSpringRotationEventPopup = "PortalSpringRotationEventPopup";
    GameDefine.SpringRotationEventPopupGuide = "SpringRotationEvent/SpringRotationEventPopupGuide";
    GameDefine.SpringRotationEventPopupHistory = "SpringRotationEvent/SpringRotationEventPopupHistory";
    //#endregion
    //#region ======== notify ========
    GameDefine.TextNotifyDisconnect = "TextNotifyDisconnect";
    GameDefine.TextNotify = "TextNotify";
    GameDefine.TextBauCuaNotify = "TextBauCuaNotify";
    GameDefine.TextSicboNotify = "TextSicboNotify";
    GameDefine.NotifyClaimAttachmentPopup = "NotifyClaimAttachmentPopup";
    //#endregion
    //#region ======== popup system ========
    GameDefine.ConfirmPopupSystem = "ConfirmPopupSystem";
    GameDefine.LoadingPopupSystem = "LoadingPopupSystem";
    GameDefine.WarningPopupSystem = "WarningPopupSystem";
    GameDefine.RetryPopupSystem = "RetryPopupSystem";
    GameDefine.MaintenacePopupSystem = "MaintenacePopupSystem";
    //#endregion
    ////#region ========MissionGameNamePortal==============
    GameDefine.generalMission = "general";
    GameDefine.bauCuaMission = "baucua";
    GameDefine.tienLenMienNamMission = "tienlenmiennam";
    GameDefine.sicBoMission = "sicbo";
    GameDefine.thanTaiMission = "thantai";
    GameDefine.pokerMission = "poker";
    GameDefine.slotMission = "slot"; //#endregion
    ////#region ========promotionPopupDefine==============
    GameDefine.promotionPopupDefine = [
        "TXMNPromotionPopup",
        "TopupPromotionPopup",
        "CardGamePromotionPopup",
        "WorldCupEventPromotionPopup",
        "AFFCupEventPromotionPopup",
        "SpringRotationPromotionPopup"
    ];
    //#region ======== fight data test ========
    GameDefine.fightTestData = [
        { id: 0, icon: "item_001_2", amount: 5, price: 100, name: "Con Dán", rotate: false },
        { id: 1, icon: "item_002_2", amount: 10, price: 200, name: "Trứng Gà", rotate: false },
        //{ id: 2, icon: "item_003_2", amount: 15, price: 300 },
        { id: 3, icon: "item_004_2", amount: 20, price: 400, name: "Lựu Đạn", rotate: true },
        { id: 4, icon: "item_005_2", amount: 25, price: 500, name: "Trứng Thúi", rotate: true },
        //{ id: 5, icon: "item_006_2", amount: 0, price: 600 },
        { id: 6, icon: "item_007_2", amount: 0, price: 700, name: "Boom", rotate: true },
    ];
    //#end
    //#region ======== gift data test ========
    GameDefine.giftTestData = [
        { id: 0, icon: "giftitems_001", amount: 0, price: 100, name: "Quả Bóng", rotate: true },
        { id: 1, icon: "giftitems_002", amount: 0, price: 200, name: "Bánh Kem", rotate: false },
        { id: 2, icon: "giftitems_003", amount: 0, price: 300, name: "Hộp Sửa", rotate: false },
        { id: 3, icon: "giftitems_004", amount: 0, price: 400, name: "Tách Cafe", rotate: false },
        {
            id: 4,
            icon: "giftitems_005",
            amount: 999,
            price: 500,
            name: "1 Đóa Hồng",
            rotate: false,
        },
    ];
    //#end
    //#region ======== gift data test ========
    GameDefine.tipsTestData = [
        {
            id: 0,
            icon: "dealeritems_001_2",
            amount: 0,
            price: 100,
            name: "Con Gấu",
            rotate: false,
        },
        {
            id: 1,
            icon: "dealeritems_002_2",
            amount: 0,
            price: 200,
            name: "Tách Cafe",
            rotate: false,
        },
        {
            id: 2,
            icon: "dealeritems_003_2",
            amount: 0,
            price: 300,
            name: "Cocktail",
            rotate: false,
        },
        {
            id: 3,
            icon: "dealeritems_004_2",
            amount: 0,
            price: 400,
            name: "Đôi Guốc",
            rotate: false,
        },
        {
            id: 4,
            icon: "dealeritems_005_2",
            amount: 10,
            price: 500,
            name: "Tim Pha Lê",
            rotate: false,
        },
        {
            id: 5,
            icon: "dealeritems_006_2",
            amount: 0,
            price: 600,
            name: "Thỏi Son",
            rotate: false,
        },
        {
            id: 6,
            icon: "dealeritems_007_2",
            amount: 0,
            price: 700,
            name: "Ly Rượu Hồng",
            rotate: false,
        },
        {
            id: 7,
            icon: "dealeritems_008_2",
            amount: 0,
            price: 700,
            name: "1 Đóa Hồng",
            rotate: false,
        },
        {
            id: 8,
            icon: "dealeritems_009_2",
            amount: 0,
            price: 700,
            name: "Bức Thư Tình",
            rotate: false,
        },
        {
            id: 9,
            icon: "dealeritems_0010_2",
            amount: 0,
            price: 700,
            name: "Nụ Hôn",
            rotate: false,
        },
    ];
    return GameDefine;
}());
exports.default = GameDefine;

cc._RF.pop();