"use strict";
cc._RF.push(module, 'b7025zYu0pPu4YaC2FstoFh', 'ScaleEffectBtnComp');
// Game/Lobby/Component/ScaleEffectBtnComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ScaleEffectBtnComp = /** @class */ (function (_super) {
    __extends(ScaleEffectBtnComp, _super);
    function ScaleEffectBtnComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.playOnAwake = true;
        _this.delayFirst = 0;
        return _this;
    }
    ScaleEffectBtnComp.prototype.start = function () {
        var _this = this;
        if (this.playOnAwake) {
            this.scaleEffect();
        }
        this.node.on("click", function () {
            cc.Tween.stopAllByTarget(_this.node);
        }, this);
    };
    ScaleEffectBtnComp.prototype.scaleEffect = function () {
        var duration = 0.2;
        var child = this.node;
        var scale = cc.Vec3.ZERO;
        this.node.getScale(scale);
        var offset = 0.1;
        cc.tween(child)
            .sequence(cc.tween(child).delay(this.delayFirst), cc.tween(child).to(duration, { scaleX: scale.x + offset }), cc.tween(child).parallel(cc.tween(child).to(duration, { scale: scale.x }), cc.tween(child).to(duration, { scaleY: scale.y + offset })), cc.tween(child).to(duration, { scale: scale.x }))
            .repeatForever()
            .start();
    };
    ScaleEffectBtnComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.Boolean)
    ], ScaleEffectBtnComp.prototype, "playOnAwake", void 0);
    __decorate([
        property(cc.Float)
    ], ScaleEffectBtnComp.prototype, "delayFirst", void 0);
    ScaleEffectBtnComp = __decorate([
        ccclass
    ], ScaleEffectBtnComp);
    return ScaleEffectBtnComp;
}(cc.Component));
exports.default = ScaleEffectBtnComp;

cc._RF.pop();