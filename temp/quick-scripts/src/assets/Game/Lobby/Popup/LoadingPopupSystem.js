"use strict";
cc._RF.push(module, 'a43b7LxEXtHeLNnkRjoYdA7', 'LoadingPopupSystem');
// Portal/Common/Scripts/Managers/UIManager/PopupSystem/LoadingPopupSystem.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var PopupSystemInstance_1 = require("../Base/PopupSystemInstance");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var LoadingPopupSystem = /** @class */ (function (_super) {
    __extends(LoadingPopupSystem, _super);
    function LoadingPopupSystem() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoadingPopupSystem.prototype.onShow = function (data) {
        var _this = this;
        // cc.error("show loading popup system");
        if (this.background) {
            this.background.node.active = false;
            cc.Tween.stopAllByTarget(this.background.node);
            cc.tween(this.background.node)
                .delay(1)
                .call(function () {
                _this.background.node.active = true;
            })
                .start();
        }
    };
    LoadingPopupSystem.prototype.afterShow = function () { };
    LoadingPopupSystem.prototype.beforeShow = function () { };
    LoadingPopupSystem.prototype.beforeClose = function () { };
    LoadingPopupSystem.prototype.afterClose = function () { };
    LoadingPopupSystem.prototype.onDisable = function () {
        cc.Tween.stopAllByTarget(this.background.node);
    };
    LoadingPopupSystem = __decorate([
        ccclass
    ], LoadingPopupSystem);
    return LoadingPopupSystem;
}(PopupSystemInstance_1.default));
exports.default = LoadingPopupSystem;

cc._RF.pop();