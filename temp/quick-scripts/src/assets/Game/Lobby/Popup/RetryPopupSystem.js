"use strict";
cc._RF.push(module, 'cd84aa3oT5DMLy5VsQ995UZ', 'RetryPopupSystem');
// Game/Lobby/Popup/RetryPopupSystem.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventManager_1 = require("../Event/EventManager");
var PopupSystemInstance_1 = require("../Base/PopupSystemInstance");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var RetryPopupSystem = /** @class */ (function (_super) {
    __extends(RetryPopupSystem, _super);
    function RetryPopupSystem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.content = null;
        _this.limitReconnectTime = 3;
        _this.timeToReconnect = 3;
        _this.timeDelayConfirm = 3;
        return _this;
    }
    RetryPopupSystem.prototype.onShow = function (data) {
        if (data === void 0) { data = null; }
        this.node.active = true;
    };
    RetryPopupSystem.prototype.onClickCancel = function () {
        cc.log("onClickCancel");
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, 0, false);
        this.onHide();
    };
    RetryPopupSystem.prototype.onClickRetry = function () {
        cc.log("onClickRetry");
        EventManager_1.default.fire(EventManager_1.default.ON_PING_FAIL, 0, true);
        this.onHide();
    };
    RetryPopupSystem.prototype.onHide = function () {
        this.node.active = false;
    };
    RetryPopupSystem.prototype.afterShow = function () { };
    RetryPopupSystem.prototype.beforeShow = function () { };
    RetryPopupSystem.prototype.beforeClose = function () { };
    RetryPopupSystem.prototype.afterClose = function () { };
    __decorate([
        property(cc.Label)
    ], RetryPopupSystem.prototype, "content", void 0);
    __decorate([
        property(cc.Integer)
    ], RetryPopupSystem.prototype, "limitReconnectTime", void 0);
    __decorate([
        property(cc.Integer)
    ], RetryPopupSystem.prototype, "timeToReconnect", void 0);
    __decorate([
        property(cc.Integer)
    ], RetryPopupSystem.prototype, "timeDelayConfirm", void 0);
    RetryPopupSystem = __decorate([
        ccclass
    ], RetryPopupSystem);
    return RetryPopupSystem;
}(PopupSystemInstance_1.default));
exports.default = RetryPopupSystem;

cc._RF.pop();