"use strict";
cc._RF.push(module, 'f8c0cWq5kxIFbdspA8Puffr', 'EventManager');
// Game/Lobby/Manager/EventManager.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var HookEvent_1 = require("./HookEvent");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EventManager = /** @class */ (function () {
    function EventManager() {
    }
    EventManager.getEventCallbackLength = function (evt) {
        if (this.event_mapping[evt] == undefined) {
            return 0;
        }
        return this.event_mapping[evt].length;
    };
    EventManager.on = function (evt, cb, attachedObject, identity) {
        var _this = this;
        if (identity === void 0) { identity = ""; }
        evt += identity != "" ? "_" + identity : "";
        if (this.event_mapping[evt] == undefined || this.event_mapping[evt] == null) {
            this.event_mapping[evt] = [];
        }
        this.event_mapping[evt].push(cb);
        attachedObject &&
            attachedObject.addComponent(HookEvent_1.default).init(function () {
                _this.off(evt, cb);
            });
    };
    EventManager.off = function (evt, callback, identity) {
        if (callback === void 0) { callback = null; }
        if (identity === void 0) { identity = ""; }
        evt += identity != "" ? "_" + identity : "";
        if (this.event_mapping[evt] == undefined) {
            return;
        }
        var cbs = this.event_mapping[evt];
        if (callback != null)
            this.event_mapping[evt] = cbs.filter(function (cb) { return callback != cb; });
        else
            this.event_mapping[evt] = [];
    };
    EventManager.fire = function (evt, args1, args2, args3, args4, args5, identity) {
        if (args1 === void 0) { args1 = null; }
        if (args2 === void 0) { args2 = null; }
        if (args3 === void 0) { args3 = null; }
        if (args4 === void 0) { args4 = null; }
        if (args5 === void 0) { args5 = null; }
        if (identity === void 0) { identity = ""; }
        evt += identity != "" ? "_" + identity : "";
        if (this.event_mapping[evt] == undefined || this.event_mapping[evt] == null) {
            return;
        }
        var cbs = this.event_mapping[evt];
        cbs.forEach(function (cb) { return cb(args1, args2, args3, args4, args5); });
    };
    EventManager.onLogout = "onLogout";
    EventManager.onCloseAllMiniGame = "onCloseAllMiniGame";
    EventManager.onLoginSuccess = "onLoginSuccess";
    EventManager.onGetProfile = "onGetProfile";
    EventManager.onGetProfileDone = "onGetProfileDone";
    EventManager.onChangeProfile = "onChangeProfile";
    EventManager.onHandleEventAfterLoginSuccess = "onHandleEventAfterLoginSuccess";
    EventManager.onCheckCurrentChannel = "onCheckCurrentChannel";
    EventManager.ON_AUTO_LOGIN_FAIL = "OnAutoLogicFail";
    EventManager.ON_HIDE_GAME = "onHideGame";
    EventManager.ON_SHOW_GAME = "onShowGame";
    EventManager.ON_SHOW_RETRY_POPUP = "onShowRetryPopup";
    EventManager.ON_LOAD_JACKPOT = "onLoadJackpot";
    EventManager.ON_CHECK_NEW_MAIL = "checkNewMail";
    EventManager.ON_CHECK_CLAIM_REWARD = "checkClaimReward";
    EventManager.ON_SHOW_ERROR_WHEN_DOMAIN_DIE = "ON_SHOW_ERROR_WHEN_DOMAIN_DIE";
    //Wallet event
    EventManager.onUpdateAddBalance = "onUpdateAddBalance";
    EventManager.onUpdateBalance = "onUpdateBalance";
    // public static onUpdateFakeBalance: string = "onUpdateFakeBalance";
    // Game state event
    EventManager.ON_MAIN_GAME = "OnMainGame";
    EventManager.ON_FREE_GAME = "OnFreeGame";
    EventManager.ON_BONUS_GAME = "OnBonusGame";
    EventManager.ON_BONUS_SPIN_GAME = "OnBonusSpinGame";
    // UI Slot event
    EventManager.ON_PRESS_SPIN = "OnSpin";
    EventManager.ON_FREE_SPIN = "OnFreeSpin";
    EventManager.ON_BONUS_SPIN = "OnBonusSpin";
    EventManager.ON_BONUS_GAME_START = "OnBonusGameStart";
    EventManager.ON_PRESS_STOP_IMMEDIATELY = "OnStopImmediately";
    EventManager.ON_PRESSED_TOGGLE_FAST_SPIN = "OnToggleFastSpin";
    EventManager.ON_PRESSED_INCREASE_BET = "OnIncreaseBet";
    EventManager.ON_PRESSED_DECREASE_BET = "OnDecreaseBet";
    EventManager.ON_PRESSED_INCREASE_LINE = "OnIncreaseLine";
    EventManager.ON_PRESSED_DECREASE_LINE = "OnDecreaseLine";
    EventManager.ON_PRESSED_CLOSE = "OnPressClose";
    EventManager.ON_UPDATE_FREESPIN_COUNT = "OnUpdateFreeSpinCount";
    EventManager.ON_UPDATE_BONUSSPIN_COUNT = "OnUpdateBonusSpinCount";
    EventManager.ON_SHOW_WIN_AMOUNT = "OnShowWinAmount";
    EventManager.ON_SHOW_WALLET_AMOUNT = "OnShowWalletAmount";
    EventManager.ON_TRIGGER_AUTO_SPIN = "OnTriggerAutoSpin";
    EventManager.ON_HIDE_FREE_SPIN = "OnHideFreeSpin";
    EventManager.ON_WALLET_SLOT = "OnWalletSlot";
    // Info events
    EventManager.ON_OPEN_INFO = "OnOpenInfo";
    EventManager.ON_CLOSE_INFO = "OnCloseInfo";
    EventManager.ON_CHANGED_INFO = "OnChangedInfo";
    // History events
    EventManager.ON_OPEN_HISTORY = "OnOpenHistory";
    EventManager.ON_CLOSE_HISTORY = "OnCloseHistory";
    // Mission events
    EventManager.ON_CLOSE_EVENT_POPUP = "OnCloseEventPopup";
    EventManager.ON_SHOW_MISSION = "OnShowMission";
    // JACKPOT History events
    EventManager.ON_OPEN_JACKPOT_HISTORY = "OnOpenJackpotHistory";
    EventManager.ON_CLOSE_JACKPOT_HISTORY = "OnCloseJackpotHistory";
    // Leaderboard detail events
    EventManager.ON_OPEN_LEADERBOARD_DETAIL = "OnOpenLeaderboardDetail";
    EventManager.ON_CLOSE_LEADERBOARD_DETAIL = "OnCloseLeaderboardDetail";
    // Logic events
    EventManager.ON_GAME_START = "OnGameStarted";
    EventManager.ON_GAME_BACK = "OnGameBack";
    EventManager.ON_DOUBLE_START = "OnDoubleStarted";
    EventManager.ON_BET_CHANGED = "OnBetChanged";
    EventManager.ON_LINE_CHANGED = "OnLineChanged";
    EventManager.ON_TOTAL_BET_CHANGED = "OnTotalBetChanged";
    EventManager.ON_RECEIVED_SLOT_DATA = "OnReceivedSlotData";
    EventManager.ON_RECEIVED_BONUS_SPIN_DATA = "OnReceivedBonusSpinData";
    EventManager.ON_HAS_BONUS_SPIN = "OnHasBonusSpin";
    EventManager.ON_FINISH_BONUS_GAME = "OnFinishBonusGame";
    EventManager.ON_RECEIVED_BONUS_DATA = "OnReceivedBonusData";
    EventManager.ON_RECEIVED_BONUS_RESULT_DATA = "OnReceivedBonusResultData";
    EventManager.ON_SLOT_MACHINE_COLUMN_BOUNCE = "OnSlotMachineColumnBounce";
    EventManager.ON_SLOT_MACHINE_STATE_CHANGED = "OnSlotMachineStateChanged";
    EventManager.ON_SLOT_MACHINE_STOPPED = "OnSlotMachineStopped";
    EventManager.ON_SHOW_LINES = "OnShowLines";
    EventManager.ON_END_BONUS_SPIN_TURN = "OnEndBonusSpinTurn"; // Kết thúc hoàn toàn 1 lượt quay bonus
    EventManager.ON_END_SPIN_TURN = "OnEndSpinTurn"; // Kết thúc hoàn toàn 1 lượt quay
    EventManager.ON_END_FREE_SPIN_TURN = "OnEndFreeSpinTurn"; // Kết thúc hoàn toàn 1 lượt quay
    EventManager.ON_JACKPOT_CHANGED = "OnJackpotChanged";
    EventManager.ON_ACTIVATED_TENSION_SPIN = "OnActivatedTensionSpin";
    EventManager.ON_SHOW_TENSION_SPIN = "OnShowTensionSpin";
    EventManager.ON_SHOW_SPECIAL_WIN = "OnShowSpecialWin";
    EventManager.COLUMN_STOPPED = "OnColumnStopped";
    EventManager.ON_SPINABLE = "OnSpinable";
    EventManager.UPDATE_AVATAR_FRAME = "updateAvatarFrame";
    EventManager.HUB_MINI_GAME_LOCATION = "hubMiniGameLocation";
    EventManager.UPDATE_BADGE_LEVEL_UP = "UpdateBadgeLevelUp";
    EventManager.event_mapping = {};
    //WS
    EventManager.ON_RECONNECTED = "OnReconnect";
    EventManager.ON_DISCONNECT = "OnDisconnect";
    EventManager.ON_PING_FAIL = "OnPingFail";
    EventManager.ON_PING_OK = "OnPingOk";
    EventManager.ON_RECONNECTING = "OnReconnecting";
    EventManager.ON_SUBSCRIBE_AMOUNT_TXMN = "ON_SUBSCRIBE_AMOUNT_TXMN";
    // public static ON_SCREEN_CHANGE_SIZE = "OnScreenChangeSize";
    EventManager.ON_CHANGE_WEB_MOBILE_NODE_TO_HIGHEST = "OnChangeWebMobileNodeToHighest";
    EventManager = __decorate([
        ccclass
    ], EventManager);
    return EventManager;
}());
exports.default = EventManager;

cc._RF.pop();