"use strict";
cc._RF.push(module, 'cc34foUFw1BFZHh4LKERe8O', 'LoginScene');
// Game/Lobby/Scene/LoginScene.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BenTauHandler_1 = require("./../Network/BenTauHandler");
var IdentityHandler_1 = require("./../Network/IdentityHandler");
var MyAccountHandler_1 = require("./../Network/MyAccountHandler");
var EventBusManager_1 = require("./../Event/EventBusManager");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var LoginGame = /** @class */ (function (_super) {
    __extends(LoginGame, _super);
    function LoginGame() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoginGame.prototype.start = function () {
        EventBusManager_1.default.onRegisterEvent();
    };
    LoginGame.prototype.onClickLogin = function () {
        this.sendLogin("dev111", "123123");
    };
    LoginGame.prototype.sendLogin = function (userName, password) {
        var self = this;
        IdentityHandler_1.default.getInstance().onLogin(userName, password, function (response) {
            cc.log("onLoginSuccess", response);
            MyAccountHandler_1.default.getInstance().onGetAvatarList(function (res) {
                self.getProfile(function () {
                    if (MyAccountHandler_1.default.getInstance().Playah) {
                        BenTauHandler_1.default.getInstance().init(function () {
                            var GLOBAL_DATA_NAME = "GlobalData";
                            var globalNode = cc.director.getScene().getChildByName(GLOBAL_DATA_NAME);
                            var globalComp = globalNode.getComponent(GLOBAL_DATA_NAME);
                            globalComp.setBundleData();
                            cc.director.loadScene("SicboLoadingScene");
                        });
                    }
                });
            });
        }, null);
    };
    LoginGame.prototype.getProfile = function (onSuccess) {
        if (onSuccess === void 0) { onSuccess = null; }
        var self = this;
        MyAccountHandler_1.default.getInstance().getProfile(function (profile) {
            if (!self)
                return;
            if (onSuccess)
                onSuccess();
        });
    };
    LoginGame = __decorate([
        ccclass
    ], LoginGame);
    return LoginGame;
}(cc.Component));
exports.default = LoginGame;

cc._RF.pop();