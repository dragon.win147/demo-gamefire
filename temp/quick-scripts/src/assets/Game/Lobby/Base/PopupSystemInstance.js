"use strict";
cc._RF.push(module, 'dd9f6nX6PpAzakGwtMF+x/M', 'PopupSystemInstance');
// Portal/Common/Scripts/Managers/UIManager/Base/PopupSystemInstance.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var PopupInstance_1 = require("./PopupInstance");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var PopupSystemInstance = /** @class */ (function (_super) {
    __extends(PopupSystemInstance, _super);
    function PopupSystemInstance() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PopupSystemInstance.prototype.onLoad = function () {
        // if (!this.background) this.background = this.node.getChildByName("darkBackground").getComponent(cc.Widget);
        // if (this.background) {
        //   this.background.isAlignLeft = true;
        //   this.background.isAbsoluteRight = true;
        //   this.background.isAbsoluteTop = true;
        //   this.background.isAlignBottom = true;
        //   this.background.left = -500;
        //   this.background.top = -500;
        //   this.background.bottom = -500;
        //   this.background.right = -500;
        //   this.background.target = cc.director.getScene().getChildByName("Canvas");
        // }
        // this._animation = this.getComponent(cc.Animation);
        this.node.active = false;
    };
    PopupSystemInstance = __decorate([
        ccclass
    ], PopupSystemInstance);
    return PopupSystemInstance;
}(PopupInstance_1.default));
exports.default = PopupSystemInstance;

cc._RF.pop();