"use strict";
cc._RF.push(module, '7cc4fAeojdCYrZ0bU7shopQ', 'UIManager');
// Game/Lobby/Manager/UIManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocalBundlePreload = exports.BundlePreloadData = exports.PopupSystemConfigData = exports.PopupConfigData = void 0;
var PopupConfigData = /** @class */ (function () {
    function PopupConfigData(type, name) {
        this.popupType = type;
        this.popupName = name;
    }
    return PopupConfigData;
}());
exports.PopupConfigData = PopupConfigData;
var PopupSystemConfigData = /** @class */ (function () {
    function PopupSystemConfigData(type, name) {
        this.popupType = type;
        this.popupName = name;
    }
    return PopupSystemConfigData;
}());
exports.PopupSystemConfigData = PopupSystemConfigData;
var BundlePreloadData = /** @class */ (function () {
    function BundlePreloadData(topicConst, listPaths) {
        this.topicConst = -1;
        this.listPopupPaths = [];
        this.topicConst = topicConst;
        this.listPopupPaths = listPaths;
    }
    return BundlePreloadData;
}());
exports.BundlePreloadData = BundlePreloadData;
var LocalBundlePreload = /** @class */ (function () {
    function LocalBundlePreload(name) {
        this.bundleName = name;
    }
    return LocalBundlePreload;
}());
exports.LocalBundlePreload = LocalBundlePreload;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIManager = /** @class */ (function (_super) {
    __extends(UIManager, _super);
    function UIManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._persisNotifyRoot = null;
        _this._persistPopupSystemRoot = null;
        _this._persistPopupRoot = null;
        _this.paymentTopUpNode = null;
        _this.paymentWithDrawNode = null;
        _this.screenTran = null;
        _this._screenInstances = new Map();
        _this.elementTran = null;
        _this._elementInstances = new Map();
        _this.popupTran = null;
        _this._popupInstances = new Map();
        _this.notifyTran = null;
        _this._notifyInstances = new Map();
        _this.popupSystemTran = null;
        _this._currentLoading = 0;
        _this._popupSystemInstances = new Map();
        _this.textTempParent = null;
        _this.uiTempParent = null;
        _this.effTempParent = null;
        return _this;
    }
    UIManager_1 = UIManager;
    UIManager.getInstance = function () {
        if (!UIManager_1._instance) {
            var node = new cc.Node("UIManager");
            UIManager_1._instance = node.addComponent(UIManager_1);
            var self = UIManager_1._instance;
            var canvas = cc.director.getScene().getChildByName("Canvas");
            var size = canvas.getContentSize();
            cc.game.addPersistRootNode(self.node);
            self.node.zIndex = LayerHelper_1.default.uiManager;
            //self.setWidget(self.node, self.node.parent);
            // cc.log("UIManager");
            self._persistPopupRoot = new cc.Node("Popup");
            self._persistPopupRoot.setContentSize(size);
            self._persistPopupRoot.setParent(self.node);
            self._persistPopupRoot.setPosition(cc.Vec3.ZERO);
            self.setWidget(self._persistPopupRoot, self.node);
            self._persisNotifyRoot = new cc.Node("Notify");
            self._persisNotifyRoot.setContentSize(size);
            self._persisNotifyRoot.setParent(self.node);
            self._persisNotifyRoot.setPosition(cc.Vec3.ZERO);
            self.setWidget(self._persisNotifyRoot, self.node);
            self._persistPopupSystemRoot = new cc.Node("PopupSystem");
            self._persistPopupSystemRoot.setContentSize(size);
            self._persistPopupSystemRoot.setParent(self.node);
            self._persistPopupSystemRoot.setPosition(cc.Vec3.ZERO);
            self.setWidget(self._persistPopupSystemRoot, self.node);
            // EventManager.on(
            //   EventManager.ON_SCREEN_CHANGE_SIZE,
            //   () => {
            //     self.updateSize();
            //   },
            //   self
            // );
            cc.systemEvent.on(EventKey_1.default.ON_SCREEN_CHANGE_SIZE_EVENT, self.updateSize, self);
        }
        return UIManager_1._instance;
    };
    UIManager.prototype.onDestroy = function () {
        cc.systemEvent.off(EventKey_1.default.ON_SCREEN_CHANGE_SIZE_EVENT, this.updateSize, this);
    };
    UIManager.prototype.updateSize = function () {
        var self = UIManager_1._instance;
        if (self && self.node) {
            var canvas = cc.director.getScene().getChildByName("Canvas");
            if (canvas) {
                var size = canvas.getContentSize();
                self.node.setContentSize(size);
                self.node.setPosition(canvas.getPosition());
                self._persistPopupRoot.setContentSize(size);
                self._persistPopupRoot.setParent(self.node);
                self._persistPopupRoot.setPosition(cc.Vec3.ZERO);
                self._persisNotifyRoot.setContentSize(size);
                self._persisNotifyRoot.setParent(self.node);
                self._persisNotifyRoot.setPosition(cc.Vec3.ZERO);
                self._persistPopupSystemRoot.setContentSize(size);
                self._persistPopupSystemRoot.setParent(self.node);
                self._persistPopupSystemRoot.setPosition(cc.Vec3.ZERO);
            }
        }
    };
    UIManager.prototype.setWidget = function (node, parent) {
        var widget = node.getComponent(cc.Widget);
        if (widget == null)
            widget = node.addComponent(cc.Widget);
        widget.target = parent;
        widget.isAlignLeft = true;
        widget.isAbsoluteRight = true;
        widget.isAbsoluteTop = true;
        widget.isAlignBottom = true;
        widget.left = 0;
        widget.top = 0;
        widget.bottom = 0;
        widget.right = 0;
    };
    Object.defineProperty(UIManager.prototype, "PaymentTopUpObject", {
        get: function () {
            return this.paymentTopUpNode;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "PaymentWithDrawObject", {
        get: function () {
            return this.paymentWithDrawNode;
        },
        enumerable: false,
        configurable: true
    });
    UIManager.prototype.setPaymentTopUpObject = function (node) {
        this.paymentTopUpNode = node;
    };
    UIManager.prototype.setPaymentWithDrawObject = function (node) {
        this.paymentWithDrawNode = node;
    };
    Object.defineProperty(UIManager.prototype, "LayerUpPopup", {
        get: function () {
            return this._persistPopupRoot;
        },
        enumerable: false,
        configurable: true
    });
    UIManager.prototype.init = function () {
        // cc.log("UIManager...... init");
    };
    Object.defineProperty(UIManager.prototype, "currentScreen", {
        get: function () {
            return this._currentScreen;
        },
        set: function (value) {
            this._currentScreen = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "currentElement", {
        get: function () {
            return this._currentHeader;
        },
        set: function (value) {
            this._currentHeader = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "currentPopup", {
        get: function () {
            return this._currentPopup;
        },
        set: function (value) {
            this._currentPopup = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "currentNotify", {
        get: function () {
            return this._currentNotify;
        },
        set: function (value) {
            this._currentNotify = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "currentPopupSystem", {
        get: function () {
            return this._currentPopupSystem;
        },
        set: function (value) {
            this._currentPopupSystem = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "currentPopupSystemName", {
        get: function () {
            return this._currentPopupSystemName;
        },
        enumerable: false,
        configurable: true
    });
    UIManager.prototype.openElement = function (type, name, data, parent, onComplete) {
        var _this = this;
        if (data === void 0) { data = null; }
        if (parent === void 0) { parent = null; }
        if (onComplete === void 0) { onComplete = null; }
        var self = UIManager_1._instance;
        self._currentElementName = name;
        parent = parent ? parent : self.elementTran;
        self.getElementInstance(type, name, function (instance) {
            if (self.currentElement == instance) {
                return self.currentElement;
            }
            self.currentElement = instance;
            instance.open(data);
            instance._close = function () {
                self.currentElement = null;
                _this._currentElementName = "";
            };
            if (onComplete)
                onComplete(instance);
        }, parent);
    };
    UIManager.prototype.closeElement = function (type, name) {
        var self = UIManager_1._instance;
        self.getElementInstance(type, name, function (instance) {
            instance.close();
        });
    };
    UIManager.prototype.getElementInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        if (!self._elementInstances)
            self._elementInstances = new Map();
        if (self._elementInstances.has(name) && self._elementInstances.get(name) != null && self._elementInstances.get(name).node != null) {
            var element = self._elementInstances.get(name);
            element.node.parent = null;
            _parent.addChild(element.node);
            onComplete(element);
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/Element/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                _parent.addChild(newNode);
                self._elementInstances.set(name, instance);
                onComplete(instance);
            });
        }
    };
    UIManager.prototype.openScreen = function (type, name, data, parent) {
        if (data === void 0) { data = null; }
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        parent = parent ? parent : self.screenTran;
        self.getSceneInstance(type, name, function (instance) {
            if (self.currentScreen != null) {
                self.currentScreen.close();
            }
            self.currentScreen = instance;
            instance.open(data);
        }, parent);
    };
    UIManager.prototype.getSceneInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        if (!self._screenInstances)
            self._screenInstances = new Map();
        if (self._screenInstances.has(name) && self._screenInstances.get(name) != null && self._screenInstances.get(name).node != null) {
            var scene = self._screenInstances.get(name);
            scene.node.parent = null;
            _parent.addChild(scene.node);
            onComplete(scene);
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/Screen/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                _parent.addChild(newNode);
                self._screenInstances.set(name, instance);
                onComplete(instance);
            });
        }
    };
    UIManager.prototype.preloadListPopup = function (listPopups, onProgress, onComplete) {
        var _this = this;
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (listPopups.length == 0) {
            if (onComplete)
                onComplete();
            return;
        }
        var finish = 0;
        var total = listPopups.length;
        listPopups.forEach(function (data) {
            _this.preLoadPopup(data.popupType, data.popupName, function (err, item) {
                if (err) {
                    // cc.log(err);
                }
                finish++;
                if (onProgress) {
                    onProgress(finish, total, item);
                }
                if (finish == total) {
                    if (onComplete)
                        onComplete();
                }
            });
        });
    };
    UIManager.prototype.preLoadPopup = function (type, name, callback) {
        if (callback === void 0) { callback = null; }
        var self = UIManager_1._instance;
        if (!self._popupInstances)
            self._popupInstances = new Map();
        if (self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null) {
            if (callback) {
                callback(null, self._popupInstances.get(name));
            }
            return;
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/Popup/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                self._popupInstances.set(name, instance);
                if (callback) {
                    callback(null, self._popupInstances.get(name));
                }
            });
        }
    };
    UIManager.prototype.preloadListPopupSystem = function (listPopups, onProgress, onComplete) {
        var _this = this;
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (listPopups.length == 0) {
            if (onComplete)
                onComplete();
            return;
        }
        var finish = 0;
        var total = listPopups.length;
        listPopups.forEach(function (data) {
            _this.preLoadPopupSystem(data.popupType, data.popupName, function (err, item) {
                if (err) {
                    // cc.log(err);
                }
                finish++;
                if (onProgress) {
                    onProgress(finish, total, item);
                }
                if (finish == total) {
                    if (onComplete)
                        onComplete();
                }
            });
        });
    };
    UIManager.prototype.preLoadPopupSystem = function (type, name, callback) {
        if (callback === void 0) { callback = null; }
        var self = UIManager_1._instance;
        if (!self._popupSystemInstances)
            self._popupSystemInstances = new Map();
        if (self._popupSystemInstances.has(name) && self._popupSystemInstances.get(name) != null && self._popupSystemInstances.get(name).node != null) {
            if (callback) {
                callback(null, self._popupSystemInstances.get(name));
            }
            return;
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/PopupSystem/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                self._popupSystemInstances.set(name, instance);
                if (callback) {
                    callback(null, self._popupSystemInstances.get(name));
                }
            });
        }
    };
    UIManager.prototype.openPopup = function (type, name, data, onComplete, parent, showLoadingPopUp) {
        if (data === void 0) { data = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (parent === void 0) { parent = null; }
        if (showLoadingPopUp === void 0) { showLoadingPopUp = true; }
        var self = UIManager_1._instance;
        parent = parent ? parent : self.popupTran;
        if (name == self._currentPopupName && self._currentPopup.node) {
            if (onComplete)
                onComplete(self._currentPopup);
            return;
        }
        self._currentPopupName = name;
        self.getPopupInstance(type, name, function (instance) {
            if (self.currentPopup == instance) {
                if (onComplete)
                    onComplete(self.currentPopup);
                return;
            }
            self.currentPopup = instance;
            instance.open(data);
            instance._close = function () {
                self.currentPopup = null;
                self._currentPopupName = "";
            };
            if (onComplete)
                onComplete(self.currentPopup);
        }, parent, showLoadingPopUp);
    };
    UIManager.prototype.openPopupWithErrorHandler = function (type, name, data, parent, showLoadingPopUp) {
        if (data === void 0) { data = null; }
        if (parent === void 0) { parent = null; }
        if (showLoadingPopUp === void 0) { showLoadingPopUp = true; }
        var self = UIManager_1._instance;
        parent = parent ? parent : self.popupTran;
        if (name == self._currentPopupName && self._currentPopup != null && self._currentPopup.node) {
            return new Promise(function (resolve) {
                resolve(self._currentPopup);
            });
        }
        self._currentPopupName = name;
        return self
            .getPopupInstanceWithError(type, name, parent, showLoadingPopUp)
            .then(function (instance) {
            if (self.currentPopup == instance) {
                return new Promise(function (resolve) {
                    resolve(self.currentPopup);
                });
            }
            self.currentPopup = instance;
            instance.open(data);
            instance._close = function () {
                self.currentPopup = null;
                self._currentPopupName = "";
            };
            return instance;
        })
            .catch(function (error) {
            if (showLoadingPopUp) {
                self._currentPopupName = "";
                UIManager_1.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
            }
            // UIManager.getInstance().openPopupSystem(WarningPopupSystem, GameDefine.WarningPopupSystem, PortalText.POR_MESS_NETWORK_DISCONNECT);
        });
    };
    UIManager.prototype.hasPopupInstance = function (name) {
        return UIManager_1._instance._popupInstances.has(name);
    };
    UIManager.prototype.closePopup = function (type, name) {
        var self = UIManager_1._instance;
        self.getPopupInstance(type, name, function (instance) {
            instance.closeInstance();
        });
    };
    UIManager.prototype.getPopupInstance = function (type, name, onComplete, parent, showLoadingPopUp) {
        var _this = this;
        if (parent === void 0) { parent = null; }
        if (showLoadingPopUp === void 0) { showLoadingPopUp = true; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        if (!self._popupInstances)
            self._popupInstances = new Map();
        if (self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null) {
            var popup = self._popupInstances.get(name);
            popup.node.parent = null;
            _parent.addChild(popup.node);
            onComplete(popup);
        }
        else {
            if (showLoadingPopUp) {
                self.openPopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, true, null, null, function (popupLoading) {
                    _this.loadPopUpPrefab(type, name, onComplete, _parent);
                });
            }
            else {
                this.loadPopUpPrefab(type, name, onComplete, _parent);
            }
        }
    };
    UIManager.prototype.getPopupInstanceWithError = function (type, name, parent, showLoadingPopUp) {
        if (parent === void 0) { parent = null; }
        if (showLoadingPopUp === void 0) { showLoadingPopUp = true; }
        return __awaiter(this, void 0, void 0, function () {
            var self, _parent, popup_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        self = UIManager_1._instance;
                        _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
                        if (!self._popupInstances)
                            self._popupInstances = new Map();
                        if (!(self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null)) return [3 /*break*/, 1];
                        popup_1 = self._popupInstances.get(name);
                        popup_1.node.parent = null;
                        _parent.addChild(popup_1.node);
                        return [2 /*return*/, new Promise(function (resolve) {
                                resolve(popup_1);
                            })];
                    case 1:
                        if (!showLoadingPopUp) return [3 /*break*/, 3];
                        return [4 /*yield*/, self.openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, true, null, null)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, this.loadPopUpPrefabWithErrorHandler(type, name, _parent)];
                    case 3: return [2 /*return*/, this.loadPopUpPrefabWithErrorHandler(type, name, _parent)];
                }
            });
        });
    };
    UIManager.prototype.loadPopUpPrefab = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        ResourceManager_1.default.loadPrefab("Prefabs/Popup/" + name, function (prefab) {
            var newNode = cc.instantiate(prefab);
            var instance = newNode.getComponent(type);
            parent.addChild(newNode);
            self._popupInstances.set(name, instance);
            self.closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
            onComplete(instance);
        });
    };
    UIManager.prototype.loadPopUpPrefabWithErrorHandler = function (type, name, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        return ResourceManager_1.default.loadPrefabWithErrorHandler("Prefabs/Popup/" + name).then(function (prefab) {
            var newNode = cc.instantiate(prefab);
            var instance = newNode.getComponent(type);
            parent.addChild(newNode);
            self._popupInstances.set(name, instance);
            self.closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
            return instance;
        });
    };
    UIManager.prototype.openNotify = function (type, name, data, parent) {
        if (data === void 0) { data = null; }
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        parent = parent ? parent : self.notifyTran;
        if (name == self._currentNotifyName)
            return;
        self._currentNotifyName = name;
        self.getNotifyInstance(type, name, function (instance) {
            if (self.currentNotify == instance)
                return;
            self.currentNotify = instance;
            instance.open(data);
            instance._close = function () {
                self.currentNotify = null;
                self._currentNotifyName = "";
            };
        }, parent);
    };
    UIManager.prototype.getNotifyInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        if (name == GameDefine_1.default.TextNotify)
            _parent = self._persisNotifyRoot;
        if (!self._notifyInstances)
            self._notifyInstances = new Map();
        if (self._notifyInstances.has(name) && self._notifyInstances.get(name) != null && self._notifyInstances.get(name).node != null) {
            var notify = self._notifyInstances.get(name);
            if (name == GameDefine_1.default.TextNotify) {
            }
            else {
                notify.node.parent = null;
                _parent.addChild(notify.node);
            }
            onComplete(notify);
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/Notify/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                _parent.addChild(newNode);
                self._notifyInstances.set(name, instance);
                onComplete(instance);
            });
        }
    };
    UIManager.prototype.openPopupSystem = function (type, name, data, onYes, onNo, onComplete, parent) {
        if (data === void 0) { data = null; }
        if (onYes === void 0) { onYes = null; }
        if (onNo === void 0) { onNo = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        // self.popupSystemTran = cc.director.getScene().getComponentInChildren(MiniGameManager)?.popupSystemTran;
        parent = parent ? parent : self.popupSystemTran;
        if (name == GameDefine_1.default.LoadingPopupSystem) {
            self._currentLoading++;
        }
        if (name == self._currentPopupSystemName) {
            return;
        }
        if (self._currentPopupSystem && self._currentPopupSystem.node) {
            if (onComplete)
                onComplete(self._currentPopupSystem);
            return;
        }
        self._currentPopupSystemName = name;
        self.getPopupSystemInstance(type, name, function (instance) {
            if (self._currentPopupSystem == instance) {
                if (onComplete)
                    onComplete(self._currentPopupSystem);
                return;
            }
            self._currentPopupSystem = instance;
            instance.open(data, onYes, onNo);
            instance._close = function () {
                self._currentPopupSystem = null;
                self._currentPopupSystemName = "";
            };
            if (onComplete)
                onComplete(instance);
        }, parent);
    };
    UIManager.prototype.openPopupSystemV2 = function (type, name, data, onYes, onNo, parent) {
        if (data === void 0) { data = null; }
        if (onYes === void 0) { onYes = null; }
        if (onNo === void 0) { onNo = null; }
        if (parent === void 0) { parent = null; }
        return new Promise(function (resolve) {
            var self = UIManager_1._instance;
            // self.popupSystemTran = cc.director.getScene().getComponentInChildren(MiniGameManager)?.popupSystemTran;
            parent = parent ? parent : self.popupSystemTran;
            if (name == GameDefine_1.default.LoadingPopupSystem) {
                self._currentLoading++;
            }
            if (name == self._currentPopupSystemName && self._currentPopupSystem && self._currentPopupSystem.node) {
                resolve(self._currentPopupSystem);
                return;
            }
            self._currentPopupSystemName = name;
            self.getPopupSystemInstance(type, name, function (instance) {
                if (self._currentPopupSystem == instance) {
                    resolve(self._currentPopupSystem);
                    return;
                }
                self._currentPopupSystem = instance;
                instance.open(data, onYes, onNo);
                instance._close = function () {
                    self._currentPopupSystem = null;
                    self._currentPopupSystemName = "";
                };
                resolve(instance);
            }, parent);
        });
    };
    UIManager.prototype.closeAllPopupSystem = function () {
        var self = UIManager_1._instance;
        self._popupSystemInstances.forEach(function (p) {
            if (p != null && p.node != null)
                p.closeInstance();
            self._currentLoading = 0;
            self._currentPopupSystemName = "";
        });
    };
    UIManager.prototype.closeAllPopup = function (shouldCloseProfilePopup) {
        if (shouldCloseProfilePopup === void 0) { shouldCloseProfilePopup = true; }
        var self = UIManager_1._instance;
        self._popupInstances.forEach(function (p, name) {
            if (!shouldCloseProfilePopup) {
            }
            if (p != null && p.node != null)
                p.closeInstance();
            self._currentPopupName = "";
        });
        self.closeAllPopupSystem();
    };
    UIManager.prototype.closePaymentPopup = function () {
        if (this.paymentWithDrawNode) {
            if (this.paymentWithDrawNode.active)
                this.paymentWithDrawNode.active = false;
        }
        if (this.paymentTopUpNode) {
            if (this.paymentTopUpNode.active)
                this.paymentTopUpNode.active = false;
        }
    };
    UIManager.prototype.closePopupSystem = function (type, name) {
        var self = UIManager_1._instance;
        if (name == GameDefine_1.default.LoadingPopupSystem) {
            self._currentLoading--;
            if (self._currentLoading < 0)
                self._currentLoading = 0;
        }
        self.getPopupSystemInstance(type, name, function (instance) {
            if (self._currentLoading == 0) {
                instance.closeInstance();
                self._currentPopupSystemName = "";
            }
        });
    };
    UIManager.prototype.getPopupSystemInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        _parent = self._persistPopupSystemRoot;
        if (!self._popupSystemInstances)
            self._popupSystemInstances = new Map();
        if (self._popupSystemInstances.has(name) && self._popupSystemInstances.get(name) != null && self._popupSystemInstances.get(name).node != null) {
            var popup = self._popupSystemInstances.get(name);
            popup.node.parent = null;
            _parent.addChild(popup.node);
            onComplete(popup);
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/PopupSystem/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                _parent.addChild(newNode);
                self._popupSystemInstances.set(name, instance);
                onComplete(instance);
            });
        }
    };
    UIManager.prototype.openActionElementPopup = function (type, name, data, onComplete, parent) {
        if (data === void 0) { data = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (parent === void 0) { parent = null; }
        // cc.log("open action popup");
        var self = UIManager_1._instance;
        parent = parent ? parent : self.popupTran;
        // if (name == self._currentPopupName) {
        //   if (onComplete) onComplete(self._currentPopup);
        //   return;
        // }
        self._currentPopupName = name;
        self.getActionElementPopupInstance(type, name, function (instance) {
            if (self.currentPopup == instance) {
                if (onComplete)
                    onComplete(self.currentPopup);
                return;
            }
            self.currentPopup = instance;
            instance.open(data);
            instance._close = function () {
                self.currentPopup = null;
                self._currentPopupName = "";
            };
            if (onComplete)
                onComplete(self.currentPopup);
        }, parent);
    };
    UIManager.prototype.closeActionElementPopup = function (type, name) {
        var self = UIManager_1._instance;
        self.getActionElementPopupInstance(type, name, function (instance) {
            instance.closeInstance();
        });
    };
    UIManager.prototype.getActionElementPopupInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        if (!self._popupInstances)
            self._popupInstances = new Map();
        if (self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null) {
            var popup = self._popupInstances.get(name);
            popup.node.parent = null;
            _parent.addChild(popup.node);
            onComplete(popup);
        }
        else {
            // self.openPopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem, true, null, null, (popupLoading) => {
            ResourceManager_1.default.loadPrefab("Prefabs/Popup/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                _parent.addChild(newNode);
                self._popupInstances.set(name, instance);
                // self.closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
                onComplete(instance);
            });
            // });
        }
    };
    UIManager.prototype.setParentText = function (node, parent) {
        if (!this.textTempParent) {
            this.textTempParent = new cc.Node("textTemp");
        }
        else {
            this.textTempParent = new cc.Node("textTemp");
        }
    };
    UIManager.prototype.setParentUI = function (node) {
        var parent = new cc.Node("ui");
    };
    UIManager.prototype.setParentEff = function (node) {
        var parent = new cc.Node("eff");
    };
    var UIManager_1;
    __decorate([
        property(cc.Node)
    ], UIManager.prototype, "screenTran", void 0);
    __decorate([
        property(cc.Node)
    ], UIManager.prototype, "elementTran", void 0);
    __decorate([
        property(cc.Node)
    ], UIManager.prototype, "popupTran", void 0);
    __decorate([
        property(cc.Node)
    ], UIManager.prototype, "notifyTran", void 0);
    __decorate([
        property(cc.Node)
    ], UIManager.prototype, "popupSystemTran", void 0);
    UIManager = UIManager_1 = __decorate([
        ccclass
    ], UIManager);
    return UIManager;
}(cc.Component));
exports.default = UIManager;
var GameDefine_1 = require("./../Define/GameDefine");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var LayerHelper_1 = require("../Utils/LayerHelper");
var ResourceManager_1 = require("./ResourceManager");
var EventKey_1 = require("@gameloot/client-base-handler/build/EventKey");

cc._RF.pop();