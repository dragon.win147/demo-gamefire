"use strict";
cc._RF.push(module, '19421DZWcFOwo3CKvx1snzI', 'EnumGame');
// Portal/Common/Scripts/Enums/EnumGame.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeSocket = exports.SoundCard = exports.TypeChatHeo = exports.TypeLoadingChangeScene = exports.typeScene = exports.GameType = void 0;
var GameType;
(function (GameType) {
    GameType[GameType["LandingPage"] = 0] = "LandingPage";
    GameType[GameType["BauCua"] = 1] = "BauCua";
    GameType[GameType["TLMN"] = 2] = "TLMN";
    GameType[GameType["Sicbo"] = 3] = "Sicbo";
    GameType[GameType["TienLenMN"] = 4] = "TienLenMN";
    GameType[GameType["Tala"] = 6] = "Tala";
    GameType[GameType["Fishing"] = 7] = "Fishing";
    GameType[GameType["ThanTai"] = 8] = "ThanTai";
    GameType[GameType["Loading"] = 9] = "Loading";
    GameType[GameType["ChienThan"] = 10] = "ChienThan";
    GameType[GameType["MauBinh"] = 11] = "MauBinh";
    GameType[GameType["XocDia"] = 12] = "XocDia";
    GameType[GameType["LoDe"] = 13] = "LoDe";
    GameType[GameType["Poker"] = 14] = "Poker";
})(GameType = exports.GameType || (exports.GameType = {}));
var typeScene;
(function (typeScene) {
    typeScene[typeScene["release"] = 0] = "release";
    typeScene[typeScene["lobby"] = 1] = "lobby";
    typeScene[typeScene["loadingScene"] = 2] = "loadingScene";
    typeScene[typeScene["home"] = 3] = "home";
})(typeScene = exports.typeScene || (exports.typeScene = {}));
var TypeLoadingChangeScene;
(function (TypeLoadingChangeScene) {
    TypeLoadingChangeScene[TypeLoadingChangeScene["NORMAL"] = 0] = "NORMAL";
    TypeLoadingChangeScene[TypeLoadingChangeScene["SLOT"] = 1] = "SLOT";
    TypeLoadingChangeScene[TypeLoadingChangeScene["BET"] = 2] = "BET";
    TypeLoadingChangeScene[TypeLoadingChangeScene["CARD"] = 3] = "CARD";
})(TypeLoadingChangeScene = exports.TypeLoadingChangeScene || (exports.TypeLoadingChangeScene = {}));
var TypeChatHeo;
(function (TypeChatHeo) {
    TypeChatHeo[TypeChatHeo["NONE"] = -1] = "NONE";
    TypeChatHeo[TypeChatHeo["HEO"] = 0] = "HEO";
    TypeChatHeo[TypeChatHeo["CHAT_HEO"] = 1] = "CHAT_HEO";
    TypeChatHeo[TypeChatHeo["CHAT_DOI_HEO"] = 2] = "CHAT_DOI_HEO";
    TypeChatHeo[TypeChatHeo["DOI_HEO_DE"] = 3] = "DOI_HEO_DE";
    TypeChatHeo[TypeChatHeo["DE_DOI_HEO"] = 4] = "DE_DOI_HEO";
    TypeChatHeo[TypeChatHeo["DOI_HEO"] = 5] = "DOI_HEO";
    TypeChatHeo[TypeChatHeo["BA_HEO"] = 6] = "BA_HEO";
    TypeChatHeo[TypeChatHeo["BA_DOI_THONG"] = 7] = "BA_DOI_THONG";
    TypeChatHeo[TypeChatHeo["BA_DOI_THONG_DE"] = 8] = "BA_DOI_THONG_DE";
    TypeChatHeo[TypeChatHeo["DE_BA_DOI_THONG"] = 9] = "DE_BA_DOI_THONG";
    TypeChatHeo[TypeChatHeo["CHAT_BA_DOI_THONG"] = 10] = "CHAT_BA_DOI_THONG";
    TypeChatHeo[TypeChatHeo["TU_QUY"] = 11] = "TU_QUY";
    TypeChatHeo[TypeChatHeo["TU_QUY_DE"] = 12] = "TU_QUY_DE";
    TypeChatHeo[TypeChatHeo["DE_TU_QUY"] = 13] = "DE_TU_QUY";
    TypeChatHeo[TypeChatHeo["CHAT_TU_QUY"] = 14] = "CHAT_TU_QUY";
    TypeChatHeo[TypeChatHeo["BON_DOI_THONG"] = 15] = "BON_DOI_THONG";
    TypeChatHeo[TypeChatHeo["BON_DOI_THONG_DE"] = 16] = "BON_DOI_THONG_DE";
    TypeChatHeo[TypeChatHeo["DE_BON_DOI_THONG"] = 17] = "DE_BON_DOI_THONG";
})(TypeChatHeo = exports.TypeChatHeo || (exports.TypeChatHeo = {}));
var SoundCard;
(function (SoundCard) {
    SoundCard[SoundCard["DEAL"] = 0] = "DEAL";
    SoundCard[SoundCard["HIT"] = 1] = "HIT";
    SoundCard[SoundCard["CHAT"] = 2] = "CHAT";
    SoundCard[SoundCard["BI_CHAT"] = 3] = "BI_CHAT";
    SoundCard[SoundCard["WIN"] = 4] = "WIN";
    SoundCard[SoundCard["LOSE"] = 5] = "LOSE";
    SoundCard[SoundCard["REFUND"] = 6] = "REFUND";
    SoundCard[SoundCard["LOST_MONEY"] = 7] = "LOST_MONEY";
    SoundCard[SoundCard["MONEY_CUT_PIG"] = 8] = "MONEY_CUT_PIG";
    SoundCard[SoundCard["AUTO_WIN"] = 9] = "AUTO_WIN";
    SoundCard[SoundCard["AUTO_LOSE"] = 10] = "AUTO_LOSE";
})(SoundCard = exports.SoundCard || (exports.SoundCard = {}));
var CodeSocket;
(function (CodeSocket) {
    CodeSocket[CodeSocket["FORCE_CLOSE"] = 4999] = "FORCE_CLOSE";
    CodeSocket[CodeSocket["KOTIC"] = 1001] = "KOTIC";
    CodeSocket[CodeSocket["AUTO_CLOSE"] = 1000] = "AUTO_CLOSE";
    CodeSocket[CodeSocket["RETRY"] = 3006] = "RETRY";
})(CodeSocket = exports.CodeSocket || (exports.CodeSocket = {}));

cc._RF.pop();