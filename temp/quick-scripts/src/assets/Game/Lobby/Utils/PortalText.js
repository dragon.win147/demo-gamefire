"use strict";
cc._RF.push(module, '2a95b/HnkdG/aPK/9JktXes', 'PortalText');
// Portal/Portal/Scripts/Defines/PortalText.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var PortalText = /** @class */ (function () {
    function PortalText() {
    }
    PortalText.POR_ACCEPTFRIEND_LEVEL = "Level: {0}";
    PortalText.POR_HEADER_LV = "Lv.{0}";
    PortalText.POR_GLOBALINVITE_RECEIVE = "Quý Khách nhận được lời mời tham gia game <color=#ffd630>{0}\n<color=#ff6c6c>Mức cược: <color=#ffd630>{1}</color>";
    PortalText.POR_GLOBALINVITE_RECEIVE_2 = "<color=#ffd630>{0}\n<color=#ff6c6c>Mức cược: <color=#ffd630>{1}</color>";
    PortalText.POR_GLOBALINVITE_REJECT = "Đã từ chối tham gia bàn chơi";
    PortalText.POR_GLOBALINVITE_ACCEPT = '"Đã chấp nhận tham gia bàn chơi, hệ thống đang chuyển hướng xin vui lòng chờ!';
    PortalText.POR_SETTING_VERSION = "Version {0}";
    PortalText.POR_LOADING_SCENE = "Loading...{0}%";
    // public static POR_FANPAGE_URL = "https://www.facebook.com/pokerZPlay";
    // public static POR_TELEGRAM_URL = "https://t.me/pokerist_club";
    // public static POR_ZALO_URL = "https://zalo.me/g/ziaiqd372";
    // public static POR_LIVECHAT_URL = "https://secure.livechatinc.com/licence/12444771/v2/open_chat.cgi";
    // public static POR_HOTLINE_URL = "tel:+84985972703";
    // public static POR_HOTLINE_TEXT = "Xin vui lòng liên lạc đến Hotline +84985972703";
    PortalText.POR_SHARE_FACEBOOK_URL = "https://www.facebook.com/sharer/sharer.php?u={0}";
    PortalText.POR_SHARE_TWITTER_URL = "http://www.twitter.com/share?url={0}&text=alo alo";
    //#region Popup Quest
    PortalText.POR_QUEST_ACHIEVEMENT_NOTIFY = "Thành Tựu chỉ hoàn thành được 1 lần trong suốt tiến trình chơi";
    PortalText.POR_QUEST_DAILY_NOTIFY = "Nhiệm vụ hàng ngày sẽ được làm mới mỗi ngày vào lúc 00:00";
    PortalText.POR_QUEST_WEEKLY_NOTIFY = "Nhiệm vụ hàng tuần sẽ được làm mới mỗi tuần vào lúc 00:00 ngày Chủ nhật";
    //#endregion
    //#region Popup Leaderboard
    PortalText.POR_DUATOP_NOTIFY = "<color=#ffffff>*Bảng xếp hạng sẽ được cập nhật vào mỗi</c><color=#5efffd> {0} phút</color>";
    //#endregion
    //#region Popup User Profile
    PortalText.POR_USERPROFILE_ANONYMOUS = "Người chơi đang ở chế độ ẩn danh";
    PortalText.POR_USERPROFILE_MAXVIP_NOTIFY = "EXP: Quý Khách đã đạt cấp VIP tối đa";
    PortalText.POR_USERPROFILE_CHECKING = "Đang kiểm tra!";
    PortalText.POR_PROFILE_HISTORY_TIME = "<color=#ffc547>{0}\n</c><color=#ffffff>{1}</color>";
    PortalText.POR_PROFILE_ID = "ID: {0}...";
    PortalText.POR_PROFILE_NICKNAME = "Tên TK: {0}";
    PortalText.POR_PROFILE_EXP_NEED = "Còn cần {0} EXP để tăng cấp {1}";
    PortalText.POR_SECURITY_NOT_UPDATE = "Chưa cập nhật";
    PortalText.POR_SECURITY_INPUT_OLDPASS = "Nhập mật khẩu cũ";
    PortalText.POR_SECURITY_INPUT_NEWPASS = "Nhập mật khẩu mới";
    PortalText.POR_SECURITY_REINPUT_NEWPASS = "Nhập lại mật khẩu mới";
    PortalText.POR_VIP = "Vip {0}";
    PortalText.POR_VIP_UPPER = "VIP {0}";
    PortalText.POR_VIP_EXP_MAX = "Max";
    PortalText.POR_VIP_NAME = {
        1: "WOOD",
        2: "BRONZE",
        3: "SLIVER",
        4: "GOLD",
        5: "PLATINUM",
        6: "DIAMOND",
        7: "RUBY",
        8: "EMERALD",
    };
    //#endregion
    //#region Popup Confirm Vault
    PortalText.POR_CONFIRM_VAULT_FRONT_SEND = "Quý Khách muốn gửi";
    PortalText.POR_CONFIRM_VAULT_AFTER_SEND = "vào két sắt?";
    PortalText.POR_CONFIRM_VAULT_FRONT_WITHDRAW = "Quý Khách muốn rút";
    PortalText.POR_CONFIRM_VAULT_AFTER_WITHDRAW = "từ két sắt?";
    //#endregion
    //#region Popup Login/Register/ForgetPass...
    PortalText.POR_CAPTCHA_PLACEHOLDER = "MÃ XÁC NHẬN";
    PortalText.POR_EMAIL_PLACEHOLDER = "EMAIL";
    PortalText.POR_PHONENUMBER_PLACEHOLDER = "SỐ ĐIỆN THOẠI";
    PortalText.POR_TELEGRAM_PLACEHOLDER = "TÀI KHOẢN TELEGRAM";
    PortalText.POR_EMAIL = "email";
    PortalText.POR_PHONENUMBER = "số điện thoại";
    PortalText.POR_TELEGRAM = "Telegram";
    PortalText.POR_FORGETPASS_NOTIFY_NOT_ENABLE_OTP = "Quý Khách Chưa Kích Hoạt Hình Thức Này!";
    PortalText.POR_FORGETPASS_NOTIFY_1 = "*Tính năng chỉ hoạt động khi Quý Khách đã xác thực {0}";
    PortalText.POR_FORGETPASS_NOTIFY_2 = "*Mã OTP sẽ được gửi vào {0} của Quý Khách";
    PortalText.POR_CHANGE_PASS_SUCCESS = "Đặt lại mật khẩu thành công, vui lòng đăng nhập với mật khẩu mới";
    PortalText.POR_OTP_SEND_SUCCESS = "OTP đã được gửi thành công, xin vui lòng kiểm tra!";
    PortalText.POR_MAIL_SEND_SUCCESS = "Mail xác nhận đã được gửi thành công, xin vui lòng kiểm tra email!";
    //#endregion
    //#region Card Lobby
    PortalText.POR_CARDLOBBY_TABLE_SOLO = "BÀN SOLO ({0})";
    PortalText.POR_CARDLOBBY_TABLE_4 = "BÀN {0} NGƯỜI ({1})";
    PortalText.POR_CARDLOBBY_TABLE_PRIVATE = "BÀN RIÊNG ({0})";
    PortalText.POR_CARDLOBBY_TABLE_TAB = "BÀN {0} NGƯỜI";
    PortalText.POR_CARD_LEADERBOARD_NOTIFY = " phút";
    //#endregion
    //#region Card report result message
    PortalText.POR_CARD_REPORT_SUCCESSED = "Đã báo vây thành công. Hệ thống sẽ tiếp nhận và xử lý.";
    PortalText.POR_CARD_REPORT_FAILED_REPORTED = "Bạn đã báo đánh vây ván đấu này rồi. Hệ thống đang tiếp nhận và xử lý.";
    PortalText.POR_CARD_REPORT_ANONYMOUS = "Quý khách không tham gia chơi ván chơi hiện tại nên không thể báo vây.";
    PortalText.POR_CARD_REPORT_FAILED_NOT_ENOUGH_PLAYER = "Chức năng báo đánh vây chỉ hoạt động cho bàn có nhiều hơn 2 người chơi!";
    //#region Card report result message
    //#region Message Network
    PortalText.POR_MESS_NETWORK_DISCONNECT = "Đường truyền không ổn định,\nvui lòng kiểm tra lại kết nối mạng!";
    PortalText.POR_MESS_TOKEN_EXPIRED = "Phiên đăng nhập đã hết hạn. Vui lòng\nđăng nhập lại!";
    PortalText.POR_MESS_ACCOUNT_LOGIN_OTHER_DEVICE = "Tài khoản này đang được đăng nhập trên một thiết bị khác.\nVui lòng thử lại!";
    PortalText.POR_MESS_EVENT_END = "Sự kiện đã hết thời gian tham gia!";
    PortalText.POR_MESS_GET_CAPTCHA_DELAY = "Quý Khách Phải Chờ 3 Giây Để Gọi Lại";
    PortalText.POR_MESS_INTERNAL = "Có lỗi xảy ra xin vui lòng thử lại!";
    PortalText.POR_MESS_INSUFFICIENT_BALANCE = "Quý Khách đã hết tiền, xin vui lòng nạp thêm nhé!";
    PortalText.POR_MESS_SERVER_CODE_UNKNOWN = '"Server trả về mã lỗi Code.UNKNOWN';
    //#endregion
    //#region Notify
    PortalText.POR_NOTIFY_COPY_SUCCESS = "Đã sao chép";
    PortalText.POR_NOTIFY_FEATURE_DEVELOPING = "Tính năng đang phát triển.";
    PortalText.POR_NOTIFY_PING_FAIL = "Mất kết nối với máy chủ, hệ thống đang\nkết nối lại, xin vui lòng chờ.";
    PortalText.POR_NOTIFY_SUCCESS_SECURITY = "Quý Khách đã hoàn thành đăng ký bảo mật tài khoản";
    PortalText.POR_NOTIFY_GAME_FUTURE_RELEASE = "Game sắp ra mắt.";
    PortalText.POR_NOTIFY_GIFTCODE_SUCCESS = "Quý Khách đã kích hoạt Gift Code thành công. Chúc Quý Khách chơi game vui vẻ!";
    PortalText.POR_NOTIFY_FEATURE_FUTURE_RELEASE = "Tính năng sắp ra mắt.";
    PortalText.POR_NOTIFY_VAULT_SEND = "Đã gửi {0} $ vào Két Sắt";
    PortalText.POR_NOTIFY_VAULT_WITHDRAW = "Đã rút {0} $ vào Ví";
    PortalText.POR_NOTIFY_VAULT_INVALID_AMOUNT = "Quý Khách vui lòng không nhập số tiền có phần thập phân!";
    PortalText.POR_NOTIFY_COPY_ID_SUCCESS = "Đã sao chép UserID!";
    PortalText.POR_NOTIFY_COPY_TABLE_ID_SUCCESS = "Đã sao chép TableID!";
    PortalText.POR_NOTIFY_COPY_LINK_SUCCESS = "Đã sao chép đường dẫn!";
    PortalText.POR_NOTIFY_COPY_PASS_SUCCESS = "Đã sao chép mật khẩu!";
    PortalText.POR_NOTIFY_COPY_GIFTCODE_SUCCESS = "Đã sao chép thành công!";
    PortalText.POR_NOTIFY_SEND_OTP_SUCCESS = "Quý khách vui lòng kiểm tra số điện thoại vừa nhập để nhận cuộc gọi thông báo mã OTP.";
    PortalText.POR_NOTIFY_WARNING_RESET_OTP = "Xin vui lòng chờ {0}s để có thể phát hành lại OTP.";
    PortalText.POR_NOTIFY_WARNING_RESET_CODE_CONFIRM = "Xin vui lòng chờ {0}s để có thể phát hành lại Mã xác thực";
    PortalText.POR_NOTIFY_VERIFY_SUCCESS = "Xác thực thành công!";
    PortalText.POR_NOTIFY_UPDATE_SETTING_TELEGRAM_SUCCESS = "Cập nhật thành công!";
    PortalText.POR_NOTIFY_UPDATE_MAIL_SUCCESS = "Mật khẩu đã được cập nhật thành công!";
    PortalText.POR_NOTIFY_UPDATE_TELEGRAM_OTP_SUCCESS = "Chúc mừng quý khách đã đăng ký OTP thành công!";
    PortalText.POR_NOTIFY_AVATAR_EMPTY = "Quý Khách chưa chọn avatar!";
    PortalText.POR_NOTIFY_AVATAR_UPDATE_SUCCESS = "Quý Khách đã cập nhật Avatar thành công!";
    PortalText.POR_NOTIFY_USERNAME_INVALID = "Tên đăng nhập hoặc Mật khẩu không chính xác!";
    PortalText.POR_NOTIFY_USERNAME_NOT_EXIST = "Tên đăng nhập không tồn tại.";
    PortalText.POR_NOTIFY_USERNAME_TOO_SHORT = "Tên Tài Khoản phải có ít nhất 6 ký tự!";
    PortalText.POR_NOTIFY_USERNAME_TOO_LONG = "Tên tài khoản chỉ được phép đặt tối đa 12 ký tự!";
    PortalText.POR_NOTIFY_USERNAME_ILLEGAL_CHARACTER = "Tên Tài Khoản không được phép sử dụng ký tự đặc biệt hay ký tự có dấu!";
    PortalText.POR_NOTIFY_USERNAME_INVALID_PREFIX = "Tên Tài Khoản phải được bắt đầu bằng ký tự!";
    PortalText.POR_NOTIFY_USERNAME_EXISTED = "Tên Tài Khoản đã tồn tại!";
    PortalText.POR_NOTIFY_USERNAME_EMPTY = "Tên Tài Khoản không được để trống!";
    PortalText.POR_NOTIFY_USER_ACCOUNT_IS_BANNED = "Tài khoản đã bị khoá. Chi tiết vui lòng liên hệ CSKH.";
    PortalText.POR_NOTIFY_USER_ACCOUNT_IS_LOCKED = "Quý Khách bị cấm rút tiền do vi phạm quy định. Liên hệ với CSKH để biết thêm chi tiết";
    PortalText.POR_NOTIFY_TELEGRAM_USERNAME_NOT_FOUND = "Không tìm thấy tên tài khoản, Quý Khách vui lòng kiểm tra Telegram";
    PortalText.POR_NOTIFY_NICKNAME_EMPTY = "Quý Khách chưa đặt tên nhân vật!";
    PortalText.POR_NOTIFY_NICKNAME_TOO_SHORT = "Tên nhân vật phải có ít nhất 6 kí tự!";
    PortalText.POR_NOTIFY_NICKNAME_TOO_LONG = "Tên nhân vật chỉ được phép đặt tối đa 12 kí tự!";
    PortalText.POR_NOTIFY_NICKNAME_INVALID_PREFIX = "Tên nhân vật không được bắt đầu bằng khoảng trắng";
    PortalText.POR_NOTIFY_NICKNAME_SAME_USERNAME = "Tên nhân vật nhập trùng với tên tài khoản!";
    PortalText.POR_NOTIFY_NICKNAME_PROFANITY = "Tên nhân vật có chứa từ ngữ không lịch sự xin vui lòng đặt lại!";
    PortalText.POR_NOTIFY_NICKNAME_HAS_BANNED_CHARACTER = "Tên người chơi có bao gồm ký tự cấm, xin vui lòng thử lại!";
    PortalText.POR_NOTIFY_NICKNAME_EXISTED = "Tên nhân vật này đã được sử dụng!";
    PortalText.POR_NOTIFY_NICKNAME_CHANGE_SUCCESS = "Cập nhật tên nhân vật thành công!";
    PortalText.POR_NOTIFY_PASS_INVALID = "Mật khẩu không đúng.";
    PortalText.POR_NOTIFY_PASS_MISMATCH = "Mật khẩu chưa trùng khớp.";
    PortalText.POR_NOTIFY_PASS_TOO_SHORT = "Mật Khẩu phải có ít nhất 6 ký tự!";
    PortalText.POR_NOTIFY_PASS_ILLEGAL_CHARACTER = "Mật Khẩu không được phép sử dụng ký tự có dấu!";
    PortalText.POR_NOTIFY_PASS_HAS_SPACE = "Mật Khẩu không được sử dụng khoảng trắng!";
    PortalText.POR_NOTIFY_PASS_NO_CHANGE = "Mật khẩu mới không được trùng với mật khẩu cũ!";
    PortalText.POR_NOTIFY_PASS_EMPTY = "Xin vui lòng điền mật khẩu";
    PortalText.POR_NOTIFY_PASS_CHANGE_SUCCESS = "Cập nhật mật khẩu thành công!";
    PortalText.POR_NOTIFY_NEW_PASS_INVALID = "Mật khẩu mới chưa đúng cú pháp";
    PortalText.POR_NOTIFY_NEW_PASS_MISMATCH = "Mật khẩu mới chưa khớp.";
    PortalText.POR_NOTIFY_CURRENT_PASS_INVALID = "Mật khẩu hiện tại không chính xác, xin vui lòng kiểm tra lại!";
    PortalText.POR_NOTIFY_EMAIL_INVALID = "Cú pháp của email không chính xác, xin vui lòng thử lại!";
    PortalText.POR_NOTIFY_EMAIL_NOT_FOUND = "Email không đúng hoặc chưa được liên kết bảo mật với tài khoản này!";
    PortalText.POR_NOTIFY_EMAIL_EXISTED = "Email này đã được sử dụng, xin vui lòng kiểm tra lại!";
    PortalText.POR_NOTIFY_PHONENUMBER_INVALID = "Số điện thoại không chính xác, hoặc đã được sử dụng!";
    PortalText.POR_NOTIFY_PHONENUMBER_NOT_FOUND = "Số điện thoại không đúng hoặc chưa được liên kết với tài khoản";
    PortalText.POR_NOTIFY_PHONE_NUMBER_EXISTED = "Số điện thoại này đã được sử dụng, xin vui lòng kiểm tra lại!";
    PortalText.POR_NOTIFY_OTP_INVALID = "Mã xác thực không chính xác hoặc đã hết hạn!";
    PortalText.POR_NOTIFY_OTP_INVALID_2 = "Mã OTP không chính xác, Quý Khách vui lòng thử lại!";
    PortalText.POR_NOTIFY_CAPTCHA_INVALID = "Mã xác nhận không chính xác, xin vui lòng thử lại!";
    PortalText.POR_NOTIFY_RESET_PASSWORD_CAPTCHA_IS_INCORRECT = "Xác thực thất bại";
    PortalText.POR_NOTIFY_DEEPLINK_INVALID = "Đường link đã hết hạn, vui lòng tiến hành lại!";
    PortalText.POR_NOTIFY_INVALID_ARGUMENT = "Quý khách điền chưa chính xác, vui lòng điền lại!";
    PortalText.POR_NOTIFY_SIGNUP_LIMITED = "Quý Khách đã đăng kí quá nhiều tài khoản trong hôm nay, xin vui lòng quay lại sau!";
    PortalText.POR_NOTIFY_REDEEM_FAIL_MANY_TIMES = "Quý Khách đã nhập sai quá nhiều lần trong hôm nay, xin vui lòng quay lại sau!";
    PortalText.POR_NOTIFY_REDEEM_OUT_OF_CODES = "Rất tiếc số lượng Gift Code này đã hết";
    PortalText.POR_NOTIFY_REDEEM_CODE_INVALID = "Mã Gift Code của Quý Khách không chính xác hay đã hết hạn xin vui lòng kiểm tra lại!";
    PortalText.POR_NOTIFY_REDEEM_CODE_INVALID_2 = "Gift Code không chính xác hay đã hết hạn!";
    PortalText.POR_NOTIFY_REDEEM_MAX_ATTEMPT = "Rất tiếc Quý Khách đã nhận Gift Code này rồi";
    PortalText.POR_NOTIFY_REDEEM_INTERNAL = "Có lỗi xảy ra, xin vui lòng thử lại sau!";
    PortalText.POR_NOTIFY_REDEEM_NOT_IN_LIST = "Rất tiếc, tài khoản của Quý Khách không nằm trong chương trình khuyến mãi này";
    // New
    PortalText.POR_NOTIFY_GIFTCODE_FAIL_TOO_MANY_TIMES = "Quý Khách Đã Nhập Sai Quá Số Lần Quy Định Trong Hôm Nay, Xin Vui Lòng Thử Lại Sau!";
    PortalText.POR_NOTIFY_GIFTCODE_REACHED_MAX_ATTEMPT = "Gift Code Không Hợp Lệ. Quý Khách Vui Lòng Sử Dụng Mã Khác!";
    PortalText.POR_NOTIFY_GIFTCODE_GIFT_CODE_IS_EXPIRED = "Gift Code Đã Hết Hạn.";
    PortalText.POR_NOTIFY_GIFTCODE_USER_JOINED_EVENT = "Gift Code Đã Được Sử Dụng. Quý Khách Vui Lòng Sử Dụng Mã Khác!";
    PortalText.POR_NOTIFY_GIFTCODE_USER_UNVERIFIED_PHONE = "Quý Khách Cần Kích Hoạt Số Điện Thoại Để Sử Dụng Tính Năng Này!";
    PortalText.POR_NOTIFY_REJECT_INVITE_GAME = "Đã từ chối nhận lời mời chơi Game.";
    PortalText.POR_NOTIFY_AFK_KICK = "Quý khách đã bị mời ra khỏi bàn vì không thao tác trong một thời gian dài!";
    PortalText.POR_NOTIFY_INVALID_BALANCE_KICK = "Số dư của Quý Khách không đủ, xin vui lòng nạp thêm tiền để tham gia!";
    PortalText.POR_NOTITY_TABLE_INFO_INVALID = "Thông tin bàn chơi không tồn tại, xin vui lòng thử lại.";
    PortalText.POR_NOTITY_CREATE_TABLE_INVALID = "Thông tin tạo bàn không hợp lệ, xin vui lòng thử lại";
    PortalText.POR_NOTITY_ERROR_INVALID = "Có lỗi, xin vui lòng thử lại.";
    PortalText.POR_NOTITY_TABLE_FULL = "Hiện tại bàn chơi này đã đầy, xin vui lòng chọn bàn chơi khác để tham gia!";
    PortalText.POR_NOTITY_TABLE_PLAYING = "Bàn đang chơi, xin vui lòng đợi!";
    PortalText.POR_NOTITY_TABLE_MATCH_NOT_FOUND = '"Không tìm thấy bàn chơi phù hợp, xin vui lòng thử lại.';
    PortalText.POR_NOTITY_TABLE_NOT_ENOUGH_MONEY = "Quý Khách không đủ tiền để tham gia mức cược này.";
    PortalText.POR_NOTIFY_USER_EXIST_IN_OTHER_TABLE = "Quý Khách đang tham gia một phòng khác.";
    PortalText.POR_NOTIFY_USER_NOT_EXIST = "Người chơi không tồn tại trong bàn!";
    PortalText.POR_NOTITY_PASS_WRONG = "Mật khẩu chưa chính xác, xin vui lòng thử lại!";
    PortalText.POR_NOTIFY_PASS_NOT_WORK = "Mật khẩu không được bao gồm ký tự có dấu, ký tự đặc biệt và khoảng trắng.";
    PortalText.POR_NOTITY_REQUEST_TOO_MANY_TIMES = "Quý khách đã mời quá nhiều lần.";
    PortalText.POR_NOTIFY_CARDCHAT_WARNING_WAITING = "Quý Khách cần chờ {0}s để có thể chat tiếp!";
    PortalText.POR_NOTIFY_CARDCHAT_WARNING_WAITING_2 = "Quý Khách cần chờ {0}s để có thể chat   tiếp!";
    PortalText.POR_NOTIFY_CARDCHAT_WARNING_MIN = "Số dư của Quý Khách phải trên {0} để có thể chat!";
    PortalText.POR_NOTIFY_CARDCHAT_WARNING_LENGTH = "Quý Khách chỉ có thể chat tối đa {0} ký tự!";
    PortalText.POR_NOTIFY_CARDCHAT_EMPTY = "Xin vui lòng gõ nội dung";
    PortalText.POR_NOTIFY_CARDCHAT_EMOJI_NOT_SUPPORT = "Không hỗ trợ hình ảnh Emoji";
    PortalText.POR_NOTIFY_CARDCHAT_CHAT_NOT_WORK = "Nội dung không hợp lệ!";
    PortalText.POR_NOTIFY_CARD_MIN_BALANCE = "Số dư của Quý Khách cần phải có tối thiểu {0} để tham gia!";
    //#endregion
    PortalText.POR_PAY_FOR_CONTINUE = "Nạp tiền để chơi tiếp nhé";
    PortalText.POR_MORE_PAY_MORE_CHANCE = "Thêm tiền thêm cơ hội";
    PortalText.POR_PAY_FOR_PRIZE = "Nạp tiền trúng liền";
    PortalText.POR_DELETE_MESSAGE = "Đã xóa thư thành công";
    PortalText.PORTAL_REFERRAL_NOTIFY = "Hoa hồng của quý khách được kết toán vào <color =#eaff60>{0}</color> hằng ngày";
    //#region Facebook login err msg
    PortalText.POR_NOTIFY_MY_ID_SIGN_UP_WITH_FACEBOOK_FAILED = "Có lỗi xảy ra xin vui lòng thử lại!";
    PortalText.POR_NOTIFY_MY_ID_SIGN_UP_WITH_FACEBOOK_EXISTED = "Tài khoản facebook này đã được đăng ký trên hệ thống!";
    PortalText.POR_NOTIFY_MY_ID_SIGN_IN_WITH_FACEBOOK_FAILED = "Phiên đăng nhập đã hết hạn. Xin vui lòng thử lại!";
    PortalText.POR_NOTIFY_MY_ID_SIGN_IN_WITH_FACEBOOK_NOT_FOUND = "Tài khoản này chưa được đăng ký trên hệ thống!";
    PortalText.POR_NOTIFY_MY_ID_CHANGE_PASSWORD_IS_UNAVAILABLE = "Tài khoản được tạo bằng Facebook nên không thể sử dụng tính năng này!";
    PortalText.POR_NOTIFY_ROSE_REFERRAL_CODE_NOT_FOUND = "Vui lòng kiểm tra lại, Mã Giới Thiệu không tồn tại trong hệ thống.";
    //#endregion facbook login err msg
    //#region Mail error Msg
    PortalText.POR_NOTIFY_MAIL_NOT_FOUND = "Thư không tồn tại, đã xóa hoặc đã hết hạn";
    PortalText.POR_NOTIFY_MAIL_ATTACHMENT_UNAVAILABLE = "Thư này không đi kèm với quà hoặc Quý Khách đã nhận trước đó!";
    PortalText.POR_NOTIFY_MAIL_ATTACHMENT_EXPIRED = "Đã quá thời hạn nhận quà!";
    PortalText.POR_NOTIFY_MAIL_ATTACHMENT_CLAIMED = "Quà này đã được nhận rồi!";
    PortalText.POR_NOTIFY_MAIL_INDELIBLE = "Vui lòng nhận quà đính kèm trước khi xóa thư!";
    //#endregion
    //#region Mission error Msg
    PortalText.POR_NOTIFY_MISSION_REWARD_HAS_EXPIRED = "Đã quá thời hạn có thể nhận phần thưởng!";
    //#endregion
    //------Maintenace - Force update--------------------
    PortalText.FULL_MAINTENACE_MSG = "Cổng game đang trong quá trình bảo trì, mong quý khách thông cảm và quay lại sau!";
    PortalText.FULL_MAINTENACE_REMIND_MSG = "Cổng game sắp tiến hành bảo trì trong ít phút nữa, mong quý khách thông cảm và quay lại sau!";
    PortalText.ONE_GAME_MAINTENACE_MSG = "Game {0} đang trong quá trình bảo trì, mong quý khách thông cảm và quay lại lúc %h:%m ngày %d-%M-%Y. Chúc quý khách đặt một ăn trăm.";
    PortalText.ONE_GAME_MAINTENACE_REMIND_MSG = "Cổng game sắp tiến hành bảo trì game {0}, mong quý khách thông cảm và quay lại lúc %h:%m ngày %d-%M-%Y. Chúc quý khách nhiều may mắn.";
    PortalText.FORCE_UPDATE_MSG = "Đã có bản update mới, xin quý khách vui lòng cập nhật";
    PortalText.MY_ID_MAINTENANCE = "Cổng Game Đang Bảo Trì, Quý Khách Vui Lòng Quay Lại Sau";
    //---EndMaintenace - Force update--------------------
    PortalText.QR_SAVE_SUCCESS_MSG = "Lưu ảnh thành công";
    PortalText.QR_SAVE_FAIL_MSG = "Lưu ảnh thất bại";
    PortalText.QR_SAVE_FAIL_MSG_IOS = "Lưu ảnh thất bại. Vui lòng kiểm tra quyền lưu ảnh trong cài đặt của máy";
    PortalText.QR_SAVE_FAIL_MSG_ANDROID = "Lưu ảnh thất bại. Vui lòng kiểm tra quyền lưu ảnh trong cài đặt của máy";
    PortalText.PORTAL_NOTIFY_HOTLINE_CONTACT = "Xin vui lòng liên lạc đến Hotline {0}";
    PortalText.PORTAL_USERNAME_NOT_INPUT = "Quý Khách Vui Lòng Điền Tên Đăng Nhập!";
    PortalText.EWC_INSUFFICIENT_TICKET = "Quý Khách không đủ lượt sút để tham gia!";
    PortalText.EWC_UNVERIFIED = "Quý Khách cần kích hoạt SĐT để tham gia!";
    PortalText.EWC_NOT_EVENT_TIME = "Sự kiện chưa diễn ra hoặc đã kết thúc, Quý Khách vui lòng quay lại sau.";
    PortalText.TET_INSUFFICIENT_TICKET = "Quý Khách không đủ ticket để tham gia lượt quay!";
    PortalText.TET_UNVERIFIED = "Quý Khách cần kích hoạt SĐT để tham gia!";
    PortalText.TET_NOT_EVENT_TIME = "Sự kiện chưa diễn ra hoặc đã kết thúc, Quý Khách vui lòng quay lại sau.";
    return PortalText;
}());
exports.default = PortalText;

cc._RF.pop();