
(function () {
var scripts = [{"deps":{"./assets/SicboModuleAdapter":1,"./assets/Game/Lobby/Base/ScreenInstance":23,"./assets/Game/Lobby/Base/PopupSystemInstance":4,"./assets/Game/Lobby/Base/ElementInstance":26,"./assets/Game/Lobby/Component/ScaleEffectBtnComp":139,"./assets/Game/Lobby/Define/GameDefine":24,"./assets/Game/Lobby/Define/ConnectDefine":6,"./assets/Game/Lobby/Event/EventManager":7,"./assets/Game/Lobby/Event/HookEvent":25,"./assets/Game/Lobby/Event/EventBusManager":27,"./assets/Game/Lobby/Manager/ResourceManager":33,"./assets/Game/Lobby/Manager/TextNotify":32,"./assets/Game/Lobby/Manager/TextNotifyDisconnect":8,"./assets/Game/Lobby/Manager/UIManager":29,"./assets/Game/Lobby/Manager/NetworkManager":28,"./assets/Game/Lobby/Network/IdentityHandler":31,"./assets/Game/Lobby/Network/MyAccountHandler":30,"./assets/Game/Lobby/Network/StabilizeConnection":34,"./assets/Game/Lobby/Network/WalletHandler":35,"./assets/Game/Lobby/Network/BenTauHandler":36,"./assets/Game/Lobby/Network/WebSocketClient/WSClient_Android":45,"./assets/Game/Lobby/Network/WebSocketClient/WSClient_WebAndIOS":2,"./assets/Game/Lobby/Network/WebSocketClient/WSClient":39,"./assets/Game/Lobby/Popup/RetryPopupSystem":37,"./assets/Game/Lobby/Popup/WarningPopupSystem":9,"./assets/Game/Lobby/Popup/LoadingPopupSystem":43,"./assets/Game/Lobby/Scene/LoginScene":11,"./assets/Game/Lobby/Utils/GameUtils":10,"./assets/Game/Lobby/Utils/GlobalData":38,"./assets/Game/Lobby/Utils/LayerHelper":41,"./assets/Game/Lobby/Utils/PortalText":44,"./assets/Game/Lobby/Utils/TimeUtils":48,"./assets/Game/Lobby/Utils/EnumGame":42,"./assets/Game/Lobby/Base/PopupInstance":97,"./assets/Game/Sicbo/Scripts/SicboPortalAdapter":50,"./assets/Game/Sicbo/Scripts/SicboTracking":46,"./assets/Game/Sicbo/Scripts/SicboVersionComp":47,"./assets/Game/Sicbo/Scripts/Components/SicboAvatarWinMoneyComp":12,"./assets/Game/Sicbo/Scripts/Components/SicboBetComp":49,"./assets/Game/Sicbo/Scripts/Components/SicboButtonSound":52,"./assets/Game/Sicbo/Scripts/Components/SicboChangeTabComp":51,"./assets/Game/Sicbo/Scripts/Components/SicboLatestResultComp":53,"./assets/Game/Sicbo/Scripts/Components/SicboLoadingBackgroundAnimation":55,"./assets/Game/Sicbo/Scripts/Components/SicboLoadingScene":54,"./assets/Game/Sicbo/Scripts/Components/SicboMenuDetails":62,"./assets/Game/Sicbo/Scripts/Components/SicboNotifyComp":57,"./assets/Game/Sicbo/Scripts/Components/SicboSessionInfoComp":56,"./assets/Game/Sicbo/Scripts/Components/SicboSlotWinMoneyComp":61,"./assets/Game/Sicbo/Scripts/Components/SicboUIComp":66,"./assets/Game/Sicbo/Scripts/Components/SicboUserComp":60,"./assets/Game/Sicbo/Scripts/Components/SicboUserItemEffect":58,"./assets/Game/Sicbo/Scripts/Components/DropDown/SicboBaseDropDownItemComp":3,"./assets/Game/Sicbo/Scripts/Components/DropDown/SicboBaseDropDownOptionDataComp":59,"./assets/Game/Sicbo/Scripts/Components/DropDown/SicboCutOffComp":68,"./assets/Game/Sicbo/Scripts/Components/DropDown/SicboDropDownComp":64,"./assets/Game/Sicbo/Scripts/Components/DropDown/SicboBaseDropDownComp":67,"./assets/Game/Sicbo/Scripts/Components/Items/SicboCoinItemComp":13,"./assets/Game/Sicbo/Scripts/Components/Items/SicboItemComp":63,"./assets/Game/Sicbo/Scripts/Components/Items/SicboUserItemComp":70,"./assets/Game/Sicbo/Scripts/Components/Items/SicboBetSlotItemComp":65,"./assets/Game/Sicbo/Scripts/Components/ScaleNodeComp":73,"./assets/Game/Sicbo/Scripts/Components/States/SicboClockStateComp":14,"./assets/Game/Sicbo/Scripts/Components/States/SicboJackpotStateComp":72,"./assets/Game/Sicbo/Scripts/Components/States/SicboNotifyStateComp":71,"./assets/Game/Sicbo/Scripts/Components/States/SicboDealerStateComp":75,"./assets/Game/Sicbo/Scripts/Components/States/SicboResultStateComp":69,"./assets/Game/Sicbo/Scripts/Components/States/ISicboState":74,"./assets/Game/Sicbo/Scripts/Configs/SicboDiceConfig":16,"./assets/Game/Sicbo/Scripts/Configs/SicboChipSpriteConfig":76,"./assets/Game/Sicbo/Scripts/Enums/SicboResultType":15,"./assets/Game/Sicbo/Scripts/Controllers/SicboController":17,"./assets/Game/Sicbo/Scripts/Helpers/SicboHelper":18,"./assets/Game/Sicbo/Scripts/Helpers/SicboConsoleHelper":78,"./assets/Game/Sicbo/Scripts/Managers/SicboPopUpInstance":79,"./assets/Game/Sicbo/Scripts/Managers/SicboResourceManager":19,"./assets/Game/Sicbo/Scripts/Managers/SicboSoundManager":83,"./assets/Game/Sicbo/Scripts/Managers/SicboSoundItem":80,"./assets/Game/Sicbo/Scripts/Managers/SicboUIManager":77,"./assets/Game/Sicbo/Scripts/Managers/SicboLocalStorageManager":82,"./assets/Game/Sicbo/Scripts/PopUps/SicboInforPopup":88,"./assets/Game/Sicbo/Scripts/PopUps/SicboCannotJoinPopup":20,"./assets/Game/Sicbo/Scripts/RNGCommons/SicboCoinType":81,"./assets/Game/Sicbo/Scripts/RNGCommons/SicboHandleEventShowHideComp":86,"./assets/Game/Sicbo/Scripts/RNGCommons/SicboMoneyFormatComp":84,"./assets/Game/Sicbo/Scripts/RNGCommons/SicboAutoCanvasSizeComp":85,"./assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboInfiniteScrollViewComp":5,"./assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboLoading":92,"./assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboPageViewLoopComp":91,"./assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboReplayAnimationOnActiveComp":87,"./assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboScrollViewWithButton":93,"./assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboSnapScrollView":89,"./assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboButtonEffectComp":90,"./assets/Game/Sicbo/Scripts/RNGCommons/Utils/SicboSkeletonUtils":21,"./assets/Game/Sicbo/Scripts/RNGCommons/Utils/SicboTimeUtils":95,"./assets/Game/Sicbo/Scripts/RNGCommons/Utils/SicboGameUtils":94,"./assets/Game/Sicbo/Scripts/Setting/SicboText":22,"./assets/Game/Sicbo/Scripts/Setting/SicboSetting":99,"./assets/Game/Sicbo/Scripts/SicboMessageHandler":96,"./assets/Game/Sicbo/Scripts/SicboNative":98,"./assets/Game/Lobby/Base/NotifyInstance":40},"path":"preview-scripts/__qc_index__.js"},{"deps":{"@gameloot/client-base-handler/build/EventKey":115,"@gameloot/topic/topic_pb":103,"@gameloot/sicbov2/sicbov2_code_pb":109,"@gameloot/myhistory/MyHistoryServiceClientPb":105,"@gameloot/leaderboard/leaderboard_pb":113,"@gameloot/leaderboard/LeaderboardServiceClientPb":108,"@gameloot/potter/PotterServiceClientPb":112,"@gameloot/potter/potter_pb":100,"@gameloot/conveyor/conveyor_pb":102,"@gameloot/sicbov2/Sicbov2ServiceClientPb":104,"@gameloot/myhistory/myhistory_pb":111,"google-protobuf/google/protobuf/timestamp_pb":118,"google-protobuf/google/protobuf/empty_pb":119,"@gameloot/whatsapp/whatsapp_pb":101,"@gameloot/client-base-handler/build/HandlerClientBase":116,"@gameloot/client-base-handler/build":114,"@gameloot/bentau/bentau_pb":106,"@gameloot/playah/playah_pb":107,"@gameloot/sicbov2/sicbov2_pb":110},"path":"preview-scripts/assets/SicboModuleAdapter.js"},{"deps":{"../../Utils/EnumGame":42,"../../Network/IdentityHandler":31,"@gameloot/bentau/bentau_pb":106},"path":"preview-scripts/assets/Game/Lobby/Network/WebSocketClient/WSClient_WebAndIOS.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/DropDown/SicboBaseDropDownItemComp.js"},{"deps":{"./PopupInstance":97},"path":"preview-scripts/assets/Game/Lobby/Base/PopupSystemInstance.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboInfiniteScrollViewComp.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Define/ConnectDefine.js"},{"deps":{"./HookEvent":25},"path":"preview-scripts/assets/Game/Lobby/Event/EventManager.js"},{"deps":{"../Base/NotifyInstance":40},"path":"preview-scripts/assets/Game/Lobby/Manager/TextNotifyDisconnect.js"},{"deps":{"../Base/PopupSystemInstance":4},"path":"preview-scripts/assets/Game/Lobby/Popup/WarningPopupSystem.js"},{"deps":{"../Utils/EnumGame":42,"../Define/GameDefine":24,"@gameloot/topic/topic_pb":103},"path":"preview-scripts/assets/Game/Lobby/Utils/GameUtils.js"},{"deps":{"./../Network/MyAccountHandler":30,"./../Network/IdentityHandler":31,"./../Network/BenTauHandler":36,"./../Event/EventBusManager":27},"path":"preview-scripts/assets/Game/Lobby/Scene/LoginScene.js"},{"deps":{"../RNGCommons/SicboMoneyFormatComp":84,"../RNGCommons/Utils/SicboSkeletonUtils":21},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboAvatarWinMoneyComp.js"},{"deps":{"../../Configs/SicboChipSpriteConfig":76,"../../Helpers/SicboHelper":18,"../../RNGCommons/SicboCoinType":81},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/Items/SicboCoinItemComp.js"},{"deps":{"../../../../../SicboModuleAdapter":1,"../../Setting/SicboSetting":99,"../../Controllers/SicboController":17,"../../RNGCommons/Utils/SicboSkeletonUtils":21},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/States/SicboClockStateComp.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Enums/SicboResultType.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Configs/SicboDiceConfig.js"},{"deps":{"../../../../SicboModuleAdapter":1,"../Components/SicboUIComp":66,"../Helpers/SicboHelper":18,"../SicboMessageHandler":96,"../Setting/SicboSetting":99,"../Managers/SicboSoundManager":83,"../SicboPortalAdapter":50,"../RNGCommons/SicboHandleEventShowHideComp":86,"../SicboTracking":46,"../Managers/SicboUIManager":77},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Controllers/SicboController.js"},{"deps":{"../../../../SicboModuleAdapter":1,"../Enums/SicboResultType":15,"../RNGCommons/SicboCoinType":81,"../RNGCommons/Utils/SicboGameUtils":94},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Helpers/SicboHelper.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboResourceManager.js"},{"deps":{"../SicboPortalAdapter":50,"../Managers/SicboPopUpInstance":79,"../Setting/SicboSetting":99,"../Setting/SicboText":22},"path":"preview-scripts/assets/Game/Sicbo/Scripts/PopUps/SicboCannotJoinPopup.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/Utils/SicboSkeletonUtils.js"},{"deps":{"../../../../SicboModuleAdapter":1},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Setting/SicboText.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Base/ScreenInstance.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Define/GameDefine.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Event/HookEvent.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Base/ElementInstance.js"},{"deps":{"../Define/GameDefine":24,"../Network/WalletHandler":35,"../Utils/GameUtils":10,"../Manager/UIManager":29,"../Popup/LoadingPopupSystem":43,"../Popup/WarningPopupSystem":9,"../Utils/PortalText":44,"../Manager/TextNotify":32,"../Event/EventManager":7,"@gameloot/topic/topic_pb":103,"@gameloot/client-base-handler/build/EventKey":115},"path":"preview-scripts/assets/Game/Lobby/Event/EventBusManager.js"},{"deps":{"../Utils/PortalText":44,"../Define/GameDefine":24,"../Utils/EnumGame":42,"../Network/BenTauHandler":36,"../Network/IdentityHandler":31,"../Network/WalletHandler":35,"../Utils/LayerHelper":41,"../Utils/GameUtils":10,"../Event/EventBusManager":27,"../Event/EventManager":7,"./UIManager":29,"../Manager/TextNotify":32,"../Manager/TextNotifyDisconnect":8,"../Popup/LoadingPopupSystem":43,"../Popup/RetryPopupSystem":37,"../Popup/WarningPopupSystem":9},"path":"preview-scripts/assets/Game/Lobby/Manager/NetworkManager.js"},{"deps":{"./../Define/GameDefine":24,"../Popup/LoadingPopupSystem":43,"../Utils/LayerHelper":41,"./ResourceManager":33,"@gameloot/client-base-handler/build/EventKey":115},"path":"preview-scripts/assets/Game/Lobby/Manager/UIManager.js"},{"deps":{"../Utils/PortalText":44,"../Define/ConnectDefine":6,"../Define/GameDefine":24,"../Event/EventManager":7,"../Manager/UIManager":29,"../Manager/TextNotify":32,"../Popup/LoadingPopupSystem":43,"../Popup/WarningPopupSystem":9,"./BenTauHandler":36,"./IdentityHandler":31,"@gameloot/topic/topic_pb":103,"@gameloot/myaccount/myaccount_pb":124,"@gameloot/playah/playah_pb":107,"@gameloot/client-base-handler/build":114,"@gameloot/myaccount/myaccount_code_pb":125,"google-protobuf/google/protobuf/empty_pb":119,"@gameloot/myaccount/MyaccountServiceClientPb":126},"path":"preview-scripts/assets/Game/Lobby/Network/MyAccountHandler.js"},{"deps":{"../Manager/UIManager":29,"../Define/GameDefine":24,"../Manager/TextNotify":32,"../Define/ConnectDefine":6,"../Event/EventManager":7,"../Popup/LoadingPopupSystem":43,"../Popup/WarningPopupSystem":9,"../Utils/PortalText":44,"@gameloot/client-base-handler/build/HandlerClientBase":116,"@greyhole/myid/myid_code_pb":132,"google-protobuf/google/protobuf/empty_pb":119,"@greyhole/myid/myid_pb":130,"@greyhole/myid/MyidServiceClientPb":135},"path":"preview-scripts/assets/Game/Lobby/Network/IdentityHandler.js"},{"deps":{"../Base/NotifyInstance":40},"path":"preview-scripts/assets/Game/Lobby/Manager/TextNotify.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Manager/ResourceManager.js"},{"deps":{"../Utils/GameUtils":10,"../Utils/TimeUtils":48,"../Event/EventManager":7,"./BenTauHandler":36,"../Event/EventBusManager":27,"@gameloot/bentau/bentau_pb":106,"@gameloot/topic/topic_pb":103},"path":"preview-scripts/assets/Game/Lobby/Network/StabilizeConnection.js"},{"deps":{"../Define/ConnectDefine":6,"./IdentityHandler":31,"../Event/EventManager":7,"../Manager/UIManager":29,"../Popup/WarningPopupSystem":9,"./BenTauHandler":36,"../Popup/LoadingPopupSystem":43,"../Define/GameDefine":24,"../Utils/PortalText":44,"../Event/EventBusManager":27,"@gameloot/client-base-handler":114,"@gameloot/topic/topic_pb":103,"@gameloot/mywallet/mywallet_code_pb":129,"google-protobuf/google/protobuf/empty_pb":119,"@gameloot/mywallet/mywallet_change_pb":127,"@gameloot/mywallet/MywalletServiceClientPb":128,"@gameloot/mywallet/mywallet_pb":131},"path":"preview-scripts/assets/Game/Lobby/Network/WalletHandler.js"},{"deps":{"../Define/ConnectDefine":6,"../Manager/UIManager":29,"../Define/GameDefine":24,"../Popup/LoadingPopupSystem":43,"../Network/WebSocketClient/WSClient":39,"./StabilizeConnection":34,"../Event/EventManager":7,"../Utils/EnumGame":42,"./IdentityHandler":31,"../Event/EventBusManager":27,"../Utils/PortalText":44,"@gameloot/bentau/bentau_pb":106,"@gameloot/client-base-handler/build":114,"@marketplace/rpc/code_pb":133},"path":"preview-scripts/assets/Game/Lobby/Network/BenTauHandler.js"},{"deps":{"../Event/EventManager":7,"../Base/PopupSystemInstance":4},"path":"preview-scripts/assets/Game/Lobby/Popup/RetryPopupSystem.js"},{"deps":{"../Define/ConnectDefine":6,"../Network/BenTauHandler":36,"../Network/IdentityHandler":31,"../Network/MyAccountHandler":30,"@gameloot/client-base-handler/build/EventKey":115},"path":"preview-scripts/assets/Game/Lobby/Utils/GlobalData.js"},{"deps":{"./WSClient_Android":45,"./WSClient_WebAndIOS":2},"path":"preview-scripts/assets/Game/Lobby/Network/WebSocketClient/WSClient.js"},{"deps":{"./PopupInstance":97},"path":"preview-scripts/assets/Game/Lobby/Base/NotifyInstance.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Utils/LayerHelper.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Utils/EnumGame.js"},{"deps":{"../Base/PopupSystemInstance":4},"path":"preview-scripts/assets/Game/Lobby/Popup/LoadingPopupSystem.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Utils/PortalText.js"},{"deps":{"@gameloot/bentau/bentau_pb":106},"path":"preview-scripts/assets/Game/Lobby/Network/WebSocketClient/WSClient_Android.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/SicboTracking.js"},{"deps":{"./SicboPortalAdapter":50},"path":"preview-scripts/assets/Game/Sicbo/Scripts/SicboVersionComp.js"},{"deps":{"google-protobuf/google/protobuf/timestamp_pb":118},"path":"preview-scripts/assets/Game/Lobby/Utils/TimeUtils.js"},{"deps":{"../../../../SicboModuleAdapter":1,"./SicboUIComp":66,"../Helpers/SicboHelper":18,"./Items/SicboCoinItemComp":13,"./Items/SicboBetSlotItemComp":65,"../Setting/SicboSetting":99,"../Controllers/SicboController":17,"../RNGCommons/UIComponents/SicboScrollViewWithButton":93,"../RNGCommons/Utils/SicboGameUtils":94,"../RNGCommons/SicboCoinType":81},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboBetComp.js"},{"deps":{"../../../SicboModuleAdapter":1,"./Managers/SicboResourceManager":19},"path":"preview-scripts/assets/Game/Sicbo/Scripts/SicboPortalAdapter.js"},{"deps":{"../Managers/SicboSoundManager":83},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboChangeTabComp.js"},{"deps":{"../Setting/SicboSetting":99,"../Managers/SicboSoundManager":83},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboButtonSound.js"},{"deps":{"../../../Sicbo/Scripts/Helpers/SicboHelper":18,"./Items/SicboItemComp":63},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboLatestResultComp.js"},{"deps":{"../../../../SicboModuleAdapter":1,"../SicboMessageHandler":96,"../Managers/SicboResourceManager":19,"../Setting/SicboSetting":99,"../RNGCommons/Utils/SicboGameUtils":94,"./SicboLoadingBackgroundAnimation":55,"../Setting/SicboText":22},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboLoadingScene.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboLoadingBackgroundAnimation.js"},{"deps":{"../../../../SicboModuleAdapter":1,"../RNGCommons/SicboMoneyFormatComp":84,"../RNGCommons/Utils/SicboGameUtils":94,"../Setting/SicboText":22},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboSessionInfoComp.js"},{"deps":{"../Setting/SicboSetting":99,"../Controllers/SicboController":17,"../RNGCommons/Utils/SicboSkeletonUtils":21},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboNotifyComp.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboUserItemEffect.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/DropDown/SicboBaseDropDownOptionDataComp.js"},{"deps":{"../../../../SicboModuleAdapter":1,"./Items/SicboUserItemComp":70,"../SicboPortalAdapter":50,"../Setting/SicboSetting":99,"../Setting/SicboText":22},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboUserComp.js"},{"deps":{"../RNGCommons/SicboMoneyFormatComp":84,"../RNGCommons/Utils/SicboSkeletonUtils":21},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboSlotWinMoneyComp.js"},{"deps":{"../../../../SicboModuleAdapter":1,"../SicboMessageHandler":96,"../Controllers/SicboController":17,"../Managers/SicboUIManager":77,"../PopUps/SicboInforPopup":88,"../Managers/SicboSoundManager":83,"./SicboUIComp":66,"../Setting/SicboText":22},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboMenuDetails.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/Items/SicboItemComp.js"},{"deps":{"./SicboBaseDropDownComp":67},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/DropDown/SicboDropDownComp.js"},{"deps":{"../../../../../SicboModuleAdapter":1,"../../Setting/SicboSetting":99,"../../Helpers/SicboHelper":18,"../../RNGCommons/SicboMoneyFormatComp":84,"../SicboBetComp":49,"./SicboCoinItemComp":13},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/Items/SicboBetSlotItemComp.js"},{"deps":{"../../../../SicboModuleAdapter":1,"./SicboUserComp":60,"../Setting/SicboSetting":99,"./SicboBetComp":49,"../Controllers/SicboController":17,"./SicboNotifyComp":57,"../Helpers/SicboHelper":18,"./SicboSessionInfoComp":56,"./States/SicboClockStateComp":14,"./States/SicboDealerStateComp":75,"./States/SicboResultStateComp":69,"./States/SicboJackpotStateComp":72,"./States/SicboNotifyStateComp":71,"../SicboPortalAdapter":50,"../RNGCommons/Utils/SicboGameUtils":94,"../Setting/SicboText":22,"./SicboMenuDetails":62},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboUIComp.js"},{"deps":{"./SicboBaseDropDownItemComp":3},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/DropDown/SicboBaseDropDownComp.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/DropDown/SicboCutOffComp.js"},{"deps":{"../../../../../SicboModuleAdapter":1,"../../Configs/SicboDiceConfig":16,"../../Setting/SicboSetting":99,"../../Controllers/SicboController":17,"../../Helpers/SicboHelper":18,"../SicboBetComp":49,"../SicboLatestResultComp":53,"../SicboNotifyComp":57,"../SicboSlotWinMoneyComp":61,"../SicboUIComp":66,"../../SicboPortalAdapter":50,"../../RNGCommons/Utils/SicboSkeletonUtils":21,"../../RNGCommons/Utils/SicboGameUtils":94},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/States/SicboResultStateComp.js"},{"deps":{"../../../../../SicboModuleAdapter":1,"../../RNGCommons/SicboMoneyFormatComp":84,"../../SicboPortalAdapter":50,"../SicboAvatarWinMoneyComp":12,"../SicboUIComp":66,"../SicboUserItemEffect":58,"../../Setting/SicboSetting":99},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/Items/SicboUserItemComp.js"},{"deps":{"../../../../../SicboModuleAdapter":1,"../../Setting/SicboSetting":99,"../../Controllers/SicboController":17,"../../Helpers/SicboHelper":18,"../../RNGCommons/Utils/SicboGameUtils":94,"../SicboNotifyComp":57,"../../Setting/SicboText":22},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/States/SicboNotifyStateComp.js"},{"deps":{"../../../../../SicboModuleAdapter":1,"../../Setting/SicboSetting":99,"../../Controllers/SicboController":17,"../../SicboPortalAdapter":50,"../../Helpers/SicboHelper":18,"../../RNGCommons/Utils/SicboGameUtils":94,"../../RNGCommons/Utils/SicboSkeletonUtils":21,"../../RNGCommons/SicboMoneyFormatComp":84},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/States/SicboJackpotStateComp.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/ScaleNodeComp.js"},{"deps":{"../../../../../SicboModuleAdapter":1},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/States/ISicboState.js"},{"deps":{"../../../../../SicboModuleAdapter":1,"../../RNGCommons/Utils/SicboGameUtils":94,"../../RNGCommons/Utils/SicboSkeletonUtils":21},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Components/States/SicboDealerStateComp.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Configs/SicboChipSpriteConfig.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboUIManager.js"},{"deps":{"../Controllers/SicboController":17,"../RNGCommons/Utils/SicboGameUtils":94},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Helpers/SicboConsoleHelper.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboPopUpInstance.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboSoundItem.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/SicboCoinType.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboLocalStorageManager.js"},{"deps":{"../..../../../../../SicboModuleAdapter":1,"./SicboLocalStorageManager":82,"./SicboSoundItem":80},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboSoundManager.js"},{"deps":{"./Utils/SicboGameUtils":94},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/SicboMoneyFormatComp.js"},{"deps":{"../../../../SicboModuleAdapter":1},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/SicboAutoCanvasSizeComp.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/SicboHandleEventShowHideComp.js"},{"deps":{"../Utils/SicboSkeletonUtils":21},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboReplayAnimationOnActiveComp.js"},{"deps":{"../Managers/SicboPopUpInstance":79},"path":"preview-scripts/assets/Game/Sicbo/Scripts/PopUps/SicboInforPopup.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboSnapScrollView.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboButtonEffectComp.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboPageViewLoopComp.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboLoading.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboScrollViewWithButton.js"},{"deps":{"../../SicboPortalAdapter":50,"../../SicboNative":98},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/Utils/SicboGameUtils.js"},{"deps":{"../../../../../SicboModuleAdapter":1},"path":"preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/Utils/SicboTimeUtils.js"},{"deps":{"../../../SicboModuleAdapter":1,"./SicboPortalAdapter":50,"./Setting/SicboSetting":99,"./Setting/SicboText":22,"./Managers/SicboUIManager":77,"./PopUps/SicboCannotJoinPopup":20,"./SicboTracking":46},"path":"preview-scripts/assets/Game/Sicbo/Scripts/SicboMessageHandler.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Base/PopupInstance.js"},{"deps":{},"path":"preview-scripts/assets/Game/Sicbo/Scripts/SicboNative.js"},{"deps":{"../../../../SicboModuleAdapter":1},"path":"preview-scripts/assets/Game/Sicbo/Scripts/Setting/SicboSetting.js"},{"deps":{"google-protobuf":121,"google-protobuf/google/protobuf/timestamp_pb.js":118},"path":"preview-scripts/__node_modules/@gameloot/potter/potter_pb.js"},{"deps":{"google-protobuf":121,"@gameloot/bentau/bentau_pb.js":106,"@gameloot/conveyor/conveyor_pb.js":102,"@gameloot/myaccount/myaccount_pb.js":124,"google-protobuf/google/protobuf/timestamp_pb.js":118,"@gameloot/whatsapp/whatsapp_code_pb.js":123},"path":"preview-scripts/__node_modules/@gameloot/whatsapp/whatsapp_pb.js"},{"deps":{"google-protobuf":121,"google-protobuf/google/protobuf/timestamp_pb.js":118},"path":"preview-scripts/__node_modules/@gameloot/conveyor/conveyor_pb.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/@gameloot/topic/topic_pb.js"},{"deps":{"grpc-web":122,"@gameloot/sicbov2/sicbov2_pb":110,"google-protobuf/google/protobuf/empty_pb":119},"path":"preview-scripts/__node_modules/@gameloot/sicbov2/Sicbov2ServiceClientPb.js"},{"deps":{"grpc-web":122,"@gameloot/myhistory/myhistory_pb":111},"path":"preview-scripts/__node_modules/@gameloot/myhistory/MyHistoryServiceClientPb.js"},{"deps":{"google-protobuf":121,"google-protobuf/google/protobuf/timestamp_pb.js":118,"google-protobuf/google/protobuf/any_pb.js":136},"path":"preview-scripts/__node_modules/@gameloot/bentau/bentau_pb.js"},{"deps":{"google-protobuf":121,"@gameloot/mywallet/v1/mywallet_wallet_pb.js":137,"@gameloot/myaccount/myaccount_pb.js":124},"path":"preview-scripts/__node_modules/@gameloot/playah/playah_pb.js"},{"deps":{"grpc-web":122,"@gameloot/leaderboard/leaderboard_pb":113},"path":"preview-scripts/__node_modules/@gameloot/leaderboard/LeaderboardServiceClientPb.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/@gameloot/sicbov2/sicbov2_code_pb.js"},{"deps":{"google-protobuf":121,"@gameloot/playah/playah_pb.js":107,"google-protobuf/google/protobuf/empty_pb.js":119,"google-protobuf/google/protobuf/timestamp_pb.js":118,"@gameloot/mywallet/v1/mywallet_change_pb.js":134},"path":"preview-scripts/__node_modules/@gameloot/sicbov2/sicbov2_pb.js"},{"deps":{"google-protobuf":121,"google-protobuf/google/protobuf/timestamp_pb.js":118},"path":"preview-scripts/__node_modules/@gameloot/myhistory/myhistory_pb.js"},{"deps":{"grpc-web":122,"@gameloot/potter/potter_pb":100},"path":"preview-scripts/__node_modules/@gameloot/potter/PotterServiceClientPb.js"},{"deps":{"google-protobuf":121,"@gameloot/playah/playah_pb.js":107},"path":"preview-scripts/__node_modules/@gameloot/leaderboard/leaderboard_pb.js"},{"deps":{"./HandlerClientBase":116,"./EventKey":115,"./HandlerClientBenTauBase":117},"path":"preview-scripts/__node_modules/@gameloot/client-base-handler/build/index.js"},{"deps":{},"path":"preview-scripts/__node_modules/@gameloot/client-base-handler/build/EventKey.js"},{"deps":{"./TextKey":120,"@gameloot/topic/topic_pb":103,"@marketplace/rpc/code_pb":133},"path":"preview-scripts/__node_modules/@gameloot/client-base-handler/build/HandlerClientBase.js"},{"deps":{"./HandlerClientBase":116,"@marketplace/rpc/code_pb":133},"path":"preview-scripts/__node_modules/@gameloot/client-base-handler/build/HandlerClientBenTauBase.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/google-protobuf/google/protobuf/timestamp_pb.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/google-protobuf/google/protobuf/empty_pb.js"},{"deps":{},"path":"preview-scripts/__node_modules/@gameloot/client-base-handler/build/TextKey.js"},{"deps":{},"path":"preview-scripts/__node_modules/google-protobuf/google-protobuf.js"},{"deps":{},"path":"preview-scripts/__node_modules/grpc-web/index.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/@gameloot/whatsapp/whatsapp_code_pb.js"},{"deps":{"google-protobuf":121,"google-protobuf/google/protobuf/empty_pb.js":119},"path":"preview-scripts/__node_modules/@gameloot/myaccount/myaccount_pb.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/@gameloot/myaccount/myaccount_code_pb.js"},{"deps":{"grpc-web":122,"@gameloot/myaccount/myaccount_pb":124,"google-protobuf/google/protobuf/empty_pb":119},"path":"preview-scripts/__node_modules/@gameloot/myaccount/MyaccountServiceClientPb.js"},{"deps":{"google-protobuf":121,"google-protobuf/google/protobuf/timestamp_pb.js":118},"path":"preview-scripts/__node_modules/@gameloot/mywallet/mywallet_change_pb.js"},{"deps":{"grpc-web":122,"@gameloot/mywallet/mywallet_pb":131,"google-protobuf/google/protobuf/empty_pb":119},"path":"preview-scripts/__node_modules/@gameloot/mywallet/MywalletServiceClientPb.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/@gameloot/mywallet/mywallet_code_pb.js"},{"deps":{"google-protobuf":121,"google-protobuf/google/protobuf/timestamp_pb.js":118,"google-protobuf/google/protobuf/empty_pb.js":119},"path":"preview-scripts/__node_modules/@greyhole/myid/myid_pb.js"},{"deps":{"google-protobuf":121,"@gameloot/mywallet/mywallet_change_pb.js":127,"google-protobuf/google/protobuf/empty_pb.js":119,"google-protobuf/google/protobuf/timestamp_pb.js":118,"@gameloot/mywallet/mywallet_wallet_pb.js":138},"path":"preview-scripts/__node_modules/@gameloot/mywallet/mywallet_pb.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/@greyhole/myid/myid_code_pb.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/@marketplace/rpc/code_pb.js"},{"deps":{"google-protobuf":121,"google-protobuf/google/protobuf/timestamp_pb.js":118},"path":"preview-scripts/__node_modules/@gameloot/mywallet/v1/mywallet_change_pb.js"},{"deps":{"grpc-web":122,"@greyhole/myid/myid_pb":130,"google-protobuf/google/protobuf/empty_pb":119},"path":"preview-scripts/__node_modules/@greyhole/myid/MyidServiceClientPb.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/google-protobuf/google/protobuf/any_pb.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/@gameloot/mywallet/v1/mywallet_wallet_pb.js"},{"deps":{"google-protobuf":121},"path":"preview-scripts/__node_modules/@gameloot/mywallet/mywallet_wallet_pb.js"},{"deps":{},"path":"preview-scripts/assets/Game/Lobby/Component/ScaleEffectBtnComp.js"}];
var entries = ["preview-scripts/__qc_index__.js"];
var bundleScript = 'preview-scripts/__qc_bundle__.js';

/**
 * Notice: This file can not use ES6 (for IE 11)
 */
var modules = {};
var name2path = {};

// Will generated by module.js plugin
// var scripts = ${scripts};
// var entries = ${entries};
// var bundleScript = ${bundleScript};

if (typeof global === 'undefined') {
    window.global = window;
}

var isJSB = typeof jsb !== 'undefined';

function getXMLHttpRequest () {
    return window.XMLHttpRequest ? new window.XMLHttpRequest() : new ActiveXObject('MSXML2.XMLHTTP');
}

function downloadText(url, callback) {
    if (isJSB) {
        var result = jsb.fileUtils.getStringFromFile(url);
        callback(null, result);
        return;
    }

    var xhr = getXMLHttpRequest(),
        errInfo = 'Load text file failed: ' + url;
    xhr.open('GET', url, true);
    if (xhr.overrideMimeType) xhr.overrideMimeType('text\/plain; charset=utf-8');
    xhr.onload = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200 || xhr.status === 0) {
                callback(null, xhr.responseText);
            }
            else {
                callback({status:xhr.status, errorMessage:errInfo + ', status: ' + xhr.status});
            }
        }
        else {
            callback({status:xhr.status, errorMessage:errInfo + '(wrong readyState)'});
        }
    };
    xhr.onerror = function(){
        callback({status:xhr.status, errorMessage:errInfo + '(error)'});
    };
    xhr.ontimeout = function(){
        callback({status:xhr.status, errorMessage:errInfo + '(time out)'});
    };
    xhr.send(null);
};

function loadScript (src, cb) {
    if (typeof require !== 'undefined') {
        require(src);
        return cb();
    }

    // var timer = 'load ' + src;
    // console.time(timer);

    var scriptElement = document.createElement('script');

    function done() {
        // console.timeEnd(timer);
        // deallocation immediate whatever
        scriptElement.remove();
    }

    scriptElement.onload = function () {
        done();
        cb();
    };
    scriptElement.onerror = function () {
        done();
        var error = 'Failed to load ' + src;
        console.error(error);
        cb(new Error(error));
    };
    scriptElement.setAttribute('type','text/javascript');
    scriptElement.setAttribute('charset', 'utf-8');
    scriptElement.setAttribute('src', src);

    document.head.appendChild(scriptElement);
}

function loadScripts (srcs, cb) {
    var n = srcs.length;

    srcs.forEach(function (src) {
        loadScript(src, function () {
            n--;
            if (n === 0) {
                cb();
            }
        });
    })
}

function formatPath (path) {
    let destPath = window.__quick_compile_project__.destPath;
    if (destPath) {
        let prefix = 'preview-scripts';
        if (destPath[destPath.length - 1] === '/') {
            prefix += '/';
        }
        path = path.replace(prefix, destPath);
    }
    return path;
}

window.__quick_compile_project__ = {
    destPath: '',

    registerModule: function (path, module) {
        path = formatPath(path);
        modules[path].module = module;
    },

    registerModuleFunc: function (path, func) {
        path = formatPath(path);
        modules[path].func = func;

        var sections = path.split('/');
        var name = sections[sections.length - 1];
        name = name.replace(/\.(?:js|ts|json)$/i, '');
        name2path[name] = path;
    },

    require: function (request, path) {
        var m, requestScript;

        path = formatPath(path);
        if (path) {
            m = modules[path];
            if (!m) {
                console.warn('Can not find module for path : ' + path);
                return null;
            }
        }

        if (m) {
            let depIndex = m.deps[request];
            // dependence script was excluded
            if (depIndex === -1) {
                return null;
            }
            else {
                requestScript = scripts[ m.deps[request] ];
            }
        }
        
        let requestPath = '';
        if (!requestScript) {
            // search from name2path when request is a dynamic module name
            if (/^[\w- .]*$/.test(request)) {
                requestPath = name2path[request];
            }

            if (!requestPath) {
                if (CC_JSB) {
                    return require(request);
                }
                else {
                    console.warn('Can not find deps [' + request + '] for path : ' + path);
                    return null;
                }
            }
        }
        else {
            requestPath = formatPath(requestScript.path);
        }

        let requestModule = modules[requestPath];
        if (!requestModule) {
            console.warn('Can not find request module for path : ' + requestPath);
            return null;
        }

        if (!requestModule.module && requestModule.func) {
            requestModule.func();
        }

        if (!requestModule.module) {
            console.warn('Can not find requestModule.module for path : ' + path);
            return null;
        }

        return requestModule.module.exports;
    },

    run: function () {
        entries.forEach(function (entry) {
            entry = formatPath(entry);
            var module = modules[entry];
            if (!module.module) {
                module.func();
            }
        });
    },

    load: function (cb) {
        var self = this;

        var srcs = scripts.map(function (script) {
            var path = formatPath(script.path);
            modules[path] = script;

            if (script.mtime) {
                path += ("?mtime=" + script.mtime);
            }
            return path;
        });

        console.time && console.time('load __quick_compile_project__');
        // jsb can not analysis sourcemap, so keep separate files.
        if (bundleScript && !isJSB) {
            downloadText(formatPath(bundleScript), function (err, bundleSource) {
                console.timeEnd && console.timeEnd('load __quick_compile_project__');
                if (err) {
                    console.error(err);
                    return;
                }

                let evalTime = 'eval __quick_compile_project__ : ' + srcs.length + ' files';
                console.time && console.time(evalTime);
                var sources = bundleSource.split('\n//------QC-SOURCE-SPLIT------\n');
                for (var i = 0; i < sources.length; i++) {
                    if (sources[i]) {
                        window.eval(sources[i]);
                        // not sure why new Function cannot set breakpoints precisely
                        // new Function(sources[i])()
                    }
                }
                self.run();
                console.timeEnd && console.timeEnd(evalTime);
                cb();
            })
        }
        else {
            loadScripts(srcs, function () {
                self.run();
                console.timeEnd && console.timeEnd('load __quick_compile_project__');
                cb();
            });
        }
    }
};

// Polyfill for IE 11
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function () {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}
})();
    