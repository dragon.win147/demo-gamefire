
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboResourceManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '87163Pl1uZLeLywpKJGF7u4', 'SicboResourceManager');
// Game/Sicbo_New/Scripts/Managers/SicboResourceManager.ts

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboResourceManager = /** @class */ (function () {
    function SicboResourceManager() {
    }
    SicboResourceManager_1 = SicboResourceManager;
    SicboResourceManager.prototype.setAddressResources = function (addressResources) {
        SicboResourceManager_1.addressResources = addressResources;
    };
    SicboResourceManager.loadSpineFromForgeAsObj = function (rf, onComplete) {
        var image = "";
        var json = "";
        var atlas = "";
        var namePng = "";
        rf.filesList.forEach(function (file) {
            switch (file.type) {
                case "image/png":
                    image = file.path;
                    namePng = file.name;
                    break;
                case "application/json":
                    json = file.path;
                    break;
                case "text/plain; charset=utf-8":
                    atlas = file.path;
                    break;
            }
        });
        image = SicboResourceManager_1.addressResources + image;
        json = SicboResourceManager_1.addressResources + json;
        atlas = SicboResourceManager_1.addressResources + atlas;
        this.loadRemoteSpine(image, json, atlas, namePng, onComplete);
    };
    SicboResourceManager.loadRemoteSpine = function (image, json, atlas, namePNG, onComplete) {
        cc.assetManager.loadRemote(image, function (error, texture) {
            cc.assetManager.loadAny({ url: atlas, type: "txt" }, function (error, atlasJson) {
                cc.assetManager.loadAny({ url: json, type: "txt" }, function (error, spineJson) {
                    if (error) {
                        //cc.log(error);
                        return;
                    }
                    var asset = new sp.SkeletonData();
                    asset.skeletonJson = spineJson;
                    asset.atlasText = atlasJson;
                    asset.textures = [texture];
                    asset.textureNames = [namePNG];
                    if (onComplete)
                        onComplete(asset);
                });
            });
        });
    };
    SicboResourceManager.loadRemoteImageWithFileField = function (spr, fileField, onComplete) {
        if (onComplete === void 0) { onComplete = null; }
        if (spr)
            spr.spriteFrame = null;
        this.loadRemoteImageByFileFieldAsObj(fileField, function (spriteFrame) {
            if (spr) {
                spr.spriteFrame = spriteFrame;
                if (onComplete)
                    onComplete();
            }
        });
    };
    SicboResourceManager.loadRemoteImageByFileFieldAsObj = function (fileField, onComplete) {
        if (fileField) {
            var remoteUrl = SicboResourceManager_1.addressResources + fileField.path;
            cc.assetManager.loadRemote(remoteUrl, function (err, result) {
                ////cc.log("url: "+remoteUrl);
                if (err) {
                    //cc.error(err);
                    if (onComplete)
                        onComplete(null);
                    return;
                }
                var res = new cc.SpriteFrame(result);
                if (onComplete)
                    onComplete(res);
            });
        }
        else {
            if (onComplete)
                onComplete(null);
        }
    };
    SicboResourceManager.loadRemoteImageWithPath = function (spr, path, onComplete, resetFrame) {
        if (onComplete === void 0) { onComplete = null; }
        if (resetFrame === void 0) { resetFrame = false; }
        if (resetFrame)
            spr.spriteFrame = null;
        this.loadImageFromForge(path, function (spriteFrame) {
            if (spr) {
                spr.spriteFrame = spriteFrame;
                if (onComplete)
                    onComplete();
            }
        });
    };
    SicboResourceManager.loadImageFromForge = function (path, onComplete) {
        if (path == "" || path == "undefined" || path === undefined) {
            if (onComplete)
                onComplete(null);
            return;
        }
        var remoteUrl = SicboResourceManager_1.addressResources + path;
        cc.assetManager.loadRemote(remoteUrl, function (err, result) {
            ////cc.log("url: "+remoteUrl);
            if (err) {
                ///LogManager.error(err);
                if (onComplete)
                    onComplete(null);
                return;
            }
            var res = new cc.SpriteFrame(result);
            if (onComplete)
                onComplete(res);
        });
    };
    SicboResourceManager.preloadScene = function (nameScene, onProgress, onComplete) {
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        cc.director.preloadScene(nameScene, onProgress, function () {
            if (cc.sys.isNative) {
                // cc.loader.releaseAll();
            }
            if (onComplete)
                onComplete();
        });
    };
    SicboResourceManager.preloadResourceLocal = function (paths, onProgress, onComplete) {
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (paths.length == 0) {
            if (onComplete)
                onComplete();
            return;
        }
        cc.resources.preload(paths, onProgress, function (err, items) {
            if (err) {
                //cc.error(err);
                if (onComplete)
                    onComplete();
                return;
            }
            if (onComplete)
                onComplete();
        });
    };
    SicboResourceManager.preloadResourceRemote = function (paths, onProgress, onComplete) {
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (paths.length == 0) {
            if (onComplete)
                onComplete();
            return;
        }
        var finish = 0;
        var total = paths.length;
        paths.forEach(function (path) {
            path = SicboResourceManager_1.addressResources + path;
            cc.assetManager.loadRemote(path, function (err, item) {
                if (err) {
                    //cc.error(err);
                }
                finish++;
                if (onProgress)
                    onProgress(finish, total, item);
                if (finish == total) {
                    if (onComplete)
                        onComplete();
                }
            });
        });
    };
    SicboResourceManager.loadScene = function (sceneName, callback) {
        if (callback === void 0) { callback = null; }
        // SoundManager.getInstance().stopAllEffects();
        cc.director.loadScene(sceneName, function (err, asset) {
            if (err) {
                //cc.error(err);
                //TODO : handle error load scene : suggest user reload page and force reload
                // GameUtils.showPopupNotice({ title: i18n.t("common.notice"), text: i18n.t("common.reloadPage") }, function () {
                //   window.location.reload(true);
                // });
            }
            if (callback)
                callback();
        });
    };
    var SicboResourceManager_1;
    SicboResourceManager.addressResources = "";
    SicboResourceManager = SicboResourceManager_1 = __decorate([
        ccclass
    ], SicboResourceManager);
    return SicboResourceManager;
}());
exports.default = SicboResourceManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL01hbmFnZXJzL1NpY2JvUmVzb3VyY2VNYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRU0sSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBQTtJQXFMQSxDQUFDOzZCQXJMb0Isb0JBQW9CO0lBR2hDLGtEQUFtQixHQUExQixVQUEyQixnQkFBd0I7UUFDakQsc0JBQW9CLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7SUFDM0QsQ0FBQztJQUVhLDRDQUF1QixHQUFyQyxVQUFzQyxFQUFPLEVBQUUsVUFBNkM7UUFDMUYsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2YsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2QsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2YsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBRWpCLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtZQUN4QixRQUFRLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2pCLEtBQUssV0FBVztvQkFDZCxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFDbEIsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ3BCLE1BQU07Z0JBQ1IsS0FBSyxrQkFBa0I7b0JBQ3JCLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNqQixNQUFNO2dCQUNSLEtBQUssMkJBQTJCO29CQUM5QixLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFDbEIsTUFBTTthQUNUO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxLQUFLLEdBQUcsc0JBQW9CLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQ3RELElBQUksR0FBRyxzQkFBb0IsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDcEQsS0FBSyxHQUFHLHNCQUFvQixDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUV0RCxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBRWEsb0NBQWUsR0FBN0IsVUFBOEIsS0FBYSxFQUFFLElBQVksRUFBRSxLQUFhLEVBQUUsT0FBZSxFQUFFLFVBQTZDO1FBQ3RJLEVBQUUsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxVQUFDLEtBQUssRUFBRSxPQUFPO1lBQy9DLEVBQUUsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUUsVUFBQyxLQUFLLEVBQUUsU0FBUztnQkFDcEUsRUFBRSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRSxVQUFDLEtBQUssRUFBRSxTQUFTO29CQUNuRSxJQUFJLEtBQUssRUFBRTt3QkFDVCxnQkFBZ0I7d0JBQ2hCLE9BQU87cUJBQ1I7b0JBQ0QsSUFBSSxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLENBQUM7b0JBQ2xDLEtBQUssQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDO29CQUMvQixLQUFLLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztvQkFDNUIsS0FBSyxDQUFDLFFBQVEsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUMzQixLQUFLLENBQUMsWUFBWSxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQy9CLElBQUksVUFBVTt3QkFBRSxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3BDLENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFHYSxpREFBNEIsR0FBMUMsVUFBMkMsR0FBYyxFQUFFLFNBQWMsRUFBRSxVQUE2QjtRQUE3QiwyQkFBQSxFQUFBLGlCQUE2QjtRQUN0RyxJQUFHLEdBQUc7WUFDSixHQUFHLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsK0JBQStCLENBQUMsU0FBUyxFQUFFLFVBQUMsV0FBVztZQUMxRCxJQUFJLEdBQUcsRUFBRTtnQkFDUCxHQUFHLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztnQkFDOUIsSUFBSSxVQUFVO29CQUFFLFVBQVUsRUFBRSxDQUFDO2FBQzlCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRWEsb0RBQStCLEdBQTdDLFVBQThDLFNBQWMsRUFBRSxVQUE0QjtRQUN4RixJQUFJLFNBQVMsRUFBRTtZQUNiLElBQUksU0FBUyxHQUFHLHNCQUFvQixDQUFDLGdCQUFnQixHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUM7WUFDdkUsRUFBRSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLFVBQVUsR0FBRyxFQUFFLE1BQU07Z0JBQ3pELDhCQUE4QjtnQkFDOUIsSUFBSSxHQUFHLEVBQUU7b0JBQ1AsZ0JBQWdCO29CQUNoQixJQUFJLFVBQVU7d0JBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNqQyxPQUFPO2lCQUNSO2dCQUNELElBQUksR0FBRyxHQUFHLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDckMsSUFBSSxVQUFVO29CQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNsQyxDQUFDLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLFVBQVU7Z0JBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2xDO0lBQ0gsQ0FBQztJQUdhLDRDQUF1QixHQUFyQyxVQUFzQyxHQUFjLEVBQUUsSUFBWSxFQUFFLFVBQTZCLEVBQUUsVUFBMkI7UUFBMUQsMkJBQUEsRUFBQSxpQkFBNkI7UUFBRSwyQkFBQSxFQUFBLGtCQUEyQjtRQUM1SCxJQUFJLFVBQVU7WUFBRSxHQUFHLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUV2QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLFVBQUMsV0FBVztZQUN4QyxJQUFJLEdBQUcsRUFBRTtnQkFDUCxHQUFHLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztnQkFDOUIsSUFBSSxVQUFVO29CQUFFLFVBQVUsRUFBRSxDQUFDO2FBQzlCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRWEsdUNBQWtCLEdBQWhDLFVBQWlDLElBQVksRUFBRSxVQUE0QjtRQUN6RSxJQUFJLElBQUksSUFBSSxFQUFFLElBQUksSUFBSSxJQUFJLFdBQVcsSUFBSSxJQUFJLEtBQUssU0FBUyxFQUFFO1lBQzNELElBQUksVUFBVTtnQkFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakMsT0FBTztTQUNSO1FBQ0QsSUFBSSxTQUFTLEdBQUcsc0JBQW9CLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQzdELEVBQUUsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxVQUFVLEdBQUcsRUFBRSxNQUFNO1lBQ3pELDhCQUE4QjtZQUM5QixJQUFJLEdBQUcsRUFBRTtnQkFDUCx5QkFBeUI7Z0JBQ3pCLElBQUksVUFBVTtvQkFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2pDLE9BQU87YUFDUjtZQUNELElBQUksR0FBRyxHQUFHLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNyQyxJQUFJLFVBQVU7Z0JBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVhLGlDQUFZLEdBQTFCLFVBQTJCLFNBQWlCLEVBQUUsVUFBNkYsRUFBRSxVQUE2QjtRQUE1SCwyQkFBQSxFQUFBLGlCQUE2RjtRQUFFLDJCQUFBLEVBQUEsaUJBQTZCO1FBQ3hLLEVBQUUsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxVQUFVLEVBQUU7WUFDOUMsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRTtnQkFDbkIsMEJBQTBCO2FBQzNCO1lBQ0QsSUFBSSxVQUFVO2dCQUFFLFVBQVUsRUFBRSxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVhLHlDQUFvQixHQUFsQyxVQUNFLEtBQWUsRUFDZixVQUE2RixFQUM3RixVQUE2QjtRQUQ3QiwyQkFBQSxFQUFBLGlCQUE2RjtRQUM3RiwyQkFBQSxFQUFBLGlCQUE2QjtRQUU3QixJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQ3JCLElBQUksVUFBVTtnQkFBRSxVQUFVLEVBQUUsQ0FBQztZQUM3QixPQUFPO1NBQ1I7UUFDRCxFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsVUFBVSxFQUFFLFVBQVUsR0FBRyxFQUFFLEtBQUs7WUFDMUQsSUFBSSxHQUFHLEVBQUU7Z0JBQ1AsZ0JBQWdCO2dCQUNoQixJQUFJLFVBQVU7b0JBQUUsVUFBVSxFQUFFLENBQUM7Z0JBQzdCLE9BQU87YUFDUjtZQUNELElBQUksVUFBVTtnQkFBRSxVQUFVLEVBQUUsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFYSwwQ0FBcUIsR0FBbkMsVUFDRSxLQUFlLEVBQ2YsVUFBNkYsRUFDN0YsVUFBNkI7UUFEN0IsMkJBQUEsRUFBQSxpQkFBNkY7UUFDN0YsMkJBQUEsRUFBQSxpQkFBNkI7UUFFN0IsSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUNyQixJQUFJLFVBQVU7Z0JBQUUsVUFBVSxFQUFFLENBQUM7WUFDN0IsT0FBTztTQUNSO1FBQ0QsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ2YsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUN6QixLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtZQUNqQixJQUFJLEdBQUcsc0JBQW9CLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1lBQ3BELEVBQUUsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxVQUFVLEdBQUcsRUFBRSxJQUFJO2dCQUNsRCxJQUFJLEdBQUcsRUFBRTtvQkFDUCxnQkFBZ0I7aUJBQ2pCO2dCQUNELE1BQU0sRUFBRSxDQUFDO2dCQUNULElBQUksVUFBVTtvQkFBRSxVQUFVLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDaEQsSUFBSSxNQUFNLElBQUksS0FBSyxFQUFFO29CQUNuQixJQUFJLFVBQVU7d0JBQUUsVUFBVSxFQUFFLENBQUM7aUJBQzlCO1lBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFYSw4QkFBUyxHQUF2QixVQUF3QixTQUFpQixFQUFFLFFBQTJCO1FBQTNCLHlCQUFBLEVBQUEsZUFBMkI7UUFDcEUsK0NBQStDO1FBQy9DLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxVQUFVLEdBQUcsRUFBRSxLQUFLO1lBQ25ELElBQUksR0FBRyxFQUFFO2dCQUNQLGdCQUFnQjtnQkFDaEIsNEVBQTRFO2dCQUM1RSxpSEFBaUg7Z0JBQ2pILGtDQUFrQztnQkFDbEMsTUFBTTthQUNQO1lBQ0QsSUFBSSxRQUFRO2dCQUFFLFFBQVEsRUFBRSxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7SUFuTE0scUNBQWdCLEdBQVcsRUFBRSxDQUFDO0lBRGxCLG9CQUFvQjtRQUR4QyxPQUFPO09BQ2Esb0JBQW9CLENBcUx4QztJQUFELDJCQUFDO0NBckxELEFBcUxDLElBQUE7a0JBckxvQixvQkFBb0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvUmVzb3VyY2VNYW5hZ2VyIHtcbiAgc3RhdGljIGFkZHJlc3NSZXNvdXJjZXM6IHN0cmluZyA9IFwiXCI7XG5cbiAgcHVibGljIHNldEFkZHJlc3NSZXNvdXJjZXMoYWRkcmVzc1Jlc291cmNlczogc3RyaW5nKXtcbiAgICBTaWNib1Jlc291cmNlTWFuYWdlci5hZGRyZXNzUmVzb3VyY2VzID0gYWRkcmVzc1Jlc291cmNlcztcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgbG9hZFNwaW5lRnJvbUZvcmdlQXNPYmoocmY6IGFueSwgb25Db21wbGV0ZTogKHJlc3VsdDogc3AuU2tlbGV0b25EYXRhKSA9PiB2b2lkKSB7XG4gICAgbGV0IGltYWdlID0gXCJcIjtcbiAgICBsZXQganNvbiA9IFwiXCI7XG4gICAgbGV0IGF0bGFzID0gXCJcIjtcbiAgICBsZXQgbmFtZVBuZyA9IFwiXCI7XG5cbiAgICByZi5maWxlc0xpc3QuZm9yRWFjaCgoZmlsZSkgPT4ge1xuICAgICAgc3dpdGNoIChmaWxlLnR5cGUpIHtcbiAgICAgICAgY2FzZSBcImltYWdlL3BuZ1wiOlxuICAgICAgICAgIGltYWdlID0gZmlsZS5wYXRoO1xuICAgICAgICAgIG5hbWVQbmcgPSBmaWxlLm5hbWU7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgXCJhcHBsaWNhdGlvbi9qc29uXCI6XG4gICAgICAgICAganNvbiA9IGZpbGUucGF0aDtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSBcInRleHQvcGxhaW47IGNoYXJzZXQ9dXRmLThcIjpcbiAgICAgICAgICBhdGxhcyA9IGZpbGUucGF0aDtcbiAgICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIGltYWdlID0gU2ljYm9SZXNvdXJjZU1hbmFnZXIuYWRkcmVzc1Jlc291cmNlcyArIGltYWdlO1xuICAgIGpzb24gPSBTaWNib1Jlc291cmNlTWFuYWdlci5hZGRyZXNzUmVzb3VyY2VzICsganNvbjtcbiAgICBhdGxhcyA9IFNpY2JvUmVzb3VyY2VNYW5hZ2VyLmFkZHJlc3NSZXNvdXJjZXMgKyBhdGxhcztcblxuICAgIHRoaXMubG9hZFJlbW90ZVNwaW5lKGltYWdlLCBqc29uLCBhdGxhcywgbmFtZVBuZywgb25Db21wbGV0ZSk7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGxvYWRSZW1vdGVTcGluZShpbWFnZTogc3RyaW5nLCBqc29uOiBzdHJpbmcsIGF0bGFzOiBzdHJpbmcsIG5hbWVQTkc6IHN0cmluZywgb25Db21wbGV0ZTogKHJlc3VsdDogc3AuU2tlbGV0b25EYXRhKSA9PiB2b2lkKSB7XG4gICAgY2MuYXNzZXRNYW5hZ2VyLmxvYWRSZW1vdGUoaW1hZ2UsIChlcnJvciwgdGV4dHVyZSkgPT4ge1xuICAgICAgY2MuYXNzZXRNYW5hZ2VyLmxvYWRBbnkoeyB1cmw6IGF0bGFzLCB0eXBlOiBcInR4dFwiIH0sIChlcnJvciwgYXRsYXNKc29uKSA9PiB7XG4gICAgICAgIGNjLmFzc2V0TWFuYWdlci5sb2FkQW55KHsgdXJsOiBqc29uLCB0eXBlOiBcInR4dFwiIH0sIChlcnJvciwgc3BpbmVKc29uKSA9PiB7XG4gICAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgICAvL2NjLmxvZyhlcnJvcik7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBhc3NldCA9IG5ldyBzcC5Ta2VsZXRvbkRhdGEoKTtcbiAgICAgICAgICBhc3NldC5za2VsZXRvbkpzb24gPSBzcGluZUpzb247XG4gICAgICAgICAgYXNzZXQuYXRsYXNUZXh0ID0gYXRsYXNKc29uO1xuICAgICAgICAgIGFzc2V0LnRleHR1cmVzID0gW3RleHR1cmVdO1xuICAgICAgICAgIGFzc2V0LnRleHR1cmVOYW1lcyA9IFtuYW1lUE5HXTtcbiAgICAgICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZShhc3NldCk7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuXG4gIHB1YmxpYyBzdGF0aWMgbG9hZFJlbW90ZUltYWdlV2l0aEZpbGVGaWVsZChzcHI6IGNjLlNwcml0ZSwgZmlsZUZpZWxkOiBhbnksIG9uQ29tcGxldGU6ICgpID0+IHZvaWQgPSBudWxsKSB7XG4gICAgaWYoc3ByKVxuICAgICAgc3ByLnNwcml0ZUZyYW1lID0gbnVsbDtcbiAgICB0aGlzLmxvYWRSZW1vdGVJbWFnZUJ5RmlsZUZpZWxkQXNPYmooZmlsZUZpZWxkLCAoc3ByaXRlRnJhbWUpID0+IHtcbiAgICAgIGlmIChzcHIpIHtcbiAgICAgICAgc3ByLnNwcml0ZUZyYW1lID0gc3ByaXRlRnJhbWU7XG4gICAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGxvYWRSZW1vdGVJbWFnZUJ5RmlsZUZpZWxkQXNPYmooZmlsZUZpZWxkOiBhbnksIG9uQ29tcGxldGU6IChyZXN1bHQpID0+IHZvaWQpIHtcbiAgICBpZiAoZmlsZUZpZWxkKSB7XG4gICAgICBsZXQgcmVtb3RlVXJsID0gU2ljYm9SZXNvdXJjZU1hbmFnZXIuYWRkcmVzc1Jlc291cmNlcyArIGZpbGVGaWVsZC5wYXRoO1xuICAgICAgY2MuYXNzZXRNYW5hZ2VyLmxvYWRSZW1vdGUocmVtb3RlVXJsLCBmdW5jdGlvbiAoZXJyLCByZXN1bHQpIHtcbiAgICAgICAgLy8vL2NjLmxvZyhcInVybDogXCIrcmVtb3RlVXJsKTtcbiAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgIC8vY2MuZXJyb3IoZXJyKTtcbiAgICAgICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZShudWxsKTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgbGV0IHJlcyA9IG5ldyBjYy5TcHJpdGVGcmFtZShyZXN1bHQpO1xuICAgICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZShyZXMpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKG51bGwpO1xuICAgIH1cbiAgfVxuXG4gIFxuICBwdWJsaWMgc3RhdGljIGxvYWRSZW1vdGVJbWFnZVdpdGhQYXRoKHNwcjogY2MuU3ByaXRlLCBwYXRoOiBzdHJpbmcsIG9uQ29tcGxldGU6ICgpID0+IHZvaWQgPSBudWxsLCByZXNldEZyYW1lOiBib29sZWFuID0gZmFsc2UpIHtcbiAgICBpZiAocmVzZXRGcmFtZSkgc3ByLnNwcml0ZUZyYW1lID0gbnVsbDtcblxuICAgIHRoaXMubG9hZEltYWdlRnJvbUZvcmdlKHBhdGgsIChzcHJpdGVGcmFtZSkgPT4ge1xuICAgICAgaWYgKHNwcikge1xuICAgICAgICBzcHIuc3ByaXRlRnJhbWUgPSBzcHJpdGVGcmFtZTtcbiAgICAgICAgaWYgKG9uQ29tcGxldGUpIG9uQ29tcGxldGUoKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgbG9hZEltYWdlRnJvbUZvcmdlKHBhdGg6IHN0cmluZywgb25Db21wbGV0ZTogKHJlc3VsdCkgPT4gdm9pZCkge1xuICAgIGlmIChwYXRoID09IFwiXCIgfHwgcGF0aCA9PSBcInVuZGVmaW5lZFwiIHx8IHBhdGggPT09IHVuZGVmaW5lZCkge1xuICAgICAgaWYgKG9uQ29tcGxldGUpIG9uQ29tcGxldGUobnVsbCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGxldCByZW1vdGVVcmwgPSBTaWNib1Jlc291cmNlTWFuYWdlci5hZGRyZXNzUmVzb3VyY2VzICsgcGF0aDtcbiAgICBjYy5hc3NldE1hbmFnZXIubG9hZFJlbW90ZShyZW1vdGVVcmwsIGZ1bmN0aW9uIChlcnIsIHJlc3VsdCkge1xuICAgICAgLy8vL2NjLmxvZyhcInVybDogXCIrcmVtb3RlVXJsKTtcbiAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgLy8vTG9nTWFuYWdlci5lcnJvcihlcnIpO1xuICAgICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZShudWxsKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgbGV0IHJlcyA9IG5ldyBjYy5TcHJpdGVGcmFtZShyZXN1bHQpO1xuICAgICAgaWYgKG9uQ29tcGxldGUpIG9uQ29tcGxldGUocmVzKTtcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgcHJlbG9hZFNjZW5lKG5hbWVTY2VuZTogc3RyaW5nLCBvblByb2dyZXNzOiAoZmluaXNoOiBudW1iZXIsIHRvdGFsOiBudW1iZXIsIGl0ZW06IGNjLkFzc2V0TWFuYWdlci5SZXF1ZXN0SXRlbSkgPT4gdm9pZCA9IG51bGwsIG9uQ29tcGxldGU6ICgpID0+IHZvaWQgPSBudWxsKSB7XG4gICAgY2MuZGlyZWN0b3IucHJlbG9hZFNjZW5lKG5hbWVTY2VuZSwgb25Qcm9ncmVzcywgZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKGNjLnN5cy5pc05hdGl2ZSkge1xuICAgICAgICAvLyBjYy5sb2FkZXIucmVsZWFzZUFsbCgpO1xuICAgICAgfVxuICAgICAgaWYgKG9uQ29tcGxldGUpIG9uQ29tcGxldGUoKTtcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgcHJlbG9hZFJlc291cmNlTG9jYWwoXG4gICAgcGF0aHM6IHN0cmluZ1tdLFxuICAgIG9uUHJvZ3Jlc3M6IChmaW5pc2g6IG51bWJlciwgdG90YWw6IG51bWJlciwgaXRlbTogY2MuQXNzZXRNYW5hZ2VyLlJlcXVlc3RJdGVtKSA9PiB2b2lkID0gbnVsbCxcbiAgICBvbkNvbXBsZXRlOiAoKSA9PiB2b2lkID0gbnVsbFxuICApIHtcbiAgICBpZiAocGF0aHMubGVuZ3RoID09IDApIHtcbiAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGNjLnJlc291cmNlcy5wcmVsb2FkKHBhdGhzLCBvblByb2dyZXNzLCBmdW5jdGlvbiAoZXJyLCBpdGVtcykge1xuICAgICAgaWYgKGVycikge1xuICAgICAgICAvL2NjLmVycm9yKGVycik7XG4gICAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKCk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKCk7XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIHByZWxvYWRSZXNvdXJjZVJlbW90ZShcbiAgICBwYXRoczogc3RyaW5nW10sXG4gICAgb25Qcm9ncmVzczogKGZpbmlzaDogbnVtYmVyLCB0b3RhbDogbnVtYmVyLCBpdGVtOiBjYy5Bc3NldE1hbmFnZXIuUmVxdWVzdEl0ZW0pID0+IHZvaWQgPSBudWxsLFxuICAgIG9uQ29tcGxldGU6ICgpID0+IHZvaWQgPSBudWxsXG4gICkge1xuICAgIGlmIChwYXRocy5sZW5ndGggPT0gMCkge1xuICAgICAgaWYgKG9uQ29tcGxldGUpIG9uQ29tcGxldGUoKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgbGV0IGZpbmlzaCA9IDA7XG4gICAgbGV0IHRvdGFsID0gcGF0aHMubGVuZ3RoO1xuICAgIHBhdGhzLmZvckVhY2goKHBhdGgpID0+IHtcbiAgICAgIHBhdGggPSBTaWNib1Jlc291cmNlTWFuYWdlci5hZGRyZXNzUmVzb3VyY2VzICsgcGF0aDtcbiAgICAgIGNjLmFzc2V0TWFuYWdlci5sb2FkUmVtb3RlKHBhdGgsIGZ1bmN0aW9uIChlcnIsIGl0ZW0pIHtcbiAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgIC8vY2MuZXJyb3IoZXJyKTtcbiAgICAgICAgfVxuICAgICAgICBmaW5pc2grKztcbiAgICAgICAgaWYgKG9uUHJvZ3Jlc3MpIG9uUHJvZ3Jlc3MoZmluaXNoLCB0b3RhbCwgaXRlbSk7XG4gICAgICAgIGlmIChmaW5pc2ggPT0gdG90YWwpIHtcbiAgICAgICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZSgpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgbG9hZFNjZW5lKHNjZW5lTmFtZTogc3RyaW5nLCBjYWxsYmFjazogKCkgPT4gdm9pZCA9IG51bGwpIHtcbiAgICAvLyBTb3VuZE1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5zdG9wQWxsRWZmZWN0cygpO1xuICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZShzY2VuZU5hbWUsIGZ1bmN0aW9uIChlcnIsIGFzc2V0KSB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIC8vY2MuZXJyb3IoZXJyKTtcbiAgICAgICAgLy9UT0RPIDogaGFuZGxlIGVycm9yIGxvYWQgc2NlbmUgOiBzdWdnZXN0IHVzZXIgcmVsb2FkIHBhZ2UgYW5kIGZvcmNlIHJlbG9hZFxuICAgICAgICAvLyBHYW1lVXRpbHMuc2hvd1BvcHVwTm90aWNlKHsgdGl0bGU6IGkxOG4udChcImNvbW1vbi5ub3RpY2VcIiksIHRleHQ6IGkxOG4udChcImNvbW1vbi5yZWxvYWRQYWdlXCIpIH0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgLy8gICB3aW5kb3cubG9jYXRpb24ucmVsb2FkKHRydWUpO1xuICAgICAgICAvLyB9KTtcbiAgICAgIH1cbiAgICAgIGlmIChjYWxsYmFjaykgY2FsbGJhY2soKTtcbiAgICB9KTtcbiAgfVxufVxuIl19