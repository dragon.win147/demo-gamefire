
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboSoundManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c43164Pn2tJXJvVH3wPdnJm', 'SicboSoundManager');
// Game/Sicbo_New/Scripts/Managers/SicboSoundManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var SicboModuleAdapter_1 = require("../..../../../../../SicboModuleAdapter");
var EventKey = SicboModuleAdapter_1.default.getAllRefs().EventKey;
var SicboLocalStorageManager_1 = require("./SicboLocalStorageManager");
var SicboSoundItem_1 = require("./SicboSoundItem");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboSoundManager = /** @class */ (function (_super) {
    __extends(SicboSoundManager, _super);
    function SicboSoundManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.soundMap = new Map();
        _this.KEY_EFFECT_VOLUME = EventKey.KEY_EFFECT_VOLUME;
        _this.KEY_EFFECT = EventKey.KEY_EFFECT;
        _this.KEY_MUSIC_VOLUME = EventKey.KEY_MUSIC_VOLUME;
        _this.KEY_MUSIC = EventKey.KEY_MUSIC;
        _this.isEnableMusic = true;
        _this.isEnableSfx = true;
        _this.musicVolume = 1;
        _this.sfxVolume = 1;
        return _this;
    }
    SicboSoundManager_1 = SicboSoundManager;
    SicboSoundManager.getInstance = function () {
        if (SicboSoundManager_1.instance == null) {
            var canvas = cc.director.getScene().getChildByName("Canvas");
            var node = canvas.getChildByName("SicboSoundManager");
            SicboSoundManager_1.instance = node.getComponent(SicboSoundManager_1);
        }
        return SicboSoundManager_1.instance;
    };
    SicboSoundManager.prototype.onLoad = function () {
        this.init();
        this.loadConfigFromFromStorage();
    };
    SicboSoundManager.prototype.init = function () {
        var sounds = this.node.getComponentsInChildren(SicboSoundItem_1.default);
        for (var i = 0; i < sounds.length; i++) {
            this.soundMap.set(sounds[i].node.name, sounds[i]);
        }
    };
    SicboSoundManager.prototype.play = function (soundName, from, autoStopDelay) {
        if (from === void 0) { from = 0; }
        if (autoStopDelay === void 0) { autoStopDelay = -1; }
        if (this.soundMap.has(soundName)) {
            this.soundMap.get(soundName).play(from, autoStopDelay);
        }
    };
    SicboSoundManager.prototype.stop = function (soundName) {
        if (this.soundMap.has(soundName)) {
            this.soundMap.get(soundName).stop();
        }
    };
    SicboSoundManager.prototype.loadConfigFromFromStorage = function () {
        this.isEnableMusic = this.getMusicStatusFromStorage();
        this.isEnableSfx = this.getSfxStatusFromStorage();
        this.musicVolume = this.getMusicVolumeFromStorage();
        this.sfxVolume = this.getSfxVolumeFromStorage();
        this.setMusicVolume(this.musicVolume);
        this.setSfxVolume(this.sfxVolume);
    };
    SicboSoundManager.prototype.setMusicStatus = function (isOn) {
        this.isEnableMusic = isOn;
        cc.audioEngine.setMusicVolume(this.MusicVolume);
        this.saveMusicStatusToStorage(isOn);
        cc.systemEvent.emit(EventKey.ON_SOUND_BG_EVENT, isOn);
    };
    SicboSoundManager.prototype.setMusicVolume = function (volume, isSave) {
        if (isSave === void 0) { isSave = true; }
        this.musicVolume = volume;
        cc.audioEngine.setMusicVolume(volume);
        if (!isSave)
            return;
        this.saveMusicVolumeToStorage(volume);
        var isOn = volume > 0;
        this.saveMusicStatusToStorage(isOn);
        cc.systemEvent.emit(EventKey.ON_SOUND_BG_EVENT, isOn);
    };
    Object.defineProperty(SicboSoundManager.prototype, "MusicVolume", {
        get: function () {
            return this.isEnableMusic ? this.musicVolume : 0;
        },
        enumerable: false,
        configurable: true
    });
    SicboSoundManager.prototype.setSfxVolume = function (volume, isSave) {
        if (isSave === void 0) { isSave = true; }
        this.sfxVolume = volume;
        cc.audioEngine.setEffectsVolume(volume);
        if (!isSave)
            return;
        this.saveSfxVolumeToStorage(volume);
        var isOn = this.sfxVolume > 0;
        this.saveSfxStatusToStorage(isOn);
        cc.systemEvent.emit(EventKey.ON_SOUND_EFF_EVENT, isOn);
    };
    SicboSoundManager.prototype.setSfxStatus = function (isOn) {
        this.isEnableSfx = isOn;
        cc.audioEngine.setEffectsVolume(this.SfxVolume);
        this.saveSfxStatusToStorage(isOn);
        cc.systemEvent.emit(EventKey.ON_SOUND_EFF_EVENT, isOn);
    };
    Object.defineProperty(SicboSoundManager.prototype, "SfxVolume", {
        get: function () {
            return this.isEnableSfx ? this.sfxVolume : 0;
        },
        enumerable: false,
        configurable: true
    });
    //SECTION Storage
    SicboSoundManager.prototype.getMusicStatusFromStorage = function () {
        return SicboLocalStorageManager_1.default.internalGetBoolean(this.KEY_MUSIC, true);
    };
    SicboSoundManager.prototype.saveMusicStatusToStorage = function (isOn) {
        SicboLocalStorageManager_1.default.internalSaveBoolean(this.KEY_MUSIC, isOn);
    };
    SicboSoundManager.prototype.getMusicVolumeFromStorage = function () {
        return Number.parseFloat(SicboLocalStorageManager_1.default.internalGetString(this.KEY_MUSIC_VOLUME, "1"));
    };
    SicboSoundManager.prototype.saveMusicVolumeToStorage = function (volume) {
        SicboLocalStorageManager_1.default.internalSaveString(this.KEY_MUSIC_VOLUME, volume.toString());
    };
    SicboSoundManager.prototype.getSfxStatusFromStorage = function () {
        return SicboLocalStorageManager_1.default.internalGetBoolean(this.KEY_EFFECT, true);
    };
    SicboSoundManager.prototype.saveSfxStatusToStorage = function (isOn) {
        SicboLocalStorageManager_1.default.internalSaveBoolean(this.KEY_EFFECT, isOn);
    };
    SicboSoundManager.prototype.getSfxVolumeFromStorage = function () {
        return Number.parseFloat(SicboLocalStorageManager_1.default.internalGetString(this.KEY_EFFECT_VOLUME, "1"));
    };
    SicboSoundManager.prototype.saveSfxVolumeToStorage = function (volume) {
        SicboLocalStorageManager_1.default.internalSaveString(this.KEY_EFFECT_VOLUME, volume.toString());
    };
    //!SECTION
    SicboSoundManager.prototype.pauseAllEffects = function () {
        cc.audioEngine.pauseAllEffects();
    };
    SicboSoundManager.prototype.resumeAllEffects = function () {
        cc.audioEngine.resumeAllEffects();
    };
    SicboSoundManager.prototype.onDestroy = function () {
        cc.audioEngine.stopAllEffects();
        delete SicboSoundManager_1.instance;
        SicboSoundManager_1.instance = null;
    };
    var SicboSoundManager_1;
    SicboSoundManager.instance = null;
    SicboSoundManager = SicboSoundManager_1 = __decorate([
        ccclass
    ], SicboSoundManager);
    return SicboSoundManager;
}(cc.Component));
exports.default = SicboSoundManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL01hbmFnZXJzL1NpY2JvU291bmRNYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGO0FBQ2xGLDZFQUF3RTtBQUd0RSxJQUFBLFFBQVEsR0FDTiw0QkFBa0IsQ0FBQyxVQUFVLEVBQUUsU0FEekIsQ0FDMEI7QUFDcEMsdUVBQWtFO0FBQ2xFLG1EQUE4QztBQUV4QyxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUErQyxxQ0FBWTtJQUEzRDtRQUFBLHFFQXVKQztRQXJKQyxjQUFRLEdBQWdDLElBQUksR0FBRyxFQUEwQixDQUFDO1FBR2xFLHVCQUFpQixHQUFXLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQztRQUN2RCxnQkFBVSxHQUFXLFFBQVEsQ0FBQyxVQUFVLENBQUM7UUFDekMsc0JBQWdCLEdBQVcsUUFBUSxDQUFDLGdCQUFnQixDQUFDO1FBQ3JELGVBQVMsR0FBVyxRQUFRLENBQUMsU0FBUyxDQUFDO1FBRXZDLG1CQUFhLEdBQVksSUFBSSxDQUFDO1FBQzlCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBQzVCLGlCQUFXLEdBQVcsQ0FBQyxDQUFDO1FBQ3hCLGVBQVMsR0FBVyxDQUFDLENBQUM7O0lBMEloQyxDQUFDOzBCQXZKb0IsaUJBQWlCO0lBZTdCLDZCQUFXLEdBQWxCO1FBQ0UsSUFBSSxtQkFBaUIsQ0FBQyxRQUFRLElBQUksSUFBSSxFQUFFO1lBQ3RDLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdELElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUN0RCxtQkFBaUIsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxtQkFBaUIsQ0FBQyxDQUFDO1NBQ25FO1FBQ0QsT0FBTyxtQkFBaUIsQ0FBQyxRQUFRLENBQUM7SUFDcEMsQ0FBQztJQUVELGtDQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDWixJQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQztJQUNuQyxDQUFDO0lBRUQsZ0NBQUksR0FBSjtRQUNFLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsd0JBQWMsQ0FBQyxDQUFDO1FBQy9ELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3RDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ25EO0lBQ0gsQ0FBQztJQUVELGdDQUFJLEdBQUosVUFBSyxTQUFpQixFQUFFLElBQWdCLEVBQUUsYUFBMEI7UUFBNUMscUJBQUEsRUFBQSxRQUFnQjtRQUFFLDhCQUFBLEVBQUEsaUJBQXlCLENBQUM7UUFDbEUsSUFBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsRUFBQztZQUM5QixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1NBQ3hEO0lBQ0gsQ0FBQztJQUVELGdDQUFJLEdBQUosVUFBSyxTQUFpQjtRQUNwQixJQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxFQUFDO1lBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ3JDO0lBQ0gsQ0FBQztJQUVPLHFEQUF5QixHQUFqQztRQUNFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7UUFDdEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztRQUNsRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7UUFFaEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELDBDQUFjLEdBQWQsVUFBZSxJQUFhO1FBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLEVBQUUsQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRCwwQ0FBYyxHQUFkLFVBQWUsTUFBYyxFQUFFLE1BQXNCO1FBQXRCLHVCQUFBLEVBQUEsYUFBc0I7UUFDbkQsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7UUFDMUIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEMsSUFBRyxDQUFDLE1BQU07WUFBRSxPQUFPO1FBRW5CLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0QyxJQUFJLElBQUksR0FBRyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVELHNCQUFJLDBDQUFXO2FBQWY7WUFDRSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqRCxDQUFDOzs7T0FBQTtJQUVELHdDQUFZLEdBQVosVUFBYSxNQUFjLEVBQUUsTUFBc0I7UUFBdEIsdUJBQUEsRUFBQSxhQUFzQjtRQUNqRCxJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQztRQUN4QixFQUFFLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hDLElBQUcsQ0FBQyxNQUFNO1lBQUUsT0FBTztRQUVuQixJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7UUFDOUIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xDLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQsd0NBQVksR0FBWixVQUFhLElBQWE7UUFDeEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDeEIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xDLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQsc0JBQUksd0NBQVM7YUFBYjtZQUNFLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdDLENBQUM7OztPQUFBO0lBRUQsaUJBQWlCO0lBQ2pCLHFEQUF5QixHQUF6QjtRQUNFLE9BQU8sa0NBQXdCLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMzRSxDQUFDO0lBRUQsb0RBQXdCLEdBQXhCLFVBQXlCLElBQWE7UUFDcEMsa0NBQXdCLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNyRSxDQUFDO0lBRUQscURBQXlCLEdBQXpCO1FBQ0UsT0FBTyxNQUFNLENBQUMsVUFBVSxDQUFDLGtDQUF3QixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ25HLENBQUM7SUFFRCxvREFBd0IsR0FBeEIsVUFBeUIsTUFBYztRQUNyQyxrQ0FBd0IsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDeEYsQ0FBQztJQUVELG1EQUF1QixHQUF2QjtRQUNFLE9BQU8sa0NBQXdCLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBRUQsa0RBQXNCLEdBQXRCLFVBQXVCLElBQWE7UUFDbEMsa0NBQXdCLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN0RSxDQUFDO0lBRUQsbURBQXVCLEdBQXZCO1FBQ0UsT0FBTyxNQUFNLENBQUMsVUFBVSxDQUFDLGtDQUF3QixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3BHLENBQUM7SUFFRCxrREFBc0IsR0FBdEIsVUFBdUIsTUFBYztRQUNuQyxrQ0FBd0IsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDekYsQ0FBQztJQUNELFVBQVU7SUFHSCwyQ0FBZSxHQUF0QjtRQUNFLEVBQUUsQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVNLDRDQUFnQixHQUF2QjtRQUNFLEVBQUUsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUNwQyxDQUFDO0lBRUQscUNBQVMsR0FBVDtRQUNFLEVBQUUsQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFaEMsT0FBTyxtQkFBaUIsQ0FBQyxRQUFRLENBQUM7UUFDbEMsbUJBQWlCLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUNwQyxDQUFDOztJQXJKYywwQkFBUSxHQUFzQixJQUFJLENBQUM7SUFEL0IsaUJBQWlCO1FBRHJDLE9BQU87T0FDYSxpQkFBaUIsQ0F1SnJDO0lBQUQsd0JBQUM7Q0F2SkQsQUF1SkMsQ0F2SjhDLEVBQUUsQ0FBQyxTQUFTLEdBdUoxRDtrQkF2Sm9CLGlCQUFpQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuaW1wb3J0IFNpY2JvTW9kdWxlQWRhcHRlciBmcm9tIFwiLi4vLi4uLi8uLi8uLi8uLi8uLi9TaWNib01vZHVsZUFkYXB0ZXJcIjtcblxuY29uc3Qge1xuICBFdmVudEtleSxcbn0gPSBTaWNib01vZHVsZUFkYXB0ZXIuZ2V0QWxsUmVmcygpO1xuaW1wb3J0IFNpY2JvTG9jYWxTdG9yYWdlTWFuYWdlciBmcm9tIFwiLi9TaWNib0xvY2FsU3RvcmFnZU1hbmFnZXJcIjtcbmltcG9ydCBTaWNib1NvdW5kSXRlbSBmcm9tIFwiLi9TaWNib1NvdW5kSXRlbVwiO1xuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvU291bmRNYW5hZ2VyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IFNpY2JvU291bmRNYW5hZ2VyID0gbnVsbDtcbiAgc291bmRNYXA6IE1hcDxzdHJpbmcsIFNpY2JvU291bmRJdGVtPiA9IG5ldyBNYXA8c3RyaW5nLCBTaWNib1NvdW5kSXRlbT4oKTtcblxuXG4gIHByaXZhdGUgS0VZX0VGRkVDVF9WT0xVTUU6IHN0cmluZyA9IEV2ZW50S2V5LktFWV9FRkZFQ1RfVk9MVU1FO1xuICBwcml2YXRlIEtFWV9FRkZFQ1Q6IHN0cmluZyA9IEV2ZW50S2V5LktFWV9FRkZFQ1Q7XG4gIHByaXZhdGUgS0VZX01VU0lDX1ZPTFVNRTogc3RyaW5nID0gRXZlbnRLZXkuS0VZX01VU0lDX1ZPTFVNRTtcbiAgcHJpdmF0ZSBLRVlfTVVTSUM6IHN0cmluZyA9IEV2ZW50S2V5LktFWV9NVVNJQztcbiAgXG4gIHByaXZhdGUgaXNFbmFibGVNdXNpYzogYm9vbGVhbiA9IHRydWU7XG4gIHByaXZhdGUgaXNFbmFibGVTZng6IGJvb2xlYW4gPSB0cnVlO1xuICBwcml2YXRlIG11c2ljVm9sdW1lOiBudW1iZXIgPSAxO1xuICBwcml2YXRlIHNmeFZvbHVtZTogbnVtYmVyID0gMTtcblxuICBzdGF0aWMgZ2V0SW5zdGFuY2UoKTogU2ljYm9Tb3VuZE1hbmFnZXIge1xuICAgIGlmIChTaWNib1NvdW5kTWFuYWdlci5pbnN0YW5jZSA9PSBudWxsKSB7XG4gICAgICBsZXQgY2FudmFzID0gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShcIkNhbnZhc1wiKTtcbiAgICAgIGxldCBub2RlID0gY2FudmFzLmdldENoaWxkQnlOYW1lKFwiU2ljYm9Tb3VuZE1hbmFnZXJcIik7XG4gICAgICBTaWNib1NvdW5kTWFuYWdlci5pbnN0YW5jZSA9IG5vZGUuZ2V0Q29tcG9uZW50KFNpY2JvU291bmRNYW5hZ2VyKTtcbiAgICB9XG4gICAgcmV0dXJuIFNpY2JvU291bmRNYW5hZ2VyLmluc3RhbmNlO1xuICB9XG5cbiAgb25Mb2FkKCl7XG4gICAgdGhpcy5pbml0KCk7XG4gICAgdGhpcy5sb2FkQ29uZmlnRnJvbUZyb21TdG9yYWdlKCk7XG4gIH1cblxuICBpbml0KCl7XG4gICAgbGV0IHNvdW5kcyA9IHRoaXMubm9kZS5nZXRDb21wb25lbnRzSW5DaGlsZHJlbihTaWNib1NvdW5kSXRlbSk7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBzb3VuZHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMuc291bmRNYXAuc2V0KHNvdW5kc1tpXS5ub2RlLm5hbWUsIHNvdW5kc1tpXSk7XG4gICAgfVxuICB9XG5cbiAgcGxheShzb3VuZE5hbWU6IHN0cmluZywgZnJvbTogbnVtYmVyID0gMCwgYXV0b1N0b3BEZWxheTogbnVtYmVyID0gLTEpe1xuICAgIGlmKHRoaXMuc291bmRNYXAuaGFzKHNvdW5kTmFtZSkpe1xuICAgICAgdGhpcy5zb3VuZE1hcC5nZXQoc291bmROYW1lKS5wbGF5KGZyb20sIGF1dG9TdG9wRGVsYXkpO1xuICAgIH1cbiAgfVxuXG4gIHN0b3Aoc291bmROYW1lOiBzdHJpbmcpe1xuICAgIGlmKHRoaXMuc291bmRNYXAuaGFzKHNvdW5kTmFtZSkpe1xuICAgICAgdGhpcy5zb3VuZE1hcC5nZXQoc291bmROYW1lKS5zdG9wKCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBsb2FkQ29uZmlnRnJvbUZyb21TdG9yYWdlKCkge1xuICAgIHRoaXMuaXNFbmFibGVNdXNpYyA9IHRoaXMuZ2V0TXVzaWNTdGF0dXNGcm9tU3RvcmFnZSgpO1xuICAgIHRoaXMuaXNFbmFibGVTZnggPSB0aGlzLmdldFNmeFN0YXR1c0Zyb21TdG9yYWdlKCk7XG4gICAgdGhpcy5tdXNpY1ZvbHVtZSA9IHRoaXMuZ2V0TXVzaWNWb2x1bWVGcm9tU3RvcmFnZSgpO1xuICAgIHRoaXMuc2Z4Vm9sdW1lID0gdGhpcy5nZXRTZnhWb2x1bWVGcm9tU3RvcmFnZSgpO1xuXG4gICAgdGhpcy5zZXRNdXNpY1ZvbHVtZSh0aGlzLm11c2ljVm9sdW1lKTtcbiAgICB0aGlzLnNldFNmeFZvbHVtZSh0aGlzLnNmeFZvbHVtZSk7XG4gIH1cblxuICBzZXRNdXNpY1N0YXR1cyhpc09uOiBib29sZWFuKXtcbiAgICB0aGlzLmlzRW5hYmxlTXVzaWMgPSBpc09uO1xuICAgIGNjLmF1ZGlvRW5naW5lLnNldE11c2ljVm9sdW1lKHRoaXMuTXVzaWNWb2x1bWUpO1xuICAgIHRoaXMuc2F2ZU11c2ljU3RhdHVzVG9TdG9yYWdlKGlzT24pO1xuICAgIGNjLnN5c3RlbUV2ZW50LmVtaXQoRXZlbnRLZXkuT05fU09VTkRfQkdfRVZFTlQsIGlzT24pO1xuICB9XG5cbiAgc2V0TXVzaWNWb2x1bWUodm9sdW1lOiBudW1iZXIsIGlzU2F2ZTogYm9vbGVhbiA9IHRydWUpe1xuICAgIHRoaXMubXVzaWNWb2x1bWUgPSB2b2x1bWU7XG4gICAgY2MuYXVkaW9FbmdpbmUuc2V0TXVzaWNWb2x1bWUodm9sdW1lKTtcbiAgICBpZighaXNTYXZlKSByZXR1cm47IFxuICAgIFxuICAgIHRoaXMuc2F2ZU11c2ljVm9sdW1lVG9TdG9yYWdlKHZvbHVtZSk7XG4gICAgbGV0IGlzT24gPSB2b2x1bWUgPiAwO1xuICAgIHRoaXMuc2F2ZU11c2ljU3RhdHVzVG9TdG9yYWdlKGlzT24pO1xuICAgIGNjLnN5c3RlbUV2ZW50LmVtaXQoRXZlbnRLZXkuT05fU09VTkRfQkdfRVZFTlQsIGlzT24pO1xuICB9XG5cbiAgZ2V0IE11c2ljVm9sdW1lKCl7XG4gICAgcmV0dXJuIHRoaXMuaXNFbmFibGVNdXNpYz8gdGhpcy5tdXNpY1ZvbHVtZTogMDtcbiAgfVxuXG4gIHNldFNmeFZvbHVtZSh2b2x1bWU6IG51bWJlciwgaXNTYXZlOiBib29sZWFuID0gdHJ1ZSl7XG4gICAgdGhpcy5zZnhWb2x1bWUgPSB2b2x1bWU7XG4gICAgY2MuYXVkaW9FbmdpbmUuc2V0RWZmZWN0c1ZvbHVtZSh2b2x1bWUpO1xuICAgIGlmKCFpc1NhdmUpIHJldHVybjtcbiAgICBcbiAgICB0aGlzLnNhdmVTZnhWb2x1bWVUb1N0b3JhZ2Uodm9sdW1lKTtcbiAgICBsZXQgaXNPbiA9IHRoaXMuc2Z4Vm9sdW1lID4gMDtcbiAgICB0aGlzLnNhdmVTZnhTdGF0dXNUb1N0b3JhZ2UoaXNPbik7XG4gICAgY2Muc3lzdGVtRXZlbnQuZW1pdChFdmVudEtleS5PTl9TT1VORF9FRkZfRVZFTlQsIGlzT24pO1xuICB9XG5cbiAgc2V0U2Z4U3RhdHVzKGlzT246IGJvb2xlYW4pe1xuICAgIHRoaXMuaXNFbmFibGVTZnggPSBpc09uO1xuICAgIGNjLmF1ZGlvRW5naW5lLnNldEVmZmVjdHNWb2x1bWUodGhpcy5TZnhWb2x1bWUpO1xuICAgIHRoaXMuc2F2ZVNmeFN0YXR1c1RvU3RvcmFnZShpc09uKTtcbiAgICBjYy5zeXN0ZW1FdmVudC5lbWl0KEV2ZW50S2V5Lk9OX1NPVU5EX0VGRl9FVkVOVCwgaXNPbik7XG4gIH1cblxuICBnZXQgU2Z4Vm9sdW1lKCl7XG4gICAgcmV0dXJuIHRoaXMuaXNFbmFibGVTZng/IHRoaXMuc2Z4Vm9sdW1lOiAwO1xuICB9XG5cbiAgLy9TRUNUSU9OIFN0b3JhZ2VcbiAgZ2V0TXVzaWNTdGF0dXNGcm9tU3RvcmFnZSgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gU2ljYm9Mb2NhbFN0b3JhZ2VNYW5hZ2VyLmludGVybmFsR2V0Qm9vbGVhbih0aGlzLktFWV9NVVNJQywgdHJ1ZSk7XG4gIH1cblxuICBzYXZlTXVzaWNTdGF0dXNUb1N0b3JhZ2UoaXNPbjogYm9vbGVhbil7XG4gICAgU2ljYm9Mb2NhbFN0b3JhZ2VNYW5hZ2VyLmludGVybmFsU2F2ZUJvb2xlYW4odGhpcy5LRVlfTVVTSUMsIGlzT24pO1xuICB9XG5cbiAgZ2V0TXVzaWNWb2x1bWVGcm9tU3RvcmFnZSgpOiBudW1iZXIge1xuICAgIHJldHVybiBOdW1iZXIucGFyc2VGbG9hdChTaWNib0xvY2FsU3RvcmFnZU1hbmFnZXIuaW50ZXJuYWxHZXRTdHJpbmcodGhpcy5LRVlfTVVTSUNfVk9MVU1FLCBcIjFcIikpO1xuICB9XG5cbiAgc2F2ZU11c2ljVm9sdW1lVG9TdG9yYWdlKHZvbHVtZTogbnVtYmVyKTogdm9pZCB7XG4gICAgU2ljYm9Mb2NhbFN0b3JhZ2VNYW5hZ2VyLmludGVybmFsU2F2ZVN0cmluZyh0aGlzLktFWV9NVVNJQ19WT0xVTUUsIHZvbHVtZS50b1N0cmluZygpKTtcbiAgfVxuXG4gIGdldFNmeFN0YXR1c0Zyb21TdG9yYWdlKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBTaWNib0xvY2FsU3RvcmFnZU1hbmFnZXIuaW50ZXJuYWxHZXRCb29sZWFuKHRoaXMuS0VZX0VGRkVDVCwgdHJ1ZSk7XG4gIH1cblxuICBzYXZlU2Z4U3RhdHVzVG9TdG9yYWdlKGlzT246IGJvb2xlYW4pe1xuICAgIFNpY2JvTG9jYWxTdG9yYWdlTWFuYWdlci5pbnRlcm5hbFNhdmVCb29sZWFuKHRoaXMuS0VZX0VGRkVDVCwgaXNPbik7XG4gIH1cblxuICBnZXRTZnhWb2x1bWVGcm9tU3RvcmFnZSgpOiBudW1iZXIge1xuICAgIHJldHVybiBOdW1iZXIucGFyc2VGbG9hdChTaWNib0xvY2FsU3RvcmFnZU1hbmFnZXIuaW50ZXJuYWxHZXRTdHJpbmcodGhpcy5LRVlfRUZGRUNUX1ZPTFVNRSwgXCIxXCIpKTtcbiAgfVxuICBcbiAgc2F2ZVNmeFZvbHVtZVRvU3RvcmFnZSh2b2x1bWU6IG51bWJlcik6IHZvaWQge1xuICAgIFNpY2JvTG9jYWxTdG9yYWdlTWFuYWdlci5pbnRlcm5hbFNhdmVTdHJpbmcodGhpcy5LRVlfRUZGRUNUX1ZPTFVNRSwgdm9sdW1lLnRvU3RyaW5nKCkpO1xuICB9XG4gIC8vIVNFQ1RJT05cblxuICBcbiAgcHVibGljIHBhdXNlQWxsRWZmZWN0cygpOiB2b2lkIHtcbiAgICBjYy5hdWRpb0VuZ2luZS5wYXVzZUFsbEVmZmVjdHMoKTtcbiAgfVxuXG4gIHB1YmxpYyByZXN1bWVBbGxFZmZlY3RzKCk6IHZvaWQge1xuICAgIGNjLmF1ZGlvRW5naW5lLnJlc3VtZUFsbEVmZmVjdHMoKTtcbiAgfVxuICBcbiAgb25EZXN0cm95KCl7XG4gICAgY2MuYXVkaW9FbmdpbmUuc3RvcEFsbEVmZmVjdHMoKTtcbiAgICBcbiAgICBkZWxldGUgU2ljYm9Tb3VuZE1hbmFnZXIuaW5zdGFuY2U7XG4gICAgU2ljYm9Tb3VuZE1hbmFnZXIuaW5zdGFuY2UgPSBudWxsO1xuICB9XG59XG4iXX0=