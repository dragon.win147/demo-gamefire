
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboLocalStorageManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '05744F843tEyLYeZw5Aw4tK', 'SicboLocalStorageManager');
// Game/Sicbo_New/Scripts/Managers/SicboLocalStorageManager.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboLocalStorageManager = /** @class */ (function () {
    function SicboLocalStorageManager() {
    }
    SicboLocalStorageManager.internalGetString = function (key, defaultValue) {
        if (defaultValue === void 0) { defaultValue = ""; }
        var isValue = cc.sys.localStorage.getItem(key);
        return isValue == undefined ? defaultValue : isValue;
    };
    SicboLocalStorageManager.internalSaveString = function (key, strValue) {
        if (strValue === void 0) { strValue = ""; }
        if (strValue == undefined) {
            cc.sys.localStorage.removeItem(key);
        }
        else {
            cc.sys.localStorage.setItem(key, strValue);
        }
    };
    SicboLocalStorageManager.internalSaveBoolean = function (key, defaultValue) {
        if (defaultValue === void 0) { defaultValue = true; }
        if (defaultValue == undefined) {
            cc.sys.localStorage.removeItem(key);
        }
        else {
            cc.sys.localStorage.setItem(key, defaultValue);
        }
    };
    SicboLocalStorageManager.internalGetBoolean = function (key, defaultValue) {
        if (defaultValue === void 0) { defaultValue = true; }
        var isValue = cc.sys.localStorage.getItem(key);
        if (isValue == undefined) {
            isValue = defaultValue;
        }
        if (typeof isValue === "string") {
            return isValue === "true";
        }
        else {
            return isValue;
        }
    };
    SicboLocalStorageManager = __decorate([
        ccclass
    ], SicboLocalStorageManager);
    return SicboLocalStorageManager;
}());
exports.default = SicboLocalStorageManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL01hbmFnZXJzL1NpY2JvTG9jYWxTdG9yYWdlTWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7O0FBRTVFLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQUE7SUFrQ0EsQ0FBQztJQWpDZSwwQ0FBaUIsR0FBL0IsVUFBZ0MsR0FBVyxFQUFFLFlBQXlCO1FBQXpCLDZCQUFBLEVBQUEsaUJBQXlCO1FBQ3BFLElBQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVqRCxPQUFPLE9BQU8sSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO0lBQ3ZELENBQUM7SUFFYSwyQ0FBa0IsR0FBaEMsVUFBaUMsR0FBVyxFQUFFLFFBQXFCO1FBQXJCLHlCQUFBLEVBQUEsYUFBcUI7UUFDakUsSUFBSSxRQUFRLElBQUksU0FBUyxFQUFFO1lBQ3pCLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNyQzthQUFNO1lBQ0wsRUFBRSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUM1QztJQUNILENBQUM7SUFFYSw0Q0FBbUIsR0FBakMsVUFBa0MsR0FBVyxFQUFFLFlBQTRCO1FBQTVCLDZCQUFBLEVBQUEsbUJBQTRCO1FBQ3pFLElBQUksWUFBWSxJQUFJLFNBQVMsRUFBRTtZQUM3QixFQUFFLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDckM7YUFBTTtZQUNMLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUM7U0FDaEQ7SUFDSCxDQUFDO0lBRWEsMkNBQWtCLEdBQWhDLFVBQWlDLEdBQVcsRUFBRSxZQUE0QjtRQUE1Qiw2QkFBQSxFQUFBLG1CQUE0QjtRQUN4RSxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0MsSUFBSSxPQUFPLElBQUksU0FBUyxFQUFFO1lBQ3hCLE9BQU8sR0FBRyxZQUFZLENBQUM7U0FDeEI7UUFDRCxJQUFJLE9BQU8sT0FBTyxLQUFLLFFBQVEsRUFBRTtZQUMvQixPQUFPLE9BQU8sS0FBSyxNQUFNLENBQUM7U0FDM0I7YUFBTTtZQUNMLE9BQU8sT0FBTyxDQUFDO1NBQ2hCO0lBQ0gsQ0FBQztJQWpDa0Isd0JBQXdCO1FBRDVDLE9BQU87T0FDYSx3QkFBd0IsQ0FrQzVDO0lBQUQsK0JBQUM7Q0FsQ0QsQUFrQ0MsSUFBQTtrQkFsQ29CLHdCQUF3QiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9Mb2NhbFN0b3JhZ2VNYW5hZ2VyIHtcbiAgcHVibGljIHN0YXRpYyBpbnRlcm5hbEdldFN0cmluZyhrZXk6IHN0cmluZywgZGVmYXVsdFZhbHVlOiBzdHJpbmcgPSBcIlwiKTogc3RyaW5nIHtcbiAgICBjb25zdCBpc1ZhbHVlID0gY2Muc3lzLmxvY2FsU3RvcmFnZS5nZXRJdGVtKGtleSk7XG5cbiAgICByZXR1cm4gaXNWYWx1ZSA9PSB1bmRlZmluZWQgPyBkZWZhdWx0VmFsdWUgOiBpc1ZhbHVlO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBpbnRlcm5hbFNhdmVTdHJpbmcoa2V5OiBzdHJpbmcsIHN0clZhbHVlOiBzdHJpbmcgPSBcIlwiKTogdm9pZCB7XG4gICAgaWYgKHN0clZhbHVlID09IHVuZGVmaW5lZCkge1xuICAgICAgY2Muc3lzLmxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKGtleSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNjLnN5cy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbShrZXksIHN0clZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGludGVybmFsU2F2ZUJvb2xlYW4oa2V5OiBzdHJpbmcsIGRlZmF1bHRWYWx1ZTogYm9vbGVhbiA9IHRydWUpOiB2b2lkIHtcbiAgICBpZiAoZGVmYXVsdFZhbHVlID09IHVuZGVmaW5lZCkge1xuICAgICAgY2Muc3lzLmxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKGtleSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNjLnN5cy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbShrZXksIGRlZmF1bHRWYWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIHN0YXRpYyBpbnRlcm5hbEdldEJvb2xlYW4oa2V5OiBzdHJpbmcsIGRlZmF1bHRWYWx1ZTogYm9vbGVhbiA9IHRydWUpOiBib29sZWFuIHtcbiAgICBsZXQgaXNWYWx1ZSA9IGNjLnN5cy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbShrZXkpO1xuICAgIGlmIChpc1ZhbHVlID09IHVuZGVmaW5lZCkge1xuICAgICAgaXNWYWx1ZSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gICAgaWYgKHR5cGVvZiBpc1ZhbHVlID09PSBcInN0cmluZ1wiKSB7XG4gICAgICByZXR1cm4gaXNWYWx1ZSA9PT0gXCJ0cnVlXCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBpc1ZhbHVlO1xuICAgIH1cbiAgfVxufVxuIl19