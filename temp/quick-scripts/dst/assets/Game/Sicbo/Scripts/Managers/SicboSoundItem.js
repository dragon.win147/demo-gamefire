
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboSoundItem.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '448c7+jRtNEgqH/EFBo4zhg', 'SicboSoundItem');
// Game/Sicbo_New/Scripts/Managers/SicboSoundItem.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboSoundType = void 0;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboSoundType;
(function (SicboSoundType) {
    SicboSoundType[SicboSoundType["Background"] = 0] = "Background";
    SicboSoundType[SicboSoundType["Effect_Single"] = 1] = "Effect_Single";
    SicboSoundType[SicboSoundType["Effect_Multiple"] = 2] = "Effect_Multiple";
    SicboSoundType[SicboSoundType["Effect_Loop_AutoStop"] = 3] = "Effect_Loop_AutoStop"; //loop sfx va tu dong tat sau n giay
})(SicboSoundType = exports.SicboSoundType || (exports.SicboSoundType = {}));
var SicboSoundItem = /** @class */ (function (_super) {
    __extends(SicboSoundItem, _super);
    function SicboSoundItem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.audioSource = null;
        _this.soundType = SicboSoundType.Background;
        _this.autoStopDelay = -1;
        _this.audioId = -1;
        return _this;
    }
    SicboSoundItem.prototype.play = function (from, autoStopDelay) {
        if (from === void 0) { from = 0; }
        if (autoStopDelay === void 0) { autoStopDelay = -1; }
        this.autoStopDelay = autoStopDelay;
        switch (this.soundType) {
            case SicboSoundType.Background:
                if (this.isValidAudioId() && cc.audioEngine.getState(this.audioId) == cc.audioEngine.AudioState.PLAYING)
                    return;
                this.audioId = cc.audioEngine.playMusic(this.audioSource.clip, true);
                break;
            case SicboSoundType.Effect_Single:
                if (this.isValidAudioId() && cc.audioEngine.getState(this.audioId) == cc.audioEngine.AudioState.PLAYING)
                    return;
                this.audioId = cc.audioEngine.playEffect(this.audioSource.clip, false);
                var durationES = cc.audioEngine.getDuration(this.audioId);
                cc.audioEngine.setCurrentTime(this.audioId, Math.min(from, durationES));
                break;
            case SicboSoundType.Effect_Loop_AutoStop:
                if (this.isValidAudioId() && cc.audioEngine.getState(this.audioId) == cc.audioEngine.AudioState.PLAYING)
                    return;
                this.audioId = cc.audioEngine.playEffect(this.audioSource.clip, true);
                var durationLAE = cc.audioEngine.getDuration(this.audioId);
                cc.audioEngine.setCurrentTime(this.audioId, Math.min(from, durationLAE));
                var self_1 = this;
                if (self_1.autoStopDelay > 0) {
                    cc.tween(self_1.node)
                        .delay(self_1.autoStopDelay)
                        .call(function () {
                        self_1.stop();
                    }).start();
                }
                break;
            case SicboSoundType.Effect_Multiple:
                break;
        }
        // //cc.error(this.audioId);
    };
    SicboSoundItem.prototype.stop = function () {
        if (this.isValidAudioId()) {
            cc.audioEngine.stop(this.audioId);
            cc.Tween.stopAllByTarget(this.node);
        }
    };
    SicboSoundItem.prototype.isValidAudioId = function () {
        return this.audioId >= 0;
    };
    SicboSoundItem.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.AudioSource)
    ], SicboSoundItem.prototype, "audioSource", void 0);
    __decorate([
        property({ type: cc.Enum(SicboSoundType) })
    ], SicboSoundItem.prototype, "soundType", void 0);
    __decorate([
        property()
    ], SicboSoundItem.prototype, "autoStopDelay", void 0);
    SicboSoundItem = __decorate([
        ccclass
    ], SicboSoundItem);
    return SicboSoundItem;
}(cc.Component));
exports.default = SicboSoundItem;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL01hbmFnZXJzL1NpY2JvU291bmRJdGVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUc1RSxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUMxQyxJQUFZLGNBS1g7QUFMRCxXQUFZLGNBQWM7SUFDekIsK0RBQVUsQ0FBQTtJQUNWLHFFQUFhLENBQUE7SUFDYix5RUFBZSxDQUFBO0lBQ1osbUZBQW9CLENBQUEsQ0FBQyxvQ0FBb0M7QUFDN0QsQ0FBQyxFQUxXLGNBQWMsR0FBZCxzQkFBYyxLQUFkLHNCQUFjLFFBS3pCO0FBSUQ7SUFBNEMsa0NBQVk7SUFBeEQ7UUFBQSxxRUErREM7UUE3REcsaUJBQVcsR0FBbUIsSUFBSSxDQUFDO1FBR25DLGVBQVMsR0FBbUIsY0FBYyxDQUFDLFVBQVUsQ0FBQztRQUd0RCxtQkFBYSxHQUFXLENBQUMsQ0FBQyxDQUFDO1FBRTNCLGFBQU8sR0FBVyxDQUFDLENBQUMsQ0FBQzs7SUFxRHpCLENBQUM7SUFuREcsNkJBQUksR0FBSixVQUFLLElBQWdCLEVBQUUsYUFBMEI7UUFBNUMscUJBQUEsRUFBQSxRQUFnQjtRQUFFLDhCQUFBLEVBQUEsaUJBQXlCLENBQUM7UUFDN0MsSUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7UUFDbkMsUUFBTyxJQUFJLENBQUMsU0FBUyxFQUFDO1lBQ2xCLEtBQUssY0FBYyxDQUFDLFVBQVU7Z0JBQzFCLElBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxPQUFPO29CQUFFLE9BQU87Z0JBQy9HLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3JFLE1BQU07WUFFVixLQUFLLGNBQWMsQ0FBQyxhQUFhO2dCQUM3QixJQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxFQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsT0FBTztvQkFBRSxPQUFPO2dCQUMvRyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN2RSxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzFELEVBQUUsQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDeEUsTUFBTTtZQUVWLEtBQUssY0FBYyxDQUFDLG9CQUFvQjtnQkFDcEMsSUFBRyxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLE9BQU87b0JBQUUsT0FBTztnQkFDL0csSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDdEUsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUMzRCxFQUFFLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBRXpFLElBQUksTUFBSSxHQUFHLElBQUksQ0FBQztnQkFDaEIsSUFBRyxNQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsRUFBQztvQkFDdEIsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFJLENBQUMsSUFBSSxDQUFDO3lCQUNkLEtBQUssQ0FBQyxNQUFJLENBQUMsYUFBYSxDQUFDO3lCQUN6QixJQUFJLENBQUM7d0JBQ0YsTUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUNoQixDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDbEI7Z0JBQ0QsTUFBTTtZQUVWLEtBQUssY0FBYyxDQUFDLGVBQWU7Z0JBQy9CLE1BQU07U0FDYjtRQUNELDRCQUE0QjtJQUNoQyxDQUFDO0lBRUQsNkJBQUksR0FBSjtRQUNJLElBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxFQUFDO1lBQ3JCLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNsQyxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkM7SUFDTCxDQUFDO0lBRUQsdUNBQWMsR0FBZDtRQUNJLE9BQU8sSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVELGtDQUFTLEdBQVQ7UUFDSSxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQTVERDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO3VEQUNVO0lBR25DO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztxREFDVTtJQUd0RDtRQURDLFFBQVEsRUFBRTt5REFDZ0I7SUFSVixjQUFjO1FBRGxDLE9BQU87T0FDYSxjQUFjLENBK0RsQztJQUFELHFCQUFDO0NBL0RELEFBK0RDLENBL0QyQyxFQUFFLENBQUMsU0FBUyxHQStEdkQ7a0JBL0RvQixjQUFjIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5leHBvcnQgZW51bSBTaWNib1NvdW5kVHlwZXtcblx0QmFja2dyb3VuZCxcblx0RWZmZWN0X1NpbmdsZSxcblx0RWZmZWN0X011bHRpcGxlLFxuICAgIEVmZmVjdF9Mb29wX0F1dG9TdG9wIC8vbG9vcCBzZnggdmEgdHUgZG9uZyB0YXQgc2F1IG4gZ2lheVxufVxuXG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1NvdW5kSXRlbSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KGNjLkF1ZGlvU291cmNlKVxuICAgIGF1ZGlvU291cmNlOiBjYy5BdWRpb1NvdXJjZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5FbnVtKFNpY2JvU291bmRUeXBlKSB9KVxuICAgIHNvdW5kVHlwZTogU2ljYm9Tb3VuZFR5cGUgPSBTaWNib1NvdW5kVHlwZS5CYWNrZ3JvdW5kO1xuXG4gICAgQHByb3BlcnR5KClcbiAgICBhdXRvU3RvcERlbGF5OiBudW1iZXIgPSAtMTtcblxuICAgIGF1ZGlvSWQ6IG51bWJlciA9IC0xO1xuXG4gICAgcGxheShmcm9tOiBudW1iZXIgPSAwLCBhdXRvU3RvcERlbGF5OiBudW1iZXIgPSAtMSl7XG4gICAgICAgIHRoaXMuYXV0b1N0b3BEZWxheSA9IGF1dG9TdG9wRGVsYXk7XG4gICAgICAgIHN3aXRjaCh0aGlzLnNvdW5kVHlwZSl7XG4gICAgICAgICAgICBjYXNlIFNpY2JvU291bmRUeXBlLkJhY2tncm91bmQ6XG4gICAgICAgICAgICAgICAgaWYodGhpcy5pc1ZhbGlkQXVkaW9JZCgpICYmIGNjLmF1ZGlvRW5naW5lLmdldFN0YXRlKHRoaXMuYXVkaW9JZCkgPT0gY2MuYXVkaW9FbmdpbmUuQXVkaW9TdGF0ZS5QTEFZSU5HKSByZXR1cm47XG4gICAgICAgICAgICAgICAgdGhpcy5hdWRpb0lkID0gY2MuYXVkaW9FbmdpbmUucGxheU11c2ljKHRoaXMuYXVkaW9Tb3VyY2UuY2xpcCwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgU2ljYm9Tb3VuZFR5cGUuRWZmZWN0X1NpbmdsZTpcbiAgICAgICAgICAgICAgICBpZih0aGlzLmlzVmFsaWRBdWRpb0lkKCkgJiYgY2MuYXVkaW9FbmdpbmUuZ2V0U3RhdGUodGhpcy5hdWRpb0lkKSA9PSBjYy5hdWRpb0VuZ2luZS5BdWRpb1N0YXRlLlBMQVlJTkcpIHJldHVybjtcbiAgICAgICAgICAgICAgICB0aGlzLmF1ZGlvSWQgPSBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KHRoaXMuYXVkaW9Tb3VyY2UuY2xpcCwgZmFsc2UpO1xuICAgICAgICAgICAgICAgIGxldCBkdXJhdGlvbkVTID0gY2MuYXVkaW9FbmdpbmUuZ2V0RHVyYXRpb24odGhpcy5hdWRpb0lkKTtcbiAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5zZXRDdXJyZW50VGltZSh0aGlzLmF1ZGlvSWQsIE1hdGgubWluKGZyb20sIGR1cmF0aW9uRVMpKTtcbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBTaWNib1NvdW5kVHlwZS5FZmZlY3RfTG9vcF9BdXRvU3RvcDpcbiAgICAgICAgICAgICAgICBpZih0aGlzLmlzVmFsaWRBdWRpb0lkKCkgJiYgY2MuYXVkaW9FbmdpbmUuZ2V0U3RhdGUodGhpcy5hdWRpb0lkKSA9PSBjYy5hdWRpb0VuZ2luZS5BdWRpb1N0YXRlLlBMQVlJTkcpIHJldHVybjtcbiAgICAgICAgICAgICAgICB0aGlzLmF1ZGlvSWQgPSBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KHRoaXMuYXVkaW9Tb3VyY2UuY2xpcCwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgbGV0IGR1cmF0aW9uTEFFID0gY2MuYXVkaW9FbmdpbmUuZ2V0RHVyYXRpb24odGhpcy5hdWRpb0lkKTtcbiAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5zZXRDdXJyZW50VGltZSh0aGlzLmF1ZGlvSWQsIE1hdGgubWluKGZyb20sIGR1cmF0aW9uTEFFKSk7XG5cbiAgICAgICAgICAgICAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgICAgICAgICAgICAgaWYoc2VsZi5hdXRvU3RvcERlbGF5ID4gMCl7XG4gICAgICAgICAgICAgICAgICAgIGNjLnR3ZWVuKHNlbGYubm9kZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5kZWxheShzZWxmLmF1dG9TdG9wRGVsYXkpXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2FsbCgoKT0+e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuc3RvcCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSkuc3RhcnQoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgU2ljYm9Tb3VuZFR5cGUuRWZmZWN0X011bHRpcGxlOlxuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIC8vIC8vY2MuZXJyb3IodGhpcy5hdWRpb0lkKTtcbiAgICB9XG5cbiAgICBzdG9wKCl7XG4gICAgICAgIGlmKHRoaXMuaXNWYWxpZEF1ZGlvSWQoKSl7XG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5zdG9wKHRoaXMuYXVkaW9JZCk7XG4gICAgICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5ub2RlKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGlzVmFsaWRBdWRpb0lkKCl7XG4gICAgICAgIHJldHVybiB0aGlzLmF1ZGlvSWQgPj0gMDtcbiAgICB9XG5cbiAgICBvbkRlc3Ryb3koKXtcbiAgICAgICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMubm9kZSk7XG4gICAgfVxufVxuXG5cbiJdfQ==