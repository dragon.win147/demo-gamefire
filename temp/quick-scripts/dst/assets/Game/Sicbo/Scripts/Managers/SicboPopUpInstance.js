
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboPopUpInstance.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fe47feNq/RId4+IXrYWCKIs', 'SicboPopUpInstance');
// Game/Sicbo_New/Scripts/Managers/SicboPopUpInstance.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboPopUpInstance = /** @class */ (function (_super) {
    __extends(SicboPopUpInstance, _super);
    function SicboPopUpInstance() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.background = null;
        _this.panel = null;
        _this.normalPanelSize = 1;
        _this.scaleWhenOpenAndClosePanelSize = 1.05;
        _this._open = false;
        return _this;
    }
    SicboPopUpInstance.prototype.onDestroy = function () {
        this.unscheduleAllCallbacks();
    };
    SicboPopUpInstance.prototype.onLoad = function () {
        var _a;
        if (!this.background)
            this.background = (_a = this.node.getChildByName("darkBackground")) === null || _a === void 0 ? void 0 : _a.getComponent(cc.Widget);
        if (this.background) {
            this.background.isAlignLeft = true;
            this.background.isAbsoluteRight = true;
            this.background.isAbsoluteTop = true;
            this.background.isAlignBottom = true;
            this.background.left = -500;
            this.background.top = -500;
            this.background.bottom = -500;
            this.background.right = -500;
            this.background.target = cc.director.getScene().getChildByName("Canvas");
        }
        this._animation = this.getComponent(cc.Animation);
        this.node.active = false;
    };
    SicboPopUpInstance.prototype.showPopup = function () {
        var _this = this;
        if (this.panel != null) {
            this.panel.scale = 0;
            this.panel.opacity = 0;
            this.node.active = true;
            cc.Tween.stopAllByTarget(this.panel);
            cc.tween(this.panel)
                .to(0, { scale: 0, opacity: 0 })
                .to(0.2, { scale: this.normalPanelSize * this.scaleWhenOpenAndClosePanelSize, opacity: 255 })
                .to(0.1, { scale: this.normalPanelSize })
                .call(function () {
                _this.showDone();
            }).start();
        }
        else {
            this.node.active = true;
            if (this._animation != null) {
                this._animation.stop();
                this._animation.play("Show");
            }
            else {
                this.showDone();
            }
        }
    };
    SicboPopUpInstance.prototype.hidePopup = function () {
        var _this = this;
        if (this.panel != null) {
            cc.Tween.stopAllByTarget(this.panel);
            cc.tween(this.panel)
                .to(0.2, { scale: this.normalPanelSize * this.scaleWhenOpenAndClosePanelSize, opacity: 0 }).call(function () {
                _this.node.active = false;
                _this.closeDone();
            }).start();
        }
        else {
            if (this._animation != null) {
                this._animation.stop();
                this._animation.play("Hide");
            }
            else {
                this.closeDone();
            }
        }
    };
    SicboPopUpInstance.prototype.open = function (data, onYes, onNo) {
        if (onYes === void 0) { onYes = null; }
        if (onNo === void 0) { onNo = null; }
        this.beforeShow();
        this._open = true;
        this.showPopup();
        this._data = data;
        this._onYes = onYes;
        this._onNo = onNo;
        this.onShow(data);
    };
    SicboPopUpInstance.prototype.closeInstance = function () {
        if (!this._open)
            return;
        this._open = false;
        if (this._close)
            this._close();
        this.beforeClose();
        this.hidePopup();
    };
    SicboPopUpInstance.prototype.close = function () {
        this._open = false;
        if (this._close)
            this._close();
        this.beforeClose();
        this.hidePopup();
    };
    SicboPopUpInstance.prototype.onYes = function () {
        if (this._onYes) {
            this._onYes();
        }
        this.close();
    };
    SicboPopUpInstance.prototype.onNo = function () {
        if (this._onNo) {
            this._onNo();
        }
        this.close();
    };
    //#region Call From Animation Event
    SicboPopUpInstance.prototype.showDone = function () {
        this.afterShow();
    };
    SicboPopUpInstance.prototype.closeDone = function () {
        if (this._open == false) {
            this.node.active = false;
            this.afterClose();
        }
    };
    //#endregion
    SicboPopUpInstance.prototype.onShow = function (data) { };
    SicboPopUpInstance.prototype.afterShow = function () { };
    SicboPopUpInstance.prototype.beforeShow = function () { };
    SicboPopUpInstance.prototype.beforeClose = function () { };
    SicboPopUpInstance.prototype.afterClose = function () { };
    __decorate([
        property(cc.Widget)
    ], SicboPopUpInstance.prototype, "background", void 0);
    __decorate([
        property(cc.Node)
    ], SicboPopUpInstance.prototype, "panel", void 0);
    __decorate([
        property
    ], SicboPopUpInstance.prototype, "normalPanelSize", void 0);
    __decorate([
        property
    ], SicboPopUpInstance.prototype, "scaleWhenOpenAndClosePanelSize", void 0);
    SicboPopUpInstance = __decorate([
        ccclass
    ], SicboPopUpInstance);
    return SicboPopUpInstance;
}(cc.Component));
exports.default = SicboPopUpInstance;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL01hbmFnZXJzL1NpY2JvUG9wVXBJbnN0YW5jZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUU1RSxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFnRCxzQ0FBWTtJQUE1RDtRQUFBLHFFQTZKQztRQTNKRyxnQkFBVSxHQUFjLElBQUksQ0FBQztRQUU3QixXQUFLLEdBQVksSUFBSSxDQUFDO1FBRXRCLHFCQUFlLEdBQVcsQ0FBQyxDQUFDO1FBRTVCLG9DQUE4QixHQUFXLElBQUksQ0FBQztRQUc5QyxXQUFLLEdBQUcsS0FBSyxDQUFDOztJQWtKbEIsQ0FBQztJQTNJRyxzQ0FBUyxHQUFUO1FBQ0UsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVELG1DQUFNLEdBQU47O1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVO1lBQUUsSUFBSSxDQUFDLFVBQVUsU0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQywwQ0FBRSxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzVHLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDbkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztZQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDckMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDNUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDMUU7UUFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBQ0Qsc0NBQVMsR0FBVDtRQUFBLGlCQWlDQztRQWhDQyxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNyQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ3hCLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7aUJBQ2pCLEVBQUUsQ0FDRCxDQUFDLEVBQ0QsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FDekI7aUJBQ0EsRUFBRSxDQUNELEdBQUcsRUFDSCxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLENBQ3BGO2lCQUNBLEVBQUUsQ0FDRCxHQUFHLEVBQ0gsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUNoQztpQkFDQSxJQUFJLENBQUM7Z0JBQ0osS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2xCLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBRWQ7YUFBTTtZQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUN4QixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUM5QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakI7U0FFRjtJQUNILENBQUM7SUFDRCxzQ0FBUyxHQUFUO1FBQUEsaUJBb0JDO1FBbkJDLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDdEIsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3JDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztpQkFDakIsRUFBRSxDQUNELEdBQUcsRUFDSCxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBRWxGLENBQUMsSUFBSSxDQUFDO2dCQUNMLEtBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDekIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ25CLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2Q7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLEVBQUU7Z0JBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzlCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUNsQjtTQUNGO0lBQ0gsQ0FBQztJQUVNLGlDQUFJLEdBQVgsVUFBWSxJQUFJLEVBQUUsS0FBd0IsRUFBRSxJQUF1QjtRQUFqRCxzQkFBQSxFQUFBLFlBQXdCO1FBQUUscUJBQUEsRUFBQSxXQUF1QjtRQUNqRSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEIsQ0FBQztJQUVNLDBDQUFhLEdBQXBCO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLO1lBQUUsT0FBTztRQUN4QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLElBQUksQ0FBQyxNQUFNO1lBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFFbkIsQ0FBQztJQUVTLGtDQUFLLEdBQWY7UUFDRSxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLElBQUksQ0FBQyxNQUFNO1lBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVNLGtDQUFLLEdBQVo7UUFDRSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDZixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7U0FDZjtRQUNELElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFFTSxpQ0FBSSxHQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2QsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2Q7UUFDRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDZixDQUFDO0lBRUQsbUNBQW1DO0lBQzVCLHFDQUFRLEdBQWY7UUFDRSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVNLHNDQUFTLEdBQWhCO1FBQ0UsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLEtBQUssRUFBRTtZQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDekIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQ25CO0lBQ0gsQ0FBQztJQUVELFlBQVk7SUFFRixtQ0FBTSxHQUFoQixVQUFpQixJQUFJLElBQUksQ0FBQztJQUVoQixzQ0FBUyxHQUFuQixjQUF3QixDQUFDO0lBRWYsdUNBQVUsR0FBcEIsY0FBeUIsQ0FBQztJQUVoQix3Q0FBVyxHQUFyQixjQUEwQixDQUFDO0lBRWpCLHVDQUFVLEdBQXBCLGNBQXlCLENBQUM7SUExSjFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7MERBQ1M7SUFFN0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztxREFDSTtJQUV0QjtRQURDLFFBQVE7K0RBQ21CO0lBRTVCO1FBREMsUUFBUTs4RUFDcUM7SUFSN0Isa0JBQWtCO1FBRHRDLE9BQU87T0FDYSxrQkFBa0IsQ0E2SnRDO0lBQUQseUJBQUM7Q0E3SkQsQUE2SkMsQ0E3SitDLEVBQUUsQ0FBQyxTQUFTLEdBNkozRDtrQkE3Sm9CLGtCQUFrQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvUG9wVXBJbnN0YW5jZSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KGNjLldpZGdldClcbiAgICBiYWNrZ3JvdW5kOiBjYy5XaWRnZXQgPSBudWxsO1xuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIHBhbmVsOiBjYy5Ob2RlID0gbnVsbDtcbiAgICBAcHJvcGVydHlcbiAgICBub3JtYWxQYW5lbFNpemU6IG51bWJlciA9IDE7XG4gICAgQHByb3BlcnR5XG4gICAgc2NhbGVXaGVuT3BlbkFuZENsb3NlUGFuZWxTaXplOiBudW1iZXIgPSAxLjA1O1xuICBcbiAgICBfYW5pbWF0aW9uOiBjYy5BbmltYXRpb247XG4gICAgX29wZW4gPSBmYWxzZTtcbiAgICBfZGF0YTtcbiAgXG4gICAgX29uWWVzOiAoKSA9PiB2b2lkO1xuICAgIF9vbk5vOiAoKSA9PiB2b2lkO1xuICAgIF9jbG9zZTogKCkgPT4gdm9pZDtcbiAgXG4gICAgb25EZXN0cm95KCkge1xuICAgICAgdGhpcy51bnNjaGVkdWxlQWxsQ2FsbGJhY2tzKCk7XG4gICAgfVxuICBcbiAgICBvbkxvYWQoKSB7XG4gICAgICBpZiAoIXRoaXMuYmFja2dyb3VuZCkgdGhpcy5iYWNrZ3JvdW5kID0gdGhpcy5ub2RlLmdldENoaWxkQnlOYW1lKFwiZGFya0JhY2tncm91bmRcIik/LmdldENvbXBvbmVudChjYy5XaWRnZXQpO1xuICAgICAgaWYgKHRoaXMuYmFja2dyb3VuZCkge1xuICAgICAgICB0aGlzLmJhY2tncm91bmQuaXNBbGlnbkxlZnQgPSB0cnVlO1xuICAgICAgICB0aGlzLmJhY2tncm91bmQuaXNBYnNvbHV0ZVJpZ2h0ID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5iYWNrZ3JvdW5kLmlzQWJzb2x1dGVUb3AgPSB0cnVlO1xuICAgICAgICB0aGlzLmJhY2tncm91bmQuaXNBbGlnbkJvdHRvbSA9IHRydWU7XG4gICAgICAgIHRoaXMuYmFja2dyb3VuZC5sZWZ0ID0gLTUwMDtcbiAgICAgICAgdGhpcy5iYWNrZ3JvdW5kLnRvcCA9IC01MDA7XG4gICAgICAgIHRoaXMuYmFja2dyb3VuZC5ib3R0b20gPSAtNTAwO1xuICAgICAgICB0aGlzLmJhY2tncm91bmQucmlnaHQgPSAtNTAwO1xuICAgICAgICB0aGlzLmJhY2tncm91bmQudGFyZ2V0ID0gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShcIkNhbnZhc1wiKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuX2FuaW1hdGlvbiA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkFuaW1hdGlvbik7XG4gICAgICB0aGlzLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgfVxuICAgIHNob3dQb3B1cCgpIHtcbiAgICAgIGlmICh0aGlzLnBhbmVsICE9IG51bGwpIHtcbiAgICAgICAgdGhpcy5wYW5lbC5zY2FsZSA9IDA7XG4gICAgICAgIHRoaXMucGFuZWwub3BhY2l0eSA9IDA7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5wYW5lbCk7XG4gICAgICAgIGNjLnR3ZWVuKHRoaXMucGFuZWwpXG4gICAgICAgICAgLnRvKFxuICAgICAgICAgICAgMCxcbiAgICAgICAgICAgIHsgc2NhbGU6IDAsIG9wYWNpdHk6IDAgfVxuICAgICAgICAgIClcbiAgICAgICAgICAudG8oXG4gICAgICAgICAgICAwLjIsXG4gICAgICAgICAgICB7IHNjYWxlOiB0aGlzLm5vcm1hbFBhbmVsU2l6ZSAqIHRoaXMuc2NhbGVXaGVuT3BlbkFuZENsb3NlUGFuZWxTaXplLCBvcGFjaXR5OiAyNTUgfVxuICAgICAgICAgIClcbiAgICAgICAgICAudG8oXG4gICAgICAgICAgICAwLjEsXG4gICAgICAgICAgICB7IHNjYWxlOiB0aGlzLm5vcm1hbFBhbmVsU2l6ZSB9XG4gICAgICAgICAgKVxuICAgICAgICAgIC5jYWxsKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2hvd0RvbmUoKTtcbiAgICAgICAgICB9KS5zdGFydCgpO1xuICBcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBpZiAodGhpcy5fYW5pbWF0aW9uICE9IG51bGwpIHtcbiAgICAgICAgICB0aGlzLl9hbmltYXRpb24uc3RvcCgpO1xuICAgICAgICAgIHRoaXMuX2FuaW1hdGlvbi5wbGF5KFwiU2hvd1wiKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnNob3dEb25lKCk7XG4gICAgICAgIH1cbiAgXG4gICAgICB9XG4gICAgfVxuICAgIGhpZGVQb3B1cCgpIHtcbiAgICAgIGlmICh0aGlzLnBhbmVsICE9IG51bGwpIHtcbiAgICAgICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMucGFuZWwpO1xuICAgICAgICBjYy50d2Vlbih0aGlzLnBhbmVsKVxuICAgICAgICAgIC50byhcbiAgICAgICAgICAgIDAuMixcbiAgICAgICAgICAgIHsgc2NhbGU6IHRoaXMubm9ybWFsUGFuZWxTaXplICogdGhpcy5zY2FsZVdoZW5PcGVuQW5kQ2xvc2VQYW5lbFNpemUsIG9wYWNpdHk6IDAgfVxuICBcbiAgICAgICAgICApLmNhbGwoKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5jbG9zZURvbmUoKTtcbiAgICAgICAgICB9KS5zdGFydCgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKHRoaXMuX2FuaW1hdGlvbiAhPSBudWxsKSB7XG4gICAgICAgICAgdGhpcy5fYW5pbWF0aW9uLnN0b3AoKTtcbiAgICAgICAgICB0aGlzLl9hbmltYXRpb24ucGxheShcIkhpZGVcIik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5jbG9zZURvbmUoKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgXG4gICAgcHVibGljIG9wZW4oZGF0YSwgb25ZZXM6ICgpID0+IHZvaWQgPSBudWxsLCBvbk5vOiAoKSA9PiB2b2lkID0gbnVsbCkge1xuICAgICAgdGhpcy5iZWZvcmVTaG93KCk7XG4gICAgICB0aGlzLl9vcGVuID0gdHJ1ZTtcbiAgICAgIHRoaXMuc2hvd1BvcHVwKCk7XG4gICAgICB0aGlzLl9kYXRhID0gZGF0YTtcbiAgICAgIHRoaXMuX29uWWVzID0gb25ZZXM7XG4gICAgICB0aGlzLl9vbk5vID0gb25ObztcbiAgICAgIHRoaXMub25TaG93KGRhdGEpO1xuICAgIH1cbiAgXG4gICAgcHVibGljIGNsb3NlSW5zdGFuY2UoKSB7XG4gICAgICBpZiAoIXRoaXMuX29wZW4pIHJldHVybjtcbiAgICAgIHRoaXMuX29wZW4gPSBmYWxzZTtcbiAgICAgIGlmICh0aGlzLl9jbG9zZSkgdGhpcy5fY2xvc2UoKTtcbiAgICAgIHRoaXMuYmVmb3JlQ2xvc2UoKTtcbiAgICAgIHRoaXMuaGlkZVBvcHVwKCk7XG4gIFxuICAgIH1cbiAgXG4gICAgcHJvdGVjdGVkIGNsb3NlKCkge1xuICAgICAgdGhpcy5fb3BlbiA9IGZhbHNlO1xuICAgICAgaWYgKHRoaXMuX2Nsb3NlKSB0aGlzLl9jbG9zZSgpO1xuICAgICAgdGhpcy5iZWZvcmVDbG9zZSgpO1xuICAgICAgdGhpcy5oaWRlUG9wdXAoKTtcbiAgICB9XG4gIFxuICAgIHB1YmxpYyBvblllcygpIHtcbiAgICAgIGlmICh0aGlzLl9vblllcykge1xuICAgICAgICB0aGlzLl9vblllcygpO1xuICAgICAgfVxuICAgICAgdGhpcy5jbG9zZSgpO1xuICAgIH1cbiAgXG4gICAgcHVibGljIG9uTm8oKSB7XG4gICAgICBpZiAodGhpcy5fb25Obykge1xuICAgICAgICB0aGlzLl9vbk5vKCk7XG4gICAgICB9XG4gICAgICB0aGlzLmNsb3NlKCk7XG4gICAgfVxuICBcbiAgICAvLyNyZWdpb24gQ2FsbCBGcm9tIEFuaW1hdGlvbiBFdmVudFxuICAgIHB1YmxpYyBzaG93RG9uZSgpIHtcbiAgICAgIHRoaXMuYWZ0ZXJTaG93KCk7XG4gICAgfVxuICBcbiAgICBwdWJsaWMgY2xvc2VEb25lKCkge1xuICAgICAgaWYgKHRoaXMuX29wZW4gPT0gZmFsc2UpIHtcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmFmdGVyQ2xvc2UoKTtcbiAgICAgIH1cbiAgICB9XG4gIFxuICAgIC8vI2VuZHJlZ2lvblxuICBcbiAgICBwcm90ZWN0ZWQgb25TaG93KGRhdGEpIHsgfVxuICBcbiAgICBwcm90ZWN0ZWQgYWZ0ZXJTaG93KCkgeyB9XG4gIFxuICAgIHByb3RlY3RlZCBiZWZvcmVTaG93KCkgeyB9XG4gIFxuICAgIHByb3RlY3RlZCBiZWZvcmVDbG9zZSgpIHsgfVxuICBcbiAgICBwcm90ZWN0ZWQgYWZ0ZXJDbG9zZSgpIHsgfVxufVxuIl19