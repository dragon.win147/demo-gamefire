
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Managers/SicboUIManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '947b6PbhdlBO4MuiMVN64Lg', 'SicboUIManager');
// Game/Sicbo/Scripts/Managers/SicboUIManager.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboUIManager = /** @class */ (function (_super) {
    __extends(SicboUIManager, _super);
    function SicboUIManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popupRoot = null;
        _this.popupPrefabs = [];
        _this.popupMap = new Map();
        _this._popupInstances = new Map();
        return _this;
    }
    SicboUIManager_1 = SicboUIManager;
    SicboUIManager.getInstance = function () {
        if (SicboUIManager_1._instance == null) {
            var canvas = cc.director.getScene().getChildByName("Canvas");
            var node = canvas.getChildByName("SicboUIManager");
            SicboUIManager_1._instance = node.getComponent(SicboUIManager_1);
        }
        return SicboUIManager_1._instance;
    };
    SicboUIManager.prototype.onLoad = function () {
        this.init();
    };
    SicboUIManager.prototype.init = function () {
        var _this = this;
        this.popupPrefabs.forEach(function (popupPrefab) {
            if (popupPrefab) {
                _this.popupMap.set(popupPrefab.name, popupPrefab);
            }
        });
    };
    SicboUIManager.prototype.openPopup = function (type, name, data, onComplete, parent) {
        if (data === void 0) { data = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (parent === void 0) { parent = null; }
        var self = SicboUIManager_1._instance;
        parent = parent ? parent : self.popupRoot;
        if (name == self._currentPopupName && self._currentPopup.node) {
            if (onComplete)
                onComplete(self._currentPopup);
            return;
        }
        self._currentPopupName = name;
        self.getPopupInstance(type, name, function (instance) {
            if (self._currentPopup == instance) {
                if (onComplete)
                    onComplete(self._currentPopup);
                return;
            }
            self._currentPopup = instance;
            instance.open(data);
            instance._close = function () {
                self._currentPopup = null;
                self._currentPopupName = "";
            };
            if (onComplete)
                onComplete(self._currentPopup);
        }, parent);
    };
    SicboUIManager.prototype.hidePopup = function (name) {
        var _a;
        var self = SicboUIManager_1._instance;
        (_a = self._popupInstances.get(name)) === null || _a === void 0 ? void 0 : _a.hidePopup();
    };
    SicboUIManager.prototype.getPopupInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = SicboUIManager_1._instance;
        var _parent = parent == null ? this.node : parent;
        if (self._popupInstances.has(name)) {
            var popup = self._popupInstances.get(name);
            popup.node.parent = null;
            _parent.addChild(popup.node);
            onComplete(popup);
        }
        else {
            this.loadPopUpPrefab(type, name, onComplete, _parent);
        }
    };
    SicboUIManager.prototype.loadPopUpPrefab = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = SicboUIManager_1._instance;
        var prefab = this.popupMap.get(name);
        var newNode = cc.instantiate(prefab);
        var instance = newNode.getComponent(type);
        parent.addChild(newNode);
        self._popupInstances.set(name, instance);
        onComplete(instance);
    };
    SicboUIManager.prototype.onDestroy = function () {
        delete SicboUIManager_1._instance;
        SicboUIManager_1._instance = null;
    };
    var SicboUIManager_1;
    SicboUIManager._instance = null;
    SicboUIManager.SicboInforPopup = "SicboInforPopup";
    SicboUIManager.SicboCannotJoinPopup = "SicboCannotJoinPopup";
    __decorate([
        property(cc.Node)
    ], SicboUIManager.prototype, "popupRoot", void 0);
    __decorate([
        property([cc.Prefab])
    ], SicboUIManager.prototype, "popupPrefabs", void 0);
    SicboUIManager = SicboUIManager_1 = __decorate([
        ccclass
    ], SicboUIManager);
    return SicboUIManager;
}(cc.Component));
exports.default = SicboUIManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvL1NjcmlwdHMvTWFuYWdlcnMvU2ljYm9VSU1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJNUUsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBNEMsa0NBQVk7SUFBeEQ7UUFBQSxxRUFtR0M7UUE1RkMsZUFBUyxHQUFZLElBQUksQ0FBQztRQUcxQixrQkFBWSxHQUFnQixFQUFFLENBQUM7UUFFdkIsY0FBUSxHQUEyQixJQUFJLEdBQUcsRUFBcUIsQ0FBQztRQUVoRSxxQkFBZSxHQUFvQyxJQUFJLEdBQUcsRUFBOEIsQ0FBQzs7SUFxRm5HLENBQUM7dUJBbkdvQixjQUFjO0lBa0IxQiwwQkFBVyxHQUFsQjtRQUNFLElBQUksZ0JBQWMsQ0FBQyxTQUFTLElBQUksSUFBSSxFQUFFO1lBQ3BDLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdELElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNuRCxnQkFBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFjLENBQUMsQ0FBQztTQUM5RDtRQUNELE9BQU8sZ0JBQWMsQ0FBQyxTQUFTLENBQUM7SUFDbEMsQ0FBQztJQUVELCtCQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQsNkJBQUksR0FBSjtRQUFBLGlCQU1DO1FBTEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBQyxXQUFXO1lBQ3BDLElBQUcsV0FBVyxFQUFDO2dCQUNiLEtBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7YUFDbEQ7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxrQ0FBUyxHQUFoQixVQUFpQixJQUErQixFQUFFLElBQVksRUFBRSxJQUFXLEVBQUUsVUFBeUQsRUFBRSxNQUFzQjtRQUE5RixxQkFBQSxFQUFBLFdBQVc7UUFBRSwyQkFBQSxFQUFBLGlCQUF5RDtRQUFFLHVCQUFBLEVBQUEsYUFBc0I7UUFDNUosSUFBSSxJQUFJLEdBQUcsZ0JBQWMsQ0FBQyxTQUFTLENBQUM7UUFDcEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRTtZQUM3RCxJQUFJLFVBQVU7Z0JBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUMvQyxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQzlCLElBQUksQ0FBQyxnQkFBZ0IsQ0FDbkIsSUFBSSxFQUNKLElBQUksRUFDSixVQUFDLFFBQVE7WUFDUCxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksUUFBUSxFQUFFO2dCQUNsQyxJQUFJLFVBQVU7b0JBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDL0MsT0FBTzthQUNSO1lBQ0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUM7WUFDOUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixRQUFRLENBQUMsTUFBTSxHQUFHO2dCQUNoQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztnQkFDMUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztZQUM5QixDQUFDLENBQUM7WUFDRixJQUFJLFVBQVU7Z0JBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNqRCxDQUFDLEVBQ0QsTUFBTSxDQUNQLENBQUM7SUFDSixDQUFDO0lBRU0sa0NBQVMsR0FBaEIsVUFBaUIsSUFBWTs7UUFDM0IsSUFBSSxJQUFJLEdBQUcsZ0JBQWMsQ0FBQyxTQUFTLENBQUM7UUFDcEMsTUFBQSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsMENBQUUsU0FBUyxHQUFHO0lBQzlDLENBQUM7SUFFTSx5Q0FBZ0IsR0FBdkIsVUFBd0IsSUFBK0IsRUFBRSxJQUFZLEVBQUUsVUFBa0QsRUFBRSxNQUFzQjtRQUF0Qix1QkFBQSxFQUFBLGFBQXNCO1FBQy9JLElBQUksSUFBSSxHQUFHLGdCQUFjLENBQUMsU0FBUyxDQUFDO1FBQ3BDLElBQU0sT0FBTyxHQUFHLE1BQU0sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUNwRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2xDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUN6QixPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3QixVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbkI7YUFBTTtZQUNMLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7U0FDdkQ7SUFDSCxDQUFDO0lBRU8sd0NBQWUsR0FBdkIsVUFBd0IsSUFBK0IsRUFBRSxJQUFZLEVBQUUsVUFBa0QsRUFBRSxNQUFzQjtRQUF0Qix1QkFBQSxFQUFBLGFBQXNCO1FBQy9JLElBQUksSUFBSSxHQUFHLGdCQUFjLENBQUMsU0FBUyxDQUFDO1FBQ3BDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JDLElBQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUF1QixDQUFDO1FBQzdELElBQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDekMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxrQ0FBUyxHQUFUO1FBQ0UsT0FBTyxnQkFBYyxDQUFDLFNBQVMsQ0FBQztRQUNoQyxnQkFBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7SUFDbEMsQ0FBQzs7SUFqR2Msd0JBQVMsR0FBbUIsSUFBSSxDQUFDO0lBRWxDLDhCQUFlLEdBQVcsaUJBQWlCLENBQUM7SUFDNUMsbUNBQW9CLEdBQVcsc0JBQXNCLENBQUM7SUFHcEU7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztxREFDUTtJQUcxQjtRQURDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQzt3REFDUztJQVZaLGNBQWM7UUFEbEMsT0FBTztPQUNhLGNBQWMsQ0FtR2xDO0lBQUQscUJBQUM7Q0FuR0QsQUFtR0MsQ0FuRzJDLEVBQUUsQ0FBQyxTQUFTLEdBbUd2RDtrQkFuR29CLGNBQWMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFNpY2JvUG9wVXBJbnN0YW5jZSBmcm9tIFwiLi9TaWNib1BvcFVwSW5zdGFuY2VcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvVUlNYW5hZ2VyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgcHJpdmF0ZSBzdGF0aWMgX2luc3RhbmNlOiBTaWNib1VJTWFuYWdlciA9IG51bGw7XG5cbiAgcHVibGljIHN0YXRpYyBTaWNib0luZm9yUG9wdXA6IHN0cmluZyA9IFwiU2ljYm9JbmZvclBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgU2ljYm9DYW5ub3RKb2luUG9wdXA6IHN0cmluZyA9IFwiU2ljYm9DYW5ub3RKb2luUG9wdXBcIjtcblxuICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgcG9wdXBSb290OiBjYy5Ob2RlID0gbnVsbDtcblxuICBAcHJvcGVydHkoW2NjLlByZWZhYl0pXG4gIHBvcHVwUHJlZmFiczogY2MuUHJlZmFiW10gPSBbXTtcblxuICBwcml2YXRlIHBvcHVwTWFwOiBNYXA8c3RyaW5nLCBjYy5QcmVmYWI+ID0gbmV3IE1hcDxzdHJpbmcsIGNjLlByZWZhYj4oKTtcblxuICBwcml2YXRlIF9wb3B1cEluc3RhbmNlczogTWFwPHN0cmluZywgU2ljYm9Qb3BVcEluc3RhbmNlPiA9IG5ldyBNYXA8c3RyaW5nLCBTaWNib1BvcFVwSW5zdGFuY2U+KCk7XG4gIHByaXZhdGUgX2N1cnJlbnRQb3B1cDogU2ljYm9Qb3BVcEluc3RhbmNlO1xuICBwcml2YXRlIF9jdXJyZW50UG9wdXBOYW1lOiBzdHJpbmc7XG5cbiAgc3RhdGljIGdldEluc3RhbmNlKCk6IFNpY2JvVUlNYW5hZ2VyIHtcbiAgICBpZiAoU2ljYm9VSU1hbmFnZXIuX2luc3RhbmNlID09IG51bGwpIHtcbiAgICAgIGxldCBjYW52YXMgPSBjYy5kaXJlY3Rvci5nZXRTY2VuZSgpLmdldENoaWxkQnlOYW1lKFwiQ2FudmFzXCIpO1xuICAgICAgbGV0IG5vZGUgPSBjYW52YXMuZ2V0Q2hpbGRCeU5hbWUoXCJTaWNib1VJTWFuYWdlclwiKTtcbiAgICAgIFNpY2JvVUlNYW5hZ2VyLl9pbnN0YW5jZSA9IG5vZGUuZ2V0Q29tcG9uZW50KFNpY2JvVUlNYW5hZ2VyKTtcbiAgICB9XG4gICAgcmV0dXJuIFNpY2JvVUlNYW5hZ2VyLl9pbnN0YW5jZTtcbiAgfVxuXG4gIG9uTG9hZCgpIHtcbiAgICB0aGlzLmluaXQoKTtcbiAgfVxuXG4gIGluaXQoKSB7XG4gICAgdGhpcy5wb3B1cFByZWZhYnMuZm9yRWFjaCgocG9wdXBQcmVmYWIpID0+IHtcbiAgICAgIGlmKHBvcHVwUHJlZmFiKXtcbiAgICAgICAgdGhpcy5wb3B1cE1hcC5zZXQocG9wdXBQcmVmYWIubmFtZSwgcG9wdXBQcmVmYWIpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIG9wZW5Qb3B1cCh0eXBlOiB0eXBlb2YgU2ljYm9Qb3BVcEluc3RhbmNlLCBuYW1lOiBzdHJpbmcsIGRhdGEgPSBudWxsLCBvbkNvbXBsZXRlOiAoaW5zdGFuY2U6IFNpY2JvUG9wVXBJbnN0YW5jZSkgPT4gdm9pZCA9IG51bGwsIHBhcmVudDogY2MuTm9kZSA9IG51bGwpIHtcbiAgICBsZXQgc2VsZiA9IFNpY2JvVUlNYW5hZ2VyLl9pbnN0YW5jZTtcbiAgICBwYXJlbnQgPSBwYXJlbnQgPyBwYXJlbnQgOiBzZWxmLnBvcHVwUm9vdDtcbiAgICBpZiAobmFtZSA9PSBzZWxmLl9jdXJyZW50UG9wdXBOYW1lICYmIHNlbGYuX2N1cnJlbnRQb3B1cC5ub2RlKSB7XG4gICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZShzZWxmLl9jdXJyZW50UG9wdXApO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBzZWxmLl9jdXJyZW50UG9wdXBOYW1lID0gbmFtZTtcbiAgICBzZWxmLmdldFBvcHVwSW5zdGFuY2UoXG4gICAgICB0eXBlLFxuICAgICAgbmFtZSxcbiAgICAgIChpbnN0YW5jZSkgPT4ge1xuICAgICAgICBpZiAoc2VsZi5fY3VycmVudFBvcHVwID09IGluc3RhbmNlKSB7XG4gICAgICAgICAgaWYgKG9uQ29tcGxldGUpIG9uQ29tcGxldGUoc2VsZi5fY3VycmVudFBvcHVwKTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgc2VsZi5fY3VycmVudFBvcHVwID0gaW5zdGFuY2U7XG4gICAgICAgIGluc3RhbmNlLm9wZW4oZGF0YSk7XG4gICAgICAgIGluc3RhbmNlLl9jbG9zZSA9ICgpID0+IHtcbiAgICAgICAgICBzZWxmLl9jdXJyZW50UG9wdXAgPSBudWxsO1xuICAgICAgICAgIHNlbGYuX2N1cnJlbnRQb3B1cE5hbWUgPSBcIlwiO1xuICAgICAgICB9O1xuICAgICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZShzZWxmLl9jdXJyZW50UG9wdXApO1xuICAgICAgfSxcbiAgICAgIHBhcmVudFxuICAgICk7XG4gIH1cblxuICBwdWJsaWMgaGlkZVBvcHVwKG5hbWU6IHN0cmluZykge1xuICAgIGxldCBzZWxmID0gU2ljYm9VSU1hbmFnZXIuX2luc3RhbmNlO1xuICAgIHNlbGYuX3BvcHVwSW5zdGFuY2VzLmdldChuYW1lKT8uaGlkZVBvcHVwKCk7XG4gIH1cblxuICBwdWJsaWMgZ2V0UG9wdXBJbnN0YW5jZSh0eXBlOiB0eXBlb2YgU2ljYm9Qb3BVcEluc3RhbmNlLCBuYW1lOiBzdHJpbmcsIG9uQ29tcGxldGU6IChpbnN0YW5jZTogU2ljYm9Qb3BVcEluc3RhbmNlKSA9PiB2b2lkLCBwYXJlbnQ6IGNjLk5vZGUgPSBudWxsKSB7XG4gICAgbGV0IHNlbGYgPSBTaWNib1VJTWFuYWdlci5faW5zdGFuY2U7XG4gICAgY29uc3QgX3BhcmVudCA9IHBhcmVudCA9PSBudWxsID8gdGhpcy5ub2RlIDogcGFyZW50O1xuICAgIGlmIChzZWxmLl9wb3B1cEluc3RhbmNlcy5oYXMobmFtZSkpIHtcbiAgICAgIGxldCBwb3B1cCA9IHNlbGYuX3BvcHVwSW5zdGFuY2VzLmdldChuYW1lKTtcbiAgICAgIHBvcHVwLm5vZGUucGFyZW50ID0gbnVsbDtcbiAgICAgIF9wYXJlbnQuYWRkQ2hpbGQocG9wdXAubm9kZSk7XG4gICAgICBvbkNvbXBsZXRlKHBvcHVwKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5sb2FkUG9wVXBQcmVmYWIodHlwZSwgbmFtZSwgb25Db21wbGV0ZSwgX3BhcmVudCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBsb2FkUG9wVXBQcmVmYWIodHlwZTogdHlwZW9mIFNpY2JvUG9wVXBJbnN0YW5jZSwgbmFtZTogc3RyaW5nLCBvbkNvbXBsZXRlOiAoaW5zdGFuY2U6IFNpY2JvUG9wVXBJbnN0YW5jZSkgPT4gdm9pZCwgcGFyZW50OiBjYy5Ob2RlID0gbnVsbCkge1xuICAgIGxldCBzZWxmID0gU2ljYm9VSU1hbmFnZXIuX2luc3RhbmNlO1xuICAgIGxldCBwcmVmYWIgPSB0aGlzLnBvcHVwTWFwLmdldChuYW1lKTtcbiAgICBjb25zdCBuZXdOb2RlID0gY2MuaW5zdGFudGlhdGUocHJlZmFiKSBhcyB1bmtub3duIGFzIGNjLk5vZGU7XG4gICAgY29uc3QgaW5zdGFuY2UgPSBuZXdOb2RlLmdldENvbXBvbmVudCh0eXBlKTtcbiAgICBwYXJlbnQuYWRkQ2hpbGQobmV3Tm9kZSk7XG4gICAgc2VsZi5fcG9wdXBJbnN0YW5jZXMuc2V0KG5hbWUsIGluc3RhbmNlKTtcbiAgICBvbkNvbXBsZXRlKGluc3RhbmNlKTtcbiAgfVxuXG4gIG9uRGVzdHJveSgpIHtcbiAgICBkZWxldGUgU2ljYm9VSU1hbmFnZXIuX2luc3RhbmNlO1xuICAgIFNpY2JvVUlNYW5hZ2VyLl9pbnN0YW5jZSA9IG51bGw7XG4gIH1cbn1cbiJdfQ==