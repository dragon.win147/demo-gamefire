
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/PopUps/SicboCannotJoinPopup.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '30fd5I8wj1DW5bHl+VUlTQg', 'SicboCannotJoinPopup');
// Game/Sicbo_New/Scripts/PopUps/SicboCannotJoinPopup.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboPortalAdapter_1 = require("../SicboPortalAdapter");
var SicboPopUpInstance_1 = require("../Managers/SicboPopUpInstance");
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboText_1 = require("../Setting/SicboText");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboCannotJoinPopup = /** @class */ (function (_super) {
    __extends(SicboCannotJoinPopup, _super);
    function SicboCannotJoinPopup() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.notIngameMsgLabel = null;
        _this.ingameMsgLabel = null;
        _this.notIngameNode = null;
        _this.ingameNode = null;
        return _this;
    }
    SicboCannotJoinPopup.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        this.notIngameMsgLabel.string = SicboText_1.default.getCannotJoinMessageWhenNotIngame;
        this.ingameMsgLabel.string = SicboText_1.default.getCannotJoinMessageWhenIngame;
    };
    SicboCannotJoinPopup.prototype.onShow = function () {
        var inGame = (cc.director.getScene().name != SicboSetting_1.SicboScene.Loading);
        this.ingameNode.active = inGame;
        this.notIngameNode.active = !inGame;
    };
    SicboCannotJoinPopup.prototype.leave = function () {
        this.close();
        SicboPortalAdapter_1.default.getInstance().leaveGame();
    };
    __decorate([
        property(cc.Label)
    ], SicboCannotJoinPopup.prototype, "notIngameMsgLabel", void 0);
    __decorate([
        property(cc.Label)
    ], SicboCannotJoinPopup.prototype, "ingameMsgLabel", void 0);
    __decorate([
        property(cc.Node)
    ], SicboCannotJoinPopup.prototype, "notIngameNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboCannotJoinPopup.prototype, "ingameNode", void 0);
    SicboCannotJoinPopup = __decorate([
        ccclass
    ], SicboCannotJoinPopup);
    return SicboCannotJoinPopup;
}(SicboPopUpInstance_1.default));
exports.default = SicboCannotJoinPopup;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1BvcFVwcy9TaWNib0Nhbm5vdEpvaW5Qb3B1cC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVsRiw0REFBdUQ7QUFDdkQscUVBQWdFO0FBQ2hFLHdEQUFxRDtBQUNyRCxrREFBNkM7QUFFdkMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBa0Qsd0NBQWtCO0lBQXBFO1FBQUEscUVBNkJDO1FBM0JHLHVCQUFpQixHQUFhLElBQUksQ0FBQztRQUduQyxvQkFBYyxHQUFhLElBQUksQ0FBQztRQUdoQyxtQkFBYSxHQUFZLElBQUksQ0FBQztRQUc5QixnQkFBVSxHQUFZLElBQUksQ0FBQzs7SUFrQi9CLENBQUM7SUFoQkcscUNBQU0sR0FBTjtRQUNJLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBQ2YsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxtQkFBUyxDQUFDLGlDQUFpQyxDQUFDO1FBQzVFLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLG1CQUFTLENBQUMsOEJBQThCLENBQUM7SUFDMUUsQ0FBQztJQUVELHFDQUFNLEdBQU47UUFDSSxJQUFJLE1BQU0sR0FBWSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxJQUFJLHlCQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsTUFBTSxDQUFDO0lBQ3hDLENBQUM7SUFFRCxvQ0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2IsNEJBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDakQsQ0FBQztJQTFCRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO21FQUNnQjtJQUduQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2dFQUNhO0lBR2hDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0RBQ1k7SUFHOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0REFDUztJQVhWLG9CQUFvQjtRQUR4QyxPQUFPO09BQ2Esb0JBQW9CLENBNkJ4QztJQUFELDJCQUFDO0NBN0JELEFBNkJDLENBN0JpRCw0QkFBa0IsR0E2Qm5FO2tCQTdCb0Isb0JBQW9CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBTaWNib1BvcnRhbEFkYXB0ZXIgZnJvbSBcIi4uL1NpY2JvUG9ydGFsQWRhcHRlclwiO1xuaW1wb3J0IFNpY2JvUG9wVXBJbnN0YW5jZSBmcm9tIFwiLi4vTWFuYWdlcnMvU2ljYm9Qb3BVcEluc3RhbmNlXCI7XG5pbXBvcnQgeyBTaWNib1NjZW5lIH0gZnJvbSBcIi4uL1NldHRpbmcvU2ljYm9TZXR0aW5nXCI7XG5pbXBvcnQgU2ljYm9UZXh0IGZyb20gJy4uL1NldHRpbmcvU2ljYm9UZXh0JztcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvQ2Fubm90Sm9pblBvcHVwIGV4dGVuZHMgU2ljYm9Qb3BVcEluc3RhbmNlIHtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgbm90SW5nYW1lTXNnTGFiZWw6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBpbmdhbWVNc2dMYWJlbDogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgbm90SW5nYW1lTm9kZTogY2MuTm9kZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBpbmdhbWVOb2RlOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIG9uTG9hZCgpIHtcbiAgICAgICAgc3VwZXIub25Mb2FkKCk7XG4gICAgICAgIHRoaXMubm90SW5nYW1lTXNnTGFiZWwuc3RyaW5nID0gU2ljYm9UZXh0LmdldENhbm5vdEpvaW5NZXNzYWdlV2hlbk5vdEluZ2FtZTtcbiAgICAgICAgdGhpcy5pbmdhbWVNc2dMYWJlbC5zdHJpbmcgPSBTaWNib1RleHQuZ2V0Q2Fubm90Sm9pbk1lc3NhZ2VXaGVuSW5nYW1lO1xuICAgIH1cbiAgICBcbiAgICBvblNob3coKXtcbiAgICAgICAgbGV0IGluR2FtZTogYm9vbGVhbiA9IChjYy5kaXJlY3Rvci5nZXRTY2VuZSgpLm5hbWUgIT0gU2ljYm9TY2VuZS5Mb2FkaW5nKTtcbiAgICAgICAgdGhpcy5pbmdhbWVOb2RlLmFjdGl2ZSA9IGluR2FtZTtcbiAgICAgICAgdGhpcy5ub3RJbmdhbWVOb2RlLmFjdGl2ZSA9ICFpbkdhbWU7XG4gICAgfVxuXG4gICAgbGVhdmUoKXtcbiAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgICAgICBTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0SW5zdGFuY2UoKS5sZWF2ZUdhbWUoKTtcbiAgICB9XG59XG4iXX0=