
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/PopUps/SicboInforPopup.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5dd59Fs+ClGt4tHfosysUUN', 'SicboInforPopup');
// Game/Sicbo_New/Scripts/PopUps/SicboInforPopup.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboPopUpInstance_1 = require("../Managers/SicboPopUpInstance");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboInforPopup = /** @class */ (function (_super) {
    __extends(SicboInforPopup, _super);
    function SicboInforPopup() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scrollview = null;
        _this.infoSprite = null;
        return _this;
    }
    SicboInforPopup.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
    };
    SicboInforPopup.prototype.onShow = function () {
        // this.scrollview.scrollToTop(0);
        // this.infoSprite.spriteFrame = null;
        // let infoResources = SicboController.Instance.getGameInfo()
        // if(infoResources == null) return
        // infoResources.forEach((info) => {
        //   SicboResourceManager.loadRemoteImageWithFileField(this.infoSprite, info);
        // });
    };
    SicboInforPopup.prototype.onButtonClose = function () {
        this.close();
    };
    __decorate([
        property(cc.ScrollView)
    ], SicboInforPopup.prototype, "scrollview", void 0);
    __decorate([
        property(cc.Sprite)
    ], SicboInforPopup.prototype, "infoSprite", void 0);
    SicboInforPopup = __decorate([
        ccclass
    ], SicboInforPopup);
    return SicboInforPopup;
}(SicboPopUpInstance_1.default));
exports.default = SicboInforPopup;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1BvcFVwcy9TaWNib0luZm9yUG9wdXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHbEYscUVBQWdFO0FBRzFELElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQTZDLG1DQUFrQjtJQUEvRDtRQUFBLHFFQXdCQztRQXRCQyxnQkFBVSxHQUFrQixJQUFJLENBQUM7UUFHakMsZ0JBQVUsR0FBYyxJQUFJLENBQUM7O0lBbUIvQixDQUFDO0lBakJDLGdDQUFNLEdBQU47UUFDRSxpQkFBTSxNQUFNLFdBQUUsQ0FBQztJQUNqQixDQUFDO0lBRUQsZ0NBQU0sR0FBTjtRQUNFLGtDQUFrQztRQUNsQyxzQ0FBc0M7UUFDdEMsNkRBQTZEO1FBQzdELG1DQUFtQztRQUNuQyxvQ0FBb0M7UUFDcEMsOEVBQThFO1FBQzlFLE1BQU07SUFDUixDQUFDO0lBRUQsdUNBQWEsR0FBYjtRQUNFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFyQkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQzt1REFDUztJQUdqQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3VEQUNTO0lBTFYsZUFBZTtRQURuQyxPQUFPO09BQ2EsZUFBZSxDQXdCbkM7SUFBRCxzQkFBQztDQXhCRCxBQXdCQyxDQXhCNEMsNEJBQWtCLEdBd0I5RDtrQkF4Qm9CLGVBQWUiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFNpY2JvQ29udHJvbGxlciBmcm9tIFwiLi4vQ29udHJvbGxlcnMvU2ljYm9Db250cm9sbGVyXCI7XG5pbXBvcnQgU2ljYm9Qb3BVcEluc3RhbmNlIGZyb20gXCIuLi9NYW5hZ2Vycy9TaWNib1BvcFVwSW5zdGFuY2VcIjtcbmltcG9ydCBTaWNib1Jlc291cmNlTWFuYWdlciBmcm9tIFwiLi4vTWFuYWdlcnMvU2ljYm9SZXNvdXJjZU1hbmFnZXJcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvSW5mb3JQb3B1cCBleHRlbmRzIFNpY2JvUG9wVXBJbnN0YW5jZSB7XG4gIEBwcm9wZXJ0eShjYy5TY3JvbGxWaWV3KVxuICBzY3JvbGx2aWV3OiBjYy5TY3JvbGxWaWV3ID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuU3ByaXRlKVxuICBpbmZvU3ByaXRlOiBjYy5TcHJpdGUgPSBudWxsO1xuXG4gIG9uTG9hZCgpIHtcbiAgICBzdXBlci5vbkxvYWQoKTtcbiAgfVxuXG4gIG9uU2hvdygpIHtcbiAgICAvLyB0aGlzLnNjcm9sbHZpZXcuc2Nyb2xsVG9Ub3AoMCk7XG4gICAgLy8gdGhpcy5pbmZvU3ByaXRlLnNwcml0ZUZyYW1lID0gbnVsbDtcbiAgICAvLyBsZXQgaW5mb1Jlc291cmNlcyA9IFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5nZXRHYW1lSW5mbygpXG4gICAgLy8gaWYoaW5mb1Jlc291cmNlcyA9PSBudWxsKSByZXR1cm5cbiAgICAvLyBpbmZvUmVzb3VyY2VzLmZvckVhY2goKGluZm8pID0+IHtcbiAgICAvLyAgIFNpY2JvUmVzb3VyY2VNYW5hZ2VyLmxvYWRSZW1vdGVJbWFnZVdpdGhGaWxlRmllbGQodGhpcy5pbmZvU3ByaXRlLCBpbmZvKTtcbiAgICAvLyB9KTtcbiAgfVxuXG4gIG9uQnV0dG9uQ2xvc2UoKSB7XG4gICAgdGhpcy5jbG9zZSgpO1xuICB9XG59XG4iXX0=