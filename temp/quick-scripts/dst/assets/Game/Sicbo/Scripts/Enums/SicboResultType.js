
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Enums/SicboResultType.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '685belZ3YxFwotjWM3/Npnm', 'SicboResultType');
// Game/Sicbo_New/Scripts/Enums/SicboResultType.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboResultType = void 0;
var SicboResultType;
(function (SicboResultType) {
    SicboResultType[SicboResultType["TAI"] = 0] = "TAI";
    SicboResultType[SicboResultType["XIU"] = 1] = "XIU";
    SicboResultType[SicboResultType["BAO"] = 2] = "BAO";
})(SicboResultType = exports.SicboResultType || (exports.SicboResultType = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0VudW1zL1NpY2JvUmVzdWx0VHlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxJQUFZLGVBSVg7QUFKRCxXQUFZLGVBQWU7SUFDekIsbURBQU8sQ0FBQTtJQUNQLG1EQUFPLENBQUE7SUFDUCxtREFBTyxDQUFBO0FBQ1QsQ0FBQyxFQUpXLGVBQWUsR0FBZix1QkFBZSxLQUFmLHVCQUFlLFFBSTFCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gU2ljYm9SZXN1bHRUeXBlIHtcbiAgVEFJID0gMCxcbiAgWElVID0gMSxcbiAgQkFPID0gMixcbn1cbiJdfQ==