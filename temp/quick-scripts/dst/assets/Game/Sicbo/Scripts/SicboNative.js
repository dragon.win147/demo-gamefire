
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/SicboNative.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8ca3ck80uFH9IF5nBQOQuBW', 'SicboNative');
// Game/Sicbo_New/Scripts/SicboNative.ts

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboNative = /** @class */ (function () {
    function SicboNative() {
        this.DEFAULT_PACKAGE_NAME = "org/cocos2dx/javascript/";
        SicboNative_1.instance = this;
    }
    SicboNative_1 = SicboNative;
    SicboNative.getInstance = function () {
        if (SicboNative_1.instance == null) {
            SicboNative_1.instance = new SicboNative_1();
        }
        return SicboNative_1.instance;
    };
    /**
     * call jsb with class name is default on native
     * @param arg
     */
    SicboNative.prototype.call = function () {
        var arg = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            arg[_i] = arguments[_i];
        }
        var args = Array.prototype.slice.call(arguments);
        if (cc.sys.os == cc.sys.OS_ANDROID) {
            args.splice(0, 0, "AndroidNativeUtils");
        }
        else if (cc.sys.os == cc.sys.OS_IOS) {
            args.splice(0, 0, "IOSNativeUtils");
        }
        else {
            return;
        }
        this.callClz.apply(this, args);
    };
    /**
     * call jsb with class name is clz
     * @param clz
     * @param funcName
     * @param arg
     */
    SicboNative.prototype.callClz = function (clz, funcName) {
        var arg = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            arg[_i - 2] = arguments[_i];
        }
        //console.log("callClz clz:", clz);
        var args = Array.prototype.slice.call(arguments);
        args.splice(2, 0, "V");
        return this._callClz.apply(this, args);
    };
    SicboNative.prototype._callClz = function (clz, funcName, returnType) {
        //console.log("_callClz clz:", clz);
        var args = Array.prototype.slice.call(arguments);
        args.unshift(this.DEFAULT_PACKAGE_NAME);
        return this.callClzWithPackage.apply(this, args);
    };
    SicboNative.prototype.callClzWithPackage = function (pkg, clz, funcName, returnType) {
        var args = Array.prototype.slice.call(arguments);
        args.splice(0, 4);
        var real_args = [clz, funcName];
        //console.log("clz:", clz);
        //console.log("funcName:", funcName);
        if (cc.sys.os == cc.sys.OS_ANDROID) {
            real_args[0] = pkg + clz;
            real_args[2] = "()" + returnType;
            if (args.length > 0) {
                var sig_1 = "";
                args.forEach(function (v) {
                    switch (typeof v) {
                        case "boolean":
                            sig_1 += "Z";
                            real_args.push(v);
                            break;
                        case "string":
                            sig_1 += "Ljava/lang/String;";
                            real_args.push(v);
                            break;
                        case "number":
                            sig_1 += "D";
                            real_args.push(v);
                            break;
                        case "function":
                            sig_1 += "Ljava/lang/String;";
                            //   real_args.push(this._newCB(v));
                            real_args.push(v);
                            break;
                    }
                });
                real_args[2] = "(" + sig_1 + ")" + returnType;
            }
        }
        else if (cc.sys.os == cc.sys.OS_IOS) {
            if (args.length > 0) {
                for (var i = 0; i < args.length; i++) {
                    var v = args[i];
                    //   if (typeof v == "function") {
                    //     real_args.push(this._newCB(v));
                    //   } else {
                    real_args.push(v);
                    //   }
                    if (i == 0) {
                        funcName += ":";
                    }
                    else {
                        funcName += "arg" + i + ":";
                    }
                }
                real_args[1] = funcName;
            }
        }
        else {
            return;
        }
        //console.log("real_args:", real_args);
        return jsb.reflection.callStaticMethod.apply(jsb.reflection, real_args);
    };
    SicboNative.prototype.getStr = function (clz, funcName) {
        var args = Array.prototype.slice.call(arguments);
        args.splice(2, 0, "Ljava/lang/String;");
        return this._callClz.apply(this, args);
    };
    var SicboNative_1;
    SicboNative = SicboNative_1 = __decorate([
        ccclass
    ], SicboNative);
    return SicboNative;
}());
exports.default = SicboNative;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1NpY2JvTmF0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFtQjVDO0lBV0U7UUFGUSx5QkFBb0IsR0FBVywwQkFBMEIsQ0FBQztRQUdoRSxhQUFXLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUM5QixDQUFDO29CQWJrQixXQUFXO0lBRXZCLHVCQUFXLEdBQWxCO1FBQ0UsSUFBSSxhQUFXLENBQUMsUUFBUSxJQUFJLElBQUksRUFBRTtZQUNoQyxhQUFXLENBQUMsUUFBUSxHQUFHLElBQUksYUFBVyxFQUFFLENBQUM7U0FDMUM7UUFDRCxPQUFPLGFBQVcsQ0FBQyxRQUFRLENBQUM7SUFDOUIsQ0FBQztJQU9EOzs7T0FHRztJQUNILDBCQUFJLEdBQUo7UUFBSyxhQUFNO2FBQU4sVUFBTSxFQUFOLHFCQUFNLEVBQU4sSUFBTTtZQUFOLHdCQUFNOztRQUNULElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNqRCxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFO1lBQ2xDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1NBQ3pDO2FBQU0sSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRTtZQUNyQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztTQUNyQzthQUFNO1lBQ0wsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFDRDs7Ozs7T0FLRztJQUNILDZCQUFPLEdBQVAsVUFBUSxHQUFHLEVBQUUsUUFBUTtRQUFFLGFBQU07YUFBTixVQUFNLEVBQU4scUJBQU0sRUFBTixJQUFNO1lBQU4sNEJBQU07O1FBQzNCLG1DQUFtQztRQUNuQyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCw4QkFBUSxHQUFSLFVBQVMsR0FBRyxFQUFFLFFBQVEsRUFBRSxVQUFVO1FBQ2hDLG9DQUFvQztRQUNwQyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN4QyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCx3Q0FBa0IsR0FBbEIsVUFBbUIsR0FBRyxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsVUFBVTtRQUMvQyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDbEIsSUFBSSxTQUFTLEdBQUcsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFFaEMsMkJBQTJCO1FBQzNCLHFDQUFxQztRQUVyQyxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFO1lBQ2xDLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDO1lBQ3pCLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLEdBQUcsVUFBVSxDQUFDO1lBQ2pDLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ25CLElBQUksS0FBRyxHQUFHLEVBQUUsQ0FBQztnQkFDYixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQztvQkFDYixRQUFRLE9BQU8sQ0FBQyxFQUFFO3dCQUNoQixLQUFLLFNBQVM7NEJBQ1osS0FBRyxJQUFJLEdBQUcsQ0FBQzs0QkFDWCxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNsQixNQUFNO3dCQUNSLEtBQUssUUFBUTs0QkFDWCxLQUFHLElBQUksb0JBQW9CLENBQUM7NEJBQzVCLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ2xCLE1BQU07d0JBQ1IsS0FBSyxRQUFROzRCQUNYLEtBQUcsSUFBSSxHQUFHLENBQUM7NEJBQ1gsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbEIsTUFBTTt3QkFDUixLQUFLLFVBQVU7NEJBQ2IsS0FBRyxJQUFJLG9CQUFvQixDQUFDOzRCQUM1QixvQ0FBb0M7NEJBQ3BDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ2xCLE1BQU07cUJBQ1Q7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxLQUFHLEdBQUcsR0FBRyxHQUFHLFVBQVUsQ0FBQzthQUM3QztTQUNGO2FBQU0sSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRTtZQUNyQyxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNuQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDcEMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNoQixrQ0FBa0M7b0JBQ2xDLHNDQUFzQztvQkFDdEMsYUFBYTtvQkFDYixTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNsQixNQUFNO29CQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDVixRQUFRLElBQUksR0FBRyxDQUFDO3FCQUNqQjt5QkFBTTt3QkFDTCxRQUFRLElBQUksS0FBSyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUM7cUJBQzdCO2lCQUNGO2dCQUVELFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUM7YUFDekI7U0FDRjthQUFNO1lBQ0wsT0FBTztTQUNSO1FBQ0QsdUNBQXVDO1FBQ3ZDLE9BQU8sR0FBRyxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUMxRSxDQUFDO0lBRUQsNEJBQU0sR0FBTixVQUFPLEdBQUcsRUFBRSxRQUFRO1FBQ2xCLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztRQUN4QyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN6QyxDQUFDOztJQW5Ia0IsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQW9IL0I7SUFBRCxrQkFBQztDQXBIRCxBQW9IQyxJQUFBO2tCQXBIb0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbi8qKlxuICogXG4gKiBOYXRpdmUgQ2FsbGVyXG4gKiBib29sL3N0cmluZy9udW1iZXIvc3RyaW5nL2Z1bmN0aW9uXG4gICAgYW5kcm9pZCAtPiBvcmcuY29jb3MyZHguamF2YXNjcmlwdC5OYXRpdmUuU2F5SGVsbG8oU3RyaW5nIGhlbGxvU3RyaW5nLCBTdHJpbmcgY2JOYW1lKTtcbiAgICBpb3MgICAgLT4gTmF0aXZlLlNheUhlbGxvIDogKE5TU3RyaW5nKikgaGVsbG9TdHJpbmdcbiAgICAgICAgICAgICAgICAgICAgICAgICBhcmcxIDogKE5TU3RyaW5nKikgY2JOYW1lO1xuICogbmF0aXZlLmNhbGwoXCJTYXlIZWxsb1wiLCBcImhlbGxvIHdvcmxkXCIsIChvaykgPT4geyB9KVxuICogbmF0aXZlLmNhbGxDbHooXCJOYXRpdmVcIiwgXCJTYXlIZWxsb1wiLCBcImhlbGxvIHdvcmxkXCIsIChvaykgPT4geyB9KVxuICogbGV0IHN0ciA9IG5hdGl2ZS5nZXRTdHIoXCJOYXRpdmVcIiwgXCJzb21lQXJnXCIpXG4qL1xuZGVjbGFyZSBnbG9iYWwge1xuICBpbnRlcmZhY2UgV2luZG93IHtcbiAgICBqc19uYXRpdmVfY2I6IGFueTtcbiAgfVxufVxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvTmF0aXZlIHtcbiAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IFNpY2JvTmF0aXZlO1xuICBzdGF0aWMgZ2V0SW5zdGFuY2UoKTogU2ljYm9OYXRpdmUge1xuICAgIGlmIChTaWNib05hdGl2ZS5pbnN0YW5jZSA9PSBudWxsKSB7XG4gICAgICBTaWNib05hdGl2ZS5pbnN0YW5jZSA9IG5ldyBTaWNib05hdGl2ZSgpO1xuICAgIH1cbiAgICByZXR1cm4gU2ljYm9OYXRpdmUuaW5zdGFuY2U7XG4gIH1cblxuICBwcml2YXRlIERFRkFVTFRfUEFDS0FHRV9OQU1FOiBzdHJpbmcgPSBcIm9yZy9jb2NvczJkeC9qYXZhc2NyaXB0L1wiO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIFNpY2JvTmF0aXZlLmluc3RhbmNlID0gdGhpcztcbiAgfVxuICAvKipcbiAgICogY2FsbCBqc2Igd2l0aCBjbGFzcyBuYW1lIGlzIGRlZmF1bHQgb24gbmF0aXZlXG4gICAqIEBwYXJhbSBhcmdcbiAgICovXG4gIGNhbGwoLi4uYXJnKSB7XG4gICAgbGV0IGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpO1xuICAgIGlmIChjYy5zeXMub3MgPT0gY2Muc3lzLk9TX0FORFJPSUQpIHtcbiAgICAgIGFyZ3Muc3BsaWNlKDAsIDAsIFwiQW5kcm9pZE5hdGl2ZVV0aWxzXCIpO1xuICAgIH0gZWxzZSBpZiAoY2Muc3lzLm9zID09IGNjLnN5cy5PU19JT1MpIHtcbiAgICAgIGFyZ3Muc3BsaWNlKDAsIDAsIFwiSU9TTmF0aXZlVXRpbHNcIik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLmNhbGxDbHouYXBwbHkodGhpcywgYXJncyk7XG4gIH1cbiAgLyoqXG4gICAqIGNhbGwganNiIHdpdGggY2xhc3MgbmFtZSBpcyBjbHpcbiAgICogQHBhcmFtIGNselxuICAgKiBAcGFyYW0gZnVuY05hbWVcbiAgICogQHBhcmFtIGFyZ1xuICAgKi9cbiAgY2FsbENseihjbHosIGZ1bmNOYW1lLCAuLi5hcmcpIHtcbiAgICAvL2NvbnNvbGUubG9nKFwiY2FsbENseiBjbHo6XCIsIGNseik7XG4gICAgbGV0IGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpO1xuICAgIGFyZ3Muc3BsaWNlKDIsIDAsIFwiVlwiKTtcbiAgICByZXR1cm4gdGhpcy5fY2FsbENsei5hcHBseSh0aGlzLCBhcmdzKTtcbiAgfVxuXG4gIF9jYWxsQ2x6KGNseiwgZnVuY05hbWUsIHJldHVyblR5cGUpIHtcbiAgICAvL2NvbnNvbGUubG9nKFwiX2NhbGxDbHogY2x6OlwiLCBjbHopO1xuICAgIGxldCBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzKTtcbiAgICBhcmdzLnVuc2hpZnQodGhpcy5ERUZBVUxUX1BBQ0tBR0VfTkFNRSk7XG4gICAgcmV0dXJuIHRoaXMuY2FsbENseldpdGhQYWNrYWdlLmFwcGx5KHRoaXMsIGFyZ3MpO1xuICB9XG5cbiAgY2FsbENseldpdGhQYWNrYWdlKHBrZywgY2x6LCBmdW5jTmFtZSwgcmV0dXJuVHlwZSkge1xuICAgIGxldCBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzKTtcbiAgICBhcmdzLnNwbGljZSgwLCA0KTtcbiAgICBsZXQgcmVhbF9hcmdzID0gW2NseiwgZnVuY05hbWVdO1xuXG4gICAgLy9jb25zb2xlLmxvZyhcImNsejpcIiwgY2x6KTtcbiAgICAvL2NvbnNvbGUubG9nKFwiZnVuY05hbWU6XCIsIGZ1bmNOYW1lKTtcblxuICAgIGlmIChjYy5zeXMub3MgPT0gY2Muc3lzLk9TX0FORFJPSUQpIHtcbiAgICAgIHJlYWxfYXJnc1swXSA9IHBrZyArIGNsejtcbiAgICAgIHJlYWxfYXJnc1syXSA9IFwiKClcIiArIHJldHVyblR5cGU7XG4gICAgICBpZiAoYXJncy5sZW5ndGggPiAwKSB7XG4gICAgICAgIGxldCBzaWcgPSBcIlwiO1xuICAgICAgICBhcmdzLmZvckVhY2goKHYpID0+IHtcbiAgICAgICAgICBzd2l0Y2ggKHR5cGVvZiB2KSB7XG4gICAgICAgICAgICBjYXNlIFwiYm9vbGVhblwiOlxuICAgICAgICAgICAgICBzaWcgKz0gXCJaXCI7XG4gICAgICAgICAgICAgIHJlYWxfYXJncy5wdXNoKHYpO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgXCJzdHJpbmdcIjpcbiAgICAgICAgICAgICAgc2lnICs9IFwiTGphdmEvbGFuZy9TdHJpbmc7XCI7XG4gICAgICAgICAgICAgIHJlYWxfYXJncy5wdXNoKHYpO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgXCJudW1iZXJcIjpcbiAgICAgICAgICAgICAgc2lnICs9IFwiRFwiO1xuICAgICAgICAgICAgICByZWFsX2FyZ3MucHVzaCh2KTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIFwiZnVuY3Rpb25cIjpcbiAgICAgICAgICAgICAgc2lnICs9IFwiTGphdmEvbGFuZy9TdHJpbmc7XCI7XG4gICAgICAgICAgICAgIC8vICAgcmVhbF9hcmdzLnB1c2godGhpcy5fbmV3Q0IodikpO1xuICAgICAgICAgICAgICByZWFsX2FyZ3MucHVzaCh2KTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgcmVhbF9hcmdzWzJdID0gXCIoXCIgKyBzaWcgKyBcIilcIiArIHJldHVyblR5cGU7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChjYy5zeXMub3MgPT0gY2Muc3lzLk9TX0lPUykge1xuICAgICAgaWYgKGFyZ3MubGVuZ3RoID4gMCkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFyZ3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBsZXQgdiA9IGFyZ3NbaV07XG4gICAgICAgICAgLy8gICBpZiAodHlwZW9mIHYgPT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgLy8gICAgIHJlYWxfYXJncy5wdXNoKHRoaXMuX25ld0NCKHYpKTtcbiAgICAgICAgICAvLyAgIH0gZWxzZSB7XG4gICAgICAgICAgcmVhbF9hcmdzLnB1c2godik7XG4gICAgICAgICAgLy8gICB9XG4gICAgICAgICAgaWYgKGkgPT0gMCkge1xuICAgICAgICAgICAgZnVuY05hbWUgKz0gXCI6XCI7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGZ1bmNOYW1lICs9IFwiYXJnXCIgKyBpICsgXCI6XCI7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmVhbF9hcmdzWzFdID0gZnVuY05hbWU7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgLy9jb25zb2xlLmxvZyhcInJlYWxfYXJnczpcIiwgcmVhbF9hcmdzKTtcbiAgICByZXR1cm4ganNiLnJlZmxlY3Rpb24uY2FsbFN0YXRpY01ldGhvZC5hcHBseShqc2IucmVmbGVjdGlvbiwgcmVhbF9hcmdzKTtcbiAgfVxuXG4gIGdldFN0cihjbHosIGZ1bmNOYW1lKSB7XG4gICAgbGV0IGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpO1xuICAgIGFyZ3Muc3BsaWNlKDIsIDAsIFwiTGphdmEvbGFuZy9TdHJpbmc7XCIpO1xuICAgIHJldHVybiB0aGlzLl9jYWxsQ2x6LmFwcGx5KHRoaXMsIGFyZ3MpO1xuICB9XG59XG4iXX0=