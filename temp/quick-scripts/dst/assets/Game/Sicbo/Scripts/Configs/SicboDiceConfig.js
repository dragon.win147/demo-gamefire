
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Configs/SicboDiceConfig.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e61d21/C2tHo40Kx1X8zgZT', 'SicboDiceConfig');
// Game/Sicbo_New/Scripts/Configs/SicboDiceConfig.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboDiceConfig = /** @class */ (function (_super) {
    __extends(SicboDiceConfig, _super);
    function SicboDiceConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.diceSprites = [];
        return _this;
    }
    SicboDiceConfig_1 = SicboDiceConfig;
    Object.defineProperty(SicboDiceConfig, "Instance", {
        get: function () {
            return SicboDiceConfig_1.instance;
        },
        enumerable: false,
        configurable: true
    });
    SicboDiceConfig.prototype.onLoad = function () {
        SicboDiceConfig_1.instance = this;
    };
    SicboDiceConfig.prototype.onDestroy = function () {
        delete SicboDiceConfig_1.instance;
        SicboDiceConfig_1.instance = null;
    };
    SicboDiceConfig.prototype.getDice = function (diceName) {
        var rs = null;
        SicboDiceConfig_1.Instance.diceSprites.forEach(function (element) {
            if (element.name == diceName) {
                rs = element;
            }
        });
        return rs;
    };
    var SicboDiceConfig_1;
    SicboDiceConfig.instance = null;
    __decorate([
        property([cc.SpriteFrame])
    ], SicboDiceConfig.prototype, "diceSprites", void 0);
    SicboDiceConfig = SicboDiceConfig_1 = __decorate([
        ccclass
    ], SicboDiceConfig);
    return SicboDiceConfig;
}(cc.Component));
exports.default = SicboDiceConfig;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbmZpZ3MvU2ljYm9EaWNlQ29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRTVFLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQTZDLG1DQUFZO0lBQXpEO1FBQUEscUVBNkJDO1FBMUJHLGlCQUFXLEdBQXFCLEVBQUUsQ0FBQzs7SUEwQnZDLENBQUM7d0JBN0JvQixlQUFlO0lBTWhDLHNCQUFXLDJCQUFRO2FBQW5CO1lBQ0UsT0FBTyxpQkFBZSxDQUFDLFFBQVEsQ0FBQztRQUNsQyxDQUFDOzs7T0FBQTtJQUVELGdDQUFNLEdBQU47UUFDSSxpQkFBZSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDcEMsQ0FBQztJQUVELG1DQUFTLEdBQVQ7UUFDSSxPQUFPLGlCQUFlLENBQUMsUUFBUSxDQUFDO1FBQ2hDLGlCQUFlLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUNsQyxDQUFDO0lBR0gsaUNBQU8sR0FBUCxVQUFRLFFBQWdCO1FBQ3BCLElBQUksRUFBRSxHQUFtQixJQUFJLENBQUM7UUFDOUIsaUJBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87WUFDaEQsSUFBRyxPQUFPLENBQUMsSUFBSSxJQUFJLFFBQVEsRUFBQztnQkFDeEIsRUFBRSxHQUFHLE9BQU8sQ0FBQzthQUNoQjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDOztJQXZCYyx3QkFBUSxHQUFvQixJQUFJLENBQUM7SUFGaEQ7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUM7d0RBQ1E7SUFIbEIsZUFBZTtRQURuQyxPQUFPO09BQ2EsZUFBZSxDQTZCbkM7SUFBRCxzQkFBQztDQTdCRCxBQTZCQyxDQTdCNEMsRUFBRSxDQUFDLFNBQVMsR0E2QnhEO2tCQTdCb0IsZUFBZSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvRGljZUNvbmZpZyBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBAcHJvcGVydHkoW2NjLlNwcml0ZUZyYW1lXSlcbiAgICBkaWNlU3ByaXRlczogY2MuU3ByaXRlRnJhbWVbXSA9IFtdO1xuXG4gICAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IFNpY2JvRGljZUNvbmZpZyA9IG51bGw7XG4gICAgc3RhdGljIGdldCBJbnN0YW5jZSgpOiBTaWNib0RpY2VDb25maWcge1xuICAgICAgcmV0dXJuIFNpY2JvRGljZUNvbmZpZy5pbnN0YW5jZTtcbiAgICB9XG5cbiAgICBvbkxvYWQoKXtcbiAgICAgICAgU2ljYm9EaWNlQ29uZmlnLmluc3RhbmNlID0gdGhpcztcbiAgICB9XG5cbiAgICBvbkRlc3Ryb3koKSB7XG4gICAgICAgIGRlbGV0ZSBTaWNib0RpY2VDb25maWcuaW5zdGFuY2U7XG4gICAgICAgIFNpY2JvRGljZUNvbmZpZy5pbnN0YW5jZSA9IG51bGw7XG4gICAgICB9XG4gICAgXG5cbiAgICBnZXREaWNlKGRpY2VOYW1lOiBzdHJpbmcpOiBjYy5TcHJpdGVGcmFtZSB7XG4gICAgICAgIGxldCByczogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xuICAgICAgICBTaWNib0RpY2VDb25maWcuSW5zdGFuY2UuZGljZVNwcml0ZXMuZm9yRWFjaChlbGVtZW50ID0+IHtcbiAgICAgICAgICAgIGlmKGVsZW1lbnQubmFtZSA9PSBkaWNlTmFtZSl7XG4gICAgICAgICAgICAgICAgcnMgPSBlbGVtZW50O1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHJzO1xuICAgIH1cbn1cbiJdfQ==