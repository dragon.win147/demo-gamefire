
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Configs/SicboChipSpriteConfig.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '11bfdYTi4hMhLrZf+dZFoOY', 'SicboChipSpriteConfig');
// Game/Sicbo_New/Scripts/Configs/SicboChipSpriteConfig.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboChipSpriteConfig = /** @class */ (function (_super) {
    __extends(SicboChipSpriteConfig, _super);
    function SicboChipSpriteConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.normalChips = [];
        _this.disableChips = [];
        _this.miniChips = [];
        return _this;
    }
    SicboChipSpriteConfig_1 = SicboChipSpriteConfig;
    Object.defineProperty(SicboChipSpriteConfig, "Instance", {
        get: function () {
            return SicboChipSpriteConfig_1.instance;
        },
        enumerable: false,
        configurable: true
    });
    SicboChipSpriteConfig.prototype.onLoad = function () {
        SicboChipSpriteConfig_1.instance = this;
    };
    SicboChipSpriteConfig.prototype.onDestroy = function () {
        delete SicboChipSpriteConfig_1.instance;
        SicboChipSpriteConfig_1.instance = null;
    };
    SicboChipSpriteConfig.prototype.getNormalChipSprite = function (spriteName) {
        var rs = null;
        SicboChipSpriteConfig_1.Instance.normalChips.forEach(function (element) {
            if (element.name == spriteName) {
                rs = element;
            }
        });
        return rs;
    };
    SicboChipSpriteConfig.prototype.getDisableChipSprite = function (spriteName) {
        var rs = null;
        SicboChipSpriteConfig_1.Instance.disableChips.forEach(function (element) {
            if (element.name == spriteName) {
                rs = element;
            }
        });
        return rs;
    };
    SicboChipSpriteConfig.prototype.getMiniChipSprite = function (spriteName) {
        var rs = null;
        SicboChipSpriteConfig_1.Instance.miniChips.forEach(function (element) {
            if (element.name == spriteName) {
                rs = element;
            }
        });
        return rs;
    };
    var SicboChipSpriteConfig_1;
    SicboChipSpriteConfig.instance = null;
    __decorate([
        property([cc.SpriteFrame])
    ], SicboChipSpriteConfig.prototype, "normalChips", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], SicboChipSpriteConfig.prototype, "disableChips", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], SicboChipSpriteConfig.prototype, "miniChips", void 0);
    SicboChipSpriteConfig = SicboChipSpriteConfig_1 = __decorate([
        ccclass
    ], SicboChipSpriteConfig);
    return SicboChipSpriteConfig;
}(cc.Component));
exports.default = SicboChipSpriteConfig;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbmZpZ3MvU2ljYm9DaGlwU3ByaXRlQ29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRTVFLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQW1ELHlDQUFZO0lBQS9EO1FBQUEscUVBd0RDO1FBckRHLGlCQUFXLEdBQXFCLEVBQUUsQ0FBQztRQUduQyxrQkFBWSxHQUFxQixFQUFFLENBQUM7UUFJcEMsZUFBUyxHQUFxQixFQUFFLENBQUM7O0lBOENyQyxDQUFDOzhCQXhEb0IscUJBQXFCO0lBYXRDLHNCQUFXLGlDQUFRO2FBQW5CO1lBQ0UsT0FBTyx1QkFBcUIsQ0FBQyxRQUFRLENBQUM7UUFDeEMsQ0FBQzs7O09BQUE7SUFFRCxzQ0FBTSxHQUFOO1FBQ0ksdUJBQXFCLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUMxQyxDQUFDO0lBRUQseUNBQVMsR0FBVDtRQUNJLE9BQU8sdUJBQXFCLENBQUMsUUFBUSxDQUFDO1FBQ3RDLHVCQUFxQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDeEMsQ0FBQztJQUdILG1EQUFtQixHQUFuQixVQUFvQixVQUFrQjtRQUNsQyxJQUFJLEVBQUUsR0FBbUIsSUFBSSxDQUFDO1FBQzlCLHVCQUFxQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUN0RCxJQUFHLE9BQU8sQ0FBQyxJQUFJLElBQUksVUFBVSxFQUFDO2dCQUMxQixFQUFFLEdBQUcsT0FBTyxDQUFDO2FBQ2hCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCxvREFBb0IsR0FBcEIsVUFBcUIsVUFBa0I7UUFDbkMsSUFBSSxFQUFFLEdBQW1CLElBQUksQ0FBQztRQUM5Qix1QkFBcUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87WUFDdkQsSUFBRyxPQUFPLENBQUMsSUFBSSxJQUFJLFVBQVUsRUFBQztnQkFDMUIsRUFBRSxHQUFHLE9BQU8sQ0FBQzthQUNoQjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQsaURBQWlCLEdBQWpCLFVBQWtCLFVBQWtCO1FBQ2hDLElBQUksRUFBRSxHQUFtQixJQUFJLENBQUM7UUFDOUIsdUJBQXFCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO1lBQ3BELElBQUcsT0FBTyxDQUFDLElBQUksSUFBSSxVQUFVLEVBQUM7Z0JBQzFCLEVBQUUsR0FBRyxPQUFPLENBQUM7YUFDaEI7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQzs7SUEzQ2MsOEJBQVEsR0FBMEIsSUFBSSxDQUFDO0lBVHREO1FBREMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDOzhEQUNRO0lBR25DO1FBREMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDOytEQUNTO0lBSXBDO1FBREMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDOzREQUNNO0lBVmhCLHFCQUFxQjtRQUR6QyxPQUFPO09BQ2EscUJBQXFCLENBd0R6QztJQUFELDRCQUFDO0NBeERELEFBd0RDLENBeERrRCxFQUFFLENBQUMsU0FBUyxHQXdEOUQ7a0JBeERvQixxQkFBcUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib0NoaXBTcHJpdGVDb25maWcgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KFtjYy5TcHJpdGVGcmFtZV0pXG4gICAgbm9ybWFsQ2hpcHM6IGNjLlNwcml0ZUZyYW1lW10gPSBbXTtcblxuICAgIEBwcm9wZXJ0eShbY2MuU3ByaXRlRnJhbWVdKVxuICAgIGRpc2FibGVDaGlwczogY2MuU3ByaXRlRnJhbWVbXSA9IFtdO1xuXG5cbiAgICBAcHJvcGVydHkoW2NjLlNwcml0ZUZyYW1lXSlcbiAgICBtaW5pQ2hpcHM6IGNjLlNwcml0ZUZyYW1lW10gPSBbXTtcblxuICAgIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBTaWNib0NoaXBTcHJpdGVDb25maWcgPSBudWxsO1xuICAgIHN0YXRpYyBnZXQgSW5zdGFuY2UoKTogU2ljYm9DaGlwU3ByaXRlQ29uZmlnIHtcbiAgICAgIHJldHVybiBTaWNib0NoaXBTcHJpdGVDb25maWcuaW5zdGFuY2U7XG4gICAgfVxuXG4gICAgb25Mb2FkKCl7XG4gICAgICAgIFNpY2JvQ2hpcFNwcml0ZUNvbmZpZy5pbnN0YW5jZSA9IHRoaXM7XG4gICAgfVxuXG4gICAgb25EZXN0cm95KCkge1xuICAgICAgICBkZWxldGUgU2ljYm9DaGlwU3ByaXRlQ29uZmlnLmluc3RhbmNlO1xuICAgICAgICBTaWNib0NoaXBTcHJpdGVDb25maWcuaW5zdGFuY2UgPSBudWxsO1xuICAgICAgfVxuICAgIFxuXG4gICAgZ2V0Tm9ybWFsQ2hpcFNwcml0ZShzcHJpdGVOYW1lOiBzdHJpbmcpOiBjYy5TcHJpdGVGcmFtZSB7XG4gICAgICAgIGxldCByczogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xuICAgICAgICBTaWNib0NoaXBTcHJpdGVDb25maWcuSW5zdGFuY2Uubm9ybWFsQ2hpcHMuZm9yRWFjaChlbGVtZW50ID0+IHtcbiAgICAgICAgICAgIGlmKGVsZW1lbnQubmFtZSA9PSBzcHJpdGVOYW1lKXtcbiAgICAgICAgICAgICAgICBycyA9IGVsZW1lbnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gcnM7XG4gICAgfVxuXG4gICAgZ2V0RGlzYWJsZUNoaXBTcHJpdGUoc3ByaXRlTmFtZTogc3RyaW5nKTogY2MuU3ByaXRlRnJhbWUge1xuICAgICAgICBsZXQgcnM6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcbiAgICAgICAgU2ljYm9DaGlwU3ByaXRlQ29uZmlnLkluc3RhbmNlLmRpc2FibGVDaGlwcy5mb3JFYWNoKGVsZW1lbnQgPT4ge1xuICAgICAgICAgICAgaWYoZWxlbWVudC5uYW1lID09IHNwcml0ZU5hbWUpe1xuICAgICAgICAgICAgICAgIHJzID0gZWxlbWVudDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBycztcbiAgICB9XG5cbiAgICBnZXRNaW5pQ2hpcFNwcml0ZShzcHJpdGVOYW1lOiBzdHJpbmcpOiBjYy5TcHJpdGVGcmFtZSB7XG4gICAgICAgIGxldCByczogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xuICAgICAgICBTaWNib0NoaXBTcHJpdGVDb25maWcuSW5zdGFuY2UubWluaUNoaXBzLmZvckVhY2goZWxlbWVudCA9PiB7XG4gICAgICAgICAgICBpZihlbGVtZW50Lm5hbWUgPT0gc3ByaXRlTmFtZSl7XG4gICAgICAgICAgICAgICAgcnMgPSBlbGVtZW50O1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHJzO1xuICAgIH1cbn1cbiJdfQ==