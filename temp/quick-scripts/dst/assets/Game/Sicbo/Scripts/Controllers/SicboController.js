
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Controllers/SicboController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'be40et5SA5E16BUkW/4QWUD', 'SicboController');
// Game/Sicbo/Scripts/Controllers/SicboController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Code = _a.Code, Topic = _a.Topic, State = _a.State, Event = _a.Event, DoorAndBetAmount = _a.DoorAndBetAmount, Status = _a.Status, WhatAppMessage = _a.WhatAppMessage, EventKey = _a.EventKey, AutoJoinReply = _a.AutoJoinReply;
var SicboUIComp_1 = require("../Components/SicboUIComp");
var SicboHelper_1 = require("../Helpers/SicboHelper");
var SicboMessageHandler_1 = require("../SicboMessageHandler");
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboSoundManager_1 = require("../Managers/SicboSoundManager");
var SicboPortalAdapter_1 = require("../SicboPortalAdapter");
var SicboHandleEventShowHideComp_1 = require("../RNGCommons/SicboHandleEventShowHideComp");
var SicboTracking_1 = require("../SicboTracking");
var SicboUIManager_1 = require("../Managers/SicboUIManager");
var ccclass = cc._decorator.ccclass;
var SicboController = /** @class */ (function (_super) {
    __extends(SicboController, _super);
    function SicboController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.stateStatus = Status.STATUS_UNSPECIFIED;
        _this.isKicked = false;
        _this.handleEventShowHideComp = null;
        _this.lastTickTime = -100;
        _this.lastSession = 0;
        _this.appHiding = false;
        _this.appResuming = false;
        _this.isDisconnected = false;
        return _this;
    }
    SicboController_1 = SicboController;
    Object.defineProperty(SicboController, "Instance", {
        get: function () {
            return SicboController_1.instance;
        },
        enumerable: false,
        configurable: true
    });
    SicboController.prototype.onLoad = function () {
        SicboController_1.instance = this;
        if (!SicboController_1.Instance.handleEventShowHideComp)
            SicboController_1.Instance.handleEventShowHideComp = SicboController_1.Instance.addComponent(SicboHandleEventShowHideComp_1.default); //Device home button
        SicboController_1.Instance.handleEventShowHideComp.listenEvent(function () { return SicboController_1.Instance.onHideEvent(); }, function () { return SicboController_1.Instance.onShowEvent(); });
        cc.systemEvent.on(EventKey.ON_PING_ERROR, function (code, isRetry, messError) {
            SicboMessageHandler_1.default.IS_WS_DISCONNECTED = true;
            SicboController_1.Instance.isDisconnected = true;
            SicboController_1.Instance.onHideEvent();
            //cc.warn("EventKey.ON_DISCONNECT_EVENT", code, isRetry, messError)
        }, this);
        cc.systemEvent.on(EventKey.ON_RECONNECTED_EVENT, function () {
            SicboMessageHandler_1.default.IS_WS_DISCONNECTED = false;
            SicboController_1.Instance.isDisconnected = false;
            SicboUIManager_1.default.getInstance().hidePopup(SicboUIManager_1.default.SicboCannotJoinPopup);
            SicboController_1.Instance.onShowEvent();
        }, this);
    };
    SicboController.prototype.start = function () {
        SicboMessageHandler_1.default.getInstance().receiveState = SicboController_1.Instance.onReceiveState;
        SicboMessageHandler_1.default.getInstance().onErrorCustom = SicboController_1.Instance.onError;
        cc.systemEvent.on(EventKey.ON_ADD_BALANCE_EVENT, SicboController_1.Instance.onBalanceAdded);
        cc.systemEvent.on(EventKey.ON_UPDATE_BALANCE_EVENT, SicboController_1.Instance.onBalanceUpdated);
        SicboPortalAdapter_1.default.getInstance().getLatestBalance();
        SicboController_1.Instance.onBalanceUpdated();
        SicboController_1.Instance.playBgm();
        // SicboPortalAdapter.getInstance().getBundleInfo(bundleInfo=>{
        //   SicboTracking.setGameVersion(bundleInfo?.version);
        // })
    };
    SicboController.prototype.onBalanceUpdated = function () {
        SicboPortalAdapter_1.default.getInstance().getCurrentBalance(function (balance) {
            SicboUIComp_1.default.Instance.changeMyUserBalance(balance);
        });
    };
    SicboController.prototype.onBalanceAdded = function () {
        SicboPortalAdapter_1.default.getInstance().getCurrentBalance(function (balance) {
            SicboUIComp_1.default.Instance.changeMyUserBalance(balance);
        });
    };
    SicboController.prototype.onEnable = function () {
        SicboPortalAdapter_1.default.getInstance().enableWallet();
        var chanelID = SicboMessageHandler_1.default.getInstance().getChannelId();
        this.handleNotEnoughMoneyOnJointGame(Math.min.apply(Math, SicboSetting_1.default.getChipLevelList()));
    };
    SicboController.prototype.onDisable = function () {
        SicboPortalAdapter_1.default.getInstance().disableWallet();
    };
    SicboController.prototype.onDestroy = function () {
        cc.systemEvent.off(EventKey.ON_ADD_BALANCE_EVENT, SicboController_1.Instance.onBalanceAdded);
        cc.systemEvent.off(EventKey.ON_UPDATE_BALANCE_EVENT, SicboController_1.Instance.onBalanceUpdated);
        SicboMessageHandler_1.default.getInstance().onDestroy();
        SicboPortalAdapter_1.default.destroy();
        SicboController_1.Instance.unscheduleAllCallbacks();
        delete SicboController_1.instance;
        SicboController_1.instance = null;
    };
    SicboController.prototype.onHideEvent = function () {
        //cc.log("SICBO onHideEvent");
        SicboUIComp_1.default.Instance.onHide();
        SicboController_1.Instance.appHiding = true;
        SicboController_1.Instance.appResuming = false;
    };
    SicboController.prototype.onShowEvent = function () {
        //cc.log("SICBO onShowEvent");
        SicboController_1.Instance.stateStatus = Status.STATUS_UNSPECIFIED;
        SicboController_1.Instance.appHiding = false;
        SicboController_1.Instance.appResuming = true;
        if (SicboController_1.Instance.isKicked)
            return;
        SicboPortalAdapter_1.default.getInstance().getLatestBalance();
        SicboMessageHandler_1.default.getInstance().onClearAllMsg();
        SicboMessageHandler_1.default.getInstance().sendAutoJoinRequest(function () {
            SicboController_1.Instance.onBalanceUpdated();
        });
    };
    //ANCHOR My User Wallet
    SicboController.prototype.addMyUserMoney = function (amount, transaction) {
        SicboPortalAdapter_1.default.getInstance().addMoney(amount, transaction);
    };
    //SECTION Send message
    SicboController.prototype.sendCheckServerAvailable = function () {
        //Work around for checking server available
        SicboMessageHandler_1.default.getInstance().sendHealthCheck(function (res) { });
    };
    SicboController.prototype.sendBet = function (door, coinType, onSuccess) {
        if (onSuccess === void 0) { onSuccess = null; }
        var self = this;
        var coinNumber = SicboHelper_1.default.convertCoinTypeToValue(coinType);
        self.checkWalletLocalForBetting(coinNumber, function (enoughMoney) {
            if (enoughMoney) {
                SicboMessageHandler_1.default.getInstance().sendBetRequest(door, coinNumber, function (res) {
                    self.addMyUserMoney(res.getTx().getAmount(), res.getTx().getLastTxId());
                    onSuccess && onSuccess();
                });
            }
            else {
                cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, Topic.SICBO, coinNumber);
                // self.onError(Code.SICBO_BET_NOT_ENOUGH_MONEY, "");
            }
        });
    };
    SicboController.prototype.sendReBet = function (onSuccess) {
        if (onSuccess === void 0) { onSuccess = null; }
        SicboMessageHandler_1.default.getInstance().sendRebetRequest(function (res) {
            SicboController_1.Instance.addMyUserMoney(res.getTx().getAmount(), res.getTx().getLastTxId());
            onSuccess && onSuccess(res.getDoorsAndBetAmountsList());
        });
    };
    SicboController.prototype.sendBetDouble = function (onSuccess) {
        if (onSuccess === void 0) { onSuccess = null; }
        SicboMessageHandler_1.default.getInstance().sendBetDoubleRequest(function (res) {
            SicboController_1.Instance.addMyUserMoney(res.getTx().getAmount(), res.getTx().getLastTxId());
            onSuccess && onSuccess(res.getDoorsAndBetAmountsList());
        });
    };
    SicboController.prototype.checkWalletLocalForBetting = function (amount, callback) {
        SicboPortalAdapter_1.default.getInstance().getCurrentBalance(function (balance) {
            callback && callback(amount <= balance);
        });
    };
    //!SECTION
    //SECTION Receive message from MessageHandler
    SicboController.prototype.onError = function (errCode, err) {
        var _a;
        //cc.log("SICBO ERROR", errCode, err);
        (_a = SicboUIComp_1.default.Instance) === null || _a === void 0 ? void 0 : _a.showErrMessage(errCode, err);
    };
    SicboController.prototype.onReceiveState = function (state) {
        SicboSetting_1.default.setStatusDurationConfig(state.getSessionStatesTime());
        SicboUIComp_1.default.Instance.updateUsersInRoom(state.getDisplayPlayersList(), state.getRemainingPlayersCount());
        var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
        state.getEventsList().forEach(function (e) {
            var payloadCase = e.getPayloadCase();
            switch (payloadCase) {
                case Event.PayloadCase.BET:
                    if (e.getUserId() == myUserId) {
                        //cc.error("SICBO Event.PayloadCase.BET broadcast self userid");
                    }
                    else
                        SicboUIComp_1.default.Instance.playUserBet(e.getUserId(), e.getBet().getDoor(), e.getBet().getChip());
                    break;
                //TODO: xu ly kick, join, ... .
                case Event.PayloadCase.KICKED:
                    if (e.hasKicked() && e.getUserId() == myUserId) {
                        SicboController_1.Instance.isKicked = true;
                    }
                    break;
                default:
                    break;
            }
        });
        if (SicboController_1.Instance.isKicked)
            return;
        if (SicboController_1.Instance.appHiding)
            return;
        if (SicboController_1.Instance.isDisconnected)
            return;
        // if(SicboController.Instance.appResuming){
        //   if(state.getStatus() == Status.PAYING){
        //     SicboMessageHandler.getInstance().sendLeaveRequest(() => {
        //       SicboController.Instance.leaveGame();
        //     });
        //   }
        // }
        if (SicboController_1.Instance.lastSession != state.getSessionId()) {
            SicboController_1.Instance.startNewSession(state.getSessionId());
            //cc.log("ZZZZZZ startNewSession", SicboController.Instance.stateStatus, SicboController.Instance.lastTickTime);
        }
        if ((SicboController_1.Instance.stateStatus != state.getStatus() && SicboController_1.Instance.isValidTick(state.getTickTime())) || SicboController_1.Instance.appResuming) {
            SicboController_1.Instance.exitState(SicboController_1.Instance.stateStatus);
            SicboController_1.Instance.startState(state, SicboSetting_1.default.getStatusDuration(state.getStatus()));
            SicboController_1.Instance.lastTickTime = state.getTickTime();
        }
        SicboController_1.Instance.stateStatus = state.getStatus();
        SicboController_1.Instance.updateState(state);
        SicboController_1.Instance.appResuming = false;
    };
    SicboController.prototype.exitState = function (status) {
        SicboUIComp_1.default.Instance.exitState(status);
    };
    SicboController.prototype.updateState = function (state) {
        SicboUIComp_1.default.Instance.updateState(state);
    };
    SicboController.prototype.startState = function (state, stateDuration) {
        if (stateDuration == 0) {
            //cc.log("SKIP STATE DURATION = 0", state.getStatus().toString());
            return;
        }
        SicboUIComp_1.default.Instance.startState(state, stateDuration);
    };
    SicboController.prototype.startNewSession = function (newSession) {
        SicboController_1.Instance.lastSession = newSession;
        SicboController_1.Instance.lastTickTime = -100;
        SicboTracking_1.default.startSession(newSession);
    };
    SicboController.prototype.isValidTick = function (curTick) {
        //cc.log("ZZZZZZ isValidTick", curTick, SicboController.Instance.lastTickTime)
        return true;
        // return curTick >= SicboController.Instance.lastTickTime;
    };
    //!SECTION
    //SECTION Sound
    SicboController.prototype.playBgm = function () {
        SicboSoundManager_1.default.getInstance().play(SicboSetting_1.default.getSoundName(SicboSetting_1.SicboSound.BGM));
    };
    SicboController.prototype.playSfx = function (sound, loop) {
        if (loop === void 0) { loop = false; }
        SicboSoundManager_1.default.getInstance().play(SicboSetting_1.default.getSoundName(sound));
    };
    SicboController.prototype.stopSfx = function (sound) {
        SicboSoundManager_1.default.getInstance().stop(SicboSetting_1.default.getSoundName(sound));
    };
    //!SECTION
    SicboController.prototype.leaveGame = function () {
        // SicboPortalAdapter.getInstance().leaveGame();
        cc.director.loadScene("LoginScene");
    };
    SicboController.prototype.getGameInfo = function () {
        // let games = SicboPortalAdapter.getInstance().getGameInfo() as any[];
        // let res: [] = [];
        // games.forEach((game) => {
        //   if (game.topic == Topic.SICBO) {
        //     res = game.info.filesList;
        //   }
        // });
        // return res;
    };
    SicboController.prototype.handleNotEnoughMoneyOnJointGame = function (coinNumber) {
        this.checkWalletLocalForBetting(coinNumber, function (enoughMoney) {
            if (!enoughMoney)
                cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_JOINING_GAME, Topic.SICBO, coinNumber);
        });
    };
    var SicboController_1;
    SicboController.instance = null;
    SicboController = SicboController_1 = __decorate([
        ccclass
    ], SicboController);
    return SicboController;
}(cc.Component));
exports.default = SicboController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvL1NjcmlwdHMvQ29udHJvbGxlcnMvU2ljYm9Db250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHFFQUFnRTtBQUUxRCxJQUFBLEtBV0YsNEJBQWtCLENBQUMsVUFBVSxFQUFFLEVBVmpDLElBQUksVUFBQSxFQUNKLEtBQUssV0FBQSxFQUNMLEtBQUssV0FBQSxFQUNMLEtBQUssV0FBQSxFQUNMLGdCQUFnQixzQkFBQSxFQUNoQixNQUFNLFlBQUEsRUFFTixjQUFjLG9CQUFBLEVBQ2QsUUFBUSxjQUFBLEVBQ1IsYUFBYSxtQkFDb0IsQ0FBQztBQUVwQyx5REFBb0Q7QUFDcEQsc0RBQWlEO0FBRWpELDhEQUF5RDtBQUN6RCx3REFBbUU7QUFDbkUsbUVBQThEO0FBQzlELDREQUF1RDtBQUV2RCwyRkFBc0Y7QUFDdEYsa0RBQTZDO0FBQzdDLDZEQUF3RDtBQUVoRCxJQUFBLE9BQU8sR0FBSyxFQUFFLENBQUMsVUFBVSxRQUFsQixDQUFtQjtBQUdsQztJQUE2QyxtQ0FBWTtJQUF6RDtRQUFBLHFFQW9TQztRQTlSUyxpQkFBVyxHQUFHLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQztRQUN4QyxjQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLDZCQUF1QixHQUFpQyxJQUFJLENBQUM7UUFDN0Qsa0JBQVksR0FBRyxDQUFDLEdBQUcsQ0FBQztRQUNwQixpQkFBVyxHQUFHLENBQUMsQ0FBQztRQXNGeEIsZUFBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixpQkFBVyxHQUFZLEtBQUssQ0FBQztRQUM3QixvQkFBYyxHQUFZLEtBQUssQ0FBQzs7SUFrTWxDLENBQUM7d0JBcFNvQixlQUFlO0lBRWxDLHNCQUFXLDJCQUFRO2FBQW5CO1lBQ0UsT0FBTyxpQkFBZSxDQUFDLFFBQVEsQ0FBQztRQUNsQyxDQUFDOzs7T0FBQTtJQU9ELGdDQUFNLEdBQU47UUFDRSxpQkFBZSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFFaEMsSUFBSSxDQUFDLGlCQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QjtZQUFFLGlCQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixHQUFHLGlCQUFlLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxzQ0FBNEIsQ0FBQyxDQUFDLENBQUMsb0JBQW9CO1FBQ25NLGlCQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsQ0FDMUQsY0FBTSxPQUFBLGlCQUFlLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxFQUF0QyxDQUFzQyxFQUM1QyxjQUFNLE9BQUEsaUJBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEVBQXRDLENBQXNDLENBQzdDLENBQUM7UUFFRixFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FDZixRQUFRLENBQUMsYUFBYSxFQUN0QixVQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUztZQUN2Qiw2QkFBbUIsQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7WUFDOUMsaUJBQWUsQ0FBQyxRQUFRLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMvQyxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN2QyxtRUFBbUU7UUFDckUsQ0FBQyxFQUNELElBQUksQ0FDTCxDQUFDO1FBQ0YsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQ2YsUUFBUSxDQUFDLG9CQUFvQixFQUM3QjtZQUNFLDZCQUFtQixDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztZQUMvQyxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1lBRWhELHdCQUFjLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLHdCQUFjLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUM1RSxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN6QyxDQUFDLEVBQ0QsSUFBSSxDQUNMLENBQUM7SUFDSixDQUFDO0lBRUQsK0JBQUssR0FBTDtRQUNFLDZCQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDLFlBQVksR0FBRyxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUM7UUFDekYsNkJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUMsYUFBYSxHQUFHLGlCQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQztRQUVuRixFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLEVBQUUsaUJBQWUsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDMUYsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLHVCQUF1QixFQUFFLGlCQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFFL0YsNEJBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUVwRCxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBRTVDLGlCQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBRW5DLCtEQUErRDtRQUMvRCx1REFBdUQ7UUFDdkQsS0FBSztJQUNQLENBQUM7SUFFRCwwQ0FBZ0IsR0FBaEI7UUFDRSw0QkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFDLE9BQU87WUFDekQscUJBQVcsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDcEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsd0NBQWMsR0FBZDtRQUNFLDRCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLGlCQUFpQixDQUFDLFVBQUMsT0FBTztZQUN6RCxxQkFBVyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNwRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxrQ0FBUSxHQUFSO1FBQ0UsNEJBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFaEQsSUFBSSxRQUFRLEdBQUcsNkJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFaEUsSUFBSSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxHQUFHLE9BQVIsSUFBSSxFQUFRLHNCQUFZLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxDQUFDO0lBQ3JGLENBQUM7SUFFRCxtQ0FBUyxHQUFUO1FBQ0UsNEJBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDbkQsQ0FBQztJQUVELG1DQUFTLEdBQVQ7UUFDRSxFQUFFLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLEVBQUUsaUJBQWUsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDM0YsRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLHVCQUF1QixFQUFFLGlCQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDaEcsNkJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDOUMsNEJBQWtCLENBQUMsT0FBTyxFQUFFLENBQUM7UUFFN0IsaUJBQWUsQ0FBQyxRQUFRLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUNsRCxPQUFPLGlCQUFlLENBQUMsUUFBUSxDQUFDO1FBQ2hDLGlCQUFlLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUNsQyxDQUFDO0lBTUQscUNBQVcsR0FBWDtRQUNFLDhCQUE4QjtRQUM5QixxQkFBVyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUM5QixpQkFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQzFDLGlCQUFlLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDL0MsQ0FBQztJQUVELHFDQUFXLEdBQVg7UUFDRSw4QkFBOEI7UUFDOUIsaUJBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQztRQUNqRSxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQzNDLGlCQUFlLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFFNUMsSUFBSSxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQUUsT0FBTztRQUU5Qyw0QkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBRXBELDZCQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ2xELDZCQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDLG1CQUFtQixDQUFDO1lBQ3BELGlCQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDOUMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsdUJBQXVCO0lBQ3ZCLHdDQUFjLEdBQWQsVUFBZSxNQUFjLEVBQUUsV0FBbUI7UUFDaEQsNEJBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxXQUFXLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRUQsc0JBQXNCO0lBQ3RCLGtEQUF3QixHQUF4QjtRQUNFLDJDQUEyQztRQUMzQyw2QkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxlQUFlLENBQUMsVUFBQyxHQUFHLElBQU0sQ0FBQyxDQUFDLENBQUM7SUFDakUsQ0FBQztJQUVELGlDQUFPLEdBQVAsVUFBUSxJQUFJLEVBQUUsUUFBdUIsRUFBRSxTQUE0QjtRQUE1QiwwQkFBQSxFQUFBLGdCQUE0QjtRQUNqRSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxVQUFVLEdBQUcscUJBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsMEJBQTBCLENBQUMsVUFBVSxFQUFFLFVBQUMsV0FBVztZQUN0RCxJQUFJLFdBQVcsRUFBRTtnQkFDZiw2QkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxVQUFDLEdBQUc7b0JBQ3JFLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO29CQUN4RSxTQUFTLElBQUksU0FBUyxFQUFFLENBQUM7Z0JBQzNCLENBQUMsQ0FBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLHVDQUF1QyxFQUFFLEtBQUssQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQy9GLHFEQUFxRDthQUN0RDtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELG1DQUFTLEdBQVQsVUFBVSxTQUF5RTtRQUF6RSwwQkFBQSxFQUFBLGdCQUF5RTtRQUNqRiw2QkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFDLEdBQUc7WUFDckQsaUJBQWUsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUM1RixTQUFTLElBQUksU0FBUyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSxDQUFDLENBQUM7UUFDMUQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsdUNBQWEsR0FBYixVQUFjLFNBQXlFO1FBQXpFLDBCQUFBLEVBQUEsZ0JBQXlFO1FBQ3JGLDZCQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDLG9CQUFvQixDQUFDLFVBQUMsR0FBRztZQUN6RCxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQzVGLFNBQVMsSUFBSSxTQUFTLENBQUMsR0FBRyxDQUFDLHlCQUF5QixFQUFFLENBQUMsQ0FBQztRQUMxRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxvREFBMEIsR0FBMUIsVUFBMkIsTUFBYyxFQUFFLFFBQXdDO1FBQ2pGLDRCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLGlCQUFpQixDQUFDLFVBQUMsT0FBTztZQUN6RCxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxPQUFPLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxVQUFVO0lBRVYsNkNBQTZDO0lBQzdDLGlDQUFPLEdBQVAsVUFBUSxPQUFPLEVBQUUsR0FBVzs7UUFDMUIsc0NBQXNDO1FBQ3RDLE1BQUEscUJBQVcsQ0FBQyxRQUFRLDBDQUFFLGNBQWMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFO0lBQ3JELENBQUM7SUFFRCx3Q0FBYyxHQUFkLFVBQWUsS0FBaUM7UUFDOUMsc0JBQVksQ0FBQyx1QkFBdUIsQ0FBQyxLQUFLLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDO1FBQ25FLHFCQUFXLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxxQkFBcUIsRUFBRSxFQUFFLEtBQUssQ0FBQyx3QkFBd0IsRUFBRSxDQUFDLENBQUM7UUFFeEcsSUFBSSxRQUFRLEdBQUcsNEJBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDOUQsS0FBSyxDQUFDLGFBQWEsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFDLENBQUM7WUFDOUIsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3JDLFFBQVEsV0FBVyxFQUFFO2dCQUNuQixLQUFLLEtBQUssQ0FBQyxXQUFXLENBQUMsR0FBRztvQkFDeEIsSUFBSSxDQUFDLENBQUMsU0FBUyxFQUFFLElBQUksUUFBUSxFQUFFO3dCQUM3QixnRUFBZ0U7cUJBQ2pFOzt3QkFBTSxxQkFBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztvQkFDbkcsTUFBTTtnQkFFUiwrQkFBK0I7Z0JBQy9CLEtBQUssS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNO29CQUMzQixJQUFJLENBQUMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUyxFQUFFLElBQUksUUFBUSxFQUFFO3dCQUM5QyxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO3FCQUMxQztvQkFDRCxNQUFNO2dCQUNSO29CQUNFLE1BQU07YUFDVDtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQUUsT0FBTztRQUM5QyxJQUFJLGlCQUFlLENBQUMsUUFBUSxDQUFDLFNBQVM7WUFBRSxPQUFPO1FBQy9DLElBQUksaUJBQWUsQ0FBQyxRQUFRLENBQUMsY0FBYztZQUFFLE9BQU87UUFDcEQsNENBQTRDO1FBQzVDLDRDQUE0QztRQUM1QyxpRUFBaUU7UUFDakUsOENBQThDO1FBQzlDLFVBQVU7UUFDVixNQUFNO1FBQ04sSUFBSTtRQUVKLElBQUksaUJBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxJQUFJLEtBQUssQ0FBQyxZQUFZLEVBQUUsRUFBRTtZQUNoRSxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7WUFDL0QsZ0hBQWdIO1NBQ2pIO1FBRUQsSUFBSSxDQUFDLGlCQUFlLENBQUMsUUFBUSxDQUFDLFdBQVcsSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFLElBQUksaUJBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLElBQUksaUJBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFO1lBQ3BLLGlCQUFlLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN6RSxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLHNCQUFZLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM5RixpQkFBZSxDQUFDLFFBQVEsQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzdEO1FBQ0QsaUJBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUN6RCxpQkFBZSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFNUMsaUJBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztJQUMvQyxDQUFDO0lBRUQsbUNBQVMsR0FBVCxVQUFVLE1BQU07UUFDZCxxQkFBVyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELHFDQUFXLEdBQVgsVUFBWSxLQUFpQztRQUMzQyxxQkFBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELG9DQUFVLEdBQVYsVUFBVyxLQUFpQyxFQUFFLGFBQXFCO1FBQ2pFLElBQUksYUFBYSxJQUFJLENBQUMsRUFBRTtZQUN0QixrRUFBa0U7WUFDbEUsT0FBTztTQUNSO1FBQ0QscUJBQVcsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRUQseUNBQWUsR0FBZixVQUFnQixVQUFVO1FBQ3hCLGlCQUFlLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUM7UUFDbEQsaUJBQWUsQ0FBQyxRQUFRLENBQUMsWUFBWSxHQUFHLENBQUMsR0FBRyxDQUFDO1FBQzdDLHVCQUFhLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCxxQ0FBVyxHQUFYLFVBQVksT0FBTztRQUNqQiw4RUFBOEU7UUFDOUUsT0FBTyxJQUFJLENBQUM7UUFDWiwyREFBMkQ7SUFDN0QsQ0FBQztJQUNELFVBQVU7SUFFVixlQUFlO0lBQ2YsaUNBQU8sR0FBUDtRQUNFLDJCQUFpQixDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxzQkFBWSxDQUFDLFlBQVksQ0FBQyx5QkFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDbEYsQ0FBQztJQUVELGlDQUFPLEdBQVAsVUFBUSxLQUFpQixFQUFFLElBQXFCO1FBQXJCLHFCQUFBLEVBQUEsWUFBcUI7UUFDOUMsMkJBQWlCLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLHNCQUFZLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUVELGlDQUFPLEdBQVAsVUFBUSxLQUFpQjtRQUN2QiwyQkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsc0JBQVksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBQ0QsVUFBVTtJQUNWLG1DQUFTLEdBQVQ7UUFDRSxnREFBZ0Q7UUFDaEQsRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELHFDQUFXLEdBQVg7UUFDRSx1RUFBdUU7UUFDdkUsb0JBQW9CO1FBQ3BCLDRCQUE0QjtRQUM1QixxQ0FBcUM7UUFDckMsaUNBQWlDO1FBQ2pDLE1BQU07UUFDTixNQUFNO1FBQ04sY0FBYztJQUNoQixDQUFDO0lBRUQseURBQStCLEdBQS9CLFVBQWdDLFVBQWtCO1FBQ2hELElBQUksQ0FBQywwQkFBMEIsQ0FBQyxVQUFVLEVBQUUsVUFBQyxXQUFXO1lBQ3RELElBQUksQ0FBQyxXQUFXO2dCQUFFLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyx1Q0FBdUMsRUFBRSxLQUFLLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ25ILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7SUFsU2Msd0JBQVEsR0FBb0IsSUFBSSxDQUFDO0lBRDdCLGVBQWU7UUFEbkMsT0FBTztPQUNhLGVBQWUsQ0FvU25DO0lBQUQsc0JBQUM7Q0FwU0QsQUFvU0MsQ0FwUzRDLEVBQUUsQ0FBQyxTQUFTLEdBb1N4RDtrQkFwU29CLGVBQWUiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU2ljYm9Nb2R1bGVBZGFwdGVyIGZyb20gXCIuLi8uLi8uLi8uLi9TaWNib01vZHVsZUFkYXB0ZXJcIjtcblxuY29uc3Qge1xuICBDb2RlLFxuICBUb3BpYyxcbiAgU3RhdGUsXG4gIEV2ZW50LFxuICBEb29yQW5kQmV0QW1vdW50LFxuICBTdGF0dXMsXG5cbiAgV2hhdEFwcE1lc3NhZ2UsXG4gIEV2ZW50S2V5LFxuICBBdXRvSm9pblJlcGx5LFxufSA9IFNpY2JvTW9kdWxlQWRhcHRlci5nZXRBbGxSZWZzKCk7XG5cbmltcG9ydCBTaWNib1VJQ29tcCBmcm9tIFwiLi4vQ29tcG9uZW50cy9TaWNib1VJQ29tcFwiO1xuaW1wb3J0IFNpY2JvSGVscGVyIGZyb20gXCIuLi9IZWxwZXJzL1NpY2JvSGVscGVyXCI7XG5cbmltcG9ydCBTaWNib01lc3NhZ2VIYW5kbGVyIGZyb20gXCIuLi9TaWNib01lc3NhZ2VIYW5kbGVyXCI7XG5pbXBvcnQgU2ljYm9TZXR0aW5nLCB7IFNpY2JvU291bmQgfSBmcm9tIFwiLi4vU2V0dGluZy9TaWNib1NldHRpbmdcIjtcbmltcG9ydCBTaWNib1NvdW5kTWFuYWdlciBmcm9tIFwiLi4vTWFuYWdlcnMvU2ljYm9Tb3VuZE1hbmFnZXJcIjtcbmltcG9ydCBTaWNib1BvcnRhbEFkYXB0ZXIgZnJvbSBcIi4uL1NpY2JvUG9ydGFsQWRhcHRlclwiO1xuaW1wb3J0IHsgU2ljYm9Db2luVHlwZSB9IGZyb20gXCIuLi9STkdDb21tb25zL1NpY2JvQ29pblR5cGVcIjtcbmltcG9ydCBTaWNib0hhbmRsZUV2ZW50U2hvd0hpZGVDb21wIGZyb20gXCIuLi9STkdDb21tb25zL1NpY2JvSGFuZGxlRXZlbnRTaG93SGlkZUNvbXBcIjtcbmltcG9ydCBTaWNib1RyYWNraW5nIGZyb20gXCIuLi9TaWNib1RyYWNraW5nXCI7XG5pbXBvcnQgU2ljYm9VSU1hbmFnZXIgZnJvbSBcIi4uL01hbmFnZXJzL1NpY2JvVUlNYW5hZ2VyXCI7XG5cbmNvbnN0IHsgY2NjbGFzcyB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvQ29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBTaWNib0NvbnRyb2xsZXIgPSBudWxsO1xuICBzdGF0aWMgZ2V0IEluc3RhbmNlKCk6IFNpY2JvQ29udHJvbGxlciB7XG4gICAgcmV0dXJuIFNpY2JvQ29udHJvbGxlci5pbnN0YW5jZTtcbiAgfVxuXG4gIHByaXZhdGUgc3RhdGVTdGF0dXMgPSBTdGF0dXMuU1RBVFVTX1VOU1BFQ0lGSUVEO1xuICBwcml2YXRlIGlzS2lja2VkOiBib29sZWFuID0gZmFsc2U7XG4gIHByaXZhdGUgaGFuZGxlRXZlbnRTaG93SGlkZUNvbXA6IFNpY2JvSGFuZGxlRXZlbnRTaG93SGlkZUNvbXAgPSBudWxsO1xuICBwcml2YXRlIGxhc3RUaWNrVGltZSA9IC0xMDA7XG4gIHByaXZhdGUgbGFzdFNlc3Npb24gPSAwO1xuICBvbkxvYWQoKSB7XG4gICAgU2ljYm9Db250cm9sbGVyLmluc3RhbmNlID0gdGhpcztcblxuICAgIGlmICghU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLmhhbmRsZUV2ZW50U2hvd0hpZGVDb21wKSBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuaGFuZGxlRXZlbnRTaG93SGlkZUNvbXAgPSBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuYWRkQ29tcG9uZW50KFNpY2JvSGFuZGxlRXZlbnRTaG93SGlkZUNvbXApOyAvL0RldmljZSBob21lIGJ1dHRvblxuICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5oYW5kbGVFdmVudFNob3dIaWRlQ29tcC5saXN0ZW5FdmVudChcbiAgICAgICgpID0+IFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5vbkhpZGVFdmVudCgpLFxuICAgICAgKCkgPT4gU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLm9uU2hvd0V2ZW50KClcbiAgICApO1xuXG4gICAgY2Muc3lzdGVtRXZlbnQub24oXG4gICAgICBFdmVudEtleS5PTl9QSU5HX0VSUk9SLFxuICAgICAgKGNvZGUsIGlzUmV0cnksIG1lc3NFcnJvcikgPT4ge1xuICAgICAgICBTaWNib01lc3NhZ2VIYW5kbGVyLklTX1dTX0RJU0NPTk5FQ1RFRCA9IHRydWU7XG4gICAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5pc0Rpc2Nvbm5lY3RlZCA9IHRydWU7XG4gICAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5vbkhpZGVFdmVudCgpO1xuICAgICAgICAvL2NjLndhcm4oXCJFdmVudEtleS5PTl9ESVNDT05ORUNUX0VWRU5UXCIsIGNvZGUsIGlzUmV0cnksIG1lc3NFcnJvcilcbiAgICAgIH0sXG4gICAgICB0aGlzXG4gICAgKTtcbiAgICBjYy5zeXN0ZW1FdmVudC5vbihcbiAgICAgIEV2ZW50S2V5Lk9OX1JFQ09OTkVDVEVEX0VWRU5ULFxuICAgICAgKCkgPT4ge1xuICAgICAgICBTaWNib01lc3NhZ2VIYW5kbGVyLklTX1dTX0RJU0NPTk5FQ1RFRCA9IGZhbHNlO1xuICAgICAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuaXNEaXNjb25uZWN0ZWQgPSBmYWxzZTtcblxuICAgICAgICBTaWNib1VJTWFuYWdlci5nZXRJbnN0YW5jZSgpLmhpZGVQb3B1cChTaWNib1VJTWFuYWdlci5TaWNib0Nhbm5vdEpvaW5Qb3B1cCk7XG4gICAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5vblNob3dFdmVudCgpO1xuICAgICAgfSxcbiAgICAgIHRoaXNcbiAgICApO1xuICB9XG5cbiAgc3RhcnQoKSB7XG4gICAgU2ljYm9NZXNzYWdlSGFuZGxlci5nZXRJbnN0YW5jZSgpLnJlY2VpdmVTdGF0ZSA9IFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5vblJlY2VpdmVTdGF0ZTtcbiAgICBTaWNib01lc3NhZ2VIYW5kbGVyLmdldEluc3RhbmNlKCkub25FcnJvckN1c3RvbSA9IFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5vbkVycm9yO1xuXG4gICAgY2Muc3lzdGVtRXZlbnQub24oRXZlbnRLZXkuT05fQUREX0JBTEFOQ0VfRVZFTlQsIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5vbkJhbGFuY2VBZGRlZCk7XG4gICAgY2Muc3lzdGVtRXZlbnQub24oRXZlbnRLZXkuT05fVVBEQVRFX0JBTEFOQ0VfRVZFTlQsIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5vbkJhbGFuY2VVcGRhdGVkKTtcblxuICAgIFNpY2JvUG9ydGFsQWRhcHRlci5nZXRJbnN0YW5jZSgpLmdldExhdGVzdEJhbGFuY2UoKTtcblxuICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5vbkJhbGFuY2VVcGRhdGVkKCk7XG5cbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UucGxheUJnbSgpO1xuXG4gICAgLy8gU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuZ2V0QnVuZGxlSW5mbyhidW5kbGVJbmZvPT57XG4gICAgLy8gICBTaWNib1RyYWNraW5nLnNldEdhbWVWZXJzaW9uKGJ1bmRsZUluZm8/LnZlcnNpb24pO1xuICAgIC8vIH0pXG4gIH1cblxuICBvbkJhbGFuY2VVcGRhdGVkKCkge1xuICAgIFNpY2JvUG9ydGFsQWRhcHRlci5nZXRJbnN0YW5jZSgpLmdldEN1cnJlbnRCYWxhbmNlKChiYWxhbmNlKSA9PiB7XG4gICAgICBTaWNib1VJQ29tcC5JbnN0YW5jZS5jaGFuZ2VNeVVzZXJCYWxhbmNlKGJhbGFuY2UpO1xuICAgIH0pO1xuICB9XG5cbiAgb25CYWxhbmNlQWRkZWQoKSB7XG4gICAgU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuZ2V0Q3VycmVudEJhbGFuY2UoKGJhbGFuY2UpID0+IHtcbiAgICAgIFNpY2JvVUlDb21wLkluc3RhbmNlLmNoYW5nZU15VXNlckJhbGFuY2UoYmFsYW5jZSk7XG4gICAgfSk7XG4gIH1cblxuICBvbkVuYWJsZSgpIHtcbiAgICBTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0SW5zdGFuY2UoKS5lbmFibGVXYWxsZXQoKTtcblxuICAgIGxldCBjaGFuZWxJRCA9IFNpY2JvTWVzc2FnZUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5nZXRDaGFubmVsSWQoKTtcblxuICAgIHRoaXMuaGFuZGxlTm90RW5vdWdoTW9uZXlPbkpvaW50R2FtZShNYXRoLm1pbiguLi5TaWNib1NldHRpbmcuZ2V0Q2hpcExldmVsTGlzdCgpKSk7XG4gIH1cblxuICBvbkRpc2FibGUoKSB7XG4gICAgU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuZGlzYWJsZVdhbGxldCgpO1xuICB9XG5cbiAgb25EZXN0cm95KCkge1xuICAgIGNjLnN5c3RlbUV2ZW50Lm9mZihFdmVudEtleS5PTl9BRERfQkFMQU5DRV9FVkVOVCwgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLm9uQmFsYW5jZUFkZGVkKTtcbiAgICBjYy5zeXN0ZW1FdmVudC5vZmYoRXZlbnRLZXkuT05fVVBEQVRFX0JBTEFOQ0VfRVZFTlQsIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5vbkJhbGFuY2VVcGRhdGVkKTtcbiAgICBTaWNib01lc3NhZ2VIYW5kbGVyLmdldEluc3RhbmNlKCkub25EZXN0cm95KCk7XG4gICAgU2ljYm9Qb3J0YWxBZGFwdGVyLmRlc3Ryb3koKTtcblxuICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS51bnNjaGVkdWxlQWxsQ2FsbGJhY2tzKCk7XG4gICAgZGVsZXRlIFNpY2JvQ29udHJvbGxlci5pbnN0YW5jZTtcbiAgICBTaWNib0NvbnRyb2xsZXIuaW5zdGFuY2UgPSBudWxsO1xuICB9XG5cbiAgYXBwSGlkaW5nOiBib29sZWFuID0gZmFsc2U7XG4gIGFwcFJlc3VtaW5nOiBib29sZWFuID0gZmFsc2U7XG4gIGlzRGlzY29ubmVjdGVkOiBib29sZWFuID0gZmFsc2U7XG5cbiAgb25IaWRlRXZlbnQoKSB7XG4gICAgLy9jYy5sb2coXCJTSUNCTyBvbkhpZGVFdmVudFwiKTtcbiAgICBTaWNib1VJQ29tcC5JbnN0YW5jZS5vbkhpZGUoKTtcbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuYXBwSGlkaW5nID0gdHJ1ZTtcbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuYXBwUmVzdW1pbmcgPSBmYWxzZTtcbiAgfVxuXG4gIG9uU2hvd0V2ZW50KCkge1xuICAgIC8vY2MubG9nKFwiU0lDQk8gb25TaG93RXZlbnRcIik7XG4gICAgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLnN0YXRlU3RhdHVzID0gU3RhdHVzLlNUQVRVU19VTlNQRUNJRklFRDtcbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuYXBwSGlkaW5nID0gZmFsc2U7XG4gICAgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLmFwcFJlc3VtaW5nID0gdHJ1ZTtcblxuICAgIGlmIChTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuaXNLaWNrZWQpIHJldHVybjtcblxuICAgIFNpY2JvUG9ydGFsQWRhcHRlci5nZXRJbnN0YW5jZSgpLmdldExhdGVzdEJhbGFuY2UoKTtcblxuICAgIFNpY2JvTWVzc2FnZUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5vbkNsZWFyQWxsTXNnKCk7XG4gICAgU2ljYm9NZXNzYWdlSGFuZGxlci5nZXRJbnN0YW5jZSgpLnNlbmRBdXRvSm9pblJlcXVlc3QoKCkgPT4ge1xuICAgICAgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLm9uQmFsYW5jZVVwZGF0ZWQoKTtcbiAgICB9KTtcbiAgfVxuXG4gIC8vQU5DSE9SIE15IFVzZXIgV2FsbGV0XG4gIGFkZE15VXNlck1vbmV5KGFtb3VudDogbnVtYmVyLCB0cmFuc2FjdGlvbjogbnVtYmVyKSB7XG4gICAgU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuYWRkTW9uZXkoYW1vdW50LCB0cmFuc2FjdGlvbik7XG4gIH1cblxuICAvL1NFQ1RJT04gU2VuZCBtZXNzYWdlXG4gIHNlbmRDaGVja1NlcnZlckF2YWlsYWJsZSgpIHtcbiAgICAvL1dvcmsgYXJvdW5kIGZvciBjaGVja2luZyBzZXJ2ZXIgYXZhaWxhYmxlXG4gICAgU2ljYm9NZXNzYWdlSGFuZGxlci5nZXRJbnN0YW5jZSgpLnNlbmRIZWFsdGhDaGVjaygocmVzKSA9PiB7fSk7XG4gIH1cblxuICBzZW5kQmV0KGRvb3IsIGNvaW5UeXBlOiBTaWNib0NvaW5UeXBlLCBvblN1Y2Nlc3M6ICgpID0+IHZvaWQgPSBudWxsKSB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIGxldCBjb2luTnVtYmVyID0gU2ljYm9IZWxwZXIuY29udmVydENvaW5UeXBlVG9WYWx1ZShjb2luVHlwZSk7XG4gICAgc2VsZi5jaGVja1dhbGxldExvY2FsRm9yQmV0dGluZyhjb2luTnVtYmVyLCAoZW5vdWdoTW9uZXkpID0+IHtcbiAgICAgIGlmIChlbm91Z2hNb25leSkge1xuICAgICAgICBTaWNib01lc3NhZ2VIYW5kbGVyLmdldEluc3RhbmNlKCkuc2VuZEJldFJlcXVlc3QoZG9vciwgY29pbk51bWJlciwgKHJlcykgPT4ge1xuICAgICAgICAgIHNlbGYuYWRkTXlVc2VyTW9uZXkocmVzLmdldFR4KCkuZ2V0QW1vdW50KCksIHJlcy5nZXRUeCgpLmdldExhc3RUeElkKCkpO1xuICAgICAgICAgIG9uU3VjY2VzcyAmJiBvblN1Y2Nlc3MoKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjYy5zeXN0ZW1FdmVudC5lbWl0KEV2ZW50S2V5LkhBTkRMRV9PVVRfT0ZfQkFMQU5DRV9XSEVOX0JFVFRJTkdfR0FNRSwgVG9waWMuU0lDQk8sIGNvaW5OdW1iZXIpO1xuICAgICAgICAvLyBzZWxmLm9uRXJyb3IoQ29kZS5TSUNCT19CRVRfTk9UX0VOT1VHSF9NT05FWSwgXCJcIik7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBzZW5kUmVCZXQob25TdWNjZXNzOiAoZGFiczogSW5zdGFuY2VUeXBlPHR5cGVvZiBEb29yQW5kQmV0QW1vdW50PltdKSA9PiB2b2lkID0gbnVsbCkge1xuICAgIFNpY2JvTWVzc2FnZUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5zZW5kUmViZXRSZXF1ZXN0KChyZXMpID0+IHtcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5hZGRNeVVzZXJNb25leShyZXMuZ2V0VHgoKS5nZXRBbW91bnQoKSwgcmVzLmdldFR4KCkuZ2V0TGFzdFR4SWQoKSk7XG4gICAgICBvblN1Y2Nlc3MgJiYgb25TdWNjZXNzKHJlcy5nZXREb29yc0FuZEJldEFtb3VudHNMaXN0KCkpO1xuICAgIH0pO1xuICB9XG5cbiAgc2VuZEJldERvdWJsZShvblN1Y2Nlc3M6IChkYWJzOiBJbnN0YW5jZVR5cGU8dHlwZW9mIERvb3JBbmRCZXRBbW91bnQ+W10pID0+IHZvaWQgPSBudWxsKSB7XG4gICAgU2ljYm9NZXNzYWdlSGFuZGxlci5nZXRJbnN0YW5jZSgpLnNlbmRCZXREb3VibGVSZXF1ZXN0KChyZXMpID0+IHtcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5hZGRNeVVzZXJNb25leShyZXMuZ2V0VHgoKS5nZXRBbW91bnQoKSwgcmVzLmdldFR4KCkuZ2V0TGFzdFR4SWQoKSk7XG4gICAgICBvblN1Y2Nlc3MgJiYgb25TdWNjZXNzKHJlcy5nZXREb29yc0FuZEJldEFtb3VudHNMaXN0KCkpO1xuICAgIH0pO1xuICB9XG5cbiAgY2hlY2tXYWxsZXRMb2NhbEZvckJldHRpbmcoYW1vdW50OiBudW1iZXIsIGNhbGxiYWNrOiAoZW5vdWdoTW9uZXk6IGJvb2xlYW4pID0+IHZvaWQpIHtcbiAgICBTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0SW5zdGFuY2UoKS5nZXRDdXJyZW50QmFsYW5jZSgoYmFsYW5jZSkgPT4ge1xuICAgICAgY2FsbGJhY2sgJiYgY2FsbGJhY2soYW1vdW50IDw9IGJhbGFuY2UpO1xuICAgIH0pO1xuICB9XG4gIC8vIVNFQ1RJT05cblxuICAvL1NFQ1RJT04gUmVjZWl2ZSBtZXNzYWdlIGZyb20gTWVzc2FnZUhhbmRsZXJcbiAgb25FcnJvcihlcnJDb2RlLCBlcnI6IHN0cmluZykge1xuICAgIC8vY2MubG9nKFwiU0lDQk8gRVJST1JcIiwgZXJyQ29kZSwgZXJyKTtcbiAgICBTaWNib1VJQ29tcC5JbnN0YW5jZT8uc2hvd0Vyck1lc3NhZ2UoZXJyQ29kZSwgZXJyKTtcbiAgfVxuXG4gIG9uUmVjZWl2ZVN0YXRlKHN0YXRlOiBJbnN0YW5jZVR5cGU8dHlwZW9mIFN0YXRlPikge1xuICAgIFNpY2JvU2V0dGluZy5zZXRTdGF0dXNEdXJhdGlvbkNvbmZpZyhzdGF0ZS5nZXRTZXNzaW9uU3RhdGVzVGltZSgpKTtcbiAgICBTaWNib1VJQ29tcC5JbnN0YW5jZS51cGRhdGVVc2Vyc0luUm9vbShzdGF0ZS5nZXREaXNwbGF5UGxheWVyc0xpc3QoKSwgc3RhdGUuZ2V0UmVtYWluaW5nUGxheWVyc0NvdW50KCkpO1xuXG4gICAgbGV0IG15VXNlcklkID0gU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuZ2V0TXlVc2VySWQoKTtcbiAgICBzdGF0ZS5nZXRFdmVudHNMaXN0KCkuZm9yRWFjaCgoZSkgPT4ge1xuICAgICAgbGV0IHBheWxvYWRDYXNlID0gZS5nZXRQYXlsb2FkQ2FzZSgpO1xuICAgICAgc3dpdGNoIChwYXlsb2FkQ2FzZSkge1xuICAgICAgICBjYXNlIEV2ZW50LlBheWxvYWRDYXNlLkJFVDpcbiAgICAgICAgICBpZiAoZS5nZXRVc2VySWQoKSA9PSBteVVzZXJJZCkge1xuICAgICAgICAgICAgLy9jYy5lcnJvcihcIlNJQ0JPIEV2ZW50LlBheWxvYWRDYXNlLkJFVCBicm9hZGNhc3Qgc2VsZiB1c2VyaWRcIik7XG4gICAgICAgICAgfSBlbHNlIFNpY2JvVUlDb21wLkluc3RhbmNlLnBsYXlVc2VyQmV0KGUuZ2V0VXNlcklkKCksIGUuZ2V0QmV0KCkuZ2V0RG9vcigpLCBlLmdldEJldCgpLmdldENoaXAoKSk7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgLy9UT0RPOiB4dSBseSBraWNrLCBqb2luLCAuLi4gLlxuICAgICAgICBjYXNlIEV2ZW50LlBheWxvYWRDYXNlLktJQ0tFRDpcbiAgICAgICAgICBpZiAoZS5oYXNLaWNrZWQoKSAmJiBlLmdldFVzZXJJZCgpID09IG15VXNlcklkKSB7XG4gICAgICAgICAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuaXNLaWNrZWQgPSB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIGlmIChTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuaXNLaWNrZWQpIHJldHVybjtcbiAgICBpZiAoU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLmFwcEhpZGluZykgcmV0dXJuO1xuICAgIGlmIChTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuaXNEaXNjb25uZWN0ZWQpIHJldHVybjtcbiAgICAvLyBpZihTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuYXBwUmVzdW1pbmcpe1xuICAgIC8vICAgaWYoc3RhdGUuZ2V0U3RhdHVzKCkgPT0gU3RhdHVzLlBBWUlORyl7XG4gICAgLy8gICAgIFNpY2JvTWVzc2FnZUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5zZW5kTGVhdmVSZXF1ZXN0KCgpID0+IHtcbiAgICAvLyAgICAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UubGVhdmVHYW1lKCk7XG4gICAgLy8gICAgIH0pO1xuICAgIC8vICAgfVxuICAgIC8vIH1cblxuICAgIGlmIChTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UubGFzdFNlc3Npb24gIT0gc3RhdGUuZ2V0U2Vzc2lvbklkKCkpIHtcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5zdGFydE5ld1Nlc3Npb24oc3RhdGUuZ2V0U2Vzc2lvbklkKCkpO1xuICAgICAgLy9jYy5sb2coXCJaWlpaWlogc3RhcnROZXdTZXNzaW9uXCIsIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5zdGF0ZVN0YXR1cywgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLmxhc3RUaWNrVGltZSk7XG4gICAgfVxuXG4gICAgaWYgKChTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2Uuc3RhdGVTdGF0dXMgIT0gc3RhdGUuZ2V0U3RhdHVzKCkgJiYgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLmlzVmFsaWRUaWNrKHN0YXRlLmdldFRpY2tUaW1lKCkpKSB8fCBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UuYXBwUmVzdW1pbmcpIHtcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5leGl0U3RhdGUoU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLnN0YXRlU3RhdHVzKTtcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5zdGFydFN0YXRlKHN0YXRlLCBTaWNib1NldHRpbmcuZ2V0U3RhdHVzRHVyYXRpb24oc3RhdGUuZ2V0U3RhdHVzKCkpKTtcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5sYXN0VGlja1RpbWUgPSBzdGF0ZS5nZXRUaWNrVGltZSgpO1xuICAgIH1cbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2Uuc3RhdGVTdGF0dXMgPSBzdGF0ZS5nZXRTdGF0dXMoKTtcbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UudXBkYXRlU3RhdGUoc3RhdGUpO1xuXG4gICAgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLmFwcFJlc3VtaW5nID0gZmFsc2U7XG4gIH1cblxuICBleGl0U3RhdGUoc3RhdHVzKSB7XG4gICAgU2ljYm9VSUNvbXAuSW5zdGFuY2UuZXhpdFN0YXRlKHN0YXR1cyk7XG4gIH1cblxuICB1cGRhdGVTdGF0ZShzdGF0ZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGF0ZT4pIHtcbiAgICBTaWNib1VJQ29tcC5JbnN0YW5jZS51cGRhdGVTdGF0ZShzdGF0ZSk7XG4gIH1cblxuICBzdGFydFN0YXRlKHN0YXRlOiBJbnN0YW5jZVR5cGU8dHlwZW9mIFN0YXRlPiwgc3RhdGVEdXJhdGlvbjogbnVtYmVyKSB7XG4gICAgaWYgKHN0YXRlRHVyYXRpb24gPT0gMCkge1xuICAgICAgLy9jYy5sb2coXCJTS0lQIFNUQVRFIERVUkFUSU9OID0gMFwiLCBzdGF0ZS5nZXRTdGF0dXMoKS50b1N0cmluZygpKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgU2ljYm9VSUNvbXAuSW5zdGFuY2Uuc3RhcnRTdGF0ZShzdGF0ZSwgc3RhdGVEdXJhdGlvbik7XG4gIH1cblxuICBzdGFydE5ld1Nlc3Npb24obmV3U2Vzc2lvbikge1xuICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5sYXN0U2Vzc2lvbiA9IG5ld1Nlc3Npb247XG4gICAgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLmxhc3RUaWNrVGltZSA9IC0xMDA7XG4gICAgU2ljYm9UcmFja2luZy5zdGFydFNlc3Npb24obmV3U2Vzc2lvbik7XG4gIH1cblxuICBpc1ZhbGlkVGljayhjdXJUaWNrKTogYm9vbGVhbiB7XG4gICAgLy9jYy5sb2coXCJaWlpaWlogaXNWYWxpZFRpY2tcIiwgY3VyVGljaywgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLmxhc3RUaWNrVGltZSlcbiAgICByZXR1cm4gdHJ1ZTtcbiAgICAvLyByZXR1cm4gY3VyVGljayA+PSBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UubGFzdFRpY2tUaW1lO1xuICB9XG4gIC8vIVNFQ1RJT05cblxuICAvL1NFQ1RJT04gU291bmRcbiAgcGxheUJnbSgpIHtcbiAgICBTaWNib1NvdW5kTWFuYWdlci5nZXRJbnN0YW5jZSgpLnBsYXkoU2ljYm9TZXR0aW5nLmdldFNvdW5kTmFtZShTaWNib1NvdW5kLkJHTSkpO1xuICB9XG5cbiAgcGxheVNmeChzb3VuZDogU2ljYm9Tb3VuZCwgbG9vcDogYm9vbGVhbiA9IGZhbHNlKSB7XG4gICAgU2ljYm9Tb3VuZE1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5wbGF5KFNpY2JvU2V0dGluZy5nZXRTb3VuZE5hbWUoc291bmQpKTtcbiAgfVxuXG4gIHN0b3BTZngoc291bmQ6IFNpY2JvU291bmQpIHtcbiAgICBTaWNib1NvdW5kTWFuYWdlci5nZXRJbnN0YW5jZSgpLnN0b3AoU2ljYm9TZXR0aW5nLmdldFNvdW5kTmFtZShzb3VuZCkpO1xuICB9XG4gIC8vIVNFQ1RJT05cbiAgbGVhdmVHYW1lKCkge1xuICAgIC8vIFNpY2JvUG9ydGFsQWRhcHRlci5nZXRJbnN0YW5jZSgpLmxlYXZlR2FtZSgpO1xuICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZShcIkxvZ2luU2NlbmVcIik7XG4gIH1cblxuICBnZXRHYW1lSW5mbygpIHtcbiAgICAvLyBsZXQgZ2FtZXMgPSBTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0SW5zdGFuY2UoKS5nZXRHYW1lSW5mbygpIGFzIGFueVtdO1xuICAgIC8vIGxldCByZXM6IFtdID0gW107XG4gICAgLy8gZ2FtZXMuZm9yRWFjaCgoZ2FtZSkgPT4ge1xuICAgIC8vICAgaWYgKGdhbWUudG9waWMgPT0gVG9waWMuU0lDQk8pIHtcbiAgICAvLyAgICAgcmVzID0gZ2FtZS5pbmZvLmZpbGVzTGlzdDtcbiAgICAvLyAgIH1cbiAgICAvLyB9KTtcbiAgICAvLyByZXR1cm4gcmVzO1xuICB9XG5cbiAgaGFuZGxlTm90RW5vdWdoTW9uZXlPbkpvaW50R2FtZShjb2luTnVtYmVyOiBudW1iZXIpIHtcbiAgICB0aGlzLmNoZWNrV2FsbGV0TG9jYWxGb3JCZXR0aW5nKGNvaW5OdW1iZXIsIChlbm91Z2hNb25leSkgPT4ge1xuICAgICAgaWYgKCFlbm91Z2hNb25leSkgY2Muc3lzdGVtRXZlbnQuZW1pdChFdmVudEtleS5IQU5ETEVfT1VUX09GX0JBTEFOQ0VfV0hFTl9KT0lOSU5HX0dBTUUsIFRvcGljLlNJQ0JPLCBjb2luTnVtYmVyKTtcbiAgICB9KTtcbiAgfVxufVxuIl19