
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/Utils/SicboSkeletonUtils.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0abe3eaLONMMLpQQLBqkbDy', 'SicboSkeletonUtils');
// Game/Sicbo_New/Scripts/RNGCommons/Utils/SicboSkeletonUtils.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SicboSkeletonUtils = /** @class */ (function () {
    function SicboSkeletonUtils() {
    }
    SicboSkeletonUtils.setAnimation = function (skeleton, animationName, loop, startTime, timeScale) {
        if (animationName === void 0) { animationName = "animation"; }
        if (loop === void 0) { loop = false; }
        if (startTime === void 0) { startTime = 0; }
        if (timeScale === void 0) { timeScale = 1; }
        if (!skeleton)
            return;
        var state = skeleton.setAnimation(0, animationName, loop);
        if (state)
            state.animationStart = startTime;
        skeleton.timeScale = timeScale;
    };
    return SicboSkeletonUtils;
}());
exports.default = SicboSkeletonUtils;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvVXRpbHMvU2ljYm9Ta2VsZXRvblV0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7SUFBQTtJQU9BLENBQUM7SUFOUSwrQkFBWSxHQUFuQixVQUFvQixRQUFxQixFQUFFLGFBQW1DLEVBQUUsSUFBcUIsRUFBRSxTQUFxQixFQUFFLFNBQXFCO1FBQXhHLDhCQUFBLEVBQUEsMkJBQW1DO1FBQUUscUJBQUEsRUFBQSxZQUFxQjtRQUFFLDBCQUFBLEVBQUEsYUFBcUI7UUFBRSwwQkFBQSxFQUFBLGFBQXFCO1FBQ2pKLElBQUksQ0FBQyxRQUFRO1lBQUUsT0FBTztRQUN0QixJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUF3QixDQUFDO1FBQ2pGLElBQUksS0FBSztZQUFFLEtBQUssQ0FBQyxjQUFjLEdBQUcsU0FBUyxDQUFDO1FBQzVDLFFBQVEsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO0lBQ2pDLENBQUM7SUFDSCx5QkFBQztBQUFELENBUEEsQUFPQyxJQUFBIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1NrZWxldG9uVXRpbHMge1xuICBzdGF0aWMgc2V0QW5pbWF0aW9uKHNrZWxldG9uOiBzcC5Ta2VsZXRvbiwgYW5pbWF0aW9uTmFtZTogc3RyaW5nID0gXCJhbmltYXRpb25cIiwgbG9vcDogYm9vbGVhbiA9IGZhbHNlLCBzdGFydFRpbWU6IG51bWJlciA9IDAsIHRpbWVTY2FsZTogbnVtYmVyID0gMSkge1xuICAgIGlmICghc2tlbGV0b24pIHJldHVybjtcbiAgICBsZXQgc3RhdGUgPSBza2VsZXRvbi5zZXRBbmltYXRpb24oMCwgYW5pbWF0aW9uTmFtZSwgbG9vcCkgYXMgc3Auc3BpbmUuVHJhY2tFbnRyeTtcbiAgICBpZiAoc3RhdGUpIHN0YXRlLmFuaW1hdGlvblN0YXJ0ID0gc3RhcnRUaW1lO1xuICAgIHNrZWxldG9uLnRpbWVTY2FsZSA9IHRpbWVTY2FsZTtcbiAgfVxufVxuIl19