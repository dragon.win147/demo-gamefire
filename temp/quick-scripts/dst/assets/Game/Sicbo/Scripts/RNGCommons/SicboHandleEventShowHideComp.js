
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/SicboHandleEventShowHideComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '139ddCi/WpIPLtIJbk1SVCo', 'SicboHandleEventShowHideComp');
// Game/Sicbo_New/Scripts/RNGCommons/SicboHandleEventShowHideComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboHandleEventShowHideComp = /** @class */ (function (_super) {
    __extends(SicboHandleEventShowHideComp, _super);
    function SicboHandleEventShowHideComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.onHide = null;
        _this.onShow = null;
        return _this;
    }
    SicboHandleEventShowHideComp.prototype.onLoad = function () {
        cc.game.on(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
        cc.game.on(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
    };
    SicboHandleEventShowHideComp.prototype.onChangeTabHide = function () {
        cc.game.pause();
        if (this.onHide)
            this.onHide();
    };
    SicboHandleEventShowHideComp.prototype.onChangeTabShow = function () {
        cc.game.resume();
        if (this.onShow)
            this.onShow();
    };
    SicboHandleEventShowHideComp.prototype.listenEvent = function (onHide, onShow) {
        this.onHide = onHide;
        this.onShow = onShow;
    };
    SicboHandleEventShowHideComp.prototype.onDestroy = function () {
        cc.game.off(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
        cc.game.off(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
    };
    SicboHandleEventShowHideComp = __decorate([
        ccclass
    ], SicboHandleEventShowHideComp);
    return SicboHandleEventShowHideComp;
}(cc.Component));
exports.default = SicboHandleEventShowHideComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvU2ljYm9IYW5kbGVFdmVudFNob3dIaWRlQ29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUEwRCxnREFBWTtJQUF0RTtRQUFBLHFFQTRCQztRQTNCUyxZQUFNLEdBQWUsSUFBSSxDQUFDO1FBQzFCLFlBQU0sR0FBZSxJQUFJLENBQUM7O0lBMEJwQyxDQUFDO0lBeEJDLDZDQUFNLEdBQU47UUFDRSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzNELEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFHLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVTLHNEQUFlLEdBQXpCO1FBQ0UsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQixJQUFJLElBQUksQ0FBQyxNQUFNO1lBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFFUyxzREFBZSxHQUF6QjtRQUNJLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDakIsSUFBSSxJQUFJLENBQUMsTUFBTTtZQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNuQyxDQUFDO0lBR00sa0RBQVcsR0FBbEIsVUFBbUIsTUFBa0IsRUFBRSxNQUFrQjtRQUN2RCxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBQ0QsZ0RBQVMsR0FBVDtRQUNFLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDNUQsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBM0JrQiw0QkFBNEI7UUFEaEQsT0FBTztPQUNhLDRCQUE0QixDQTRCaEQ7SUFBRCxtQ0FBQztDQTVCRCxBQTRCQyxDQTVCeUQsRUFBRSxDQUFDLFNBQVMsR0E0QnJFO2tCQTVCb0IsNEJBQTRCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvSGFuZGxlRXZlbnRTaG93SGlkZUNvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICBwcml2YXRlIG9uSGlkZTogKCkgPT4gdm9pZCA9IG51bGw7XG4gIHByaXZhdGUgb25TaG93OiAoKSA9PiB2b2lkID0gbnVsbDtcblxuICBvbkxvYWQoKSB7XG4gICAgY2MuZ2FtZS5vbihjYy5nYW1lLkVWRU5UX0hJREUsIHRoaXMub25DaGFuZ2VUYWJIaWRlLCB0aGlzKTtcbiAgICBjYy5nYW1lLm9uKGNjLmdhbWUuRVZFTlRfU0hPVywgIHRoaXMub25DaGFuZ2VUYWJTaG93LCB0aGlzKTtcbiAgfVxuICBcbiAgcHJvdGVjdGVkIG9uQ2hhbmdlVGFiSGlkZSgpIHtcbiAgICBjYy5nYW1lLnBhdXNlKCk7XG4gICAgaWYgKHRoaXMub25IaWRlKSB0aGlzLm9uSGlkZSgpO1xuICB9XG5cbiAgcHJvdGVjdGVkIG9uQ2hhbmdlVGFiU2hvdygpIHtcbiAgICAgIGNjLmdhbWUucmVzdW1lKCk7XG4gICAgICBpZiAodGhpcy5vblNob3cpIHRoaXMub25TaG93KCk7XG4gIH1cblxuXG4gIHB1YmxpYyBsaXN0ZW5FdmVudChvbkhpZGU6ICgpID0+IHZvaWQsIG9uU2hvdzogKCkgPT4gdm9pZCkge1xuICAgIHRoaXMub25IaWRlID0gb25IaWRlO1xuICAgIHRoaXMub25TaG93ID0gb25TaG93O1xuICB9XG4gIG9uRGVzdHJveSgpIHtcbiAgICBjYy5nYW1lLm9mZihjYy5nYW1lLkVWRU5UX0hJREUsIHRoaXMub25DaGFuZ2VUYWJIaWRlLCB0aGlzKTtcbiAgICBjYy5nYW1lLm9mZihjYy5nYW1lLkVWRU5UX1NIT1csIHRoaXMub25DaGFuZ2VUYWJTaG93LCB0aGlzKTtcbiAgfVxufVxuIl19