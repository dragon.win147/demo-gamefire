
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/SicboMoneyFormatComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '59266c742JCBY4DW0XLZp9n', 'SicboMoneyFormatComp');
// Game/Sicbo_New/Scripts/RNGCommons/SicboMoneyFormatComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboMoneyFormatType = void 0;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboMoneyFormatType;
(function (SicboMoneyFormatType) {
    SicboMoneyFormatType[SicboMoneyFormatType["None"] = 0] = "None";
    SicboMoneyFormatType[SicboMoneyFormatType["Common"] = 1] = "Common";
    SicboMoneyFormatType[SicboMoneyFormatType["Number"] = 2] = "Number";
    SicboMoneyFormatType[SicboMoneyFormatType["Config"] = 3] = "Config";
    SicboMoneyFormatType[SicboMoneyFormatType["Dot"] = 4] = "Dot";
})(SicboMoneyFormatType = exports.SicboMoneyFormatType || (exports.SicboMoneyFormatType = {}));
var SicboMoneyFormatComp = /** @class */ (function (_super) {
    __extends(SicboMoneyFormatComp, _super);
    function SicboMoneyFormatComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.formatLabel = null;
        _this.formatType = SicboMoneyFormatType.Number;
        _this.prefix = "";
        _this._moneyValue = 0;
        return _this;
    }
    Object.defineProperty(SicboMoneyFormatComp.prototype, "moneyValue", {
        get: function () {
            return this._moneyValue;
        },
        set: function (value) {
            this._moneyValue = value;
            this.doFormat();
        },
        enumerable: false,
        configurable: true
    });
    SicboMoneyFormatComp.prototype.onLoad = function () {
        this.formatLabel = this.getComponent(cc.Label);
        this.doFormat();
    };
    SicboMoneyFormatComp.prototype.onDestroy = function () {
        if (this.t)
            this.t.stop();
    };
    SicboMoneyFormatComp.prototype.setMoney = function (money, isPrefix) {
        if (isPrefix === void 0) { isPrefix = false; }
        if (isPrefix) {
            this.prefix = money > 0 ? "+" : "";
        }
        if (this.t)
            this.t.stop();
        this.moneyValue = money;
    };
    SicboMoneyFormatComp.prototype.setColor = function (color) {
        this.node.color = color;
    };
    SicboMoneyFormatComp.prototype.runMoneyTo = function (money, duration) {
        var _this = this;
        if (duration === void 0) { duration = 0.5; }
        if (money == this.moneyValue)
            return;
        var obj = { current: this.moneyValue };
        if (this.t)
            this.t.stop();
        this.t = cc
            .tween(obj)
            .to(duration, { current: money }, {
            progress: function (start, end, current, ratio) {
                _this.moneyValue = Math.floor(current);
                return start + (end - start) * ratio;
            },
        })
            .call(function (current) { return (_this.moneyValue = money); })
            .start();
    };
    SicboMoneyFormatComp.prototype.doFormat = function () {
        if (this.formatLabel) {
            var formattedString = "";
            var moneyFormat = Math.abs(this.moneyValue);
            if (this.formatType === SicboMoneyFormatType.None) {
                formattedString = this.moneyValue.toString();
            }
            else if (this.formatType === SicboMoneyFormatType.Config) {
                //  formattedString = SicboGameUtils.formatByConfigConvert(moneyFormat);
            }
            else if (this.formatType === SicboMoneyFormatType.Number) {
                formattedString = SicboGameUtils_1.default.formatMoneyNumber(moneyFormat);
            }
            else if (this.formatType === SicboMoneyFormatType.Common) {
                formattedString = SicboGameUtils_1.default.numberWithCommas(moneyFormat);
            }
            else if (this.formatType === SicboMoneyFormatType.Dot) {
                formattedString = SicboGameUtils_1.default.numberWithDot(moneyFormat);
            }
            if (this.moneyValue < 0)
                formattedString = "-" + formattedString;
            this.formatLabel.string = this.prefix + formattedString; // this.moneyValue;
        }
    };
    __decorate([
        property({ type: cc.Enum(SicboMoneyFormatType) })
    ], SicboMoneyFormatComp.prototype, "formatType", void 0);
    __decorate([
        property
    ], SicboMoneyFormatComp.prototype, "prefix", void 0);
    SicboMoneyFormatComp = __decorate([
        ccclass
    ], SicboMoneyFormatComp);
    return SicboMoneyFormatComp;
}(cc.Component));
exports.default = SicboMoneyFormatComp;
var SicboGameUtils_1 = require("./Utils/SicboGameUtils");

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvU2ljYm9Nb25leUZvcm1hdENvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFNLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBQzVDLElBQVksb0JBTVg7QUFORCxXQUFZLG9CQUFvQjtJQUM5QiwrREFBUSxDQUFBO0lBQ1IsbUVBQVUsQ0FBQTtJQUNWLG1FQUFVLENBQUE7SUFDVixtRUFBVSxDQUFBO0lBQ1YsNkRBQU8sQ0FBQTtBQUNULENBQUMsRUFOVyxvQkFBb0IsR0FBcEIsNEJBQW9CLEtBQXBCLDRCQUFvQixRQU0vQjtBQUdEO0lBQWtELHdDQUFZO0lBQTlEO1FBQUEscUVBZ0ZDO1FBL0VDLGlCQUFXLEdBQWEsSUFBSSxDQUFDO1FBRzdCLGdCQUFVLEdBQXlCLG9CQUFvQixDQUFDLE1BQU0sQ0FBQztRQUcvRCxZQUFNLEdBQVcsRUFBRSxDQUFDO1FBRVosaUJBQVcsR0FBVyxDQUFDLENBQUM7O0lBdUVsQyxDQUFDO0lBckVDLHNCQUFJLDRDQUFVO2FBQWQ7WUFDRSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDMUIsQ0FBQzthQUVELFVBQWUsS0FBSztZQUNsQixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztZQUN6QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQzs7O09BTEE7SUFPRCxxQ0FBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDbEIsQ0FBQztJQUVELHdDQUFTLEdBQVQ7UUFDRSxJQUFJLElBQUksQ0FBQyxDQUFDO1lBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsdUNBQVEsR0FBUixVQUFTLEtBQUssRUFBRSxRQUFnQjtRQUFoQix5QkFBQSxFQUFBLGdCQUFnQjtRQUM5QixJQUFJLFFBQVEsRUFBRTtZQUNaLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxDQUFDO1lBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztJQUMxQixDQUFDO0lBQ0QsdUNBQVEsR0FBUixVQUFTLEtBQUs7UUFDWixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDMUIsQ0FBQztJQUdELHlDQUFVLEdBQVYsVUFBVyxLQUFhLEVBQUUsUUFBc0I7UUFBaEQsaUJBa0JDO1FBbEJ5Qix5QkFBQSxFQUFBLGNBQXNCO1FBQzlDLElBQUksS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVO1lBQUUsT0FBTztRQUNyQyxJQUFJLEdBQUcsR0FBRyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDdkMsSUFBSSxJQUFJLENBQUMsQ0FBQztZQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFO2FBQ1IsS0FBSyxDQUFDLEdBQUcsQ0FBQzthQUNWLEVBQUUsQ0FDRCxRQUFRLEVBQ1IsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQ2xCO1lBQ0UsUUFBUSxFQUFFLFVBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsS0FBSztnQkFDbkMsS0FBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN0QyxPQUFPLEtBQUssR0FBRyxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDdkMsQ0FBQztTQUNGLENBQ0Y7YUFDQSxJQUFJLENBQUMsVUFBQyxPQUFPLElBQUssT0FBQSxDQUFDLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLEVBQXpCLENBQXlCLENBQUM7YUFDNUMsS0FBSyxFQUFFLENBQUM7SUFDYixDQUFDO0lBRUQsdUNBQVEsR0FBUjtRQUNFLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNwQixJQUFJLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFDekIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDNUMsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLG9CQUFvQixDQUFDLElBQUksRUFBRTtnQkFDakQsZUFBZSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDOUM7aUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLG9CQUFvQixDQUFDLE1BQU0sRUFBRTtnQkFDMUQsd0VBQXdFO2FBQ3pFO2lCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUU7Z0JBQzFELGVBQWUsR0FBRyx3QkFBYyxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ2pFO2lCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUU7Z0JBQzFELGVBQWUsR0FBRyx3QkFBYyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ2hFO2lCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxvQkFBb0IsQ0FBQyxHQUFHLEVBQUU7Z0JBQ3ZELGVBQWUsR0FBRyx3QkFBYyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUM3RDtZQUNELElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDO2dCQUFFLGVBQWUsR0FBRyxHQUFHLEdBQUcsZUFBZSxDQUFDO1lBQ2pFLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsZUFBZSxDQUFDLENBQUMsbUJBQW1CO1NBQzdFO0lBQ0gsQ0FBQztJQTNFRDtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEVBQUUsQ0FBQzs0REFDYTtJQUcvRDtRQURDLFFBQVE7d0RBQ1c7SUFQRCxvQkFBb0I7UUFEeEMsT0FBTztPQUNhLG9CQUFvQixDQWdGeEM7SUFBRCwyQkFBQztDQWhGRCxBQWdGQyxDQWhGaUQsRUFBRSxDQUFDLFNBQVMsR0FnRjdEO2tCQWhGb0Isb0JBQW9CO0FBa0Z6Qyx5REFBb0QiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuZXhwb3J0IGVudW0gU2ljYm9Nb25leUZvcm1hdFR5cGUge1xuICBOb25lID0gMCwgLy8gMTIzNDU2XG4gIENvbW1vbiA9IDEsIC8vIDEyMy40NTZcbiAgTnVtYmVyID0gMiwgLy8gMTBNXG4gIENvbmZpZyA9IDMsIC8vIG51bSAvIGNvbmZpZ0NvbnZlcnRcbiAgRG90ID0gNCxcbn1cblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvTW9uZXlGb3JtYXRDb21wIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgZm9ybWF0TGFiZWw6IGNjLkxhYmVsID0gbnVsbDtcblxuICBAcHJvcGVydHkoeyB0eXBlOiBjYy5FbnVtKFNpY2JvTW9uZXlGb3JtYXRUeXBlKSB9KVxuICBmb3JtYXRUeXBlOiBTaWNib01vbmV5Rm9ybWF0VHlwZSA9IFNpY2JvTW9uZXlGb3JtYXRUeXBlLk51bWJlcjtcblxuICBAcHJvcGVydHlcbiAgcHJlZml4OiBzdHJpbmcgPSBcIlwiO1xuXG4gIHByaXZhdGUgX21vbmV5VmFsdWU6IG51bWJlciA9IDA7XG5cbiAgZ2V0IG1vbmV5VmFsdWUoKSB7XG4gICAgcmV0dXJuIHRoaXMuX21vbmV5VmFsdWU7XG4gIH1cblxuICBzZXQgbW9uZXlWYWx1ZSh2YWx1ZSkge1xuICAgIHRoaXMuX21vbmV5VmFsdWUgPSB2YWx1ZTtcbiAgICB0aGlzLmRvRm9ybWF0KCk7XG4gIH1cblxuICBvbkxvYWQoKSB7XG4gICAgdGhpcy5mb3JtYXRMYWJlbCA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkxhYmVsKTtcbiAgICB0aGlzLmRvRm9ybWF0KCk7XG4gIH1cblxuICBvbkRlc3Ryb3koKSB7XG4gICAgaWYgKHRoaXMudCkgdGhpcy50LnN0b3AoKTtcbiAgfVxuXG4gIHNldE1vbmV5KG1vbmV5LCBpc1ByZWZpeCA9IGZhbHNlKSB7XG4gICAgaWYgKGlzUHJlZml4KSB7XG4gICAgICB0aGlzLnByZWZpeCA9IG1vbmV5ID4gMCA/IFwiK1wiIDogXCJcIjtcbiAgICB9XG4gICAgaWYgKHRoaXMudCkgdGhpcy50LnN0b3AoKTtcbiAgICB0aGlzLm1vbmV5VmFsdWUgPSBtb25leTtcbiAgfVxuICBzZXRDb2xvcihjb2xvcikge1xuICAgIHRoaXMubm9kZS5jb2xvciA9IGNvbG9yO1xuICB9XG5cbiAgcHJpdmF0ZSB0OiBjYy5Ud2VlbjtcbiAgcnVuTW9uZXlUbyhtb25leTogbnVtYmVyLCBkdXJhdGlvbjogbnVtYmVyID0gMC41KSB7XG4gICAgaWYgKG1vbmV5ID09IHRoaXMubW9uZXlWYWx1ZSkgcmV0dXJuO1xuICAgIHZhciBvYmogPSB7IGN1cnJlbnQ6IHRoaXMubW9uZXlWYWx1ZSB9O1xuICAgIGlmICh0aGlzLnQpIHRoaXMudC5zdG9wKCk7XG4gICAgdGhpcy50ID0gY2NcbiAgICAgIC50d2VlbihvYmopXG4gICAgICAudG8oXG4gICAgICAgIGR1cmF0aW9uLFxuICAgICAgICB7IGN1cnJlbnQ6IG1vbmV5IH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm9ncmVzczogKHN0YXJ0LCBlbmQsIGN1cnJlbnQsIHJhdGlvKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm1vbmV5VmFsdWUgPSBNYXRoLmZsb29yKGN1cnJlbnQpO1xuICAgICAgICAgICAgcmV0dXJuIHN0YXJ0ICsgKGVuZCAtIHN0YXJ0KSAqIHJhdGlvO1xuICAgICAgICAgIH0sXG4gICAgICAgIH1cbiAgICAgIClcbiAgICAgIC5jYWxsKChjdXJyZW50KSA9PiAodGhpcy5tb25leVZhbHVlID0gbW9uZXkpKVxuICAgICAgLnN0YXJ0KCk7XG4gIH1cblxuICBkb0Zvcm1hdCgpIHtcbiAgICBpZiAodGhpcy5mb3JtYXRMYWJlbCkge1xuICAgICAgbGV0IGZvcm1hdHRlZFN0cmluZyA9IFwiXCI7XG4gICAgICBsZXQgbW9uZXlGb3JtYXQgPSBNYXRoLmFicyh0aGlzLm1vbmV5VmFsdWUpO1xuICAgICAgaWYgKHRoaXMuZm9ybWF0VHlwZSA9PT0gU2ljYm9Nb25leUZvcm1hdFR5cGUuTm9uZSkge1xuICAgICAgICBmb3JtYXR0ZWRTdHJpbmcgPSB0aGlzLm1vbmV5VmFsdWUudG9TdHJpbmcoKTtcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5mb3JtYXRUeXBlID09PSBTaWNib01vbmV5Rm9ybWF0VHlwZS5Db25maWcpIHtcbiAgICAgICAgLy8gIGZvcm1hdHRlZFN0cmluZyA9IFNpY2JvR2FtZVV0aWxzLmZvcm1hdEJ5Q29uZmlnQ29udmVydChtb25leUZvcm1hdCk7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMuZm9ybWF0VHlwZSA9PT0gU2ljYm9Nb25leUZvcm1hdFR5cGUuTnVtYmVyKSB7XG4gICAgICAgIGZvcm1hdHRlZFN0cmluZyA9IFNpY2JvR2FtZVV0aWxzLmZvcm1hdE1vbmV5TnVtYmVyKG1vbmV5Rm9ybWF0KTtcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5mb3JtYXRUeXBlID09PSBTaWNib01vbmV5Rm9ybWF0VHlwZS5Db21tb24pIHtcbiAgICAgICAgZm9ybWF0dGVkU3RyaW5nID0gU2ljYm9HYW1lVXRpbHMubnVtYmVyV2l0aENvbW1hcyhtb25leUZvcm1hdCk7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMuZm9ybWF0VHlwZSA9PT0gU2ljYm9Nb25leUZvcm1hdFR5cGUuRG90KSB7XG4gICAgICAgIGZvcm1hdHRlZFN0cmluZyA9IFNpY2JvR2FtZVV0aWxzLm51bWJlcldpdGhEb3QobW9uZXlGb3JtYXQpO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMubW9uZXlWYWx1ZSA8IDApIGZvcm1hdHRlZFN0cmluZyA9IFwiLVwiICsgZm9ybWF0dGVkU3RyaW5nO1xuICAgICAgdGhpcy5mb3JtYXRMYWJlbC5zdHJpbmcgPSB0aGlzLnByZWZpeCArIGZvcm1hdHRlZFN0cmluZzsgLy8gdGhpcy5tb25leVZhbHVlO1xuICAgIH1cbiAgfVxufVxuXG5pbXBvcnQgU2ljYm9HYW1lVXRpbHMgZnJvbSBcIi4vVXRpbHMvU2ljYm9HYW1lVXRpbHNcIjtcblxuIl19