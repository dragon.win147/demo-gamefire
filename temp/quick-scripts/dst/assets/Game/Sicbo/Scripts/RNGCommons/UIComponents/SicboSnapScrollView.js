
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboSnapScrollView.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b5d6bc/eZdIpaukMTL7PNWm', 'SicboSnapScrollView');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboSnapScrollView.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ScrollEvent = cc.ScrollView.EventType;
var SicboSnapScrollView = /** @class */ (function (_super) {
    __extends(SicboSnapScrollView, _super);
    function SicboSnapScrollView() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scrollView = null;
        _this.numOfItemInView = 4;
        _this.contentStartPosX = 0;
        _this.preScrollEvent = ScrollEvent.SCROLL_BEGAN;
        _this.snapActive = false;
        return _this;
    }
    Object.defineProperty(SicboSnapScrollView.prototype, "contentPosX", {
        get: function () {
            return this.scrollView.getContentPosition().x;
        },
        enumerable: false,
        configurable: true
    });
    SicboSnapScrollView.prototype.onLoad = function () {
        this.scrollView = this.getComponent(cc.ScrollView);
        this.contentStartPosX = this.contentPosX;
        this.init();
        this.registerEvent();
    };
    SicboSnapScrollView.prototype.init = function () {
        var scrollViewEventHandler = new cc.Component.EventHandler();
        scrollViewEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        scrollViewEventHandler.component = "SicboSnapScrollView"; // This is the code file name
        scrollViewEventHandler.handler = "callback";
        // scrollViewEventHandler.customEventData = "";    
        this.scrollView.scrollEvents.push(scrollViewEventHandler);
    };
    SicboSnapScrollView.prototype.registerEvent = function () {
        // this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this, true);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnded, this, true);
        this.node.on(cc.Node.EventType.MOUSE_WHEEL, this.onMouseWheel, this, true);
    };
    SicboSnapScrollView.prototype.unregisterEvent = function () {
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this, true);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnded, this, true);
        this.node.off(cc.Node.EventType.MOUSE_WHEEL, this.onMouseWheel, this, true);
    };
    SicboSnapScrollView.prototype.onTouchBegan = function () {
        // //cc.log("DKM", "onTouchBegan");
    };
    SicboSnapScrollView.prototype.onTouchEnded = function () {
        // //cc.log("DKM", "onTouchEnded");
        this.snapActive = true;
    };
    // private onTouchCancelled(){
    //     //cc.log("DKM", "onTouchCancelled");
    // }
    SicboSnapScrollView.prototype.onMouseWheel = function () {
        // //cc.log("DKM", "onMouseWheel");
        this.snapActive = true;
    };
    // update (dt) {}
    SicboSnapScrollView.prototype.callback = function (scrollView, eventType, customEventData) {
        this.preScrollEvent = eventType;
        switch (eventType) {
            case cc.ScrollView.EventType.SCROLL_ENDED:
                this.onScrollEnd();
                break;
            case cc.ScrollView.EventType.AUTOSCROLL_ENDED_WITH_THRESHOLD:
                // //cc.log("DKM", "AUTOSCROLL_ENDED_WITH_THRESHOLD");
                break;
            case cc.ScrollView.EventType.SCROLL_BEGAN:
                // //cc.log("DKM", "SCROLL_BEGAN");
                break;
            case cc.ScrollView.EventType.SCROLLING:
                // //cc.log("DKM", "SCROLLING");
                break;
        }
    };
    SicboSnapScrollView.prototype.onScrollEnd = function () {
        this.snap();
        this.snapActive = false;
    };
    SicboSnapScrollView.prototype.snap = function () {
        if (this.snapActive == false)
            return;
        var numOfItem = this.scrollView.content.childrenCount;
        var spacing = this.scrollView.content.width / numOfItem;
        var curPosX = this.contentPosX;
        var itemIndex = Math.abs(Math.round((curPosX - this.contentStartPosX) / spacing)
            - Math.round((numOfItem - this.numOfItemInView) / 2));
        var percent = itemIndex * (numOfItem / (numOfItem - this.numOfItemInView)) / numOfItem;
        this.scrollView.scrollToPercentHorizontal(percent, .5);
        //cc.log("Snap", itemIndex);
        // this.scrollView.scrollToPercentHorizontal(1 * 2/numOfItem, .5);
    };
    SicboSnapScrollView.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
        this.unregisterEvent();
    };
    __decorate([
        property
    ], SicboSnapScrollView.prototype, "numOfItemInView", void 0);
    SicboSnapScrollView = __decorate([
        ccclass
    ], SicboSnapScrollView);
    return SicboSnapScrollView;
}(cc.Component));
exports.default = SicboSnapScrollView;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvVUlDb21wb25lbnRzL1NpY2JvU25hcFNjcm9sbFZpZXcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFNUUsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFDMUMsSUFBTSxXQUFXLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7QUFFNUM7SUFBaUQsdUNBQVk7SUFBN0Q7UUFBQSxxRUEyR0M7UUExR0csZ0JBQVUsR0FBaUIsSUFBSSxDQUFDO1FBRWhDLHFCQUFlLEdBQVUsQ0FBQyxDQUFDO1FBQzNCLHNCQUFnQixHQUFXLENBQUMsQ0FBQztRQUU3QixvQkFBYyxHQUFHLFdBQVcsQ0FBQyxZQUFZLENBQUM7UUFDMUMsZ0JBQVUsR0FBWSxLQUFLLENBQUM7O0lBb0doQyxDQUFDO0lBbEdHLHNCQUFZLDRDQUFXO2FBQXZCO1lBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2xELENBQUM7OztPQUFBO0lBRUQsb0NBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDekMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ1osSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxrQ0FBSSxHQUFKO1FBQ0ksSUFBSSxzQkFBc0IsR0FBRyxJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDN0Qsc0JBQXNCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQywyRUFBMkU7UUFDdEgsc0JBQXNCLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLENBQUMsNkJBQTZCO1FBQ3ZGLHNCQUFzQixDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUM7UUFDNUMsbURBQW1EO1FBRW5ELElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFHTywyQ0FBYSxHQUFyQjtRQUNJLDhFQUE4RTtRQUM5RSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFFTyw2Q0FBZSxHQUF2QjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM1RSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2hGLENBQUM7SUFFTywwQ0FBWSxHQUFwQjtRQUNJLG1DQUFtQztJQUN2QyxDQUFDO0lBRU8sMENBQVksR0FBcEI7UUFDSSxtQ0FBbUM7UUFDbkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDM0IsQ0FBQztJQUVELDhCQUE4QjtJQUM5QiwyQ0FBMkM7SUFDM0MsSUFBSTtJQUVJLDBDQUFZLEdBQXBCO1FBQ0ksbUNBQW1DO1FBQ25DLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQzNCLENBQUM7SUFFRCxpQkFBaUI7SUFHakIsc0NBQVEsR0FBUixVQUFTLFVBQXlCLEVBQUUsU0FBa0MsRUFBRSxlQUFlO1FBQ25GLElBQUksQ0FBQyxjQUFjLEdBQUcsU0FBUyxDQUFDO1FBQ2hDLFFBQVEsU0FBUyxFQUFFO1lBQ2pCLEtBQUssRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsWUFBWTtnQkFDdkMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFBO2dCQUNsQixNQUFNO1lBQ1IsS0FBSyxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQywrQkFBK0I7Z0JBQzFELHNEQUFzRDtnQkFDdEQsTUFBTTtZQUNSLEtBQUssRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsWUFBWTtnQkFDdkMsbUNBQW1DO2dCQUNuQyxNQUFNO1lBQ1IsS0FBSyxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxTQUFTO2dCQUNwQyxnQ0FBZ0M7Z0JBQ2hDLE1BQU07U0FDVDtJQUNMLENBQUM7SUFFRCx5Q0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ1osSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQUVELGtDQUFJLEdBQUo7UUFDSSxJQUFHLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSztZQUFFLE9BQU87UUFDcEMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDO1FBQ3RELElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBQyxTQUFTLENBQUM7UUFDdEQsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUUvQixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUMsT0FBTyxDQUFDO2NBQzVELElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFcEUsSUFBSSxPQUFPLEdBQUcsU0FBUyxHQUFHLENBQUMsU0FBUyxHQUFDLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxHQUFDLFNBQVMsQ0FBQztRQUVuRixJQUFJLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztRQUN2RCw0QkFBNEI7UUFDNUIsa0VBQWtFO0lBQ3RFLENBQUM7SUFFRCx1Q0FBUyxHQUFUO1FBQ0ksRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBdkdEO1FBREMsUUFBUTtnRUFDa0I7SUFIVixtQkFBbUI7UUFEdkMsT0FBTztPQUNhLG1CQUFtQixDQTJHdkM7SUFBRCwwQkFBQztDQTNHRCxBQTJHQyxDQTNHZ0QsRUFBRSxDQUFDLFNBQVMsR0EyRzVEO2tCQTNHb0IsbUJBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuY29uc3QgU2Nyb2xsRXZlbnQgPSBjYy5TY3JvbGxWaWV3LkV2ZW50VHlwZTtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1NuYXBTY3JvbGxWaWV3IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBzY3JvbGxWaWV3OmNjLlNjcm9sbFZpZXcgPSBudWxsO1xuICAgIEBwcm9wZXJ0eVxuICAgIG51bU9mSXRlbUluVmlldzpudW1iZXIgPSA0O1xuICAgIGNvbnRlbnRTdGFydFBvc1g6IG51bWJlciA9IDA7XG5cbiAgICBwcmVTY3JvbGxFdmVudCA9IFNjcm9sbEV2ZW50LlNDUk9MTF9CRUdBTjtcbiAgICBzbmFwQWN0aXZlOiBib29sZWFuID0gZmFsc2U7XG5cbiAgICBwcml2YXRlIGdldCBjb250ZW50UG9zWCgpe1xuICAgICAgICByZXR1cm4gdGhpcy5zY3JvbGxWaWV3LmdldENvbnRlbnRQb3NpdGlvbigpLng7XG4gICAgfVxuXG4gICAgb25Mb2FkKCl7ICAgICAgICBcbiAgICAgICAgdGhpcy5zY3JvbGxWaWV3ID0gdGhpcy5nZXRDb21wb25lbnQoY2MuU2Nyb2xsVmlldyk7XG4gICAgICAgIHRoaXMuY29udGVudFN0YXJ0UG9zWCA9IHRoaXMuY29udGVudFBvc1g7XG4gICAgICAgIHRoaXMuaW5pdCgpO1xuICAgICAgICB0aGlzLnJlZ2lzdGVyRXZlbnQoKTtcbiAgICB9XG5cbiAgICBpbml0KCkge1xuICAgICAgICB2YXIgc2Nyb2xsVmlld0V2ZW50SGFuZGxlciA9IG5ldyBjYy5Db21wb25lbnQuRXZlbnRIYW5kbGVyKCk7XG4gICAgICAgIHNjcm9sbFZpZXdFdmVudEhhbmRsZXIudGFyZ2V0ID0gdGhpcy5ub2RlOyAvLyBUaGlzIG5vZGUgaXMgdGhlIG5vZGUgdG8gd2hpY2ggeW91ciBldmVudCBoYW5kbGVyIGNvZGUgY29tcG9uZW50IGJlbG9uZ3NcbiAgICAgICAgc2Nyb2xsVmlld0V2ZW50SGFuZGxlci5jb21wb25lbnQgPSBcIlNpY2JvU25hcFNjcm9sbFZpZXdcIjsgLy8gVGhpcyBpcyB0aGUgY29kZSBmaWxlIG5hbWVcbiAgICAgICAgc2Nyb2xsVmlld0V2ZW50SGFuZGxlci5oYW5kbGVyID0gXCJjYWxsYmFja1wiO1xuICAgICAgICAvLyBzY3JvbGxWaWV3RXZlbnRIYW5kbGVyLmN1c3RvbUV2ZW50RGF0YSA9IFwiXCI7ICAgIFxuICAgICBcbiAgICAgICAgdGhpcy5zY3JvbGxWaWV3LnNjcm9sbEV2ZW50cy5wdXNoKHNjcm9sbFZpZXdFdmVudEhhbmRsZXIpOyAgICAgICAgXG4gICAgfVxuXG4gICAgXG4gICAgcHJpdmF0ZSByZWdpc3RlckV2ZW50KCkgeyAgICAgICAgXG4gICAgICAgIC8vIHRoaXMubm9kZS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9TVEFSVCwgdGhpcy5vblRvdWNoQmVnYW4sIHRoaXMsIHRydWUpO1xuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfRU5ELCB0aGlzLm9uVG91Y2hFbmRlZCwgdGhpcywgdHJ1ZSk7XG4gICAgICAgIHRoaXMubm9kZS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5NT1VTRV9XSEVFTCwgdGhpcy5vbk1vdXNlV2hlZWwsIHRoaXMsIHRydWUpO1xuICAgIH1cblxuICAgIHByaXZhdGUgdW5yZWdpc3RlckV2ZW50KCkgeyAgICAgICAgXG4gICAgICAgIHRoaXMubm9kZS5vZmYoY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfU1RBUlQsIHRoaXMub25Ub3VjaEJlZ2FuLCB0aGlzLCB0cnVlKTsgICAgICAgIFxuICAgICAgICB0aGlzLm5vZGUub2ZmKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX0VORCwgdGhpcy5vblRvdWNoRW5kZWQsIHRoaXMsIHRydWUpOyAgICAgICAgXG4gICAgICAgIHRoaXMubm9kZS5vZmYoY2MuTm9kZS5FdmVudFR5cGUuTU9VU0VfV0hFRUwsIHRoaXMub25Nb3VzZVdoZWVsLCB0aGlzLCB0cnVlKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIG9uVG91Y2hCZWdhbigpe1xuICAgICAgICAvLyAvL2NjLmxvZyhcIkRLTVwiLCBcIm9uVG91Y2hCZWdhblwiKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIG9uVG91Y2hFbmRlZCgpe1xuICAgICAgICAvLyAvL2NjLmxvZyhcIkRLTVwiLCBcIm9uVG91Y2hFbmRlZFwiKTtcbiAgICAgICAgdGhpcy5zbmFwQWN0aXZlID0gdHJ1ZTtcbiAgICB9XG5cbiAgICAvLyBwcml2YXRlIG9uVG91Y2hDYW5jZWxsZWQoKXtcbiAgICAvLyAgICAgLy9jYy5sb2coXCJES01cIiwgXCJvblRvdWNoQ2FuY2VsbGVkXCIpO1xuICAgIC8vIH1cblxuICAgIHByaXZhdGUgb25Nb3VzZVdoZWVsKCl7ICAgICAgICBcbiAgICAgICAgLy8gLy9jYy5sb2coXCJES01cIiwgXCJvbk1vdXNlV2hlZWxcIik7XG4gICAgICAgIHRoaXMuc25hcEFjdGl2ZSA9IHRydWU7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cblxuXG4gICAgY2FsbGJhY2soc2Nyb2xsVmlldzogY2MuU2Nyb2xsVmlldywgZXZlbnRUeXBlOiBjYy5TY3JvbGxWaWV3LkV2ZW50VHlwZSwgY3VzdG9tRXZlbnREYXRhKSB7XG4gICAgICAgIHRoaXMucHJlU2Nyb2xsRXZlbnQgPSBldmVudFR5cGU7XG4gICAgICAgIHN3aXRjaCAoZXZlbnRUeXBlKSB7XG4gICAgICAgICAgY2FzZSBjYy5TY3JvbGxWaWV3LkV2ZW50VHlwZS5TQ1JPTExfRU5ERUQ6ICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLm9uU2Nyb2xsRW5kKClcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgY2MuU2Nyb2xsVmlldy5FdmVudFR5cGUuQVVUT1NDUk9MTF9FTkRFRF9XSVRIX1RIUkVTSE9MRDogICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIC8vY2MubG9nKFwiREtNXCIsIFwiQVVUT1NDUk9MTF9FTkRFRF9XSVRIX1RIUkVTSE9MRFwiKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgY2MuU2Nyb2xsVmlldy5FdmVudFR5cGUuU0NST0xMX0JFR0FOOlxuICAgICAgICAgICAgLy8gLy9jYy5sb2coXCJES01cIiwgXCJTQ1JPTExfQkVHQU5cIik7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIGNjLlNjcm9sbFZpZXcuRXZlbnRUeXBlLlNDUk9MTElORzpcbiAgICAgICAgICAgIC8vIC8vY2MubG9nKFwiREtNXCIsIFwiU0NST0xMSU5HXCIpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBvblNjcm9sbEVuZCgpe1xuICAgICAgICB0aGlzLnNuYXAoKTsgICAgICAgIFxuICAgICAgICB0aGlzLnNuYXBBY3RpdmUgPSBmYWxzZTtcbiAgICB9ICAgIFxuXG4gICAgc25hcCgpe1xuICAgICAgICBpZih0aGlzLnNuYXBBY3RpdmUgPT0gZmFsc2UpIHJldHVybjtcbiAgICAgICAgbGV0IG51bU9mSXRlbSA9IHRoaXMuc2Nyb2xsVmlldy5jb250ZW50LmNoaWxkcmVuQ291bnQ7XG4gICAgICAgIGxldCBzcGFjaW5nID0gdGhpcy5zY3JvbGxWaWV3LmNvbnRlbnQud2lkdGgvbnVtT2ZJdGVtO1xuICAgICAgICBsZXQgY3VyUG9zWCA9IHRoaXMuY29udGVudFBvc1g7XG5cbiAgICAgICAgbGV0IGl0ZW1JbmRleCA9IE1hdGguYWJzKE1hdGgucm91bmQoKGN1clBvc1ggLSB0aGlzLmNvbnRlbnRTdGFydFBvc1gpL3NwYWNpbmcpIFxuICAgICAgICAgICAgICAgICAgICAgICAgLSBNYXRoLnJvdW5kKChudW1PZkl0ZW0gLSB0aGlzLm51bU9mSXRlbUluVmlldykvMikpO1xuICAgICAgXG4gICAgICAgIGxldCBwZXJjZW50ID0gaXRlbUluZGV4ICogKG51bU9mSXRlbS8obnVtT2ZJdGVtIC0gdGhpcy5udW1PZkl0ZW1JblZpZXcpKS9udW1PZkl0ZW07XG4gICAgXG4gICAgICAgIHRoaXMuc2Nyb2xsVmlldy5zY3JvbGxUb1BlcmNlbnRIb3Jpem9udGFsKHBlcmNlbnQsIC41KTtcbiAgICAgICAgLy9jYy5sb2coXCJTbmFwXCIsIGl0ZW1JbmRleCk7XG4gICAgICAgIC8vIHRoaXMuc2Nyb2xsVmlldy5zY3JvbGxUb1BlcmNlbnRIb3Jpem9udGFsKDEgKiAyL251bU9mSXRlbSwgLjUpO1xuICAgIH1cblxuICAgIG9uRGVzdHJveSgpe1xuICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5ub2RlKTtcbiAgICAgICAgdGhpcy51bnJlZ2lzdGVyRXZlbnQoKTtcbiAgICB9XG59XG4iXX0=