
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/SicboAutoCanvasSizeComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ebf91qnCRNIQbM6FVs8N1An', 'SicboAutoCanvasSizeComp');
// Game/Sicbo_New/Scripts/RNGCommons/SicboAutoCanvasSizeComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var EventKey = SicboModuleAdapter_1.default.getAllRefs().EventKey;
var Size = cc.Size;
var ccclass = cc._decorator.ccclass;
var SicboAutoCanvasSizeComp = /** @class */ (function (_super) {
    __extends(SicboAutoCanvasSizeComp, _super);
    function SicboAutoCanvasSizeComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.defaultRatio = 16 / 9;
        _this.defaultRatioMax = 13 / 6;
        _this.defaultRatioMin = 4 / 3;
        _this.startSize = new cc.Size(1280, 720);
        return _this;
    }
    SicboAutoCanvasSizeComp.prototype.onLoad = function () {
        var _this = this;
        var canvas = this.getComponent(cc.Canvas);
        this.startSize = canvas.designResolution;
        this.setFlexibleWebScreen();
        cc.view.setResizeCallback(function () {
            if (_this && _this.node)
                _this.setFlexibleWebScreen();
        });
    };
    SicboAutoCanvasSizeComp.prototype.onDestroy = function () {
        if (this.isWebBuild()) {
            cc.view.setResizeCallback(null);
        }
        this.unscheduleAllCallbacks();
    };
    SicboAutoCanvasSizeComp.prototype.setFlexibleWebScreen = function () {
        var size = cc.view.getFrameSize();
        var w = size.width;
        var h = size.height;
        var ratio = w / h;
        var fitHeight = ratio > this.defaultRatio;
        var max = false;
        if (ratio > this.defaultRatioMax) {
            ratio = this.defaultRatioMax;
            max = true;
        }
        var min = false;
        if (ratio < this.defaultRatioMin) {
            ratio = this.defaultRatioMin;
            min = true;
        }
        var canvas = this.getComponent(cc.Canvas);
        canvas.fitWidth = !fitHeight;
        canvas.fitHeight = fitHeight;
        if (fitHeight) {
            if (this.isWebBuild() && max) {
                canvas.designResolution = new Size(this.startSize.height * this.defaultRatioMax, this.startSize.height);
                canvas.fitWidth = true;
                canvas.fitHeight = true;
            }
            else {
                canvas.designResolution = new Size(this.startSize.width, this.startSize.height);
            }
        }
        else {
            if (this.isWebBuild() && min) {
                canvas.designResolution = new Size(this.startSize.width, this.startSize.width / this.defaultRatioMin);
                canvas.fitWidth = true;
                canvas.fitHeight = true;
            }
            else {
                canvas.designResolution = new Size(this.startSize.width, this.startSize.height);
            }
        }
        this.scheduleOnce(function () {
            cc.systemEvent.emit(EventKey.ON_SCREEN_CHANGE_SIZE_EVENT);
        }, 1 / 60);
    };
    SicboAutoCanvasSizeComp.prototype.isWebBuild = function () {
        return (cc.sys.platform == cc.sys.DESKTOP_BROWSER || cc.sys.platform == cc.sys.MOBILE_BROWSER) && CC_BUILD;
    };
    SicboAutoCanvasSizeComp = __decorate([
        ccclass
    ], SicboAutoCanvasSizeComp);
    return SicboAutoCanvasSizeComp;
}(cc.Component));
exports.default = SicboAutoCanvasSizeComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvU2ljYm9BdXRvQ2FudmFzU2l6ZUNvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYscUVBQWdFO0FBRzlELElBQUEsUUFBUSxHQUNOLDRCQUFrQixDQUFDLFVBQVUsRUFBRSxTQUR6QixDQUMwQjtBQUNwQyxJQUFPLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDO0FBRWQsSUFBQSxPQUFPLEdBQUssRUFBRSxDQUFDLFVBQVUsUUFBbEIsQ0FBbUI7QUFHbEM7SUFBcUQsMkNBQVk7SUFBakU7UUFBQSxxRUFzRUM7UUFyRWtCLGtCQUFZLEdBQVcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM5QixxQkFBZSxHQUFXLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDakMscUJBQWUsR0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pDLGVBQVMsR0FBWSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDOztJQWtFdEQsQ0FBQztJQWhFQyx3Q0FBTSxHQUFOO1FBQUEsaUJBT0M7UUFOQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQztRQUN6QyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUM1QixFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1lBQ3hCLElBQUksS0FBSSxJQUFJLEtBQUksQ0FBQyxJQUFJO2dCQUFFLEtBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQ3JELENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVTLDJDQUFTLEdBQW5CO1FBQ0UsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDckIsRUFBRSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNqQztRQUNELElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFTyxzREFBb0IsR0FBNUI7UUFDRSxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNwQixJQUFJLEtBQUssR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2xCLElBQUksU0FBUyxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQzFDLElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQztRQUNoQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ2hDLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQzdCLEdBQUcsR0FBRyxJQUFJLENBQUM7U0FDWjtRQUVELElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQztRQUNoQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ2hDLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQzdCLEdBQUcsR0FBRyxJQUFJLENBQUM7U0FDWjtRQUVELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxTQUFTLENBQUM7UUFDN0IsTUFBTSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFFN0IsSUFBSSxTQUFTLEVBQUU7WUFDYixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxHQUFHLEVBQUU7Z0JBQzVCLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hHLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQzthQUN6QjtpQkFBTTtnQkFDTCxNQUFNLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNqRjtTQUNGO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxHQUFHLEVBQUU7Z0JBQzVCLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQ3RHLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQzthQUN6QjtpQkFBTTtnQkFDTCxNQUFNLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNqRjtTQUNGO1FBRUQsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNoQixFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsMkJBQTJCLENBQUMsQ0FBQTtRQUMzRCxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVPLDRDQUFVLEdBQWxCO1FBQ0UsT0FBTyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsZUFBZSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksUUFBUSxDQUFDO0lBQzdHLENBQUM7SUFyRWtCLHVCQUF1QjtRQUQzQyxPQUFPO09BQ2EsdUJBQXVCLENBc0UzQztJQUFELDhCQUFDO0NBdEVELEFBc0VDLENBdEVvRCxFQUFFLENBQUMsU0FBUyxHQXNFaEU7a0JBdEVvQix1QkFBdUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFNpY2JvTW9kdWxlQWRhcHRlciBmcm9tICcuLi8uLi8uLi8uLi9TaWNib01vZHVsZUFkYXB0ZXInO1xuXG5jb25zdCB7XG4gIEV2ZW50S2V5XG59ID0gU2ljYm9Nb2R1bGVBZGFwdGVyLmdldEFsbFJlZnMoKTtcbmltcG9ydCBTaXplID0gY2MuU2l6ZTtcblxuY29uc3QgeyBjY2NsYXNzIH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9BdXRvQ2FudmFzU2l6ZUNvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICBwcml2YXRlIHJlYWRvbmx5IGRlZmF1bHRSYXRpbzogbnVtYmVyID0gMTYgLyA5O1xuICBwcml2YXRlIHJlYWRvbmx5IGRlZmF1bHRSYXRpb01heDogbnVtYmVyID0gMTMgLyA2O1xuICBwcml2YXRlIHJlYWRvbmx5IGRlZmF1bHRSYXRpb01pbjogbnVtYmVyID0gNCAvIDM7XG4gIHByaXZhdGUgc3RhcnRTaXplOiBjYy5TaXplID0gbmV3IGNjLlNpemUoMTI4MCwgNzIwKTtcblxuICBvbkxvYWQoKSB7XG4gICAgbGV0IGNhbnZhcyA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkNhbnZhcyk7XG4gICAgdGhpcy5zdGFydFNpemUgPSBjYW52YXMuZGVzaWduUmVzb2x1dGlvbjtcbiAgICB0aGlzLnNldEZsZXhpYmxlV2ViU2NyZWVuKCk7XG4gICAgY2Mudmlldy5zZXRSZXNpemVDYWxsYmFjaygoKSA9PiB7XG4gICAgICBpZiAodGhpcyAmJiB0aGlzLm5vZGUpIHRoaXMuc2V0RmxleGlibGVXZWJTY3JlZW4oKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBvbkRlc3Ryb3koKSB7XG4gICAgaWYgKHRoaXMuaXNXZWJCdWlsZCgpKSB7XG4gICAgICBjYy52aWV3LnNldFJlc2l6ZUNhbGxiYWNrKG51bGwpO1xuICAgIH1cbiAgICB0aGlzLnVuc2NoZWR1bGVBbGxDYWxsYmFja3MoKTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0RmxleGlibGVXZWJTY3JlZW4oKSB7XG4gICAgbGV0IHNpemUgPSBjYy52aWV3LmdldEZyYW1lU2l6ZSgpO1xuICAgIGxldCB3ID0gc2l6ZS53aWR0aDtcbiAgICBsZXQgaCA9IHNpemUuaGVpZ2h0O1xuICAgIGxldCByYXRpbyA9IHcgLyBoO1xuICAgIGxldCBmaXRIZWlnaHQgPSByYXRpbyA+IHRoaXMuZGVmYXVsdFJhdGlvO1xuICAgIGxldCBtYXggPSBmYWxzZTtcbiAgICBpZiAocmF0aW8gPiB0aGlzLmRlZmF1bHRSYXRpb01heCkge1xuICAgICAgcmF0aW8gPSB0aGlzLmRlZmF1bHRSYXRpb01heDtcbiAgICAgIG1heCA9IHRydWU7XG4gICAgfVxuXG4gICAgbGV0IG1pbiA9IGZhbHNlO1xuICAgIGlmIChyYXRpbyA8IHRoaXMuZGVmYXVsdFJhdGlvTWluKSB7XG4gICAgICByYXRpbyA9IHRoaXMuZGVmYXVsdFJhdGlvTWluO1xuICAgICAgbWluID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBsZXQgY2FudmFzID0gdGhpcy5nZXRDb21wb25lbnQoY2MuQ2FudmFzKTtcbiAgICBjYW52YXMuZml0V2lkdGggPSAhZml0SGVpZ2h0O1xuICAgIGNhbnZhcy5maXRIZWlnaHQgPSBmaXRIZWlnaHQ7XG5cbiAgICBpZiAoZml0SGVpZ2h0KSB7XG4gICAgICBpZiAodGhpcy5pc1dlYkJ1aWxkKCkgJiYgbWF4KSB7XG4gICAgICAgIGNhbnZhcy5kZXNpZ25SZXNvbHV0aW9uID0gbmV3IFNpemUodGhpcy5zdGFydFNpemUuaGVpZ2h0ICogdGhpcy5kZWZhdWx0UmF0aW9NYXgsIHRoaXMuc3RhcnRTaXplLmhlaWdodCk7XG4gICAgICAgIGNhbnZhcy5maXRXaWR0aCA9IHRydWU7XG4gICAgICAgIGNhbnZhcy5maXRIZWlnaHQgPSB0cnVlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY2FudmFzLmRlc2lnblJlc29sdXRpb24gPSBuZXcgU2l6ZSh0aGlzLnN0YXJ0U2l6ZS53aWR0aCwgdGhpcy5zdGFydFNpemUuaGVpZ2h0KTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHRoaXMuaXNXZWJCdWlsZCgpICYmIG1pbikge1xuICAgICAgICBjYW52YXMuZGVzaWduUmVzb2x1dGlvbiA9IG5ldyBTaXplKHRoaXMuc3RhcnRTaXplLndpZHRoLCB0aGlzLnN0YXJ0U2l6ZS53aWR0aCAvIHRoaXMuZGVmYXVsdFJhdGlvTWluKTtcbiAgICAgICAgY2FudmFzLmZpdFdpZHRoID0gdHJ1ZTtcbiAgICAgICAgY2FudmFzLmZpdEhlaWdodCA9IHRydWU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjYW52YXMuZGVzaWduUmVzb2x1dGlvbiA9IG5ldyBTaXplKHRoaXMuc3RhcnRTaXplLndpZHRoLCB0aGlzLnN0YXJ0U2l6ZS5oZWlnaHQpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcbiAgICAgIGNjLnN5c3RlbUV2ZW50LmVtaXQoRXZlbnRLZXkuT05fU0NSRUVOX0NIQU5HRV9TSVpFX0VWRU5UKVxuICAgIH0sIDEgLyA2MCk7XG4gIH1cblxuICBwcml2YXRlIGlzV2ViQnVpbGQoKSB7XG4gICAgcmV0dXJuIChjYy5zeXMucGxhdGZvcm0gPT0gY2Muc3lzLkRFU0tUT1BfQlJPV1NFUiB8fCBjYy5zeXMucGxhdGZvcm0gPT0gY2Muc3lzLk1PQklMRV9CUk9XU0VSKSAmJiBDQ19CVUlMRDtcbiAgfVxufVxuIl19