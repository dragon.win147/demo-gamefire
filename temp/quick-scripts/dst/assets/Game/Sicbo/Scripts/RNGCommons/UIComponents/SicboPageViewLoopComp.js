
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboPageViewLoopComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c5e374/lcNIqoGp3o2ZbSIf', 'SicboPageViewLoopComp');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboPageViewLoopComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboPageViewLoopComp = /** @class */ (function (_super) {
    __extends(SicboPageViewLoopComp, _super);
    function SicboPageViewLoopComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.content = null;
        _this.pages = [];
        _this.distanceBetweenPage = 677 + 16;
        _this.moveTime = 0.1;
        _this.currentPage = null;
        _this.idCurrentPage = 0;
        _this.xOriginalPageNode = 0;
        _this.isPageMoving = false;
        _this.nextPage = null;
        _this.nextID = 0;
        return _this;
    }
    SicboPageViewLoopComp.prototype.onLoad = function () {
        this.pages.forEach(function (page) {
            page.active = false;
        });
        this.currentPage = this.pages[0];
        this.idCurrentPage = 0;
        this.currentPage.active = true;
        // this.nextPage = this.pages[1];
        this.xOriginalPageNode = this.currentPage.x;
        // this.distanceBetweenPage += this.xOriginalPageNode;
        // this.nextPage.parent = this.currentPage;
        // this.nextPage.x = this.distanceBetweenPage;
        this.setUpOnTouchPage();
    };
    SicboPageViewLoopComp.prototype.setUpOnTouchPage = function () {
        var _this = this;
        this.pages.forEach(function (page) {
            // page.on(
            //     "touchstart",
            //     (e: EventTouch) => {
            //         if (this.isPageMoving) return;
            //         if (e.target.name == this.currentPage.name) {
            //             this.xPageNode = this.currentPage.x;
            //         }
            //     },
            //     this
            // );
            page.on("touchmove", function (e) {
                if (_this.isPageMoving)
                    return;
                if (e.target.name == _this.currentPage.name) {
                    _this.currentPage.x = _this.currentPage.x + e.getDeltaX();
                    if (_this.currentPage.x > _this.distanceBetweenPage + _this.xOriginalPageNode) {
                        _this.currentPage.x = _this.distanceBetweenPage + _this.xOriginalPageNode;
                    }
                    else if (_this.currentPage.x < -_this.distanceBetweenPage + _this.xOriginalPageNode) {
                        _this.currentPage.x = -_this.distanceBetweenPage + _this.xOriginalPageNode;
                    }
                    if (_this.currentPage.x > _this.xOriginalPageNode) {
                        _this.showNextByPage(true);
                    }
                    else {
                        _this.showNextByPage(false);
                    }
                }
            }, _this);
            page.on("touchcancel", function (e) {
                if (_this.isPageMoving)
                    return;
                if (e.target.name == _this.currentPage.name) {
                    //cc.log("this.current.x " + this.currentPage.x + " " + ((-this.distanceBetweenPage) / 2 + this.xOriginalPageNode));
                    if (_this.currentPage.x > _this.distanceBetweenPage / 2 + _this.xOriginalPageNode) {
                        _this.movePage(false);
                    }
                    else if (_this.currentPage.x < (-_this.distanceBetweenPage) / 2 + _this.xOriginalPageNode) {
                        _this.movePage(true);
                    }
                    else {
                        _this.moveBackToOriginal();
                    }
                }
            }, _this);
            page.on("touchend", function (e) {
                if (_this.isPageMoving)
                    return;
                if (e.target.name == _this.currentPage.name) {
                    //cc.log("this.current.x " + this.currentPage.x + " " + ((-this.distanceBetweenPage) / 2 + this.xOriginalPageNode));
                    if (_this.currentPage.x > _this.distanceBetweenPage / 2 + _this.xOriginalPageNode) {
                        _this.movePage(false);
                    }
                    else if (_this.currentPage.x < (-_this.distanceBetweenPage) / 2 + _this.xOriginalPageNode) {
                        _this.movePage(true);
                    }
                    else {
                        _this.moveBackToOriginal();
                    }
                }
            }, _this);
        });
    };
    SicboPageViewLoopComp.prototype.movePage = function (isMoveLeft) {
        var _this = this;
        this.isPageMoving = true;
        cc.Tween.stopAllByTarget(this.currentPage);
        cc.tween(this.currentPage)
            .to(this.moveTime, { x: isMoveLeft ? -this.distanceBetweenPage + this.xOriginalPageNode : this.distanceBetweenPage + this.xOriginalPageNode })
            .call(function () {
            _this.pages.forEach(function (page, index) {
                if (index != _this.idCurrentPage) {
                    page.parent = _this.content;
                    page.active = false;
                }
            });
            _this.idCurrentPage = _this.nextID;
            _this.nextPage.active = true;
            var tempPage = _this.currentPage;
            _this.currentPage = _this.nextPage;
            _this.nextPage = tempPage;
            _this.nextPage.parent = _this.currentPage;
            _this.currentPage.x = _this.xOriginalPageNode;
            _this.nextPage.x = _this.xOriginalPageNode + _this.distanceBetweenPage;
            _this.isPageMoving = false;
        })
            .start();
    };
    SicboPageViewLoopComp.prototype.moveToFirstPage = function () {
        var _this = this;
        this.pages.forEach(function (page) {
            page.parent = _this.content;
            page.active = false;
        });
        this.currentPage = this.pages[0];
        this.idCurrentPage = 0;
        this.currentPage.active = true;
        this.currentPage.x = 0;
        this.xOriginalPageNode = this.currentPage.x;
    };
    SicboPageViewLoopComp.prototype.moveBackToOriginal = function () {
        var _this = this;
        this.isPageMoving = true;
        cc.Tween.stopAllByTarget(this.currentPage);
        cc.tween(this.currentPage)
            .to(this.moveTime, { x: this.xOriginalPageNode })
            .call(function () {
            _this.isPageMoving = false;
        })
            .start();
    };
    SicboPageViewLoopComp.prototype.showNextByPage = function (isLeft) {
        var _this = this;
        if (isLeft === void 0) { isLeft = true; }
        this.pages.forEach(function (page, index) {
            if (index != _this.idCurrentPage) {
                page.parent = _this.content;
                page.active = false;
            }
        });
        if (isLeft) {
            var id = this.idCurrentPage - 1;
            if (id < 0) {
                id += this.pages.length;
            }
            this.nextID = id;
            this.pages[id].parent = this.currentPage;
            this.pages[id].x = -this.distanceBetweenPage;
            this.pages[id].active = true;
            this.nextPage = this.pages[id];
        }
        else {
            var id = this.idCurrentPage + 1;
            if (id > this.pages.length - 1) {
                id -= this.pages.length;
            }
            this.nextID = id;
            this.pages[id].parent = this.currentPage;
            this.pages[id].x = this.distanceBetweenPage;
            this.pages[id].active = true;
            this.nextPage = this.pages[id];
        }
    };
    SicboPageViewLoopComp.prototype.onNextPage = function () {
        this.showNextByPage(false);
        this.movePage(true);
    };
    SicboPageViewLoopComp.prototype.onPrePage = function () {
        this.showNextByPage(true);
        this.movePage(false);
    };
    __decorate([
        property(cc.Node)
    ], SicboPageViewLoopComp.prototype, "content", void 0);
    __decorate([
        property([cc.Node])
    ], SicboPageViewLoopComp.prototype, "pages", void 0);
    __decorate([
        property
    ], SicboPageViewLoopComp.prototype, "distanceBetweenPage", void 0);
    __decorate([
        property
    ], SicboPageViewLoopComp.prototype, "moveTime", void 0);
    SicboPageViewLoopComp = __decorate([
        ccclass
    ], SicboPageViewLoopComp);
    return SicboPageViewLoopComp;
}(cc.Component));
exports.default = SicboPageViewLoopComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvVUlDb21wb25lbnRzL1NpY2JvUGFnZVZpZXdMb29wQ29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFPTSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFtRCx5Q0FBWTtJQUEvRDtRQUFBLHFFQTBNQztRQXZNVyxhQUFPLEdBQVksSUFBSSxDQUFDO1FBRXhCLFdBQUssR0FBYyxFQUFFLENBQUM7UUFHdEIseUJBQW1CLEdBQVcsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUV2QyxjQUFRLEdBQVcsR0FBRyxDQUFDO1FBRXZCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBQzVCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLHVCQUFpQixHQUFXLENBQUMsQ0FBQztRQUM5QixrQkFBWSxHQUFZLEtBQUssQ0FBQztRQUM5QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBQ3pCLFlBQU0sR0FBVyxDQUFDLENBQUM7O0lBeUwvQixDQUFDO0lBdkxHLHNDQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7WUFDcEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUE7UUFDRixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQy9CLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDNUMsc0RBQXNEO1FBQ3RELDJDQUEyQztRQUMzQyw4Q0FBOEM7UUFDOUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELGdEQUFnQixHQUFoQjtRQUFBLGlCQXdFQztRQXZFRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7WUFDcEIsV0FBVztZQUNYLG9CQUFvQjtZQUNwQiwyQkFBMkI7WUFDM0IseUNBQXlDO1lBQ3pDLHdEQUF3RDtZQUN4RCxtREFBbUQ7WUFDbkQsWUFBWTtZQUNaLFNBQVM7WUFDVCxXQUFXO1lBQ1gsS0FBSztZQUVMLElBQUksQ0FBQyxFQUFFLENBQ0gsV0FBVyxFQUNYLFVBQUMsQ0FBYTtnQkFDVixJQUFJLEtBQUksQ0FBQyxZQUFZO29CQUFFLE9BQU87Z0JBQzlCLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUU7b0JBQ3hDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFDeEQsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxLQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixFQUFFO3dCQUN4RSxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxLQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDO3FCQUMxRTt5QkFBTSxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsRUFBRTt3QkFDaEYsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDO3FCQUMzRTtvQkFDRCxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsRUFBRTt3QkFDN0MsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDN0I7eUJBQU07d0JBQ0gsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDOUI7aUJBQ0o7WUFFTCxDQUFDLEVBQ0QsS0FBSSxDQUNQLENBQUM7WUFFRixJQUFJLENBQUMsRUFBRSxDQUNILGFBQWEsRUFDYixVQUFDLENBQXNCO2dCQUNuQixJQUFJLEtBQUksQ0FBQyxZQUFZO29CQUFFLE9BQU87Z0JBQzlCLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUU7b0JBQ3hDLG9IQUFvSDtvQkFDcEgsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxLQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsRUFBRTt3QkFDNUUsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDeEI7eUJBQU0sSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsRUFBRTt3QkFDdEYsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDdkI7eUJBQU07d0JBQ0gsS0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7cUJBQzdCO2lCQUNKO1lBQ0wsQ0FBQyxFQUNELEtBQUksQ0FDUCxDQUFDO1lBRUYsSUFBSSxDQUFDLEVBQUUsQ0FDSCxVQUFVLEVBQ1YsVUFBQyxDQUFzQjtnQkFDbkIsSUFBSSxLQUFJLENBQUMsWUFBWTtvQkFBRSxPQUFPO2dCQUM5QixJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFO29CQUN4QyxvSEFBb0g7b0JBQ3BILElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixHQUFHLENBQUMsR0FBRyxLQUFJLENBQUMsaUJBQWlCLEVBQUU7d0JBQzVFLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQ3hCO3lCQUFNLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFJLENBQUMsaUJBQWlCLEVBQUU7d0JBQ3RGLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3ZCO3lCQUFNO3dCQUNILEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO3FCQUM3QjtpQkFDSjtZQUNMLENBQUMsRUFDRCxLQUFJLENBQ1AsQ0FBQztRQUNOLENBQUMsQ0FBQyxDQUFBO0lBRU4sQ0FBQztJQUNPLHdDQUFRLEdBQWhCLFVBQWlCLFVBQW1CO1FBQXBDLGlCQTJCQztRQTFCRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDM0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO2FBQ3JCLEVBQUUsQ0FDQyxJQUFJLENBQUMsUUFBUSxFQUNiLEVBQUUsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQzdIO2FBQ0EsSUFBSSxDQUFDO1lBQ0YsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSztnQkFDM0IsSUFBSSxLQUFLLElBQUksS0FBSSxDQUFDLGFBQWEsRUFBRTtvQkFDN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDO29CQUMzQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztpQkFDdkI7WUFFTCxDQUFDLENBQUMsQ0FBQztZQUNILEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQztZQUNqQyxLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDNUIsSUFBSSxRQUFRLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQztZQUNoQyxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUM7WUFDakMsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7WUFDekIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQztZQUN4QyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUM7WUFDNUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQztZQUNwRSxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUM5QixDQUFDLENBQUM7YUFDRCxLQUFLLEVBQUUsQ0FBQztJQUNqQixDQUFDO0lBRUQsK0NBQWUsR0FBZjtRQUFBLGlCQVVDO1FBVEcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJO1lBQ3BCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQztZQUMzQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRU8sa0RBQWtCLEdBQTFCO1FBQUEsaUJBYUM7UUFaRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDM0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO2FBQ3JCLEVBQUUsQ0FDQyxJQUFJLENBQUMsUUFBUSxFQUNiLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUNoQzthQUNBLElBQUksQ0FBQztZQUNGLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzlCLENBQUMsQ0FBQzthQUNELEtBQUssRUFBRSxDQUFDO0lBRWpCLENBQUM7SUFFTyw4Q0FBYyxHQUF0QixVQUF1QixNQUFzQjtRQUE3QyxpQkE4QkM7UUE5QnNCLHVCQUFBLEVBQUEsYUFBc0I7UUFDekMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSztZQUMzQixJQUFJLEtBQUssSUFBSSxLQUFJLENBQUMsYUFBYSxFQUFFO2dCQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2FBQ3ZCO1FBRUwsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLE1BQU0sRUFBRTtZQUNSLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1lBQ2hDLElBQUksRUFBRSxHQUFHLENBQUMsRUFBRTtnQkFDUixFQUFFLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7YUFDM0I7WUFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1lBQzdDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDbEM7YUFBTTtZQUNILElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1lBQ2hDLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDNUIsRUFBRSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO2FBQzNCO1lBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7WUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUN6QyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7WUFDNUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzdCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNsQztJQUNMLENBQUM7SUFDTSwwQ0FBVSxHQUFqQjtRQUNJLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBQ00seUNBQVMsR0FBaEI7UUFDSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekIsQ0FBQztJQXRNRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzBEQUNjO0lBRWhDO1FBREMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO3dEQUNVO0lBRzlCO1FBREMsUUFBUTtzRUFDc0M7SUFFL0M7UUFEQyxRQUFROzJEQUNzQjtJQVZkLHFCQUFxQjtRQUR6QyxPQUFPO09BQ2EscUJBQXFCLENBME16QztJQUFELDRCQUFDO0NBMU1ELEFBME1DLENBMU1rRCxFQUFFLENBQUMsU0FBUyxHQTBNOUQ7a0JBMU1vQixxQkFBcUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcbmltcG9ydCBFdmVudFRvdWNoID0gY2MuRXZlbnQuRXZlbnRUb3VjaDtcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1BhZ2VWaWV3TG9vcENvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgcHJpdmF0ZSBjb250ZW50OiBjYy5Ob2RlID0gbnVsbDtcbiAgICBAcHJvcGVydHkoW2NjLk5vZGVdKVxuICAgIHByaXZhdGUgcGFnZXM6IGNjLk5vZGVbXSA9IFtdO1xuXG4gICAgQHByb3BlcnR5XG4gICAgcHJpdmF0ZSBkaXN0YW5jZUJldHdlZW5QYWdlOiBudW1iZXIgPSA2NzcgKyAxNjtcbiAgICBAcHJvcGVydHlcbiAgICBwcml2YXRlIG1vdmVUaW1lOiBudW1iZXIgPSAwLjE7XG5cbiAgICBwcml2YXRlIGN1cnJlbnRQYWdlOiBjYy5Ob2RlID0gbnVsbDtcbiAgICBwcml2YXRlIGlkQ3VycmVudFBhZ2U6IG51bWJlciA9IDA7XG4gICAgcHJpdmF0ZSB4T3JpZ2luYWxQYWdlTm9kZTogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIGlzUGFnZU1vdmluZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgbmV4dFBhZ2U6IGNjLk5vZGUgPSBudWxsO1xuICAgIHByaXZhdGUgbmV4dElEOiBudW1iZXIgPSAwO1xuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICB0aGlzLnBhZ2VzLmZvckVhY2goKHBhZ2UpID0+IHtcbiAgICAgICAgICAgIHBhZ2UuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIH0pXG4gICAgICAgIHRoaXMuY3VycmVudFBhZ2UgPSB0aGlzLnBhZ2VzWzBdO1xuICAgICAgICB0aGlzLmlkQ3VycmVudFBhZ2UgPSAwO1xuICAgICAgICB0aGlzLmN1cnJlbnRQYWdlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIC8vIHRoaXMubmV4dFBhZ2UgPSB0aGlzLnBhZ2VzWzFdO1xuICAgICAgICB0aGlzLnhPcmlnaW5hbFBhZ2VOb2RlID0gdGhpcy5jdXJyZW50UGFnZS54O1xuICAgICAgICAvLyB0aGlzLmRpc3RhbmNlQmV0d2VlblBhZ2UgKz0gdGhpcy54T3JpZ2luYWxQYWdlTm9kZTtcbiAgICAgICAgLy8gdGhpcy5uZXh0UGFnZS5wYXJlbnQgPSB0aGlzLmN1cnJlbnRQYWdlO1xuICAgICAgICAvLyB0aGlzLm5leHRQYWdlLnggPSB0aGlzLmRpc3RhbmNlQmV0d2VlblBhZ2U7XG4gICAgICAgIHRoaXMuc2V0VXBPblRvdWNoUGFnZSgpO1xuICAgIH1cblxuICAgIHNldFVwT25Ub3VjaFBhZ2UoKSB7XG4gICAgICAgIHRoaXMucGFnZXMuZm9yRWFjaCgocGFnZSkgPT4ge1xuICAgICAgICAgICAgLy8gcGFnZS5vbihcbiAgICAgICAgICAgIC8vICAgICBcInRvdWNoc3RhcnRcIixcbiAgICAgICAgICAgIC8vICAgICAoZTogRXZlbnRUb3VjaCkgPT4ge1xuICAgICAgICAgICAgLy8gICAgICAgICBpZiAodGhpcy5pc1BhZ2VNb3ZpbmcpIHJldHVybjtcbiAgICAgICAgICAgIC8vICAgICAgICAgaWYgKGUudGFyZ2V0Lm5hbWUgPT0gdGhpcy5jdXJyZW50UGFnZS5uYW1lKSB7XG4gICAgICAgICAgICAvLyAgICAgICAgICAgICB0aGlzLnhQYWdlTm9kZSA9IHRoaXMuY3VycmVudFBhZ2UueDtcbiAgICAgICAgICAgIC8vICAgICAgICAgfVxuICAgICAgICAgICAgLy8gICAgIH0sXG4gICAgICAgICAgICAvLyAgICAgdGhpc1xuICAgICAgICAgICAgLy8gKTtcblxuICAgICAgICAgICAgcGFnZS5vbihcbiAgICAgICAgICAgICAgICBcInRvdWNobW92ZVwiLFxuICAgICAgICAgICAgICAgIChlOiBFdmVudFRvdWNoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzUGFnZU1vdmluZykgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZS50YXJnZXQubmFtZSA9PSB0aGlzLmN1cnJlbnRQYWdlLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFBhZ2UueCA9IHRoaXMuY3VycmVudFBhZ2UueCArIGUuZ2V0RGVsdGFYKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UGFnZS54ID4gdGhpcy5kaXN0YW5jZUJldHdlZW5QYWdlICsgdGhpcy54T3JpZ2luYWxQYWdlTm9kZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFBhZ2UueCA9IHRoaXMuZGlzdGFuY2VCZXR3ZWVuUGFnZSArIHRoaXMueE9yaWdpbmFsUGFnZU5vZGU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuY3VycmVudFBhZ2UueCA8IC10aGlzLmRpc3RhbmNlQmV0d2VlblBhZ2UgKyB0aGlzLnhPcmlnaW5hbFBhZ2VOb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UGFnZS54ID0gLXRoaXMuZGlzdGFuY2VCZXR3ZWVuUGFnZSArIHRoaXMueE9yaWdpbmFsUGFnZU5vZGU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UGFnZS54ID4gdGhpcy54T3JpZ2luYWxQYWdlTm9kZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hvd05leHRCeVBhZ2UodHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hvd05leHRCeVBhZ2UoZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHRoaXNcbiAgICAgICAgICAgICk7XG5cbiAgICAgICAgICAgIHBhZ2Uub24oXG4gICAgICAgICAgICAgICAgXCJ0b3VjaGNhbmNlbFwiLFxuICAgICAgICAgICAgICAgIChlOiBjYy5FdmVudC5FdmVudFRvdWNoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzUGFnZU1vdmluZykgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZS50YXJnZXQubmFtZSA9PSB0aGlzLmN1cnJlbnRQYWdlLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vY2MubG9nKFwidGhpcy5jdXJyZW50LnggXCIgKyB0aGlzLmN1cnJlbnRQYWdlLnggKyBcIiBcIiArICgoLXRoaXMuZGlzdGFuY2VCZXR3ZWVuUGFnZSkgLyAyICsgdGhpcy54T3JpZ2luYWxQYWdlTm9kZSkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVudFBhZ2UueCA+IHRoaXMuZGlzdGFuY2VCZXR3ZWVuUGFnZSAvIDIgKyB0aGlzLnhPcmlnaW5hbFBhZ2VOb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tb3ZlUGFnZShmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuY3VycmVudFBhZ2UueCA8ICgtdGhpcy5kaXN0YW5jZUJldHdlZW5QYWdlKSAvIDIgKyB0aGlzLnhPcmlnaW5hbFBhZ2VOb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tb3ZlUGFnZSh0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tb3ZlQmFja1RvT3JpZ2luYWwoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgdGhpc1xuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgcGFnZS5vbihcbiAgICAgICAgICAgICAgICBcInRvdWNoZW5kXCIsXG4gICAgICAgICAgICAgICAgKGU6IGNjLkV2ZW50LkV2ZW50VG91Y2gpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNQYWdlTW92aW5nKSByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIGlmIChlLnRhcmdldC5uYW1lID09IHRoaXMuY3VycmVudFBhZ2UubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9jYy5sb2coXCJ0aGlzLmN1cnJlbnQueCBcIiArIHRoaXMuY3VycmVudFBhZ2UueCArIFwiIFwiICsgKCgtdGhpcy5kaXN0YW5jZUJldHdlZW5QYWdlKSAvIDIgKyB0aGlzLnhPcmlnaW5hbFBhZ2VOb2RlKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UGFnZS54ID4gdGhpcy5kaXN0YW5jZUJldHdlZW5QYWdlIC8gMiArIHRoaXMueE9yaWdpbmFsUGFnZU5vZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vdmVQYWdlKGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5jdXJyZW50UGFnZS54IDwgKC10aGlzLmRpc3RhbmNlQmV0d2VlblBhZ2UpIC8gMiArIHRoaXMueE9yaWdpbmFsUGFnZU5vZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vdmVQYWdlKHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vdmVCYWNrVG9PcmlnaW5hbCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB0aGlzXG4gICAgICAgICAgICApO1xuICAgICAgICB9KVxuXG4gICAgfVxuICAgIHByaXZhdGUgbW92ZVBhZ2UoaXNNb3ZlTGVmdDogYm9vbGVhbikge1xuICAgICAgICB0aGlzLmlzUGFnZU1vdmluZyA9IHRydWU7XG4gICAgICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLmN1cnJlbnRQYWdlKTtcbiAgICAgICAgY2MudHdlZW4odGhpcy5jdXJyZW50UGFnZSlcbiAgICAgICAgICAgIC50byhcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVUaW1lLFxuICAgICAgICAgICAgICAgIHsgeDogaXNNb3ZlTGVmdCA/IC10aGlzLmRpc3RhbmNlQmV0d2VlblBhZ2UgKyB0aGlzLnhPcmlnaW5hbFBhZ2VOb2RlIDogdGhpcy5kaXN0YW5jZUJldHdlZW5QYWdlICsgdGhpcy54T3JpZ2luYWxQYWdlTm9kZSB9XG4gICAgICAgICAgICApXG4gICAgICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5wYWdlcy5mb3JFYWNoKChwYWdlLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAoaW5kZXggIT0gdGhpcy5pZEN1cnJlbnRQYWdlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlLnBhcmVudCA9IHRoaXMuY29udGVudDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2UuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHRoaXMuaWRDdXJyZW50UGFnZSA9IHRoaXMubmV4dElEO1xuICAgICAgICAgICAgICAgIHRoaXMubmV4dFBhZ2UuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBsZXQgdGVtcFBhZ2UgPSB0aGlzLmN1cnJlbnRQYWdlO1xuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFBhZ2UgPSB0aGlzLm5leHRQYWdlO1xuICAgICAgICAgICAgICAgIHRoaXMubmV4dFBhZ2UgPSB0ZW1wUGFnZTtcbiAgICAgICAgICAgICAgICB0aGlzLm5leHRQYWdlLnBhcmVudCA9IHRoaXMuY3VycmVudFBhZ2U7XG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UGFnZS54ID0gdGhpcy54T3JpZ2luYWxQYWdlTm9kZTtcbiAgICAgICAgICAgICAgICB0aGlzLm5leHRQYWdlLnggPSB0aGlzLnhPcmlnaW5hbFBhZ2VOb2RlICsgdGhpcy5kaXN0YW5jZUJldHdlZW5QYWdlO1xuICAgICAgICAgICAgICAgIHRoaXMuaXNQYWdlTW92aW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnN0YXJ0KCk7XG4gICAgfVxuXG4gICAgbW92ZVRvRmlyc3RQYWdlKCl7XG4gICAgICAgIHRoaXMucGFnZXMuZm9yRWFjaCgocGFnZSkgPT4ge1xuICAgICAgICAgICAgcGFnZS5wYXJlbnQgPSB0aGlzLmNvbnRlbnQ7XG4gICAgICAgICAgICBwYWdlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9KVxuICAgICAgICB0aGlzLmN1cnJlbnRQYWdlID0gdGhpcy5wYWdlc1swXTtcbiAgICAgICAgdGhpcy5pZEN1cnJlbnRQYWdlID0gMDtcbiAgICAgICAgdGhpcy5jdXJyZW50UGFnZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB0aGlzLmN1cnJlbnRQYWdlLnggPSAwO1xuICAgICAgICB0aGlzLnhPcmlnaW5hbFBhZ2VOb2RlID0gdGhpcy5jdXJyZW50UGFnZS54O1xuICAgIH1cblxuICAgIHByaXZhdGUgbW92ZUJhY2tUb09yaWdpbmFsKCkge1xuICAgICAgICB0aGlzLmlzUGFnZU1vdmluZyA9IHRydWU7XG4gICAgICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLmN1cnJlbnRQYWdlKTtcbiAgICAgICAgY2MudHdlZW4odGhpcy5jdXJyZW50UGFnZSlcbiAgICAgICAgICAgIC50byhcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVUaW1lLFxuICAgICAgICAgICAgICAgIHsgeDogdGhpcy54T3JpZ2luYWxQYWdlTm9kZSB9XG4gICAgICAgICAgICApXG4gICAgICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5pc1BhZ2VNb3ZpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuc3RhcnQoKTtcblxuICAgIH1cblxuICAgIHByaXZhdGUgc2hvd05leHRCeVBhZ2UoaXNMZWZ0OiBib29sZWFuID0gdHJ1ZSkge1xuICAgICAgICB0aGlzLnBhZ2VzLmZvckVhY2goKHBhZ2UsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICBpZiAoaW5kZXggIT0gdGhpcy5pZEN1cnJlbnRQYWdlKSB7XG4gICAgICAgICAgICAgICAgcGFnZS5wYXJlbnQgPSB0aGlzLmNvbnRlbnQ7XG4gICAgICAgICAgICAgICAgcGFnZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9KTtcblxuICAgICAgICBpZiAoaXNMZWZ0KSB7XG4gICAgICAgICAgICBsZXQgaWQgPSB0aGlzLmlkQ3VycmVudFBhZ2UgLSAxO1xuICAgICAgICAgICAgaWYgKGlkIDwgMCkge1xuICAgICAgICAgICAgICAgIGlkICs9IHRoaXMucGFnZXMubGVuZ3RoO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5uZXh0SUQgPSBpZDtcbiAgICAgICAgICAgIHRoaXMucGFnZXNbaWRdLnBhcmVudCA9IHRoaXMuY3VycmVudFBhZ2U7XG4gICAgICAgICAgICB0aGlzLnBhZ2VzW2lkXS54ID0gLXRoaXMuZGlzdGFuY2VCZXR3ZWVuUGFnZTtcbiAgICAgICAgICAgIHRoaXMucGFnZXNbaWRdLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLm5leHRQYWdlID0gdGhpcy5wYWdlc1tpZF07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBsZXQgaWQgPSB0aGlzLmlkQ3VycmVudFBhZ2UgKyAxO1xuICAgICAgICAgICAgaWYgKGlkID4gdGhpcy5wYWdlcy5sZW5ndGggLSAxKSB7XG4gICAgICAgICAgICAgICAgaWQgLT0gdGhpcy5wYWdlcy5sZW5ndGg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLm5leHRJRCA9IGlkO1xuICAgICAgICAgICAgdGhpcy5wYWdlc1tpZF0ucGFyZW50ID0gdGhpcy5jdXJyZW50UGFnZTtcbiAgICAgICAgICAgIHRoaXMucGFnZXNbaWRdLnggPSB0aGlzLmRpc3RhbmNlQmV0d2VlblBhZ2U7XG4gICAgICAgICAgICB0aGlzLnBhZ2VzW2lkXS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5uZXh0UGFnZSA9IHRoaXMucGFnZXNbaWRdO1xuICAgICAgICB9XG4gICAgfVxuICAgIHB1YmxpYyBvbk5leHRQYWdlKCkge1xuICAgICAgICB0aGlzLnNob3dOZXh0QnlQYWdlKGZhbHNlKTtcbiAgICAgICAgdGhpcy5tb3ZlUGFnZSh0cnVlKTtcbiAgICB9XG4gICAgcHVibGljIG9uUHJlUGFnZSgpIHtcbiAgICAgICAgdGhpcy5zaG93TmV4dEJ5UGFnZSh0cnVlKTtcbiAgICAgICAgdGhpcy5tb3ZlUGFnZShmYWxzZSk7XG4gICAgfVxufVxuIl19