
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboLoading.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c2455M+uvpIcpqPa+DVali+', 'SicboLoading');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboLoading.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboLoading = /** @class */ (function (_super) {
    __extends(SicboLoading, _super);
    function SicboLoading() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.activesOnStart = [];
        _this.deactivesOnStart = [];
        _this.activesOnFinish = [];
        _this.deactivesOnFinish = [];
        _this.onStartEvents = [];
        _this.onFinishEvents = [];
        return _this;
    }
    SicboLoading.prototype.startLoading = function () {
        this.setActiveNodes(this.activesOnStart, true);
        this.setActiveNodes(this.deactivesOnStart, false);
        this.runEvents(this.onStartEvents);
    };
    SicboLoading.prototype.finishLoading = function () {
        this.setActiveNodes(this.activesOnFinish, true);
        this.setActiveNodes(this.deactivesOnFinish, false);
        this.runEvents(this.onFinishEvents);
    };
    SicboLoading.prototype.setActiveNodes = function (nodes, isActive) {
        if (isActive === void 0) { isActive = true; }
        for (var i = 0; i < nodes.length; i++) {
            nodes[i].active = isActive;
        }
    };
    SicboLoading.prototype.runEvents = function (events) {
        for (var i = 0; i < events.length; i++) {
            events[i].emit([events[i].customEventData]);
        }
    };
    __decorate([
        property([cc.Node])
    ], SicboLoading.prototype, "activesOnStart", void 0);
    __decorate([
        property([cc.Node])
    ], SicboLoading.prototype, "deactivesOnStart", void 0);
    __decorate([
        property([cc.Node])
    ], SicboLoading.prototype, "activesOnFinish", void 0);
    __decorate([
        property([cc.Node])
    ], SicboLoading.prototype, "deactivesOnFinish", void 0);
    __decorate([
        property([cc.Component.EventHandler])
    ], SicboLoading.prototype, "onStartEvents", void 0);
    __decorate([
        property([cc.Component.EventHandler])
    ], SicboLoading.prototype, "onFinishEvents", void 0);
    SicboLoading = __decorate([
        ccclass
    ], SicboLoading);
    return SicboLoading;
}(cc.Component));
exports.default = SicboLoading;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvVUlDb21wb25lbnRzL1NpY2JvTG9hZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUU1RSxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUEwQyxnQ0FBWTtJQUF0RDtRQUFBLHFFQTRDQztRQXpDRyxvQkFBYyxHQUFjLEVBQUUsQ0FBQztRQUcvQixzQkFBZ0IsR0FBYyxFQUFFLENBQUM7UUFJakMscUJBQWUsR0FBYyxFQUFFLENBQUM7UUFHaEMsdUJBQWlCLEdBQWMsRUFBRSxDQUFDO1FBR2xDLG1CQUFhLEdBQWdDLEVBQUUsQ0FBQztRQUdoRCxvQkFBYyxHQUFnQyxFQUFFLENBQUM7O0lBeUJyRCxDQUFDO0lBdkJHLG1DQUFZLEdBQVo7UUFDSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELG9DQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVELHFDQUFjLEdBQWQsVUFBZSxLQUFnQixFQUFFLFFBQXdCO1FBQXhCLHlCQUFBLEVBQUEsZUFBd0I7UUFDckQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDbkMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUM7U0FDOUI7SUFDTCxDQUFDO0lBRUQsZ0NBQVMsR0FBVCxVQUFVLE1BQW1DO1FBQ3pDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3BDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztTQUMvQztJQUNMLENBQUM7SUF4Q0Q7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7d0RBQ1c7SUFHL0I7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7MERBQ2E7SUFJakM7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7eURBQ1k7SUFHaEM7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7MkRBQ2M7SUFHbEM7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO3VEQUNVO0lBR2hEO1FBREMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQzt3REFDVztJQW5CaEMsWUFBWTtRQURoQyxPQUFPO09BQ2EsWUFBWSxDQTRDaEM7SUFBRCxtQkFBQztDQTVDRCxBQTRDQyxDQTVDeUMsRUFBRSxDQUFDLFNBQVMsR0E0Q3JEO2tCQTVDb0IsWUFBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvTG9hZGluZyBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBAcHJvcGVydHkoW2NjLk5vZGVdKVxuICAgIGFjdGl2ZXNPblN0YXJ0OiBjYy5Ob2RlW10gPSBbXTtcblxuICAgIEBwcm9wZXJ0eShbY2MuTm9kZV0pXG4gICAgZGVhY3RpdmVzT25TdGFydDogY2MuTm9kZVtdID0gW107XG4gICBcblxuICAgIEBwcm9wZXJ0eShbY2MuTm9kZV0pXG4gICAgYWN0aXZlc09uRmluaXNoOiBjYy5Ob2RlW10gPSBbXTtcblxuICAgIEBwcm9wZXJ0eShbY2MuTm9kZV0pXG4gICAgZGVhY3RpdmVzT25GaW5pc2g6IGNjLk5vZGVbXSA9IFtdO1xuXG4gICAgQHByb3BlcnR5KFtjYy5Db21wb25lbnQuRXZlbnRIYW5kbGVyXSlcbiAgICBvblN0YXJ0RXZlbnRzOiBjYy5Db21wb25lbnQuRXZlbnRIYW5kbGVyW10gPSBbXTtcblxuICAgIEBwcm9wZXJ0eShbY2MuQ29tcG9uZW50LkV2ZW50SGFuZGxlcl0pXG4gICAgb25GaW5pc2hFdmVudHM6IGNjLkNvbXBvbmVudC5FdmVudEhhbmRsZXJbXSA9IFtdOyBcbiAgICBcbiAgICBzdGFydExvYWRpbmcoKXtcbiAgICAgICAgdGhpcy5zZXRBY3RpdmVOb2Rlcyh0aGlzLmFjdGl2ZXNPblN0YXJ0LCB0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRBY3RpdmVOb2Rlcyh0aGlzLmRlYWN0aXZlc09uU3RhcnQsIGZhbHNlKTtcbiAgICAgICAgdGhpcy5ydW5FdmVudHModGhpcy5vblN0YXJ0RXZlbnRzKTtcbiAgICB9XG5cbiAgICBmaW5pc2hMb2FkaW5nKCl7XG4gICAgICAgIHRoaXMuc2V0QWN0aXZlTm9kZXModGhpcy5hY3RpdmVzT25GaW5pc2gsIHRydWUpO1xuICAgICAgICB0aGlzLnNldEFjdGl2ZU5vZGVzKHRoaXMuZGVhY3RpdmVzT25GaW5pc2gsIGZhbHNlKTtcbiAgICAgICAgdGhpcy5ydW5FdmVudHModGhpcy5vbkZpbmlzaEV2ZW50cyk7XG4gICAgfVxuXG4gICAgc2V0QWN0aXZlTm9kZXMobm9kZXM6IGNjLk5vZGVbXSwgaXNBY3RpdmU6IGJvb2xlYW4gPSB0cnVlKXtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBub2Rlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbm9kZXNbaV0uYWN0aXZlID0gaXNBY3RpdmU7ICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBydW5FdmVudHMoZXZlbnRzOiBjYy5Db21wb25lbnQuRXZlbnRIYW5kbGVyW10pe1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGV2ZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgZXZlbnRzW2ldLmVtaXQoW2V2ZW50c1tpXS5jdXN0b21FdmVudERhdGFdKTsgICAgICAgICAgICBcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==