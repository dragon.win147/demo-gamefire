
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboScrollViewWithButton.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ae64eHPPBxHrqdoGoFIjgEU', 'SicboScrollViewWithButton');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboScrollViewWithButton.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ScrollEvent = cc.ScrollView.EventType;
var SicboScrollViewWithButton = /** @class */ (function (_super) {
    __extends(SicboScrollViewWithButton, _super);
    function SicboScrollViewWithButton() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scrollView = null;
        _this.preButton = null;
        _this.nextButton = null;
        _this.leftThreshold = 1;
        _this.rightThreshold = 2;
        return _this;
        //!SECTION
    }
    SicboScrollViewWithButton.prototype.onLoad = function () {
        this.scrollView = this.getComponent(cc.ScrollView);
        this.init();
    };
    //SECTION Scroll Pre/next button
    SicboScrollViewWithButton.prototype.init = function () {
        var scrollViewEventHandler = new cc.Component.EventHandler();
        scrollViewEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        scrollViewEventHandler.component = "SicboScrollViewWithButton"; // This is the code file name
        scrollViewEventHandler.handler = "callback";
        // scrollViewEventHandler.customEventData = "";    
        this.scrollView.scrollEvents.push(scrollViewEventHandler);
        this.checkScrollViewButton();
    };
    SicboScrollViewWithButton.prototype.callback = function (scrollView, eventType, customEventData) {
        switch (eventType) {
            case cc.ScrollView.EventType.SCROLL_ENDED:
                this.checkScrollViewButton();
                break;
        }
    };
    SicboScrollViewWithButton.prototype.checkScrollViewButton = function () {
        var offSetX = this.scrollView.getScrollOffset().x;
        var halfContentW = this.scrollView.content.getContentSize().width / 2;
        //cc.log("SCROLL END", offSetX, halfContentW);
        var isLeft = Math.abs(offSetX) <= this.leftThreshold;
        var isRight = offSetX <= -halfContentW + this.rightThreshold;
        this.preButton.interactable = !isLeft;
        this.nextButton.interactable = !isRight;
    };
    __decorate([
        property(cc.Button)
    ], SicboScrollViewWithButton.prototype, "preButton", void 0);
    __decorate([
        property(cc.Button)
    ], SicboScrollViewWithButton.prototype, "nextButton", void 0);
    __decorate([
        property()
    ], SicboScrollViewWithButton.prototype, "leftThreshold", void 0);
    __decorate([
        property()
    ], SicboScrollViewWithButton.prototype, "rightThreshold", void 0);
    SicboScrollViewWithButton = __decorate([
        ccclass
    ], SicboScrollViewWithButton);
    return SicboScrollViewWithButton;
}(cc.Component));
exports.default = SicboScrollViewWithButton;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvVUlDb21wb25lbnRzL1NpY2JvU2Nyb2xsVmlld1dpdGhCdXR0b24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFNUUsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFDMUMsSUFBTSxXQUFXLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7QUFFNUM7SUFBdUQsNkNBQVk7SUFBbkU7UUFBQSxxRUF1REM7UUF0REMsZ0JBQVUsR0FBaUIsSUFBSSxDQUFDO1FBR2hDLGVBQVMsR0FBYyxJQUFJLENBQUM7UUFHNUIsZ0JBQVUsR0FBYyxJQUFJLENBQUM7UUFHN0IsbUJBQWEsR0FBVyxDQUFDLENBQUM7UUFHMUIsb0JBQWMsR0FBVyxDQUFDLENBQUM7O1FBeUMzQixVQUFVO0lBQ1osQ0FBQztJQXhDQywwQ0FBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUVELGdDQUFnQztJQUNoQyx3Q0FBSSxHQUFKO1FBQ0UsSUFBSSxzQkFBc0IsR0FBRyxJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDN0Qsc0JBQXNCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQywyRUFBMkU7UUFDdEgsc0JBQXNCLENBQUMsU0FBUyxHQUFHLDJCQUEyQixDQUFDLENBQUMsNkJBQTZCO1FBQzdGLHNCQUFzQixDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUM7UUFDNUMsbURBQW1EO1FBRW5ELElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFHQyw0Q0FBUSxHQUFSLFVBQVMsVUFBeUIsRUFBRSxTQUFrQyxFQUFFLGVBQWU7UUFDckYsUUFBUSxTQUFTLEVBQUU7WUFDakIsS0FBSyxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxZQUFZO2dCQUN2QyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztnQkFDN0IsTUFBTTtTQUNUO0lBQ0gsQ0FBQztJQUVELHlEQUFxQixHQUFyQjtRQUNFLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2xELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDLEtBQUssR0FBQyxDQUFDLENBQUM7UUFFcEUsOENBQThDO1FBQzlDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUNyRCxJQUFJLE9BQU8sR0FBRyxPQUFPLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUU3RCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUV0QyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksR0FBRyxDQUFDLE9BQU8sQ0FBQztJQUUxQyxDQUFDO0lBakREO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7Z0VBQ1E7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztpRUFDUztJQUc3QjtRQURDLFFBQVEsRUFBRTtvRUFDZTtJQUcxQjtRQURDLFFBQVEsRUFBRTtxRUFDZ0I7SUFiUix5QkFBeUI7UUFEN0MsT0FBTztPQUNhLHlCQUF5QixDQXVEN0M7SUFBRCxnQ0FBQztDQXZERCxBQXVEQyxDQXZEc0QsRUFBRSxDQUFDLFNBQVMsR0F1RGxFO2tCQXZEb0IseUJBQXlCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuY29uc3QgU2Nyb2xsRXZlbnQgPSBjYy5TY3JvbGxWaWV3LkV2ZW50VHlwZTtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1Njcm9sbFZpZXdXaXRoQnV0dG9uIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgc2Nyb2xsVmlldzpjYy5TY3JvbGxWaWV3ID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuQnV0dG9uKVxuICBwcmVCdXR0b246IGNjLkJ1dHRvbiA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLkJ1dHRvbilcbiAgbmV4dEJ1dHRvbjogY2MuQnV0dG9uID0gbnVsbDtcblxuICBAcHJvcGVydHkoKVxuICBsZWZ0VGhyZXNob2xkOiBudW1iZXIgPSAxO1xuXG4gIEBwcm9wZXJ0eSgpXG4gIHJpZ2h0VGhyZXNob2xkOiBudW1iZXIgPSAyO1xuXG4gIG9uTG9hZCgpeyAgICAgICAgXG4gICAgICB0aGlzLnNjcm9sbFZpZXcgPSB0aGlzLmdldENvbXBvbmVudChjYy5TY3JvbGxWaWV3KTtcbiAgICAgIHRoaXMuaW5pdCgpO1xuICB9IFxuXG4gIC8vU0VDVElPTiBTY3JvbGwgUHJlL25leHQgYnV0dG9uXG4gIGluaXQoKSB7XG4gICAgdmFyIHNjcm9sbFZpZXdFdmVudEhhbmRsZXIgPSBuZXcgY2MuQ29tcG9uZW50LkV2ZW50SGFuZGxlcigpO1xuICAgIHNjcm9sbFZpZXdFdmVudEhhbmRsZXIudGFyZ2V0ID0gdGhpcy5ub2RlOyAvLyBUaGlzIG5vZGUgaXMgdGhlIG5vZGUgdG8gd2hpY2ggeW91ciBldmVudCBoYW5kbGVyIGNvZGUgY29tcG9uZW50IGJlbG9uZ3NcbiAgICBzY3JvbGxWaWV3RXZlbnRIYW5kbGVyLmNvbXBvbmVudCA9IFwiU2ljYm9TY3JvbGxWaWV3V2l0aEJ1dHRvblwiOyAvLyBUaGlzIGlzIHRoZSBjb2RlIGZpbGUgbmFtZVxuICAgIHNjcm9sbFZpZXdFdmVudEhhbmRsZXIuaGFuZGxlciA9IFwiY2FsbGJhY2tcIjtcbiAgICAvLyBzY3JvbGxWaWV3RXZlbnRIYW5kbGVyLmN1c3RvbUV2ZW50RGF0YSA9IFwiXCI7ICAgIFxuXG4gICAgdGhpcy5zY3JvbGxWaWV3LnNjcm9sbEV2ZW50cy5wdXNoKHNjcm9sbFZpZXdFdmVudEhhbmRsZXIpOyAgICAgICBcbiAgICB0aGlzLmNoZWNrU2Nyb2xsVmlld0J1dHRvbigpOyBcbn1cblxuXG4gIGNhbGxiYWNrKHNjcm9sbFZpZXc6IGNjLlNjcm9sbFZpZXcsIGV2ZW50VHlwZTogY2MuU2Nyb2xsVmlldy5FdmVudFR5cGUsIGN1c3RvbUV2ZW50RGF0YSkgeyAgICBcbiAgICBzd2l0Y2ggKGV2ZW50VHlwZSkge1xuICAgICAgY2FzZSBjYy5TY3JvbGxWaWV3LkV2ZW50VHlwZS5TQ1JPTExfRU5ERUQ6ICAgICAgICAgICAgXG4gICAgICAgIHRoaXMuY2hlY2tTY3JvbGxWaWV3QnV0dG9uKCk7XG4gICAgICAgIGJyZWFrOyAgICAgICAgIFxuICAgIH1cbiAgfVxuXG4gIGNoZWNrU2Nyb2xsVmlld0J1dHRvbigpe1xuICAgIGxldCBvZmZTZXRYID0gdGhpcy5zY3JvbGxWaWV3LmdldFNjcm9sbE9mZnNldCgpLng7XG4gICAgbGV0IGhhbGZDb250ZW50VyA9IHRoaXMuc2Nyb2xsVmlldy5jb250ZW50LmdldENvbnRlbnRTaXplKCkud2lkdGgvMjtcblxuICAgIC8vY2MubG9nKFwiU0NST0xMIEVORFwiLCBvZmZTZXRYLCBoYWxmQ29udGVudFcpO1xuICAgIGxldCBpc0xlZnQgPSBNYXRoLmFicyhvZmZTZXRYKSA8PSB0aGlzLmxlZnRUaHJlc2hvbGQ7XG4gICAgbGV0IGlzUmlnaHQgPSBvZmZTZXRYIDw9IC1oYWxmQ29udGVudFcgKyB0aGlzLnJpZ2h0VGhyZXNob2xkO1xuICAgIFxuICAgIHRoaXMucHJlQnV0dG9uLmludGVyYWN0YWJsZSA9ICFpc0xlZnQ7XG5cbiAgICB0aGlzLm5leHRCdXR0b24uaW50ZXJhY3RhYmxlID0gIWlzUmlnaHQ7XG5cbiAgfVxuICAvLyFTRUNUSU9OXG59XG4iXX0=