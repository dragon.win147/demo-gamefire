
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboReplayAnimationOnActiveComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c7a3f7TS39LboAh+JP12jnl', 'SicboReplayAnimationOnActiveComp');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboReplayAnimationOnActiveComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboSkeletonUtils_1 = require("../Utils/SicboSkeletonUtils");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboReplayAnimationOnActiveComp = /** @class */ (function (_super) {
    __extends(SicboReplayAnimationOnActiveComp, _super);
    function SicboReplayAnimationOnActiveComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.skeleton = null;
        _this.animation = null;
        _this.animationName = "";
        _this.loop = true;
        return _this;
    }
    SicboReplayAnimationOnActiveComp.prototype.onEnable = function () {
        if (this.skeleton != null)
            SicboSkeletonUtils_1.default.setAnimation(this.skeleton, this.animationName, this.loop);
        if (this.animation != null)
            this.animation.play(this.animationName, 0);
    };
    __decorate([
        property(sp.Skeleton)
    ], SicboReplayAnimationOnActiveComp.prototype, "skeleton", void 0);
    __decorate([
        property(cc.Animation)
    ], SicboReplayAnimationOnActiveComp.prototype, "animation", void 0);
    __decorate([
        property
    ], SicboReplayAnimationOnActiveComp.prototype, "animationName", void 0);
    __decorate([
        property
    ], SicboReplayAnimationOnActiveComp.prototype, "loop", void 0);
    SicboReplayAnimationOnActiveComp = __decorate([
        ccclass
    ], SicboReplayAnimationOnActiveComp);
    return SicboReplayAnimationOnActiveComp;
}(cc.Component));
exports.default = SicboReplayAnimationOnActiveComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvVUlDb21wb25lbnRzL1NpY2JvUmVwbGF5QW5pbWF0aW9uT25BY3RpdmVDb21wLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWxGLGtFQUE2RDtBQUl2RCxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUE4RCxvREFBWTtJQUExRTtRQUFBLHFFQXFCQztRQWxCRyxjQUFRLEdBQWdCLElBQUksQ0FBQztRQUc3QixlQUFTLEdBQWlCLElBQUksQ0FBQztRQUcvQixtQkFBYSxHQUFZLEVBQUUsQ0FBQztRQUc1QixVQUFJLEdBQVksSUFBSSxDQUFDOztJQVN6QixDQUFDO0lBUEcsbURBQVEsR0FBUjtRQUNJLElBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJO1lBQ3BCLDRCQUFrQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRWxGLElBQUcsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJO1lBQ3JCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQWpCRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO3NFQUNPO0lBRzdCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7dUVBQ1E7SUFHL0I7UUFEQyxRQUFROzJFQUNtQjtJQUc1QjtRQURDLFFBQVE7a0VBQ1k7SUFaSixnQ0FBZ0M7UUFEcEQsT0FBTztPQUNhLGdDQUFnQyxDQXFCcEQ7SUFBRCx1Q0FBQztDQXJCRCxBQXFCQyxDQXJCNkQsRUFBRSxDQUFDLFNBQVMsR0FxQnpFO2tCQXJCb0IsZ0NBQWdDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBTaWNib1NrZWxldG9uVXRpbHMgZnJvbSBcIi4uL1V0aWxzL1NpY2JvU2tlbGV0b25VdGlsc1wiO1xuXG5cblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1JlcGxheUFuaW1hdGlvbk9uQWN0aXZlQ29tcCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBAcHJvcGVydHkoc3AuU2tlbGV0b24pXG4gICAgc2tlbGV0b246IHNwLlNrZWxldG9uID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5BbmltYXRpb24pXG4gICAgYW5pbWF0aW9uOiBjYy5BbmltYXRpb24gPSBudWxsO1xuXG4gICAgQHByb3BlcnR5XG4gICAgYW5pbWF0aW9uTmFtZTogc3RyaW5nID0gIFwiXCI7XG5cbiAgICBAcHJvcGVydHlcbiAgICBsb29wOiBib29sZWFuID0gdHJ1ZTtcblxuICAgIG9uRW5hYmxlKCl7XG4gICAgICAgIGlmKHRoaXMuc2tlbGV0b24gIT0gbnVsbClcbiAgICAgICAgICAgIFNpY2JvU2tlbGV0b25VdGlscy5zZXRBbmltYXRpb24odGhpcy5za2VsZXRvbiwgdGhpcy5hbmltYXRpb25OYW1lLCB0aGlzLmxvb3ApO1xuICAgICAgICBcbiAgICAgICAgaWYodGhpcy5hbmltYXRpb24gIT0gbnVsbClcbiAgICAgICAgICAgIHRoaXMuYW5pbWF0aW9uLnBsYXkodGhpcy5hbmltYXRpb25OYW1lLCAwKTtcbiAgICB9XG59XG4iXX0=