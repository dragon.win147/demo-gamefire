
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/SicboCoinType.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '061d9v/VcxP57DrdIBH+JNm', 'SicboCoinType');
// Game/Sicbo_New/Scripts/RNGCommons/SicboCoinType.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboCoinType = void 0;
var SicboCoinType;
(function (SicboCoinType) {
    SicboCoinType[SicboCoinType["Coin100"] = 1] = "Coin100";
    SicboCoinType[SicboCoinType["Coin200"] = 2] = "Coin200";
    SicboCoinType[SicboCoinType["Coin500"] = 3] = "Coin500";
    SicboCoinType[SicboCoinType["Coin1k"] = 4] = "Coin1k";
    SicboCoinType[SicboCoinType["Coin2k"] = 5] = "Coin2k";
    SicboCoinType[SicboCoinType["Coin5k"] = 6] = "Coin5k";
    SicboCoinType[SicboCoinType["Coin10k"] = 7] = "Coin10k";
    SicboCoinType[SicboCoinType["Coin20k"] = 8] = "Coin20k";
    SicboCoinType[SicboCoinType["Coin50k"] = 9] = "Coin50k";
    SicboCoinType[SicboCoinType["Coin100k"] = 10] = "Coin100k";
    SicboCoinType[SicboCoinType["Coin200k"] = 11] = "Coin200k";
    SicboCoinType[SicboCoinType["Coin500k"] = 12] = "Coin500k";
    SicboCoinType[SicboCoinType["Coin1M"] = 13] = "Coin1M";
    SicboCoinType[SicboCoinType["Coin2M"] = 14] = "Coin2M";
    SicboCoinType[SicboCoinType["Coin5M"] = 15] = "Coin5M";
    SicboCoinType[SicboCoinType["Coin10M"] = 16] = "Coin10M";
    SicboCoinType[SicboCoinType["Coin20M"] = 17] = "Coin20M";
    SicboCoinType[SicboCoinType["Coin50M"] = 18] = "Coin50M";
})(SicboCoinType = exports.SicboCoinType || (exports.SicboCoinType = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvU2ljYm9Db2luVHlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxJQUFZLGFBbUJYO0FBbkJELFdBQVksYUFBYTtJQUN2Qix1REFBVyxDQUFBO0lBQ1gsdURBQVcsQ0FBQTtJQUNYLHVEQUFXLENBQUE7SUFDWCxxREFBVSxDQUFBO0lBQ1YscURBQVUsQ0FBQTtJQUNWLHFEQUFVLENBQUE7SUFDVix1REFBVyxDQUFBO0lBQ1gsdURBQVcsQ0FBQTtJQUNYLHVEQUFXLENBQUE7SUFDWCwwREFBYSxDQUFBO0lBQ2IsMERBQWEsQ0FBQTtJQUNiLDBEQUFhLENBQUE7SUFDYixzREFBVyxDQUFBO0lBQ1gsc0RBQVcsQ0FBQTtJQUNYLHNEQUFXLENBQUE7SUFDWCx3REFBWSxDQUFBO0lBQ1osd0RBQVksQ0FBQTtJQUNaLHdEQUFZLENBQUE7QUFDZCxDQUFDLEVBbkJXLGFBQWEsR0FBYixxQkFBYSxLQUFiLHFCQUFhLFFBbUJ4QiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIFNpY2JvQ29pblR5cGUge1xuICBDb2luMTAwID0gMSxcbiAgQ29pbjIwMCA9IDIsXG4gIENvaW41MDAgPSAzLFxuICBDb2luMWsgPSA0LFxuICBDb2luMmsgPSA1LFxuICBDb2luNWsgPSA2LFxuICBDb2luMTBrID0gNyxcbiAgQ29pbjIwayA9IDgsXG4gIENvaW41MGsgPSA5LFxuICBDb2luMTAwayA9IDEwLFxuICBDb2luMjAwayA9IDExLFxuICBDb2luNTAwayA9IDEyLFxuICBDb2luMU0gPSAxMyxcbiAgQ29pbjJNID0gMTQsXG4gIENvaW41TSA9IDE1LFxuICBDb2luMTBNID0gMTYsXG4gIENvaW4yME0gPSAxNyxcbiAgQ29pbjUwTSA9IDE4LFxufVxuIl19