
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboInfiniteScrollViewComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6f739r/FmxB66CjMAWuN27L', 'SicboInfiniteScrollViewComp');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboInfiniteScrollViewComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboInfiniteScrollViewComp = /** @class */ (function (_super) {
    __extends(SicboInfiniteScrollViewComp, _super);
    function SicboInfiniteScrollViewComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scrollView = null;
        _this.view = null;
        _this.content = null;
        _this.itemSpacing = 100;
        _this.threshold = 200;
        _this.visibleStartY = 0;
        _this.maxItemCanbeLoaded = 0;
        _this.totalItem = 0;
        _this.lastItemIndexes = [];
        _this.nodeArray = [];
        _this.interfaceInstance = null;
        return _this;
    }
    SicboInfiniteScrollViewComp.prototype.setNumberOfItem = function (num, firstCreated) {
        if (firstCreated === void 0) { firstCreated = false; }
        this.content.height = this.itemSpacing * num;
        this.totalItem = num;
        this.clearAll();
        this.nodeArray = new Array(this.totalItem);
        if (!firstCreated) {
            this.checkAutoScrollToEnd();
            // this.checkScrollViewItem();
        }
    };
    SicboInfiniteScrollViewComp.prototype.init = function (interfaceInstance, totalItem) {
        if (interfaceInstance === void 0) { interfaceInstance = null; }
        var scrollViewEventHandler = new cc.Component.EventHandler();
        scrollViewEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        scrollViewEventHandler.component = "SicboInfiniteScrollViewComp"; // This is the code file name
        scrollViewEventHandler.handler = "callback";
        // scrollViewEventHandler.customEventData = "";        
        this.scrollView.scrollEvents.push(scrollViewEventHandler);
        this.visibleStartY = this.content.y;
        this.maxItemCanbeLoaded = Math.ceil((this.view.height + this.threshold * 2) / this.itemSpacing);
        this.interfaceInstance = interfaceInstance;
        this.setNumberOfItem(totalItem, true);
        this.checkScrollViewItem();
    };
    SicboInfiniteScrollViewComp.prototype.firstLoad = function () {
        var start = 0;
        var end = Math.min(start + this.maxItemCanbeLoaded, this.totalItem - 1);
        var arr = Array.from({ length: end + 1 - start }, function (_, i) { return i + start; });
        this.lastItemIndexes = arr;
        for (var n = 0; n < this.lastItemIndexes.length; n++) {
            var i = this.lastItemIndexes[n];
            if (this.nodeArray[i] == null && this.interfaceInstance) {
                var node = this.interfaceInstance.createItem(i);
                this.setItemPosInScrollView(node, i);
                this.nodeArray[i] = node;
            }
        }
    };
    SicboInfiniteScrollViewComp.prototype.callback = function (scrollView, eventType, customEventData) {
        switch (eventType) { //baka mitai
            case cc.ScrollView.EventType.SCROLLING:
                this.checkScrollViewItem();
                ////cc.log("zzzz", this.content.childrenCount);
                break;
            case cc.ScrollView.EventType.SCROLL_TO_BOTTOM:
            case cc.ScrollView.EventType.SCROLL_TO_TOP:
                this.checkScrollViewItem();
                break;
        }
    };
    SicboInfiniteScrollViewComp.prototype.checkScrollViewItem = function (isRefresh) {
        if (isRefresh === void 0) { isRefresh = false; }
        if (this.interfaceInstance == null)
            return;
        var newItemIndexes = this.getItemsToLoad();
        //delete node not in newItemIndexes
        var deleteIndexes = this.lastItemIndexes.filter(function (o1) { return !newItemIndexes.some(function (o2) { return o2 === o1; }); }); //find element in arr1 not in arr 2
        for (var d = 0; d < deleteIndexes.length; d++) {
            var i = deleteIndexes[d];
            if (this.nodeArray[i] != null)
                this.interfaceInstance.destroyItem(this.nodeArray[i]);
            this.nodeArray[i] = null;
        }
        this.lastItemIndexes = newItemIndexes;
        for (var n = 0; n < newItemIndexes.length; n++) {
            var i = newItemIndexes[n];
            if (this.nodeArray[i] == null) {
                var node = this.interfaceInstance.createItem(i);
                this.setItemPosInScrollView(node, i);
                this.nodeArray[i] = node;
            }
            else {
                if (isRefresh) {
                    this.interfaceInstance.refreshItem(this.nodeArray[i], i);
                }
            }
        }
    };
    // refresh(){
    //     for (let n = 0; n < this.lastItemIndexes.length; n++) {
    //         let i = this.lastItemIndexes[n];
    //         if(this.nodeArray[i] !=null && this.interfaceInstance){
    //             this.interfaceInstance.refreshItem(this.nodeArray[i], i);
    //         }
    //     }
    // }
    SicboInfiniteScrollViewComp.prototype.checkAutoScrollToEnd = function () {
        if (this.totalItem <= this.maxItemCanbeLoaded) {
            this.scrollView.scrollToTop(0.5);
            return;
        }
        var needScroll = false;
        var y = this.content.y - this.content.height;
        var yBot = this.visibleStartY - this.view.height;
        needScroll = y > yBot;
        if (needScroll)
            this.scrollView.scrollToBottom(0.5);
        else {
            this.checkScrollViewItem();
        }
    };
    SicboInfiniteScrollViewComp.prototype.setItemPosInScrollView = function (item, index) {
        item.y = -index * this.itemSpacing;
    };
    SicboInfiniteScrollViewComp.prototype.getItemsToLoad = function () {
        var currentY = this.content.y;
        var start = (currentY - this.threshold - this.visibleStartY) / this.itemSpacing;
        start = Math.floor(Math.max(0, start));
        var end = Math.min(start + this.maxItemCanbeLoaded, this.totalItem - 1);
        var arr = Array.from({ length: end + 1 - start }, function (_, i) { return i + start; });
        return arr;
    };
    SicboInfiniteScrollViewComp.prototype.clearAll = function () {
        for (var i = 0; i < this.nodeArray.length; i++) {
            if (this.nodeArray[i] != null && this.interfaceInstance.destroyItem)
                this.interfaceInstance.destroyItem(this.nodeArray[i]);
            this.nodeArray[i] = null;
        }
    };
    __decorate([
        property(cc.ScrollView)
    ], SicboInfiniteScrollViewComp.prototype, "scrollView", void 0);
    __decorate([
        property(cc.Node)
    ], SicboInfiniteScrollViewComp.prototype, "view", void 0);
    __decorate([
        property(cc.Node)
    ], SicboInfiniteScrollViewComp.prototype, "content", void 0);
    __decorate([
        property()
    ], SicboInfiniteScrollViewComp.prototype, "itemSpacing", void 0);
    __decorate([
        property()
    ], SicboInfiniteScrollViewComp.prototype, "threshold", void 0);
    SicboInfiniteScrollViewComp = __decorate([
        ccclass
    ], SicboInfiniteScrollViewComp);
    return SicboInfiniteScrollViewComp;
}(cc.Component));
exports.default = SicboInfiniteScrollViewComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvVUlDb21wb25lbnRzL1NpY2JvSW5maW5pdGVTY3JvbGxWaWV3Q29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUU1RSxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQVExQztJQUF5RCwrQ0FBWTtJQUFyRTtRQUFBLHFFQW1LQztRQWpLVyxnQkFBVSxHQUFrQixJQUFJLENBQUM7UUFHakMsVUFBSSxHQUFZLElBQUksQ0FBQztRQUdyQixhQUFPLEdBQVksSUFBSSxDQUFDO1FBR3hCLGlCQUFXLEdBQVcsR0FBRyxDQUFDO1FBRzFCLGVBQVMsR0FBVyxHQUFHLENBQUM7UUFFeEIsbUJBQWEsR0FBVyxDQUFDLENBQUM7UUFDMUIsd0JBQWtCLEdBQVcsQ0FBQyxDQUFDO1FBQy9CLGVBQVMsR0FBVyxDQUFDLENBQUM7UUFFdEIscUJBQWUsR0FBYSxFQUFFLENBQUM7UUFFL0IsZUFBUyxHQUFjLEVBQUUsQ0FBQztRQUMxQix1QkFBaUIsR0FBcUMsSUFBSSxDQUFDOztJQTRJdkUsQ0FBQztJQTFJRyxxREFBZSxHQUFmLFVBQWdCLEdBQVcsRUFBRSxZQUE2QjtRQUE3Qiw2QkFBQSxFQUFBLG9CQUE2QjtRQUN0RCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztRQUM3QyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQztRQUVyQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDM0MsSUFBRyxDQUFDLFlBQVksRUFBQztZQUNiLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1lBQzVCLDhCQUE4QjtTQUNqQztJQUNMLENBQUM7SUFHRCwwQ0FBSSxHQUFKLFVBQUssaUJBQTBELEVBQUUsU0FBaUI7UUFBN0Usa0NBQUEsRUFBQSx3QkFBMEQ7UUFDM0QsSUFBSSxzQkFBc0IsR0FBRyxJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDN0Qsc0JBQXNCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQywyRUFBMkU7UUFDdEgsc0JBQXNCLENBQUMsU0FBUyxHQUFHLDZCQUE2QixDQUFDLENBQUMsNkJBQTZCO1FBQy9GLHNCQUFzQixDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUM7UUFDNUMsdURBQXVEO1FBQ3ZELElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBRTFELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUVoRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsaUJBQWlCLENBQUM7UUFFM0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVPLCtDQUFTLEdBQWpCO1FBQ0ksSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxHQUFHLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFDLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQyxHQUFHLEtBQUssRUFBQyxFQUFFLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLENBQUMsR0FBRyxLQUFLLEVBQVQsQ0FBUyxDQUFDLENBQUM7UUFFckUsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUM7UUFDM0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2xELElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEMsSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFFLElBQUksSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUM7Z0JBQ2pELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDO2FBQzVCO1NBQ0o7SUFDTCxDQUFDO0lBRUQsOENBQVEsR0FBUixVQUFTLFVBQXlCLEVBQUUsU0FBa0MsRUFBRSxlQUFlO1FBQ25GLFFBQVEsU0FBUyxFQUFFLEVBQUMsWUFBWTtZQUM1QixLQUFLLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFNBQVM7Z0JBQ2xDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2dCQUMzQiwrQ0FBK0M7Z0JBQy9DLE1BQU07WUFFVixLQUFLLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDO1lBQzlDLEtBQUssRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsYUFBYTtnQkFDdEMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7Z0JBQzNCLE1BQU07U0FDYjtJQUNMLENBQUM7SUFFRCx5REFBbUIsR0FBbkIsVUFBb0IsU0FBMEI7UUFBMUIsMEJBQUEsRUFBQSxpQkFBMEI7UUFDMUMsSUFBRyxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSTtZQUFFLE9BQU87UUFFMUMsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzNDLG1DQUFtQztRQUNuQyxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxVQUFDLEVBQUUsSUFBSyxPQUFBLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFDLEVBQUUsSUFBSyxPQUFBLEVBQUUsS0FBSyxFQUFFLEVBQVQsQ0FBUyxDQUFDLEVBQXZDLENBQXVDLENBQUMsQ0FBQyxDQUFBLG1DQUFtQztRQUNwSSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMzQyxJQUFJLENBQUMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFFLElBQUk7Z0JBQ3RCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFELElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDO1NBQzVCO1FBRUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxjQUFjLENBQUM7UUFDdEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDNUMsSUFBSSxDQUFDLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRTFCLElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBRSxJQUFJLEVBQUM7Z0JBQ3ZCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDO2FBQzVCO2lCQUFJO2dCQUNELElBQUcsU0FBUyxFQUFDO29CQUNULElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztpQkFDNUQ7YUFDSjtTQUNKO0lBQ0wsQ0FBQztJQUVELGFBQWE7SUFDYiw4REFBOEQ7SUFDOUQsMkNBQTJDO0lBQzNDLGtFQUFrRTtJQUNsRSx3RUFBd0U7SUFDeEUsWUFBWTtJQUNaLFFBQVE7SUFDUixJQUFJO0lBRUosMERBQW9CLEdBQXBCO1FBQ0ksSUFBRyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBQztZQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNqQyxPQUFPO1NBQ1Y7UUFFRCxJQUFJLFVBQVUsR0FBWSxLQUFLLENBQUM7UUFDaEMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFDN0MsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNqRCxVQUFVLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUV0QixJQUFHLFVBQVU7WUFDVCxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNwQztZQUNBLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1NBQzlCO0lBQ0wsQ0FBQztJQUVELDREQUFzQixHQUF0QixVQUF1QixJQUFhLEVBQUUsS0FBYTtRQUMvQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDdkMsQ0FBQztJQUdELG9EQUFjLEdBQWQ7UUFDSSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztRQUM5QixJQUFJLEtBQUssR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ2hGLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDdkMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFeEUsSUFBSSxHQUFHLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFDLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQyxHQUFHLEtBQUssRUFBQyxFQUFFLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLENBQUMsR0FBRyxLQUFLLEVBQVQsQ0FBUyxDQUFDLENBQUM7UUFDckUsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDO0lBRUQsOENBQVEsR0FBUjtRQUNJLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxJQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXO2dCQUM1RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxRCxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQztTQUM1QjtJQUNMLENBQUM7SUFoS0Q7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQzttRUFDaUI7SUFHekM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs2REFDVztJQUc3QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dFQUNjO0lBR2hDO1FBREMsUUFBUSxFQUFFO29FQUN1QjtJQUdsQztRQURDLFFBQVEsRUFBRTtrRUFDcUI7SUFkZiwyQkFBMkI7UUFEL0MsT0FBTztPQUNhLDJCQUEyQixDQW1LL0M7SUFBRCxrQ0FBQztDQW5LRCxBQW1LQyxDQW5Ld0QsRUFBRSxDQUFDLFNBQVMsR0FtS3BFO2tCQW5Lb0IsMkJBQTJCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuZXhwb3J0IGludGVyZmFjZSBTaWNib0luZmluaXRlU2Nyb2xsVmlld0ludGVyZmFjZXtcbiAgICBjcmVhdGVJdGVtOiAoaW5kZXg6IG51bWJlcikgPT4gY2MuTm9kZTtcbiAgICBkZXN0cm95SXRlbTogKGl0ZW1Ob2RlOiBjYy5Ob2RlKSA9PiB2b2lkO1xuICAgIHJlZnJlc2hJdGVtOiAoaXRlbU5vZGU6IGNjLk5vZGUsIGluZGV4OiBudW1iZXIpID0+IHZvaWQ7XG59XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib0luZmluaXRlU2Nyb2xsVmlld0NvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIEBwcm9wZXJ0eShjYy5TY3JvbGxWaWV3KVxuICAgIHByaXZhdGUgc2Nyb2xsVmlldzogY2MuU2Nyb2xsVmlldyA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBwcml2YXRlIHZpZXc6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgcHJpdmF0ZSBjb250ZW50OiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eSgpXG4gICAgcHJpdmF0ZSBpdGVtU3BhY2luZzogbnVtYmVyID0gMTAwO1xuXG4gICAgQHByb3BlcnR5KClcbiAgICBwcml2YXRlIHRocmVzaG9sZDogbnVtYmVyID0gMjAwO1xuICAgIFxuICAgIHByaXZhdGUgdmlzaWJsZVN0YXJ0WTogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIG1heEl0ZW1DYW5iZUxvYWRlZDogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIHRvdGFsSXRlbTogbnVtYmVyID0gMDtcblxuICAgIHByaXZhdGUgbGFzdEl0ZW1JbmRleGVzOiBudW1iZXJbXSA9IFtdO1xuXG4gICAgcHJpdmF0ZSBub2RlQXJyYXk6IGNjLk5vZGVbXSA9IFtdO1xuICAgIHByaXZhdGUgaW50ZXJmYWNlSW5zdGFuY2U6IFNpY2JvSW5maW5pdGVTY3JvbGxWaWV3SW50ZXJmYWNlID0gbnVsbDtcbiAgIFxuICAgIHNldE51bWJlck9mSXRlbShudW06IG51bWJlciwgZmlyc3RDcmVhdGVkOiBib29sZWFuID0gZmFsc2Upe1xuICAgICAgICB0aGlzLmNvbnRlbnQuaGVpZ2h0ID0gdGhpcy5pdGVtU3BhY2luZyAqIG51bTtcbiAgICAgICAgdGhpcy50b3RhbEl0ZW0gPSBudW07XG5cbiAgICAgICAgdGhpcy5jbGVhckFsbCgpO1xuICAgICAgICB0aGlzLm5vZGVBcnJheSA9IG5ldyBBcnJheSh0aGlzLnRvdGFsSXRlbSk7XG4gICAgICAgIGlmKCFmaXJzdENyZWF0ZWQpeyAgIFxuICAgICAgICAgICAgdGhpcy5jaGVja0F1dG9TY3JvbGxUb0VuZCgpO1xuICAgICAgICAgICAgLy8gdGhpcy5jaGVja1Njcm9sbFZpZXdJdGVtKCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgIFxuXG4gICAgaW5pdChpbnRlcmZhY2VJbnN0YW5jZTogU2ljYm9JbmZpbml0ZVNjcm9sbFZpZXdJbnRlcmZhY2UgPSBudWxsLCB0b3RhbEl0ZW06IG51bWJlcikge1xuICAgICAgICB2YXIgc2Nyb2xsVmlld0V2ZW50SGFuZGxlciA9IG5ldyBjYy5Db21wb25lbnQuRXZlbnRIYW5kbGVyKCk7XG4gICAgICAgIHNjcm9sbFZpZXdFdmVudEhhbmRsZXIudGFyZ2V0ID0gdGhpcy5ub2RlOyAvLyBUaGlzIG5vZGUgaXMgdGhlIG5vZGUgdG8gd2hpY2ggeW91ciBldmVudCBoYW5kbGVyIGNvZGUgY29tcG9uZW50IGJlbG9uZ3NcbiAgICAgICAgc2Nyb2xsVmlld0V2ZW50SGFuZGxlci5jb21wb25lbnQgPSBcIlNpY2JvSW5maW5pdGVTY3JvbGxWaWV3Q29tcFwiOyAvLyBUaGlzIGlzIHRoZSBjb2RlIGZpbGUgbmFtZVxuICAgICAgICBzY3JvbGxWaWV3RXZlbnRIYW5kbGVyLmhhbmRsZXIgPSBcImNhbGxiYWNrXCI7XG4gICAgICAgIC8vIHNjcm9sbFZpZXdFdmVudEhhbmRsZXIuY3VzdG9tRXZlbnREYXRhID0gXCJcIjsgICAgICAgIFxuICAgICAgICB0aGlzLnNjcm9sbFZpZXcuc2Nyb2xsRXZlbnRzLnB1c2goc2Nyb2xsVmlld0V2ZW50SGFuZGxlcik7XG4gICAgICAgIFxuICAgICAgICB0aGlzLnZpc2libGVTdGFydFkgPSB0aGlzLmNvbnRlbnQueTtcbiAgICAgICAgdGhpcy5tYXhJdGVtQ2FuYmVMb2FkZWQgPSBNYXRoLmNlaWwoKHRoaXMudmlldy5oZWlnaHQgKyB0aGlzLnRocmVzaG9sZCAqIDIpIC8gdGhpcy5pdGVtU3BhY2luZyk7XG5cbiAgICAgICAgdGhpcy5pbnRlcmZhY2VJbnN0YW5jZSA9IGludGVyZmFjZUluc3RhbmNlO1xuXG4gICAgICAgIHRoaXMuc2V0TnVtYmVyT2ZJdGVtKHRvdGFsSXRlbSwgdHJ1ZSk7XG4gICAgICAgIHRoaXMuY2hlY2tTY3JvbGxWaWV3SXRlbSgpO1xuICAgIH1cblxuICAgIHByaXZhdGUgZmlyc3RMb2FkKCl7XG4gICAgICAgIGxldCBzdGFydCA9IDA7XG4gICAgICAgIGxldCBlbmQgPSBNYXRoLm1pbihzdGFydCArIHRoaXMubWF4SXRlbUNhbmJlTG9hZGVkLCB0aGlzLnRvdGFsSXRlbSAtIDEpO1xuICAgICAgICBsZXQgYXJyID0gQXJyYXkuZnJvbSh7bGVuZ3RoOiBlbmQgKyAxIC0gc3RhcnR9LCAoXywgaSkgPT4gaSArIHN0YXJ0KTtcblxuICAgICAgICB0aGlzLmxhc3RJdGVtSW5kZXhlcyA9IGFycjtcbiAgICAgICAgZm9yIChsZXQgbiA9IDA7IG4gPCB0aGlzLmxhc3RJdGVtSW5kZXhlcy5sZW5ndGg7IG4rKykge1xuICAgICAgICAgICAgbGV0IGkgPSB0aGlzLmxhc3RJdGVtSW5kZXhlc1tuXTtcbiAgICAgICAgICAgIGlmKHRoaXMubm9kZUFycmF5W2ldPT1udWxsICYmIHRoaXMuaW50ZXJmYWNlSW5zdGFuY2Upe1xuICAgICAgICAgICAgICAgIGxldCBub2RlID0gdGhpcy5pbnRlcmZhY2VJbnN0YW5jZS5jcmVhdGVJdGVtKGkpO1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0SXRlbVBvc0luU2Nyb2xsVmlldyhub2RlLCBpKTtcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGVBcnJheVtpXSA9IG5vZGU7ICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgY2FsbGJhY2soc2Nyb2xsVmlldzogY2MuU2Nyb2xsVmlldywgZXZlbnRUeXBlOiBjYy5TY3JvbGxWaWV3LkV2ZW50VHlwZSwgY3VzdG9tRXZlbnREYXRhKSB7ICAgIFxuICAgICAgICBzd2l0Y2ggKGV2ZW50VHlwZSkgey8vYmFrYSBtaXRhaVxuICAgICAgICAgICAgY2FzZSBjYy5TY3JvbGxWaWV3LkV2ZW50VHlwZS5TQ1JPTExJTkc6ICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5jaGVja1Njcm9sbFZpZXdJdGVtKCk7XG4gICAgICAgICAgICAgICAgLy8vL2NjLmxvZyhcInp6enpcIiwgdGhpcy5jb250ZW50LmNoaWxkcmVuQ291bnQpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlIGNjLlNjcm9sbFZpZXcuRXZlbnRUeXBlLlNDUk9MTF9UT19CT1RUT006ICAgICAgICAgICAgXG4gICAgICAgICAgICBjYXNlIGNjLlNjcm9sbFZpZXcuRXZlbnRUeXBlLlNDUk9MTF9UT19UT1A6ICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5jaGVja1Njcm9sbFZpZXdJdGVtKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7ICAgICAgICAgXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBjaGVja1Njcm9sbFZpZXdJdGVtKGlzUmVmcmVzaDogYm9vbGVhbiA9IGZhbHNlKXtcbiAgICAgICAgaWYodGhpcy5pbnRlcmZhY2VJbnN0YW5jZSA9PSBudWxsKSByZXR1cm47XG5cbiAgICAgICAgbGV0IG5ld0l0ZW1JbmRleGVzID0gdGhpcy5nZXRJdGVtc1RvTG9hZCgpO1xuICAgICAgICAvL2RlbGV0ZSBub2RlIG5vdCBpbiBuZXdJdGVtSW5kZXhlc1xuICAgICAgICBsZXQgZGVsZXRlSW5kZXhlcyA9IHRoaXMubGFzdEl0ZW1JbmRleGVzLmZpbHRlcigobzEpID0+ICFuZXdJdGVtSW5kZXhlcy5zb21lKChvMikgPT4gbzIgPT09IG8xKSk7Ly9maW5kIGVsZW1lbnQgaW4gYXJyMSBub3QgaW4gYXJyIDJcbiAgICAgICAgZm9yIChsZXQgZCA9IDA7IGQgPCBkZWxldGVJbmRleGVzLmxlbmd0aDsgZCsrKSB7XG4gICAgICAgICAgICBsZXQgaSA9IGRlbGV0ZUluZGV4ZXNbZF07XG4gICAgICAgICAgICBpZih0aGlzLm5vZGVBcnJheVtpXSE9bnVsbClcbiAgICAgICAgICAgICAgICB0aGlzLmludGVyZmFjZUluc3RhbmNlLmRlc3Ryb3lJdGVtKHRoaXMubm9kZUFycmF5W2ldKTtcbiAgICAgICAgICAgIHRoaXMubm9kZUFycmF5W2ldID0gbnVsbDsgICAgICAgICAgXG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmxhc3RJdGVtSW5kZXhlcyA9IG5ld0l0ZW1JbmRleGVzO1xuICAgICAgICBmb3IgKGxldCBuID0gMDsgbiA8IG5ld0l0ZW1JbmRleGVzLmxlbmd0aDsgbisrKSB7XG4gICAgICAgICAgICBsZXQgaSA9IG5ld0l0ZW1JbmRleGVzW25dO1xuXG4gICAgICAgICAgICBpZih0aGlzLm5vZGVBcnJheVtpXT09bnVsbCl7XG4gICAgICAgICAgICAgICAgbGV0IG5vZGUgPSB0aGlzLmludGVyZmFjZUluc3RhbmNlLmNyZWF0ZUl0ZW0oaSk7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRJdGVtUG9zSW5TY3JvbGxWaWV3KG5vZGUsIGkpO1xuICAgICAgICAgICAgICAgIHRoaXMubm9kZUFycmF5W2ldID0gbm9kZTsgICAgICAgICAgXG4gICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICBpZihpc1JlZnJlc2gpe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmludGVyZmFjZUluc3RhbmNlLnJlZnJlc2hJdGVtKHRoaXMubm9kZUFycmF5W2ldLCBpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvLyByZWZyZXNoKCl7XG4gICAgLy8gICAgIGZvciAobGV0IG4gPSAwOyBuIDwgdGhpcy5sYXN0SXRlbUluZGV4ZXMubGVuZ3RoOyBuKyspIHtcbiAgICAvLyAgICAgICAgIGxldCBpID0gdGhpcy5sYXN0SXRlbUluZGV4ZXNbbl07XG4gICAgLy8gICAgICAgICBpZih0aGlzLm5vZGVBcnJheVtpXSAhPW51bGwgJiYgdGhpcy5pbnRlcmZhY2VJbnN0YW5jZSl7XG4gICAgLy8gICAgICAgICAgICAgdGhpcy5pbnRlcmZhY2VJbnN0YW5jZS5yZWZyZXNoSXRlbSh0aGlzLm5vZGVBcnJheVtpXSwgaSk7XG4gICAgLy8gICAgICAgICB9XG4gICAgLy8gICAgIH1cbiAgICAvLyB9XG5cbiAgICBjaGVja0F1dG9TY3JvbGxUb0VuZCgpe1xuICAgICAgICBpZih0aGlzLnRvdGFsSXRlbSA8PSB0aGlzLm1heEl0ZW1DYW5iZUxvYWRlZCl7XG4gICAgICAgICAgICB0aGlzLnNjcm9sbFZpZXcuc2Nyb2xsVG9Ub3AoMC41KTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBuZWVkU2Nyb2xsOiBib29sZWFuID0gZmFsc2U7XG4gICAgICAgIGxldCB5ID0gdGhpcy5jb250ZW50LnkgLSB0aGlzLmNvbnRlbnQuaGVpZ2h0O1xuICAgICAgICBsZXQgeUJvdCA9IHRoaXMudmlzaWJsZVN0YXJ0WSAtIHRoaXMudmlldy5oZWlnaHQ7XG4gICAgICAgIG5lZWRTY3JvbGwgPSB5ID4geUJvdDtcblxuICAgICAgICBpZihuZWVkU2Nyb2xsKVxuICAgICAgICAgICAgdGhpcy5zY3JvbGxWaWV3LnNjcm9sbFRvQm90dG9tKDAuNSk7XG4gICAgICAgIGVsc2V7XG4gICAgICAgICAgICB0aGlzLmNoZWNrU2Nyb2xsVmlld0l0ZW0oKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHNldEl0ZW1Qb3NJblNjcm9sbFZpZXcoaXRlbTogY2MuTm9kZSwgaW5kZXg6IG51bWJlcil7XG4gICAgICAgIGl0ZW0ueSA9IC1pbmRleCAqIHRoaXMuaXRlbVNwYWNpbmc7XG4gICAgfVxuICAgIFxuXG4gICAgZ2V0SXRlbXNUb0xvYWQoKTpudW1iZXJbXXtcbiAgICAgICAgbGV0IGN1cnJlbnRZID0gdGhpcy5jb250ZW50Lnk7XG4gICAgICAgIGxldCBzdGFydCA9IChjdXJyZW50WSAtIHRoaXMudGhyZXNob2xkIC0gdGhpcy52aXNpYmxlU3RhcnRZKSAvIHRoaXMuaXRlbVNwYWNpbmc7XG4gICAgICAgIHN0YXJ0ID0gTWF0aC5mbG9vcihNYXRoLm1heCgwLCBzdGFydCkpO1xuICAgICAgICBsZXQgZW5kID0gTWF0aC5taW4oc3RhcnQgKyB0aGlzLm1heEl0ZW1DYW5iZUxvYWRlZCwgdGhpcy50b3RhbEl0ZW0gLSAxKTtcblxuICAgICAgICBsZXQgYXJyID0gQXJyYXkuZnJvbSh7bGVuZ3RoOiBlbmQgKyAxIC0gc3RhcnR9LCAoXywgaSkgPT4gaSArIHN0YXJ0KTtcbiAgICAgICAgcmV0dXJuIGFycjtcbiAgICB9XG5cbiAgICBjbGVhckFsbCgpe1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubm9kZUFycmF5Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZih0aGlzLm5vZGVBcnJheVtpXSE9bnVsbCAmJiB0aGlzLmludGVyZmFjZUluc3RhbmNlLmRlc3Ryb3lJdGVtKVxuICAgICAgICAgICAgICAgIHRoaXMuaW50ZXJmYWNlSW5zdGFuY2UuZGVzdHJveUl0ZW0odGhpcy5ub2RlQXJyYXlbaV0pO1xuICAgICAgICAgICAgdGhpcy5ub2RlQXJyYXlbaV0gPSBudWxsO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19