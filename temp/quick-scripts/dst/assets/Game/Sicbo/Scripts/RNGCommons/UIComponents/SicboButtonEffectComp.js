
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/RNGCommons/UIComponents/SicboButtonEffectComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6ec2fHROdxCUYcyFdDUo+rc', 'SicboButtonEffectComp');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboButtonEffectComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboButtonEffectComp = /** @class */ (function (_super) {
    __extends(SicboButtonEffectComp, _super);
    function SicboButtonEffectComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.button = null;
        _this.toggle = null;
        _this.target = null;
        return _this;
    }
    SicboButtonEffectComp.prototype.onLoad = function () {
        this.button = this.node.getComponent(cc.Button);
        this.toggle = this.node.getComponent(cc.Toggle);
        if (this.target == null)
            this.target = this.node;
    };
    SicboButtonEffectComp.prototype.start = function () {
        this.registerEvent();
    };
    // update (dt) {}
    SicboButtonEffectComp.prototype.registerEvent = function () {
        this.node.on(cc.Node.EventType.MOUSE_ENTER, this.onMouseEnter, this, true);
        this.node.on(cc.Node.EventType.MOUSE_LEAVE, this.onMouseLeave, this, true);
    };
    SicboButtonEffectComp.prototype.unregisterEvent = function () {
        this.node.off(cc.Node.EventType.MOUSE_ENTER, this.onMouseEnter, this, true);
        this.node.off(cc.Node.EventType.MOUSE_LEAVE, this.onMouseLeave, this, true);
    };
    SicboButtonEffectComp.prototype.onMouseEnter = function (_, __) {
        if (this.button != null && this.button.interactable)
            this.target.scale = 1.05;
        if (this.toggle != null && this.toggle.interactable)
            this.target.scale = 1.05;
    };
    SicboButtonEffectComp.prototype.onMouseLeave = function (event, captureListeners) {
        this.target.scale = 1;
    };
    SicboButtonEffectComp.prototype.onDestroy = function () {
        this.unregisterEvent();
    };
    __decorate([
        property(cc.Node)
    ], SicboButtonEffectComp.prototype, "target", void 0);
    SicboButtonEffectComp = __decorate([
        ccclass
    ], SicboButtonEffectComp);
    return SicboButtonEffectComp;
}(cc.Component));
exports.default = SicboButtonEffectComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1JOR0NvbW1vbnMvVUlDb21wb25lbnRzL1NpY2JvQnV0dG9uRWZmZWN0Q29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUU1RSxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFtRCx5Q0FBWTtJQUEvRDtRQUFBLHFFQTBDQztRQXpDRyxZQUFNLEdBQWMsSUFBSSxDQUFDO1FBQ3pCLFlBQU0sR0FBYyxJQUFJLENBQUM7UUFFekIsWUFBTSxHQUFZLElBQUksQ0FBQzs7SUFzQzNCLENBQUM7SUFyQ0csc0NBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2hELElBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJO1lBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ3BELENBQUM7SUFFRCxxQ0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxpQkFBaUI7SUFFVCw2Q0FBYSxHQUFyQjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMzRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0UsQ0FBQztJQUVPLCtDQUFlLEdBQXZCO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzVFLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNoRixDQUFDO0lBRUQsNENBQVksR0FBWixVQUFhLENBQXNCLEVBQUUsRUFBTztRQUN4QyxJQUFHLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWTtZQUM5QyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFFN0IsSUFBRyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVk7WUFDOUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7SUFFRCw0Q0FBWSxHQUFaLFVBQWEsS0FBMEIsRUFBRSxnQkFBcUI7UUFDMUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRCx5Q0FBUyxHQUFUO1FBQ0ksSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFyQ0Q7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt5REFDSztJQUpOLHFCQUFxQjtRQUR6QyxPQUFPO09BQ2EscUJBQXFCLENBMEN6QztJQUFELDRCQUFDO0NBMUNELEFBMENDLENBMUNrRCxFQUFFLENBQUMsU0FBUyxHQTBDOUQ7a0JBMUNvQixxQkFBcUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib0J1dHRvbkVmZmVjdENvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIGJ1dHRvbjogY2MuQnV0dG9uID0gbnVsbDtcbiAgICB0b2dnbGU6IGNjLlRvZ2dsZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgdGFyZ2V0OiBjYy5Ob2RlID0gbnVsbDtcbiAgICBvbkxvYWQgKCkge1xuICAgICAgICB0aGlzLmJ1dHRvbiA9IHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuQnV0dG9uKTtcbiAgICAgICAgdGhpcy50b2dnbGUgPSB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlRvZ2dsZSk7XG4gICAgICAgIGlmKHRoaXMudGFyZ2V0ID09IG51bGwpIHRoaXMudGFyZ2V0ID0gdGhpcy5ub2RlO1xuICAgIH1cblxuICAgIHN0YXJ0ICgpIHtcbiAgICAgICAgdGhpcy5yZWdpc3RlckV2ZW50KCk7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cblxuICAgIHByaXZhdGUgcmVnaXN0ZXJFdmVudCgpIHsgICAgICAgIFxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuTU9VU0VfRU5URVIsIHRoaXMub25Nb3VzZUVudGVyLCB0aGlzLCB0cnVlKTtcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLk1PVVNFX0xFQVZFLCB0aGlzLm9uTW91c2VMZWF2ZSwgdGhpcywgdHJ1ZSk7XG4gICAgfVxuICAgIFxuICAgIHByaXZhdGUgdW5yZWdpc3RlckV2ZW50KCkgeyAgICAgICAgXG4gICAgICAgIHRoaXMubm9kZS5vZmYoY2MuTm9kZS5FdmVudFR5cGUuTU9VU0VfRU5URVIsIHRoaXMub25Nb3VzZUVudGVyLCB0aGlzLCB0cnVlKTtcbiAgICAgICAgdGhpcy5ub2RlLm9mZihjYy5Ob2RlLkV2ZW50VHlwZS5NT1VTRV9MRUFWRSwgdGhpcy5vbk1vdXNlTGVhdmUsIHRoaXMsIHRydWUpOyAgICAgICAgXG4gICAgfVxuIFxuICAgIG9uTW91c2VFbnRlcihfOiBjYy5FdmVudC5FdmVudE1vdXNlLCBfXzogYW55KXtcbiAgICAgICAgaWYodGhpcy5idXR0b24gIT0gbnVsbCAmJiB0aGlzLmJ1dHRvbi5pbnRlcmFjdGFibGUpXG4gICAgICAgICAgICB0aGlzLnRhcmdldC5zY2FsZSA9IDEuMDU7XG5cbiAgICAgICAgaWYodGhpcy50b2dnbGUgIT0gbnVsbCAmJiB0aGlzLnRvZ2dsZS5pbnRlcmFjdGFibGUpXG4gICAgICAgICAgICB0aGlzLnRhcmdldC5zY2FsZSA9IDEuMDU7XG4gICAgfVxuXG4gICAgb25Nb3VzZUxlYXZlKGV2ZW50OiBjYy5FdmVudC5FdmVudE1vdXNlLCBjYXB0dXJlTGlzdGVuZXJzOiBhbnkpe1xuICAgICAgICB0aGlzLnRhcmdldC5zY2FsZSA9IDE7XG4gICAgfVxuXG4gICAgb25EZXN0cm95KCl7XG4gICAgICAgIHRoaXMudW5yZWdpc3RlckV2ZW50KCk7XG4gICAgfVxufVxuIl19