
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/DropDown/SicboBaseDropDownComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bfe0b8BCa9KTrKHNTCEyGMw', 'SicboBaseDropDownComp');
// Game/Sicbo_New/Scripts/Components/DropDown/SicboBaseDropDownComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboBaseDropDownItemComp_1 = require("./SicboBaseDropDownItemComp");
var Vec3 = cc.Vec3;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboBaseDropDownComp = /** @class */ (function (_super) {
    __extends(SicboBaseDropDownComp, _super);
    function SicboBaseDropDownComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.view = [];
        _this.loadObject = false;
        _this.template = null;
        _this.labelCaption = null;
        _this.spriteCaption = null;
        _this.labelItem = null;
        _this.spriteItem = null;
        _this.arrow = null;
        _this.blackCover = null;
        // extra thing you wanna do when Show()
        _this.extraShow = function () { };
        _this.extraHide = function () { };
        _this.clickFunc = [];
        _this.optionDatas = [];
        _this.validTemplate = false;
        _this.items = [];
        _this.isShow = false;
        _this._selectedIndex = -1;
        return _this;
    }
    Object.defineProperty(SicboBaseDropDownComp.prototype, "selectedIndex", {
        get: function () {
            return this._selectedIndex;
        },
        set: function (value) {
            this._selectedIndex = value;
            this.refreshShownValue();
        },
        enumerable: false,
        configurable: true
    });
    SicboBaseDropDownComp.prototype.addOptionDatas = function (optionDatas) {
        var _this = this;
        optionDatas &&
            optionDatas.forEach(function (data) {
                _this.optionDatas.push(data);
            });
        this.refreshShownValue();
    };
    SicboBaseDropDownComp.prototype.clearOptionDatas = function () {
        cc.js.clear(this.optionDatas);
        this.refreshShownValue();
    };
    SicboBaseDropDownComp.prototype.resetSelectIndex = function () {
        this.selectedIndex = 0;
    };
    SicboBaseDropDownComp.prototype.setSelectIndex = function (int) {
        this.selectedIndex = int;
    };
    SicboBaseDropDownComp.prototype.show = function () {
        this.blackCover.parent = this.node.parent;
        this.blackCover.width = 4000;
        this.blackCover.height = 4000;
        this.blackCover.setPosition(new Vec3(0, 0, 0));
        this.blackCover.active = true;
        this.blackCover.setSiblingIndex(this.node.parent.childrenCount - 2);
        if (!this.validTemplate) {
            this.setUpTemplate();
            if (!this.validTemplate) {
                return;
            }
        }
        this.isShow = true;
        this._dropDown = this.createDropDownList(this.template);
        this._dropDown.name = "DropDownList";
        this._dropDown.active = true;
        this._dropDown.setParent(this.template.parent);
        this._dropDown.setSiblingIndex(0);
        this.extraShow();
        var itemTemplate = this._dropDown.getComponentInChildren(SicboBaseDropDownItemComp_1.default);
        var content = itemTemplate.node.parent;
        itemTemplate.node.active = true;
        cc.js.clear(this.items);
        for (var i = 0, len = this.optionDatas.length; i < len; i++) {
            var data = this.optionDatas[i];
            var item = this.addItem(data, i == this.selectedIndex, itemTemplate, this.items);
            if (!item) {
                continue;
            }
            item.toggle.isChecked = i == this.selectedIndex;
            item.toggle.node.on("toggle", this.onSelectedItem, this);
            // if(i == this.selectedIndex){
            //     this.onSelectedItem(item.toggle);
            // }
        }
        itemTemplate.node.active = false;
        content.height = itemTemplate.node.height * this.optionDatas.length;
    };
    SicboBaseDropDownComp.prototype.addItem = function (data, selected, itemTemplate, DropDownItemComps) {
        var item = this.createItem(itemTemplate);
        item.node.setParent(itemTemplate.node.parent);
        item.node.active = true;
        item.node.name = "item_" + (this.items.length + data.optionString ? data.optionString : "");
        if (item.toggle) {
            item.toggle.isChecked = false;
        }
        if (item.label) {
            item.label.string = data.optionString;
        }
        if (item.sprite) {
            item.sprite.spriteFrame = data.optionSf;
            item.sprite.enabled = data.optionSf != undefined;
        }
        this.items.push(item);
        return item;
    };
    SicboBaseDropDownComp.prototype.hide = function () {
        this.blackCover.parent = this.node;
        this.blackCover.active = false;
        this.extraHide();
        this.isShow = false;
        if (this._dropDown != undefined) {
            this.delayedDestroyDropdownList(0.15);
        }
    };
    SicboBaseDropDownComp.prototype.delayedDestroyDropdownList = function (delay) {
        return __awaiter(this, void 0, void 0, function () {
            var i, len;
            return __generator(this, function (_a) {
                // await WaitUtil.waitForSeconds(delay);
                // wait delay;
                for (i = 0, len = this.items.length; i < len; i++) {
                    if (this.items[i] != undefined)
                        this.destroyItem(this.items[i]);
                }
                cc.js.clear(this.items);
                if (this._dropDown != undefined)
                    this.destroyDropDownList(this._dropDown);
                this._dropDown = undefined;
                return [2 /*return*/];
            });
        });
    };
    SicboBaseDropDownComp.prototype.destroyItem = function (item) { };
    // 设置模板，方便后面item
    SicboBaseDropDownComp.prototype.setUpTemplate = function () {
        this.validTemplate = false;
        if (!this.template) {
            cc.error("The dropdown template is not assigned. The template needs to be assigned and must have a child GameObject with a Toggle component serving as the item");
            return;
        }
        this.template.active = true;
        var itemToggle = this.template.getComponentInChildren(cc.Toggle);
        this.validTemplate = true;
        // 一些判断
        if (!itemToggle || itemToggle.node == this.template) {
            this.validTemplate = false;
            cc.error("The dropdown template is not valid. The template must have a child Node with a Toggle component serving as the item.");
        }
        else if (this.labelItem != undefined && !this.labelItem.node.isChildOf(itemToggle.node)) {
            this.validTemplate = false;
            cc.error("The dropdown template is not valid. The Item Label must be on the item Node or children of it.");
        }
        else if (this.spriteItem != undefined && !this.spriteItem.node.isChildOf(itemToggle.node)) {
            this.validTemplate = false;
            cc.error("The dropdown template is not valid. The Item Sprite must be on the item Node or children of it.");
        }
        if (!this.validTemplate) {
            this.template.active = false;
            return;
        }
        var item = itemToggle.node.addComponent(SicboBaseDropDownItemComp_1.default);
        item.label = this.labelItem;
        item.sprite = this.spriteItem;
        item.toggle = itemToggle;
        item.node = itemToggle.node;
        this.template.active = false;
        this.validTemplate = true;
    };
    // 刷新显示的选中信息
    SicboBaseDropDownComp.prototype.refreshShownValue = function () {
        if (this.optionDatas.length <= 0) {
            return;
        }
        var data = this.optionDatas[this.clamp(this.selectedIndex, 0, this.optionDatas.length - 1)];
        if (this.labelCaption) {
            if (data && data.optionString) {
                this.labelCaption.string = data.optionString;
            }
            else {
                this.labelCaption.string = "";
            }
        }
        if (this.spriteCaption) {
            if (data && data.optionSf) {
                this.spriteCaption.spriteFrame = data.optionSf;
            }
            else {
                this.spriteCaption.spriteFrame = undefined;
            }
            this.spriteCaption.enabled = this.spriteCaption.spriteFrame != undefined;
        }
    };
    SicboBaseDropDownComp.prototype.createDropDownList = function (template) {
        return cc.instantiate(template);
    };
    SicboBaseDropDownComp.prototype.destroyDropDownList = function (dropDownList) {
        dropDownList.destroy();
    };
    SicboBaseDropDownComp.prototype.createItem = function (itemTemplate) {
        var newItem = cc.instantiate(itemTemplate.node);
        return newItem.getComponent(SicboBaseDropDownItemComp_1.default);
    };
    /** 当toggle被选中 */
    SicboBaseDropDownComp.prototype.onSelectedItem = function (toggle) {
        var _a;
        var parent = toggle.node.parent;
        for (var i = 0; i < parent.childrenCount; i++) {
            if (parent.children[i] == toggle.node) {
                // Subtract one to account for template child.
                (_a = this.optionDatas[i - 1]) === null || _a === void 0 ? void 0 : _a.clickFunc(i - 1);
                this.selectedIndex = i - 1;
                break;
            }
        }
        this.hide();
    };
    SicboBaseDropDownComp.prototype.onClick = function () {
        if (!this.isShow) {
            this.show();
        }
        else {
            this.hide();
        }
    };
    SicboBaseDropDownComp.prototype.onLoad = function () {
        if (this.loadObject == false) {
            this.view = [];
            this.load_all_object(this.node, "");
        }
        this.template = this.view["Template"];
        this.labelCaption = this.view["LabelCaption"].getComponent(cc.Label);
        this.spriteCaption = this.view["SpriteCaption"].getComponent(cc.Sprite);
        this.labelItem = this.view["Template/view/content/item/Label"].getComponent(cc.Label);
        this.spriteItem = this.view["Template/view/content/item/Sprite"].getComponent(cc.Sprite);
        this.blackCover = this.view["BlackCover"];
        this.arrow = this.view["Arrow"];
        this.popup = cc.find("UIManager/Popup");
        var blackCoverEvent = new cc.Component.EventHandler();
        blackCoverEvent.target = this.node; // This node is the node to which your event handler code component belongs
        blackCoverEvent.component = "SicboBaseDropDownComp"; // This is the code file name
        blackCoverEvent.handler = "clickBlackCover";
        this.dropDownBackGround = this.view["Background"];
        this.blackCover.getComponent(cc.Button).clickEvents.push(blackCoverEvent);
        // let dropDownBackGroundEvent = new cc.Component.EventHandler();
        // dropDownBackGroundEvent.target = this.node; // This node is the node to which your event handler code component belongs
        // dropDownBackGroundEvent.component = "BaseDropDownComp"; // This is the code file name
        // dropDownBackGroundEvent.handler = "onClick";
        // this.dropDownBackGround.addComponent(cc.Button).clickEvents.push(dropDownBackGroundEvent)
    };
    SicboBaseDropDownComp.prototype.load_all_object = function (root, path) {
        for (var i = 0; i < root.childrenCount; i++) {
            this.view[path + root.children[i].name] = root.children[i];
            this.load_all_object(root.children[i], path + root.children[i].name + "/");
        }
    };
    SicboBaseDropDownComp.prototype.start = function () {
        this.template.active = false;
        this.refreshShownValue();
    };
    SicboBaseDropDownComp.prototype.onEnable = function () {
        this.node.on("touchend", this.onClick, this);
    };
    SicboBaseDropDownComp.prototype.onDisable = function () {
        this.node.off("touchend", this.onClick, this);
    };
    SicboBaseDropDownComp.prototype.clamp = function (value, min, max) {
        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    };
    SicboBaseDropDownComp.prototype.clickBlackCover = function () {
        console.log("clickBtn");
        this.hide();
    };
    SicboBaseDropDownComp = __decorate([
        ccclass
    ], SicboBaseDropDownComp);
    return SicboBaseDropDownComp;
}(cc.Component));
exports.default = SicboBaseDropDownComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvRHJvcERvd24vU2ljYm9CYXNlRHJvcERvd25Db21wLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHlFQUFnRTtBQUdoRSxJQUFPLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDO0FBRWhCLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQW1ELHlDQUFZO0lBQS9EO1FBQUEscUVBa1NDO1FBalNTLFVBQUksR0FBRyxFQUFFLENBQUM7UUFDVixnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBQ3pCLGtCQUFZLEdBQWEsSUFBSSxDQUFDO1FBQzlCLG1CQUFhLEdBQWMsSUFBSSxDQUFDO1FBQ2hDLGVBQVMsR0FBYSxJQUFJLENBQUM7UUFDM0IsZ0JBQVUsR0FBYyxJQUFJLENBQUM7UUFDM0IsV0FBSyxHQUFZLElBQUksQ0FBQTtRQUNyQixnQkFBVSxHQUFZLElBQUksQ0FBQTtRQUVwQyx1Q0FBdUM7UUFDN0IsZUFBUyxHQUFhLGNBQVEsQ0FBQyxDQUFBO1FBQy9CLGVBQVMsR0FBYSxjQUFRLENBQUMsQ0FBQTtRQUcvQixlQUFTLEdBQU8sRUFBRSxDQUFDO1FBRXRCLGlCQUFXLEdBQWtDLEVBQUUsQ0FBQztRQUcvQyxtQkFBYSxHQUFZLEtBQUssQ0FBQztRQUMvQixXQUFLLEdBQTRCLEVBQUUsQ0FBQztRQUNwQyxZQUFNLEdBQVksS0FBSyxDQUFDO1FBRXhCLG9CQUFjLEdBQVcsQ0FBQyxDQUFDLENBQUM7O0lBeVF0QyxDQUFDO0lBeFFDLHNCQUFZLGdEQUFhO2FBQXpCO1lBQ0UsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQzdCLENBQUM7YUFDRCxVQUEwQixLQUFhO1lBQ3JDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1lBQzVCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQzNCLENBQUM7OztPQUpBO0lBTU0sOENBQWMsR0FBckIsVUFBc0IsV0FBMEM7UUFBaEUsaUJBTUM7UUFMQyxXQUFXO1lBQ1QsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7Z0JBQ3ZCLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlCLENBQUMsQ0FBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDM0IsQ0FBQztJQUVNLGdEQUFnQixHQUF2QjtRQUNFLEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRU0sZ0RBQWdCLEdBQXZCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUE7SUFDeEIsQ0FBQztJQUVNLDhDQUFjLEdBQXJCLFVBQXNCLEdBQVc7UUFDL0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUE7SUFDMUIsQ0FBQztJQUNNLG9DQUFJLEdBQVg7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMxQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzlCLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFBO1FBQ25FLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDdkIsT0FBTzthQUNSO1NBQ0Y7UUFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsY0FBYyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ2pDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUVqQixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUF3QixtQ0FBcUIsQ0FBQyxDQUFDO1FBQ3ZHLElBQUksT0FBTyxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUVoQyxFQUFFLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFeEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDM0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLElBQUksR0FBMEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUUsWUFBWSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4RyxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNULFNBQVM7YUFDVjtZQUNELElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQ2hELElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN6RCwrQkFBK0I7WUFDL0Isd0NBQXdDO1lBQ3hDLElBQUk7U0FDTDtRQUNELFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUVqQyxPQUFPLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO0lBQ3RFLENBQUM7SUFFTyx1Q0FBTyxHQUFmLFVBQWdCLElBQWlDLEVBQUUsUUFBaUIsRUFBRSxZQUFtQyxFQUFFLGlCQUEwQztRQUNuSixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLFdBQVEsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFFLENBQUM7UUFDMUYsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1NBQy9CO1FBQ0QsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztTQUN2QztRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDeEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxTQUFTLENBQUM7U0FDbEQ7UUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTSxvQ0FBSSxHQUFYO1FBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxTQUFTLEVBQUU7WUFDL0IsSUFBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0gsQ0FBQztJQUVhLDBEQUEwQixHQUF4QyxVQUF5QyxLQUFhOzs7O2dCQUNwRCx3Q0FBd0M7Z0JBQ3hDLGNBQWM7Z0JBQ2QsS0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNyRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksU0FBUzt3QkFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDakU7Z0JBQ0QsRUFBRSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN4QixJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksU0FBUztvQkFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUMxRSxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQzs7OztLQUM1QjtJQUVPLDJDQUFXLEdBQW5CLFVBQW9CLElBQUksSUFBSSxDQUFDO0lBRTdCLGdCQUFnQjtJQUNSLDZDQUFhLEdBQXJCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFFM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDbEIsRUFBRSxDQUFDLEtBQUssQ0FBQyx1SkFBdUosQ0FBQyxDQUFDO1lBQ2xLLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLFVBQVUsR0FBYyxJQUFJLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFZLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN2RixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUMxQixPQUFPO1FBQ1AsSUFBSSxDQUFDLFVBQVUsSUFBSSxVQUFVLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDbkQsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsRUFBRSxDQUFDLEtBQUssQ0FBQyxzSEFBc0gsQ0FBQyxDQUFDO1NBQ2xJO2FBQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLFNBQVMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDekYsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsRUFBRSxDQUFDLEtBQUssQ0FBQyxnR0FBZ0csQ0FBQyxDQUFDO1NBQzVHO2FBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLFNBQVMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDM0YsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsRUFBRSxDQUFDLEtBQUssQ0FBQyxpR0FBaUcsQ0FBQyxDQUFDO1NBQzdHO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQzdCLE9BQU87U0FDUjtRQUNELElBQUksSUFBSSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUF3QixtQ0FBcUIsQ0FBQyxDQUFDO1FBQ3RGLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7UUFDekIsSUFBSSxDQUFDLElBQUksR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDO1FBRTVCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztJQUM1QixDQUFDO0lBRUQsWUFBWTtJQUNKLGlEQUFpQixHQUF6QjtRQUNFLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQ2hDLE9BQU87U0FDUjtRQUNELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzVGLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUM3QixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO2FBQzlDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQzthQUMvQjtTQUNGO1FBQ0QsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7YUFDaEQ7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO2FBQzVDO1lBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLElBQUksU0FBUyxDQUFDO1NBQzFFO0lBQ0gsQ0FBQztJQUVTLGtEQUFrQixHQUE1QixVQUE2QixRQUFpQjtRQUM1QyxPQUFPLEVBQUUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVTLG1EQUFtQixHQUE3QixVQUE4QixZQUFxQjtRQUNqRCxZQUFZLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVTLDBDQUFVLEdBQXBCLFVBQXFCLFlBQW1DO1FBQ3RELElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hELE9BQU8sT0FBTyxDQUFDLFlBQVksQ0FBd0IsbUNBQXFCLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBRUQsaUJBQWlCO0lBQ1QsOENBQWMsR0FBdEIsVUFBdUIsTUFBaUI7O1FBQ3RDLElBQUksTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2hDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzdDLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxFQUFFO2dCQUNyQyw4Q0FBOEM7Z0JBQzlDLE1BQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLDBDQUFFLFNBQVMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUMxQyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzNCLE1BQU07YUFDUDtTQUNGO1FBQ0QsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVPLHVDQUFPLEdBQWY7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNoQixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDYjthQUFNO1lBQ0wsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsc0NBQU0sR0FBTjtRQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLEVBQUU7WUFDNUIsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDZixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDckM7UUFDRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGtDQUFrQyxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0RixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsbUNBQW1DLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3pGLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUN6QyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDL0IsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDeEMsSUFBSSxlQUFlLEdBQUcsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3RELGVBQWUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLDJFQUEyRTtRQUMvRyxlQUFlLENBQUMsU0FBUyxHQUFHLHVCQUF1QixDQUFDLENBQUMsNkJBQTZCO1FBQ2xGLGVBQWUsQ0FBQyxPQUFPLEdBQUcsaUJBQWlCLENBQUM7UUFDNUMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7UUFDakQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7UUFDekUsaUVBQWlFO1FBQ2pFLDBIQUEwSDtRQUMxSCx3RkFBd0Y7UUFDeEYsK0NBQStDO1FBQy9DLDRGQUE0RjtJQUM5RixDQUFDO0lBQ0QsK0NBQWUsR0FBZixVQUFnQixJQUFJLEVBQUUsSUFBSTtRQUN4QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMzQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztTQUM1RTtJQUNILENBQUM7SUFFRCxxQ0FBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFFRCx3Q0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELHlDQUFTLEdBQVQ7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRU8scUNBQUssR0FBYixVQUFjLEtBQWEsRUFBRSxHQUFXLEVBQUUsR0FBVztRQUNuRCxJQUFJLEtBQUssR0FBRyxHQUFHO1lBQUUsT0FBTyxHQUFHLENBQUM7UUFDNUIsSUFBSSxLQUFLLEdBQUcsR0FBRztZQUFFLE9BQU8sR0FBRyxDQUFDO1FBQzVCLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVPLCtDQUFlLEdBQXZCO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtRQUN2QixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZCxDQUFDO0lBalNrQixxQkFBcUI7UUFEekMsT0FBTztPQUNhLHFCQUFxQixDQWtTekM7SUFBRCw0QkFBQztDQWxTRCxBQWtTQyxDQWxTa0QsRUFBRSxDQUFDLFNBQVMsR0FrUzlEO2tCQWxTb0IscUJBQXFCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFNpY2JvRHJvcERvd25JdGVtQ29tcCBmcm9tIFwiLi9TaWNib0Jhc2VEcm9wRG93bkl0ZW1Db21wXCI7XG5pbXBvcnQgU2ljYm9Ecm9wRG93bk9wdGlvbkRhdGFDb21wIGZyb20gXCIuL1NpY2JvQmFzZURyb3BEb3duT3B0aW9uRGF0YUNvbXBcIjtcblxuaW1wb3J0IFZlYzMgPSBjYy5WZWMzO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9CYXNlRHJvcERvd25Db21wIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgcHJpdmF0ZSB2aWV3ID0gW107XG4gIHByaXZhdGUgbG9hZE9iamVjdDogYm9vbGVhbiA9IGZhbHNlO1xuICBwcml2YXRlIHRlbXBsYXRlOiBjYy5Ob2RlID0gbnVsbDtcbiAgcHJpdmF0ZSBsYWJlbENhcHRpb246IGNjLkxhYmVsID0gbnVsbDtcbiAgcHJpdmF0ZSBzcHJpdGVDYXB0aW9uOiBjYy5TcHJpdGUgPSBudWxsO1xuICBwcml2YXRlIGxhYmVsSXRlbTogY2MuTGFiZWwgPSBudWxsO1xuICBwcml2YXRlIHNwcml0ZUl0ZW06IGNjLlNwcml0ZSA9IG51bGw7XG4gIHByb3RlY3RlZCBhcnJvdzogY2MuTm9kZSA9IG51bGxcbiAgcHJvdGVjdGVkIGJsYWNrQ292ZXI6IGNjLk5vZGUgPSBudWxsXG4gIHByb3RlY3RlZCBkcm9wRG93bkJhY2tHcm91bmQ6IGNjLk5vZGU7XG4gIC8vIGV4dHJhIHRoaW5nIHlvdSB3YW5uYSBkbyB3aGVuIFNob3coKVxuICBwcm90ZWN0ZWQgZXh0cmFTaG93OiBGdW5jdGlvbiA9ICgpID0+IHsgfVxuICBwcm90ZWN0ZWQgZXh0cmFIaWRlOiBGdW5jdGlvbiA9ICgpID0+IHsgfVxuICBwcm90ZWN0ZWQgcG9wdXA6IGNjLk5vZGU7XG5cbiAgcHJvdGVjdGVkIGNsaWNrRnVuYzogW10gPSBbXTtcblxuICBwdWJsaWMgb3B0aW9uRGF0YXM6IFNpY2JvRHJvcERvd25PcHRpb25EYXRhQ29tcFtdID0gW107XG5cbiAgcHJpdmF0ZSBfZHJvcERvd246IGNjLk5vZGU7XG4gIHByaXZhdGUgdmFsaWRUZW1wbGF0ZTogYm9vbGVhbiA9IGZhbHNlO1xuICBwcml2YXRlIGl0ZW1zOiBTaWNib0Ryb3BEb3duSXRlbUNvbXBbXSA9IFtdO1xuICBwcml2YXRlIGlzU2hvdzogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIHByaXZhdGUgX3NlbGVjdGVkSW5kZXg6IG51bWJlciA9IC0xO1xuICBwcml2YXRlIGdldCBzZWxlY3RlZEluZGV4KCk6IG51bWJlciB7XG4gICAgcmV0dXJuIHRoaXMuX3NlbGVjdGVkSW5kZXg7XG4gIH1cbiAgcHJpdmF0ZSBzZXQgc2VsZWN0ZWRJbmRleCh2YWx1ZTogbnVtYmVyKSB7XG4gICAgdGhpcy5fc2VsZWN0ZWRJbmRleCA9IHZhbHVlO1xuICAgIHRoaXMucmVmcmVzaFNob3duVmFsdWUoKTtcbiAgfVxuXG4gIHB1YmxpYyBhZGRPcHRpb25EYXRhcyhvcHRpb25EYXRhczogU2ljYm9Ecm9wRG93bk9wdGlvbkRhdGFDb21wW10pIHtcbiAgICBvcHRpb25EYXRhcyAmJlxuICAgICAgb3B0aW9uRGF0YXMuZm9yRWFjaCgoZGF0YSkgPT4ge1xuICAgICAgICB0aGlzLm9wdGlvbkRhdGFzLnB1c2goZGF0YSk7XG4gICAgICB9KTtcbiAgICB0aGlzLnJlZnJlc2hTaG93blZhbHVlKCk7XG4gIH1cblxuICBwdWJsaWMgY2xlYXJPcHRpb25EYXRhcygpIHtcbiAgICBjYy5qcy5jbGVhcih0aGlzLm9wdGlvbkRhdGFzKTtcbiAgICB0aGlzLnJlZnJlc2hTaG93blZhbHVlKCk7XG4gIH1cblxuICBwdWJsaWMgcmVzZXRTZWxlY3RJbmRleCgpIHtcbiAgICB0aGlzLnNlbGVjdGVkSW5kZXggPSAwXG4gIH1cblxuICBwdWJsaWMgc2V0U2VsZWN0SW5kZXgoaW50OiBudW1iZXIpIHtcbiAgICB0aGlzLnNlbGVjdGVkSW5kZXggPSBpbnRcbiAgfVxuICBwdWJsaWMgc2hvdygpIHtcbiAgICB0aGlzLmJsYWNrQ292ZXIucGFyZW50ID0gdGhpcy5ub2RlLnBhcmVudDtcbiAgICB0aGlzLmJsYWNrQ292ZXIud2lkdGggPSA0MDAwO1xuICAgIHRoaXMuYmxhY2tDb3Zlci5oZWlnaHQgPSA0MDAwO1xuICAgIHRoaXMuYmxhY2tDb3Zlci5zZXRQb3NpdGlvbihuZXcgVmVjMygwLCAwLCAwKSk7XG4gICAgdGhpcy5ibGFja0NvdmVyLmFjdGl2ZSA9IHRydWU7XG4gICAgdGhpcy5ibGFja0NvdmVyLnNldFNpYmxpbmdJbmRleCh0aGlzLm5vZGUucGFyZW50LmNoaWxkcmVuQ291bnQgLSAyKVxuICAgIGlmICghdGhpcy52YWxpZFRlbXBsYXRlKSB7XG4gICAgICB0aGlzLnNldFVwVGVtcGxhdGUoKTtcbiAgICAgIGlmICghdGhpcy52YWxpZFRlbXBsYXRlKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5pc1Nob3cgPSB0cnVlO1xuICAgIHRoaXMuX2Ryb3BEb3duID0gdGhpcy5jcmVhdGVEcm9wRG93bkxpc3QodGhpcy50ZW1wbGF0ZSk7XG4gICAgdGhpcy5fZHJvcERvd24ubmFtZSA9IFwiRHJvcERvd25MaXN0XCI7XG4gICAgdGhpcy5fZHJvcERvd24uYWN0aXZlID0gdHJ1ZTtcbiAgICB0aGlzLl9kcm9wRG93bi5zZXRQYXJlbnQodGhpcy50ZW1wbGF0ZS5wYXJlbnQpO1xuICAgIHRoaXMuX2Ryb3BEb3duLnNldFNpYmxpbmdJbmRleCgwKVxuICAgIHRoaXMuZXh0cmFTaG93KCk7XG5cbiAgICBsZXQgaXRlbVRlbXBsYXRlID0gdGhpcy5fZHJvcERvd24uZ2V0Q29tcG9uZW50SW5DaGlsZHJlbjxTaWNib0Ryb3BEb3duSXRlbUNvbXA+KFNpY2JvRHJvcERvd25JdGVtQ29tcCk7XG4gICAgbGV0IGNvbnRlbnQgPSBpdGVtVGVtcGxhdGUubm9kZS5wYXJlbnQ7XG4gICAgaXRlbVRlbXBsYXRlLm5vZGUuYWN0aXZlID0gdHJ1ZTtcblxuICAgIGNjLmpzLmNsZWFyKHRoaXMuaXRlbXMpO1xuXG4gICAgZm9yIChsZXQgaSA9IDAsIGxlbiA9IHRoaXMub3B0aW9uRGF0YXMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgIGxldCBkYXRhID0gdGhpcy5vcHRpb25EYXRhc1tpXTtcbiAgICAgIGxldCBpdGVtOiBTaWNib0Ryb3BEb3duSXRlbUNvbXAgPSB0aGlzLmFkZEl0ZW0oZGF0YSwgaSA9PSB0aGlzLnNlbGVjdGVkSW5kZXgsIGl0ZW1UZW1wbGF0ZSwgdGhpcy5pdGVtcyk7XG4gICAgICBpZiAoIWl0ZW0pIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG4gICAgICBpdGVtLnRvZ2dsZS5pc0NoZWNrZWQgPSBpID09IHRoaXMuc2VsZWN0ZWRJbmRleDtcbiAgICAgIGl0ZW0udG9nZ2xlLm5vZGUub24oXCJ0b2dnbGVcIiwgdGhpcy5vblNlbGVjdGVkSXRlbSwgdGhpcyk7XG4gICAgICAvLyBpZihpID09IHRoaXMuc2VsZWN0ZWRJbmRleCl7XG4gICAgICAvLyAgICAgdGhpcy5vblNlbGVjdGVkSXRlbShpdGVtLnRvZ2dsZSk7XG4gICAgICAvLyB9XG4gICAgfVxuICAgIGl0ZW1UZW1wbGF0ZS5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuXG4gICAgY29udGVudC5oZWlnaHQgPSBpdGVtVGVtcGxhdGUubm9kZS5oZWlnaHQgKiB0aGlzLm9wdGlvbkRhdGFzLmxlbmd0aDtcbiAgfVxuXG4gIHByaXZhdGUgYWRkSXRlbShkYXRhOiBTaWNib0Ryb3BEb3duT3B0aW9uRGF0YUNvbXAsIHNlbGVjdGVkOiBib29sZWFuLCBpdGVtVGVtcGxhdGU6IFNpY2JvRHJvcERvd25JdGVtQ29tcCwgRHJvcERvd25JdGVtQ29tcHM6IFNpY2JvRHJvcERvd25JdGVtQ29tcFtdKTogU2ljYm9Ecm9wRG93bkl0ZW1Db21wIHtcbiAgICBsZXQgaXRlbSA9IHRoaXMuY3JlYXRlSXRlbShpdGVtVGVtcGxhdGUpO1xuICAgIGl0ZW0ubm9kZS5zZXRQYXJlbnQoaXRlbVRlbXBsYXRlLm5vZGUucGFyZW50KTtcbiAgICBpdGVtLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICBpdGVtLm5vZGUubmFtZSA9IGBpdGVtXyR7dGhpcy5pdGVtcy5sZW5ndGggKyBkYXRhLm9wdGlvblN0cmluZyA/IGRhdGEub3B0aW9uU3RyaW5nIDogXCJcIn1gO1xuICAgIGlmIChpdGVtLnRvZ2dsZSkge1xuICAgICAgaXRlbS50b2dnbGUuaXNDaGVja2VkID0gZmFsc2U7XG4gICAgfVxuICAgIGlmIChpdGVtLmxhYmVsKSB7XG4gICAgICBpdGVtLmxhYmVsLnN0cmluZyA9IGRhdGEub3B0aW9uU3RyaW5nO1xuICAgIH1cbiAgICBpZiAoaXRlbS5zcHJpdGUpIHtcbiAgICAgIGl0ZW0uc3ByaXRlLnNwcml0ZUZyYW1lID0gZGF0YS5vcHRpb25TZjtcbiAgICAgIGl0ZW0uc3ByaXRlLmVuYWJsZWQgPSBkYXRhLm9wdGlvblNmICE9IHVuZGVmaW5lZDtcbiAgICB9XG4gICAgdGhpcy5pdGVtcy5wdXNoKGl0ZW0pO1xuICAgIHJldHVybiBpdGVtO1xuICB9XG5cbiAgcHVibGljIGhpZGUoKSB7XG4gICAgdGhpcy5ibGFja0NvdmVyLnBhcmVudCA9IHRoaXMubm9kZTtcbiAgICB0aGlzLmJsYWNrQ292ZXIuYWN0aXZlID0gZmFsc2U7XG4gICAgdGhpcy5leHRyYUhpZGUoKTtcbiAgICB0aGlzLmlzU2hvdyA9IGZhbHNlO1xuICAgIGlmICh0aGlzLl9kcm9wRG93biAhPSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMuZGVsYXllZERlc3Ryb3lEcm9wZG93bkxpc3QoMC4xNSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBhc3luYyBkZWxheWVkRGVzdHJveURyb3Bkb3duTGlzdChkZWxheTogbnVtYmVyKSB7XG4gICAgLy8gYXdhaXQgV2FpdFV0aWwud2FpdEZvclNlY29uZHMoZGVsYXkpO1xuICAgIC8vIHdhaXQgZGVsYXk7XG4gICAgZm9yIChsZXQgaSA9IDAsIGxlbiA9IHRoaXMuaXRlbXMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgIGlmICh0aGlzLml0ZW1zW2ldICE9IHVuZGVmaW5lZCkgdGhpcy5kZXN0cm95SXRlbSh0aGlzLml0ZW1zW2ldKTtcbiAgICB9XG4gICAgY2MuanMuY2xlYXIodGhpcy5pdGVtcyk7XG4gICAgaWYgKHRoaXMuX2Ryb3BEb3duICE9IHVuZGVmaW5lZCkgdGhpcy5kZXN0cm95RHJvcERvd25MaXN0KHRoaXMuX2Ryb3BEb3duKTtcbiAgICB0aGlzLl9kcm9wRG93biA9IHVuZGVmaW5lZDtcbiAgfVxuXG4gIHByaXZhdGUgZGVzdHJveUl0ZW0oaXRlbSkgeyB9XG5cbiAgLy8g6K6+572u5qih5p2/77yM5pa55L6/5ZCO6Z2iaXRlbVxuICBwcml2YXRlIHNldFVwVGVtcGxhdGUoKSB7XG4gICAgdGhpcy52YWxpZFRlbXBsYXRlID0gZmFsc2U7XG5cbiAgICBpZiAoIXRoaXMudGVtcGxhdGUpIHtcbiAgICAgIGNjLmVycm9yKFwiVGhlIGRyb3Bkb3duIHRlbXBsYXRlIGlzIG5vdCBhc3NpZ25lZC4gVGhlIHRlbXBsYXRlIG5lZWRzIHRvIGJlIGFzc2lnbmVkIGFuZCBtdXN0IGhhdmUgYSBjaGlsZCBHYW1lT2JqZWN0IHdpdGggYSBUb2dnbGUgY29tcG9uZW50IHNlcnZpbmcgYXMgdGhlIGl0ZW1cIik7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMudGVtcGxhdGUuYWN0aXZlID0gdHJ1ZTtcbiAgICBsZXQgaXRlbVRvZ2dsZTogY2MuVG9nZ2xlID0gdGhpcy50ZW1wbGF0ZS5nZXRDb21wb25lbnRJbkNoaWxkcmVuPGNjLlRvZ2dsZT4oY2MuVG9nZ2xlKTtcbiAgICB0aGlzLnZhbGlkVGVtcGxhdGUgPSB0cnVlO1xuICAgIC8vIOS4gOS6m+WIpOaWrVxuICAgIGlmICghaXRlbVRvZ2dsZSB8fCBpdGVtVG9nZ2xlLm5vZGUgPT0gdGhpcy50ZW1wbGF0ZSkge1xuICAgICAgdGhpcy52YWxpZFRlbXBsYXRlID0gZmFsc2U7XG4gICAgICBjYy5lcnJvcihcIlRoZSBkcm9wZG93biB0ZW1wbGF0ZSBpcyBub3QgdmFsaWQuIFRoZSB0ZW1wbGF0ZSBtdXN0IGhhdmUgYSBjaGlsZCBOb2RlIHdpdGggYSBUb2dnbGUgY29tcG9uZW50IHNlcnZpbmcgYXMgdGhlIGl0ZW0uXCIpO1xuICAgIH0gZWxzZSBpZiAodGhpcy5sYWJlbEl0ZW0gIT0gdW5kZWZpbmVkICYmICF0aGlzLmxhYmVsSXRlbS5ub2RlLmlzQ2hpbGRPZihpdGVtVG9nZ2xlLm5vZGUpKSB7XG4gICAgICB0aGlzLnZhbGlkVGVtcGxhdGUgPSBmYWxzZTtcbiAgICAgIGNjLmVycm9yKFwiVGhlIGRyb3Bkb3duIHRlbXBsYXRlIGlzIG5vdCB2YWxpZC4gVGhlIEl0ZW0gTGFiZWwgbXVzdCBiZSBvbiB0aGUgaXRlbSBOb2RlIG9yIGNoaWxkcmVuIG9mIGl0LlwiKTtcbiAgICB9IGVsc2UgaWYgKHRoaXMuc3ByaXRlSXRlbSAhPSB1bmRlZmluZWQgJiYgIXRoaXMuc3ByaXRlSXRlbS5ub2RlLmlzQ2hpbGRPZihpdGVtVG9nZ2xlLm5vZGUpKSB7XG4gICAgICB0aGlzLnZhbGlkVGVtcGxhdGUgPSBmYWxzZTtcbiAgICAgIGNjLmVycm9yKFwiVGhlIGRyb3Bkb3duIHRlbXBsYXRlIGlzIG5vdCB2YWxpZC4gVGhlIEl0ZW0gU3ByaXRlIG11c3QgYmUgb24gdGhlIGl0ZW0gTm9kZSBvciBjaGlsZHJlbiBvZiBpdC5cIik7XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLnZhbGlkVGVtcGxhdGUpIHtcbiAgICAgIHRoaXMudGVtcGxhdGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGxldCBpdGVtID0gaXRlbVRvZ2dsZS5ub2RlLmFkZENvbXBvbmVudDxTaWNib0Ryb3BEb3duSXRlbUNvbXA+KFNpY2JvRHJvcERvd25JdGVtQ29tcCk7XG4gICAgaXRlbS5sYWJlbCA9IHRoaXMubGFiZWxJdGVtO1xuICAgIGl0ZW0uc3ByaXRlID0gdGhpcy5zcHJpdGVJdGVtO1xuICAgIGl0ZW0udG9nZ2xlID0gaXRlbVRvZ2dsZTtcbiAgICBpdGVtLm5vZGUgPSBpdGVtVG9nZ2xlLm5vZGU7XG5cbiAgICB0aGlzLnRlbXBsYXRlLmFjdGl2ZSA9IGZhbHNlO1xuICAgIHRoaXMudmFsaWRUZW1wbGF0ZSA9IHRydWU7XG4gIH1cblxuICAvLyDliLfmlrDmmL7npLrnmoTpgInkuK3kv6Hmga9cbiAgcHJpdmF0ZSByZWZyZXNoU2hvd25WYWx1ZSgpIHtcbiAgICBpZiAodGhpcy5vcHRpb25EYXRhcy5sZW5ndGggPD0gMCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBsZXQgZGF0YSA9IHRoaXMub3B0aW9uRGF0YXNbdGhpcy5jbGFtcCh0aGlzLnNlbGVjdGVkSW5kZXgsIDAsIHRoaXMub3B0aW9uRGF0YXMubGVuZ3RoIC0gMSldO1xuICAgIGlmICh0aGlzLmxhYmVsQ2FwdGlvbikge1xuICAgICAgaWYgKGRhdGEgJiYgZGF0YS5vcHRpb25TdHJpbmcpIHtcbiAgICAgICAgdGhpcy5sYWJlbENhcHRpb24uc3RyaW5nID0gZGF0YS5vcHRpb25TdHJpbmc7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmxhYmVsQ2FwdGlvbi5zdHJpbmcgPSBcIlwiO1xuICAgICAgfVxuICAgIH1cbiAgICBpZiAodGhpcy5zcHJpdGVDYXB0aW9uKSB7XG4gICAgICBpZiAoZGF0YSAmJiBkYXRhLm9wdGlvblNmKSB7XG4gICAgICAgIHRoaXMuc3ByaXRlQ2FwdGlvbi5zcHJpdGVGcmFtZSA9IGRhdGEub3B0aW9uU2Y7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnNwcml0ZUNhcHRpb24uc3ByaXRlRnJhbWUgPSB1bmRlZmluZWQ7XG4gICAgICB9XG4gICAgICB0aGlzLnNwcml0ZUNhcHRpb24uZW5hYmxlZCA9IHRoaXMuc3ByaXRlQ2FwdGlvbi5zcHJpdGVGcmFtZSAhPSB1bmRlZmluZWQ7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIGNyZWF0ZURyb3BEb3duTGlzdCh0ZW1wbGF0ZTogY2MuTm9kZSk6IGNjLk5vZGUge1xuICAgIHJldHVybiBjYy5pbnN0YW50aWF0ZSh0ZW1wbGF0ZSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZGVzdHJveURyb3BEb3duTGlzdChkcm9wRG93bkxpc3Q6IGNjLk5vZGUpIHtcbiAgICBkcm9wRG93bkxpc3QuZGVzdHJveSgpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGNyZWF0ZUl0ZW0oaXRlbVRlbXBsYXRlOiBTaWNib0Ryb3BEb3duSXRlbUNvbXApOiBTaWNib0Ryb3BEb3duSXRlbUNvbXAge1xuICAgIGxldCBuZXdJdGVtID0gY2MuaW5zdGFudGlhdGUoaXRlbVRlbXBsYXRlLm5vZGUpO1xuICAgIHJldHVybiBuZXdJdGVtLmdldENvbXBvbmVudDxTaWNib0Ryb3BEb3duSXRlbUNvbXA+KFNpY2JvRHJvcERvd25JdGVtQ29tcCk7XG4gIH1cblxuICAvKiog5b2TdG9nZ2xl6KKr6YCJ5LitICovXG4gIHByaXZhdGUgb25TZWxlY3RlZEl0ZW0odG9nZ2xlOiBjYy5Ub2dnbGUpIHtcbiAgICBsZXQgcGFyZW50ID0gdG9nZ2xlLm5vZGUucGFyZW50O1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xuICAgICAgaWYgKHBhcmVudC5jaGlsZHJlbltpXSA9PSB0b2dnbGUubm9kZSkge1xuICAgICAgICAvLyBTdWJ0cmFjdCBvbmUgdG8gYWNjb3VudCBmb3IgdGVtcGxhdGUgY2hpbGQuXG4gICAgICAgIHRoaXMub3B0aW9uRGF0YXNbaSAtIDFdPy5jbGlja0Z1bmMoaSAtIDEpO1xuICAgICAgICB0aGlzLnNlbGVjdGVkSW5kZXggPSBpIC0gMTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuICAgIHRoaXMuaGlkZSgpO1xuICB9XG5cbiAgcHJpdmF0ZSBvbkNsaWNrKCkge1xuICAgIGlmICghdGhpcy5pc1Nob3cpIHtcbiAgICAgIHRoaXMuc2hvdygpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmhpZGUoKTtcbiAgICB9XG4gIH1cblxuICBvbkxvYWQoKSB7XG4gICAgaWYgKHRoaXMubG9hZE9iamVjdCA9PSBmYWxzZSkge1xuICAgICAgdGhpcy52aWV3ID0gW107XG4gICAgICB0aGlzLmxvYWRfYWxsX29iamVjdCh0aGlzLm5vZGUsIFwiXCIpO1xuICAgIH1cbiAgICB0aGlzLnRlbXBsYXRlID0gdGhpcy52aWV3W1wiVGVtcGxhdGVcIl07XG4gICAgdGhpcy5sYWJlbENhcHRpb24gPSB0aGlzLnZpZXdbXCJMYWJlbENhcHRpb25cIl0uZ2V0Q29tcG9uZW50KGNjLkxhYmVsKTtcbiAgICB0aGlzLnNwcml0ZUNhcHRpb24gPSB0aGlzLnZpZXdbXCJTcHJpdGVDYXB0aW9uXCJdLmdldENvbXBvbmVudChjYy5TcHJpdGUpO1xuICAgIHRoaXMubGFiZWxJdGVtID0gdGhpcy52aWV3W1wiVGVtcGxhdGUvdmlldy9jb250ZW50L2l0ZW0vTGFiZWxcIl0uZ2V0Q29tcG9uZW50KGNjLkxhYmVsKTtcbiAgICB0aGlzLnNwcml0ZUl0ZW0gPSB0aGlzLnZpZXdbXCJUZW1wbGF0ZS92aWV3L2NvbnRlbnQvaXRlbS9TcHJpdGVcIl0uZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSk7XG4gICAgdGhpcy5ibGFja0NvdmVyID0gdGhpcy52aWV3W1wiQmxhY2tDb3ZlclwiXVxuICAgIHRoaXMuYXJyb3cgPSB0aGlzLnZpZXdbXCJBcnJvd1wiXVxuICAgIHRoaXMucG9wdXAgPSBjYy5maW5kKFwiVUlNYW5hZ2VyL1BvcHVwXCIpO1xuICAgIHZhciBibGFja0NvdmVyRXZlbnQgPSBuZXcgY2MuQ29tcG9uZW50LkV2ZW50SGFuZGxlcigpO1xuICAgIGJsYWNrQ292ZXJFdmVudC50YXJnZXQgPSB0aGlzLm5vZGU7IC8vIFRoaXMgbm9kZSBpcyB0aGUgbm9kZSB0byB3aGljaCB5b3VyIGV2ZW50IGhhbmRsZXIgY29kZSBjb21wb25lbnQgYmVsb25nc1xuICAgIGJsYWNrQ292ZXJFdmVudC5jb21wb25lbnQgPSBcIlNpY2JvQmFzZURyb3BEb3duQ29tcFwiOyAvLyBUaGlzIGlzIHRoZSBjb2RlIGZpbGUgbmFtZVxuICAgIGJsYWNrQ292ZXJFdmVudC5oYW5kbGVyID0gXCJjbGlja0JsYWNrQ292ZXJcIjtcbiAgICB0aGlzLmRyb3BEb3duQmFja0dyb3VuZCA9IHRoaXMudmlld1tcIkJhY2tncm91bmRcIl1cbiAgICB0aGlzLmJsYWNrQ292ZXIuZ2V0Q29tcG9uZW50KGNjLkJ1dHRvbikuY2xpY2tFdmVudHMucHVzaChibGFja0NvdmVyRXZlbnQpXG4gICAgLy8gbGV0IGRyb3BEb3duQmFja0dyb3VuZEV2ZW50ID0gbmV3IGNjLkNvbXBvbmVudC5FdmVudEhhbmRsZXIoKTtcbiAgICAvLyBkcm9wRG93bkJhY2tHcm91bmRFdmVudC50YXJnZXQgPSB0aGlzLm5vZGU7IC8vIFRoaXMgbm9kZSBpcyB0aGUgbm9kZSB0byB3aGljaCB5b3VyIGV2ZW50IGhhbmRsZXIgY29kZSBjb21wb25lbnQgYmVsb25nc1xuICAgIC8vIGRyb3BEb3duQmFja0dyb3VuZEV2ZW50LmNvbXBvbmVudCA9IFwiQmFzZURyb3BEb3duQ29tcFwiOyAvLyBUaGlzIGlzIHRoZSBjb2RlIGZpbGUgbmFtZVxuICAgIC8vIGRyb3BEb3duQmFja0dyb3VuZEV2ZW50LmhhbmRsZXIgPSBcIm9uQ2xpY2tcIjtcbiAgICAvLyB0aGlzLmRyb3BEb3duQmFja0dyb3VuZC5hZGRDb21wb25lbnQoY2MuQnV0dG9uKS5jbGlja0V2ZW50cy5wdXNoKGRyb3BEb3duQmFja0dyb3VuZEV2ZW50KVxuICB9XG4gIGxvYWRfYWxsX29iamVjdChyb290LCBwYXRoKSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCByb290LmNoaWxkcmVuQ291bnQ7IGkrKykge1xuICAgICAgdGhpcy52aWV3W3BhdGggKyByb290LmNoaWxkcmVuW2ldLm5hbWVdID0gcm9vdC5jaGlsZHJlbltpXTtcbiAgICAgIHRoaXMubG9hZF9hbGxfb2JqZWN0KHJvb3QuY2hpbGRyZW5baV0sIHBhdGggKyByb290LmNoaWxkcmVuW2ldLm5hbWUgKyBcIi9cIik7XG4gICAgfVxuICB9XG5cbiAgc3RhcnQoKSB7XG4gICAgdGhpcy50ZW1wbGF0ZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB0aGlzLnJlZnJlc2hTaG93blZhbHVlKCk7XG4gIH1cblxuICBvbkVuYWJsZSgpIHtcbiAgICB0aGlzLm5vZGUub24oXCJ0b3VjaGVuZFwiLCB0aGlzLm9uQ2xpY2ssIHRoaXMpO1xuICB9XG5cbiAgb25EaXNhYmxlKCkge1xuICAgIHRoaXMubm9kZS5vZmYoXCJ0b3VjaGVuZFwiLCB0aGlzLm9uQ2xpY2ssIHRoaXMpO1xuICB9XG5cbiAgcHJpdmF0ZSBjbGFtcCh2YWx1ZTogbnVtYmVyLCBtaW46IG51bWJlciwgbWF4OiBudW1iZXIpOiBudW1iZXIge1xuICAgIGlmICh2YWx1ZSA8IG1pbikgcmV0dXJuIG1pbjtcbiAgICBpZiAodmFsdWUgPiBtYXgpIHJldHVybiBtYXg7XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9XG5cbiAgcHJpdmF0ZSBjbGlja0JsYWNrQ292ZXIoKSB7XG4gICAgY29uc29sZS5sb2coYGNsaWNrQnRuYClcbiAgICB0aGlzLmhpZGUoKTtcbiAgfVxufVxuIl19