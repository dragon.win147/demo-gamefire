
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/DropDown/SicboDropDownComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '99c816qd/tH6rBtjfUMsr1R', 'SicboDropDownComp');
// Game/Sicbo_New/Scripts/Components/DropDown/SicboDropDownComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboBaseDropDownComp_1 = require("./SicboBaseDropDownComp");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboSicboDropDownComp = /** @class */ (function (_super) {
    __extends(SicboSicboDropDownComp, _super);
    function SicboSicboDropDownComp() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // you can add extra setting to dropDown here
    SicboSicboDropDownComp.prototype.onLoad = function () {
        var _this = this;
        _super.prototype.onLoad.call(this);
        this.extraShow = function () {
            _this.arrow.angle = 180;
        };
        this.extraHide = function () {
            _this.arrow.angle = 0;
        };
    };
    SicboSicboDropDownComp = __decorate([
        ccclass
    ], SicboSicboDropDownComp);
    return SicboSicboDropDownComp;
}(SicboBaseDropDownComp_1.default));
exports.default = SicboSicboDropDownComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvRHJvcERvd24vU2ljYm9Ecm9wRG93bkNvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsaUVBQTREO0FBRXRELElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQW9ELDBDQUFxQjtJQUF6RTs7SUFXQSxDQUFDO0lBVkcsNkNBQTZDO0lBQzdDLHVDQUFNLEdBQU47UUFBQSxpQkFRQztRQVBHLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBQ2YsSUFBSSxDQUFDLFNBQVMsR0FBRztZQUNiLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUMzQixDQUFDLENBQUM7UUFDRixJQUFJLENBQUMsU0FBUyxHQUFHO1lBQ2IsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFBO1FBQ3hCLENBQUMsQ0FBQztJQUNOLENBQUM7SUFWZ0Isc0JBQXNCO1FBRDFDLE9BQU87T0FDYSxzQkFBc0IsQ0FXMUM7SUFBRCw2QkFBQztDQVhELEFBV0MsQ0FYbUQsK0JBQXFCLEdBV3hFO2tCQVhvQixzQkFBc0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU2ljYm9CYXNlRHJvcERvd25Db21wIGZyb20gXCIuL1NpY2JvQmFzZURyb3BEb3duQ29tcFwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9TaWNib0Ryb3BEb3duQ29tcCBleHRlbmRzIFNpY2JvQmFzZURyb3BEb3duQ29tcCB7XG4gICAgLy8geW91IGNhbiBhZGQgZXh0cmEgc2V0dGluZyB0byBkcm9wRG93biBoZXJlXG4gICAgb25Mb2FkKCk6IHZvaWQge1xuICAgICAgICBzdXBlci5vbkxvYWQoKTtcbiAgICAgICAgdGhpcy5leHRyYVNob3cgPSAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmFycm93LmFuZ2xlID0gMTgwO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmV4dHJhSGlkZSA9ICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuYXJyb3cuYW5nbGUgPSAwXG4gICAgICAgIH07XG4gICAgfVxufVxuIl19