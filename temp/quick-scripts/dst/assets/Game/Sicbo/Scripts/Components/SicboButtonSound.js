
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboButtonSound.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3a25bRxxwpIeYRpvuZBEl6V', 'SicboButtonSound');
// Game/Sicbo_New/Scripts/Components/SicboButtonSound.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboSoundManager_1 = require("../Managers/SicboSoundManager");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboButtonSound = /** @class */ (function (_super) {
    __extends(SicboButtonSound, _super);
    function SicboButtonSound() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sound = SicboSetting_1.SicboSound.SfxClick;
        return _this;
        // update (dt) {}
    }
    SicboButtonSound.prototype.onLoad = function () {
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node;
        clickEventHandler.component = "SicboButtonSound";
        clickEventHandler.handler = "callback";
        var button = this.node.getComponent(cc.Button);
        if (button)
            button.clickEvents.push(clickEventHandler);
        var toggle = this.node.getComponent(cc.Toggle);
        if (toggle)
            toggle.clickEvents.push(clickEventHandler);
    };
    SicboButtonSound.prototype.callback = function () {
        SicboSoundManager_1.default.getInstance().play(SicboSetting_1.default.getSoundName(this.sound));
    };
    __decorate([
        property({ type: cc.Enum(SicboSetting_1.SicboSound) })
    ], SicboButtonSound.prototype, "sound", void 0);
    SicboButtonSound = __decorate([
        ccclass
    ], SicboButtonSound);
    return SicboButtonSound;
}(cc.Component));
exports.default = SicboButtonSound;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9CdXR0b25Tb3VuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVsRix3REFBbUU7QUFFbkUsbUVBQThEO0FBRXhELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQThDLG9DQUFZO0lBQTFEO1FBQUEscUVBeUJDO1FBdkJHLFdBQUssR0FBZSx5QkFBVSxDQUFDLFFBQVEsQ0FBQzs7UUFzQnhDLGlCQUFpQjtJQUNyQixDQUFDO0lBckJHLGlDQUFNLEdBQU47UUFDSSxJQUFJLGlCQUFpQixHQUFHLElBQUksRUFBRSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUN4RCxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNyQyxpQkFBaUIsQ0FBQyxTQUFTLEdBQUcsa0JBQWtCLENBQUM7UUFDakQsaUJBQWlCLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQztRQUV2QyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0MsSUFBRyxNQUFNO1lBQ0wsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUUvQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0MsSUFBRyxNQUFNO1lBQ0wsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUVuRCxDQUFDO0lBRUQsbUNBQVEsR0FBUjtRQUNJLDJCQUFpQixDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxzQkFBWSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNoRixDQUFDO0lBcEJEO1FBREMsUUFBUSxDQUFDLEVBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMseUJBQVUsQ0FBQyxFQUFDLENBQUM7bURBQ0U7SUFGdkIsZ0JBQWdCO1FBRHBDLE9BQU87T0FDYSxnQkFBZ0IsQ0F5QnBDO0lBQUQsdUJBQUM7Q0F6QkQsQUF5QkMsQ0F6QjZDLEVBQUUsQ0FBQyxTQUFTLEdBeUJ6RDtrQkF6Qm9CLGdCQUFnQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgU2ljYm9TZXR0aW5nLCB7IFNpY2JvU291bmQgfSBmcm9tIFwiLi4vU2V0dGluZy9TaWNib1NldHRpbmdcIjtcbmltcG9ydCBTaWNib0NvbnRyb2xsZXIgZnJvbSBcIi4uL0NvbnRyb2xsZXJzL1NpY2JvQ29udHJvbGxlclwiO1xuaW1wb3J0IFNpY2JvU291bmRNYW5hZ2VyIGZyb20gXCIuLi9NYW5hZ2Vycy9TaWNib1NvdW5kTWFuYWdlclwiO1xuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvQnV0dG9uU291bmQgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIEBwcm9wZXJ0eSh7dHlwZTogY2MuRW51bShTaWNib1NvdW5kKX0pXG4gICAgc291bmQ6IFNpY2JvU291bmQgPSBTaWNib1NvdW5kLlNmeENsaWNrO1xuXG4gICAgb25Mb2FkICgpIHtcbiAgICAgICAgdmFyIGNsaWNrRXZlbnRIYW5kbGVyID0gbmV3IGNjLkNvbXBvbmVudC5FdmVudEhhbmRsZXIoKTtcbiAgICAgICAgY2xpY2tFdmVudEhhbmRsZXIudGFyZ2V0ID0gdGhpcy5ub2RlOyBcbiAgICAgICAgY2xpY2tFdmVudEhhbmRsZXIuY29tcG9uZW50ID0gXCJTaWNib0J1dHRvblNvdW5kXCI7XG4gICAgICAgIGNsaWNrRXZlbnRIYW5kbGVyLmhhbmRsZXIgPSBcImNhbGxiYWNrXCI7XG5cbiAgICAgICAgdmFyIGJ1dHRvbiA9IHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuQnV0dG9uKTtcbiAgICAgICAgaWYoYnV0dG9uKVxuICAgICAgICAgICAgYnV0dG9uLmNsaWNrRXZlbnRzLnB1c2goY2xpY2tFdmVudEhhbmRsZXIpO1xuXG4gICAgICAgIHZhciB0b2dnbGUgPSB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlRvZ2dsZSk7XG4gICAgICAgIGlmKHRvZ2dsZSlcbiAgICAgICAgICAgIHRvZ2dsZS5jbGlja0V2ZW50cy5wdXNoKGNsaWNrRXZlbnRIYW5kbGVyKTtcblxuICAgIH1cblxuICAgIGNhbGxiYWNrICgpIHtcbiAgICAgICAgU2ljYm9Tb3VuZE1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5wbGF5KFNpY2JvU2V0dGluZy5nZXRTb3VuZE5hbWUodGhpcy5zb3VuZCkpO1xuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG59XG4iXX0=