
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/DropDown/SicboBaseDropDownOptionDataComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bf993nd8gRNVK5ma3UZkBY/', 'SicboBaseDropDownOptionDataComp');
// Game/Sicbo_New/Scripts/Components/DropDown/SicboBaseDropDownOptionDataComp.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboBaseDropDownOptionDataComp = /** @class */ (function () {
    function SicboBaseDropDownOptionDataComp(obj) {
        this.optionString = "";
        this.optionSf = null;
        this.clickFunc = null;
        var optionString = obj.optionString, optionSf = obj.optionSf, clickFunc = obj.clickFunc;
        this.optionString = optionString;
        this.optionSf = optionSf;
        this.clickFunc = clickFunc;
    }
    return SicboBaseDropDownOptionDataComp;
}());
exports.default = SicboBaseDropDownOptionDataComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvRHJvcERvd24vU2ljYm9CYXNlRHJvcERvd25PcHRpb25EYXRhQ29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFNLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRTVDO0lBQ0UseUNBQVksR0FBMEM7UUFNL0MsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFDMUIsYUFBUSxHQUFtQixJQUFJLENBQUM7UUFFdkMsY0FBUyxHQUFhLElBQUksQ0FBQTtRQVJoQixJQUFBLFlBQVksR0FBMEIsR0FBRyxhQUE3QixFQUFFLFFBQVEsR0FBZ0IsR0FBRyxTQUFuQixFQUFFLFNBQVMsR0FBSyxHQUFHLFVBQVIsQ0FBUztRQUNsRCxJQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQTtRQUNoQyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQTtRQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQTtJQUM1QixDQUFDO0lBS0gsc0NBQUM7QUFBRCxDQVhBLEFBV0MsSUFBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvQmFzZURyb3BEb3duT3B0aW9uRGF0YUNvbXAge1xuICBjb25zdHJ1Y3RvcihvYmo6IHsgb3B0aW9uU3RyaW5nLCBvcHRpb25TZiwgY2xpY2tGdW5jIH0pIHtcbiAgICBjb25zdCB7IG9wdGlvblN0cmluZywgb3B0aW9uU2YsIGNsaWNrRnVuYyB9ID0gb2JqO1xuICAgIHRoaXMub3B0aW9uU3RyaW5nID0gb3B0aW9uU3RyaW5nXG4gICAgdGhpcy5vcHRpb25TZiA9IG9wdGlvblNmXG4gICAgdGhpcy5jbGlja0Z1bmMgPSBjbGlja0Z1bmNcbiAgfVxuICBwdWJsaWMgb3B0aW9uU3RyaW5nOiBzdHJpbmcgPSBcIlwiO1xuICBwdWJsaWMgb3B0aW9uU2Y6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcblxuICBjbGlja0Z1bmM6IEZ1bmN0aW9uID0gbnVsbFxufVxuIl19