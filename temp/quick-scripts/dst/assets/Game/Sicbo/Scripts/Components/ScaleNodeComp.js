
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/ScaleNodeComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '947b9avXPpP/ZuSNV+4EGFr', 'ScaleNodeComp');
// Game/Sicbo_New/Scripts/Components/ScaleNodeComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode;
var ScaleNodeComp = /** @class */ (function (_super) {
    __extends(ScaleNodeComp, _super);
    function ScaleNodeComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.maxWidth = 20;
        _this.maxScale = 1;
        return _this;
    }
    ScaleNodeComp.prototype.update = function (dt) {
        this.updateScale();
    };
    ScaleNodeComp.prototype.updateScale = function () {
        if (this.node.width <= 0)
            this.node.scale = this.maxScale;
        var scale = Math.min(this.maxScale, this.maxWidth * 1.0 / this.node.width);
        this.node.scale = scale;
    };
    __decorate([
        property()
    ], ScaleNodeComp.prototype, "maxWidth", void 0);
    __decorate([
        property()
    ], ScaleNodeComp.prototype, "maxScale", void 0);
    ScaleNodeComp = __decorate([
        ccclass,
        executeInEditMode
    ], ScaleNodeComp);
    return ScaleNodeComp;
}(cc.Component));
exports.default = ScaleNodeComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2NhbGVOb2RlQ29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUU1RSxJQUFBLEtBQXlDLEVBQUUsQ0FBQyxVQUFVLEVBQXJELE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBQSxFQUFFLGlCQUFpQix1QkFBaUIsQ0FBQztBQUk3RDtJQUEyQyxpQ0FBWTtJQUF2RDtRQUFBLHFFQWlCQztRQWZHLGNBQVEsR0FBVyxFQUFFLENBQUM7UUFHdEIsY0FBUSxHQUFXLENBQUMsQ0FBQzs7SUFZekIsQ0FBQztJQVZHLDhCQUFNLEdBQU4sVUFBUSxFQUFFO1FBQ04sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxtQ0FBVyxHQUFYO1FBQ0ksSUFBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBRSxDQUFDO1lBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUV2RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLEdBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQWREO1FBREMsUUFBUSxFQUFFO21EQUNXO0lBR3RCO1FBREMsUUFBUSxFQUFFO21EQUNVO0lBTEosYUFBYTtRQUZqQyxPQUFPO1FBQ1AsaUJBQWlCO09BQ0csYUFBYSxDQWlCakM7SUFBRCxvQkFBQztDQWpCRCxBQWlCQyxDQWpCMEMsRUFBRSxDQUFDLFNBQVMsR0FpQnREO2tCQWpCb0IsYUFBYSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHksIGV4ZWN1dGVJbkVkaXRNb2RlfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5AZXhlY3V0ZUluRWRpdE1vZGVcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNjYWxlTm9kZUNvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIEBwcm9wZXJ0eSgpXG4gICAgbWF4V2lkdGg6IG51bWJlciA9IDIwO1xuXG4gICAgQHByb3BlcnR5KClcbiAgICBtYXhTY2FsZTogbnVtYmVyID0gMTtcblxuICAgIHVwZGF0ZSAoZHQpIHtcbiAgICAgICAgdGhpcy51cGRhdGVTY2FsZSgpO1xuICAgIH1cblxuICAgIHVwZGF0ZVNjYWxlKCl7XG4gICAgICAgIGlmKHRoaXMubm9kZS53aWR0aDw9MCkgdGhpcy5ub2RlLnNjYWxlID0gdGhpcy5tYXhTY2FsZTtcblxuICAgICAgICBsZXQgc2NhbGUgPSBNYXRoLm1pbih0aGlzLm1heFNjYWxlLCB0aGlzLm1heFdpZHRoICogMS4wLyB0aGlzLm5vZGUud2lkdGgpO1xuICAgICAgICB0aGlzLm5vZGUuc2NhbGUgPSBzY2FsZTtcbiAgICB9XG59XG4iXX0=