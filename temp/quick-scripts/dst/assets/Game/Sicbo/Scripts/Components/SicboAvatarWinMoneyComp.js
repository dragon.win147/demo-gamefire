
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboAvatarWinMoneyComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cf745trf/hHCY816IjaXx/y', 'SicboAvatarWinMoneyComp');
// Game/Sicbo_New/Scripts/Components/SicboAvatarWinMoneyComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboMoneyFormatComp_1 = require("../RNGCommons/SicboMoneyFormatComp");
var SicboSkeletonUtils_1 = require("../RNGCommons/Utils/SicboSkeletonUtils");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboAvatarWinMoneyComp = /** @class */ (function (_super) {
    __extends(SicboAvatarWinMoneyComp, _super);
    function SicboAvatarWinMoneyComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.animationBack = null;
        _this.animationFront = null;
        _this.money = null;
        _this.backAnimation = "win_avata";
        _this.frontAnimation = "win_avata2";
        _this.showTime = 1;
        _this.outTime = .5;
        _this.isShowing = false;
        return _this;
    }
    SicboAvatarWinMoneyComp.prototype.onLoad = function () {
        this.animationBack.node.active = false;
        this.animationFront.node.active = false;
        this.isShowing = false;
    };
    SicboAvatarWinMoneyComp.prototype.show = function (money) {
        if (this.isShowing)
            return;
        this.isShowing = true;
        this.animationBack.node.active = true;
        this.animationFront.node.active = true;
        this.money.setMoney(money, true);
        SicboSkeletonUtils_1.default.setAnimation(this.animationBack, this.backAnimation, false);
        SicboSkeletonUtils_1.default.setAnimation(this.animationFront, this.frontAnimation, false);
        this.money.node.opacity = 255;
        var self = this;
        cc.tween(self.money.node)
            .delay(self.showTime)
            .to(self.outTime, { opacity: 0 })
            .call(function () { return self.isShowing = false; })
            .start();
    };
    SicboAvatarWinMoneyComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.money.node);
    };
    __decorate([
        property(sp.Skeleton)
    ], SicboAvatarWinMoneyComp.prototype, "animationBack", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboAvatarWinMoneyComp.prototype, "animationFront", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboAvatarWinMoneyComp.prototype, "money", void 0);
    SicboAvatarWinMoneyComp = __decorate([
        ccclass
    ], SicboAvatarWinMoneyComp);
    return SicboAvatarWinMoneyComp;
}(cc.Component));
exports.default = SicboAvatarWinMoneyComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9BdmF0YXJXaW5Nb25leUNvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYsMkVBQXNFO0FBQ3RFLDZFQUF3RTtBQUVsRSxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFxRCwyQ0FBWTtJQUFqRTtRQUFBLHFFQThDQztRQTVDRyxtQkFBYSxHQUFnQixJQUFJLENBQUM7UUFHbEMsb0JBQWMsR0FBZ0IsSUFBSSxDQUFDO1FBR25DLFdBQUssR0FBeUIsSUFBSSxDQUFDO1FBRW5DLG1CQUFhLEdBQUcsV0FBVyxDQUFDO1FBQzVCLG9CQUFjLEdBQUcsWUFBWSxDQUFDO1FBRXRCLGNBQVEsR0FBRyxDQUFDLENBQUM7UUFDYixhQUFPLEdBQUksRUFBRSxDQUFDO1FBRXRCLGVBQVMsR0FBWSxLQUFLLENBQUM7O0lBOEIvQixDQUFDO0lBNUJHLHdDQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDeEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQUVELHNDQUFJLEdBQUosVUFBSyxLQUFhO1FBQ2QsSUFBRyxJQUFJLENBQUMsU0FBUztZQUFFLE9BQU87UUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN0QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVqQyw0QkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQy9FLDRCQUFrQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDakYsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUU5QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQzthQUNwQixLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUNwQixFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQzthQUM5QixJQUFJLENBQUMsY0FBSSxPQUFBLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxFQUF0QixDQUFzQixDQUFDO2FBQ2hDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFFRCwyQ0FBUyxHQUFUO1FBQ0ksRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBM0NEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7a0VBQ1k7SUFHbEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQzttRUFDYTtJQUduQztRQURDLFFBQVEsQ0FBQyw4QkFBb0IsQ0FBQzswREFDSTtJQVJsQix1QkFBdUI7UUFEM0MsT0FBTztPQUNhLHVCQUF1QixDQThDM0M7SUFBRCw4QkFBQztDQTlDRCxBQThDQyxDQTlDb0QsRUFBRSxDQUFDLFNBQVMsR0E4Q2hFO2tCQTlDb0IsdUJBQXVCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBTaWNib01vbmV5Rm9ybWF0Q29tcCBmcm9tIFwiLi4vUk5HQ29tbW9ucy9TaWNib01vbmV5Rm9ybWF0Q29tcFwiO1xuaW1wb3J0IFNpY2JvU2tlbGV0b25VdGlscyBmcm9tIFwiLi4vUk5HQ29tbW9ucy9VdGlscy9TaWNib1NrZWxldG9uVXRpbHNcIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib0F2YXRhcldpbk1vbmV5Q29tcCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KHNwLlNrZWxldG9uKVxuICAgIGFuaW1hdGlvbkJhY2s6IHNwLlNrZWxldG9uID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShzcC5Ta2VsZXRvbilcbiAgICBhbmltYXRpb25Gcm9udDogc3AuU2tlbGV0b24gPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KFNpY2JvTW9uZXlGb3JtYXRDb21wKVxuICAgIG1vbmV5OiBTaWNib01vbmV5Rm9ybWF0Q29tcCA9IG51bGw7XG5cbiAgICBiYWNrQW5pbWF0aW9uID0gXCJ3aW5fYXZhdGFcIjtcbiAgICBmcm9udEFuaW1hdGlvbiA9IFwid2luX2F2YXRhMlwiO1xuICAgIFxuICAgIHByaXZhdGUgc2hvd1RpbWUgPSAxO1xuICAgIHByaXZhdGUgb3V0VGltZSA9ICAuNTtcblxuICAgIGlzU2hvd2luZzogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgb25Mb2FkKCl7XG4gICAgICAgIHRoaXMuYW5pbWF0aW9uQmFjay5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmFuaW1hdGlvbkZyb250Lm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuaXNTaG93aW5nID0gZmFsc2U7XG4gICAgfVxuICBcbiAgICBzaG93KG1vbmV5OiBudW1iZXIpeyAgIFxuICAgICAgICBpZih0aGlzLmlzU2hvd2luZykgcmV0dXJuO1xuICAgICAgICB0aGlzLmlzU2hvd2luZyA9IHRydWU7XG4gICAgICAgIHRoaXMuYW5pbWF0aW9uQmFjay5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIHRoaXMuYW5pbWF0aW9uRnJvbnQubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB0aGlzLm1vbmV5LnNldE1vbmV5KG1vbmV5LCB0cnVlKTsgICAgICAgIFxuXG4gICAgICAgIFNpY2JvU2tlbGV0b25VdGlscy5zZXRBbmltYXRpb24odGhpcy5hbmltYXRpb25CYWNrLCB0aGlzLmJhY2tBbmltYXRpb24sIGZhbHNlKTtcbiAgICAgICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLmFuaW1hdGlvbkZyb250LCB0aGlzLmZyb250QW5pbWF0aW9uLCBmYWxzZSk7XG4gICAgICAgIHRoaXMubW9uZXkubm9kZS5vcGFjaXR5ID0gMjU1O1xuXG4gICAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgY2MudHdlZW4oc2VsZi5tb25leS5ub2RlKVxuICAgICAgICAgICAgLmRlbGF5KHNlbGYuc2hvd1RpbWUpXG4gICAgICAgICAgICAudG8oc2VsZi5vdXRUaW1lLCB7b3BhY2l0eTogMH0pXG4gICAgICAgICAgICAuY2FsbCgoKT0+c2VsZi5pc1Nob3dpbmcgPSBmYWxzZSlcbiAgICAgICAgICAgIC5zdGFydCgpO1xuICAgIH0gIFxuXG4gICAgb25EZXN0cm95KCl7XG4gICAgICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLm1vbmV5Lm5vZGUpO1xuICAgIH1cbn1cbiJdfQ==