
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/States/SicboClockStateComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4b8ae3J+clL5IQW6jPjOf3w', 'SicboClockStateComp');
// Game/Sicbo_New/Scripts/Components/States/SicboClockStateComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), State = _a.State, Status = _a.Status;
var SicboSetting_1 = require("../../Setting/SicboSetting");
var SicboController_1 = require("../../Controllers/SicboController");
var SicboSkeletonUtils_1 = require("../../RNGCommons/Utils/SicboSkeletonUtils");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboClockStateComp = /** @class */ (function (_super) {
    __extends(SicboClockStateComp, _super);
    function SicboClockStateComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.countDownText = null;
        _this.ringAnimation = null;
        _this.RING_TIME = 5;
        _this.elapseTime = 0;
        _this.totalDuration = 0;
        // lastRemainingTime = -1;
        _this.isRushSfxPlaying = false;
        _this.isClockPaused = true;
        return _this;
    }
    SicboClockStateComp.prototype.onLoad = function () {
        // this.node.active = false;
        this.stopAnimation();
    };
    SicboClockStateComp.prototype.exitState = function (status) {
        this.stopAnimation();
        // this.node.active = false;
        // this.lastRemainingTime = -1;
        this.isRushSfxPlaying = false;
        if (status == Status.BETTING) {
            // SicboController.Instance.playSfx(SicboSound.SfxStopBetting);
            SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxRush);
        }
    };
    SicboClockStateComp.prototype.updateState = function (state) {
        var status = state.getStatus();
        this.elapseTime = state.getStageTime();
        if (status != Status.BETTING && status != Status.PAYING)
            return;
        var remainingTime = Math.max(Math.ceil((this.totalDuration - this.elapseTime) / 1000), 0);
        this.countDownText.string = remainingTime.toString();
        if (status == Status.BETTING && remainingTime <= this.RING_TIME) {
            // if(remainingTime == 0){
            //     this.stopAnimation();
            //     return;
            // }
            if (this.isRushSfxPlaying == false) {
                SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxRush, true);
                this.isRushSfxPlaying = true;
            }
            if (this.isClockPaused) {
                this.isClockPaused = false;
                // this.ringAnimation.animation = "animation";
                SicboSkeletonUtils_1.default.setAnimation(this.ringAnimation, "animation", true);
            }
            if (this.elapseTime % 1000 == 0) {
                // //cc.log("PlaySfx SfxTictoc");
                // SicboController.Instance.playSfx(SicboSound.SfxTictoc);
                // this.lastRemainingTime = remainingTime;
                cc.tween(this.countDownText.node).to(0.1, { scale: 2 }).to(0.1, { scale: 1 }).start();
            }
        }
    };
    SicboClockStateComp.prototype.startState = function (state, totalDuration) {
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        this.elapseTime = elapseTime * 1000;
        this.totalDuration = totalDuration;
        switch (status) {
            case Status.BETTING:
            case Status.PAYING:
                // this.node.active = true;
                break;
            default:
                // this.node.active = false;
                this.stopAnimation();
                break;
        }
    };
    SicboClockStateComp.prototype.stopAnimation = function () {
        SicboSkeletonUtils_1.default.setAnimation(this.ringAnimation, "idle", true);
        this.isClockPaused = true;
        this.countDownText.node.scale = 1;
        this.countDownText.string = "0";
        // this.ringAnimation.setBonesToSetupPose();
    };
    SicboClockStateComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.countDownText.node);
    };
    SicboClockStateComp.prototype.onHide = function () {
        cc.Tween.stopAllByTarget(this.countDownText.node);
        this.stopAnimation();
        this.isRushSfxPlaying = false;
        SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxRush);
    };
    __decorate([
        property(cc.Label)
    ], SicboClockStateComp.prototype, "countDownText", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboClockStateComp.prototype, "ringAnimation", void 0);
    SicboClockStateComp = __decorate([
        ccclass
    ], SicboClockStateComp);
    return SicboClockStateComp;
}(cc.Component));
exports.default = SicboClockStateComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU3RhdGVzL1NpY2JvQ2xvY2tTdGF0ZUNvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYsd0VBQW1FO0FBRTdELElBQUEsS0FFTSw0QkFBa0IsQ0FBQyxVQUFVLEVBQUUsRUFEekMsS0FBSyxXQUFBLEVBQ0wsTUFBTSxZQUFtQyxDQUFDO0FBQzVDLDJEQUF3RDtBQUN4RCxxRUFBZ0U7QUFDaEUsZ0ZBQTJFO0FBR3JFLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQWlELHVDQUFZO0lBQTdEO1FBQUEscUVBMkdDO1FBekdDLG1CQUFhLEdBQWEsSUFBSSxDQUFDO1FBRy9CLG1CQUFhLEdBQWdCLElBQUksQ0FBQztRQUVsQyxlQUFTLEdBQVcsQ0FBQyxDQUFDO1FBRXRCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRTFCLDBCQUEwQjtRQUMxQixzQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDekIsbUJBQWEsR0FBWSxJQUFJLENBQUM7O0lBNkZoQyxDQUFDO0lBM0ZDLG9DQUFNLEdBQU47UUFDRSw0QkFBNEI7UUFDNUIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCx1Q0FBUyxHQUFULFVBQVUsTUFBTTtRQUNkLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQiw0QkFBNEI7UUFDNUIsK0JBQStCO1FBQy9CLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFFOUIsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTtZQUM1QiwrREFBK0Q7WUFDL0QseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHlCQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDdEQ7SUFDSCxDQUFDO0lBRUQseUNBQVcsR0FBWCxVQUFZLEtBQWlDO1FBQzNDLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUV2QyxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTTtZQUFFLE9BQU87UUFDaEUsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFMUYsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3JELElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxPQUFPLElBQUksYUFBYSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDL0QsMEJBQTBCO1lBQzFCLDRCQUE0QjtZQUM1QixjQUFjO1lBQ2QsSUFBSTtZQUNKLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLEtBQUssRUFBRTtnQkFDbEMseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHlCQUFVLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUMzRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2FBQzlCO1lBRUQsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztnQkFDM0IsOENBQThDO2dCQUM5Qyw0QkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDeEU7WUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxJQUFJLENBQUMsRUFBRTtnQkFDL0IsaUNBQWlDO2dCQUNqQywwREFBMEQ7Z0JBQzFELDBDQUEwQztnQkFDMUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDdkY7U0FDRjtJQUNILENBQUM7SUFFRCx3Q0FBVSxHQUFWLFVBQVcsS0FBaUMsRUFBRSxhQUFxQjtRQUNqRSxJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDL0IsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDLFlBQVksRUFBRSxHQUFHLElBQUksQ0FBQztRQUU3QyxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7UUFFbkMsUUFBUSxNQUFNLEVBQUU7WUFDZCxLQUFLLE1BQU0sQ0FBQyxPQUFPLENBQUM7WUFBQyxLQUFLLE1BQU0sQ0FBQyxNQUFNO2dCQUNyQywyQkFBMkI7Z0JBQzNCLE1BQU07WUFFUjtnQkFDRSw0QkFBNEI7Z0JBQzVCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDckIsTUFBTTtTQUNUO0lBQ0gsQ0FBQztJQUVELDJDQUFhLEdBQWI7UUFDRSw0QkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFbEUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFFMUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7UUFFaEMsNENBQTRDO0lBQzlDLENBQUM7SUFFRCx1Q0FBUyxHQUFUO1FBQ0UsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsb0NBQU0sR0FBTjtRQUNFLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRXJCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDOUIseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHlCQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQXhHRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzhEQUNZO0lBRy9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7OERBQ1k7SUFMZixtQkFBbUI7UUFEdkMsT0FBTztPQUNhLG1CQUFtQixDQTJHdkM7SUFBRCwwQkFBQztDQTNHRCxBQTJHQyxDQTNHZ0QsRUFBRSxDQUFDLFNBQVMsR0EyRzVEO2tCQTNHb0IsbUJBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBTaWNib01vZHVsZUFkYXB0ZXIgZnJvbSBcIi4uLy4uLy4uLy4uLy4uL1NpY2JvTW9kdWxlQWRhcHRlclwiO1xuXG5jb25zdCB7XG4gIFN0YXRlLFxuICBTdGF0dXN9ID0gU2ljYm9Nb2R1bGVBZGFwdGVyLmdldEFsbFJlZnMoKTtcbmltcG9ydCB7IFNpY2JvU291bmQgfSBmcm9tIFwiLi4vLi4vU2V0dGluZy9TaWNib1NldHRpbmdcIjtcbmltcG9ydCBTaWNib0NvbnRyb2xsZXIgZnJvbSBcIi4uLy4uL0NvbnRyb2xsZXJzL1NpY2JvQ29udHJvbGxlclwiO1xuaW1wb3J0IFNpY2JvU2tlbGV0b25VdGlscyBmcm9tIFwiLi4vLi4vUk5HQ29tbW9ucy9VdGlscy9TaWNib1NrZWxldG9uVXRpbHNcIjtcbmltcG9ydCBJU2ljYm9TdGF0ZSBmcm9tIFwiLi9JU2ljYm9TdGF0ZVwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9DbG9ja1N0YXRlQ29tcCBleHRlbmRzIGNjLkNvbXBvbmVudCBpbXBsZW1lbnRzIElTaWNib1N0YXRlIHtcbiAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICBjb3VudERvd25UZXh0OiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KHNwLlNrZWxldG9uKVxuICByaW5nQW5pbWF0aW9uOiBzcC5Ta2VsZXRvbiA9IG51bGw7XG5cbiAgUklOR19USU1FOiBudW1iZXIgPSA1O1xuXG4gIGVsYXBzZVRpbWU6IG51bWJlciA9IDA7XG4gIHRvdGFsRHVyYXRpb246IG51bWJlciA9IDA7XG5cbiAgLy8gbGFzdFJlbWFpbmluZ1RpbWUgPSAtMTtcbiAgaXNSdXNoU2Z4UGxheWluZyA9IGZhbHNlO1xuICBpc0Nsb2NrUGF1c2VkOiBib29sZWFuID0gdHJ1ZTtcblxuICBvbkxvYWQoKSB7XG4gICAgLy8gdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgIHRoaXMuc3RvcEFuaW1hdGlvbigpO1xuICB9XG5cbiAgZXhpdFN0YXRlKHN0YXR1cyk6IHZvaWQge1xuICAgIHRoaXMuc3RvcEFuaW1hdGlvbigpO1xuICAgIC8vIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAvLyB0aGlzLmxhc3RSZW1haW5pbmdUaW1lID0gLTE7XG4gICAgdGhpcy5pc1J1c2hTZnhQbGF5aW5nID0gZmFsc2U7XG5cbiAgICBpZiAoc3RhdHVzID09IFN0YXR1cy5CRVRUSU5HKSB7XG4gICAgICAvLyBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UucGxheVNmeChTaWNib1NvdW5kLlNmeFN0b3BCZXR0aW5nKTtcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5zdG9wU2Z4KFNpY2JvU291bmQuU2Z4UnVzaCk7XG4gICAgfVxuICB9XG5cbiAgdXBkYXRlU3RhdGUoc3RhdGU6IEluc3RhbmNlVHlwZTx0eXBlb2YgU3RhdGU+KSB7XG4gICAgbGV0IHN0YXR1cyA9IHN0YXRlLmdldFN0YXR1cygpO1xuICAgIHRoaXMuZWxhcHNlVGltZSA9IHN0YXRlLmdldFN0YWdlVGltZSgpO1xuXG4gICAgaWYgKHN0YXR1cyAhPSBTdGF0dXMuQkVUVElORyAmJiBzdGF0dXMgIT0gU3RhdHVzLlBBWUlORykgcmV0dXJuO1xuICAgIGxldCByZW1haW5pbmdUaW1lID0gTWF0aC5tYXgoTWF0aC5jZWlsKCh0aGlzLnRvdGFsRHVyYXRpb24gLSB0aGlzLmVsYXBzZVRpbWUpIC8gMTAwMCksIDApO1xuXG4gICAgdGhpcy5jb3VudERvd25UZXh0LnN0cmluZyA9IHJlbWFpbmluZ1RpbWUudG9TdHJpbmcoKTtcbiAgICBpZiAoc3RhdHVzID09IFN0YXR1cy5CRVRUSU5HICYmIHJlbWFpbmluZ1RpbWUgPD0gdGhpcy5SSU5HX1RJTUUpIHtcbiAgICAgIC8vIGlmKHJlbWFpbmluZ1RpbWUgPT0gMCl7XG4gICAgICAvLyAgICAgdGhpcy5zdG9wQW5pbWF0aW9uKCk7XG4gICAgICAvLyAgICAgcmV0dXJuO1xuICAgICAgLy8gfVxuICAgICAgaWYgKHRoaXMuaXNSdXNoU2Z4UGxheWluZyA9PSBmYWxzZSkge1xuICAgICAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UucGxheVNmeChTaWNib1NvdW5kLlNmeFJ1c2gsIHRydWUpO1xuICAgICAgICB0aGlzLmlzUnVzaFNmeFBsYXlpbmcgPSB0cnVlO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5pc0Nsb2NrUGF1c2VkKSB7XG4gICAgICAgIHRoaXMuaXNDbG9ja1BhdXNlZCA9IGZhbHNlO1xuICAgICAgICAvLyB0aGlzLnJpbmdBbmltYXRpb24uYW5pbWF0aW9uID0gXCJhbmltYXRpb25cIjtcbiAgICAgICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLnJpbmdBbmltYXRpb24sIFwiYW5pbWF0aW9uXCIsIHRydWUpO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5lbGFwc2VUaW1lICUgMTAwMCA9PSAwKSB7XG4gICAgICAgIC8vIC8vY2MubG9nKFwiUGxheVNmeCBTZnhUaWN0b2NcIik7XG4gICAgICAgIC8vIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4VGljdG9jKTtcbiAgICAgICAgLy8gdGhpcy5sYXN0UmVtYWluaW5nVGltZSA9IHJlbWFpbmluZ1RpbWU7XG4gICAgICAgIGNjLnR3ZWVuKHRoaXMuY291bnREb3duVGV4dC5ub2RlKS50bygwLjEsIHsgc2NhbGU6IDIgfSkudG8oMC4xLCB7IHNjYWxlOiAxIH0pLnN0YXJ0KCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgc3RhcnRTdGF0ZShzdGF0ZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGF0ZT4sIHRvdGFsRHVyYXRpb246IG51bWJlcik6IHZvaWQge1xuICAgIGxldCBzdGF0dXMgPSBzdGF0ZS5nZXRTdGF0dXMoKTtcbiAgICBsZXQgZWxhcHNlVGltZSA9IHN0YXRlLmdldFN0YWdlVGltZSgpIC8gMTAwMDtcblxuICAgIHRoaXMuZWxhcHNlVGltZSA9IGVsYXBzZVRpbWUgKiAxMDAwO1xuICAgIHRoaXMudG90YWxEdXJhdGlvbiA9IHRvdGFsRHVyYXRpb247XG5cbiAgICBzd2l0Y2ggKHN0YXR1cykge1xuICAgICAgY2FzZSBTdGF0dXMuQkVUVElORzogY2FzZSBTdGF0dXMuUEFZSU5HOlxuICAgICAgICAvLyB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIC8vIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zdG9wQW5pbWF0aW9uKCk7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIHN0b3BBbmltYXRpb24oKSB7XG4gICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLnJpbmdBbmltYXRpb24sIFwiaWRsZVwiLCB0cnVlKTtcblxuICAgIHRoaXMuaXNDbG9ja1BhdXNlZCA9IHRydWU7XG5cbiAgICB0aGlzLmNvdW50RG93blRleHQubm9kZS5zY2FsZSA9IDE7XG4gICAgdGhpcy5jb3VudERvd25UZXh0LnN0cmluZyA9IFwiMFwiO1xuICAgIFxuICAgIC8vIHRoaXMucmluZ0FuaW1hdGlvbi5zZXRCb25lc1RvU2V0dXBQb3NlKCk7XG4gIH1cblxuICBvbkRlc3Ryb3koKSB7XG4gICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMuY291bnREb3duVGV4dC5ub2RlKTtcbiAgfVxuXG4gIG9uSGlkZSgpOiB2b2lkIHtcbiAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5jb3VudERvd25UZXh0Lm5vZGUpO1xuICAgIHRoaXMuc3RvcEFuaW1hdGlvbigpOyAgICBcblxuICAgIHRoaXMuaXNSdXNoU2Z4UGxheWluZyA9IGZhbHNlO1xuICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5zdG9wU2Z4KFNpY2JvU291bmQuU2Z4UnVzaCk7XG4gIH1cbn1cbiJdfQ==