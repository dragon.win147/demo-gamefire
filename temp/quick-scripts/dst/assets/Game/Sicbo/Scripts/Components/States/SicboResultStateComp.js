
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/States/SicboResultStateComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a3fe8QkQjhCSr2gVIiLftIM', 'SicboResultStateComp');
// Game/Sicbo_New/Scripts/Components/States/SicboResultStateComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), GetChannelHistoryReply = _a.GetChannelHistoryReply, Door = _a.Door, State = _a.State, Result = _a.Result, Status = _a.Status, ChannelHistoryMetadata = _a.ChannelHistoryMetadata;
var SicboSetting_1 = require("../../Setting/SicboSetting");
var SicboDiceConfig_1 = require("../../Configs/SicboDiceConfig");
var SicboController_1 = require("../../Controllers/SicboController");
var SicboHelper_1 = require("../../Helpers/SicboHelper");
var SicboBetComp_1 = require("../SicboBetComp");
var SicboLatestResultComp_1 = require("../SicboLatestResultComp");
var SicboNotifyComp_1 = require("../SicboNotifyComp");
var SicboSlotWinMoneyComp_1 = require("../SicboSlotWinMoneyComp");
var SicboUIComp_1 = require("../SicboUIComp");
var SicboPortalAdapter_1 = require("../../SicboPortalAdapter");
var SicboGameUtils_1 = require("../../RNGCommons/Utils/SicboGameUtils");
var SicboSkeletonUtils_1 = require("../../RNGCommons/Utils/SicboSkeletonUtils");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboResultStateComp = /** @class */ (function (_super) {
    __extends(SicboResultStateComp, _super);
    function SicboResultStateComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.betCompNode = null;
        _this.betComp = null;
        _this.winFxShinnyNode = null;
        _this.chenAnimation = null;
        _this.chenSmallRoot = null;
        _this.chenZoomInRoot = null;
        _this.diceSprites = [];
        _this.latestResultComp = null;
        _this.dealerSkeleton = null;
        _this.slotWinMoneySmallPrefab = null;
        _this.slotWinMoneyLargePrefab = null;
        _this.notifyComp = null;
        _this.winFxSkeletons = [];
        _this.winFxShinnySkeletons = [];
        _this.isWinFxPlaying = false;
        _this.winFxTween = null;
        _this.wonDoors = [];
        _this.wonDoorsFiltered = [];
        _this.items = [];
        _this.chenOpenAnim = "ChenOpen";
        _this.chenIdleAnim = "ChenIdle";
        // private chenCloseAnim = "ChenClose";
        _this.chenRollDiceAnim = "chenRollDice";
        //NOTE startTime tinh tu khi state bat dau
        //ANCHOR Status.RESULTING start time
        _this.chenRollDiceStartTime = 0.1;
        _this.chenRollDiceSfxStartTime = _this.chenRollDiceStartTime + 0.2;
        _this.chenRollDiceSfxStopTime = _this.chenRollDiceSfxStartTime + 0.8;
        _this.chenOpenStartTime = 0.5; //NOTE 1;
        _this.chenOpenSfxStartTime = _this.chenOpenStartTime + 1.5;
        // private chenCloseStartTime = 0;
        _this.chenIdleStartTime = _this.chenOpenStartTime + 5;
        _this.dicesFlyToLatestResultStartTime = _this.chenIdleStartTime + 0.7;
        _this.doorWinFxShinnyStartTime = _this.chenIdleStartTime + 0.5;
        _this.doorWinFxStartTime = _this.chenIdleStartTime + +2.1;
        //ANCHOR Status.PAYING start time
        _this.chipLoseStartTime = 0;
        _this.chipStackStartTime = 1;
        _this.returnChipStartTime = 3 - 1;
        _this.payingStartTime = 6 - 1;
        _this.isPayingPlaying = false;
        //other user group key
        _this.otherGroupKey = "otherGroupKey";
        _this.winMoneyAtSlots = [];
        return _this;
    }
    SicboResultStateComp.prototype.onLoad = function () {
        this.betComp = this.betCompNode.getComponent(SicboBetComp_1.default);
        this.winFxShinnySkeletons = this.winFxShinnyNode.getComponentsInChildren(sp.Skeleton);
        this.winFxSkeletons = this.winFxShinnySkeletons;
        this.hideWinFx();
    };
    //SECTION ISicboState
    SicboResultStateComp.prototype.exitState = function (status) {
        this.isPayingPlaying = false;
    };
    SicboResultStateComp.prototype.updateState = function (state) {
        if (this.isPayingPlaying)
            return;
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        if (status == Status.PAYING && elapseTime >= 0.1) {
            //NOTE server delay 0.1s for generate result
            this.setResult(state.getResult());
            //cc.log("updateState", this.result.toObject());
            this.playPayingAnimation(elapseTime);
            this.isPayingPlaying = true;
        }
    };
    SicboResultStateComp.prototype.setResult = function (result) {
        if (SicboGameUtils_1.default.isNullOrUndefined(result))
            return;
        //cc.log("RESULT1", result.hasPlayerBetTx(), result.toObject());
        this.result = result;
        this.items = result.getItemsList().sort();
        this.wonDoors = result.getDoorsList();
        this.wonDoorsFiltered = this.wonDoors.filter(function (elem, index, self) {
            return index === self.indexOf(elem);
        });
        this.setDiceResult();
    };
    SicboResultStateComp.prototype.startState = function (state, totalDuration) {
        this.curSessionId = state.getTableSessionId();
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        this.setResult(state.getResult());
        //console.error(SicboHelper.StatusToString(status), elapseTime);
        switch (status) {
            case Status.WAITING:
                SicboHelper_1.default.changeParent(this.chenAnimation.node, this.chenZoomInRoot);
                this.chenRollDice(elapseTime);
                this.chenRollDiceSfx(elapseTime);
                this.chenRollDiceSfx(elapseTime, false);
                this.hideWinFx();
                this.latestResultComp.playFx(false);
                this.destroyWinMoneyAtDoor();
                break;
            case Status.RESULTING:
                this.setDiceResult();
                SicboHelper_1.default.changeParent(this.chenAnimation.node, this.chenZoomInRoot);
                this.chenOpen(elapseTime);
                this.chenOpenSfx(elapseTime);
                this.chenIdle(elapseTime);
                this.dicesFlyToLatestResult(elapseTime);
                this.playWinFxShinny(elapseTime);
                this.playWinFx(elapseTime);
                break;
            case Status.PAYING:
                // this.destroyWinMoneyAtDoor();
                // this.statisticComp.showHideSoiCauNewEffect(true);
                SicboHelper_1.default.changeParent(this.chenAnimation.node, this.chenSmallRoot);
                this.chenIdle(100);
                ////console.log("BCF PAYING WinnersList", elapseTime, state.getResult()?.getWinnersList());
                // //console.log("BCF PAYING Receipt", elapseTime, state.getResult()?.getPlayerBetTx());
                this.playWinFx(100);
                this.latestResultComp.playFx();
                // this.playChipInDoorAnimation(elapseTime);
                this.playLoseAnimation(elapseTime);
                this.playChipStackAnimation(elapseTime);
                this.dealerReturnWinChip(elapseTime);
                // this.playPayingAnimation(elapseTime);  //NOTE server delay 0.1s for generate result
                break;
            // case Status.FINISHING:
            //   return;//FIXME
            // this.chenClose(elapseTime);
            // this.statisticComp.showHideSoiCauNewEffect(false);
            default:
                this.hideWinFx();
                this.latestResultComp.playFx(false);
        }
    };
    //!SECTION
    //SECTION Chen
    SicboResultStateComp.prototype.setDiceResult = function () {
        //cc.log("DICES", this.items);
        for (var i = 0; i < this.items.length; i++) {
            var spriteName = SicboHelper_1.default.convertItemToDiceSpriteName(this.items[i]);
            var self = this;
            self.diceSprites[i].spriteFrame = SicboDiceConfig_1.default.Instance.getDice(spriteName);
        }
    };
    SicboResultStateComp.prototype.chenRollDice = function (elapseTime) {
        var _this = this;
        if (elapseTime === void 0) { elapseTime = 0; }
        if (elapseTime > this.chenRollDiceStartTime) {
            this.chenAnimation.play(this.chenRollDiceAnim, elapseTime - this.chenRollDiceStartTime);
            // this.playDealerOpenAnimation();
        }
        else {
            cc.tween(this.node)
                .delay(this.chenRollDiceStartTime - elapseTime)
                .call(function () {
                // this.playDealerOpenAnimation();
                _this.chenAnimation.play(_this.chenRollDiceAnim);
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.chenRollDiceSfx = function (elapseTime, isPlay) {
        if (elapseTime === void 0) { elapseTime = 0; }
        if (isPlay === void 0) { isPlay = true; }
        var time = isPlay ? this.chenRollDiceSfxStartTime : this.chenRollDiceSfxStopTime;
        if (elapseTime > time) {
            if (isPlay)
                SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxDealerShake);
            else
                SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxDealerShake);
        }
        else {
            cc.tween(this.node)
                .delay(time - elapseTime)
                .call(function () {
                if (isPlay)
                    SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxDealerShake);
                else
                    SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxDealerShake);
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.chenOpen = function (elapseTime) {
        var _this = this;
        if (elapseTime === void 0) { elapseTime = 0; }
        if (elapseTime > this.chenOpenStartTime) {
            this.chenAnimation.play(this.chenOpenAnim, elapseTime - this.chenOpenStartTime);
            // this.playDealerOpenAnimation();
        }
        else {
            cc.tween(this.node)
                .delay(this.chenOpenStartTime - elapseTime)
                .call(function () {
                // this.playDealerOpenAnimation();
                _this.chenAnimation.play(_this.chenOpenAnim);
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.chenOpenSfx = function (elapseTime) {
        if (elapseTime === void 0) { elapseTime = 0; }
        var self = this;
        if (elapseTime > this.chenOpenSfxStartTime) {
        }
        else {
            cc.tween(self.node)
                .delay(self.chenOpenSfxStartTime - elapseTime)
                .call(function () {
                SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxResult);
                self.notifyComp.showResultNoti(self.getResultText(self.result.getItemsList()));
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.getResultText = function (items) {
        var total = SicboHelper_1.default.getResultTotalValue(items).toString();
        var typeResult = SicboHelper_1.default.getResultType(items);
        var typeName = SicboHelper_1.default.getResultTypeName(typeResult);
        return total + " điểm - " + typeName;
    };
    // private playDealerOpenAnimation(){
    //   SicboSkeletonUtils.setAnimation(this.dealerSkeleton, "open", true);
    //   //cc.log("Dealer Open");
    // }
    SicboResultStateComp.prototype.chenIdle = function (elapseTime) {
        var _this = this;
        if (elapseTime === void 0) { elapseTime = 0; }
        if (elapseTime > this.chenIdleStartTime) {
            this.chenAnimation.play(this.chenIdleAnim, elapseTime - this.chenIdleStartTime);
        }
        else
            cc.tween(this.node)
                .delay(this.chenIdleStartTime - elapseTime)
                .call(function () {
                _this.chenAnimation.play(_this.chenIdleAnim);
                // SoundManager.getInstance().playEffLocalName(SoundDefine.Sicbo_LidClose, SoundDefine.Sicbo);
            })
                .start();
    };
    // chenClose(elapseTime: number = 0){
    //   if(elapseTime>this.chenCloseStartTime)
    //     this.chenAnimation.play(this.chenCloseAnim, elapseTime - this.chenCloseStartTime);
    //   else
    //     cc.tween(this.node)
    //       .delay(this.chenCloseStartTime - elapseTime)
    //       .call(
    //         ()=>{
    //           this.chenAnimation.play(this.chenCloseAnim);
    //           // SoundManager.getInstance().playEffLocalName(SoundDefine.Sicbo_LidClose, SoundDefine.Sicbo);
    //         }
    //       ).start();
    // }
    SicboResultStateComp.prototype.dicesFlyToLatestResult = function (elapseTime) {
        if (elapseTime === void 0) { elapseTime = 0; }
        if (elapseTime > this.dicesFlyToLatestResultStartTime) {
            // this.statisticComp.reloadLatestRecordWithAnimation(this.result.getItemsList());
            // this.statisticComp.showHideSoiCauNewEffect(true, .5);
            // SicboUIComp.Instance.getHistory(true);
            // SicboController.Instance.playSfx(SicboSound.SfxMessage);
        }
        else {
            var self_1 = this;
            cc.tween(self_1.node)
                .delay(self_1.dicesFlyToLatestResultStartTime - elapseTime)
                .call(function () {
                //cc.log("dicesFlyToLatestResultStartTime", self.dicesFlyToLatestResultStartTime);
                var diceFlyDuration = 0.5;
                var _loop_1 = function (i) {
                    var diceOrigin = self_1.diceSprites[i].node;
                    var dice = cc.instantiate(diceOrigin);
                    dice.setParent(diceOrigin.parent);
                    dice.setPosition(diceOrigin.getPosition());
                    dice.scale = 0.165;
                    SicboHelper_1.default.changeParent(dice, self_1.chenZoomInRoot);
                    var targetPos = self_1.latestResultComp.items[i].node.getPosition();
                    targetPos = SicboGameUtils_1.default.convertToOtherNode2(self_1.latestResultComp.items[i].node, self_1.chenZoomInRoot);
                    cc.tween(dice)
                        .to(diceFlyDuration, {
                        x: targetPos.x,
                        y: targetPos.y,
                        scale: 0.2,
                    })
                        .call(function () {
                        dice.destroy();
                    })
                        .start();
                };
                for (var i = 0; i < self_1.diceSprites.length; i++) {
                    _loop_1(i);
                }
                cc.tween(self_1.node)
                    .delay(diceFlyDuration)
                    .call(function () {
                    self_1.latestResultComp.setData(self_1.result.getItemsList());
                    self_1.latestResultComp.playFx();
                    // self.statisticComp.showHideSoiCauNewEffect(true, .5);
                    SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxMessage);
                    // SicboUIComp.Instance.getHistory(true);
                })
                    .start();
            })
                .start();
        }
    };
    //!SECTION
    //SECTION Door WinFX
    SicboResultStateComp.prototype.playWinFx = function (elapseTime) {
        var _this = this;
        var _a;
        if (elapseTime === void 0) { elapseTime = 0; }
        if (this.wonDoors.length <= 0)
            return;
        if (this.isWinFxPlaying)
            return;
        if (elapseTime > this.doorWinFxStartTime) {
            this.winFx();
        }
        else {
            var self = this;
            (_a = self.winFxTween) === null || _a === void 0 ? void 0 : _a.stop();
            self.winFxTween = cc
                .tween(this.winFxShinnyNode)
                .delay(this.doorWinFxStartTime - elapseTime)
                .call(function () {
                _this.winFx();
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.winFx = function () {
        this.isWinFxPlaying = true;
        for (var index = 0; index < this.winFxSkeletons.length; index++) {
            if (this.wonDoorsFiltered[index] != null) {
                SicboSkeletonUtils_1.default.setAnimation(this.winFxSkeletons[index], this.wonDoorsFiltered[index].toString() + "b", true);
                this.winFxSkeletons[index].node.active = true;
            }
            else {
                this.winFxSkeletons[index].node.active = false;
            }
        }
    };
    SicboResultStateComp.prototype.playWinFxShinny = function (elapseTime) {
        var self = this;
        if (elapseTime > self.doorWinFxShinnyStartTime) {
            self.winFxShinny(elapseTime - self.doorWinFxShinnyStartTime);
        }
        else {
            cc.tween(this.node)
                .delay(this.doorWinFxShinnyStartTime - elapseTime)
                .call(function () {
                self.winFxShinny();
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.winFxShinny = function (startTime) {
        if (startTime === void 0) { startTime = 0; }
        var self = this;
        for (var index = 0; index < self.winFxShinnySkeletons.length; index++) {
            if (self.wonDoorsFiltered[index] != null) {
                SicboSkeletonUtils_1.default.setAnimation(self.winFxShinnySkeletons[index], this.wonDoorsFiltered[index].toString(), false, startTime);
                self.winFxShinnySkeletons[index].node.active = true;
            }
            else {
                self.winFxShinnySkeletons[index].node.active = false;
            }
        }
    };
    SicboResultStateComp.prototype.hideWinFx = function () {
        var _a;
        (_a = this.winFxTween) === null || _a === void 0 ? void 0 : _a.stop();
        cc.Tween.stopAllByTarget(this.winFxShinnyNode);
        for (var index = 0; index < this.winFxSkeletons.length; index++) {
            this.winFxSkeletons[index].node.active = false;
        }
        for (var index = 0; index < this.winFxShinnySkeletons.length; index++) {
            this.winFxShinnySkeletons[index].node.active = false;
        }
        this.isWinFxPlaying = false;
        this.wonDoors = [];
        this.winMoneyAtSlots.forEach(function (element) {
            element.hide();
        });
    };
    //!SECTION
    //SECTION other betComp FX
    //ANCHOR playLoseAnimation
    SicboResultStateComp.prototype.playLoseAnimation = function (elapseTime) {
        var self = this;
        self.elapseTimeCheck(elapseTime, self.chipLoseStartTime, function () {
            self.betComp.playLoseAnimation(self.wonDoorsFiltered, 0);
        }, function () {
            var duration = 0.5;
            self.betComp.playLoseAnimation(self.wonDoorsFiltered, duration);
        });
    };
    SicboResultStateComp.prototype.playChipStackAnimation = function (elapseTime) {
        var self = this;
        self.elapseTimeCheck(elapseTime, self.chipStackStartTime, function () {
            self.betComp.playChipStackAnimation(self.wonDoorsFiltered, 0);
        }, function () {
            var duration = 0.5;
            self.betComp.playChipStackAnimation(self.wonDoorsFiltered, duration);
        });
    };
    //ANCHOR dealerReturnWinChip
    SicboResultStateComp.prototype.dealerReturnWinChip = function (elapseTime) {
        var _this = this;
        var userWinAmount = this.getMyUserWinAmount();
        ////console.log("BCF userWinAmount", userWinAmount);
        var self = this;
        var delayShowInMoney = 0;
        if (elapseTime > this.returnChipStartTime) {
            this.betComp.dealerReturnFullStackChip(this.wonDoorsFiltered, 0);
            delayShowInMoney = 0;
        }
        else {
            cc.tween(this.node)
                .delay(this.returnChipStartTime - elapseTime)
                .call(function () {
                delayShowInMoney = self.betComp.dealerReturnWinChip(self.wonDoorsFiltered) + 0.5;
                cc.tween(_this.node)
                    .delay(delayShowInMoney)
                    .call(function () {
                    userWinAmount.forEach(function (amount, door) {
                        self.showWinMoneyAtDoor(amount, door, elapseTime);
                    });
                    if (userWinAmount.size > 0)
                        SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxWin);
                })
                    .start();
            })
                .start();
        }
    };
    //ANCHOR playPayingAnimation
    SicboResultStateComp.prototype.playPayingAnimation = function (elapseTime) {
        var _this = this;
        //cc.log("SICBO playPayingAnimation", this.result.toObject());
        if (elapseTime > this.payingStartTime) {
            //NOTE k play animation cong tien o avatar khi user vao luc co result, do tien da duoc update o server
            this.result.getWinnersList().forEach(function (record) {
                var user = SicboUIComp_1.default.Instance.findUserComp(record.getUserId());
                var payingMoney = record.getAmount();
                _this.betComp.playPayingAnimation(record.getBet().getDoor(), user.node, payingMoney, 0);
            });
        }
        else {
            var self_2 = this;
            cc.tween(this.node)
                .delay(this.payingStartTime - elapseTime)
                .call(function () {
                self_2.result.getWinnersList().forEach(function (record) {
                    var user = SicboUIComp_1.default.Instance.findUserComp(record.getUserId());
                    self_2.betComp.playPayingAnimation(record.getBet().getDoor(), user.node, record.getAmount());
                });
                var delayShowAvatarWinMoney = 1.05;
                var payingMoney = self_2.result.hasPlayerBetTx() ? self_2.result.getPlayerBetTx().getAmount() : 0; //from chip
                var transaction = self_2.result.hasPlayerBetTx() ? self_2.result.getPlayerBetTx().getLastTxId() : 0;
                if (self_2.result.hasLotteryWinner() && SicboPortalAdapter_1.default.getInstance().getMyUserId() == self_2.result.getLotteryWinner().getUserId()) {
                    //from jackpot
                    payingMoney += self_2.result.getLotteryWinner().getAmount();
                    var jackpotTransaction = self_2.result.hasPlayerJackpotTx() ? self_2.result.getPlayerJackpotTx().getLastTxId() : 0;
                    transaction = Math.max(transaction, jackpotTransaction);
                }
                self_2.payMoneyForMyUser(payingMoney, transaction, delayShowAvatarWinMoney);
                var wonAmountFiltered = self_2.filterWinAmount(self_2.result.getWinnersList(), self_2.result.getLotteryWinner());
                wonAmountFiltered.forEach(function (amount, userId) {
                    var user = SicboUIComp_1.default.Instance.findUserComp(userId);
                    if (self_2.result.hasLotteryWinner() && userId == self_2.result.getLotteryWinner().getUserId())
                        //from jackpot
                        amount += self_2.result.getLotteryWinner().getAmount();
                    cc.tween(_this.node)
                        .delay(delayShowAvatarWinMoney)
                        .call(function () {
                        user.showWinMoney(amount);
                    })
                        .start();
                });
            })
                .start();
        }
    };
    //ANCHOR filterWinAmount
    SicboResultStateComp.prototype.filterWinAmount = function (records, jackpotWinner) {
        var _this = this;
        var result = new Map();
        var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
        if (jackpotWinner != null) {
            var jackpotUserId = jackpotWinner.getUserId();
            if (myUserId != jackpotUserId) {
                if (!result.has(jackpotUserId))
                    result.set(jackpotUserId, 0);
            }
        }
        records.forEach(function (record) {
            var userId = null;
            if (record.getOthers())
                userId = _this.otherGroupKey;
            else {
                userId = record.getUserId();
            }
            if (userId != myUserId) {
                if (!result.has(userId))
                    result.set(userId, 0);
                result.set(userId, result.get(userId) + record.getAmount());
            }
        });
        return result;
    };
    SicboResultStateComp.prototype.elapseTimeCheck = function (elapseTime, startTime, onPassedElapseTime, onNOTPassElapseTime) {
        if (elapseTime > startTime) {
            if (SicboHelper_1.default.approximatelyEqual(elapseTime - startTime, 0, 0.1)) {
                onNOTPassElapseTime && onNOTPassElapseTime();
            }
            else {
                onPassedElapseTime && onPassedElapseTime();
            }
        }
        else {
            cc.tween(this.node)
                .delay(Math.max(startTime - elapseTime, 0))
                .call(function () {
                onNOTPassElapseTime && onNOTPassElapseTime();
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.payMoneyForMyUser = function (amount, transaction, delay) {
        if (delay === void 0) { delay = 0; }
        if (amount == 0)
            return;
        var user = SicboUIComp_1.default.Instance.myUserComp();
        cc.tween(this.node)
            .delay(delay)
            .call(function () {
            SicboController_1.default.Instance.addMyUserMoney(amount, transaction);
            //cc.log("SICBO RECEIPT", amount);
            user.showWinMoney(amount);
            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxTotalWin);
        })
            .start();
    };
    //ANCHOR showWinMoneyAtDoor
    SicboResultStateComp.prototype.getMyUserWinAmount = function () {
        var winnerList = this.result.getWinnersList();
        var rs = new Map();
        var userId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
        winnerList.forEach(function (record) {
            if (record.getUserId() == userId) {
                var door = record.getBet().getDoor();
                if (rs.has(door)) {
                    rs.set(door, record.getAmount() + rs.get(door));
                }
                else {
                    rs.set(door, record.getAmount());
                }
            }
        });
        return rs;
    };
    SicboResultStateComp.prototype.showWinMoneyAtDoor = function (money, door, elapseTime) {
        if (elapseTime === void 0) { elapseTime = 0; }
        var d = this.betComp.getDoor(door);
        var prefab = this.slotWinMoneySmallPrefab;
        switch (door) {
            case Door.ANY_TRIPLE:
            case Door.BIG:
            case Door.SMALL:
            case Door.ONE_SINGLE:
            case Door.TWO_TRIPLE:
            case Door.THREE_TRIPLE:
            case Door.FOUR_TRIPLE:
            case Door.FIVE_TRIPLE:
            case Door.SIX_TRIPLE:
            case Door.ONE_SINGLE:
            case Door.TWO_SINGLE:
            case Door.THREE_SINGLE:
            case Door.FOUR_SINGLE:
            case Door.FIVE_SINGLE:
            case Door.SIX_SINGLE:
                prefab = this.slotWinMoneyLargePrefab;
                break;
        }
        var comp = SicboGameUtils_1.default.createItemFromPrefab(SicboSlotWinMoneyComp_1.default, prefab, d.node);
        comp.show(money, elapseTime);
        this.winMoneyAtSlots.push(comp);
    };
    SicboResultStateComp.prototype.destroyWinMoneyAtDoor = function () {
        this.winMoneyAtSlots.forEach(function (element) {
            var _a;
            (_a = element.node) === null || _a === void 0 ? void 0 : _a.destroy();
        });
        this.winMoneyAtSlots.length = 0;
    };
    //!SECTION
    SicboResultStateComp.prototype.updateResultFistTime = function (records) {
        if (SicboGameUtils_1.default.isNullOrUndefined(records)) {
            this.latestResultComp.active = false;
            return;
        }
        var lastRecord = records[0];
        // if(this.updateLastResult == false)
        //   lastRecord = records[1];
        if (SicboGameUtils_1.default.isNullOrUndefined(lastRecord)) {
            this.latestResultComp.active = false;
            return;
        }
        this.latestResultComp.active = true;
        var data = ChannelHistoryMetadata.deserializeBinary(lastRecord.getMetadata_asU8());
        var items = data.getItemsList().sort();
        this.latestResultComp.setData(items, false);
    };
    SicboResultStateComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
        cc.Tween.stopAllByTarget(this.winFxShinnyNode);
    };
    SicboResultStateComp.prototype.onHide = function () {
        SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxDealerShake);
        cc.Tween.stopAllByTarget(this.node);
        cc.Tween.stopAllByTarget(this.winFxShinnyNode);
        // this.chenClose(100);
        this.hideWinFx();
        this.destroyWinMoneyAtDoor();
        this.isPayingPlaying = false;
    };
    __decorate([
        property(cc.Node)
    ], SicboResultStateComp.prototype, "betCompNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboResultStateComp.prototype, "winFxShinnyNode", void 0);
    __decorate([
        property(cc.Animation)
    ], SicboResultStateComp.prototype, "chenAnimation", void 0);
    __decorate([
        property(cc.Node)
    ], SicboResultStateComp.prototype, "chenSmallRoot", void 0);
    __decorate([
        property(cc.Node)
    ], SicboResultStateComp.prototype, "chenZoomInRoot", void 0);
    __decorate([
        property([cc.Sprite])
    ], SicboResultStateComp.prototype, "diceSprites", void 0);
    __decorate([
        property(SicboLatestResultComp_1.default)
    ], SicboResultStateComp.prototype, "latestResultComp", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboResultStateComp.prototype, "dealerSkeleton", void 0);
    __decorate([
        property(cc.Prefab)
    ], SicboResultStateComp.prototype, "slotWinMoneySmallPrefab", void 0);
    __decorate([
        property(cc.Prefab)
    ], SicboResultStateComp.prototype, "slotWinMoneyLargePrefab", void 0);
    __decorate([
        property(SicboNotifyComp_1.default)
    ], SicboResultStateComp.prototype, "notifyComp", void 0);
    SicboResultStateComp = __decorate([
        ccclass
    ], SicboResultStateComp);
    return SicboResultStateComp;
}(cc.Component));
exports.default = SicboResultStateComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU3RhdGVzL1NpY2JvUmVzdWx0U3RhdGVDb21wLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWxGLHdFQUFtRTtBQUU3RCxJQUFBLEtBQWtGLDRCQUFrQixDQUFDLFVBQVUsRUFBRSxFQUEvRyxzQkFBc0IsNEJBQUEsRUFBRSxJQUFJLFVBQUEsRUFBRSxLQUFLLFdBQUEsRUFBRSxNQUFNLFlBQUEsRUFBRSxNQUFNLFlBQUEsRUFBRSxzQkFBc0IsNEJBQW9DLENBQUM7QUFFeEgsMkRBQXdEO0FBQ3hELGlFQUE0RDtBQUM1RCxxRUFBZ0U7QUFDaEUseURBQW9EO0FBRXBELGdEQUEyQztBQUMzQyxrRUFBNkQ7QUFDN0Qsc0RBQWlEO0FBQ2pELGtFQUE2RDtBQUM3RCw4Q0FBeUM7QUFFekMsK0RBQTBEO0FBQzFELHdFQUFtRTtBQUNuRSxnRkFBMkU7QUFFckUsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBa0Qsd0NBQVk7SUFBOUQ7UUFBQSxxRUFrckJDO1FBaHJCQyxpQkFBVyxHQUFZLElBQUksQ0FBQztRQUNwQixhQUFPLEdBQWlCLElBQUksQ0FBQztRQUdyQyxxQkFBZSxHQUFZLElBQUksQ0FBQztRQUdoQyxtQkFBYSxHQUFpQixJQUFJLENBQUM7UUFHbkMsbUJBQWEsR0FBWSxJQUFJLENBQUM7UUFHOUIsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsaUJBQVcsR0FBZ0IsRUFBRSxDQUFDO1FBRzlCLHNCQUFnQixHQUEwQixJQUFJLENBQUM7UUFHL0Msb0JBQWMsR0FBZ0IsSUFBSSxDQUFDO1FBR25DLDZCQUF1QixHQUFjLElBQUksQ0FBQztRQUcxQyw2QkFBdUIsR0FBYyxJQUFJLENBQUM7UUFHMUMsZ0JBQVUsR0FBb0IsSUFBSSxDQUFDO1FBRTNCLG9CQUFjLEdBQWtCLEVBQUUsQ0FBQztRQUNuQywwQkFBb0IsR0FBa0IsRUFBRSxDQUFDO1FBQ3pDLG9CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLGdCQUFVLEdBQWEsSUFBSSxDQUFDO1FBSTVCLGNBQVEsR0FBVSxFQUFFLENBQUM7UUFDckIsc0JBQWdCLEdBQVUsRUFBRSxDQUFDO1FBQzdCLFdBQUssR0FBVSxFQUFFLENBQUM7UUFFbEIsa0JBQVksR0FBRyxVQUFVLENBQUM7UUFDMUIsa0JBQVksR0FBRyxVQUFVLENBQUM7UUFDbEMsdUNBQXVDO1FBQy9CLHNCQUFnQixHQUFHLGNBQWMsQ0FBQztRQUUxQywwQ0FBMEM7UUFDMUMsb0NBQW9DO1FBQzVCLDJCQUFxQixHQUFHLEdBQUcsQ0FBQztRQUM1Qiw4QkFBd0IsR0FBRyxLQUFJLENBQUMscUJBQXFCLEdBQUcsR0FBRyxDQUFDO1FBQzVELDZCQUF1QixHQUFHLEtBQUksQ0FBQyx3QkFBd0IsR0FBRyxHQUFHLENBQUM7UUFFOUQsdUJBQWlCLEdBQUcsR0FBRyxDQUFDLENBQUMsU0FBUztRQUNsQywwQkFBb0IsR0FBRyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsR0FBRyxDQUFDO1FBQzVELGtDQUFrQztRQUMxQix1QkFBaUIsR0FBRyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLHFDQUErQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxHQUFHLENBQUM7UUFDL0QsOEJBQXdCLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEdBQUcsQ0FBQztRQUN4RCx3QkFBa0IsR0FBRyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxHQUFHLENBQUM7UUFFM0QsaUNBQWlDO1FBQ3pCLHVCQUFpQixHQUFHLENBQUMsQ0FBQztRQUN0Qix3QkFBa0IsR0FBRyxDQUFDLENBQUM7UUFDdkIseUJBQW1CLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM1QixxQkFBZSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFeEIscUJBQWUsR0FBWSxLQUFLLENBQUM7UUFFekMsc0JBQXNCO1FBQ2QsbUJBQWEsR0FBRyxlQUFlLENBQUM7UUFFeEMscUJBQWUsR0FBNEIsRUFBRSxDQUFDOztJQXNtQmhELENBQUM7SUFwbUJDLHFDQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLHNCQUFZLENBQUMsQ0FBQztRQUUzRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEYsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7UUFFaEQsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFRCxxQkFBcUI7SUFDckIsd0NBQVMsR0FBVCxVQUFVLE1BQU07UUFDZCxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0lBRUQsMENBQVcsR0FBWCxVQUFZLEtBQWlDO1FBQzNDLElBQUksSUFBSSxDQUFDLGVBQWU7WUFBRSxPQUFPO1FBQ2pDLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUMvQixJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLEdBQUcsSUFBSSxDQUFDO1FBRTdDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLElBQUksVUFBVSxJQUFJLEdBQUcsRUFBRTtZQUNoRCw0Q0FBNEM7WUFDNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQztZQUNsQyxnREFBZ0Q7WUFDaEQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1NBQzdCO0lBQ0gsQ0FBQztJQUVPLHdDQUFTLEdBQWpCLFVBQWtCLE1BQW1DO1FBQ25ELElBQUksd0JBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7WUFBRSxPQUFPO1FBQ3JELGdFQUFnRTtRQUNoRSxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQyxJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUV0QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBVSxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUk7WUFDdEUsT0FBTyxLQUFLLEtBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRUQseUNBQVUsR0FBVixVQUFXLEtBQWlDLEVBQUUsYUFBcUI7UUFDakUsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUM5QyxJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDL0IsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDLFlBQVksRUFBRSxHQUFHLElBQUksQ0FBQztRQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO1FBQ2xDLGdFQUFnRTtRQUNoRSxRQUFRLE1BQU0sRUFBRTtZQUNkLEtBQUssTUFBTSxDQUFDLE9BQU87Z0JBQ2pCLHFCQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDdkUsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDakMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBRXhDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDakIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFcEMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7Z0JBQzdCLE1BQU07WUFDUixLQUFLLE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBRXJCLHFCQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDdkUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFFMUIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUV4QyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNqQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUMzQixNQUFNO1lBRVIsS0FBSyxNQUFNLENBQUMsTUFBTTtnQkFDaEIsZ0NBQWdDO2dCQUNoQyxvREFBb0Q7Z0JBQ3BELHFCQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFFdEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFFbkIsMkZBQTJGO2dCQUMzRix3RkFBd0Y7Z0JBRXhGLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFFL0IsNENBQTRDO2dCQUM1QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFFeEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNyQyxzRkFBc0Y7Z0JBQ3RGLE1BQU07WUFDUix5QkFBeUI7WUFDekIsbUJBQW1CO1lBQ25CLDhCQUE4QjtZQUM5QixxREFBcUQ7WUFFckQ7Z0JBQ0UsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUNqQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0gsQ0FBQztJQUVELFVBQVU7SUFFVixjQUFjO0lBQ2QsNENBQWEsR0FBYjtRQUNFLDhCQUE4QjtRQUM5QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDMUMsSUFBSSxVQUFVLEdBQUcscUJBQVcsQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEUsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxHQUFHLHlCQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNoRjtJQUNILENBQUM7SUFFRCwyQ0FBWSxHQUFaLFVBQWEsVUFBc0I7UUFBbkMsaUJBYUM7UUFiWSwyQkFBQSxFQUFBLGNBQXNCO1FBQ2pDLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUMzQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsVUFBVSxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ3hGLGtDQUFrQztTQUNuQzthQUFNO1lBQ0wsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUNoQixLQUFLLENBQUMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLFVBQVUsQ0FBQztpQkFDOUMsSUFBSSxDQUFDO2dCQUNKLGtDQUFrQztnQkFDbEMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDakQsQ0FBQyxDQUFDO2lCQUNELEtBQUssRUFBRSxDQUFDO1NBQ1o7SUFDSCxDQUFDO0lBRUQsOENBQWUsR0FBZixVQUFnQixVQUFzQixFQUFFLE1BQXNCO1FBQTlDLDJCQUFBLEVBQUEsY0FBc0I7UUFBRSx1QkFBQSxFQUFBLGFBQXNCO1FBQzVELElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUM7UUFDakYsSUFBSSxVQUFVLEdBQUcsSUFBSSxFQUFFO1lBQ3JCLElBQUksTUFBTTtnQkFBRSx5QkFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMseUJBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQzs7Z0JBQ25FLHlCQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyx5QkFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ2xFO2FBQU07WUFDTCxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7aUJBQ2hCLEtBQUssQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO2lCQUN4QixJQUFJLENBQUM7Z0JBQ0osSUFBSSxNQUFNO29CQUFFLHlCQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyx5QkFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDOztvQkFDbkUseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHlCQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDbkUsQ0FBQyxDQUFDO2lCQUNELEtBQUssRUFBRSxDQUFDO1NBQ1o7SUFDSCxDQUFDO0lBRUQsdUNBQVEsR0FBUixVQUFTLFVBQXNCO1FBQS9CLGlCQWFDO1FBYlEsMkJBQUEsRUFBQSxjQUFzQjtRQUM3QixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDdkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxVQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDaEYsa0NBQWtDO1NBQ25DO2FBQU07WUFDTCxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7aUJBQ2hCLEtBQUssQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsVUFBVSxDQUFDO2lCQUMxQyxJQUFJLENBQUM7Z0JBQ0osa0NBQWtDO2dCQUNsQyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDN0MsQ0FBQyxDQUFDO2lCQUNELEtBQUssRUFBRSxDQUFDO1NBQ1o7SUFDSCxDQUFDO0lBRUQsMENBQVcsR0FBWCxVQUFZLFVBQXNCO1FBQXRCLDJCQUFBLEVBQUEsY0FBc0I7UUFDaEMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtTQUMzQzthQUFNO1lBQ0wsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUNoQixLQUFLLENBQUMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFVBQVUsQ0FBQztpQkFDN0MsSUFBSSxDQUFDO2dCQUNKLHlCQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyx5QkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN2RCxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2pGLENBQUMsQ0FBQztpQkFDRCxLQUFLLEVBQUUsQ0FBQztTQUNaO0lBQ0gsQ0FBQztJQUVELDRDQUFhLEdBQWIsVUFBYyxLQUFZO1FBQ3hCLElBQUksS0FBSyxHQUFHLHFCQUFXLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDOUQsSUFBSSxVQUFVLEdBQUcscUJBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEQsSUFBSSxRQUFRLEdBQUcscUJBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN6RCxPQUFPLEtBQUssR0FBRyxVQUFVLEdBQUcsUUFBUSxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxxQ0FBcUM7SUFDckMsd0VBQXdFO0lBQ3hFLDZCQUE2QjtJQUM3QixJQUFJO0lBRUosdUNBQVEsR0FBUixVQUFTLFVBQXNCO1FBQS9CLGlCQVdDO1FBWFEsMkJBQUEsRUFBQSxjQUFzQjtRQUM3QixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDdkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxVQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDakY7O1lBQ0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUNoQixLQUFLLENBQUMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFVBQVUsQ0FBQztpQkFDMUMsSUFBSSxDQUFDO2dCQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDM0MsOEZBQThGO1lBQ2hHLENBQUMsQ0FBQztpQkFDRCxLQUFLLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFFRCxxQ0FBcUM7SUFDckMsMkNBQTJDO0lBQzNDLHlGQUF5RjtJQUN6RixTQUFTO0lBQ1QsMEJBQTBCO0lBQzFCLHFEQUFxRDtJQUNyRCxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLHlEQUF5RDtJQUN6RCwyR0FBMkc7SUFDM0csWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixJQUFJO0lBRUoscURBQXNCLEdBQXRCLFVBQXVCLFVBQXNCO1FBQXRCLDJCQUFBLEVBQUEsY0FBc0I7UUFDM0MsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLCtCQUErQixFQUFFO1lBQ3JELGtGQUFrRjtZQUNsRix3REFBd0Q7WUFDeEQseUNBQXlDO1lBQ3pDLDJEQUEyRDtTQUM1RDthQUFNO1lBQ0wsSUFBSSxNQUFJLEdBQUcsSUFBSSxDQUFDO1lBRWhCLEVBQUUsQ0FBQyxLQUFLLENBQUMsTUFBSSxDQUFDLElBQUksQ0FBQztpQkFDaEIsS0FBSyxDQUFDLE1BQUksQ0FBQywrQkFBK0IsR0FBRyxVQUFVLENBQUM7aUJBQ3hELElBQUksQ0FBQztnQkFDSixrRkFBa0Y7Z0JBRWxGLElBQUksZUFBZSxHQUFHLEdBQUcsQ0FBQzt3Q0FDakIsQ0FBQztvQkFDUixJQUFJLFVBQVUsR0FBRyxNQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztvQkFDMUMsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDdEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ2xDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7b0JBQzNDLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO29CQUVuQixxQkFBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsTUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29CQUNwRCxJQUFJLFNBQVMsR0FBRyxNQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDbEUsU0FBUyxHQUFHLHdCQUFjLENBQUMsbUJBQW1CLENBQUMsTUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsTUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29CQUN6RyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQzt5QkFDWCxFQUFFLENBQUMsZUFBZSxFQUFFO3dCQUNuQixDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUM7d0JBQ2QsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDO3dCQUNkLEtBQUssRUFBRSxHQUFHO3FCQUNYLENBQUM7eUJBQ0QsSUFBSSxDQUFDO3dCQUNKLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDakIsQ0FBQyxDQUFDO3lCQUNELEtBQUssRUFBRSxDQUFDOztnQkFuQmIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRTs0QkFBdkMsQ0FBQztpQkFvQlQ7Z0JBRUQsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFJLENBQUMsSUFBSSxDQUFDO3FCQUNoQixLQUFLLENBQUMsZUFBZSxDQUFDO3FCQUN0QixJQUFJLENBQUM7b0JBQ0osTUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxNQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7b0JBQzFELE1BQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQkFDL0Isd0RBQXdEO29CQUN4RCx5QkFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMseUJBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDeEQseUNBQXlDO2dCQUMzQyxDQUFDLENBQUM7cUJBQ0QsS0FBSyxFQUFFLENBQUM7WUFDYixDQUFDLENBQUM7aUJBQ0QsS0FBSyxFQUFFLENBQUM7U0FDWjtJQUNILENBQUM7SUFDRCxVQUFVO0lBRVYsb0JBQW9CO0lBQ3BCLHdDQUFTLEdBQVQsVUFBVSxVQUFzQjtRQUFoQyxpQkFpQkM7O1FBakJTLDJCQUFBLEVBQUEsY0FBc0I7UUFDOUIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxDQUFDO1lBQUUsT0FBTztRQUN0QyxJQUFJLElBQUksQ0FBQyxjQUFjO1lBQUUsT0FBTztRQUVoQyxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDeEMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2Q7YUFBTTtZQUNMLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixNQUFBLElBQUksQ0FBQyxVQUFVLDBDQUFFLElBQUksR0FBRztZQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUU7aUJBQ2pCLEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO2lCQUMzQixLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFVBQVUsQ0FBQztpQkFDM0MsSUFBSSxDQUFDO2dCQUNKLEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNmLENBQUMsQ0FBQztpQkFDRCxLQUFLLEVBQUUsQ0FBQztTQUNaO0lBQ0gsQ0FBQztJQUVPLG9DQUFLLEdBQWI7UUFDRSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUMzQixLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUU7WUFDL0QsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUN4Qyw0QkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFLEdBQUcsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUNqSCxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2FBQy9DO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7YUFDaEQ7U0FDRjtJQUNILENBQUM7SUFFRCw4Q0FBZSxHQUFmLFVBQWdCLFVBQWtCO1FBQ2hDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsd0JBQXdCLEVBQUU7WUFDOUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7U0FDOUQ7YUFBTTtZQUNMLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztpQkFDaEIsS0FBSyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxVQUFVLENBQUM7aUJBQ2pELElBQUksQ0FBQztnQkFDSixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDckIsQ0FBQyxDQUFDO2lCQUNELEtBQUssRUFBRSxDQUFDO1NBQ1o7SUFDSCxDQUFDO0lBQ08sMENBQVcsR0FBbkIsVUFBb0IsU0FBcUI7UUFBckIsMEJBQUEsRUFBQSxhQUFxQjtRQUN2QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFFaEIsS0FBSyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUU7WUFDckUsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUN4Qyw0QkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7Z0JBQzdILElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzthQUNyRDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7YUFDdEQ7U0FDRjtJQUNILENBQUM7SUFFRCx3Q0FBUyxHQUFUOztRQUNFLE1BQUEsSUFBSSxDQUFDLFVBQVUsMENBQUUsSUFBSSxHQUFHO1FBRXhCLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUMvQyxLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUU7WUFDL0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNoRDtRQUVELEtBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO1lBQ3JFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUN0RDtRQUNELElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBRW5CLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBTztZQUNuQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsVUFBVTtJQUVWLDBCQUEwQjtJQUMxQiwwQkFBMEI7SUFDMUIsZ0RBQWlCLEdBQWpCLFVBQWtCLFVBQWtCO1FBQ2xDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsZUFBZSxDQUNsQixVQUFVLEVBQ1YsSUFBSSxDQUFDLGlCQUFpQixFQUN0QjtZQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzNELENBQUMsRUFDRDtZQUNFLElBQUksUUFBUSxHQUFHLEdBQUcsQ0FBQztZQUNuQixJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNsRSxDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUM7SUFFRCxxREFBc0IsR0FBdEIsVUFBdUIsVUFBa0I7UUFDdkMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxlQUFlLENBQ2xCLFVBQVUsRUFDVixJQUFJLENBQUMsa0JBQWtCLEVBQ3ZCO1lBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDaEUsQ0FBQyxFQUNEO1lBQ0UsSUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDO1lBQ25CLElBQUksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZFLENBQUMsQ0FDRixDQUFDO0lBQ0osQ0FBQztJQUVELDRCQUE0QjtJQUM1QixrREFBbUIsR0FBbkIsVUFBb0IsVUFBa0I7UUFBdEMsaUJBMEJDO1FBekJDLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzlDLG9EQUFvRDtRQUNwRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7UUFDekIsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2pFLGdCQUFnQixHQUFHLENBQUMsQ0FBQztTQUN0QjthQUFNO1lBQ0wsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUNoQixLQUFLLENBQUMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFVBQVUsQ0FBQztpQkFDNUMsSUFBSSxDQUFDO2dCQUNKLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsR0FBRyxDQUFDO2dCQUVqRixFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUM7cUJBQ2hCLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQztxQkFDdkIsSUFBSSxDQUFDO29CQUNKLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQyxNQUFjLEVBQUUsSUFBSTt3QkFDekMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7b0JBQ3BELENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksYUFBYSxDQUFDLElBQUksR0FBRyxDQUFDO3dCQUFFLHlCQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyx5QkFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNsRixDQUFDLENBQUM7cUJBQ0QsS0FBSyxFQUFFLENBQUM7WUFDYixDQUFDLENBQUM7aUJBQ0QsS0FBSyxFQUFFLENBQUM7U0FDWjtJQUNILENBQUM7SUFFRCw0QkFBNEI7SUFDNUIsa0RBQW1CLEdBQW5CLFVBQW9CLFVBQWtCO1FBQXRDLGlCQW1EQztRQWxEQyw4REFBOEQ7UUFDOUQsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUNyQyxzR0FBc0c7WUFDdEcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBQyxNQUFNO2dCQUMxQyxJQUFJLElBQUksR0FBRyxxQkFBVyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7Z0JBRWpFLElBQUksV0FBVyxHQUFHLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDckMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDekYsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxNQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztpQkFDaEIsS0FBSyxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsVUFBVSxDQUFDO2lCQUN4QyxJQUFJLENBQUM7Z0JBQ0osTUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBQyxNQUFNO29CQUMxQyxJQUFJLElBQUksR0FBRyxxQkFBVyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7b0JBQ2pFLE1BQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7Z0JBQzdGLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksdUJBQXVCLEdBQUcsSUFBSSxDQUFDO2dCQUVuQyxJQUFJLFdBQVcsR0FBRyxNQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXO2dCQUMxRyxJQUFJLFdBQVcsR0FBRyxNQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRWhHLElBQUksTUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLDRCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsRUFBRSxJQUFJLE1BQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLEVBQUUsRUFBRTtvQkFDbEksY0FBYztvQkFDZCxXQUFXLElBQUksTUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUMxRCxJQUFJLGtCQUFrQixHQUFHLE1BQUksQ0FBQyxNQUFNLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQy9HLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO2lCQUN6RDtnQkFFRCxNQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLFdBQVcsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO2dCQUUxRSxJQUFJLGlCQUFpQixHQUFHLE1BQUksQ0FBQyxlQUFlLENBQUMsTUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsRUFBRSxNQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztnQkFDM0csaUJBQWlCLENBQUMsT0FBTyxDQUFDLFVBQUMsTUFBYyxFQUFFLE1BQWM7b0JBQ3ZELElBQUksSUFBSSxHQUFHLHFCQUFXLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDckQsSUFBSSxNQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLElBQUksTUFBTSxJQUFJLE1BQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLEVBQUU7d0JBQ3hGLGNBQWM7d0JBQ2QsTUFBTSxJQUFJLE1BQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFFdkQsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDO3lCQUNoQixLQUFLLENBQUMsdUJBQXVCLENBQUM7eUJBQzlCLElBQUksQ0FBQzt3QkFDSixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUM1QixDQUFDLENBQUM7eUJBQ0QsS0FBSyxFQUFFLENBQUM7Z0JBQ2IsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLENBQUM7aUJBQ0QsS0FBSyxFQUFFLENBQUM7U0FDWjtJQUNILENBQUM7SUFFRCx3QkFBd0I7SUFDeEIsOENBQWUsR0FBZixVQUFnQixPQUE2QyxFQUFFLGFBQWE7UUFBNUUsaUJBMEJDO1FBekJDLElBQUksTUFBTSxHQUF3QixJQUFJLEdBQUcsRUFBa0IsQ0FBQztRQUM1RCxJQUFJLFFBQVEsR0FBRyw0QkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUU5RCxJQUFJLGFBQWEsSUFBSSxJQUFJLEVBQUU7WUFDekIsSUFBSSxhQUFhLEdBQUcsYUFBYSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzlDLElBQUksUUFBUSxJQUFJLGFBQWEsRUFBRTtnQkFDN0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDO29CQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQzlEO1NBQ0Y7UUFFRCxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUMsTUFBTTtZQUNyQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbEIsSUFBSSxNQUFNLENBQUMsU0FBUyxFQUFFO2dCQUFFLE1BQU0sR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDO2lCQUMvQztnQkFDSCxNQUFNLEdBQUcsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQzdCO1lBRUQsSUFBSSxNQUFNLElBQUksUUFBUSxFQUFFO2dCQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUM7b0JBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBRS9DLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7YUFDN0Q7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFTyw4Q0FBZSxHQUF2QixVQUF3QixVQUFrQixFQUFFLFNBQWlCLEVBQUUsa0JBQThCLEVBQUUsbUJBQStCO1FBQzVILElBQUksVUFBVSxHQUFHLFNBQVMsRUFBRTtZQUMxQixJQUFJLHFCQUFXLENBQUMsa0JBQWtCLENBQUMsVUFBVSxHQUFHLFNBQVMsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQUU7Z0JBQ2xFLG1CQUFtQixJQUFJLG1CQUFtQixFQUFFLENBQUM7YUFDOUM7aUJBQU07Z0JBQ0wsa0JBQWtCLElBQUksa0JBQWtCLEVBQUUsQ0FBQzthQUM1QztTQUNGO2FBQU07WUFDTCxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7aUJBQ2hCLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQzFDLElBQUksQ0FBQztnQkFDSixtQkFBbUIsSUFBSSxtQkFBbUIsRUFBRSxDQUFDO1lBQy9DLENBQUMsQ0FBQztpQkFDRCxLQUFLLEVBQUUsQ0FBQztTQUNaO0lBQ0gsQ0FBQztJQUVELGdEQUFpQixHQUFqQixVQUFrQixNQUFjLEVBQUUsV0FBVyxFQUFFLEtBQWlCO1FBQWpCLHNCQUFBLEVBQUEsU0FBaUI7UUFDOUQsSUFBSSxNQUFNLElBQUksQ0FBQztZQUFFLE9BQU87UUFDeEIsSUFBSSxJQUFJLEdBQUcscUJBQVcsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDN0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2FBQ2hCLEtBQUssQ0FBQyxLQUFLLENBQUM7YUFDWixJQUFJLENBQUM7WUFDSix5QkFBZSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBQzdELGtDQUFrQztZQUNsQyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzFCLHlCQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyx5QkFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzNELENBQUMsQ0FBQzthQUNELEtBQUssRUFBRSxDQUFDO0lBQ2IsQ0FBQztJQUVELDJCQUEyQjtJQUMzQixpREFBa0IsR0FBbEI7UUFDRSxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzlDLElBQUksRUFBRSxHQUFxQixJQUFJLEdBQUcsRUFBZSxDQUFDO1FBQ2xELElBQUksTUFBTSxHQUFHLDRCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRTVELFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQyxNQUFNO1lBQ3hCLElBQUksTUFBTSxDQUFDLFNBQVMsRUFBRSxJQUFJLE1BQU0sRUFBRTtnQkFDaEMsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNyQyxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ2hCLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxTQUFTLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ2pEO3FCQUFNO29CQUNMLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO2lCQUNsQzthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRCxpREFBa0IsR0FBbEIsVUFBbUIsS0FBYSxFQUFFLElBQUksRUFBRSxVQUFzQjtRQUF0QiwyQkFBQSxFQUFBLGNBQXNCO1FBQzVELElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLElBQUksTUFBTSxHQUFjLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztRQUNyRCxRQUFRLElBQUksRUFBRTtZQUNaLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUNyQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDZCxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDaEIsS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3JCLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUNyQixLQUFLLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDdkIsS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ3RCLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUN0QixLQUFLLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDckIsS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3JCLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUNyQixLQUFLLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDdkIsS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ3RCLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUN0QixLQUFLLElBQUksQ0FBQyxVQUFVO2dCQUNsQixNQUFNLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDO2dCQUN0QyxNQUFNO1NBQ1Q7UUFFRCxJQUFJLElBQUksR0FBRyx3QkFBYyxDQUFDLG9CQUFvQixDQUFDLCtCQUFxQixFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEYsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELG9EQUFxQixHQUFyQjtRQUNFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBTzs7WUFDbkMsTUFBQSxPQUFPLENBQUMsSUFBSSwwQ0FBRSxPQUFPLEdBQUc7UUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUNELFVBQVU7SUFFVixtREFBb0IsR0FBcEIsVUFBcUIsT0FBNkQ7UUFDaEYsSUFBSSx3QkFBYyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQzdDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3JDLE9BQU87U0FDUjtRQUVELElBQUksVUFBVSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM1QixxQ0FBcUM7UUFDckMsNkJBQTZCO1FBRTdCLElBQUksd0JBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUNoRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNyQyxPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNwQyxJQUFJLElBQUksR0FBRyxzQkFBc0IsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO1FBQ25GLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN2QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsd0NBQVMsR0FBVDtRQUNFLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELHFDQUFNLEdBQU47UUFDRSx5QkFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMseUJBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUM1RCxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQy9DLHVCQUF1QjtRQUN2QixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQS9xQkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs2REFDVTtJQUk1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2lFQUNjO0lBR2hDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7K0RBQ1k7SUFHbkM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsrREFDWTtJQUc5QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dFQUNhO0lBRy9CO1FBREMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDOzZEQUNRO0lBRzlCO1FBREMsUUFBUSxDQUFDLCtCQUFxQixDQUFDO2tFQUNlO0lBRy9DO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0VBQ2E7SUFHbkM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzt5RUFDc0I7SUFHMUM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzt5RUFDc0I7SUFHMUM7UUFEQyxRQUFRLENBQUMseUJBQWUsQ0FBQzs0REFDUztJQWpDaEIsb0JBQW9CO1FBRHhDLE9BQU87T0FDYSxvQkFBb0IsQ0FrckJ4QztJQUFELDJCQUFDO0NBbHJCRCxBQWtyQkMsQ0FsckJpRCxFQUFFLENBQUMsU0FBUyxHQWtyQjdEO2tCQWxyQm9CLG9CQUFvQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgU2ljYm9Nb2R1bGVBZGFwdGVyIGZyb20gXCIuLi8uLi8uLi8uLi8uLi9TaWNib01vZHVsZUFkYXB0ZXJcIjtcblxuY29uc3QgeyBHZXRDaGFubmVsSGlzdG9yeVJlcGx5LCBEb29yLCBTdGF0ZSwgUmVzdWx0LCBTdGF0dXMsIENoYW5uZWxIaXN0b3J5TWV0YWRhdGEgfSA9IFNpY2JvTW9kdWxlQWRhcHRlci5nZXRBbGxSZWZzKCk7XG5cbmltcG9ydCB7IFNpY2JvU291bmQgfSBmcm9tIFwiLi4vLi4vU2V0dGluZy9TaWNib1NldHRpbmdcIjtcbmltcG9ydCBTaWNib0RpY2VDb25maWcgZnJvbSBcIi4uLy4uL0NvbmZpZ3MvU2ljYm9EaWNlQ29uZmlnXCI7XG5pbXBvcnQgU2ljYm9Db250cm9sbGVyIGZyb20gXCIuLi8uLi9Db250cm9sbGVycy9TaWNib0NvbnRyb2xsZXJcIjtcbmltcG9ydCBTaWNib0hlbHBlciBmcm9tIFwiLi4vLi4vSGVscGVycy9TaWNib0hlbHBlclwiO1xuXG5pbXBvcnQgU2ljYm9CZXRDb21wIGZyb20gXCIuLi9TaWNib0JldENvbXBcIjtcbmltcG9ydCBTaWNib0xhdGVzdFJlc3VsdENvbXAgZnJvbSBcIi4uL1NpY2JvTGF0ZXN0UmVzdWx0Q29tcFwiO1xuaW1wb3J0IFNpY2JvTm90aWZ5Q29tcCBmcm9tIFwiLi4vU2ljYm9Ob3RpZnlDb21wXCI7XG5pbXBvcnQgU2ljYm9TbG90V2luTW9uZXlDb21wIGZyb20gXCIuLi9TaWNib1Nsb3RXaW5Nb25leUNvbXBcIjtcbmltcG9ydCBTaWNib1VJQ29tcCBmcm9tIFwiLi4vU2ljYm9VSUNvbXBcIjtcbmltcG9ydCBJU2ljYm9TdGF0ZSBmcm9tIFwiLi9JU2ljYm9TdGF0ZVwiO1xuaW1wb3J0IFNpY2JvUG9ydGFsQWRhcHRlciBmcm9tIFwiLi4vLi4vU2ljYm9Qb3J0YWxBZGFwdGVyXCI7XG5pbXBvcnQgU2ljYm9HYW1lVXRpbHMgZnJvbSBcIi4uLy4uL1JOR0NvbW1vbnMvVXRpbHMvU2ljYm9HYW1lVXRpbHNcIjtcbmltcG9ydCBTaWNib1NrZWxldG9uVXRpbHMgZnJvbSBcIi4uLy4uL1JOR0NvbW1vbnMvVXRpbHMvU2ljYm9Ta2VsZXRvblV0aWxzXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1Jlc3VsdFN0YXRlQ29tcCBleHRlbmRzIGNjLkNvbXBvbmVudCBpbXBsZW1lbnRzIElTaWNib1N0YXRlIHtcbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIGJldENvbXBOb2RlOiBjYy5Ob2RlID0gbnVsbDtcbiAgcHJpdmF0ZSBiZXRDb21wOiBTaWNib0JldENvbXAgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICB3aW5GeFNoaW5ueU5vZGU6IGNjLk5vZGUgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5BbmltYXRpb24pXG4gIGNoZW5BbmltYXRpb246IGNjLkFuaW1hdGlvbiA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIGNoZW5TbWFsbFJvb3Q6IGNjLk5vZGUgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBjaGVuWm9vbUluUm9vdDogY2MuTm9kZSA9IG51bGw7XG5cbiAgQHByb3BlcnR5KFtjYy5TcHJpdGVdKVxuICBkaWNlU3ByaXRlczogY2MuU3ByaXRlW10gPSBbXTtcblxuICBAcHJvcGVydHkoU2ljYm9MYXRlc3RSZXN1bHRDb21wKVxuICBsYXRlc3RSZXN1bHRDb21wOiBTaWNib0xhdGVzdFJlc3VsdENvbXAgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShzcC5Ta2VsZXRvbilcbiAgZGVhbGVyU2tlbGV0b246IHNwLlNrZWxldG9uID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuUHJlZmFiKVxuICBzbG90V2luTW9uZXlTbWFsbFByZWZhYjogY2MuUHJlZmFiID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuUHJlZmFiKVxuICBzbG90V2luTW9uZXlMYXJnZVByZWZhYjogY2MuUHJlZmFiID0gbnVsbDtcblxuICBAcHJvcGVydHkoU2ljYm9Ob3RpZnlDb21wKVxuICBub3RpZnlDb21wOiBTaWNib05vdGlmeUNvbXAgPSBudWxsO1xuXG4gIHByaXZhdGUgd2luRnhTa2VsZXRvbnM6IHNwLlNrZWxldG9uW10gPSBbXTtcbiAgcHJpdmF0ZSB3aW5GeFNoaW5ueVNrZWxldG9uczogc3AuU2tlbGV0b25bXSA9IFtdO1xuICBwcml2YXRlIGlzV2luRnhQbGF5aW5nID0gZmFsc2U7XG4gIHByaXZhdGUgd2luRnhUd2VlbjogY2MuVHdlZW4gPSBudWxsO1xuXG4gIHByaXZhdGUgY3VyU2Vzc2lvbklkOiBudW1iZXI7XG4gIHByaXZhdGUgcmVzdWx0OiBJbnN0YW5jZVR5cGU8dHlwZW9mIFJlc3VsdD47XG4gIHByaXZhdGUgd29uRG9vcnM6IGFueVtdID0gW107XG4gIHByaXZhdGUgd29uRG9vcnNGaWx0ZXJlZDogYW55W10gPSBbXTtcbiAgcHJpdmF0ZSBpdGVtczogYW55W10gPSBbXTtcblxuICBwcml2YXRlIGNoZW5PcGVuQW5pbSA9IFwiQ2hlbk9wZW5cIjtcbiAgcHJpdmF0ZSBjaGVuSWRsZUFuaW0gPSBcIkNoZW5JZGxlXCI7XG4gIC8vIHByaXZhdGUgY2hlbkNsb3NlQW5pbSA9IFwiQ2hlbkNsb3NlXCI7XG4gIHByaXZhdGUgY2hlblJvbGxEaWNlQW5pbSA9IFwiY2hlblJvbGxEaWNlXCI7XG5cbiAgLy9OT1RFIHN0YXJ0VGltZSB0aW5oIHR1IGtoaSBzdGF0ZSBiYXQgZGF1XG4gIC8vQU5DSE9SIFN0YXR1cy5SRVNVTFRJTkcgc3RhcnQgdGltZVxuICBwcml2YXRlIGNoZW5Sb2xsRGljZVN0YXJ0VGltZSA9IDAuMTtcbiAgcHJpdmF0ZSBjaGVuUm9sbERpY2VTZnhTdGFydFRpbWUgPSB0aGlzLmNoZW5Sb2xsRGljZVN0YXJ0VGltZSArIDAuMjtcbiAgcHJpdmF0ZSBjaGVuUm9sbERpY2VTZnhTdG9wVGltZSA9IHRoaXMuY2hlblJvbGxEaWNlU2Z4U3RhcnRUaW1lICsgMC44O1xuXG4gIHByaXZhdGUgY2hlbk9wZW5TdGFydFRpbWUgPSAwLjU7IC8vTk9URSAxO1xuICBwcml2YXRlIGNoZW5PcGVuU2Z4U3RhcnRUaW1lID0gdGhpcy5jaGVuT3BlblN0YXJ0VGltZSArIDEuNTtcbiAgLy8gcHJpdmF0ZSBjaGVuQ2xvc2VTdGFydFRpbWUgPSAwO1xuICBwcml2YXRlIGNoZW5JZGxlU3RhcnRUaW1lID0gdGhpcy5jaGVuT3BlblN0YXJ0VGltZSArIDU7XG4gIHByaXZhdGUgZGljZXNGbHlUb0xhdGVzdFJlc3VsdFN0YXJ0VGltZSA9IHRoaXMuY2hlbklkbGVTdGFydFRpbWUgKyAwLjc7XG4gIHByaXZhdGUgZG9vcldpbkZ4U2hpbm55U3RhcnRUaW1lID0gdGhpcy5jaGVuSWRsZVN0YXJ0VGltZSArIDAuNTtcbiAgcHJpdmF0ZSBkb29yV2luRnhTdGFydFRpbWUgPSB0aGlzLmNoZW5JZGxlU3RhcnRUaW1lICsgKzIuMTtcblxuICAvL0FOQ0hPUiBTdGF0dXMuUEFZSU5HIHN0YXJ0IHRpbWVcbiAgcHJpdmF0ZSBjaGlwTG9zZVN0YXJ0VGltZSA9IDA7XG4gIHByaXZhdGUgY2hpcFN0YWNrU3RhcnRUaW1lID0gMTtcbiAgcHJpdmF0ZSByZXR1cm5DaGlwU3RhcnRUaW1lID0gMyAtIDE7XG4gIHByaXZhdGUgcGF5aW5nU3RhcnRUaW1lID0gNiAtIDE7XG5cbiAgcHJpdmF0ZSBpc1BheWluZ1BsYXlpbmc6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAvL290aGVyIHVzZXIgZ3JvdXAga2V5XG4gIHByaXZhdGUgb3RoZXJHcm91cEtleSA9IFwib3RoZXJHcm91cEtleVwiO1xuXG4gIHdpbk1vbmV5QXRTbG90czogU2ljYm9TbG90V2luTW9uZXlDb21wW10gPSBbXTtcblxuICBvbkxvYWQoKSB7XG4gICAgdGhpcy5iZXRDb21wID0gdGhpcy5iZXRDb21wTm9kZS5nZXRDb21wb25lbnQoU2ljYm9CZXRDb21wKTtcblxuICAgIHRoaXMud2luRnhTaGlubnlTa2VsZXRvbnMgPSB0aGlzLndpbkZ4U2hpbm55Tm9kZS5nZXRDb21wb25lbnRzSW5DaGlsZHJlbihzcC5Ta2VsZXRvbik7XG4gICAgdGhpcy53aW5GeFNrZWxldG9ucyA9IHRoaXMud2luRnhTaGlubnlTa2VsZXRvbnM7XG5cbiAgICB0aGlzLmhpZGVXaW5GeCgpO1xuICB9XG5cbiAgLy9TRUNUSU9OIElTaWNib1N0YXRlXG4gIGV4aXRTdGF0ZShzdGF0dXMpOiB2b2lkIHtcbiAgICB0aGlzLmlzUGF5aW5nUGxheWluZyA9IGZhbHNlO1xuICB9XG5cbiAgdXBkYXRlU3RhdGUoc3RhdGU6IEluc3RhbmNlVHlwZTx0eXBlb2YgU3RhdGU+KTogdm9pZCB7XG4gICAgaWYgKHRoaXMuaXNQYXlpbmdQbGF5aW5nKSByZXR1cm47XG4gICAgbGV0IHN0YXR1cyA9IHN0YXRlLmdldFN0YXR1cygpO1xuICAgIGxldCBlbGFwc2VUaW1lID0gc3RhdGUuZ2V0U3RhZ2VUaW1lKCkgLyAxMDAwO1xuXG4gICAgaWYgKHN0YXR1cyA9PSBTdGF0dXMuUEFZSU5HICYmIGVsYXBzZVRpbWUgPj0gMC4xKSB7XG4gICAgICAvL05PVEUgc2VydmVyIGRlbGF5IDAuMXMgZm9yIGdlbmVyYXRlIHJlc3VsdFxuICAgICAgdGhpcy5zZXRSZXN1bHQoc3RhdGUuZ2V0UmVzdWx0KCkpO1xuICAgICAgLy9jYy5sb2coXCJ1cGRhdGVTdGF0ZVwiLCB0aGlzLnJlc3VsdC50b09iamVjdCgpKTtcbiAgICAgIHRoaXMucGxheVBheWluZ0FuaW1hdGlvbihlbGFwc2VUaW1lKTtcbiAgICAgIHRoaXMuaXNQYXlpbmdQbGF5aW5nID0gdHJ1ZTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHNldFJlc3VsdChyZXN1bHQ6IEluc3RhbmNlVHlwZTx0eXBlb2YgUmVzdWx0Pikge1xuICAgIGlmIChTaWNib0dhbWVVdGlscy5pc051bGxPclVuZGVmaW5lZChyZXN1bHQpKSByZXR1cm47XG4gICAgLy9jYy5sb2coXCJSRVNVTFQxXCIsIHJlc3VsdC5oYXNQbGF5ZXJCZXRUeCgpLCByZXN1bHQudG9PYmplY3QoKSk7XG4gICAgdGhpcy5yZXN1bHQgPSByZXN1bHQ7XG4gICAgdGhpcy5pdGVtcyA9IHJlc3VsdC5nZXRJdGVtc0xpc3QoKS5zb3J0KCk7XG4gICAgdGhpcy53b25Eb29ycyA9IHJlc3VsdC5nZXREb29yc0xpc3QoKTtcblxuICAgIHRoaXMud29uRG9vcnNGaWx0ZXJlZCA9IHRoaXMud29uRG9vcnMuZmlsdGVyKGZ1bmN0aW9uIChlbGVtLCBpbmRleCwgc2VsZikge1xuICAgICAgcmV0dXJuIGluZGV4ID09PSBzZWxmLmluZGV4T2YoZWxlbSk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnNldERpY2VSZXN1bHQoKTtcbiAgfVxuXG4gIHN0YXJ0U3RhdGUoc3RhdGU6IEluc3RhbmNlVHlwZTx0eXBlb2YgU3RhdGU+LCB0b3RhbER1cmF0aW9uOiBudW1iZXIpOiB2b2lkIHtcbiAgICB0aGlzLmN1clNlc3Npb25JZCA9IHN0YXRlLmdldFRhYmxlU2Vzc2lvbklkKCk7XG4gICAgbGV0IHN0YXR1cyA9IHN0YXRlLmdldFN0YXR1cygpO1xuICAgIGxldCBlbGFwc2VUaW1lID0gc3RhdGUuZ2V0U3RhZ2VUaW1lKCkgLyAxMDAwO1xuICAgIHRoaXMuc2V0UmVzdWx0KHN0YXRlLmdldFJlc3VsdCgpKTtcbiAgICAvL2NvbnNvbGUuZXJyb3IoU2ljYm9IZWxwZXIuU3RhdHVzVG9TdHJpbmcoc3RhdHVzKSwgZWxhcHNlVGltZSk7XG4gICAgc3dpdGNoIChzdGF0dXMpIHtcbiAgICAgIGNhc2UgU3RhdHVzLldBSVRJTkc6XG4gICAgICAgIFNpY2JvSGVscGVyLmNoYW5nZVBhcmVudCh0aGlzLmNoZW5BbmltYXRpb24ubm9kZSwgdGhpcy5jaGVuWm9vbUluUm9vdCk7XG4gICAgICAgIHRoaXMuY2hlblJvbGxEaWNlKGVsYXBzZVRpbWUpO1xuICAgICAgICB0aGlzLmNoZW5Sb2xsRGljZVNmeChlbGFwc2VUaW1lKTtcbiAgICAgICAgdGhpcy5jaGVuUm9sbERpY2VTZngoZWxhcHNlVGltZSwgZmFsc2UpO1xuXG4gICAgICAgIHRoaXMuaGlkZVdpbkZ4KCk7XG4gICAgICAgIHRoaXMubGF0ZXN0UmVzdWx0Q29tcC5wbGF5RngoZmFsc2UpO1xuXG4gICAgICAgIHRoaXMuZGVzdHJveVdpbk1vbmV5QXREb29yKCk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBTdGF0dXMuUkVTVUxUSU5HOlxuICAgICAgICB0aGlzLnNldERpY2VSZXN1bHQoKTtcblxuICAgICAgICBTaWNib0hlbHBlci5jaGFuZ2VQYXJlbnQodGhpcy5jaGVuQW5pbWF0aW9uLm5vZGUsIHRoaXMuY2hlblpvb21JblJvb3QpO1xuICAgICAgICB0aGlzLmNoZW5PcGVuKGVsYXBzZVRpbWUpO1xuICAgICAgICB0aGlzLmNoZW5PcGVuU2Z4KGVsYXBzZVRpbWUpO1xuICAgICAgICB0aGlzLmNoZW5JZGxlKGVsYXBzZVRpbWUpO1xuXG4gICAgICAgIHRoaXMuZGljZXNGbHlUb0xhdGVzdFJlc3VsdChlbGFwc2VUaW1lKTtcblxuICAgICAgICB0aGlzLnBsYXlXaW5GeFNoaW5ueShlbGFwc2VUaW1lKTtcbiAgICAgICAgdGhpcy5wbGF5V2luRngoZWxhcHNlVGltZSk7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIFN0YXR1cy5QQVlJTkc6XG4gICAgICAgIC8vIHRoaXMuZGVzdHJveVdpbk1vbmV5QXREb29yKCk7XG4gICAgICAgIC8vIHRoaXMuc3RhdGlzdGljQ29tcC5zaG93SGlkZVNvaUNhdU5ld0VmZmVjdCh0cnVlKTtcbiAgICAgICAgU2ljYm9IZWxwZXIuY2hhbmdlUGFyZW50KHRoaXMuY2hlbkFuaW1hdGlvbi5ub2RlLCB0aGlzLmNoZW5TbWFsbFJvb3QpO1xuXG4gICAgICAgIHRoaXMuY2hlbklkbGUoMTAwKTtcblxuICAgICAgICAvLy8vY29uc29sZS5sb2coXCJCQ0YgUEFZSU5HIFdpbm5lcnNMaXN0XCIsIGVsYXBzZVRpbWUsIHN0YXRlLmdldFJlc3VsdCgpPy5nZXRXaW5uZXJzTGlzdCgpKTtcbiAgICAgICAgLy8gLy9jb25zb2xlLmxvZyhcIkJDRiBQQVlJTkcgUmVjZWlwdFwiLCBlbGFwc2VUaW1lLCBzdGF0ZS5nZXRSZXN1bHQoKT8uZ2V0UGxheWVyQmV0VHgoKSk7XG5cbiAgICAgICAgdGhpcy5wbGF5V2luRngoMTAwKTtcbiAgICAgICAgdGhpcy5sYXRlc3RSZXN1bHRDb21wLnBsYXlGeCgpO1xuXG4gICAgICAgIC8vIHRoaXMucGxheUNoaXBJbkRvb3JBbmltYXRpb24oZWxhcHNlVGltZSk7XG4gICAgICAgIHRoaXMucGxheUxvc2VBbmltYXRpb24oZWxhcHNlVGltZSk7XG4gICAgICAgIHRoaXMucGxheUNoaXBTdGFja0FuaW1hdGlvbihlbGFwc2VUaW1lKTtcblxuICAgICAgICB0aGlzLmRlYWxlclJldHVybldpbkNoaXAoZWxhcHNlVGltZSk7XG4gICAgICAgIC8vIHRoaXMucGxheVBheWluZ0FuaW1hdGlvbihlbGFwc2VUaW1lKTsgIC8vTk9URSBzZXJ2ZXIgZGVsYXkgMC4xcyBmb3IgZ2VuZXJhdGUgcmVzdWx0XG4gICAgICAgIGJyZWFrO1xuICAgICAgLy8gY2FzZSBTdGF0dXMuRklOSVNISU5HOlxuICAgICAgLy8gICByZXR1cm47Ly9GSVhNRVxuICAgICAgLy8gdGhpcy5jaGVuQ2xvc2UoZWxhcHNlVGltZSk7XG4gICAgICAvLyB0aGlzLnN0YXRpc3RpY0NvbXAuc2hvd0hpZGVTb2lDYXVOZXdFZmZlY3QoZmFsc2UpO1xuXG4gICAgICBkZWZhdWx0OlxuICAgICAgICB0aGlzLmhpZGVXaW5GeCgpO1xuICAgICAgICB0aGlzLmxhdGVzdFJlc3VsdENvbXAucGxheUZ4KGZhbHNlKTtcbiAgICB9XG4gIH1cblxuICAvLyFTRUNUSU9OXG5cbiAgLy9TRUNUSU9OIENoZW5cbiAgc2V0RGljZVJlc3VsdCgpIHtcbiAgICAvL2NjLmxvZyhcIkRJQ0VTXCIsIHRoaXMuaXRlbXMpO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5pdGVtcy5sZW5ndGg7IGkrKykge1xuICAgICAgbGV0IHNwcml0ZU5hbWUgPSBTaWNib0hlbHBlci5jb252ZXJ0SXRlbVRvRGljZVNwcml0ZU5hbWUodGhpcy5pdGVtc1tpXSk7XG4gICAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgICBzZWxmLmRpY2VTcHJpdGVzW2ldLnNwcml0ZUZyYW1lID0gU2ljYm9EaWNlQ29uZmlnLkluc3RhbmNlLmdldERpY2Uoc3ByaXRlTmFtZSk7XG4gICAgfVxuICB9XG5cbiAgY2hlblJvbGxEaWNlKGVsYXBzZVRpbWU6IG51bWJlciA9IDApIHtcbiAgICBpZiAoZWxhcHNlVGltZSA+IHRoaXMuY2hlblJvbGxEaWNlU3RhcnRUaW1lKSB7XG4gICAgICB0aGlzLmNoZW5BbmltYXRpb24ucGxheSh0aGlzLmNoZW5Sb2xsRGljZUFuaW0sIGVsYXBzZVRpbWUgLSB0aGlzLmNoZW5Sb2xsRGljZVN0YXJ0VGltZSk7XG4gICAgICAvLyB0aGlzLnBsYXlEZWFsZXJPcGVuQW5pbWF0aW9uKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNjLnR3ZWVuKHRoaXMubm9kZSlcbiAgICAgICAgLmRlbGF5KHRoaXMuY2hlblJvbGxEaWNlU3RhcnRUaW1lIC0gZWxhcHNlVGltZSlcbiAgICAgICAgLmNhbGwoKCkgPT4ge1xuICAgICAgICAgIC8vIHRoaXMucGxheURlYWxlck9wZW5BbmltYXRpb24oKTtcbiAgICAgICAgICB0aGlzLmNoZW5BbmltYXRpb24ucGxheSh0aGlzLmNoZW5Sb2xsRGljZUFuaW0pO1xuICAgICAgICB9KVxuICAgICAgICAuc3RhcnQoKTtcbiAgICB9XG4gIH1cblxuICBjaGVuUm9sbERpY2VTZngoZWxhcHNlVGltZTogbnVtYmVyID0gMCwgaXNQbGF5OiBib29sZWFuID0gdHJ1ZSkge1xuICAgIGxldCB0aW1lID0gaXNQbGF5ID8gdGhpcy5jaGVuUm9sbERpY2VTZnhTdGFydFRpbWUgOiB0aGlzLmNoZW5Sb2xsRGljZVNmeFN0b3BUaW1lO1xuICAgIGlmIChlbGFwc2VUaW1lID4gdGltZSkge1xuICAgICAgaWYgKGlzUGxheSkgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLnBsYXlTZngoU2ljYm9Tb3VuZC5TZnhEZWFsZXJTaGFrZSk7XG4gICAgICBlbHNlIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5zdG9wU2Z4KFNpY2JvU291bmQuU2Z4RGVhbGVyU2hha2UpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjYy50d2Vlbih0aGlzLm5vZGUpXG4gICAgICAgIC5kZWxheSh0aW1lIC0gZWxhcHNlVGltZSlcbiAgICAgICAgLmNhbGwoKCkgPT4ge1xuICAgICAgICAgIGlmIChpc1BsYXkpIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4RGVhbGVyU2hha2UpO1xuICAgICAgICAgIGVsc2UgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLnN0b3BTZngoU2ljYm9Tb3VuZC5TZnhEZWFsZXJTaGFrZSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5zdGFydCgpO1xuICAgIH1cbiAgfVxuXG4gIGNoZW5PcGVuKGVsYXBzZVRpbWU6IG51bWJlciA9IDApIHtcbiAgICBpZiAoZWxhcHNlVGltZSA+IHRoaXMuY2hlbk9wZW5TdGFydFRpbWUpIHtcbiAgICAgIHRoaXMuY2hlbkFuaW1hdGlvbi5wbGF5KHRoaXMuY2hlbk9wZW5BbmltLCBlbGFwc2VUaW1lIC0gdGhpcy5jaGVuT3BlblN0YXJ0VGltZSk7XG4gICAgICAvLyB0aGlzLnBsYXlEZWFsZXJPcGVuQW5pbWF0aW9uKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNjLnR3ZWVuKHRoaXMubm9kZSlcbiAgICAgICAgLmRlbGF5KHRoaXMuY2hlbk9wZW5TdGFydFRpbWUgLSBlbGFwc2VUaW1lKVxuICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgLy8gdGhpcy5wbGF5RGVhbGVyT3BlbkFuaW1hdGlvbigpO1xuICAgICAgICAgIHRoaXMuY2hlbkFuaW1hdGlvbi5wbGF5KHRoaXMuY2hlbk9wZW5BbmltKTtcbiAgICAgICAgfSlcbiAgICAgICAgLnN0YXJ0KCk7XG4gICAgfVxuICB9XG5cbiAgY2hlbk9wZW5TZngoZWxhcHNlVGltZTogbnVtYmVyID0gMCkge1xuICAgIGxldCBzZWxmID0gdGhpcztcbiAgICBpZiAoZWxhcHNlVGltZSA+IHRoaXMuY2hlbk9wZW5TZnhTdGFydFRpbWUpIHtcbiAgICB9IGVsc2Uge1xuICAgICAgY2MudHdlZW4oc2VsZi5ub2RlKVxuICAgICAgICAuZGVsYXkoc2VsZi5jaGVuT3BlblNmeFN0YXJ0VGltZSAtIGVsYXBzZVRpbWUpXG4gICAgICAgIC5jYWxsKCgpID0+IHtcbiAgICAgICAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UucGxheVNmeChTaWNib1NvdW5kLlNmeFJlc3VsdCk7XG4gICAgICAgICAgc2VsZi5ub3RpZnlDb21wLnNob3dSZXN1bHROb3RpKHNlbGYuZ2V0UmVzdWx0VGV4dChzZWxmLnJlc3VsdC5nZXRJdGVtc0xpc3QoKSkpO1xuICAgICAgICB9KVxuICAgICAgICAuc3RhcnQoKTtcbiAgICB9XG4gIH1cblxuICBnZXRSZXN1bHRUZXh0KGl0ZW1zOiBhbnlbXSkge1xuICAgIGxldCB0b3RhbCA9IFNpY2JvSGVscGVyLmdldFJlc3VsdFRvdGFsVmFsdWUoaXRlbXMpLnRvU3RyaW5nKCk7XG4gICAgbGV0IHR5cGVSZXN1bHQgPSBTaWNib0hlbHBlci5nZXRSZXN1bHRUeXBlKGl0ZW1zKTtcbiAgICBsZXQgdHlwZU5hbWUgPSBTaWNib0hlbHBlci5nZXRSZXN1bHRUeXBlTmFtZSh0eXBlUmVzdWx0KTtcbiAgICByZXR1cm4gdG90YWwgKyBcIiDEkWnhu4NtIC0gXCIgKyB0eXBlTmFtZTtcbiAgfVxuXG4gIC8vIHByaXZhdGUgcGxheURlYWxlck9wZW5BbmltYXRpb24oKXtcbiAgLy8gICBTaWNib1NrZWxldG9uVXRpbHMuc2V0QW5pbWF0aW9uKHRoaXMuZGVhbGVyU2tlbGV0b24sIFwib3BlblwiLCB0cnVlKTtcbiAgLy8gICAvL2NjLmxvZyhcIkRlYWxlciBPcGVuXCIpO1xuICAvLyB9XG5cbiAgY2hlbklkbGUoZWxhcHNlVGltZTogbnVtYmVyID0gMCkge1xuICAgIGlmIChlbGFwc2VUaW1lID4gdGhpcy5jaGVuSWRsZVN0YXJ0VGltZSkge1xuICAgICAgdGhpcy5jaGVuQW5pbWF0aW9uLnBsYXkodGhpcy5jaGVuSWRsZUFuaW0sIGVsYXBzZVRpbWUgLSB0aGlzLmNoZW5JZGxlU3RhcnRUaW1lKTtcbiAgICB9IGVsc2VcbiAgICAgIGNjLnR3ZWVuKHRoaXMubm9kZSlcbiAgICAgICAgLmRlbGF5KHRoaXMuY2hlbklkbGVTdGFydFRpbWUgLSBlbGFwc2VUaW1lKVxuICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgdGhpcy5jaGVuQW5pbWF0aW9uLnBsYXkodGhpcy5jaGVuSWRsZUFuaW0pO1xuICAgICAgICAgIC8vIFNvdW5kTWFuYWdlci5nZXRJbnN0YW5jZSgpLnBsYXlFZmZMb2NhbE5hbWUoU291bmREZWZpbmUuU2ljYm9fTGlkQ2xvc2UsIFNvdW5kRGVmaW5lLlNpY2JvKTtcbiAgICAgICAgfSlcbiAgICAgICAgLnN0YXJ0KCk7XG4gIH1cblxuICAvLyBjaGVuQ2xvc2UoZWxhcHNlVGltZTogbnVtYmVyID0gMCl7XG4gIC8vICAgaWYoZWxhcHNlVGltZT50aGlzLmNoZW5DbG9zZVN0YXJ0VGltZSlcbiAgLy8gICAgIHRoaXMuY2hlbkFuaW1hdGlvbi5wbGF5KHRoaXMuY2hlbkNsb3NlQW5pbSwgZWxhcHNlVGltZSAtIHRoaXMuY2hlbkNsb3NlU3RhcnRUaW1lKTtcbiAgLy8gICBlbHNlXG4gIC8vICAgICBjYy50d2Vlbih0aGlzLm5vZGUpXG4gIC8vICAgICAgIC5kZWxheSh0aGlzLmNoZW5DbG9zZVN0YXJ0VGltZSAtIGVsYXBzZVRpbWUpXG4gIC8vICAgICAgIC5jYWxsKFxuICAvLyAgICAgICAgICgpPT57XG4gIC8vICAgICAgICAgICB0aGlzLmNoZW5BbmltYXRpb24ucGxheSh0aGlzLmNoZW5DbG9zZUFuaW0pO1xuICAvLyAgICAgICAgICAgLy8gU291bmRNYW5hZ2VyLmdldEluc3RhbmNlKCkucGxheUVmZkxvY2FsTmFtZShTb3VuZERlZmluZS5TaWNib19MaWRDbG9zZSwgU291bmREZWZpbmUuU2ljYm8pO1xuICAvLyAgICAgICAgIH1cbiAgLy8gICAgICAgKS5zdGFydCgpO1xuICAvLyB9XG5cbiAgZGljZXNGbHlUb0xhdGVzdFJlc3VsdChlbGFwc2VUaW1lOiBudW1iZXIgPSAwKSB7XG4gICAgaWYgKGVsYXBzZVRpbWUgPiB0aGlzLmRpY2VzRmx5VG9MYXRlc3RSZXN1bHRTdGFydFRpbWUpIHtcbiAgICAgIC8vIHRoaXMuc3RhdGlzdGljQ29tcC5yZWxvYWRMYXRlc3RSZWNvcmRXaXRoQW5pbWF0aW9uKHRoaXMucmVzdWx0LmdldEl0ZW1zTGlzdCgpKTtcbiAgICAgIC8vIHRoaXMuc3RhdGlzdGljQ29tcC5zaG93SGlkZVNvaUNhdU5ld0VmZmVjdCh0cnVlLCAuNSk7XG4gICAgICAvLyBTaWNib1VJQ29tcC5JbnN0YW5jZS5nZXRIaXN0b3J5KHRydWUpO1xuICAgICAgLy8gU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLnBsYXlTZngoU2ljYm9Tb3VuZC5TZnhNZXNzYWdlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuXG4gICAgICBjYy50d2VlbihzZWxmLm5vZGUpXG4gICAgICAgIC5kZWxheShzZWxmLmRpY2VzRmx5VG9MYXRlc3RSZXN1bHRTdGFydFRpbWUgLSBlbGFwc2VUaW1lKVxuICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgLy9jYy5sb2coXCJkaWNlc0ZseVRvTGF0ZXN0UmVzdWx0U3RhcnRUaW1lXCIsIHNlbGYuZGljZXNGbHlUb0xhdGVzdFJlc3VsdFN0YXJ0VGltZSk7XG5cbiAgICAgICAgICBsZXQgZGljZUZseUR1cmF0aW9uID0gMC41O1xuICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc2VsZi5kaWNlU3ByaXRlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IGRpY2VPcmlnaW4gPSBzZWxmLmRpY2VTcHJpdGVzW2ldLm5vZGU7XG4gICAgICAgICAgICBsZXQgZGljZSA9IGNjLmluc3RhbnRpYXRlKGRpY2VPcmlnaW4pO1xuICAgICAgICAgICAgZGljZS5zZXRQYXJlbnQoZGljZU9yaWdpbi5wYXJlbnQpO1xuICAgICAgICAgICAgZGljZS5zZXRQb3NpdGlvbihkaWNlT3JpZ2luLmdldFBvc2l0aW9uKCkpO1xuICAgICAgICAgICAgZGljZS5zY2FsZSA9IDAuMTY1O1xuXG4gICAgICAgICAgICBTaWNib0hlbHBlci5jaGFuZ2VQYXJlbnQoZGljZSwgc2VsZi5jaGVuWm9vbUluUm9vdCk7XG4gICAgICAgICAgICBsZXQgdGFyZ2V0UG9zID0gc2VsZi5sYXRlc3RSZXN1bHRDb21wLml0ZW1zW2ldLm5vZGUuZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgICAgIHRhcmdldFBvcyA9IFNpY2JvR2FtZVV0aWxzLmNvbnZlcnRUb090aGVyTm9kZTIoc2VsZi5sYXRlc3RSZXN1bHRDb21wLml0ZW1zW2ldLm5vZGUsIHNlbGYuY2hlblpvb21JblJvb3QpO1xuICAgICAgICAgICAgY2MudHdlZW4oZGljZSlcbiAgICAgICAgICAgICAgLnRvKGRpY2VGbHlEdXJhdGlvbiwge1xuICAgICAgICAgICAgICAgIHg6IHRhcmdldFBvcy54LFxuICAgICAgICAgICAgICAgIHk6IHRhcmdldFBvcy55LFxuICAgICAgICAgICAgICAgIHNjYWxlOiAwLjIsXG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgIC5jYWxsKCgpID0+IHtcbiAgICAgICAgICAgICAgICBkaWNlLmRlc3Ryb3koKTtcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgLnN0YXJ0KCk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgY2MudHdlZW4oc2VsZi5ub2RlKVxuICAgICAgICAgICAgLmRlbGF5KGRpY2VGbHlEdXJhdGlvbilcbiAgICAgICAgICAgIC5jYWxsKCgpID0+IHtcbiAgICAgICAgICAgICAgc2VsZi5sYXRlc3RSZXN1bHRDb21wLnNldERhdGEoc2VsZi5yZXN1bHQuZ2V0SXRlbXNMaXN0KCkpO1xuICAgICAgICAgICAgICBzZWxmLmxhdGVzdFJlc3VsdENvbXAucGxheUZ4KCk7XG4gICAgICAgICAgICAgIC8vIHNlbGYuc3RhdGlzdGljQ29tcC5zaG93SGlkZVNvaUNhdU5ld0VmZmVjdCh0cnVlLCAuNSk7XG4gICAgICAgICAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4TWVzc2FnZSk7XG4gICAgICAgICAgICAgIC8vIFNpY2JvVUlDb21wLkluc3RhbmNlLmdldEhpc3RvcnkodHJ1ZSk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnN0YXJ0KCk7XG4gICAgICAgIH0pXG4gICAgICAgIC5zdGFydCgpO1xuICAgIH1cbiAgfVxuICAvLyFTRUNUSU9OXG5cbiAgLy9TRUNUSU9OIERvb3IgV2luRlhcbiAgcGxheVdpbkZ4KGVsYXBzZVRpbWU6IG51bWJlciA9IDApIHtcbiAgICBpZiAodGhpcy53b25Eb29ycy5sZW5ndGggPD0gMCkgcmV0dXJuO1xuICAgIGlmICh0aGlzLmlzV2luRnhQbGF5aW5nKSByZXR1cm47XG5cbiAgICBpZiAoZWxhcHNlVGltZSA+IHRoaXMuZG9vcldpbkZ4U3RhcnRUaW1lKSB7XG4gICAgICB0aGlzLndpbkZ4KCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgIHNlbGYud2luRnhUd2Vlbj8uc3RvcCgpO1xuICAgICAgc2VsZi53aW5GeFR3ZWVuID0gY2NcbiAgICAgICAgLnR3ZWVuKHRoaXMud2luRnhTaGlubnlOb2RlKVxuICAgICAgICAuZGVsYXkodGhpcy5kb29yV2luRnhTdGFydFRpbWUgLSBlbGFwc2VUaW1lKVxuICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgdGhpcy53aW5GeCgpO1xuICAgICAgICB9KVxuICAgICAgICAuc3RhcnQoKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHdpbkZ4KCkge1xuICAgIHRoaXMuaXNXaW5GeFBsYXlpbmcgPSB0cnVlO1xuICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCB0aGlzLndpbkZ4U2tlbGV0b25zLmxlbmd0aDsgaW5kZXgrKykge1xuICAgICAgaWYgKHRoaXMud29uRG9vcnNGaWx0ZXJlZFtpbmRleF0gIT0gbnVsbCkge1xuICAgICAgICBTaWNib1NrZWxldG9uVXRpbHMuc2V0QW5pbWF0aW9uKHRoaXMud2luRnhTa2VsZXRvbnNbaW5kZXhdLCB0aGlzLndvbkRvb3JzRmlsdGVyZWRbaW5kZXhdLnRvU3RyaW5nKCkgKyBcImJcIiwgdHJ1ZSk7XG4gICAgICAgIHRoaXMud2luRnhTa2VsZXRvbnNbaW5kZXhdLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMud2luRnhTa2VsZXRvbnNbaW5kZXhdLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcGxheVdpbkZ4U2hpbm55KGVsYXBzZVRpbWU6IG51bWJlcikge1xuICAgIGxldCBzZWxmID0gdGhpcztcbiAgICBpZiAoZWxhcHNlVGltZSA+IHNlbGYuZG9vcldpbkZ4U2hpbm55U3RhcnRUaW1lKSB7XG4gICAgICBzZWxmLndpbkZ4U2hpbm55KGVsYXBzZVRpbWUgLSBzZWxmLmRvb3JXaW5GeFNoaW5ueVN0YXJ0VGltZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNjLnR3ZWVuKHRoaXMubm9kZSlcbiAgICAgICAgLmRlbGF5KHRoaXMuZG9vcldpbkZ4U2hpbm55U3RhcnRUaW1lIC0gZWxhcHNlVGltZSlcbiAgICAgICAgLmNhbGwoKCkgPT4ge1xuICAgICAgICAgIHNlbGYud2luRnhTaGlubnkoKTtcbiAgICAgICAgfSlcbiAgICAgICAgLnN0YXJ0KCk7XG4gICAgfVxuICB9XG4gIHByaXZhdGUgd2luRnhTaGlubnkoc3RhcnRUaW1lOiBudW1iZXIgPSAwKSB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuXG4gICAgZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IHNlbGYud2luRnhTaGlubnlTa2VsZXRvbnMubGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICBpZiAoc2VsZi53b25Eb29yc0ZpbHRlcmVkW2luZGV4XSAhPSBudWxsKSB7XG4gICAgICAgIFNpY2JvU2tlbGV0b25VdGlscy5zZXRBbmltYXRpb24oc2VsZi53aW5GeFNoaW5ueVNrZWxldG9uc1tpbmRleF0sIHRoaXMud29uRG9vcnNGaWx0ZXJlZFtpbmRleF0udG9TdHJpbmcoKSwgZmFsc2UsIHN0YXJ0VGltZSk7XG4gICAgICAgIHNlbGYud2luRnhTaGlubnlTa2VsZXRvbnNbaW5kZXhdLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNlbGYud2luRnhTaGlubnlTa2VsZXRvbnNbaW5kZXhdLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaGlkZVdpbkZ4KCkge1xuICAgIHRoaXMud2luRnhUd2Vlbj8uc3RvcCgpO1xuXG4gICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMud2luRnhTaGlubnlOb2RlKTtcbiAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgdGhpcy53aW5GeFNrZWxldG9ucy5sZW5ndGg7IGluZGV4KyspIHtcbiAgICAgIHRoaXMud2luRnhTa2VsZXRvbnNbaW5kZXhdLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IHRoaXMud2luRnhTaGlubnlTa2VsZXRvbnMubGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICB0aGlzLndpbkZ4U2hpbm55U2tlbGV0b25zW2luZGV4XS5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgIH1cbiAgICB0aGlzLmlzV2luRnhQbGF5aW5nID0gZmFsc2U7XG4gICAgdGhpcy53b25Eb29ycyA9IFtdO1xuXG4gICAgdGhpcy53aW5Nb25leUF0U2xvdHMuZm9yRWFjaCgoZWxlbWVudCkgPT4ge1xuICAgICAgZWxlbWVudC5oaWRlKCk7XG4gICAgfSk7XG4gIH1cbiAgLy8hU0VDVElPTlxuXG4gIC8vU0VDVElPTiBvdGhlciBiZXRDb21wIEZYXG4gIC8vQU5DSE9SIHBsYXlMb3NlQW5pbWF0aW9uXG4gIHBsYXlMb3NlQW5pbWF0aW9uKGVsYXBzZVRpbWU6IG51bWJlcikge1xuICAgIGxldCBzZWxmID0gdGhpcztcbiAgICBzZWxmLmVsYXBzZVRpbWVDaGVjayhcbiAgICAgIGVsYXBzZVRpbWUsXG4gICAgICBzZWxmLmNoaXBMb3NlU3RhcnRUaW1lLFxuICAgICAgKCkgPT4ge1xuICAgICAgICBzZWxmLmJldENvbXAucGxheUxvc2VBbmltYXRpb24oc2VsZi53b25Eb29yc0ZpbHRlcmVkLCAwKTtcbiAgICAgIH0sXG4gICAgICAoKSA9PiB7XG4gICAgICAgIGxldCBkdXJhdGlvbiA9IDAuNTtcbiAgICAgICAgc2VsZi5iZXRDb21wLnBsYXlMb3NlQW5pbWF0aW9uKHNlbGYud29uRG9vcnNGaWx0ZXJlZCwgZHVyYXRpb24pO1xuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBwbGF5Q2hpcFN0YWNrQW5pbWF0aW9uKGVsYXBzZVRpbWU6IG51bWJlcikge1xuICAgIGxldCBzZWxmID0gdGhpcztcbiAgICBzZWxmLmVsYXBzZVRpbWVDaGVjayhcbiAgICAgIGVsYXBzZVRpbWUsXG4gICAgICBzZWxmLmNoaXBTdGFja1N0YXJ0VGltZSxcbiAgICAgICgpID0+IHtcbiAgICAgICAgc2VsZi5iZXRDb21wLnBsYXlDaGlwU3RhY2tBbmltYXRpb24oc2VsZi53b25Eb29yc0ZpbHRlcmVkLCAwKTtcbiAgICAgIH0sXG4gICAgICAoKSA9PiB7XG4gICAgICAgIGxldCBkdXJhdGlvbiA9IDAuNTtcbiAgICAgICAgc2VsZi5iZXRDb21wLnBsYXlDaGlwU3RhY2tBbmltYXRpb24oc2VsZi53b25Eb29yc0ZpbHRlcmVkLCBkdXJhdGlvbik7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIC8vQU5DSE9SIGRlYWxlclJldHVybldpbkNoaXBcbiAgZGVhbGVyUmV0dXJuV2luQ2hpcChlbGFwc2VUaW1lOiBudW1iZXIpIHtcbiAgICBsZXQgdXNlcldpbkFtb3VudCA9IHRoaXMuZ2V0TXlVc2VyV2luQW1vdW50KCk7XG4gICAgLy8vL2NvbnNvbGUubG9nKFwiQkNGIHVzZXJXaW5BbW91bnRcIiwgdXNlcldpbkFtb3VudCk7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIGxldCBkZWxheVNob3dJbk1vbmV5ID0gMDtcbiAgICBpZiAoZWxhcHNlVGltZSA+IHRoaXMucmV0dXJuQ2hpcFN0YXJ0VGltZSkge1xuICAgICAgdGhpcy5iZXRDb21wLmRlYWxlclJldHVybkZ1bGxTdGFja0NoaXAodGhpcy53b25Eb29yc0ZpbHRlcmVkLCAwKTtcbiAgICAgIGRlbGF5U2hvd0luTW9uZXkgPSAwO1xuICAgIH0gZWxzZSB7XG4gICAgICBjYy50d2Vlbih0aGlzLm5vZGUpXG4gICAgICAgIC5kZWxheSh0aGlzLnJldHVybkNoaXBTdGFydFRpbWUgLSBlbGFwc2VUaW1lKVxuICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgZGVsYXlTaG93SW5Nb25leSA9IHNlbGYuYmV0Q29tcC5kZWFsZXJSZXR1cm5XaW5DaGlwKHNlbGYud29uRG9vcnNGaWx0ZXJlZCkgKyAwLjU7XG5cbiAgICAgICAgICBjYy50d2Vlbih0aGlzLm5vZGUpXG4gICAgICAgICAgICAuZGVsYXkoZGVsYXlTaG93SW5Nb25leSlcbiAgICAgICAgICAgIC5jYWxsKCgpID0+IHtcbiAgICAgICAgICAgICAgdXNlcldpbkFtb3VudC5mb3JFYWNoKChhbW91bnQ6IG51bWJlciwgZG9vcikgPT4ge1xuICAgICAgICAgICAgICAgIHNlbGYuc2hvd1dpbk1vbmV5QXREb29yKGFtb3VudCwgZG9vciwgZWxhcHNlVGltZSk7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICBpZiAodXNlcldpbkFtb3VudC5zaXplID4gMCkgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLnBsYXlTZngoU2ljYm9Tb3VuZC5TZnhXaW4pO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5zdGFydCgpO1xuICAgICAgICB9KVxuICAgICAgICAuc3RhcnQoKTtcbiAgICB9XG4gIH1cblxuICAvL0FOQ0hPUiBwbGF5UGF5aW5nQW5pbWF0aW9uXG4gIHBsYXlQYXlpbmdBbmltYXRpb24oZWxhcHNlVGltZTogbnVtYmVyKSB7XG4gICAgLy9jYy5sb2coXCJTSUNCTyBwbGF5UGF5aW5nQW5pbWF0aW9uXCIsIHRoaXMucmVzdWx0LnRvT2JqZWN0KCkpO1xuICAgIGlmIChlbGFwc2VUaW1lID4gdGhpcy5wYXlpbmdTdGFydFRpbWUpIHtcbiAgICAgIC8vTk9URSBrIHBsYXkgYW5pbWF0aW9uIGNvbmcgdGllbiBvIGF2YXRhciBraGkgdXNlciB2YW8gbHVjIGNvIHJlc3VsdCwgZG8gdGllbiBkYSBkdW9jIHVwZGF0ZSBvIHNlcnZlclxuICAgICAgdGhpcy5yZXN1bHQuZ2V0V2lubmVyc0xpc3QoKS5mb3JFYWNoKChyZWNvcmQpID0+IHtcbiAgICAgICAgbGV0IHVzZXIgPSBTaWNib1VJQ29tcC5JbnN0YW5jZS5maW5kVXNlckNvbXAocmVjb3JkLmdldFVzZXJJZCgpKTtcblxuICAgICAgICBsZXQgcGF5aW5nTW9uZXkgPSByZWNvcmQuZ2V0QW1vdW50KCk7XG4gICAgICAgIHRoaXMuYmV0Q29tcC5wbGF5UGF5aW5nQW5pbWF0aW9uKHJlY29yZC5nZXRCZXQoKS5nZXREb29yKCksIHVzZXIubm9kZSwgcGF5aW5nTW9uZXksIDApO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgIGNjLnR3ZWVuKHRoaXMubm9kZSlcbiAgICAgICAgLmRlbGF5KHRoaXMucGF5aW5nU3RhcnRUaW1lIC0gZWxhcHNlVGltZSlcbiAgICAgICAgLmNhbGwoKCkgPT4ge1xuICAgICAgICAgIHNlbGYucmVzdWx0LmdldFdpbm5lcnNMaXN0KCkuZm9yRWFjaCgocmVjb3JkKSA9PiB7XG4gICAgICAgICAgICBsZXQgdXNlciA9IFNpY2JvVUlDb21wLkluc3RhbmNlLmZpbmRVc2VyQ29tcChyZWNvcmQuZ2V0VXNlcklkKCkpO1xuICAgICAgICAgICAgc2VsZi5iZXRDb21wLnBsYXlQYXlpbmdBbmltYXRpb24ocmVjb3JkLmdldEJldCgpLmdldERvb3IoKSwgdXNlci5ub2RlLCByZWNvcmQuZ2V0QW1vdW50KCkpO1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgbGV0IGRlbGF5U2hvd0F2YXRhcldpbk1vbmV5ID0gMS4wNTtcblxuICAgICAgICAgIGxldCBwYXlpbmdNb25leSA9IHNlbGYucmVzdWx0Lmhhc1BsYXllckJldFR4KCkgPyBzZWxmLnJlc3VsdC5nZXRQbGF5ZXJCZXRUeCgpLmdldEFtb3VudCgpIDogMDsgLy9mcm9tIGNoaXBcbiAgICAgICAgICBsZXQgdHJhbnNhY3Rpb24gPSBzZWxmLnJlc3VsdC5oYXNQbGF5ZXJCZXRUeCgpID8gc2VsZi5yZXN1bHQuZ2V0UGxheWVyQmV0VHgoKS5nZXRMYXN0VHhJZCgpIDogMDtcblxuICAgICAgICAgIGlmIChzZWxmLnJlc3VsdC5oYXNMb3R0ZXJ5V2lubmVyKCkgJiYgU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuZ2V0TXlVc2VySWQoKSA9PSBzZWxmLnJlc3VsdC5nZXRMb3R0ZXJ5V2lubmVyKCkuZ2V0VXNlcklkKCkpIHtcbiAgICAgICAgICAgIC8vZnJvbSBqYWNrcG90XG4gICAgICAgICAgICBwYXlpbmdNb25leSArPSBzZWxmLnJlc3VsdC5nZXRMb3R0ZXJ5V2lubmVyKCkuZ2V0QW1vdW50KCk7XG4gICAgICAgICAgICBsZXQgamFja3BvdFRyYW5zYWN0aW9uID0gc2VsZi5yZXN1bHQuaGFzUGxheWVySmFja3BvdFR4KCkgPyBzZWxmLnJlc3VsdC5nZXRQbGF5ZXJKYWNrcG90VHgoKS5nZXRMYXN0VHhJZCgpIDogMDtcbiAgICAgICAgICAgIHRyYW5zYWN0aW9uID0gTWF0aC5tYXgodHJhbnNhY3Rpb24sIGphY2twb3RUcmFuc2FjdGlvbik7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgc2VsZi5wYXlNb25leUZvck15VXNlcihwYXlpbmdNb25leSwgdHJhbnNhY3Rpb24sIGRlbGF5U2hvd0F2YXRhcldpbk1vbmV5KTtcblxuICAgICAgICAgIGxldCB3b25BbW91bnRGaWx0ZXJlZCA9IHNlbGYuZmlsdGVyV2luQW1vdW50KHNlbGYucmVzdWx0LmdldFdpbm5lcnNMaXN0KCksIHNlbGYucmVzdWx0LmdldExvdHRlcnlXaW5uZXIoKSk7XG4gICAgICAgICAgd29uQW1vdW50RmlsdGVyZWQuZm9yRWFjaCgoYW1vdW50OiBudW1iZXIsIHVzZXJJZDogc3RyaW5nKSA9PiB7XG4gICAgICAgICAgICBsZXQgdXNlciA9IFNpY2JvVUlDb21wLkluc3RhbmNlLmZpbmRVc2VyQ29tcCh1c2VySWQpO1xuICAgICAgICAgICAgaWYgKHNlbGYucmVzdWx0Lmhhc0xvdHRlcnlXaW5uZXIoKSAmJiB1c2VySWQgPT0gc2VsZi5yZXN1bHQuZ2V0TG90dGVyeVdpbm5lcigpLmdldFVzZXJJZCgpKVxuICAgICAgICAgICAgICAvL2Zyb20gamFja3BvdFxuICAgICAgICAgICAgICBhbW91bnQgKz0gc2VsZi5yZXN1bHQuZ2V0TG90dGVyeVdpbm5lcigpLmdldEFtb3VudCgpO1xuXG4gICAgICAgICAgICBjYy50d2Vlbih0aGlzLm5vZGUpXG4gICAgICAgICAgICAgIC5kZWxheShkZWxheVNob3dBdmF0YXJXaW5Nb25leSlcbiAgICAgICAgICAgICAgLmNhbGwoKCkgPT4ge1xuICAgICAgICAgICAgICAgIHVzZXIuc2hvd1dpbk1vbmV5KGFtb3VudCk7XG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgIC5zdGFydCgpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9KVxuICAgICAgICAuc3RhcnQoKTtcbiAgICB9XG4gIH1cblxuICAvL0FOQ0hPUiBmaWx0ZXJXaW5BbW91bnRcbiAgZmlsdGVyV2luQW1vdW50KHJlY29yZHM6IEluc3RhbmNlVHlwZTx0eXBlb2YgUmVzdWx0LlJlY29yZD5bXSwgamFja3BvdFdpbm5lcikge1xuICAgIGxldCByZXN1bHQ6IE1hcDxzdHJpbmcsIG51bWJlcj4gPSBuZXcgTWFwPHN0cmluZywgbnVtYmVyPigpO1xuICAgIGxldCBteVVzZXJJZCA9IFNpY2JvUG9ydGFsQWRhcHRlci5nZXRJbnN0YW5jZSgpLmdldE15VXNlcklkKCk7XG5cbiAgICBpZiAoamFja3BvdFdpbm5lciAhPSBudWxsKSB7XG4gICAgICBsZXQgamFja3BvdFVzZXJJZCA9IGphY2twb3RXaW5uZXIuZ2V0VXNlcklkKCk7XG4gICAgICBpZiAobXlVc2VySWQgIT0gamFja3BvdFVzZXJJZCkge1xuICAgICAgICBpZiAoIXJlc3VsdC5oYXMoamFja3BvdFVzZXJJZCkpIHJlc3VsdC5zZXQoamFja3BvdFVzZXJJZCwgMCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmVjb3Jkcy5mb3JFYWNoKChyZWNvcmQpID0+IHtcbiAgICAgIGxldCB1c2VySWQgPSBudWxsO1xuICAgICAgaWYgKHJlY29yZC5nZXRPdGhlcnMoKSkgdXNlcklkID0gdGhpcy5vdGhlckdyb3VwS2V5O1xuICAgICAgZWxzZSB7XG4gICAgICAgIHVzZXJJZCA9IHJlY29yZC5nZXRVc2VySWQoKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHVzZXJJZCAhPSBteVVzZXJJZCkge1xuICAgICAgICBpZiAoIXJlc3VsdC5oYXModXNlcklkKSkgcmVzdWx0LnNldCh1c2VySWQsIDApO1xuXG4gICAgICAgIHJlc3VsdC5zZXQodXNlcklkLCByZXN1bHQuZ2V0KHVzZXJJZCkgKyByZWNvcmQuZ2V0QW1vdW50KCkpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxuXG4gIHByaXZhdGUgZWxhcHNlVGltZUNoZWNrKGVsYXBzZVRpbWU6IG51bWJlciwgc3RhcnRUaW1lOiBudW1iZXIsIG9uUGFzc2VkRWxhcHNlVGltZTogKCkgPT4gdm9pZCwgb25OT1RQYXNzRWxhcHNlVGltZTogKCkgPT4gdm9pZCkge1xuICAgIGlmIChlbGFwc2VUaW1lID4gc3RhcnRUaW1lKSB7XG4gICAgICBpZiAoU2ljYm9IZWxwZXIuYXBwcm94aW1hdGVseUVxdWFsKGVsYXBzZVRpbWUgLSBzdGFydFRpbWUsIDAsIDAuMSkpIHtcbiAgICAgICAgb25OT1RQYXNzRWxhcHNlVGltZSAmJiBvbk5PVFBhc3NFbGFwc2VUaW1lKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBvblBhc3NlZEVsYXBzZVRpbWUgJiYgb25QYXNzZWRFbGFwc2VUaW1lKCk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGNjLnR3ZWVuKHRoaXMubm9kZSlcbiAgICAgICAgLmRlbGF5KE1hdGgubWF4KHN0YXJ0VGltZSAtIGVsYXBzZVRpbWUsIDApKVxuICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgb25OT1RQYXNzRWxhcHNlVGltZSAmJiBvbk5PVFBhc3NFbGFwc2VUaW1lKCk7XG4gICAgICAgIH0pXG4gICAgICAgIC5zdGFydCgpO1xuICAgIH1cbiAgfVxuXG4gIHBheU1vbmV5Rm9yTXlVc2VyKGFtb3VudDogbnVtYmVyLCB0cmFuc2FjdGlvbiwgZGVsYXk6IG51bWJlciA9IDApIHtcbiAgICBpZiAoYW1vdW50ID09IDApIHJldHVybjtcbiAgICBsZXQgdXNlciA9IFNpY2JvVUlDb21wLkluc3RhbmNlLm15VXNlckNvbXAoKTtcbiAgICBjYy50d2Vlbih0aGlzLm5vZGUpXG4gICAgICAuZGVsYXkoZGVsYXkpXG4gICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5hZGRNeVVzZXJNb25leShhbW91bnQsIHRyYW5zYWN0aW9uKTtcbiAgICAgICAgLy9jYy5sb2coXCJTSUNCTyBSRUNFSVBUXCIsIGFtb3VudCk7XG4gICAgICAgIHVzZXIuc2hvd1dpbk1vbmV5KGFtb3VudCk7XG4gICAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4VG90YWxXaW4pO1xuICAgICAgfSlcbiAgICAgIC5zdGFydCgpO1xuICB9XG5cbiAgLy9BTkNIT1Igc2hvd1dpbk1vbmV5QXREb29yXG4gIGdldE15VXNlcldpbkFtb3VudCgpOiBNYXA8YW55LCBudW1iZXI+IHtcbiAgICBsZXQgd2lubmVyTGlzdCA9IHRoaXMucmVzdWx0LmdldFdpbm5lcnNMaXN0KCk7XG4gICAgbGV0IHJzOiBNYXA8YW55LCBudW1iZXI+ID0gbmV3IE1hcDxhbnksIG51bWJlcj4oKTtcbiAgICBsZXQgdXNlcklkID0gU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuZ2V0TXlVc2VySWQoKTtcblxuICAgIHdpbm5lckxpc3QuZm9yRWFjaCgocmVjb3JkKSA9PiB7XG4gICAgICBpZiAocmVjb3JkLmdldFVzZXJJZCgpID09IHVzZXJJZCkge1xuICAgICAgICBsZXQgZG9vciA9IHJlY29yZC5nZXRCZXQoKS5nZXREb29yKCk7XG4gICAgICAgIGlmIChycy5oYXMoZG9vcikpIHtcbiAgICAgICAgICBycy5zZXQoZG9vciwgcmVjb3JkLmdldEFtb3VudCgpICsgcnMuZ2V0KGRvb3IpKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBycy5zZXQoZG9vciwgcmVjb3JkLmdldEFtb3VudCgpKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBycztcbiAgfVxuXG4gIHNob3dXaW5Nb25leUF0RG9vcihtb25leTogbnVtYmVyLCBkb29yLCBlbGFwc2VUaW1lOiBudW1iZXIgPSAwKSB7XG4gICAgbGV0IGQgPSB0aGlzLmJldENvbXAuZ2V0RG9vcihkb29yKTtcbiAgICBsZXQgcHJlZmFiOiBjYy5QcmVmYWIgPSB0aGlzLnNsb3RXaW5Nb25leVNtYWxsUHJlZmFiO1xuICAgIHN3aXRjaCAoZG9vcikge1xuICAgICAgY2FzZSBEb29yLkFOWV9UUklQTEU6XG4gICAgICBjYXNlIERvb3IuQklHOlxuICAgICAgY2FzZSBEb29yLlNNQUxMOlxuICAgICAgY2FzZSBEb29yLk9ORV9TSU5HTEU6XG4gICAgICBjYXNlIERvb3IuVFdPX1RSSVBMRTpcbiAgICAgIGNhc2UgRG9vci5USFJFRV9UUklQTEU6XG4gICAgICBjYXNlIERvb3IuRk9VUl9UUklQTEU6XG4gICAgICBjYXNlIERvb3IuRklWRV9UUklQTEU6XG4gICAgICBjYXNlIERvb3IuU0lYX1RSSVBMRTpcbiAgICAgIGNhc2UgRG9vci5PTkVfU0lOR0xFOlxuICAgICAgY2FzZSBEb29yLlRXT19TSU5HTEU6XG4gICAgICBjYXNlIERvb3IuVEhSRUVfU0lOR0xFOlxuICAgICAgY2FzZSBEb29yLkZPVVJfU0lOR0xFOlxuICAgICAgY2FzZSBEb29yLkZJVkVfU0lOR0xFOlxuICAgICAgY2FzZSBEb29yLlNJWF9TSU5HTEU6XG4gICAgICAgIHByZWZhYiA9IHRoaXMuc2xvdFdpbk1vbmV5TGFyZ2VQcmVmYWI7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIGxldCBjb21wID0gU2ljYm9HYW1lVXRpbHMuY3JlYXRlSXRlbUZyb21QcmVmYWIoU2ljYm9TbG90V2luTW9uZXlDb21wLCBwcmVmYWIsIGQubm9kZSk7XG4gICAgY29tcC5zaG93KG1vbmV5LCBlbGFwc2VUaW1lKTtcbiAgICB0aGlzLndpbk1vbmV5QXRTbG90cy5wdXNoKGNvbXApO1xuICB9XG5cbiAgZGVzdHJveVdpbk1vbmV5QXREb29yKCkge1xuICAgIHRoaXMud2luTW9uZXlBdFNsb3RzLmZvckVhY2goKGVsZW1lbnQpID0+IHtcbiAgICAgIGVsZW1lbnQubm9kZT8uZGVzdHJveSgpO1xuICAgIH0pO1xuICAgIHRoaXMud2luTW9uZXlBdFNsb3RzLmxlbmd0aCA9IDA7XG4gIH1cbiAgLy8hU0VDVElPTlxuXG4gIHVwZGF0ZVJlc3VsdEZpc3RUaW1lKHJlY29yZHM6IEluc3RhbmNlVHlwZTx0eXBlb2YgR2V0Q2hhbm5lbEhpc3RvcnlSZXBseS5SZWNvcmQ+W10pIHtcbiAgICBpZiAoU2ljYm9HYW1lVXRpbHMuaXNOdWxsT3JVbmRlZmluZWQocmVjb3JkcykpIHtcbiAgICAgIHRoaXMubGF0ZXN0UmVzdWx0Q29tcC5hY3RpdmUgPSBmYWxzZTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBsZXQgbGFzdFJlY29yZCA9IHJlY29yZHNbMF07XG4gICAgLy8gaWYodGhpcy51cGRhdGVMYXN0UmVzdWx0ID09IGZhbHNlKVxuICAgIC8vICAgbGFzdFJlY29yZCA9IHJlY29yZHNbMV07XG5cbiAgICBpZiAoU2ljYm9HYW1lVXRpbHMuaXNOdWxsT3JVbmRlZmluZWQobGFzdFJlY29yZCkpIHtcbiAgICAgIHRoaXMubGF0ZXN0UmVzdWx0Q29tcC5hY3RpdmUgPSBmYWxzZTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLmxhdGVzdFJlc3VsdENvbXAuYWN0aXZlID0gdHJ1ZTtcbiAgICBsZXQgZGF0YSA9IENoYW5uZWxIaXN0b3J5TWV0YWRhdGEuZGVzZXJpYWxpemVCaW5hcnkobGFzdFJlY29yZC5nZXRNZXRhZGF0YV9hc1U4KCkpO1xuICAgIGxldCBpdGVtcyA9IGRhdGEuZ2V0SXRlbXNMaXN0KCkuc29ydCgpO1xuICAgIHRoaXMubGF0ZXN0UmVzdWx0Q29tcC5zZXREYXRhKGl0ZW1zLCBmYWxzZSk7XG4gIH1cblxuICBvbkRlc3Ryb3koKSB7XG4gICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMubm9kZSk7XG4gICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMud2luRnhTaGlubnlOb2RlKTtcbiAgfVxuXG4gIG9uSGlkZSgpOiB2b2lkIHtcbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2Uuc3RvcFNmeChTaWNib1NvdW5kLlNmeERlYWxlclNoYWtlKTtcbiAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5ub2RlKTtcbiAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy53aW5GeFNoaW5ueU5vZGUpO1xuICAgIC8vIHRoaXMuY2hlbkNsb3NlKDEwMCk7XG4gICAgdGhpcy5oaWRlV2luRngoKTtcbiAgICB0aGlzLmRlc3Ryb3lXaW5Nb25leUF0RG9vcigpO1xuICAgIHRoaXMuaXNQYXlpbmdQbGF5aW5nID0gZmFsc2U7XG4gIH1cbn1cbiJdfQ==