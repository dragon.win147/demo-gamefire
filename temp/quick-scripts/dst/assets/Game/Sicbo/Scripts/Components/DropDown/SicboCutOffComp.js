
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/DropDown/SicboCutOffComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '855e9XAa2JFDoMqAmzezc5V', 'SicboCutOffComp');
// Game/Sicbo_New/Scripts/Components/DropDown/SicboCutOffComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboCutOffComp = /** @class */ (function (_super) {
    __extends(SicboCutOffComp, _super);
    function SicboCutOffComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.dropDown = null;
        _this.day = null;
        _this.week = null;
        _this.month = null;
        _this.lifeTime = null;
        return _this;
    }
    __decorate([
        property({ type: cc.Node })
    ], SicboCutOffComp.prototype, "dropDown", void 0);
    __decorate([
        property({ type: cc.SpriteFrame })
    ], SicboCutOffComp.prototype, "day", void 0);
    __decorate([
        property({ type: cc.SpriteFrame })
    ], SicboCutOffComp.prototype, "week", void 0);
    __decorate([
        property({ type: cc.SpriteFrame })
    ], SicboCutOffComp.prototype, "month", void 0);
    __decorate([
        property({ type: cc.SpriteFrame })
    ], SicboCutOffComp.prototype, "lifeTime", void 0);
    SicboCutOffComp = __decorate([
        ccclass
    ], SicboCutOffComp);
    return SicboCutOffComp;
}(cc.Component));
exports.default = SicboCutOffComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvRHJvcERvd24vU2ljYm9DdXRPZmZDb21wLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFNLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQTZDLG1DQUFZO0lBQXpEO1FBQUEscUVBV0M7UUFURyxjQUFRLEdBQVksSUFBSSxDQUFBO1FBRXhCLFNBQUcsR0FBbUIsSUFBSSxDQUFBO1FBRTFCLFVBQUksR0FBbUIsSUFBSSxDQUFBO1FBRTNCLFdBQUssR0FBbUIsSUFBSSxDQUFBO1FBRTVCLGNBQVEsR0FBbUIsSUFBSSxDQUFBOztJQUNuQyxDQUFDO0lBVEc7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDO3FEQUNKO0lBRXhCO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnREFDVDtJQUUxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7aURBQ1I7SUFFM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO2tEQUNQO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztxREFDSjtJQVZkLGVBQWU7UUFEbkMsT0FBTztPQUNhLGVBQWUsQ0FXbkM7SUFBRCxzQkFBQztDQVhELEFBV0MsQ0FYNEMsRUFBRSxDQUFDLFNBQVMsR0FXeEQ7a0JBWG9CLGVBQWUiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvQ3V0T2ZmQ29tcCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuTm9kZSB9KVxuICAgIGRyb3BEb3duOiBjYy5Ob2RlID0gbnVsbFxuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLlNwcml0ZUZyYW1lIH0pXG4gICAgZGF5OiBjYy5TcHJpdGVGcmFtZSA9IG51bGxcbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5TcHJpdGVGcmFtZSB9KVxuICAgIHdlZWs6IGNjLlNwcml0ZUZyYW1lID0gbnVsbFxuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLlNwcml0ZUZyYW1lIH0pXG4gICAgbW9udGg6IGNjLlNwcml0ZUZyYW1lID0gbnVsbFxuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLlNwcml0ZUZyYW1lIH0pXG4gICAgbGlmZVRpbWU6IGNjLlNwcml0ZUZyYW1lID0gbnVsbFxufVxuIl19