
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/Items/SicboBetSlotItemComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4b3e3sv9j1M85/v8+NJ1Mth', 'SicboBetSlotItemComp');
// Game/Sicbo_New/Scripts/Components/Items/SicboBetSlotItemComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var Door = SicboModuleAdapter_1.default.getAllRefs().Door;
var SicboSetting_1 = require("../../Setting/SicboSetting");
var SicboHelper_1 = require("../../Helpers/SicboHelper");
var SicboMoneyFormatComp_1 = require("../../RNGCommons/SicboMoneyFormatComp");
var SicboBetComp_1 = require("../SicboBetComp");
var SicboCoinItemComp_1 = require("./SicboCoinItemComp");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboBetSlotItemComp = /** @class */ (function (_super) {
    __extends(SicboBetSlotItemComp, _super);
    function SicboBetSlotItemComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.door = Door.ONE_SINGLE;
        _this.sicboBetComp = null;
        _this.myBetBgNode = null;
        _this.myBetMoneyComp = null;
        _this.totalBetBgNode = null;
        _this.totalBetMoneyComp = null;
        _this.miniChipArea = null;
        _this.stackChipRoot = null;
        _this.maxMiniChipOnSlot = 12 + 10 + 5;
        _this.chipColCount = 0;
        _this.lastMyBetAmount = 0;
        _this.lastTotalBetAmount = 0;
        _this._miniChipsOnSlot = [];
        return _this;
    }
    Object.defineProperty(SicboBetSlotItemComp.prototype, "miniChipsOnSlot", {
        get: function () {
            //FIXME kiem tra lai loi bi sai _miniChipsOnSlot
            this._miniChipsOnSlot = this.miniChipArea.getComponentsInChildren(SicboCoinItemComp_1.default);
            return this._miniChipsOnSlot;
        },
        enumerable: false,
        configurable: true
    });
    // public set miniChipsOnSlot(value: SicboCoinItemComp[]) {
    //     this._miniChipsOnSlot = value;
    // }
    SicboBetSlotItemComp.prototype.addMiniChipToSlot = function (chip) {
        this._miniChipsOnSlot.push(chip);
    };
    SicboBetSlotItemComp.prototype.getRedundancyChip = function (isStaticSlot, mergeFrom) {
        var _a;
        if (isStaticSlot) {
            var totalValue = 0;
            var chips_1 = this.miniChipsOnSlot;
            for (var i = mergeFrom; i < chips_1.length; i++) {
                totalValue += SicboHelper_1.default.convertCoinTypeToValue((_a = chips_1[i].getComponent(SicboCoinItemComp_1.default)) === null || _a === void 0 ? void 0 : _a.data.coinType);
            }
            var newChips = SicboHelper_1.default.calculateNumOfChipToMakeValue(totalValue, SicboSetting_1.default.getChipLevelList());
            newChips.forEach(function (num, val) {
                // //console.log("val: " + val + " -> " + num);
                var coinType = SicboHelper_1.default.convertValueToCoinType(val);
                for (var i = 0; i < num; i++) {
                    var itemData = new SicboCoinItemComp_1.SicboCoinItemCompData();
                    itemData.coinType = coinType;
                    itemData.isMini = true;
                    chips_1[mergeFrom].getComponent(SicboCoinItemComp_1.default).setData(itemData);
                    mergeFrom++;
                }
            });
            return this._miniChipsOnSlot.splice(mergeFrom + newChips.values.length);
        }
        else {
            return this._miniChipsOnSlot.splice(0, this._miniChipsOnSlot.length - this.maxMiniChipOnSlot);
        }
    };
    SicboBetSlotItemComp.prototype.clearMiniChipOnSlot = function () {
        this._miniChipsOnSlot.length = 0;
    };
    SicboBetSlotItemComp.prototype.setStackChipRoot = function (stackChipRoot) {
        this.stackChipRoot = stackChipRoot;
    };
    SicboBetSlotItemComp.prototype.deleteStackChipRoot = function () {
        if (this.stackChipRoot != null)
            this.stackChipRoot.destroy();
        this.stackChipRoot = null;
    };
    Object.defineProperty(SicboBetSlotItemComp.prototype, "miniChipsOnStack", {
        get: function () {
            if (this.stackChipRoot != null)
                return this.stackChipRoot.getComponentsInChildren(SicboCoinItemComp_1.default);
            return [];
        },
        enumerable: false,
        configurable: true
    });
    SicboBetSlotItemComp.prototype.onLoad = function () {
        this.registerClickEvent();
        this.resetSlot();
    };
    SicboBetSlotItemComp.prototype.resetSlot = function () {
        this.setTotalBet(0);
        this.setMyTotalBet(0);
    };
    SicboBetSlotItemComp.prototype.setMyTotalBet = function (value) {
        if (value > 0) {
            this.myBetBgNode.active = true;
            this.myBetBgNode.width = this.myBetMoneyComp.node.width + 20 * .8; //NOTE layout not work 
            this.myBetMoneyComp.node.active = true;
            this.myBetMoneyComp.setMoney(value);
            if (this.lastMyBetAmount != value) {
                this.playBetAmountEffect(this.myBetMoneyComp.node);
            }
        }
        else {
            this.myBetBgNode.active = false;
            this.myBetMoneyComp.node.active = false;
        }
        this.lastMyBetAmount = value;
    };
    SicboBetSlotItemComp.prototype.setTotalBet = function (value) {
        if (value > 0) {
            this.totalBetMoneyComp.node.active = true;
            this.totalBetBgNode.active = true;
            this.totalBetMoneyComp.setMoney(value);
            if (this.lastTotalBetAmount != value) {
                this.playBetAmountEffect(this.totalBetMoneyComp.node);
            }
        }
        else {
            this.totalBetBgNode.active = false;
            this.totalBetMoneyComp.node.active = false;
        }
        this.lastTotalBetAmount = value;
    };
    SicboBetSlotItemComp.prototype.playBetAmountEffect = function (node) {
        node.scale = 2;
        node.opacity = 0;
        cc.tween(node)
            .to(.2, {
            scale: 1,
            opacity: 255
        })
            .start();
    };
    SicboBetSlotItemComp.prototype.registerClickEvent = function () {
        var button = this.getComponent(cc.Button);
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        clickEventHandler.component = "SicboBetSlotItemComp"; // This is the code file name
        clickEventHandler.handler = "onClickBetSlotItem";
        button.clickEvents.push(clickEventHandler);
    };
    SicboBetSlotItemComp.prototype.onClickBetSlotItem = function () {
        this.sicboBetComp.onClickBetSlotItem(this);
    };
    SicboBetSlotItemComp.prototype.onDestroy = function () {
        if (this.stackChipRoot != null)
            cc.Tween.stopAllByTarget(this.stackChipRoot);
        if (this.myBetMoneyComp.node != null)
            cc.Tween.stopAllByTarget(this.myBetMoneyComp.node);
        if (this.totalBetMoneyComp.node != null)
            cc.Tween.stopAllByTarget(this.totalBetMoneyComp.node);
    };
    __decorate([
        property({ type: cc.Enum(Door) })
    ], SicboBetSlotItemComp.prototype, "door", void 0);
    __decorate([
        property(SicboBetComp_1.default)
    ], SicboBetSlotItemComp.prototype, "sicboBetComp", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetSlotItemComp.prototype, "myBetBgNode", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboBetSlotItemComp.prototype, "myBetMoneyComp", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetSlotItemComp.prototype, "totalBetBgNode", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboBetSlotItemComp.prototype, "totalBetMoneyComp", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetSlotItemComp.prototype, "miniChipArea", void 0);
    SicboBetSlotItemComp = __decorate([
        ccclass
    ], SicboBetSlotItemComp);
    return SicboBetSlotItemComp;
}(cc.Component));
exports.default = SicboBetSlotItemComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvSXRlbXMvU2ljYm9CZXRTbG90SXRlbUNvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYsd0VBQW1FO0FBR2pFLElBQUEsSUFBSSxHQUFJLDRCQUFrQixDQUFDLFVBQVUsRUFBRSxLQUFuQyxDQUFvQztBQUMxQywyREFBc0Q7QUFDdEQseURBQW9EO0FBQ3BELDhFQUF5RTtBQUN6RSxnREFBMkM7QUFDM0MseURBQStFO0FBRXpFLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQWtELHdDQUFZO0lBQTlEO1FBQUEscUVBcUtDO1FBbEtHLFVBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBR3ZCLGtCQUFZLEdBQWlCLElBQUksQ0FBQztRQUdsQyxpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixvQkFBYyxHQUF5QixJQUFJLENBQUM7UUFHNUMsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsdUJBQWlCLEdBQXlCLElBQUksQ0FBQztRQUcvQyxrQkFBWSxHQUFZLElBQUksQ0FBQztRQUVyQixtQkFBYSxHQUFZLElBQUksQ0FBQztRQUN0Qyx1QkFBaUIsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNoQyxrQkFBWSxHQUFHLENBQUMsQ0FBQztRQUVqQixxQkFBZSxHQUFHLENBQUMsQ0FBQztRQUNwQix3QkFBa0IsR0FBRyxDQUFDLENBQUM7UUFFZixzQkFBZ0IsR0FBd0IsRUFBRSxDQUFDOztJQXVJdkQsQ0FBQztJQXJJRyxzQkFBVyxpREFBZTthQUExQjtZQUNJLGdEQUFnRDtZQUNoRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQywyQkFBaUIsQ0FBQyxDQUFDO1lBQ3JGLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQ2pDLENBQUM7OztPQUFBO0lBRUQsMkRBQTJEO0lBQzNELHFDQUFxQztJQUNyQyxJQUFJO0lBRUcsZ0RBQWlCLEdBQXhCLFVBQXlCLElBQXVCO1FBQzVDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVNLGdEQUFpQixHQUF4QixVQUF5QixZQUFxQixFQUFFLFNBQWlCOztRQUM3RCxJQUFJLFlBQVksRUFBRTtZQUNkLElBQUksVUFBVSxHQUFHLENBQUMsQ0FBQztZQUNuQixJQUFJLE9BQUssR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQ2pDLEtBQUssSUFBSSxDQUFDLEdBQUcsU0FBUyxFQUFFLENBQUMsR0FBRyxPQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUMzQyxVQUFVLElBQUkscUJBQVcsQ0FBQyxzQkFBc0IsT0FBQyxPQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLDJCQUFpQixDQUFDLDBDQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUM3RztZQUNELElBQUksUUFBUSxHQUFHLHFCQUFXLENBQUMsNkJBQTZCLENBQUMsVUFBVSxFQUFFLHNCQUFZLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO1lBRXRHLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFXLEVBQUUsR0FBVztnQkFDdEMsK0NBQStDO2dCQUMvQyxJQUFJLFFBQVEsR0FBRyxxQkFBVyxDQUFDLHNCQUFzQixDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN2RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUMxQixJQUFJLFFBQVEsR0FBRyxJQUFJLHlDQUFxQixFQUFFLENBQUM7b0JBQzNDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO29CQUM3QixRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztvQkFDdkIsT0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQywyQkFBaUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDbkUsU0FBUyxFQUFFLENBQUM7aUJBQ2Y7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMzRTthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQ2pHO0lBQ0wsQ0FBQztJQUVNLGtEQUFtQixHQUExQjtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFTSwrQ0FBZ0IsR0FBdkIsVUFBd0IsYUFBc0I7UUFDMUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7SUFDdkMsQ0FBQztJQUVNLGtEQUFtQixHQUExQjtRQUNJLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJO1lBQzFCLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7SUFDOUIsQ0FBQztJQUVELHNCQUFXLGtEQUFnQjthQUEzQjtZQUNJLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJO2dCQUMxQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsdUJBQXVCLENBQUMsMkJBQWlCLENBQUMsQ0FBQztZQUN6RSxPQUFPLEVBQUUsQ0FBQztRQUNkLENBQUM7OztPQUFBO0lBR0QscUNBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsd0NBQVMsR0FBVDtRQUNJLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRUQsNENBQWEsR0FBYixVQUFjLEtBQWE7UUFDdkIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ1gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUEsdUJBQXVCO1lBRXpGLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDdkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEMsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLEtBQUssRUFBRTtnQkFDL0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdEQ7U0FDSjthQUFNO1lBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDM0M7UUFDRCxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUNqQyxDQUFDO0lBRUQsMENBQVcsR0FBWCxVQUFZLEtBQWE7UUFDckIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ1gsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNsQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3ZDLElBQUksSUFBSSxDQUFDLGtCQUFrQixJQUFJLEtBQUssRUFBRTtnQkFDbEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN6RDtTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDbkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQzlDO1FBQ0QsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztJQUNwQyxDQUFDO0lBRUQsa0RBQW1CLEdBQW5CLFVBQW9CLElBQWE7UUFDN0IsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZixJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUNqQixFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQzthQUNULEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDSixLQUFLLEVBQUUsQ0FBQztZQUNSLE9BQU8sRUFBRSxHQUFHO1NBQ2YsQ0FBQzthQUNELEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFFTyxpREFBa0IsR0FBMUI7UUFDSSxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxQyxJQUFJLGlCQUFpQixHQUFHLElBQUksRUFBRSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUN4RCxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLDJFQUEyRTtRQUNqSCxpQkFBaUIsQ0FBQyxTQUFTLEdBQUcsc0JBQXNCLENBQUMsQ0FBQSw2QkFBNkI7UUFDbEYsaUJBQWlCLENBQUMsT0FBTyxHQUFHLG9CQUFvQixDQUFDO1FBQ2pELE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELGlEQUFrQixHQUFsQjtRQUNJLElBQUksQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELHdDQUFTLEdBQVQ7UUFDSSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSTtZQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM3RSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLElBQUk7WUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pGLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksSUFBSSxJQUFJO1lBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ25HLENBQUM7SUFoS0Q7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO3NEQUNYO0lBR3ZCO1FBREMsUUFBUSxDQUFDLHNCQUFZLENBQUM7OERBQ1c7SUFHbEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs2REFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyw4QkFBb0IsQ0FBQztnRUFDYTtJQUc1QztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dFQUNhO0lBRy9CO1FBREMsUUFBUSxDQUFDLDhCQUFvQixDQUFDO21FQUNnQjtJQUcvQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzhEQUNXO0lBckJaLG9CQUFvQjtRQUR4QyxPQUFPO09BQ2Esb0JBQW9CLENBcUt4QztJQUFELDJCQUFDO0NBcktELEFBcUtDLENBcktpRCxFQUFFLENBQUMsU0FBUyxHQXFLN0Q7a0JBcktvQixvQkFBb0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFNpY2JvTW9kdWxlQWRhcHRlciBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vU2ljYm9Nb2R1bGVBZGFwdGVyXCI7XG5cbmNvbnN0IHtcbiAgRG9vcn0gPSBTaWNib01vZHVsZUFkYXB0ZXIuZ2V0QWxsUmVmcygpO1xuaW1wb3J0IFNpY2JvU2V0dGluZyBmcm9tIFwiLi4vLi4vU2V0dGluZy9TaWNib1NldHRpbmdcIjtcbmltcG9ydCBTaWNib0hlbHBlciBmcm9tIFwiLi4vLi4vSGVscGVycy9TaWNib0hlbHBlclwiO1xuaW1wb3J0IFNpY2JvTW9uZXlGb3JtYXRDb21wIGZyb20gXCIuLi8uLi9STkdDb21tb25zL1NpY2JvTW9uZXlGb3JtYXRDb21wXCI7XG5pbXBvcnQgU2ljYm9CZXRDb21wIGZyb20gXCIuLi9TaWNib0JldENvbXBcIjtcbmltcG9ydCBTaWNib0NvaW5JdGVtQ29tcCwgeyBTaWNib0NvaW5JdGVtQ29tcERhdGEgfSBmcm9tIFwiLi9TaWNib0NvaW5JdGVtQ29tcFwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9CZXRTbG90SXRlbUNvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuRW51bShEb29yKSB9KVxuICAgIGRvb3IgPSBEb29yLk9ORV9TSU5HTEU7XG5cbiAgICBAcHJvcGVydHkoU2ljYm9CZXRDb21wKVxuICAgIHNpY2JvQmV0Q29tcDogU2ljYm9CZXRDb21wID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIG15QmV0QmdOb2RlOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShTaWNib01vbmV5Rm9ybWF0Q29tcClcbiAgICBteUJldE1vbmV5Q29tcDogU2ljYm9Nb25leUZvcm1hdENvbXAgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgdG90YWxCZXRCZ05vZGU6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KFNpY2JvTW9uZXlGb3JtYXRDb21wKVxuICAgIHRvdGFsQmV0TW9uZXlDb21wOiBTaWNib01vbmV5Rm9ybWF0Q29tcCA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBtaW5pQ2hpcEFyZWE6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgcHJpdmF0ZSBzdGFja0NoaXBSb290OiBjYy5Ob2RlID0gbnVsbDtcbiAgICBtYXhNaW5pQ2hpcE9uU2xvdCA9IDEyICsgMTAgKyA1O1xuICAgIGNoaXBDb2xDb3VudCA9IDA7XG5cbiAgICBsYXN0TXlCZXRBbW91bnQgPSAwO1xuICAgIGxhc3RUb3RhbEJldEFtb3VudCA9IDA7XG5cbiAgICBwcml2YXRlIF9taW5pQ2hpcHNPblNsb3Q6IFNpY2JvQ29pbkl0ZW1Db21wW10gPSBbXTtcblxuICAgIHB1YmxpYyBnZXQgbWluaUNoaXBzT25TbG90KCk6IFNpY2JvQ29pbkl0ZW1Db21wW10ge1xuICAgICAgICAvL0ZJWE1FIGtpZW0gdHJhIGxhaSBsb2kgYmkgc2FpIF9taW5pQ2hpcHNPblNsb3RcbiAgICAgICAgdGhpcy5fbWluaUNoaXBzT25TbG90ID0gdGhpcy5taW5pQ2hpcEFyZWEuZ2V0Q29tcG9uZW50c0luQ2hpbGRyZW4oU2ljYm9Db2luSXRlbUNvbXApO1xuICAgICAgICByZXR1cm4gdGhpcy5fbWluaUNoaXBzT25TbG90O1xuICAgIH1cblxuICAgIC8vIHB1YmxpYyBzZXQgbWluaUNoaXBzT25TbG90KHZhbHVlOiBTaWNib0NvaW5JdGVtQ29tcFtdKSB7XG4gICAgLy8gICAgIHRoaXMuX21pbmlDaGlwc09uU2xvdCA9IHZhbHVlO1xuICAgIC8vIH1cblxuICAgIHB1YmxpYyBhZGRNaW5pQ2hpcFRvU2xvdChjaGlwOiBTaWNib0NvaW5JdGVtQ29tcCkge1xuICAgICAgICB0aGlzLl9taW5pQ2hpcHNPblNsb3QucHVzaChjaGlwKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0UmVkdW5kYW5jeUNoaXAoaXNTdGF0aWNTbG90OiBib29sZWFuLCBtZXJnZUZyb206IG51bWJlcik6IFNpY2JvQ29pbkl0ZW1Db21wW10ge1xuICAgICAgICBpZiAoaXNTdGF0aWNTbG90KSB7XG4gICAgICAgICAgICBsZXQgdG90YWxWYWx1ZSA9IDA7XG4gICAgICAgICAgICBsZXQgY2hpcHMgPSB0aGlzLm1pbmlDaGlwc09uU2xvdDtcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSBtZXJnZUZyb207IGkgPCBjaGlwcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIHRvdGFsVmFsdWUgKz0gU2ljYm9IZWxwZXIuY29udmVydENvaW5UeXBlVG9WYWx1ZShjaGlwc1tpXS5nZXRDb21wb25lbnQoU2ljYm9Db2luSXRlbUNvbXApPy5kYXRhLmNvaW5UeXBlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxldCBuZXdDaGlwcyA9IFNpY2JvSGVscGVyLmNhbGN1bGF0ZU51bU9mQ2hpcFRvTWFrZVZhbHVlKHRvdGFsVmFsdWUsIFNpY2JvU2V0dGluZy5nZXRDaGlwTGV2ZWxMaXN0KCkpO1xuXG4gICAgICAgICAgICBuZXdDaGlwcy5mb3JFYWNoKChudW06IG51bWJlciwgdmFsOiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgICAgICAvLyAvL2NvbnNvbGUubG9nKFwidmFsOiBcIiArIHZhbCArIFwiIC0+IFwiICsgbnVtKTtcbiAgICAgICAgICAgICAgICBsZXQgY29pblR5cGUgPSBTaWNib0hlbHBlci5jb252ZXJ0VmFsdWVUb0NvaW5UeXBlKHZhbCk7XG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBudW07IGkrKykge1xuICAgICAgICAgICAgICAgICAgICBsZXQgaXRlbURhdGEgPSBuZXcgU2ljYm9Db2luSXRlbUNvbXBEYXRhKCk7XG4gICAgICAgICAgICAgICAgICAgIGl0ZW1EYXRhLmNvaW5UeXBlID0gY29pblR5cGU7XG4gICAgICAgICAgICAgICAgICAgIGl0ZW1EYXRhLmlzTWluaSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIGNoaXBzW21lcmdlRnJvbV0uZ2V0Q29tcG9uZW50KFNpY2JvQ29pbkl0ZW1Db21wKS5zZXREYXRhKGl0ZW1EYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgbWVyZ2VGcm9tKys7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fbWluaUNoaXBzT25TbG90LnNwbGljZShtZXJnZUZyb20gKyBuZXdDaGlwcy52YWx1ZXMubGVuZ3RoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9taW5pQ2hpcHNPblNsb3Quc3BsaWNlKDAsIHRoaXMuX21pbmlDaGlwc09uU2xvdC5sZW5ndGggLSB0aGlzLm1heE1pbmlDaGlwT25TbG90KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBjbGVhck1pbmlDaGlwT25TbG90KCkge1xuICAgICAgICB0aGlzLl9taW5pQ2hpcHNPblNsb3QubGVuZ3RoID0gMDtcbiAgICB9XG5cbiAgICBwdWJsaWMgc2V0U3RhY2tDaGlwUm9vdChzdGFja0NoaXBSb290OiBjYy5Ob2RlKSB7XG4gICAgICAgIHRoaXMuc3RhY2tDaGlwUm9vdCA9IHN0YWNrQ2hpcFJvb3Q7XG4gICAgfVxuXG4gICAgcHVibGljIGRlbGV0ZVN0YWNrQ2hpcFJvb3QoKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YWNrQ2hpcFJvb3QgIT0gbnVsbClcbiAgICAgICAgICAgIHRoaXMuc3RhY2tDaGlwUm9vdC5kZXN0cm95KCk7XG4gICAgICAgIHRoaXMuc3RhY2tDaGlwUm9vdCA9IG51bGw7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCBtaW5pQ2hpcHNPblN0YWNrKCk6IFNpY2JvQ29pbkl0ZW1Db21wW10ge1xuICAgICAgICBpZiAodGhpcy5zdGFja0NoaXBSb290ICE9IG51bGwpXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zdGFja0NoaXBSb290LmdldENvbXBvbmVudHNJbkNoaWxkcmVuKFNpY2JvQ29pbkl0ZW1Db21wKTtcbiAgICAgICAgcmV0dXJuIFtdO1xuICAgIH1cblxuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICB0aGlzLnJlZ2lzdGVyQ2xpY2tFdmVudCgpO1xuICAgICAgICB0aGlzLnJlc2V0U2xvdCgpO1xuICAgIH1cblxuICAgIHJlc2V0U2xvdCgpIHtcbiAgICAgICAgdGhpcy5zZXRUb3RhbEJldCgwKTtcbiAgICAgICAgdGhpcy5zZXRNeVRvdGFsQmV0KDApO1xuICAgIH1cblxuICAgIHNldE15VG90YWxCZXQodmFsdWU6IG51bWJlcikge1xuICAgICAgICBpZiAodmFsdWUgPiAwKSB7XG4gICAgICAgICAgICB0aGlzLm15QmV0QmdOb2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLm15QmV0QmdOb2RlLndpZHRoID0gdGhpcy5teUJldE1vbmV5Q29tcC5ub2RlLndpZHRoICsgMjAgKiAuODsvL05PVEUgbGF5b3V0IG5vdCB3b3JrIFxuXG4gICAgICAgICAgICB0aGlzLm15QmV0TW9uZXlDb21wLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMubXlCZXRNb25leUNvbXAuc2V0TW9uZXkodmFsdWUpO1xuICAgICAgICAgICAgaWYgKHRoaXMubGFzdE15QmV0QW1vdW50ICE9IHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5wbGF5QmV0QW1vdW50RWZmZWN0KHRoaXMubXlCZXRNb25leUNvbXAubm9kZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLm15QmV0QmdOb2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5teUJldE1vbmV5Q29tcC5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubGFzdE15QmV0QW1vdW50ID0gdmFsdWU7XG4gICAgfVxuXG4gICAgc2V0VG90YWxCZXQodmFsdWU6IG51bWJlcikge1xuICAgICAgICBpZiAodmFsdWUgPiAwKSB7XG4gICAgICAgICAgICB0aGlzLnRvdGFsQmV0TW9uZXlDb21wLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMudG90YWxCZXRCZ05vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMudG90YWxCZXRNb25leUNvbXAuc2V0TW9uZXkodmFsdWUpO1xuICAgICAgICAgICAgaWYgKHRoaXMubGFzdFRvdGFsQmV0QW1vdW50ICE9IHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5wbGF5QmV0QW1vdW50RWZmZWN0KHRoaXMudG90YWxCZXRNb25leUNvbXAubm9kZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnRvdGFsQmV0QmdOb2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy50b3RhbEJldE1vbmV5Q29tcC5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubGFzdFRvdGFsQmV0QW1vdW50ID0gdmFsdWU7XG4gICAgfVxuXG4gICAgcGxheUJldEFtb3VudEVmZmVjdChub2RlOiBjYy5Ob2RlKSB7XG4gICAgICAgIG5vZGUuc2NhbGUgPSAyO1xuICAgICAgICBub2RlLm9wYWNpdHkgPSAwO1xuICAgICAgICBjYy50d2Vlbihub2RlKVxuICAgICAgICAgICAgLnRvKC4yLCB7XG4gICAgICAgICAgICAgICAgc2NhbGU6IDEsXG4gICAgICAgICAgICAgICAgb3BhY2l0eTogMjU1XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnN0YXJ0KCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSByZWdpc3RlckNsaWNrRXZlbnQoKSB7XG4gICAgICAgIGxldCBidXR0b24gPSB0aGlzLmdldENvbXBvbmVudChjYy5CdXR0b24pO1xuICAgICAgICBsZXQgY2xpY2tFdmVudEhhbmRsZXIgPSBuZXcgY2MuQ29tcG9uZW50LkV2ZW50SGFuZGxlcigpO1xuICAgICAgICBjbGlja0V2ZW50SGFuZGxlci50YXJnZXQgPSB0aGlzLm5vZGU7IC8vIFRoaXMgbm9kZSBpcyB0aGUgbm9kZSB0byB3aGljaCB5b3VyIGV2ZW50IGhhbmRsZXIgY29kZSBjb21wb25lbnQgYmVsb25nc1xuICAgICAgICBjbGlja0V2ZW50SGFuZGxlci5jb21wb25lbnQgPSBcIlNpY2JvQmV0U2xvdEl0ZW1Db21wXCI7Ly8gVGhpcyBpcyB0aGUgY29kZSBmaWxlIG5hbWVcbiAgICAgICAgY2xpY2tFdmVudEhhbmRsZXIuaGFuZGxlciA9IFwib25DbGlja0JldFNsb3RJdGVtXCI7XG4gICAgICAgIGJ1dHRvbi5jbGlja0V2ZW50cy5wdXNoKGNsaWNrRXZlbnRIYW5kbGVyKTtcbiAgICB9XG5cbiAgICBvbkNsaWNrQmV0U2xvdEl0ZW0oKSB7XG4gICAgICAgIHRoaXMuc2ljYm9CZXRDb21wLm9uQ2xpY2tCZXRTbG90SXRlbSh0aGlzKTtcbiAgICB9XG5cbiAgICBvbkRlc3Ryb3koKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YWNrQ2hpcFJvb3QgIT0gbnVsbCkgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMuc3RhY2tDaGlwUm9vdCk7XG4gICAgICAgIGlmICh0aGlzLm15QmV0TW9uZXlDb21wLm5vZGUgIT0gbnVsbCkgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMubXlCZXRNb25leUNvbXAubm9kZSk7XG4gICAgICAgIGlmICh0aGlzLnRvdGFsQmV0TW9uZXlDb21wLm5vZGUgIT0gbnVsbCkgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMudG90YWxCZXRNb25leUNvbXAubm9kZSk7XG4gICAgfVxuXG59XG4iXX0=