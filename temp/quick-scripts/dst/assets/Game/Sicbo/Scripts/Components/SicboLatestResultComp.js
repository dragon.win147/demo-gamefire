
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboLatestResultComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4402e/3cohJMZFPVEmTN6An', 'SicboLatestResultComp');
// Game/Sicbo/Scripts/Components/SicboLatestResultComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboHelper_1 = require("../../../Sicbo/Scripts/Helpers/SicboHelper");
var SicboItemComp_1 = require("./Items/SicboItemComp");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboLatestResultComp = /** @class */ (function (_super) {
    __extends(SicboLatestResultComp, _super);
    function SicboLatestResultComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sumLabel = null;
        _this.typeLabel = null;
        _this.items = [];
        _this.fxNode = null;
        return _this;
    }
    SicboLatestResultComp.prototype.onLoad = function () {
        this.playFx(false);
    };
    Object.defineProperty(SicboLatestResultComp.prototype, "active", {
        set: function (isActive) {
            this.items.forEach(function (item, index) {
                item.node.scale = isActive ? 1 : 0;
            });
            this.typeLabel.node.parent.active = isActive;
        },
        enumerable: false,
        configurable: true
    });
    SicboLatestResultComp.prototype.setData = function (items, isAnimated) {
        if (isAnimated === void 0) { isAnimated = true; }
        var self = this;
        items.forEach(function (item, index) {
            self.items[index].setData(items[index], isAnimated);
        });
        self.sumLabel.string = SicboHelper_1.default.getResultTotalValue(items).toString();
        var typeResult = SicboHelper_1.default.getResultType(items);
        self.typeLabel.string = SicboHelper_1.default.getResultTypeName(typeResult);
    };
    SicboLatestResultComp.prototype.onDestroy = function () {
        var self = this;
        self.items.forEach(function (item, index) {
            if (self.items[index].node)
                cc.Tween.stopAllByTarget(self.items[index].node.parent);
        });
    };
    SicboLatestResultComp.prototype.playFx = function (isPlay) {
        if (isPlay === void 0) { isPlay = true; }
        this.fxNode.active = isPlay;
    };
    __decorate([
        property(cc.Label)
    ], SicboLatestResultComp.prototype, "sumLabel", void 0);
    __decorate([
        property(cc.Label)
    ], SicboLatestResultComp.prototype, "typeLabel", void 0);
    __decorate([
        property([SicboItemComp_1.default])
    ], SicboLatestResultComp.prototype, "items", void 0);
    __decorate([
        property(cc.Node)
    ], SicboLatestResultComp.prototype, "fxNode", void 0);
    SicboLatestResultComp = __decorate([
        ccclass
    ], SicboLatestResultComp);
    return SicboLatestResultComp;
}(cc.Component));
exports.default = SicboLatestResultComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvL1NjcmlwdHMvQ29tcG9uZW50cy9TaWNib0xhdGVzdFJlc3VsdENvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMEVBQXFFO0FBQ3JFLHVEQUFrRDtBQUU1QyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUU1QztJQUFtRCx5Q0FBWTtJQUEvRDtRQUFBLHFFQTZDQztRQTNDQyxjQUFRLEdBQWEsSUFBSSxDQUFDO1FBRzFCLGVBQVMsR0FBYSxJQUFJLENBQUM7UUFHM0IsV0FBSyxHQUFvQixFQUFFLENBQUM7UUFHNUIsWUFBTSxHQUFZLElBQUksQ0FBQzs7SUFrQ3pCLENBQUM7SUFoQ0Msc0NBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVELHNCQUFJLHlDQUFNO2FBQVYsVUFBVyxRQUFpQjtZQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO2dCQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUM7UUFDL0MsQ0FBQzs7O09BQUE7SUFFRCx1Q0FBTyxHQUFQLFVBQVEsS0FBWSxFQUFFLFVBQTBCO1FBQTFCLDJCQUFBLEVBQUEsaUJBQTBCO1FBQzlDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSSxFQUFFLEtBQUs7WUFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcscUJBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUN6RSxJQUFJLFVBQVUsR0FBRyxxQkFBVyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxxQkFBVyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFRCx5Q0FBUyxHQUFUO1FBQ0UsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSSxFQUFFLEtBQUs7WUFDN0IsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUk7Z0JBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEYsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsc0NBQU0sR0FBTixVQUFPLE1BQXNCO1FBQXRCLHVCQUFBLEVBQUEsYUFBc0I7UUFDM0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0lBQzlCLENBQUM7SUExQ0Q7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzsyREFDTztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzREQUNRO0lBRzNCO1FBREMsUUFBUSxDQUFDLENBQUMsdUJBQWEsQ0FBQyxDQUFDO3dEQUNFO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7eURBQ0s7SUFYSixxQkFBcUI7UUFEekMsT0FBTztPQUNhLHFCQUFxQixDQTZDekM7SUFBRCw0QkFBQztDQTdDRCxBQTZDQyxDQTdDa0QsRUFBRSxDQUFDLFNBQVMsR0E2QzlEO2tCQTdDb0IscUJBQXFCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFNpY2JvSGVscGVyIGZyb20gXCIuLi8uLi8uLi9TaWNiby9TY3JpcHRzL0hlbHBlcnMvU2ljYm9IZWxwZXJcIjtcbmltcG9ydCBTaWNib0l0ZW1Db21wIGZyb20gXCIuL0l0ZW1zL1NpY2JvSXRlbUNvbXBcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib0xhdGVzdFJlc3VsdENvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gIHN1bUxhYmVsOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICB0eXBlTGFiZWw6IGNjLkxhYmVsID0gbnVsbDtcblxuICBAcHJvcGVydHkoW1NpY2JvSXRlbUNvbXBdKVxuICBpdGVtczogU2ljYm9JdGVtQ29tcFtdID0gW107XG5cbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIGZ4Tm9kZTogY2MuTm9kZSA9IG51bGw7XG5cbiAgb25Mb2FkKCkge1xuICAgIHRoaXMucGxheUZ4KGZhbHNlKTtcbiAgfVxuXG4gIHNldCBhY3RpdmUoaXNBY3RpdmU6IGJvb2xlYW4pIHtcbiAgICB0aGlzLml0ZW1zLmZvckVhY2goKGl0ZW0sIGluZGV4KSA9PiB7XG4gICAgICBpdGVtLm5vZGUuc2NhbGUgPSBpc0FjdGl2ZSA/IDEgOiAwO1xuICAgIH0pO1xuICAgIHRoaXMudHlwZUxhYmVsLm5vZGUucGFyZW50LmFjdGl2ZSA9IGlzQWN0aXZlO1xuICB9XG5cbiAgc2V0RGF0YShpdGVtczogYW55W10sIGlzQW5pbWF0ZWQ6IGJvb2xlYW4gPSB0cnVlKSB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIGl0ZW1zLmZvckVhY2goKGl0ZW0sIGluZGV4KSA9PiB7XG4gICAgICBzZWxmLml0ZW1zW2luZGV4XS5zZXREYXRhKGl0ZW1zW2luZGV4XSwgaXNBbmltYXRlZCk7XG4gICAgfSk7XG5cbiAgICBzZWxmLnN1bUxhYmVsLnN0cmluZyA9IFNpY2JvSGVscGVyLmdldFJlc3VsdFRvdGFsVmFsdWUoaXRlbXMpLnRvU3RyaW5nKCk7XG4gICAgbGV0IHR5cGVSZXN1bHQgPSBTaWNib0hlbHBlci5nZXRSZXN1bHRUeXBlKGl0ZW1zKTtcbiAgICBzZWxmLnR5cGVMYWJlbC5zdHJpbmcgPSBTaWNib0hlbHBlci5nZXRSZXN1bHRUeXBlTmFtZSh0eXBlUmVzdWx0KTtcbiAgfVxuXG4gIG9uRGVzdHJveSgpIHtcbiAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgc2VsZi5pdGVtcy5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xuICAgICAgaWYgKHNlbGYuaXRlbXNbaW5kZXhdLm5vZGUpIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldChzZWxmLml0ZW1zW2luZGV4XS5ub2RlLnBhcmVudCk7XG4gICAgfSk7XG4gIH1cblxuICBwbGF5RngoaXNQbGF5OiBib29sZWFuID0gdHJ1ZSkge1xuICAgIHRoaXMuZnhOb2RlLmFjdGl2ZSA9IGlzUGxheTtcbiAgfVxufVxuIl19