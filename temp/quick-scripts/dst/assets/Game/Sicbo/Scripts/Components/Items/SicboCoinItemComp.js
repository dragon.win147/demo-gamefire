
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/Items/SicboCoinItemComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6f2b1K8pAlN6ZMMjufUGwfU', 'SicboCoinItemComp');
// Game/Sicbo_New/Scripts/Components/Items/SicboCoinItemComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboCoinItemCompData = void 0;
var SicboChipSpriteConfig_1 = require("../../Configs/SicboChipSpriteConfig");
var SicboHelper_1 = require("../../Helpers/SicboHelper");
var SicboCoinType_1 = require("../../RNGCommons/SicboCoinType");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboCoinItemCompData = /** @class */ (function () {
    function SicboCoinItemCompData() {
        this.coinType = SicboCoinType_1.SicboCoinType.Coin100;
        this.onClick = null;
        this.isMini = false;
    }
    return SicboCoinItemCompData;
}());
exports.SicboCoinItemCompData = SicboCoinItemCompData;
var SicboCoinItemComp = /** @class */ (function (_super) {
    __extends(SicboCoinItemComp, _super);
    function SicboCoinItemComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.icon = null;
        _this.iconDisable = null;
        _this.showEffect = null;
        _this.data = null;
        return _this;
    }
    //   set isOnTable(value: boolean) {    //
    //     this.node.setScale(value? new cc.Vec2(.25, .22): cc.Vec2.ONE);
    //   }
    SicboCoinItemComp.prototype.onLoad = function () {
        // var interval = 5;
        // // Time of repetition
        // var repeat = 4;
        // // Start delay
        // var delay = 2;
        // let self = this;
        // let isDisable = false;
        // self.isDisable = isDisable;
        // this.schedule(function() {
        //     // Here `this` is referring to the component
        //     isDisable = !isDisable;
        //     self.isDisable = isDisable;        
        //     //cc.log("DKM", isDisable);
        // }, interval, repeat, delay);
    };
    Object.defineProperty(SicboCoinItemComp.prototype, "isDisable", {
        set: function (disable) {
            this.icon.node.active = !disable;
            this.iconDisable.node.active = disable;
            if (disable) {
                this.showEffect.active = false;
                this.node.scale = 1;
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboCoinItemComp.prototype, "rotation", {
        set: function (value) {
            this.icon.node.angle = -value;
        },
        enumerable: false,
        configurable: true
    });
    SicboCoinItemComp.prototype.setData = function (data) {
        this.data = data;
        var spriteName = SicboHelper_1.default.convertCoinTypeToSpriteName(data.coinType, false, this.data.isMini);
        var spriteNameDisable = SicboHelper_1.default.convertCoinTypeToSpriteName(data.coinType, true);
        var self = this;
        var spriteFrame = null;
        if (self.data.isMini)
            spriteFrame = SicboChipSpriteConfig_1.default.Instance.getMiniChipSprite(spriteName);
        else
            spriteFrame = SicboChipSpriteConfig_1.default.Instance.getNormalChipSprite(spriteName);
        self.icon.spriteFrame = spriteFrame;
        if (this.data.isMini) {
            var button = this.getComponent(cc.Button);
            button.enabled = false;
            this.iconDisable.node.active = false;
        }
        else {
            self.iconDisable.spriteFrame = SicboChipSpriteConfig_1.default.Instance.getDisableChipSprite(spriteNameDisable);
            this.iconDisable.node.active = true;
        }
    };
    SicboCoinItemComp.prototype.onClickItem = function () {
        if (this.data.onClick) {
            this.data.onClick(this);
        }
    };
    SicboCoinItemComp.prototype.selectEff = function (isSelect) {
        cc.tween(this.node).to(.05, {
            // y: isSelect?25:0, 
            scale: isSelect ? 1.1 : 1
        })
            .start();
        this.showEffect.active = isSelect;
    };
    SicboCoinItemComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.Sprite)
    ], SicboCoinItemComp.prototype, "icon", void 0);
    __decorate([
        property(cc.Sprite)
    ], SicboCoinItemComp.prototype, "iconDisable", void 0);
    __decorate([
        property(cc.Node)
    ], SicboCoinItemComp.prototype, "showEffect", void 0);
    SicboCoinItemComp = __decorate([
        ccclass
    ], SicboCoinItemComp);
    return SicboCoinItemComp;
}(cc.Component));
exports.default = SicboCoinItemComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvSXRlbXMvU2ljYm9Db2luSXRlbUNvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWxGLDZFQUF3RTtBQUN4RSx5REFBb0Q7QUFDcEQsZ0VBQStEO0FBRXpELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQUE7UUFDVyxhQUFRLEdBQWtCLDZCQUFhLENBQUMsT0FBTyxDQUFDO1FBQ2hELFlBQU8sR0FBbUQsSUFBSSxDQUFDO1FBQy9ELFdBQU0sR0FBWSxLQUFLLENBQUM7SUFDbkMsQ0FBQztJQUFELDRCQUFDO0FBQUQsQ0FKQSxBQUlDLElBQUE7QUFKWSxzREFBcUI7QUFRbEM7SUFBK0MscUNBQVk7SUFBM0Q7UUFBQSxxRUF5RkM7UUF2RkMsVUFBSSxHQUFjLElBQUksQ0FBQztRQUd2QixpQkFBVyxHQUFjLElBQUksQ0FBQztRQUc5QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUUzQixVQUFJLEdBQTBCLElBQUksQ0FBQzs7SUErRXJDLENBQUM7SUE3RUQsMENBQTBDO0lBQzFDLHFFQUFxRTtJQUNyRSxNQUFNO0lBRUosa0NBQU0sR0FBTjtRQUNFLG9CQUFvQjtRQUNwQix3QkFBd0I7UUFDeEIsa0JBQWtCO1FBQ2xCLGlCQUFpQjtRQUNqQixpQkFBaUI7UUFDakIsbUJBQW1CO1FBQ25CLHlCQUF5QjtRQUN6Qiw4QkFBOEI7UUFDOUIsNkJBQTZCO1FBQzdCLG1EQUFtRDtRQUNuRCw4QkFBOEI7UUFDOUIsMENBQTBDO1FBQzFDLGtDQUFrQztRQUNsQywrQkFBK0I7SUFDakMsQ0FBQztJQUVELHNCQUFJLHdDQUFTO2FBQWIsVUFBYyxPQUFnQjtZQUM1QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxPQUFPLENBQUM7WUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztZQUN2QyxJQUFHLE9BQU8sRUFBRTtnQkFDUixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQy9CLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQzthQUN2QjtRQUNILENBQUM7OztPQUFBO0lBRUQsc0JBQUksdUNBQVE7YUFBWixVQUFhLEtBQWE7WUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsS0FBSyxDQUFDO1FBQ2hDLENBQUM7OztPQUFBO0lBRUMsbUNBQU8sR0FBUCxVQUFRLElBQTJCO1FBQy9CLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksVUFBVSxHQUFHLHFCQUFXLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNqRyxJQUFJLGlCQUFpQixHQUFHLHFCQUFXLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyRixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxXQUFXLEdBQW9CLElBQUksQ0FBQztRQUN4QyxJQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTTtZQUNmLFdBQVcsR0FBRywrQkFBcUIsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUM7O1lBRTNFLFdBQVcsR0FBRywrQkFBcUIsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDakYsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBR3BDLElBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUM7WUFDaEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDMUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUN4QzthQUFJO1lBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsK0JBQXFCLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDdEcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztTQUN2QztJQUNMLENBQUM7SUFHTSx1Q0FBVyxHQUFsQjtRQUNJLElBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUM7WUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDM0I7SUFDTCxDQUFDO0lBRUQscUNBQVMsR0FBVCxVQUFVLFFBQWdCO1FBQ3RCLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQ3RCO1lBQ0kscUJBQXFCO1lBQ3JCLEtBQUssRUFBRSxRQUFRLENBQUEsQ0FBQyxDQUFBLEdBQUcsQ0FBQSxDQUFDLENBQUEsQ0FBQztTQUN4QixDQUFDO2FBQ0QsS0FBSyxFQUFFLENBQUM7UUFDYixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUM7SUFDdEMsQ0FBQztJQUVELHFDQUFTLEdBQVQ7UUFDSSxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQXRGSDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO21EQUNHO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7MERBQ1U7SUFHOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt5REFDUztJQVJSLGlCQUFpQjtRQURyQyxPQUFPO09BQ2EsaUJBQWlCLENBeUZyQztJQUFELHdCQUFDO0NBekZELEFBeUZDLENBekY4QyxFQUFFLENBQUMsU0FBUyxHQXlGMUQ7a0JBekZvQixpQkFBaUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFNpY2JvQ2hpcFNwcml0ZUNvbmZpZyBmcm9tIFwiLi4vLi4vQ29uZmlncy9TaWNib0NoaXBTcHJpdGVDb25maWdcIjtcbmltcG9ydCBTaWNib0hlbHBlciBmcm9tIFwiLi4vLi4vSGVscGVycy9TaWNib0hlbHBlclwiO1xuaW1wb3J0IHsgU2ljYm9Db2luVHlwZSB9IGZyb20gXCIuLi8uLi9STkdDb21tb25zL1NpY2JvQ29pblR5cGVcIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cblxuZXhwb3J0IGNsYXNzIFNpY2JvQ29pbkl0ZW1Db21wRGF0YSB7XG4gICAgcHVibGljIGNvaW5UeXBlOiBTaWNib0NvaW5UeXBlID0gU2ljYm9Db2luVHlwZS5Db2luMTAwOyAgICBcbiAgICBwdWJsaWMgb25DbGljazogKHNpY2JvQ29pbkl0ZW1Db21wOiBTaWNib0NvaW5JdGVtQ29tcCkgPT4gdm9pZCA9IG51bGw7XG4gICAgcHVibGljIGlzTWluaTogYm9vbGVhbiA9IGZhbHNlO1xufVxuXG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib0NvaW5JdGVtQ29tcCBleHRlbmRzIGNjLkNvbXBvbmVudCB7ICAgIFxuICBAcHJvcGVydHkoY2MuU3ByaXRlKVxuICBpY29uOiBjYy5TcHJpdGUgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5TcHJpdGUpXG4gIGljb25EaXNhYmxlOiBjYy5TcHJpdGUgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBzaG93RWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcblxuICBkYXRhOiBTaWNib0NvaW5JdGVtQ29tcERhdGEgPSBudWxsO1xuXG4vLyAgIHNldCBpc09uVGFibGUodmFsdWU6IGJvb2xlYW4pIHsgICAgLy9cbi8vICAgICB0aGlzLm5vZGUuc2V0U2NhbGUodmFsdWU/IG5ldyBjYy5WZWMyKC4yNSwgLjIyKTogY2MuVmVjMi5PTkUpO1xuLy8gICB9XG5cbiAgb25Mb2FkKCl7XG4gICAgLy8gdmFyIGludGVydmFsID0gNTtcbiAgICAvLyAvLyBUaW1lIG9mIHJlcGV0aXRpb25cbiAgICAvLyB2YXIgcmVwZWF0ID0gNDtcbiAgICAvLyAvLyBTdGFydCBkZWxheVxuICAgIC8vIHZhciBkZWxheSA9IDI7XG4gICAgLy8gbGV0IHNlbGYgPSB0aGlzO1xuICAgIC8vIGxldCBpc0Rpc2FibGUgPSBmYWxzZTtcbiAgICAvLyBzZWxmLmlzRGlzYWJsZSA9IGlzRGlzYWJsZTtcbiAgICAvLyB0aGlzLnNjaGVkdWxlKGZ1bmN0aW9uKCkge1xuICAgIC8vICAgICAvLyBIZXJlIGB0aGlzYCBpcyByZWZlcnJpbmcgdG8gdGhlIGNvbXBvbmVudFxuICAgIC8vICAgICBpc0Rpc2FibGUgPSAhaXNEaXNhYmxlO1xuICAgIC8vICAgICBzZWxmLmlzRGlzYWJsZSA9IGlzRGlzYWJsZTsgICAgICAgIFxuICAgIC8vICAgICAvL2NjLmxvZyhcIkRLTVwiLCBpc0Rpc2FibGUpO1xuICAgIC8vIH0sIGludGVydmFsLCByZXBlYXQsIGRlbGF5KTtcbiAgfVxuXG4gIHNldCBpc0Rpc2FibGUoZGlzYWJsZTogYm9vbGVhbikgeyAgICBcbiAgICB0aGlzLmljb24ubm9kZS5hY3RpdmUgPSAhZGlzYWJsZTtcbiAgICB0aGlzLmljb25EaXNhYmxlLm5vZGUuYWN0aXZlID0gZGlzYWJsZTsgICAgXG4gICAgaWYoZGlzYWJsZSkge1xuICAgICAgICB0aGlzLnNob3dFZmZlY3QuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMubm9kZS5zY2FsZSA9IDE7XG4gICAgfVxuICB9XG5cbiAgc2V0IHJvdGF0aW9uKHZhbHVlOiBudW1iZXIpe1xuICAgIHRoaXMuaWNvbi5ub2RlLmFuZ2xlID0gLXZhbHVlO1xuICB9XG5cbiAgICBzZXREYXRhKGRhdGE6IFNpY2JvQ29pbkl0ZW1Db21wRGF0YSkge1xuICAgICAgICB0aGlzLmRhdGEgPSBkYXRhO1xuICAgICAgICBsZXQgc3ByaXRlTmFtZSA9IFNpY2JvSGVscGVyLmNvbnZlcnRDb2luVHlwZVRvU3ByaXRlTmFtZShkYXRhLmNvaW5UeXBlLCBmYWxzZSwgdGhpcy5kYXRhLmlzTWluaSk7XG4gICAgICAgIGxldCBzcHJpdGVOYW1lRGlzYWJsZSA9IFNpY2JvSGVscGVyLmNvbnZlcnRDb2luVHlwZVRvU3ByaXRlTmFtZShkYXRhLmNvaW5UeXBlLCB0cnVlKTtcbiAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgICBsZXQgc3ByaXRlRnJhbWUgOiBjYy5TcHJpdGVGcmFtZSA9IG51bGw7XG4gICAgICAgIGlmKHNlbGYuZGF0YS5pc01pbmkpXG4gICAgICAgICAgICBzcHJpdGVGcmFtZSA9IFNpY2JvQ2hpcFNwcml0ZUNvbmZpZy5JbnN0YW5jZS5nZXRNaW5pQ2hpcFNwcml0ZShzcHJpdGVOYW1lKTtcbiAgICAgICAgZWxzZVxuICAgICAgICAgICAgc3ByaXRlRnJhbWUgPSBTaWNib0NoaXBTcHJpdGVDb25maWcuSW5zdGFuY2UuZ2V0Tm9ybWFsQ2hpcFNwcml0ZShzcHJpdGVOYW1lKTtcbiAgICAgICAgc2VsZi5pY29uLnNwcml0ZUZyYW1lID0gc3ByaXRlRnJhbWU7XG5cblxuICAgICAgICBpZih0aGlzLmRhdGEuaXNNaW5pKXtcbiAgICAgICAgICAgIGxldCBidXR0b24gPSB0aGlzLmdldENvbXBvbmVudChjYy5CdXR0b24pO1xuICAgICAgICAgICAgYnV0dG9uLmVuYWJsZWQgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMuaWNvbkRpc2FibGUubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICBzZWxmLmljb25EaXNhYmxlLnNwcml0ZUZyYW1lID0gU2ljYm9DaGlwU3ByaXRlQ29uZmlnLkluc3RhbmNlLmdldERpc2FibGVDaGlwU3ByaXRlKHNwcml0ZU5hbWVEaXNhYmxlKTtcbiAgICAgICAgICAgIHRoaXMuaWNvbkRpc2FibGUubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB9XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgb25DbGlja0l0ZW0oKSB7ICAgICBcbiAgICAgICAgaWYodGhpcy5kYXRhLm9uQ2xpY2spe1xuICAgICAgICAgICAgdGhpcy5kYXRhLm9uQ2xpY2sodGhpcyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzZWxlY3RFZmYoaXNTZWxlY3Q6Ym9vbGVhbil7XG4gICAgICAgIGNjLnR3ZWVuKHRoaXMubm9kZSkudG8oLjA1LCBcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAvLyB5OiBpc1NlbGVjdD8yNTowLCBcbiAgICAgICAgICAgICAgICBzY2FsZTogaXNTZWxlY3Q/MS4xOjFcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuc3RhcnQoKTtcbiAgICAgICAgdGhpcy5zaG93RWZmZWN0LmFjdGl2ZSA9IGlzU2VsZWN0O1xuICAgIH1cblxuICAgIG9uRGVzdHJveSgpe1xuICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5ub2RlKTtcbiAgICB9XG59XG4iXX0=