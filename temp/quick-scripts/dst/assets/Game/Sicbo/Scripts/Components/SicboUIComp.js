
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboUIComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '27374cJEzNCAr43Nz1PRAJQ', 'SicboUIComp');
// Game/Sicbo_New/Scripts/Components/SicboUIComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Playah = _a.Playah, State = _a.State, Status = _a.Status, ListCurrentJackpotsReply = _a.ListCurrentJackpotsReply;
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboUserComp_1 = require("./SicboUserComp");
var SicboBetComp_1 = require("./SicboBetComp");
var SicboController_1 = require("../Controllers/SicboController");
var SicboNotifyComp_1 = require("./SicboNotifyComp");
var SicboHelper_1 = require("../Helpers/SicboHelper");
var SicboSessionInfoComp_1 = require("./SicboSessionInfoComp");
var SicboClockStateComp_1 = require("./States/SicboClockStateComp");
var SicboDealerStateComp_1 = require("./States/SicboDealerStateComp");
var SicboResultStateComp_1 = require("./States/SicboResultStateComp");
var SicboJackpotStateComp_1 = require("./States/SicboJackpotStateComp");
var SicboNotifyStateComp_1 = require("./States/SicboNotifyStateComp");
var SicboPortalAdapter_1 = require("../SicboPortalAdapter");
var SicboGameUtils_1 = require("../RNGCommons/Utils/SicboGameUtils");
var SicboText_1 = require("../Setting/SicboText");
var SicboMenuDetails_1 = require("./SicboMenuDetails");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboUIComp = /** @class */ (function (_super) {
    __extends(SicboUIComp, _super);
    function SicboUIComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //GameBetUIComp {
        //OPEN SCENE FADE OUT EFF
        _this.effectShow = null;
        //USER COMP
        _this.userComp = null;
        _this.chatButton = null;
        _this.shakeBetTarget = null;
        _this.userProfileBackground = null;
        //BET COMP
        _this.betCompNode = null;
        _this.betComp = null;
        _this.sessionComp = null;
        _this.menuDetails = null;
        //ANCHOR State UI
        _this.clock = null;
        _this.dealer = null;
        _this.result = null;
        _this.jackpot = null;
        _this.message = null;
        _this.notifyComp = null;
        _this.chatRoot = null;
        //!SECTION
        _this.stateStatus = Status.STATUS_UNSPECIFIED;
        return _this;
        //!SECTION
    }
    SicboUIComp_1 = SicboUIComp;
    Object.defineProperty(SicboUIComp, "Instance", {
        get: function () {
            return SicboUIComp_1.instance;
        },
        enumerable: false,
        configurable: true
    });
    SicboUIComp.prototype.onLoad = function () {
        SicboUIComp_1.instance = this;
        this.betComp = this.betCompNode.getComponent(SicboBetComp_1.default);
        this.onLoadJackpot();
        this.onSubscribeJackpot();
    };
    SicboUIComp.prototype.onDestroy = function () {
        this.onUnSubscribeJackpot();
        delete SicboUIComp_1.instance;
        SicboUIComp_1.instance = null;
    };
    SicboUIComp.prototype.update = function (dt) {
        // super.update(dt);
    };
    SicboUIComp.prototype.start = function () {
        this.effectShow.node.active = true;
        this.createChipItems(SicboSetting_1.default.getChipLevelList());
        this.showEffectLoadingGame();
        this.preloadInfoSpites();
    };
    SicboUIComp.prototype.preloadInfoSpites = function () {
        // let infoResources = SicboController.Instance.getGameInfo();
        // infoResources.forEach((info) => {
        //   SicboResourceManager.loadRemoteImageWithFileField(null, info);
        // });
    };
    //SECTION BET
    SicboUIComp.prototype.myUserBet = function (door, coinType) {
        var _this = this;
        if (this.stateStatus != Status.BETTING)
            return;
        var coinNumber = SicboHelper_1.default.convertCoinTypeToValue(coinType);
        SicboController_1.default.Instance.sendBet(door, coinType, function () {
            _this.playUserBet(SicboPortalAdapter_1.default.getInstance().getMyUserId(), door, coinNumber);
            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxBet);
        });
    };
    SicboUIComp.prototype.onClickReBet = function () {
        var _this = this;
        SicboController_1.default.Instance.sendReBet(function (dabs) {
            var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
            dabs.forEach(function (dab) {
                _this.playUserBet(myUserId, dab.getDoor(), dab.getBetAmount());
            });
            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxBet);
        });
    };
    SicboUIComp.prototype.onClickDoubleBet = function () {
        var _this = this;
        SicboController_1.default.Instance.sendBetDouble(function (dabs) {
            var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
            dabs.forEach(function (dab) {
                _this.playUserBet(myUserId, dab.getDoor(), dab.getBetAmount());
            });
            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxBet);
        });
    };
    //!SECTION
    SicboUIComp.prototype.setChatEnable = function (isEnable) {
        this.chatButton.node.active = isEnable;
    };
    //ANCHOR effect loading game
    SicboUIComp.prototype.showEffectLoadingGame = function () {
        var _this = this;
        cc.tween(this.effectShow.node)
            .delay(0.3)
            .to(1, {
            opacity: 0,
        })
            .call(function () { return (_this.effectShow.node.active = false); })
            .start();
    };
    //ANCHOR SicboBetComp
    SicboUIComp.prototype.createChipItems = function (chip_level) {
        this.betComp.createChipItems(chip_level);
    };
    //!SECTION
    //ANCHOR SicboUserComp
    SicboUIComp.prototype.findUserComp = function (userId) {
        var user = this.userComp.getUserInRoom(userId);
        if (SicboGameUtils_1.default.isNullOrUndefined(user)) {
            var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
            if (userId == myUserId)
                user = this.userComp.getMyUser();
            else
                user = this.userComp.getOtherUserGroup();
        }
        return user;
    };
    SicboUIComp.prototype.myUserComp = function () {
        return this.userComp.getMyUser();
    };
    SicboUIComp.prototype.updateUsersInRoom = function (displayPlayers, remainingPlayersCount) {
        this.userComp.updateUsersInRoom(displayPlayers, remainingPlayersCount);
    };
    SicboUIComp.prototype.playUserBet = function (userId, door, chip) {
        var userUI = this.userComp.findUser(userId);
        var doorComp = this.betComp.getDoor(door);
        this.userComp.playBetAnimation(userUI, this.shakeBetTarget);
        this.betComp.throwMiniChipOnTableByValue(chip, userUI.betShakeNode, doorComp);
    };
    SicboUIComp.prototype.changeMyUserBalance = function (newBalance) {
        this.userComp.getMyUser().setMoney(newBalance);
    };
    //ANCHOR State UI
    SicboUIComp.prototype.exitState = function (status) {
        this.betComp.exitState(status);
        this.clock.exitState(status);
        this.dealer.exitState(status);
        this.result.exitState(status);
        this.jackpot.exitState(status);
        this.message.exitState(status);
        this.sessionComp.exitState(status);
        this.menuDetails.exitState(status);
    };
    SicboUIComp.prototype.startState = function (state, stateDuration) {
        //cc.log("SICBO STATE", state.getStatus(), stateDuration);
        this.betComp.startState(state, stateDuration);
        this.clock.startState(state, stateDuration);
        this.dealer.startState(state, stateDuration);
        this.result.startState(state, stateDuration);
        this.jackpot.startState(state, stateDuration);
        this.message.startState(state, stateDuration);
        this.sessionComp.startState(state, stateDuration);
        this.menuDetails.startState(state, stateDuration);
        this.stateStatus = state.getStatus();
    };
    SicboUIComp.prototype.updateState = function (state) {
        this.betComp.updateState(state);
        this.clock.updateState(state);
        this.dealer.updateState(state);
        this.result.updateState(state);
        this.jackpot.updateState(state);
        this.message.updateState(state);
        this.sessionComp.updateState(state);
        this.menuDetails.updateState(state);
    };
    SicboUIComp.prototype.onHide = function () {
        this.betComp.onHide();
        this.clock.onHide();
        this.dealer.onHide();
        this.result.onHide();
        this.jackpot.onHide();
        this.message.onHide();
        this.sessionComp.onHide();
        this.menuDetails.onHide();
    };
    SicboUIComp.prototype.onLoadJackpot = function () {
        // SicboPortalAdapter.getPotterHandler().getListCurrentJackpotsRequest((res: InstanceType<typeof ListCurrentJackpotsReply>) => {
        //   this.jackpot.handleResponseJackpot(res);
        // });
    };
    SicboUIComp.prototype.onSubscribeJackpot = function () {
        // SicboPortalAdapter.getPotterHandler().onSubscribeJackpot("SicboUIComp", (res: InstanceType<typeof ListCurrentJackpotsReply>) => {
        //   this.jackpot.handleResponseJackpot(res);
        // });
    };
    SicboUIComp.prototype.onUnSubscribeJackpot = function () {
        // SicboPortalAdapter.getPotterHandler().onUnSubscribeJackpot("SicboUIComp");
    };
    //ANCHOR show Error message
    SicboUIComp.prototype.showNotifyMessage = function (message) {
        this.notifyComp.showSmallNoti(message, 0.1, 2, 0.1);
    };
    SicboUIComp.prototype.showErrMessage = function (errCode, err) {
        // if (errCode == Code.SICBO_BET_FAILED ||
        //   errCode == Code.SICBO_CANNOT_REBET ||
        //   errCode == Code.SICBO_CANNOT_BET_DOUBLE) return;
        var message = SicboText_1.default.getErrorMessage(errCode);
        if (message == null) {
            message = errCode + " - " + err;
            //cc.log("SICBO ERROR", message);
        }
        else {
            this.showNotifyMessage(message);
        }
    };
    //!SECTION
    //SECTION user profile
    SicboUIComp.prototype.showUserProfilePopUp = function (playah) {
        if (playah == null)
            return;
        SicboPortalAdapter_1.default.getInstance().showUserProfilePopUp(playah, this.userProfileBackground);
    };
    var SicboUIComp_1;
    SicboUIComp.instance = null;
    __decorate([
        property(cc.Sprite)
    ], SicboUIComp.prototype, "effectShow", void 0);
    __decorate([
        property(SicboUserComp_1.default)
    ], SicboUIComp.prototype, "userComp", void 0);
    __decorate([
        property(cc.Button)
    ], SicboUIComp.prototype, "chatButton", void 0);
    __decorate([
        property(cc.Node)
    ], SicboUIComp.prototype, "shakeBetTarget", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], SicboUIComp.prototype, "userProfileBackground", void 0);
    __decorate([
        property(cc.Node)
    ], SicboUIComp.prototype, "betCompNode", void 0);
    __decorate([
        property(SicboSessionInfoComp_1.default)
    ], SicboUIComp.prototype, "sessionComp", void 0);
    __decorate([
        property(SicboMenuDetails_1.default)
    ], SicboUIComp.prototype, "menuDetails", void 0);
    __decorate([
        property(SicboClockStateComp_1.default)
    ], SicboUIComp.prototype, "clock", void 0);
    __decorate([
        property(SicboDealerStateComp_1.default)
    ], SicboUIComp.prototype, "dealer", void 0);
    __decorate([
        property(SicboResultStateComp_1.default)
    ], SicboUIComp.prototype, "result", void 0);
    __decorate([
        property(SicboJackpotStateComp_1.default)
    ], SicboUIComp.prototype, "jackpot", void 0);
    __decorate([
        property(SicboNotifyStateComp_1.default)
    ], SicboUIComp.prototype, "message", void 0);
    __decorate([
        property(SicboNotifyComp_1.default)
    ], SicboUIComp.prototype, "notifyComp", void 0);
    __decorate([
        property(cc.Node)
    ], SicboUIComp.prototype, "chatRoot", void 0);
    SicboUIComp = SicboUIComp_1 = __decorate([
        ccclass
    ], SicboUIComp);
    return SicboUIComp;
}(cc.Component));
exports.default = SicboUIComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9VSUNvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEscUVBQWdFO0FBRTFELElBQUEsS0FBc0QsNEJBQWtCLENBQUMsVUFBVSxFQUFFLEVBQW5GLE1BQU0sWUFBQSxFQUFFLEtBQUssV0FBQSxFQUFFLE1BQU0sWUFBQSxFQUFFLHdCQUF3Qiw4QkFBb0MsQ0FBQztBQUU1Rix3REFBbUU7QUFFbkUsaURBQTRDO0FBQzVDLCtDQUEwQztBQUUxQyxrRUFBNkQ7QUFFN0QscURBQWdEO0FBQ2hELHNEQUFpRDtBQUNqRCwrREFBMEQ7QUFDMUQsb0VBQStEO0FBQy9ELHNFQUFpRTtBQUNqRSxzRUFBaUU7QUFDakUsd0VBQW1FO0FBQ25FLHNFQUFpRTtBQUVqRSw0REFBdUQ7QUFFdkQscUVBQWdFO0FBRWhFLGtEQUE2QztBQUM3Qyx1REFBa0Q7QUFFNUMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBeUMsK0JBQVk7SUFBckQ7UUFBQSxxRUF3UUM7UUFsUUMsaUJBQWlCO1FBQ2pCLHlCQUF5QjtRQUVqQixnQkFBVSxHQUFjLElBQUksQ0FBQztRQUVyQyxXQUFXO1FBRUgsY0FBUSxHQUFrQixJQUFJLENBQUM7UUFHL0IsZ0JBQVUsR0FBYyxJQUFJLENBQUM7UUFHN0Isb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsMkJBQXFCLEdBQW1CLElBQUksQ0FBQztRQUNyRCxVQUFVO1FBRUYsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFDNUIsYUFBTyxHQUFpQixJQUFJLENBQUM7UUFHN0IsaUJBQVcsR0FBeUIsSUFBSSxDQUFDO1FBR3pDLGlCQUFXLEdBQXFCLElBQUksQ0FBQztRQUU3QyxpQkFBaUI7UUFFVCxXQUFLLEdBQXdCLElBQUksQ0FBQztRQUdsQyxZQUFNLEdBQXlCLElBQUksQ0FBQztRQUdwQyxZQUFNLEdBQXlCLElBQUksQ0FBQztRQUdwQyxhQUFPLEdBQTBCLElBQUksQ0FBQztRQUd0QyxhQUFPLEdBQXlCLElBQUksQ0FBQztRQUc3QyxnQkFBVSxHQUFvQixJQUFJLENBQUM7UUFHM0IsY0FBUSxHQUFZLElBQUksQ0FBQztRQUVqQyxVQUFVO1FBQ0YsaUJBQVcsR0FBRyxNQUFNLENBQUMsa0JBQWtCLENBQUM7O1FBOE1oRCxVQUFVO0lBQ1osQ0FBQztvQkF4UW9CLFdBQVc7SUFFOUIsc0JBQVcsdUJBQVE7YUFBbkI7WUFDRSxPQUFPLGFBQVcsQ0FBQyxRQUFRLENBQUM7UUFDOUIsQ0FBQzs7O09BQUE7SUF1REQsNEJBQU0sR0FBTjtRQUNFLGFBQVcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsc0JBQVksQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsK0JBQVMsR0FBVDtRQUNFLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQzVCLE9BQU8sYUFBVyxDQUFDLFFBQVEsQ0FBQztRQUM1QixhQUFXLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUM5QixDQUFDO0lBRUQsNEJBQU0sR0FBTixVQUFPLEVBQUU7UUFDUCxvQkFBb0I7SUFDdEIsQ0FBQztJQUVELDJCQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25DLElBQUksQ0FBQyxlQUFlLENBQUMsc0JBQVksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDM0IsQ0FBQztJQUVELHVDQUFpQixHQUFqQjtRQUNFLDhEQUE4RDtRQUM5RCxvQ0FBb0M7UUFDcEMsbUVBQW1FO1FBQ25FLE1BQU07SUFDUixDQUFDO0lBRUQsYUFBYTtJQUNiLCtCQUFTLEdBQVQsVUFBVSxJQUFJLEVBQUUsUUFBdUI7UUFBdkMsaUJBUUM7UUFQQyxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksTUFBTSxDQUFDLE9BQU87WUFBRSxPQUFPO1FBRS9DLElBQUksVUFBVSxHQUFHLHFCQUFXLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDOUQseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUU7WUFDL0MsS0FBSSxDQUFDLFdBQVcsQ0FBQyw0QkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEVBQUUsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDbkYseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHlCQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsa0NBQVksR0FBWjtRQUFBLGlCQVFDO1FBUEMseUJBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQUMsSUFBSTtZQUN0QyxJQUFJLFFBQVEsR0FBRyw0QkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5RCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUMsR0FBRztnQkFDZixLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7WUFDaEUsQ0FBQyxDQUFDLENBQUM7WUFDSCx5QkFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMseUJBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxzQ0FBZ0IsR0FBaEI7UUFBQSxpQkFRQztRQVBDLHlCQUFlLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxVQUFDLElBQUk7WUFDMUMsSUFBSSxRQUFRLEdBQUcsNEJBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFDLEdBQUc7Z0JBQ2YsS0FBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1lBQ2hFLENBQUMsQ0FBQyxDQUFDO1lBQ0gseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHlCQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsVUFBVTtJQUVWLG1DQUFhLEdBQWIsVUFBYyxRQUFpQjtRQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDO0lBQ3pDLENBQUM7SUFFRCw0QkFBNEI7SUFDNUIsMkNBQXFCLEdBQXJCO1FBQUEsaUJBUUM7UUFQQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO2FBQzNCLEtBQUssQ0FBQyxHQUFHLENBQUM7YUFDVixFQUFFLENBQUMsQ0FBQyxFQUFFO1lBQ0wsT0FBTyxFQUFFLENBQUM7U0FDWCxDQUFDO2FBQ0QsSUFBSSxDQUFDLGNBQU0sT0FBQSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsRUFBckMsQ0FBcUMsQ0FBQzthQUNqRCxLQUFLLEVBQUUsQ0FBQztJQUNiLENBQUM7SUFFRCxxQkFBcUI7SUFDckIscUNBQWUsR0FBZixVQUFnQixVQUFvQjtRQUNsQyxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsVUFBVTtJQUVWLHNCQUFzQjtJQUN0QixrQ0FBWSxHQUFaLFVBQWEsTUFBYztRQUN6QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvQyxJQUFJLHdCQUFjLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDMUMsSUFBSSxRQUFRLEdBQUcsNEJBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDOUQsSUFBSSxNQUFNLElBQUksUUFBUTtnQkFBRSxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsQ0FBQzs7Z0JBQ3BELElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixFQUFFLENBQUM7U0FDL0M7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxnQ0FBVSxHQUFWO1FBQ0UsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25DLENBQUM7SUFFRCx1Q0FBaUIsR0FBakIsVUFBa0IsY0FBNkMsRUFBRSxxQkFBNkI7UUFDNUYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUscUJBQXFCLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsaUNBQVcsR0FBWCxVQUFZLE1BQWMsRUFBRSxJQUFJLEVBQUUsSUFBWTtRQUM1QyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUU1QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLE9BQU8sQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLFlBQVksRUFBRSxRQUFRLENBQUMsQ0FBQztJQUNoRixDQUFDO0lBRUQseUNBQW1CLEdBQW5CLFVBQW9CLFVBQWtCO1FBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxpQkFBaUI7SUFDakIsK0JBQVMsR0FBVCxVQUFVLE1BQU07UUFDZCxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQsZ0NBQVUsR0FBVixVQUFXLEtBQWlDLEVBQUUsYUFBcUI7UUFDakUsMERBQTBEO1FBQzFELElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFFbEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVELGlDQUFXLEdBQVgsVUFBWSxLQUFpQztRQUMzQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsNEJBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBQ1MsbUNBQWEsR0FBdkI7UUFDRSxnSUFBZ0k7UUFDaEksNkNBQTZDO1FBQzdDLE1BQU07SUFDUixDQUFDO0lBQ1Msd0NBQWtCLEdBQTVCO1FBQ0Usb0lBQW9JO1FBQ3BJLDZDQUE2QztRQUM3QyxNQUFNO0lBQ1IsQ0FBQztJQUVTLDBDQUFvQixHQUE5QjtRQUNFLDZFQUE2RTtJQUMvRSxDQUFDO0lBRUQsMkJBQTJCO0lBQzNCLHVDQUFpQixHQUFqQixVQUFrQixPQUFlO1FBQy9CLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRCxvQ0FBYyxHQUFkLFVBQWUsT0FBTyxFQUFFLEdBQVc7UUFDakMsMENBQTBDO1FBQzFDLDBDQUEwQztRQUMxQyxxREFBcUQ7UUFFckQsSUFBSSxPQUFPLEdBQUcsbUJBQVMsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDakQsSUFBSSxPQUFPLElBQUksSUFBSSxFQUFFO1lBQ25CLE9BQU8sR0FBRyxPQUFPLEdBQUcsS0FBSyxHQUFHLEdBQUcsQ0FBQztZQUNoQyxpQ0FBaUM7U0FDbEM7YUFBTTtZQUNMLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNqQztJQUNILENBQUM7SUFFRCxVQUFVO0lBRVYsc0JBQXNCO0lBQ3RCLDBDQUFvQixHQUFwQixVQUFxQixNQUFtQztRQUN0RCxJQUFJLE1BQU0sSUFBSSxJQUFJO1lBQUUsT0FBTztRQUMzQiw0QkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDNUYsQ0FBQzs7SUFyUWMsb0JBQVEsR0FBZ0IsSUFBSSxDQUFDO0lBUTVDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7bURBQ2lCO0lBSXJDO1FBREMsUUFBUSxDQUFDLHVCQUFhLENBQUM7aURBQ2U7SUFHdkM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzttREFDaUI7SUFHckM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt1REFDcUI7SUFHdkM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzs4REFDNEI7SUFHckQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztvREFDa0I7SUFJcEM7UUFEQyxRQUFRLENBQUMsOEJBQW9CLENBQUM7b0RBQ2tCO0lBR2pEO1FBREMsUUFBUSxDQUFDLDBCQUFnQixDQUFDO29EQUNrQjtJQUk3QztRQURDLFFBQVEsQ0FBQyw2QkFBbUIsQ0FBQzs4Q0FDWTtJQUcxQztRQURDLFFBQVEsQ0FBQyw4QkFBb0IsQ0FBQzsrQ0FDYTtJQUc1QztRQURDLFFBQVEsQ0FBQyw4QkFBb0IsQ0FBQzsrQ0FDYTtJQUc1QztRQURDLFFBQVEsQ0FBQywrQkFBcUIsQ0FBQztnREFDYztJQUc5QztRQURDLFFBQVEsQ0FBQyw4QkFBb0IsQ0FBQztnREFDYztJQUc3QztRQURDLFFBQVEsQ0FBQyx5QkFBZSxDQUFDO21EQUNTO0lBR25DO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ2U7SUF0RGQsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQXdRL0I7SUFBRCxrQkFBQztDQXhRRCxBQXdRQyxDQXhRd0MsRUFBRSxDQUFDLFNBQVMsR0F3UXBEO2tCQXhRb0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBTaWNib01vZHVsZUFkYXB0ZXIgZnJvbSBcIi4uLy4uLy4uLy4uL1NpY2JvTW9kdWxlQWRhcHRlclwiO1xuXG5jb25zdCB7IFBsYXlhaCwgU3RhdGUsIFN0YXR1cywgTGlzdEN1cnJlbnRKYWNrcG90c1JlcGx5IH0gPSBTaWNib01vZHVsZUFkYXB0ZXIuZ2V0QWxsUmVmcygpO1xuXG5pbXBvcnQgU2ljYm9TZXR0aW5nLCB7IFNpY2JvU291bmQgfSBmcm9tIFwiLi4vU2V0dGluZy9TaWNib1NldHRpbmdcIjtcbmltcG9ydCBTaWNib1VzZXJJdGVtQ29tcCBmcm9tIFwiLi9JdGVtcy9TaWNib1VzZXJJdGVtQ29tcFwiO1xuaW1wb3J0IFNpY2JvVXNlckNvbXAgZnJvbSBcIi4vU2ljYm9Vc2VyQ29tcFwiO1xuaW1wb3J0IFNpY2JvQmV0Q29tcCBmcm9tIFwiLi9TaWNib0JldENvbXBcIjtcblxuaW1wb3J0IFNpY2JvQ29udHJvbGxlciBmcm9tIFwiLi4vQ29udHJvbGxlcnMvU2ljYm9Db250cm9sbGVyXCI7XG5cbmltcG9ydCBTaWNib05vdGlmeUNvbXAgZnJvbSBcIi4vU2ljYm9Ob3RpZnlDb21wXCI7XG5pbXBvcnQgU2ljYm9IZWxwZXIgZnJvbSBcIi4uL0hlbHBlcnMvU2ljYm9IZWxwZXJcIjtcbmltcG9ydCBTaWNib1Nlc3Npb25JbmZvQ29tcCBmcm9tIFwiLi9TaWNib1Nlc3Npb25JbmZvQ29tcFwiO1xuaW1wb3J0IFNpY2JvQ2xvY2tTdGF0ZUNvbXAgZnJvbSBcIi4vU3RhdGVzL1NpY2JvQ2xvY2tTdGF0ZUNvbXBcIjtcbmltcG9ydCBTaWNib0RlYWxlclN0YXRlQ29tcCBmcm9tIFwiLi9TdGF0ZXMvU2ljYm9EZWFsZXJTdGF0ZUNvbXBcIjtcbmltcG9ydCBTaWNib1Jlc3VsdFN0YXRlQ29tcCBmcm9tIFwiLi9TdGF0ZXMvU2ljYm9SZXN1bHRTdGF0ZUNvbXBcIjtcbmltcG9ydCBTaWNib0phY2twb3RTdGF0ZUNvbXAgZnJvbSBcIi4vU3RhdGVzL1NpY2JvSmFja3BvdFN0YXRlQ29tcFwiO1xuaW1wb3J0IFNpY2JvTm90aWZ5U3RhdGVDb21wIGZyb20gXCIuL1N0YXRlcy9TaWNib05vdGlmeVN0YXRlQ29tcFwiO1xuaW1wb3J0IFNpY2JvVUlNYW5hZ2VyIGZyb20gXCIuLi9NYW5hZ2Vycy9TaWNib1VJTWFuYWdlclwiO1xuaW1wb3J0IFNpY2JvUG9ydGFsQWRhcHRlciBmcm9tIFwiLi4vU2ljYm9Qb3J0YWxBZGFwdGVyXCI7XG5pbXBvcnQgU2ljYm9SZXNvdXJjZU1hbmFnZXIgZnJvbSBcIi4uL01hbmFnZXJzL1NpY2JvUmVzb3VyY2VNYW5hZ2VyXCI7XG5pbXBvcnQgU2ljYm9HYW1lVXRpbHMgZnJvbSBcIi4uL1JOR0NvbW1vbnMvVXRpbHMvU2ljYm9HYW1lVXRpbHNcIjtcbmltcG9ydCB7IFNpY2JvQ29pblR5cGUgfSBmcm9tIFwiLi4vUk5HQ29tbW9ucy9TaWNib0NvaW5UeXBlXCI7XG5pbXBvcnQgU2ljYm9UZXh0IGZyb20gXCIuLi9TZXR0aW5nL1NpY2JvVGV4dFwiO1xuaW1wb3J0IFNpY2JvTWVudURldGFpbHMgZnJvbSBcIi4vU2ljYm9NZW51RGV0YWlsc1wiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9VSUNvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogU2ljYm9VSUNvbXAgPSBudWxsO1xuICBzdGF0aWMgZ2V0IEluc3RhbmNlKCk6IFNpY2JvVUlDb21wIHtcbiAgICByZXR1cm4gU2ljYm9VSUNvbXAuaW5zdGFuY2U7XG4gIH1cblxuICAvL0dhbWVCZXRVSUNvbXAge1xuICAvL09QRU4gU0NFTkUgRkFERSBPVVQgRUZGXG4gIEBwcm9wZXJ0eShjYy5TcHJpdGUpXG4gIHByaXZhdGUgZWZmZWN0U2hvdzogY2MuU3ByaXRlID0gbnVsbDtcblxuICAvL1VTRVIgQ09NUFxuICBAcHJvcGVydHkoU2ljYm9Vc2VyQ29tcClcbiAgcHJpdmF0ZSB1c2VyQ29tcDogU2ljYm9Vc2VyQ29tcCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLkJ1dHRvbilcbiAgcHJpdmF0ZSBjaGF0QnV0dG9uOiBjYy5CdXR0b24gPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBwcml2YXRlIHNoYWtlQmV0VGFyZ2V0OiBjYy5Ob2RlID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuU3ByaXRlRnJhbWUpXG4gIHByaXZhdGUgdXNlclByb2ZpbGVCYWNrZ3JvdW5kOiBjYy5TcHJpdGVGcmFtZSA9IG51bGw7XG4gIC8vQkVUIENPTVBcbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIHByaXZhdGUgYmV0Q29tcE5vZGU6IGNjLk5vZGUgPSBudWxsO1xuICBwcml2YXRlIGJldENvbXA6IFNpY2JvQmV0Q29tcCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KFNpY2JvU2Vzc2lvbkluZm9Db21wKVxuICBwcml2YXRlIHNlc3Npb25Db21wOiBTaWNib1Nlc3Npb25JbmZvQ29tcCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KFNpY2JvTWVudURldGFpbHMpXG4gIHByaXZhdGUgbWVudURldGFpbHM6IFNpY2JvTWVudURldGFpbHMgPSBudWxsO1xuXG4gIC8vQU5DSE9SIFN0YXRlIFVJXG4gIEBwcm9wZXJ0eShTaWNib0Nsb2NrU3RhdGVDb21wKVxuICBwcml2YXRlIGNsb2NrOiBTaWNib0Nsb2NrU3RhdGVDb21wID0gbnVsbDtcblxuICBAcHJvcGVydHkoU2ljYm9EZWFsZXJTdGF0ZUNvbXApXG4gIHByaXZhdGUgZGVhbGVyOiBTaWNib0RlYWxlclN0YXRlQ29tcCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KFNpY2JvUmVzdWx0U3RhdGVDb21wKVxuICBwcml2YXRlIHJlc3VsdDogU2ljYm9SZXN1bHRTdGF0ZUNvbXAgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShTaWNib0phY2twb3RTdGF0ZUNvbXApXG4gIHByaXZhdGUgamFja3BvdDogU2ljYm9KYWNrcG90U3RhdGVDb21wID0gbnVsbDtcblxuICBAcHJvcGVydHkoU2ljYm9Ob3RpZnlTdGF0ZUNvbXApXG4gIHByaXZhdGUgbWVzc2FnZTogU2ljYm9Ob3RpZnlTdGF0ZUNvbXAgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShTaWNib05vdGlmeUNvbXApXG4gIG5vdGlmeUNvbXA6IFNpY2JvTm90aWZ5Q29tcCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIHByaXZhdGUgY2hhdFJvb3Q6IGNjLk5vZGUgPSBudWxsO1xuXG4gIC8vIVNFQ1RJT05cbiAgcHJpdmF0ZSBzdGF0ZVN0YXR1cyA9IFN0YXR1cy5TVEFUVVNfVU5TUEVDSUZJRUQ7XG5cbiAgb25Mb2FkKCkge1xuICAgIFNpY2JvVUlDb21wLmluc3RhbmNlID0gdGhpcztcbiAgICB0aGlzLmJldENvbXAgPSB0aGlzLmJldENvbXBOb2RlLmdldENvbXBvbmVudChTaWNib0JldENvbXApO1xuICAgIHRoaXMub25Mb2FkSmFja3BvdCgpO1xuICAgIHRoaXMub25TdWJzY3JpYmVKYWNrcG90KCk7XG4gIH1cblxuICBvbkRlc3Ryb3koKSB7XG4gICAgdGhpcy5vblVuU3Vic2NyaWJlSmFja3BvdCgpO1xuICAgIGRlbGV0ZSBTaWNib1VJQ29tcC5pbnN0YW5jZTtcbiAgICBTaWNib1VJQ29tcC5pbnN0YW5jZSA9IG51bGw7XG4gIH1cblxuICB1cGRhdGUoZHQpIHtcbiAgICAvLyBzdXBlci51cGRhdGUoZHQpO1xuICB9XG5cbiAgc3RhcnQoKSB7XG4gICAgdGhpcy5lZmZlY3RTaG93Lm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICB0aGlzLmNyZWF0ZUNoaXBJdGVtcyhTaWNib1NldHRpbmcuZ2V0Q2hpcExldmVsTGlzdCgpKTtcbiAgICB0aGlzLnNob3dFZmZlY3RMb2FkaW5nR2FtZSgpO1xuICAgIHRoaXMucHJlbG9hZEluZm9TcGl0ZXMoKTtcbiAgfVxuXG4gIHByZWxvYWRJbmZvU3BpdGVzKCkge1xuICAgIC8vIGxldCBpbmZvUmVzb3VyY2VzID0gU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLmdldEdhbWVJbmZvKCk7XG4gICAgLy8gaW5mb1Jlc291cmNlcy5mb3JFYWNoKChpbmZvKSA9PiB7XG4gICAgLy8gICBTaWNib1Jlc291cmNlTWFuYWdlci5sb2FkUmVtb3RlSW1hZ2VXaXRoRmlsZUZpZWxkKG51bGwsIGluZm8pO1xuICAgIC8vIH0pO1xuICB9XG5cbiAgLy9TRUNUSU9OIEJFVFxuICBteVVzZXJCZXQoZG9vciwgY29pblR5cGU6IFNpY2JvQ29pblR5cGUpIHtcbiAgICBpZiAodGhpcy5zdGF0ZVN0YXR1cyAhPSBTdGF0dXMuQkVUVElORykgcmV0dXJuO1xuXG4gICAgbGV0IGNvaW5OdW1iZXIgPSBTaWNib0hlbHBlci5jb252ZXJ0Q29pblR5cGVUb1ZhbHVlKGNvaW5UeXBlKTtcbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2Uuc2VuZEJldChkb29yLCBjb2luVHlwZSwgKCkgPT4ge1xuICAgICAgdGhpcy5wbGF5VXNlckJldChTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0SW5zdGFuY2UoKS5nZXRNeVVzZXJJZCgpLCBkb29yLCBjb2luTnVtYmVyKTtcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4QmV0KTtcbiAgICB9KTtcbiAgfVxuXG4gIG9uQ2xpY2tSZUJldCgpIHtcbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2Uuc2VuZFJlQmV0KChkYWJzKSA9PiB7XG4gICAgICBsZXQgbXlVc2VySWQgPSBTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0SW5zdGFuY2UoKS5nZXRNeVVzZXJJZCgpO1xuICAgICAgZGFicy5mb3JFYWNoKChkYWIpID0+IHtcbiAgICAgICAgdGhpcy5wbGF5VXNlckJldChteVVzZXJJZCwgZGFiLmdldERvb3IoKSwgZGFiLmdldEJldEFtb3VudCgpKTtcbiAgICAgIH0pO1xuICAgICAgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLnBsYXlTZngoU2ljYm9Tb3VuZC5TZnhCZXQpO1xuICAgIH0pO1xuICB9XG5cbiAgb25DbGlja0RvdWJsZUJldCgpIHtcbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2Uuc2VuZEJldERvdWJsZSgoZGFicykgPT4ge1xuICAgICAgbGV0IG15VXNlcklkID0gU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuZ2V0TXlVc2VySWQoKTtcbiAgICAgIGRhYnMuZm9yRWFjaCgoZGFiKSA9PiB7XG4gICAgICAgIHRoaXMucGxheVVzZXJCZXQobXlVc2VySWQsIGRhYi5nZXREb29yKCksIGRhYi5nZXRCZXRBbW91bnQoKSk7XG4gICAgICB9KTtcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4QmV0KTtcbiAgICB9KTtcbiAgfVxuICAvLyFTRUNUSU9OXG5cbiAgc2V0Q2hhdEVuYWJsZShpc0VuYWJsZTogYm9vbGVhbikge1xuICAgIHRoaXMuY2hhdEJ1dHRvbi5ub2RlLmFjdGl2ZSA9IGlzRW5hYmxlO1xuICB9XG5cbiAgLy9BTkNIT1IgZWZmZWN0IGxvYWRpbmcgZ2FtZVxuICBzaG93RWZmZWN0TG9hZGluZ0dhbWUoKSB7XG4gICAgY2MudHdlZW4odGhpcy5lZmZlY3RTaG93Lm5vZGUpXG4gICAgICAuZGVsYXkoMC4zKVxuICAgICAgLnRvKDEsIHtcbiAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgIH0pXG4gICAgICAuY2FsbCgoKSA9PiAodGhpcy5lZmZlY3RTaG93Lm5vZGUuYWN0aXZlID0gZmFsc2UpKVxuICAgICAgLnN0YXJ0KCk7XG4gIH1cblxuICAvL0FOQ0hPUiBTaWNib0JldENvbXBcbiAgY3JlYXRlQ2hpcEl0ZW1zKGNoaXBfbGV2ZWw6IG51bWJlcltdKSB7XG4gICAgdGhpcy5iZXRDb21wLmNyZWF0ZUNoaXBJdGVtcyhjaGlwX2xldmVsKTtcbiAgfVxuXG4gIC8vIVNFQ1RJT05cblxuICAvL0FOQ0hPUiBTaWNib1VzZXJDb21wXG4gIGZpbmRVc2VyQ29tcCh1c2VySWQ6IHN0cmluZyk6IFNpY2JvVXNlckl0ZW1Db21wIHtcbiAgICBsZXQgdXNlciA9IHRoaXMudXNlckNvbXAuZ2V0VXNlckluUm9vbSh1c2VySWQpO1xuICAgIGlmIChTaWNib0dhbWVVdGlscy5pc051bGxPclVuZGVmaW5lZCh1c2VyKSkge1xuICAgICAgbGV0IG15VXNlcklkID0gU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuZ2V0TXlVc2VySWQoKTtcbiAgICAgIGlmICh1c2VySWQgPT0gbXlVc2VySWQpIHVzZXIgPSB0aGlzLnVzZXJDb21wLmdldE15VXNlcigpO1xuICAgICAgZWxzZSB1c2VyID0gdGhpcy51c2VyQ29tcC5nZXRPdGhlclVzZXJHcm91cCgpO1xuICAgIH1cbiAgICByZXR1cm4gdXNlcjtcbiAgfVxuXG4gIG15VXNlckNvbXAoKSB7XG4gICAgcmV0dXJuIHRoaXMudXNlckNvbXAuZ2V0TXlVc2VyKCk7XG4gIH1cblxuICB1cGRhdGVVc2Vyc0luUm9vbShkaXNwbGF5UGxheWVyczogSW5zdGFuY2VUeXBlPHR5cGVvZiBQbGF5YWg+W10sIHJlbWFpbmluZ1BsYXllcnNDb3VudDogbnVtYmVyKSB7XG4gICAgdGhpcy51c2VyQ29tcC51cGRhdGVVc2Vyc0luUm9vbShkaXNwbGF5UGxheWVycywgcmVtYWluaW5nUGxheWVyc0NvdW50KTtcbiAgfVxuXG4gIHBsYXlVc2VyQmV0KHVzZXJJZDogc3RyaW5nLCBkb29yLCBjaGlwOiBudW1iZXIpIHtcbiAgICBsZXQgdXNlclVJID0gdGhpcy51c2VyQ29tcC5maW5kVXNlcih1c2VySWQpO1xuXG4gICAgbGV0IGRvb3JDb21wID0gdGhpcy5iZXRDb21wLmdldERvb3IoZG9vcik7XG4gICAgdGhpcy51c2VyQ29tcC5wbGF5QmV0QW5pbWF0aW9uKHVzZXJVSSwgdGhpcy5zaGFrZUJldFRhcmdldCk7XG4gICAgdGhpcy5iZXRDb21wLnRocm93TWluaUNoaXBPblRhYmxlQnlWYWx1ZShjaGlwLCB1c2VyVUkuYmV0U2hha2VOb2RlLCBkb29yQ29tcCk7XG4gIH1cblxuICBjaGFuZ2VNeVVzZXJCYWxhbmNlKG5ld0JhbGFuY2U6IG51bWJlcikge1xuICAgIHRoaXMudXNlckNvbXAuZ2V0TXlVc2VyKCkuc2V0TW9uZXkobmV3QmFsYW5jZSk7XG4gIH1cblxuICAvL0FOQ0hPUiBTdGF0ZSBVSVxuICBleGl0U3RhdGUoc3RhdHVzKSB7XG4gICAgdGhpcy5iZXRDb21wLmV4aXRTdGF0ZShzdGF0dXMpO1xuICAgIHRoaXMuY2xvY2suZXhpdFN0YXRlKHN0YXR1cyk7XG4gICAgdGhpcy5kZWFsZXIuZXhpdFN0YXRlKHN0YXR1cyk7XG4gICAgdGhpcy5yZXN1bHQuZXhpdFN0YXRlKHN0YXR1cyk7XG4gICAgdGhpcy5qYWNrcG90LmV4aXRTdGF0ZShzdGF0dXMpO1xuICAgIHRoaXMubWVzc2FnZS5leGl0U3RhdGUoc3RhdHVzKTtcbiAgICB0aGlzLnNlc3Npb25Db21wLmV4aXRTdGF0ZShzdGF0dXMpO1xuICAgIHRoaXMubWVudURldGFpbHMuZXhpdFN0YXRlKHN0YXR1cyk7XG4gIH1cblxuICBzdGFydFN0YXRlKHN0YXRlOiBJbnN0YW5jZVR5cGU8dHlwZW9mIFN0YXRlPiwgc3RhdGVEdXJhdGlvbjogbnVtYmVyKSB7XG4gICAgLy9jYy5sb2coXCJTSUNCTyBTVEFURVwiLCBzdGF0ZS5nZXRTdGF0dXMoKSwgc3RhdGVEdXJhdGlvbik7XG4gICAgdGhpcy5iZXRDb21wLnN0YXJ0U3RhdGUoc3RhdGUsIHN0YXRlRHVyYXRpb24pO1xuICAgIHRoaXMuY2xvY2suc3RhcnRTdGF0ZShzdGF0ZSwgc3RhdGVEdXJhdGlvbik7XG4gICAgdGhpcy5kZWFsZXIuc3RhcnRTdGF0ZShzdGF0ZSwgc3RhdGVEdXJhdGlvbik7XG4gICAgdGhpcy5yZXN1bHQuc3RhcnRTdGF0ZShzdGF0ZSwgc3RhdGVEdXJhdGlvbik7XG4gICAgdGhpcy5qYWNrcG90LnN0YXJ0U3RhdGUoc3RhdGUsIHN0YXRlRHVyYXRpb24pO1xuICAgIHRoaXMubWVzc2FnZS5zdGFydFN0YXRlKHN0YXRlLCBzdGF0ZUR1cmF0aW9uKTtcbiAgICB0aGlzLnNlc3Npb25Db21wLnN0YXJ0U3RhdGUoc3RhdGUsIHN0YXRlRHVyYXRpb24pO1xuICAgIHRoaXMubWVudURldGFpbHMuc3RhcnRTdGF0ZShzdGF0ZSwgc3RhdGVEdXJhdGlvbik7XG5cbiAgICB0aGlzLnN0YXRlU3RhdHVzID0gc3RhdGUuZ2V0U3RhdHVzKCk7XG4gIH1cblxuICB1cGRhdGVTdGF0ZShzdGF0ZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGF0ZT4pIHtcbiAgICB0aGlzLmJldENvbXAudXBkYXRlU3RhdGUoc3RhdGUpO1xuICAgIHRoaXMuY2xvY2sudXBkYXRlU3RhdGUoc3RhdGUpO1xuICAgIHRoaXMuZGVhbGVyLnVwZGF0ZVN0YXRlKHN0YXRlKTtcbiAgICB0aGlzLnJlc3VsdC51cGRhdGVTdGF0ZShzdGF0ZSk7XG4gICAgdGhpcy5qYWNrcG90LnVwZGF0ZVN0YXRlKHN0YXRlKTtcbiAgICB0aGlzLm1lc3NhZ2UudXBkYXRlU3RhdGUoc3RhdGUpO1xuICAgIHRoaXMuc2Vzc2lvbkNvbXAudXBkYXRlU3RhdGUoc3RhdGUpO1xuICAgIHRoaXMubWVudURldGFpbHMudXBkYXRlU3RhdGUoc3RhdGUpO1xuICB9XG5cbiAgb25IaWRlKCkge1xuICAgIHRoaXMuYmV0Q29tcC5vbkhpZGUoKTtcbiAgICB0aGlzLmNsb2NrLm9uSGlkZSgpO1xuICAgIHRoaXMuZGVhbGVyLm9uSGlkZSgpO1xuICAgIHRoaXMucmVzdWx0Lm9uSGlkZSgpO1xuICAgIHRoaXMuamFja3BvdC5vbkhpZGUoKTtcbiAgICB0aGlzLm1lc3NhZ2Uub25IaWRlKCk7XG4gICAgdGhpcy5zZXNzaW9uQ29tcC5vbkhpZGUoKTtcbiAgICB0aGlzLm1lbnVEZXRhaWxzLm9uSGlkZSgpO1xuICB9XG4gIHByb3RlY3RlZCBvbkxvYWRKYWNrcG90KCkge1xuICAgIC8vIFNpY2JvUG9ydGFsQWRhcHRlci5nZXRQb3R0ZXJIYW5kbGVyKCkuZ2V0TGlzdEN1cnJlbnRKYWNrcG90c1JlcXVlc3QoKHJlczogSW5zdGFuY2VUeXBlPHR5cGVvZiBMaXN0Q3VycmVudEphY2twb3RzUmVwbHk+KSA9PiB7XG4gICAgLy8gICB0aGlzLmphY2twb3QuaGFuZGxlUmVzcG9uc2VKYWNrcG90KHJlcyk7XG4gICAgLy8gfSk7XG4gIH1cbiAgcHJvdGVjdGVkIG9uU3Vic2NyaWJlSmFja3BvdCgpIHtcbiAgICAvLyBTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0UG90dGVySGFuZGxlcigpLm9uU3Vic2NyaWJlSmFja3BvdChcIlNpY2JvVUlDb21wXCIsIChyZXM6IEluc3RhbmNlVHlwZTx0eXBlb2YgTGlzdEN1cnJlbnRKYWNrcG90c1JlcGx5PikgPT4ge1xuICAgIC8vICAgdGhpcy5qYWNrcG90LmhhbmRsZVJlc3BvbnNlSmFja3BvdChyZXMpO1xuICAgIC8vIH0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIG9uVW5TdWJzY3JpYmVKYWNrcG90KCkge1xuICAgIC8vIFNpY2JvUG9ydGFsQWRhcHRlci5nZXRQb3R0ZXJIYW5kbGVyKCkub25VblN1YnNjcmliZUphY2twb3QoXCJTaWNib1VJQ29tcFwiKTtcbiAgfVxuXG4gIC8vQU5DSE9SIHNob3cgRXJyb3IgbWVzc2FnZVxuICBzaG93Tm90aWZ5TWVzc2FnZShtZXNzYWdlOiBzdHJpbmcpIHtcbiAgICB0aGlzLm5vdGlmeUNvbXAuc2hvd1NtYWxsTm90aShtZXNzYWdlLCAwLjEsIDIsIDAuMSk7XG4gIH1cblxuICBzaG93RXJyTWVzc2FnZShlcnJDb2RlLCBlcnI6IHN0cmluZykge1xuICAgIC8vIGlmIChlcnJDb2RlID09IENvZGUuU0lDQk9fQkVUX0ZBSUxFRCB8fFxuICAgIC8vICAgZXJyQ29kZSA9PSBDb2RlLlNJQ0JPX0NBTk5PVF9SRUJFVCB8fFxuICAgIC8vICAgZXJyQ29kZSA9PSBDb2RlLlNJQ0JPX0NBTk5PVF9CRVRfRE9VQkxFKSByZXR1cm47XG5cbiAgICBsZXQgbWVzc2FnZSA9IFNpY2JvVGV4dC5nZXRFcnJvck1lc3NhZ2UoZXJyQ29kZSk7XG4gICAgaWYgKG1lc3NhZ2UgPT0gbnVsbCkge1xuICAgICAgbWVzc2FnZSA9IGVyckNvZGUgKyBcIiAtIFwiICsgZXJyO1xuICAgICAgLy9jYy5sb2coXCJTSUNCTyBFUlJPUlwiLCBtZXNzYWdlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zaG93Tm90aWZ5TWVzc2FnZShtZXNzYWdlKTtcbiAgICB9XG4gIH1cblxuICAvLyFTRUNUSU9OXG5cbiAgLy9TRUNUSU9OIHVzZXIgcHJvZmlsZVxuICBzaG93VXNlclByb2ZpbGVQb3BVcChwbGF5YWg6IEluc3RhbmNlVHlwZTx0eXBlb2YgUGxheWFoPikge1xuICAgIGlmIChwbGF5YWggPT0gbnVsbCkgcmV0dXJuO1xuICAgIFNpY2JvUG9ydGFsQWRhcHRlci5nZXRJbnN0YW5jZSgpLnNob3dVc2VyUHJvZmlsZVBvcFVwKHBsYXlhaCwgdGhpcy51c2VyUHJvZmlsZUJhY2tncm91bmQpO1xuICB9XG4gIC8vIVNFQ1RJT05cbn1cbiJdfQ==