
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboLoadingBackgroundAnimation.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '38e9c7y8DlM7aDGPUQUrYH5', 'SicboLoadingBackgroundAnimation');
// Game/Sicbo_New/Scripts/Components/SicboLoadingBackgroundAnimation.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboLoadingBackgroundAnimation = /** @class */ (function (_super) {
    __extends(SicboLoadingBackgroundAnimation, _super);
    function SicboLoadingBackgroundAnimation() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.OnBeginLoadResources = null;
        _this.OnCompleteLoadResources = null;
        return _this;
    }
    SicboLoadingBackgroundAnimation.prototype.start = function () {
        this.OnBeginLoadResources && this.OnBeginLoadResources();
    };
    SicboLoadingBackgroundAnimation.prototype.playFinishAnim = function (onPlayCompleted) {
        onPlayCompleted && onPlayCompleted();
    };
    SicboLoadingBackgroundAnimation.prototype.onSetProgress = function (fill) { };
    SicboLoadingBackgroundAnimation = __decorate([
        ccclass
    ], SicboLoadingBackgroundAnimation);
    return SicboLoadingBackgroundAnimation;
}(cc.Component));
exports.default = SicboLoadingBackgroundAnimation;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9Mb2FkaW5nQmFja2dyb3VuZEFuaW1hdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjtBQUc1RSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUE2RCxtREFBWTtJQUF6RTtRQUFBLHFFQWVDO1FBZFEsMEJBQW9CLEdBQWEsSUFBSSxDQUFDO1FBQ3RDLDZCQUF1QixHQUFhLElBQUksQ0FBQzs7SUFhbEQsQ0FBQztJQVhDLCtDQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsb0JBQW9CLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDN0QsQ0FBQztJQUVNLHdEQUFjLEdBQXJCLFVBQXNCLGVBQXlCO1FBQzdDLGVBQWUsSUFBSSxlQUFlLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBSU0sdURBQWEsR0FBcEIsVUFBcUIsSUFBWSxJQUFHLENBQUM7SUFkbEIsK0JBQStCO1FBRG5ELE9BQU87T0FDYSwrQkFBK0IsQ0FlbkQ7SUFBRCxzQ0FBQztDQWZELEFBZUMsQ0FmNEQsRUFBRSxDQUFDLFNBQVMsR0FleEU7a0JBZm9CLCtCQUErQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBTaWNib0dhbWVVdGlscyBmcm9tICcuLi9STkdDb21tb25zL1V0aWxzL1NpY2JvR2FtZVV0aWxzJztcbi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib0xvYWRpbmdCYWNrZ3JvdW5kQW5pbWF0aW9uIGV4dGVuZHMgY2MuQ29tcG9uZW50IHsgXG4gIHB1YmxpYyBPbkJlZ2luTG9hZFJlc291cmNlczogRnVuY3Rpb24gPSBudWxsO1xuICBwdWJsaWMgT25Db21wbGV0ZUxvYWRSZXNvdXJjZXM6IEZ1bmN0aW9uID0gbnVsbDtcblxuICBzdGFydCgpIHtcbiAgICAgIHRoaXMuT25CZWdpbkxvYWRSZXNvdXJjZXMgJiYgdGhpcy5PbkJlZ2luTG9hZFJlc291cmNlcygpO1xuICB9XG5cbiAgcHVibGljIHBsYXlGaW5pc2hBbmltKG9uUGxheUNvbXBsZXRlZDogRnVuY3Rpb24pIHtcbiAgICBvblBsYXlDb21wbGV0ZWQgJiYgb25QbGF5Q29tcGxldGVkKCk7XG4gIH1cblxuICBcblxuICBwdWJsaWMgb25TZXRQcm9ncmVzcyhmaWxsOiBudW1iZXIpIHt9XG59XG4iXX0=