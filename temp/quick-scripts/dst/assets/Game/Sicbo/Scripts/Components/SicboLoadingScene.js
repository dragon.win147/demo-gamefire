
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboLoadingScene.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0f8aessYx5KyrSSK6sS50QC', 'SicboLoadingScene');
// Game/Sicbo/Scripts/Components/SicboLoadingScene.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), EventKey = _a.EventKey, AutoJoinReply = _a.AutoJoinReply;
var SicboMessageHandler_1 = require("../SicboMessageHandler");
var SicboResourceManager_1 = require("../Managers/SicboResourceManager");
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboLoadingBackgroundAnimation_1 = require("./SicboLoadingBackgroundAnimation");
var SicboGameUtils_1 = require("../RNGCommons/Utils/SicboGameUtils");
var SicboText_1 = require("../Setting/SicboText");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboLoadingScene = /** @class */ (function (_super) {
    __extends(SicboLoadingScene, _super);
    function SicboLoadingScene() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.loadingAnimCmp = null;
        _this.sprProgress = null;
        _this.loadingLabel = null;
        _this._nameScene = "SicboScene";
        _this._callBack = null;
        return _this;
    }
    SicboLoadingScene.prototype.onLoad = function () {
        var _this = this;
        cc.systemEvent.on(EventKey.ON_DISCONNECT_EVENT, function () {
            //cc.log("Loading Scene ON_RECONNECTED");
            if (_this._nameScene != "") {
                _this.setDataLoadScene(_this._nameScene, _this._callBack);
            }
        }, this);
    };
    SicboLoadingScene.prototype.start = function () {
        this.setDataLoadScene(this._nameScene, this._callBack);
    };
    SicboLoadingScene.prototype.onDestroy = function () {
        this._nameScene = "";
        this._callBack = null;
    };
    SicboLoadingScene.prototype.setLoadingText = function (percent) {
        if (this.loadingLabel == null) {
            return;
        }
        percent = Math.ceil(Math.min(Math.max(percent * 100, 0), 100));
        if (percent >= this._percent) {
            this._percent = percent;
            this.loadingLabel.string = SicboGameUtils_1.default.FormatString(SicboText_1.default.loadingMsg, percent);
        }
    };
    SicboLoadingScene.prototype.setDataLoadScene = function (nameScene, callBack) {
        var _this = this;
        if (callBack === void 0) { callBack = null; }
        var self = this;
        this._callBack = callBack;
        self.onLoadProgress(SicboSetting_1.default.loadingBackgroundOverrideProgressBar);
        self._percent = 0;
        if (this.loadingAnimCmp == null)
            onSetData(null);
        else {
            this.loadingAnimCmp.OnBeginLoadResources = function () {
                onSetData(_this.loadingAnimCmp);
            };
            self.onSetProgress(0, SicboSetting_1.default.loadingBackgroundOverrideProgressBar, this.loadingAnimCmp);
        }
        var onSetData = function (backgroundAnimation) {
            var resourceLocalPaths = SicboSetting_1.default.resourceLocalPaths;
            var resourceRemotePaths = SicboSetting_1.default.resourceRemotePaths;
            var totalResourceLocal = resourceLocalPaths.length;
            var completedResourceLocal = 0;
            var totalResourceRemote = resourceRemotePaths.length;
            var completedResourceRemote = 0;
            var totalSceneResource = 100;
            var completedScene = 0;
            var totalData = 100;
            var completedLoadData = 0;
            var onPreLoadComplete = function () {
                var finish = function () {
                    SicboMessageHandler_1.default.getInstance().sendAutoJoinRequest(function (autoJoinReply) {
                        var channel = autoJoinReply.getChannel();
                        SicboSetting_1.default.setChipAndStatusDurationConfig(channel.getChipLevels(), channel.getStatusAndDuration());
                        SicboResourceManager_1.default.loadScene(nameScene, function () {
                            if (callBack) {
                                callBack();
                            }
                        });
                    });
                };
                if (backgroundAnimation == null)
                    finish();
                else {
                    backgroundAnimation.playFinishAnim(finish);
                }
            };
            var onProgress = function (completedCount, totalCount) {
                var percent = 0;
                if (totalCount > 0) {
                    percent = (100 * completedCount) / totalCount;
                    var progress = percent / 100;
                    self.onSetProgress(progress, SicboSetting_1.default.loadingBackgroundOverrideProgressBar, _this.loadingAnimCmp);
                }
                if (percent == 100) {
                    if (onPreLoadComplete)
                        onPreLoadComplete();
                }
            };
            var onProgressLoadData = function (completedCount, totalCount) {
                //cc.log("onProgressLoadData.. ", completedCount, totalCount);
                totalData = totalCount;
                completedLoadData = completedCount;
                onProgress(completedScene + completedResourceLocal + completedResourceRemote + completedLoadData, totalData + totalSceneResource + totalResourceLocal + totalResourceRemote);
            };
            var onProgressResourceLocal = function (completedCount, totalCount) {
                totalResourceLocal = totalCount;
                completedResourceLocal = completedCount;
                onProgress(completedScene + completedResourceLocal + completedResourceRemote + completedLoadData, totalData + totalSceneResource + totalResourceLocal + totalResourceRemote);
            };
            var onProgressResourceRemote = function (completedCount, totalCount) {
                totalResourceRemote = totalCount;
                completedResourceRemote = completedCount;
                onProgress(completedScene + completedResourceLocal + completedResourceRemote + completedLoadData, totalData + totalSceneResource + totalResourceLocal + totalResourceRemote);
            };
            var onProgressScene = function (completedCount, totalCount) {
                totalSceneResource = totalCount;
                completedScene = completedCount;
                onProgress(completedScene + completedResourceLocal + completedResourceRemote + completedLoadData, totalData + totalSceneResource + totalResourceLocal + totalResourceRemote);
            };
            _this.scheduleOnce(function () {
                var onLoadDataComplete = function () {
                    SicboResourceManager_1.default.preloadScene(nameScene, onProgressScene, function () { });
                    SicboResourceManager_1.default.preloadResourceLocal(resourceLocalPaths, onProgressResourceLocal, function () { });
                    SicboResourceManager_1.default.preloadResourceRemote(resourceRemotePaths, onProgressResourceRemote, function () { });
                };
                if (SicboSetting_1.default) {
                    SicboSetting_1.default.loadDataFromForge(onProgressLoadData, onLoadDataComplete);
                }
                else {
                    onProgressLoadData(0, 0);
                    onLoadDataComplete();
                }
            }, 0.3);
        };
    };
    SicboLoadingScene.prototype.onLoadProgress = function (override) {
        if (override === void 0) { override = false; }
        if (override) {
            this.sprProgress.node.active = false;
        }
    };
    SicboLoadingScene.prototype.onSetProgress = function (progress, override, loadingAnimCmp) {
        if (override === void 0) { override = false; }
        this.setLoadingText(progress);
        if (override && loadingAnimCmp) {
            loadingAnimCmp.onSetProgress(progress);
        }
        else {
            if (this.sprProgress && progress > this.sprProgress.progress) {
                this.sprProgress.progress = progress;
            }
        }
    };
    __decorate([
        property(SicboLoadingBackgroundAnimation_1.default)
    ], SicboLoadingScene.prototype, "loadingAnimCmp", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], SicboLoadingScene.prototype, "sprProgress", void 0);
    __decorate([
        property(cc.Label)
    ], SicboLoadingScene.prototype, "loadingLabel", void 0);
    SicboLoadingScene = __decorate([
        ccclass
    ], SicboLoadingScene);
    return SicboLoadingScene;
}(cc.Component));
exports.default = SicboLoadingScene;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvL1NjcmlwdHMvQ29tcG9uZW50cy9TaWNib0xvYWRpbmdTY2VuZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxxRUFBZ0U7QUFFMUQsSUFBQSxLQUE4Qiw0QkFBa0IsQ0FBQyxVQUFVLEVBQUUsRUFBM0QsUUFBUSxjQUFBLEVBQUUsYUFBYSxtQkFBb0MsQ0FBQztBQUVwRSw4REFBeUQ7QUFDekQseUVBQW9FO0FBQ3BFLHdEQUFtRDtBQUNuRCxxRkFBZ0Y7QUFDaEYscUVBQWdFO0FBQ2hFLGtEQUE2QztBQUV2QyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUU1QztJQUErQyxxQ0FBWTtJQUEzRDtRQUFBLHFFQStLQztRQTdLQyxvQkFBYyxHQUFvQyxJQUFJLENBQUM7UUFHdkQsaUJBQVcsR0FBbUIsSUFBSSxDQUFDO1FBR25DLGtCQUFZLEdBQWEsSUFBSSxDQUFDO1FBR3RCLGdCQUFVLEdBQVcsWUFBWSxDQUFDO1FBQ2xDLGVBQVMsR0FBZSxJQUFJLENBQUM7O0lBbUt2QyxDQUFDO0lBbEtDLGtDQUFNLEdBQU47UUFBQSxpQkFXQztRQVZDLEVBQUUsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUNmLFFBQVEsQ0FBQyxtQkFBbUIsRUFDNUI7WUFDRSx5Q0FBeUM7WUFDekMsSUFBSSxLQUFJLENBQUMsVUFBVSxJQUFJLEVBQUUsRUFBRTtnQkFDekIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3hEO1FBQ0gsQ0FBQyxFQUNELElBQUksQ0FDTCxDQUFDO0lBQ0osQ0FBQztJQUVELGlDQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUVELHFDQUFTLEdBQVQ7UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDO0lBRU8sMENBQWMsR0FBdEIsVUFBdUIsT0FBZTtRQUNwQyxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxFQUFFO1lBQzdCLE9BQU87U0FDUjtRQUNELE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDL0QsSUFBSSxPQUFPLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztZQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyx3QkFBYyxDQUFDLFlBQVksQ0FBQyxtQkFBUyxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztTQUN2RjtJQUNILENBQUM7SUFFRCw0Q0FBZ0IsR0FBaEIsVUFBaUIsU0FBaUIsRUFBRSxRQUEyQjtRQUEvRCxpQkErR0M7UUEvR21DLHlCQUFBLEVBQUEsZUFBMkI7UUFDN0QsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO1FBRTFCLElBQUksQ0FBQyxjQUFjLENBQUMsc0JBQVksQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1FBRWxCLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJO1lBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzVDO1lBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsR0FBRztnQkFDekMsU0FBUyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNqQyxDQUFDLENBQUM7WUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxzQkFBWSxDQUFDLG9DQUFvQyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUMvRjtRQUVELElBQUksU0FBUyxHQUFHLFVBQUMsbUJBQW9EO1lBQ25FLElBQUksa0JBQWtCLEdBQUcsc0JBQVksQ0FBQyxrQkFBa0IsQ0FBQztZQUN6RCxJQUFJLG1CQUFtQixHQUFHLHNCQUFZLENBQUMsbUJBQW1CLENBQUM7WUFFM0QsSUFBSSxrQkFBa0IsR0FBRyxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7WUFDbkQsSUFBSSxzQkFBc0IsR0FBVyxDQUFDLENBQUM7WUFFdkMsSUFBSSxtQkFBbUIsR0FBRyxtQkFBbUIsQ0FBQyxNQUFNLENBQUM7WUFDckQsSUFBSSx1QkFBdUIsR0FBVyxDQUFDLENBQUM7WUFFeEMsSUFBSSxrQkFBa0IsR0FBVyxHQUFHLENBQUM7WUFDckMsSUFBSSxjQUFjLEdBQVcsQ0FBQyxDQUFDO1lBRS9CLElBQUksU0FBUyxHQUFHLEdBQUcsQ0FBQztZQUNwQixJQUFJLGlCQUFpQixHQUFXLENBQUMsQ0FBQztZQUVsQyxJQUFJLGlCQUFpQixHQUFHO2dCQUN0QixJQUFJLE1BQU0sR0FBRztvQkFDWCw2QkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFDLGFBQWlEO3dCQUN0RyxJQUFJLE9BQU8sR0FBRyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7d0JBQ3pDLHNCQUFZLENBQUMsOEJBQThCLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxFQUFFLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUM7d0JBQ3JHLDhCQUFvQixDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUU7NEJBQ3hDLElBQUksUUFBUSxFQUFFO2dDQUNaLFFBQVEsRUFBRSxDQUFDOzZCQUNaO3dCQUNILENBQUMsQ0FBQyxDQUFDO29CQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQztnQkFFRixJQUFJLG1CQUFtQixJQUFJLElBQUk7b0JBQUUsTUFBTSxFQUFFLENBQUM7cUJBQ3JDO29CQUNILG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDNUM7WUFDSCxDQUFDLENBQUM7WUFDRixJQUFJLFVBQVUsR0FBRyxVQUFDLGNBQWMsRUFBRSxVQUFVO2dCQUMxQyxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUM7Z0JBQ2hCLElBQUksVUFBVSxHQUFHLENBQUMsRUFBRTtvQkFDbEIsT0FBTyxHQUFHLENBQUMsR0FBRyxHQUFHLGNBQWMsQ0FBQyxHQUFHLFVBQVUsQ0FBQztvQkFDOUMsSUFBSSxRQUFRLEdBQUcsT0FBTyxHQUFHLEdBQUcsQ0FBQztvQkFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsc0JBQVksQ0FBQyxvQ0FBb0MsRUFBRSxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7aUJBQ3RHO2dCQUNELElBQUksT0FBTyxJQUFJLEdBQUcsRUFBRTtvQkFDbEIsSUFBSSxpQkFBaUI7d0JBQUUsaUJBQWlCLEVBQUUsQ0FBQztpQkFDNUM7WUFDSCxDQUFDLENBQUM7WUFDRixJQUFJLGtCQUFrQixHQUFHLFVBQUMsY0FBYyxFQUFFLFVBQVU7Z0JBQ2xELDhEQUE4RDtnQkFDOUQsU0FBUyxHQUFHLFVBQVUsQ0FBQztnQkFDdkIsaUJBQWlCLEdBQUcsY0FBYyxDQUFDO2dCQUNuQyxVQUFVLENBQ1IsY0FBYyxHQUFHLHNCQUFzQixHQUFHLHVCQUF1QixHQUFHLGlCQUFpQixFQUNyRixTQUFTLEdBQUcsa0JBQWtCLEdBQUcsa0JBQWtCLEdBQUcsbUJBQW1CLENBQzFFLENBQUM7WUFDSixDQUFDLENBQUM7WUFFRixJQUFJLHVCQUF1QixHQUFHLFVBQUMsY0FBYyxFQUFFLFVBQVU7Z0JBQ3ZELGtCQUFrQixHQUFHLFVBQVUsQ0FBQztnQkFDaEMsc0JBQXNCLEdBQUcsY0FBYyxDQUFDO2dCQUN4QyxVQUFVLENBQ1IsY0FBYyxHQUFHLHNCQUFzQixHQUFHLHVCQUF1QixHQUFHLGlCQUFpQixFQUNyRixTQUFTLEdBQUcsa0JBQWtCLEdBQUcsa0JBQWtCLEdBQUcsbUJBQW1CLENBQzFFLENBQUM7WUFDSixDQUFDLENBQUM7WUFFRixJQUFJLHdCQUF3QixHQUFHLFVBQUMsY0FBYyxFQUFFLFVBQVU7Z0JBQ3hELG1CQUFtQixHQUFHLFVBQVUsQ0FBQztnQkFDakMsdUJBQXVCLEdBQUcsY0FBYyxDQUFDO2dCQUN6QyxVQUFVLENBQ1IsY0FBYyxHQUFHLHNCQUFzQixHQUFHLHVCQUF1QixHQUFHLGlCQUFpQixFQUNyRixTQUFTLEdBQUcsa0JBQWtCLEdBQUcsa0JBQWtCLEdBQUcsbUJBQW1CLENBQzFFLENBQUM7WUFDSixDQUFDLENBQUM7WUFFRixJQUFJLGVBQWUsR0FBRyxVQUFDLGNBQWMsRUFBRSxVQUFVO2dCQUMvQyxrQkFBa0IsR0FBRyxVQUFVLENBQUM7Z0JBQ2hDLGNBQWMsR0FBRyxjQUFjLENBQUM7Z0JBQ2hDLFVBQVUsQ0FDUixjQUFjLEdBQUcsc0JBQXNCLEdBQUcsdUJBQXVCLEdBQUcsaUJBQWlCLEVBQ3JGLFNBQVMsR0FBRyxrQkFBa0IsR0FBRyxrQkFBa0IsR0FBRyxtQkFBbUIsQ0FDMUUsQ0FBQztZQUNKLENBQUMsQ0FBQztZQUVGLEtBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2hCLElBQUksa0JBQWtCLEdBQUc7b0JBQ3ZCLDhCQUFvQixDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsZUFBZSxFQUFFLGNBQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ3hFLDhCQUFvQixDQUFDLG9CQUFvQixDQUFDLGtCQUFrQixFQUFFLHVCQUF1QixFQUFFLGNBQWEsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZHLDhCQUFvQixDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixFQUFFLHdCQUF3QixFQUFFLGNBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBQzVHLENBQUMsQ0FBQztnQkFDRixJQUFJLHNCQUFZLEVBQUU7b0JBQ2hCLHNCQUFZLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztpQkFDeEU7cUJBQU07b0JBQ0wsa0JBQWtCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUN6QixrQkFBa0IsRUFBRSxDQUFDO2lCQUN0QjtZQUNILENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNWLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCwwQ0FBYyxHQUFkLFVBQWUsUUFBeUI7UUFBekIseUJBQUEsRUFBQSxnQkFBeUI7UUFDdEMsSUFBSSxRQUFRLEVBQUU7WUFDWixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ3RDO0lBQ0gsQ0FBQztJQUVELHlDQUFhLEdBQWIsVUFBYyxRQUFnQixFQUFFLFFBQXlCLEVBQUUsY0FBK0M7UUFBMUUseUJBQUEsRUFBQSxnQkFBeUI7UUFDdkQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM5QixJQUFJLFFBQVEsSUFBSSxjQUFjLEVBQUU7WUFDOUIsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN4QzthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRTtnQkFDNUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO2FBQ3RDO1NBQ0Y7SUFDSCxDQUFDO0lBNUtEO1FBREMsUUFBUSxDQUFDLHlDQUErQixDQUFDOzZEQUNhO0lBR3ZEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7MERBQ1U7SUFHbkM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzsyREFDVztJQVJYLGlCQUFpQjtRQURyQyxPQUFPO09BQ2EsaUJBQWlCLENBK0tyQztJQUFELHdCQUFDO0NBL0tELEFBK0tDLENBL0s4QyxFQUFFLENBQUMsU0FBUyxHQStLMUQ7a0JBL0tvQixpQkFBaUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU2ljYm9Nb2R1bGVBZGFwdGVyIGZyb20gXCIuLi8uLi8uLi8uLi9TaWNib01vZHVsZUFkYXB0ZXJcIjtcblxuY29uc3QgeyBFdmVudEtleSwgQXV0b0pvaW5SZXBseSB9ID0gU2ljYm9Nb2R1bGVBZGFwdGVyLmdldEFsbFJlZnMoKTtcblxuaW1wb3J0IFNpY2JvTWVzc2FnZUhhbmRsZXIgZnJvbSBcIi4uL1NpY2JvTWVzc2FnZUhhbmRsZXJcIjtcbmltcG9ydCBTaWNib1Jlc291cmNlTWFuYWdlciBmcm9tIFwiLi4vTWFuYWdlcnMvU2ljYm9SZXNvdXJjZU1hbmFnZXJcIjtcbmltcG9ydCBTaWNib1NldHRpbmcgZnJvbSBcIi4uL1NldHRpbmcvU2ljYm9TZXR0aW5nXCI7XG5pbXBvcnQgU2ljYm9Mb2FkaW5nQmFja2dyb3VuZEFuaW1hdGlvbiBmcm9tIFwiLi9TaWNib0xvYWRpbmdCYWNrZ3JvdW5kQW5pbWF0aW9uXCI7XG5pbXBvcnQgU2ljYm9HYW1lVXRpbHMgZnJvbSBcIi4uL1JOR0NvbW1vbnMvVXRpbHMvU2ljYm9HYW1lVXRpbHNcIjtcbmltcG9ydCBTaWNib1RleHQgZnJvbSBcIi4uL1NldHRpbmcvU2ljYm9UZXh0XCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9Mb2FkaW5nU2NlbmUgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICBAcHJvcGVydHkoU2ljYm9Mb2FkaW5nQmFja2dyb3VuZEFuaW1hdGlvbilcbiAgbG9hZGluZ0FuaW1DbXA6IFNpY2JvTG9hZGluZ0JhY2tncm91bmRBbmltYXRpb24gPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5Qcm9ncmVzc0JhcilcbiAgc3ByUHJvZ3Jlc3M6IGNjLlByb2dyZXNzQmFyID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gIGxvYWRpbmdMYWJlbDogY2MuTGFiZWwgPSBudWxsO1xuXG4gIHByaXZhdGUgX3BlcmNlbnQ6IG51bWJlcjtcbiAgcHJpdmF0ZSBfbmFtZVNjZW5lOiBzdHJpbmcgPSBcIlNpY2JvU2NlbmVcIjtcbiAgcHJpdmF0ZSBfY2FsbEJhY2s6ICgpID0+IHZvaWQgPSBudWxsO1xuICBvbkxvYWQoKSB7XG4gICAgY2Muc3lzdGVtRXZlbnQub24oXG4gICAgICBFdmVudEtleS5PTl9ESVNDT05ORUNUX0VWRU5ULFxuICAgICAgKCkgPT4ge1xuICAgICAgICAvL2NjLmxvZyhcIkxvYWRpbmcgU2NlbmUgT05fUkVDT05ORUNURURcIik7XG4gICAgICAgIGlmICh0aGlzLl9uYW1lU2NlbmUgIT0gXCJcIikge1xuICAgICAgICAgIHRoaXMuc2V0RGF0YUxvYWRTY2VuZSh0aGlzLl9uYW1lU2NlbmUsIHRoaXMuX2NhbGxCYWNrKTtcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHRoaXNcbiAgICApO1xuICB9XG5cbiAgc3RhcnQoKSB7XG4gICAgdGhpcy5zZXREYXRhTG9hZFNjZW5lKHRoaXMuX25hbWVTY2VuZSwgdGhpcy5fY2FsbEJhY2spO1xuICB9XG5cbiAgb25EZXN0cm95KCkge1xuICAgIHRoaXMuX25hbWVTY2VuZSA9IFwiXCI7XG4gICAgdGhpcy5fY2FsbEJhY2sgPSBudWxsO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRMb2FkaW5nVGV4dChwZXJjZW50OiBudW1iZXIpIHtcbiAgICBpZiAodGhpcy5sb2FkaW5nTGFiZWwgPT0gbnVsbCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBwZXJjZW50ID0gTWF0aC5jZWlsKE1hdGgubWluKE1hdGgubWF4KHBlcmNlbnQgKiAxMDAsIDApLCAxMDApKTtcbiAgICBpZiAocGVyY2VudCA+PSB0aGlzLl9wZXJjZW50KSB7XG4gICAgICB0aGlzLl9wZXJjZW50ID0gcGVyY2VudDtcbiAgICAgIHRoaXMubG9hZGluZ0xhYmVsLnN0cmluZyA9IFNpY2JvR2FtZVV0aWxzLkZvcm1hdFN0cmluZyhTaWNib1RleHQubG9hZGluZ01zZywgcGVyY2VudCk7XG4gICAgfVxuICB9XG5cbiAgc2V0RGF0YUxvYWRTY2VuZShuYW1lU2NlbmU6IHN0cmluZywgY2FsbEJhY2s6ICgpID0+IHZvaWQgPSBudWxsKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgdGhpcy5fY2FsbEJhY2sgPSBjYWxsQmFjaztcblxuICAgIHNlbGYub25Mb2FkUHJvZ3Jlc3MoU2ljYm9TZXR0aW5nLmxvYWRpbmdCYWNrZ3JvdW5kT3ZlcnJpZGVQcm9ncmVzc0Jhcik7XG4gICAgc2VsZi5fcGVyY2VudCA9IDA7XG5cbiAgICBpZiAodGhpcy5sb2FkaW5nQW5pbUNtcCA9PSBudWxsKSBvblNldERhdGEobnVsbCk7XG4gICAgZWxzZSB7XG4gICAgICB0aGlzLmxvYWRpbmdBbmltQ21wLk9uQmVnaW5Mb2FkUmVzb3VyY2VzID0gKCkgPT4ge1xuICAgICAgICBvblNldERhdGEodGhpcy5sb2FkaW5nQW5pbUNtcCk7XG4gICAgICB9O1xuICAgICAgc2VsZi5vblNldFByb2dyZXNzKDAsIFNpY2JvU2V0dGluZy5sb2FkaW5nQmFja2dyb3VuZE92ZXJyaWRlUHJvZ3Jlc3NCYXIsIHRoaXMubG9hZGluZ0FuaW1DbXApO1xuICAgIH1cblxuICAgIHZhciBvblNldERhdGEgPSAoYmFja2dyb3VuZEFuaW1hdGlvbjogU2ljYm9Mb2FkaW5nQmFja2dyb3VuZEFuaW1hdGlvbikgPT4ge1xuICAgICAgbGV0IHJlc291cmNlTG9jYWxQYXRocyA9IFNpY2JvU2V0dGluZy5yZXNvdXJjZUxvY2FsUGF0aHM7XG4gICAgICBsZXQgcmVzb3VyY2VSZW1vdGVQYXRocyA9IFNpY2JvU2V0dGluZy5yZXNvdXJjZVJlbW90ZVBhdGhzO1xuXG4gICAgICBsZXQgdG90YWxSZXNvdXJjZUxvY2FsID0gcmVzb3VyY2VMb2NhbFBhdGhzLmxlbmd0aDtcbiAgICAgIGxldCBjb21wbGV0ZWRSZXNvdXJjZUxvY2FsOiBudW1iZXIgPSAwO1xuXG4gICAgICBsZXQgdG90YWxSZXNvdXJjZVJlbW90ZSA9IHJlc291cmNlUmVtb3RlUGF0aHMubGVuZ3RoO1xuICAgICAgbGV0IGNvbXBsZXRlZFJlc291cmNlUmVtb3RlOiBudW1iZXIgPSAwO1xuXG4gICAgICBsZXQgdG90YWxTY2VuZVJlc291cmNlOiBudW1iZXIgPSAxMDA7XG4gICAgICBsZXQgY29tcGxldGVkU2NlbmU6IG51bWJlciA9IDA7XG5cbiAgICAgIGxldCB0b3RhbERhdGEgPSAxMDA7XG4gICAgICBsZXQgY29tcGxldGVkTG9hZERhdGE6IG51bWJlciA9IDA7XG5cbiAgICAgIHZhciBvblByZUxvYWRDb21wbGV0ZSA9ICgpID0+IHtcbiAgICAgICAgbGV0IGZpbmlzaCA9ICgpID0+IHtcbiAgICAgICAgICBTaWNib01lc3NhZ2VIYW5kbGVyLmdldEluc3RhbmNlKCkuc2VuZEF1dG9Kb2luUmVxdWVzdCgoYXV0b0pvaW5SZXBseTogSW5zdGFuY2VUeXBlPHR5cGVvZiBBdXRvSm9pblJlcGx5PikgPT4ge1xuICAgICAgICAgICAgbGV0IGNoYW5uZWwgPSBhdXRvSm9pblJlcGx5LmdldENoYW5uZWwoKTtcbiAgICAgICAgICAgIFNpY2JvU2V0dGluZy5zZXRDaGlwQW5kU3RhdHVzRHVyYXRpb25Db25maWcoY2hhbm5lbC5nZXRDaGlwTGV2ZWxzKCksIGNoYW5uZWwuZ2V0U3RhdHVzQW5kRHVyYXRpb24oKSk7XG4gICAgICAgICAgICBTaWNib1Jlc291cmNlTWFuYWdlci5sb2FkU2NlbmUobmFtZVNjZW5lLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIGlmIChjYWxsQmFjaykge1xuICAgICAgICAgICAgICAgIGNhbGxCYWNrKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgIGlmIChiYWNrZ3JvdW5kQW5pbWF0aW9uID09IG51bGwpIGZpbmlzaCgpO1xuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBiYWNrZ3JvdW5kQW5pbWF0aW9uLnBsYXlGaW5pc2hBbmltKGZpbmlzaCk7XG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgICB2YXIgb25Qcm9ncmVzcyA9IChjb21wbGV0ZWRDb3VudCwgdG90YWxDb3VudCkgPT4ge1xuICAgICAgICB2YXIgcGVyY2VudCA9IDA7XG4gICAgICAgIGlmICh0b3RhbENvdW50ID4gMCkge1xuICAgICAgICAgIHBlcmNlbnQgPSAoMTAwICogY29tcGxldGVkQ291bnQpIC8gdG90YWxDb3VudDtcbiAgICAgICAgICB2YXIgcHJvZ3Jlc3MgPSBwZXJjZW50IC8gMTAwO1xuICAgICAgICAgIHNlbGYub25TZXRQcm9ncmVzcyhwcm9ncmVzcywgU2ljYm9TZXR0aW5nLmxvYWRpbmdCYWNrZ3JvdW5kT3ZlcnJpZGVQcm9ncmVzc0JhciwgdGhpcy5sb2FkaW5nQW5pbUNtcCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHBlcmNlbnQgPT0gMTAwKSB7XG4gICAgICAgICAgaWYgKG9uUHJlTG9hZENvbXBsZXRlKSBvblByZUxvYWRDb21wbGV0ZSgpO1xuICAgICAgICB9XG4gICAgICB9O1xuICAgICAgdmFyIG9uUHJvZ3Jlc3NMb2FkRGF0YSA9IChjb21wbGV0ZWRDb3VudCwgdG90YWxDb3VudCkgPT4ge1xuICAgICAgICAvL2NjLmxvZyhcIm9uUHJvZ3Jlc3NMb2FkRGF0YS4uIFwiLCBjb21wbGV0ZWRDb3VudCwgdG90YWxDb3VudCk7XG4gICAgICAgIHRvdGFsRGF0YSA9IHRvdGFsQ291bnQ7XG4gICAgICAgIGNvbXBsZXRlZExvYWREYXRhID0gY29tcGxldGVkQ291bnQ7XG4gICAgICAgIG9uUHJvZ3Jlc3MoXG4gICAgICAgICAgY29tcGxldGVkU2NlbmUgKyBjb21wbGV0ZWRSZXNvdXJjZUxvY2FsICsgY29tcGxldGVkUmVzb3VyY2VSZW1vdGUgKyBjb21wbGV0ZWRMb2FkRGF0YSxcbiAgICAgICAgICB0b3RhbERhdGEgKyB0b3RhbFNjZW5lUmVzb3VyY2UgKyB0b3RhbFJlc291cmNlTG9jYWwgKyB0b3RhbFJlc291cmNlUmVtb3RlXG4gICAgICAgICk7XG4gICAgICB9O1xuXG4gICAgICB2YXIgb25Qcm9ncmVzc1Jlc291cmNlTG9jYWwgPSAoY29tcGxldGVkQ291bnQsIHRvdGFsQ291bnQpID0+IHtcbiAgICAgICAgdG90YWxSZXNvdXJjZUxvY2FsID0gdG90YWxDb3VudDtcbiAgICAgICAgY29tcGxldGVkUmVzb3VyY2VMb2NhbCA9IGNvbXBsZXRlZENvdW50O1xuICAgICAgICBvblByb2dyZXNzKFxuICAgICAgICAgIGNvbXBsZXRlZFNjZW5lICsgY29tcGxldGVkUmVzb3VyY2VMb2NhbCArIGNvbXBsZXRlZFJlc291cmNlUmVtb3RlICsgY29tcGxldGVkTG9hZERhdGEsXG4gICAgICAgICAgdG90YWxEYXRhICsgdG90YWxTY2VuZVJlc291cmNlICsgdG90YWxSZXNvdXJjZUxvY2FsICsgdG90YWxSZXNvdXJjZVJlbW90ZVxuICAgICAgICApO1xuICAgICAgfTtcblxuICAgICAgdmFyIG9uUHJvZ3Jlc3NSZXNvdXJjZVJlbW90ZSA9IChjb21wbGV0ZWRDb3VudCwgdG90YWxDb3VudCkgPT4ge1xuICAgICAgICB0b3RhbFJlc291cmNlUmVtb3RlID0gdG90YWxDb3VudDtcbiAgICAgICAgY29tcGxldGVkUmVzb3VyY2VSZW1vdGUgPSBjb21wbGV0ZWRDb3VudDtcbiAgICAgICAgb25Qcm9ncmVzcyhcbiAgICAgICAgICBjb21wbGV0ZWRTY2VuZSArIGNvbXBsZXRlZFJlc291cmNlTG9jYWwgKyBjb21wbGV0ZWRSZXNvdXJjZVJlbW90ZSArIGNvbXBsZXRlZExvYWREYXRhLFxuICAgICAgICAgIHRvdGFsRGF0YSArIHRvdGFsU2NlbmVSZXNvdXJjZSArIHRvdGFsUmVzb3VyY2VMb2NhbCArIHRvdGFsUmVzb3VyY2VSZW1vdGVcbiAgICAgICAgKTtcbiAgICAgIH07XG5cbiAgICAgIHZhciBvblByb2dyZXNzU2NlbmUgPSAoY29tcGxldGVkQ291bnQsIHRvdGFsQ291bnQpID0+IHtcbiAgICAgICAgdG90YWxTY2VuZVJlc291cmNlID0gdG90YWxDb3VudDtcbiAgICAgICAgY29tcGxldGVkU2NlbmUgPSBjb21wbGV0ZWRDb3VudDtcbiAgICAgICAgb25Qcm9ncmVzcyhcbiAgICAgICAgICBjb21wbGV0ZWRTY2VuZSArIGNvbXBsZXRlZFJlc291cmNlTG9jYWwgKyBjb21wbGV0ZWRSZXNvdXJjZVJlbW90ZSArIGNvbXBsZXRlZExvYWREYXRhLFxuICAgICAgICAgIHRvdGFsRGF0YSArIHRvdGFsU2NlbmVSZXNvdXJjZSArIHRvdGFsUmVzb3VyY2VMb2NhbCArIHRvdGFsUmVzb3VyY2VSZW1vdGVcbiAgICAgICAgKTtcbiAgICAgIH07XG5cbiAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgbGV0IG9uTG9hZERhdGFDb21wbGV0ZSA9ICgpID0+IHtcbiAgICAgICAgICBTaWNib1Jlc291cmNlTWFuYWdlci5wcmVsb2FkU2NlbmUobmFtZVNjZW5lLCBvblByb2dyZXNzU2NlbmUsICgpID0+IHt9KTtcbiAgICAgICAgICBTaWNib1Jlc291cmNlTWFuYWdlci5wcmVsb2FkUmVzb3VyY2VMb2NhbChyZXNvdXJjZUxvY2FsUGF0aHMsIG9uUHJvZ3Jlc3NSZXNvdXJjZUxvY2FsLCBmdW5jdGlvbiAoKSB7fSk7XG4gICAgICAgICAgU2ljYm9SZXNvdXJjZU1hbmFnZXIucHJlbG9hZFJlc291cmNlUmVtb3RlKHJlc291cmNlUmVtb3RlUGF0aHMsIG9uUHJvZ3Jlc3NSZXNvdXJjZVJlbW90ZSwgZnVuY3Rpb24gKCkge30pO1xuICAgICAgICB9O1xuICAgICAgICBpZiAoU2ljYm9TZXR0aW5nKSB7XG4gICAgICAgICAgU2ljYm9TZXR0aW5nLmxvYWREYXRhRnJvbUZvcmdlKG9uUHJvZ3Jlc3NMb2FkRGF0YSwgb25Mb2FkRGF0YUNvbXBsZXRlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBvblByb2dyZXNzTG9hZERhdGEoMCwgMCk7XG4gICAgICAgICAgb25Mb2FkRGF0YUNvbXBsZXRlKCk7XG4gICAgICAgIH1cbiAgICAgIH0sIDAuMyk7XG4gICAgfTtcbiAgfVxuXG4gIG9uTG9hZFByb2dyZXNzKG92ZXJyaWRlOiBib29sZWFuID0gZmFsc2UpIHtcbiAgICBpZiAob3ZlcnJpZGUpIHtcbiAgICAgIHRoaXMuc3ByUHJvZ3Jlc3Mubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG4gIH1cblxuICBvblNldFByb2dyZXNzKHByb2dyZXNzOiBudW1iZXIsIG92ZXJyaWRlOiBib29sZWFuID0gZmFsc2UsIGxvYWRpbmdBbmltQ21wOiBTaWNib0xvYWRpbmdCYWNrZ3JvdW5kQW5pbWF0aW9uKSB7XG4gICAgdGhpcy5zZXRMb2FkaW5nVGV4dChwcm9ncmVzcyk7XG4gICAgaWYgKG92ZXJyaWRlICYmIGxvYWRpbmdBbmltQ21wKSB7XG4gICAgICBsb2FkaW5nQW5pbUNtcC5vblNldFByb2dyZXNzKHByb2dyZXNzKTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHRoaXMuc3ByUHJvZ3Jlc3MgJiYgcHJvZ3Jlc3MgPiB0aGlzLnNwclByb2dyZXNzLnByb2dyZXNzKSB7XG4gICAgICAgIHRoaXMuc3ByUHJvZ3Jlc3MucHJvZ3Jlc3MgPSBwcm9ncmVzcztcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==