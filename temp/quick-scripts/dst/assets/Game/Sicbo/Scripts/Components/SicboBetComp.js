
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboBetComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '00b4doTeWFIH5ganGHt9kdY', 'SicboBetComp');
// Game/Sicbo_New/Scripts/Components/SicboBetComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Door = _a.Door, State = _a.State, DoorAndBetAmount = _a.DoorAndBetAmount, Status = _a.Status;
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboUIComp_1 = require("./SicboUIComp");
var SicboHelper_1 = require("../Helpers/SicboHelper");
var SicboCoinItemComp_1 = require("./Items/SicboCoinItemComp");
var SicboBetSlotItemComp_1 = require("./Items/SicboBetSlotItemComp");
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboController_1 = require("../Controllers/SicboController");
var SicboScrollViewWithButton_1 = require("../RNGCommons/UIComponents/SicboScrollViewWithButton");
var SicboGameUtils_1 = require("../RNGCommons/Utils/SicboGameUtils");
var SicboCoinType_1 = require("../RNGCommons/SicboCoinType");
var SicboBetComp = /** @class */ (function (_super) {
    __extends(SicboBetComp, _super);
    function SicboBetComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sicboUIComp = null;
        _this.chipsScrollView = null;
        _this.currentSelectedChip = null;
        _this.largeChipPrefab = null;
        _this.rebetButton = null;
        _this.doubleBetButton = null;
        _this.betSlotsNode = null;
        _this.allBetSlots = [];
        _this.miniChipRoot = null;
        _this.miniChipPool = null;
        _this.loseChipTarget = null;
        _this.winChipPos = null;
        _this._disableChip = false;
        _this.chip_data = [];
        _this.chipItems = [];
        //ANCHOR chip stack pos config
        _this.chipStackPosConfig = {
            anyTrippleCloneSpacing: 30,
            singleCloneSpacing: 3,
            smallCloneSpacing: 40,
            bigCloneSpacing: -40,
            cloneSpacing: 4,
            chipSpacing: 4,
            chipStackOffsetRight: new cc.Vec2(-27, 25),
            chipStackOffsetLeft: new cc.Vec2(27, 25),
            anyTripple: [
                {
                    pos: new cc.Vec2(-84, -34),
                    height: 10,
                },
                {
                    pos: new cc.Vec2(-56, -33),
                    height: 10,
                }
            ],
            trippleLeft: [
                {
                    pos: new cc.Vec2(-82, -8),
                    height: 10,
                },
            ],
            trippleRight: [
                {
                    pos: new cc.Vec2(82, -8),
                    height: 10,
                },
            ],
            small: [
                {
                    pos: new cc.Vec2(-75.5, -50),
                    height: 10,
                },
                {
                    pos: new cc.Vec2(-46, -51),
                    height: 10,
                },
            ],
            big: [
                {
                    pos: new cc.Vec2(76.5, -51),
                    height: 10,
                },
                {
                    pos: new cc.Vec2(47, -50),
                    height: 10,
                },
            ],
            four_sum: [
                {
                    pos: new cc.Vec2(-20, -23.5),
                    height: 10,
                },
            ],
            five_sum: [
                {
                    pos: new cc.Vec2(-20, -23.5),
                    height: 10,
                },
            ],
            six_sum: [
                {
                    pos: new cc.Vec2(-23, -23.5),
                    height: 10,
                },
            ],
            seven_sum: [
                {
                    pos: new cc.Vec2(-20, -23.5),
                    height: 10,
                },
            ],
            eight_sum: [
                {
                    pos: new cc.Vec2(-20, -23.5),
                    height: 10,
                },
            ],
            nine_sum: [
                {
                    pos: new cc.Vec2(-19, -23.5),
                    height: 10,
                },
            ],
            ten_sum: [
                {
                    pos: new cc.Vec2(-19, -23.5),
                    height: 10,
                },
            ],
            eleven_sum: [
                {
                    pos: new cc.Vec2(-18, -23.5),
                    height: 10,
                },
            ],
            twelve_sum: [
                {
                    pos: new cc.Vec2(-18, -23.5),
                    height: 10,
                },
            ],
            thirteen_sum: [
                {
                    pos: new cc.Vec2(-16.5, -23.5),
                    height: 10,
                },
            ],
            fourteen_sum: [
                {
                    pos: new cc.Vec2(-16, -23.5),
                    height: 10,
                },
            ],
            fifteen_sum: [
                {
                    pos: new cc.Vec2(-15.5, -23.5),
                    height: 10,
                },
            ],
            sixteen_sum: [
                {
                    pos: new cc.Vec2(-15.5, -23.5),
                    height: 10,
                },
            ],
            seventeen_sum: [
                {
                    pos: new cc.Vec2(-14.5, -23.5),
                    height: 10,
                },
            ],
            single: [
                {
                    pos: new cc.Vec2(-57.5, -22),
                    height: 10,
                },
            ],
        };
        return _this;
    }
    SicboBetComp.prototype.onLoad = function () {
        this.sicboUIComp = this.node.parent.getComponentInChildren(SicboUIComp_1.default);
        this.allBetSlots = this.betSlotsNode.getComponentsInChildren(SicboBetSlotItemComp_1.default);
        this.miniChipPool = new cc.NodePool();
    };
    SicboBetComp.prototype.getDoor = function (door) {
        return this.allBetSlots.find(function (x) { return x.door == door; });
    };
    //SECTION Chip scroll view
    SicboBetComp.prototype.createChipItems = function (data) {
        var _a;
        this.chip_data = data;
        this.createItemChipCoins(data);
        this.selectChip(this.chipItems[0], false);
        this.chipsScrollView.scrollToTopLeft();
        (_a = this.chipsScrollView.getComponent(SicboScrollViewWithButton_1.default)) === null || _a === void 0 ? void 0 : _a.checkScrollViewButton();
    };
    SicboBetComp.prototype.createItemChipCoins = function (data) {
        var _this = this;
        data.forEach(function (chipValue) {
            _this.createItemCoin(chipValue);
        });
    };
    SicboBetComp.prototype.createItemCoin = function (chipValue) {
        var _this = this;
        var coinType = SicboHelper_1.default.convertValueToCoinType(chipValue);
        var newItem = SicboGameUtils_1.default.createItemFromPrefab(SicboCoinItemComp_1.default, this.largeChipPrefab, this.chipsScrollView.content);
        var itemData = new SicboCoinItemComp_1.SicboCoinItemCompData();
        itemData.coinType = coinType;
        itemData.onClick = function (sicboCoinItemComp) {
            _this.selectChip(sicboCoinItemComp);
        };
        newItem.setData(itemData);
        newItem.isDisable = this.disableChip;
        this.chipItems.push(newItem);
    };
    SicboBetComp.prototype.selectChip = function (sicboCoinItemComp, playSfx) {
        if (playSfx === void 0) { playSfx = true; }
        if (this.disableChip)
            return;
        if (this.currentSelectedChip != null)
            this.currentSelectedChip.selectEff(false);
        this.currentSelectedChip = sicboCoinItemComp;
        this.currentSelectedChip.selectEff(true);
        //TODO: change sound khi bam chip binh thuong va disable
        if (playSfx)
            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxClick);
    };
    SicboBetComp.prototype.getSelectedChipType = function () {
        return this.currentSelectedChip.data.coinType;
    };
    SicboBetComp.prototype.onClickChipScrollNext = function () {
        var numOfItem = this.chipsScrollView.content.childrenCount;
        var spacing = this.chipsScrollView.content.width / numOfItem;
        var offset = this.chipsScrollView.getScrollOffset();
        var maxOffset = this.chipsScrollView.getMaxScrollOffset();
        this.chipsScrollView.scrollToPercentHorizontal((Math.abs(offset.x) + spacing) / maxOffset.x, .5);
    };
    SicboBetComp.prototype.onClickChipScrollPre = function () {
        var numOfItem = this.chipsScrollView.content.childrenCount;
        var spacing = this.chipsScrollView.content.width / numOfItem;
        var offset = this.chipsScrollView.getScrollOffset();
        var maxOffset = this.chipsScrollView.getMaxScrollOffset();
        this.chipsScrollView.scrollToPercentHorizontal((Math.abs(offset.x) - spacing) / maxOffset.x, .5);
    };
    SicboBetComp.prototype.scrollToInsideView = function (chip) {
        var _a;
        this.chipsScrollView.stopAutoScroll();
        var pos = SicboGameUtils_1.default.convertToOtherNode(chip, this.chipsScrollView.node);
        if (pos.x >= -230 && pos.x <= 200)
            return; // Inside view
        var offset = this.chipsScrollView.getScrollOffset();
        var maxOffset = this.chipsScrollView.getMaxScrollOffset();
        this.chipsScrollView.scrollToPercentHorizontal((Math.abs(offset.x) + pos.x) / maxOffset.x);
        (_a = this.chipsScrollView.getComponent(SicboScrollViewWithButton_1.default)) === null || _a === void 0 ? void 0 : _a.checkScrollViewButton();
    };
    //!SECTION
    //#region BET IN TABLE
    /**
    * Bam vao o dat cuoc tren ban
        */
    SicboBetComp.prototype.onClickBetSlotItem = function (betSlotItemComp) {
        if (this.disableChip)
            return;
        this.scrollToInsideView(this.currentSelectedChip.node);
        this.sicboUIComp.myUserBet(betSlotItemComp.door, this.currentSelectedChip.data.coinType);
        // this.putMiniChipOnTableImmediately(this.currentSelectedChip.data.coinType, betSlotItemComp);
    };
    SicboBetComp.prototype.createItemCoinMini = function (coinType) {
        var newItem = null;
        if (this.miniChipPool.size() > 0) {
            newItem = this.miniChipPool.get().getComponent("SicboCoinItemComp");
        }
        else {
            newItem = SicboGameUtils_1.default.createItemFromPrefab(SicboCoinItemComp_1.default, this.largeChipPrefab, this.chipsScrollView.content);
        }
        newItem.node.parent = this.miniChipRoot;
        newItem.node.scale = 1;
        newItem.node.opacity = 255;
        var itemData = new SicboCoinItemComp_1.SicboCoinItemCompData();
        itemData.coinType = coinType;
        itemData.isMini = true;
        newItem.setData(itemData);
        // newItem.rotation = -this.randomRange(0, 360);
        //ANCHOR minichip Scale
        // newItem.isOnTable = true;
        return newItem;
    };
    SicboBetComp.prototype.recycleItemCoinMini = function (item) {
        item.opacity = 255;
        this.miniChipPool.put(item);
        cc.Tween.stopAllByTarget(item);
    };
    SicboBetComp.prototype.getRandomPos = function (betSlotItemComp) {
        var betArea = betSlotItemComp.miniChipArea;
        var rs = cc.Vec2.ZERO;
        var offset = cc.Vec2.ZERO; // new cc.Vec2(20, 20);
        if (betSlotItemComp.door == Door.BIG)
            offset = new cc.Vec2(-22, 0); //NOTE for layer fix when throw chip
        if (betSlotItemComp.door == Door.SMALL)
            offset = new cc.Vec2(22, 0); //NOTE for layer fix when throw chip
        var halfW = (betArea.width) / 2;
        var halfH = (betArea.height) / 2;
        rs = new cc.Vec2(this.randomRange(-halfW, halfW) + offset.x, this.randomRange(-halfH, halfH) + offset.y);
        rs.add(betArea.getPosition());
        var rsV3 = SicboGameUtils_1.default.convertToOtherNode(betArea, this.miniChipRoot, new cc.Vec3(rs.x, rs.y, 0));
        rs.x = rsV3.x;
        rs.y = rsV3.y;
        return rs;
    };
    SicboBetComp.prototype.randomRange = function (min, max) {
        return Math.random() * (max - min) + min;
    };
    SicboBetComp.prototype.getMiniChipStaticPos = function (betSlot) {
        var chipSpacing = this.chipStackPosConfig.chipSpacing;
        var count = betSlot.miniChipsOnSlot.length;
        var col = 0;
        var row = count;
        // if (betSlot.door == Door.ANY_TRIPLE) {
        if (this.getChipStackMaxCol(betSlot.door) > 1) {
            var maxHeight0 = this.getChipStackHeight(betSlot.door, 0);
            if (count >= maxHeight0) {
                col = 1;
                row = count - maxHeight0;
            }
        }
        var startPos = this.getChipStackStartPos(betSlot.door, col);
        var y = startPos.y;
        y += row * chipSpacing;
        return SicboGameUtils_1.default.convertToOtherNode2(betSlot.miniChipArea, this.miniChipRoot, new cc.Vec2(startPos.x, y));
    };
    //ANCHOR  putMiniChipOnTableImmediately
    SicboBetComp.prototype.putMiniChipOnTableImmediately = function (coinType, betSlotItemComp) {
        var chip = this.createItemCoinMini(coinType);
        chip.node.setPosition(this.getRandomPos(betSlotItemComp));
        if (this.isStaticSlot(betSlotItemComp.door)) {
            chip.node.setPosition(this.getMiniChipStaticPos(betSlotItemComp));
        }
        else {
            chip.node.setPosition(this.getRandomPos(betSlotItemComp));
        }
        this.changeParent(chip.node, betSlotItemComp.miniChipArea);
        betSlotItemComp.addMiniChipToSlot(chip);
        this.removeRedundancyChip(betSlotItemComp);
    };
    SicboBetComp.prototype.putMiniChipOnTableImmediatelyByValue = function (totalValue, betSlotItemComp) {
        var _this = this;
        if (betSlotItemComp == null) {
            //cc.log("putMiniChipOnTableImmediatelyByValue NO DOOR FOUND");
            return;
        }
        var parsedCoin = SicboHelper_1.default.calculateNumOfChipToMakeValue(totalValue, this.chip_data);
        parsedCoin.forEach(function (num, val) {
            var coinType = SicboHelper_1.default.convertValueToCoinType(val);
            for (var i = 0; i < num; i++) {
                _this.putMiniChipOnTableImmediately(coinType, betSlotItemComp);
            }
        });
    };
    //ANCHOR  throwMiniChipOnTable
    SicboBetComp.prototype.throwMiniChipOnTableByValue = function (totalValue, from, betSlotItemComp) {
        var _this = this;
        if (from === void 0) { from = null; }
        if (betSlotItemComp == null) {
            //cc.log("throwMiniChipOnTableByValue NO DOOR FOUND");
            return;
        }
        var parsedCoin = SicboHelper_1.default.calculateNumOfChipToMakeValue(totalValue, this.chip_data);
        parsedCoin.forEach(function (num, val) {
            // //console.log("val: " + val + " -> " + num);
            var coinType = SicboHelper_1.default.convertValueToCoinType(val);
            for (var i = 0; i < num; i++) {
                _this.throwMiniChipOnTable(coinType, from, betSlotItemComp);
            }
        });
    };
    SicboBetComp.prototype.throwMiniChipOnTable = function (coinType, from, betSlotItemComp) {
        var _this = this;
        if (from === void 0) { from = null; }
        if (from == null)
            from = this.currentSelectedChip.node;
        var chip = this.createItemCoinMini(coinType);
        var startPos = SicboGameUtils_1.default.convertToOtherNode(from, betSlotItemComp.miniChipArea);
        var targetPos = this.getMiniChipStaticPos(betSlotItemComp);
        var randomPos = this.getRandomPos(betSlotItemComp);
        targetPos = SicboGameUtils_1.default.convertToOtherNode2(chip.node.parent, betSlotItemComp.miniChipArea, targetPos);
        randomPos = SicboGameUtils_1.default.convertToOtherNode2(chip.node.parent, betSlotItemComp.miniChipArea, randomPos);
        chip.node.parent = betSlotItemComp.miniChipArea;
        chip.node.setPosition(startPos);
        // chip.node.setSiblingIndex
        betSlotItemComp.addMiniChipToSlot(chip);
        var chipThrowDuration = 0.9;
        var fadeOutDelay = 0.5;
        if (betSlotItemComp.door != Door.BIG && betSlotItemComp.door != Door.SMALL) {
            randomPos = cc.Vec2.ZERO;
            fadeOutDelay = 0;
        }
        chip.node.scale = 1.2;
        chip.node.opacity = 255;
        cc.tween(chip.node)
            .to(chipThrowDuration / 3, { x: randomPos.x, y: randomPos.y, scale: 1 })
            .delay(fadeOutDelay)
            .to(chipThrowDuration / 3, { opacity: 0, x: targetPos.x, y: targetPos.y })
            //.to(0, {x:targetPos.x, y:targetPos.y})
            .to(chipThrowDuration / 3, { opacity: 255 })
            .call(function () {
            _this.removeRedundancyChip(betSlotItemComp);
        })
            .start();
    };
    SicboBetComp.prototype.changeParent = function (child, newParent) {
        SicboHelper_1.default.changeParent(child, newParent);
    };
    //#endregion
    //SECTION WIN - LOSE
    //ANCHOR chip to lose hole animation
    SicboBetComp.prototype.playLoseAnimation = function (winDoors, duration) {
        var _this = this;
        if (duration === void 0) { duration = .75; }
        var tweenParams = {
            position: cc.Vec3.ZERO,
        };
        this.allBetSlots.forEach(function (betSlot) {
            if (winDoors.find(function (x) { return x == betSlot.door; }) == null) {
                var lostChips = betSlot.miniChipsOnSlot;
                var self_1 = _this;
                self_1.tweenChipToAnotherNode(self_1.loseChipTarget, lostChips, duration, 0, tweenParams, true, function (chip) {
                    //ANCHOR SOUND
                    SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxChipFly);
                    self_1.recycleItemCoinMini(chip.node);
                });
                betSlot.clearMiniChipOnSlot();
            }
        });
    };
    //ANCHOR stack chip into column
    SicboBetComp.prototype.playChipStackAnimation = function (winDoors, duration) {
        var _this = this;
        if (duration === void 0) { duration = .5; }
        //cc.log(duration);
        var chipSpacing = this.chipStackPosConfig.chipSpacing;
        this.allBetSlots.forEach(function (betSlot) {
            if (_this.isStaticSlot(betSlot.door))
                return;
            if (winDoors.find(function (x) { return x == betSlot.door; }) != null) {
                var chips = betSlot.miniChipsOnSlot;
                betSlot.chipColCount = 0;
                var count = 0;
                var maxCol = _this.getChipStackMaxCol(betSlot.door);
                for (var col = 0; col < maxCol; col++) {
                    betSlot.chipColCount++;
                    var maxHeight = _this.getChipStackHeight(betSlot.door, col);
                    var startPos = _this.getChipStackStartPos(betSlot.door, col);
                    var offSet = cc.Vec2.ZERO; //this.getChipStackOffset(betSlot.door);
                    var x = startPos.x;
                    var _loop_1 = function (h) {
                        if (chips.length <= 0)
                            return "break";
                        var y = startPos.y;
                        y += h * chipSpacing;
                        var idx = Math.floor(Math.random() * chips.length);
                        var chip = chips[idx];
                        chips.splice(idx, 1);
                        count++;
                        chip.node.setSiblingIndex(count);
                        cc.tween(chip.node)
                            .to(duration, { x: x + offSet.x, y: y + offSet.y })
                            .call(function () {
                            //ANCHOR SOUND
                            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxChipFly);
                            cc.Tween.stopAllByTarget(chip.node);
                        })
                            .start();
                    };
                    for (var h = 0; h < maxHeight; h++) {
                        var state_1 = _loop_1(h);
                        if (state_1 === "break")
                            break;
                    }
                }
                for (var i = 0; i < chips.length; i++) {
                    var chip = chips[i];
                    _this.recycleItemCoinMini(chip.node);
                }
                // betSlot.miniChipsOnSlot = cachedMiniChip;
            }
        });
    };
    SicboBetComp.prototype.getChipStackConfig = function (door) {
        switch (door) {
            case Door.ANY_TRIPLE:
                return this.chipStackPosConfig.anyTripple;
            case Door.BIG:
                return this.chipStackPosConfig.big;
            case Door.SMALL:
                return this.chipStackPosConfig.small;
            case Door.ONE_TRIPLE:
            case Door.TWO_TRIPLE:
            case Door.THREE_TRIPLE:
                return this.chipStackPosConfig.trippleLeft;
            case Door.FOUR_TRIPLE:
            case Door.FIVE_TRIPLE:
            case Door.SIX_TRIPLE:
                return this.chipStackPosConfig.trippleRight;
            case Door.ONE_SINGLE:
            case Door.TWO_SINGLE:
            case Door.THREE_SINGLE:
            case Door.FOUR_SINGLE:
            case Door.FIVE_SINGLE:
            case Door.SIX_SINGLE:
                return this.chipStackPosConfig.single;
            case Door.FOUR_SUM: return this.chipStackPosConfig.four_sum;
            case Door.FIVE_SUM: return this.chipStackPosConfig.five_sum;
            case Door.SIX_SUM: return this.chipStackPosConfig.six_sum;
            case Door.SEVEN_SUM: return this.chipStackPosConfig.seven_sum;
            case Door.EIGHT_SUM: return this.chipStackPosConfig.eight_sum;
            case Door.NINE_SUM: return this.chipStackPosConfig.nine_sum;
            case Door.TEN_SUM: return this.chipStackPosConfig.ten_sum;
            case Door.ELEVEN_SUM: return this.chipStackPosConfig.eleven_sum;
            case Door.TWELVE_SUM: return this.chipStackPosConfig.twelve_sum;
            case Door.THIRTEEN_SUM: return this.chipStackPosConfig.thirteen_sum;
            case Door.FOURTEEN_SUM: return this.chipStackPosConfig.fourteen_sum;
            case Door.FIFTEEN_SUM: return this.chipStackPosConfig.fifteen_sum;
            case Door.SIXTEEN_SUM: return this.chipStackPosConfig.sixteen_sum;
            case Door.SEVENTEEN_SUM: return this.chipStackPosConfig.seventeen_sum;
        }
    };
    SicboBetComp.prototype.getChipStackStartPos = function (door, col) {
        return this.getChipStackConfig(door)[col].pos;
    };
    SicboBetComp.prototype.getChipStackOffset = function (door) {
        return this.isRightChipStack(door) ? this.chipStackPosConfig.chipStackOffsetRight : this.chipStackPosConfig.chipStackOffsetLeft;
    };
    SicboBetComp.prototype.getChipStackHeight = function (door, col) {
        return this.getChipStackConfig(door)[col].height;
    };
    SicboBetComp.prototype.getChipStackMaxCol = function (door) {
        return this.getChipStackConfig(door).length;
    };
    SicboBetComp.prototype.getCloneSpacing = function (door) {
        switch (door) {
            case Door.ANY_TRIPLE:
                return this.chipStackPosConfig.anyTrippleCloneSpacing;
            case Door.ONE_SINGLE:
            case Door.TWO_SINGLE:
            case Door.THREE_SINGLE:
            case Door.FOUR_SINGLE:
            case Door.FIVE_SINGLE:
            case Door.SIX_SINGLE:
                return this.chipStackPosConfig.singleCloneSpacing;
            case Door.BIG: return this.chipStackPosConfig.bigCloneSpacing;
            case Door.SMALL: return this.chipStackPosConfig.smallCloneSpacing;
        }
        return this.isRightChipStack(door) ? -this.chipStackPosConfig.cloneSpacing : this.chipStackPosConfig.cloneSpacing;
    };
    SicboBetComp.prototype.isStaticSlot = function (door) {
        return true;
        switch (door) {
            case Door.ANY_TRIPLE:
            case Door.ONE_TRIPLE:
            case Door.TWO_TRIPLE:
            case Door.THREE_TRIPLE:
            case Door.FOUR_TRIPLE:
            case Door.FIVE_TRIPLE:
            case Door.SIX_TRIPLE:
                return true;
        }
        return false;
    };
    SicboBetComp.prototype.isRightChipStack = function (door) {
        switch (door) {
            case Door.FOUR_TRIPLE:
            case Door.FIVE_TRIPLE:
            case Door.SIX_TRIPLE:
            case Door.BIG:
                return true;
        }
        return false;
    };
    SicboBetComp.prototype.getCloneStackSibling = function (door) {
        return this.isRightChipStack(door) ? 1 : 0;
    };
    //ANCHOR return won chips to slot    
    SicboBetComp.prototype.dealerReturnWinChip = function (winDoors, onFinish) {
        if (onFinish === void 0) { onFinish = null; }
        return this.dealerReturnChipOneByOne(winDoors, .05, .5, onFinish);
    };
    SicboBetComp.prototype.dealerReturnFullStackChip = function (winDoors, duration, onFinish) {
        var _this = this;
        if (onFinish === void 0) { onFinish = null; }
        this.allBetSlots.forEach(function (betSlot) {
            if (winDoors.find(function (x) { return x == betSlot.door; }) != null) {
                var offset = _this.getChipStackOffset(betSlot.door);
                var cloneChips = cc.instantiate(betSlot.miniChipArea);
                var spacing = _this.getCloneSpacing(betSlot.door);
                cloneChips.parent = _this.winChipPos;
                cloneChips.position = new cc.Vec3(offset.x, offset.y, 0);
                betSlot.setStackChipRoot(cloneChips);
                _this.changeParent(cloneChips, betSlot.node);
                cloneChips.setSiblingIndex(_this.getCloneStackSibling(betSlot.door));
                cc.tween(cloneChips)
                    .to(duration, { position: new cc.Vec3(offset.x + spacing, 0, 0) })
                    .call(function () {
                    if (onFinish)
                        onFinish(betSlot.door);
                })
                    .start();
            }
        });
    };
    SicboBetComp.prototype.dealerReturnChipOneByOne = function (winDoors, delayBetweenChipTime, chipFlyTime, onFinish) {
        var _this = this;
        if (delayBetweenChipTime === void 0) { delayBetweenChipTime = .05; }
        if (chipFlyTime === void 0) { chipFlyTime = .5; }
        if (onFinish === void 0) { onFinish = null; }
        var maxEffectTime = 0;
        this.allBetSlots.forEach(function (betSlot) {
            if (winDoors.find(function (x) { return x == betSlot.door; }) != null) {
                var offset = _this.getChipStackOffset(betSlot.door);
                var cloneChips = cc.instantiate(betSlot.miniChipArea);
                var spacing = _this.getCloneSpacing(betSlot.door);
                cloneChips.parent = betSlot.node;
                cloneChips.position = new cc.Vec3(offset.x + spacing, 0, 0);
                betSlot.setStackChipRoot(cloneChips);
                // this.changeParent(cloneChips, betSlot.node);
                cloneChips.setSiblingIndex(_this.getCloneStackSibling(betSlot.door));
                var chipsInStack_1 = cloneChips.children;
                var delay = delayBetweenChipTime;
                var startPos = SicboGameUtils_1.default.convertToOtherNode(_this.winChipPos, cloneChips);
                maxEffectTime = Math.max(maxEffectTime, chipsInStack_1.length * delay + chipFlyTime);
                var _loop_2 = function (i) {
                    var chip = chipsInStack_1[i];
                    var chipPos = chip.position;
                    chip.position = startPos;
                    chip.active = false;
                    cc.tween(_this.node)
                        .delay(i * delay)
                        .call(function () {
                        chip.active = true;
                        cc.tween(chip).to(chipFlyTime, { position: chipPos })
                            .call(function () {
                            //ANCHOR SOUND
                            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxChipFly);
                            if (i == chipsInStack_1.length - 1) {
                                if (onFinish)
                                    onFinish(betSlot.door);
                            }
                        })
                            .start();
                    })
                        .start();
                };
                for (var i = 0; i < chipsInStack_1.length; i++) {
                    _loop_2(i);
                }
            }
        });
        return maxEffectTime;
    };
    //ANCHOR chip to win player animation
    SicboBetComp.prototype.playPayingAnimation = function (winDoor, user, wonAmount, duration) {
        var _this = this;
        if (duration === void 0) { duration = 1; }
        var tweenParams = {
            useBezier: true,
            position: cc.Vec3.ZERO,
        };
        var betSlot = this.getDoor(winDoor);
        var winChips = this.getChipForPaying(wonAmount);
        var tweenChips = [];
        winChips.forEach(function (coinType) {
            var chipComp = _this.createItemCoinMini(coinType);
            chipComp.node.parent = betSlot.miniChipArea;
            chipComp.node.position = cc.Vec3.ZERO;
            tweenChips.push(chipComp);
        });
        var self = this;
        var delayBetweenChipTime = .1;
        self.tweenChipToAnotherNode(user, tweenChips, duration, delayBetweenChipTime, tweenParams, true, function (chip) {
            self.recycleItemCoinMini(chip.node);
        });
        this.clearMiniChipOnSlot(betSlot);
    };
    SicboBetComp.prototype.clearMiniChipOnSlot = function (betSlot) {
        var _this = this;
        betSlot.miniChipsOnSlot.forEach(function (chip) {
            _this.recycleItemCoinMini(chip.node);
        });
        betSlot.clearMiniChipOnSlot();
        betSlot.miniChipsOnStack.forEach(function (chip) {
            _this.recycleItemCoinMini(chip.node);
        });
        betSlot.deleteStackChipRoot();
    };
    SicboBetComp.prototype.destroyAllMiniChip = function () {
        for (var i = 0; i < this.allBetSlots.length; i++) {
            var betSlot = this.allBetSlots[i];
            betSlot.miniChipsOnSlot.forEach(function (chip) {
                var _a;
                (_a = chip.node) === null || _a === void 0 ? void 0 : _a.destroy();
            });
            betSlot.clearMiniChipOnSlot();
            betSlot.miniChipsOnStack.forEach(function (chip) {
                var _a;
                (_a = chip.node) === null || _a === void 0 ? void 0 : _a.destroy();
            });
            betSlot.deleteStackChipRoot();
        }
        this.miniChipPool.clear();
    };
    SicboBetComp.prototype.getChipForPaying = function (wonValue) {
        if (wonValue <= 5000) {
            return [SicboCoinType_1.SicboCoinType.Coin1k, SicboCoinType_1.SicboCoinType.Coin1k, SicboCoinType_1.SicboCoinType.Coin1k];
        }
        else if (wonValue <= 20000) {
            return [SicboCoinType_1.SicboCoinType.Coin5k, SicboCoinType_1.SicboCoinType.Coin5k, SicboCoinType_1.SicboCoinType.Coin5k];
        }
        else if (wonValue <= 100000) {
            return [SicboCoinType_1.SicboCoinType.Coin100k];
        }
        else if (wonValue <= 1000000) {
            return [SicboCoinType_1.SicboCoinType.Coin1M];
        }
        else {
            return [SicboCoinType_1.SicboCoinType.Coin5M, SicboCoinType_1.SicboCoinType.Coin5M, SicboCoinType_1.SicboCoinType.Coin5M];
        }
    };
    SicboBetComp.prototype.removeRedundancyChip = function (betSlot) {
        var _this = this;
        var mergeFrom = -1;
        if (this.isStaticSlot(betSlot.door)) {
            if (this.getChipStackMaxCol(betSlot.door) > 1) {
                mergeFrom = this.getChipStackHeight(betSlot.door, 0);
            }
            else {
                mergeFrom = 0;
            }
            mergeFrom += 5;
        }
        var rc = betSlot.getRedundancyChip(this.isStaticSlot(betSlot.door), mergeFrom);
        rc.forEach(function (chip) {
            _this.recycleItemCoinMini(chip.node);
        });
    };
    SicboBetComp.prototype.tweenChipToAnotherNode = function (targetNode, chipList, duration, delayBetweenChipTime, tweenParams, changeParentToTargetNode, onComplete) {
        var _this = this;
        if (delayBetweenChipTime === void 0) { delayBetweenChipTime = 0; }
        if (changeParentToTargetNode === void 0) { changeParentToTargetNode = true; }
        var _loop_3 = function (i) {
            var chip = chipList[i];
            var cachedParent = chip.node.parent;
            if (chip.node.parent == null) {
                return "continue";
            }
            this_1.changeParent(chip.node, targetNode);
            var tween = null;
            if (tweenParams.useBezier) {
                var startPos = new cc.Vec2(chip.node.position.x, chip.node.position.y);
                var endPos = new cc.Vec2(tweenParams.position.x, tweenParams.position.y);
                tween = cc.tween(chip.node).delay(i * delayBetweenChipTime).bezierTo(duration, startPos, new cc.Vec2(startPos.x, startPos.y + (endPos.y - startPos.y) * 1), endPos);
            }
            else {
                tween = cc.tween(chip.node).delay(i * delayBetweenChipTime).to(duration, tweenParams);
            }
            tween.call(function () {
                if (changeParentToTargetNode == false)
                    _this.changeParent(chip.node, cachedParent);
                if (onComplete != null)
                    onComplete(chip);
            })
                .start();
        };
        var this_1 = this;
        for (var i = 0; i < chipList.length; i++) {
            _loop_3(i);
        }
    };
    //ANCHOR Tween    
    SicboBetComp.prototype.tween = function (node, targetNode, tweenParams, onComplete) {
        var duration = tweenParams.duration || 0;
        var delay = tweenParams.delay || 0;
        var targetPos = tweenParams.positionInTargetNode || cc.Vec2.ZERO;
        var bezierPoint = tweenParams.bezierPoint || cc.Vec2.ZERO;
        targetPos = SicboGameUtils_1.default.convertToOtherNode2(node.parent, targetNode, targetPos);
        var tween = null;
        var startPos = new cc.Vec2(node.position.x, node.position.y);
        var endPos = targetPos;
        if (tweenParams.useBezier) {
            tween = cc.tween(node).delay(delay).bezierTo(duration, startPos, bezierPoint, endPos);
        }
        else {
            tween = cc.tween(node).delay(delay).to(duration, { x: endPos.x, y: endPos.y });
        }
        tween.call(function () {
            onComplete && onComplete();
        })
            .start();
    };
    //!SECTION
    //SECTION update bet number in table  
    SicboBetComp.prototype.updateMyUserBetOnTable = function (userBetAmounts) {
        var _this = this;
        userBetAmounts.forEach(function (betAmount) {
            var door = _this.getDoor(betAmount.getDoor());
            door === null || door === void 0 ? void 0 : door.setMyTotalBet(betAmount.getBetAmount());
        });
    };
    SicboBetComp.prototype.updateTotalBetOnTable = function (totalBetAmounts) {
        var _this = this;
        totalBetAmounts.forEach(function (betAmount) {
            var door = _this.getDoor(betAmount.getDoor());
            door === null || door === void 0 ? void 0 : door.setTotalBet(betAmount.getBetAmount());
        });
    };
    //!SECTION
    //SECTION ISicboState
    SicboBetComp.prototype.exitState = function (status) {
        if (status == Status.PAYING) {
            for (var i = 0; i < this.allBetSlots.length; i++) {
                this.allBetSlots[i].clearMiniChipOnSlot();
                this.allBetSlots[i].resetSlot();
            }
        }
    };
    SicboBetComp.prototype.updateState = function (state) {
        if (state.getStatus() == Status.BETTING || state.getStatus() == Status.END_BET || state.getStatus() == Status.RESULTING || state.getStatus() == Status.PAYING) {
            this.updateTotalBetOnTable(state.getDoorsAndBetAmountsList());
            this.updateMyUserBetOnTable(state.getDoorsAndPlayerBetAmountsList());
        }
        this.setDoubleBetActive(state.getAllowBetDouble());
        this.setRebetActive(state.getAllowReBet());
    };
    SicboBetComp.prototype.startState = function (state, totalDuration) {
        var _this = this;
        var status = state.getStatus();
        this.disableChip = status != Status.BETTING;
        if (status != Status.WAITING && status != Status.FINISHING) {
            state.getDoorsAndBetAmountsList().forEach(function (dab) {
                var doorComp = _this.getDoor(dab.getDoor());
                if (doorComp.miniChipsOnSlot.length == 0)
                    _this.putMiniChipOnTableImmediatelyByValue(dab.getBetAmount(), doorComp);
            });
        }
        if (status == Status.WAITING || status == Status.FINISHING) {
            for (var i = 0; i < this.allBetSlots.length; i++) {
                this.allBetSlots[i].resetSlot();
            }
        }
    };
    Object.defineProperty(SicboBetComp.prototype, "disableChip", {
        //ANCHOR disable chip
        get: function () {
            return this._disableChip;
        },
        set: function (value) {
            var _this = this;
            this._disableChip = value;
            this.chipItems.forEach(function (chip) {
                chip.isDisable = value;
                if (!value) {
                    if (_this.currentSelectedChip != null)
                        _this.selectChip(_this.currentSelectedChip, false);
                    else
                        _this.selectChip(_this.chipItems[0], false);
                }
            });
        },
        enumerable: false,
        configurable: true
    });
    //Quick bet
    SicboBetComp.prototype.setDoubleBetActive = function (isActive) {
        this.doubleBetButton.interactable = isActive;
    };
    SicboBetComp.prototype.setRebetActive = function (isActive) {
        this.rebetButton.interactable = isActive;
    };
    //!SECTION
    SicboBetComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
    };
    SicboBetComp.prototype.onHide = function () {
        cc.Tween.stopAllByTarget(this.node);
        for (var i = 0; i < this.allBetSlots.length; i++) {
            this.allBetSlots[i].resetSlot();
            // this.clearMiniChipOnSlot(this.allBetSlots[i]);
        }
        this.destroyAllMiniChip();
    };
    __decorate([
        property(cc.ScrollView)
    ], SicboBetComp.prototype, "chipsScrollView", void 0);
    __decorate([
        property(cc.Prefab)
    ], SicboBetComp.prototype, "largeChipPrefab", void 0);
    __decorate([
        property(cc.Button)
    ], SicboBetComp.prototype, "rebetButton", void 0);
    __decorate([
        property(cc.Button)
    ], SicboBetComp.prototype, "doubleBetButton", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetComp.prototype, "betSlotsNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetComp.prototype, "miniChipRoot", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetComp.prototype, "loseChipTarget", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetComp.prototype, "winChipPos", void 0);
    SicboBetComp = __decorate([
        ccclass
    ], SicboBetComp);
    return SicboBetComp;
}(cc.Component));
exports.default = SicboBetComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9CZXRDb21wLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHFFQUFnRTtBQUUxRCxJQUFBLEtBSU0sNEJBQWtCLENBQUMsVUFBVSxFQUFFLEVBSHpDLElBQUksVUFBQSxFQUNKLEtBQUssV0FBQSxFQUNMLGdCQUFnQixzQkFBQSxFQUNoQixNQUFNLFlBQW1DLENBQUM7QUFJdEMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFDNUMsNkNBQXdDO0FBQ3hDLHNEQUFpRDtBQUNqRCwrREFBcUY7QUFDckYscUVBQWdFO0FBR2hFLHdEQUFxRDtBQUVyRCxrRUFBNkQ7QUFDN0Qsa0dBQTZGO0FBQzdGLHFFQUFnRTtBQUNoRSw2REFBNEQ7QUFLNUQ7SUFBMEMsZ0NBQVk7SUFBdEQ7UUFBQSxxRUFxK0JDO1FBcCtCQyxpQkFBVyxHQUFlLElBQUksQ0FBQztRQUcvQixxQkFBZSxHQUFrQixJQUFJLENBQUM7UUFDdEMseUJBQW1CLEdBQXNCLElBQUksQ0FBQztRQUc5QyxxQkFBZSxHQUFhLElBQUksQ0FBQztRQUdqQyxpQkFBVyxHQUFjLElBQUksQ0FBQztRQUc5QixxQkFBZSxHQUFjLElBQUksQ0FBQztRQUdsQyxrQkFBWSxHQUFXLElBQUksQ0FBQztRQUM1QixpQkFBVyxHQUEyQixFQUFFLENBQUM7UUFHekMsa0JBQVksR0FBVyxJQUFJLENBQUM7UUFDNUIsa0JBQVksR0FBZSxJQUFJLENBQUM7UUFHaEMsb0JBQWMsR0FBVyxJQUFJLENBQUM7UUFHOUIsZ0JBQVUsR0FBVyxJQUFJLENBQUM7UUFFbEIsa0JBQVksR0FBWSxLQUFLLENBQUM7UUFFOUIsZUFBUyxHQUFHLEVBQUUsQ0FBQztRQUNmLGVBQVMsR0FBeUIsRUFBRSxDQUFDO1FBQy9DLDhCQUE4QjtRQUM1Qix3QkFBa0IsR0FBRztZQUNuQixzQkFBc0IsRUFBRSxFQUFFO1lBQzFCLGtCQUFrQixFQUFFLENBQUM7WUFDckIsaUJBQWlCLEVBQUUsRUFBRTtZQUNyQixlQUFlLEVBQUUsQ0FBQyxFQUFFO1lBQ3BCLFlBQVksRUFBRSxDQUFDO1lBQ2YsV0FBVyxFQUFFLENBQUM7WUFFZCxvQkFBb0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBQzNDLG1CQUFtQixFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBRSxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBRXpDLFVBQVUsRUFBRztnQkFDWDtvQkFDSSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDO29CQUMzQixNQUFNLEVBQUUsRUFBRTtpQkFDYjtnQkFDRDtvQkFDSSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDO29CQUMzQixNQUFNLEVBQUUsRUFBRTtpQkFDYjthQUNGO1lBRUQsV0FBVyxFQUFHO2dCQUNaO29CQUNFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQzFCLE1BQU0sRUFBRSxFQUFFO2lCQUNYO2FBQ0Y7WUFFRCxZQUFZLEVBQUc7Z0JBQ2I7b0JBQ0UsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLE1BQU0sRUFBRSxFQUFFO2lCQUNYO2FBQ0Y7WUFFRCxLQUFLLEVBQUc7Z0JBQ047b0JBQ0ksR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBRSxDQUFDLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDN0IsTUFBTSxFQUFFLEVBQUU7aUJBQ2I7Z0JBQ0Q7b0JBQ0ksR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDM0IsTUFBTSxFQUFFLEVBQUU7aUJBQ2I7YUFLSDtZQUVELEdBQUcsRUFBRztnQkFDTDtvQkFDRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDM0IsTUFBTSxFQUFFLEVBQUU7aUJBQ1g7Z0JBQ0Q7b0JBQ0UsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ3pCLE1BQU0sRUFBRSxFQUFFO2lCQUNYO2FBTUY7WUFFRCxRQUFRLEVBQUc7Z0JBQ1Q7b0JBQ0UsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQztvQkFDN0IsTUFBTSxFQUFFLEVBQUU7aUJBQ1g7YUFDRjtZQUVELFFBQVEsRUFBRztnQkFDVDtvQkFDRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDO29CQUM3QixNQUFNLEVBQUUsRUFBRTtpQkFDWDthQUNGO1lBR0QsT0FBTyxFQUFHO2dCQUNSO29CQUNFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUM7b0JBQzdCLE1BQU0sRUFBRSxFQUFFO2lCQUNYO2FBQ0Y7WUFHRCxTQUFTLEVBQUc7Z0JBQ1Y7b0JBQ0UsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQztvQkFDN0IsTUFBTSxFQUFFLEVBQUU7aUJBQ1g7YUFDRjtZQUdELFNBQVMsRUFBRztnQkFDVjtvQkFDRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDO29CQUM3QixNQUFNLEVBQUUsRUFBRTtpQkFDWDthQUNGO1lBRUQsUUFBUSxFQUFHO2dCQUNUO29CQUNFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUM7b0JBQzdCLE1BQU0sRUFBRSxFQUFFO2lCQUNYO2FBQ0Y7WUFFRCxPQUFPLEVBQUc7Z0JBQ1I7b0JBQ0UsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQztvQkFDN0IsTUFBTSxFQUFFLEVBQUU7aUJBQ1g7YUFDRjtZQUdELFVBQVUsRUFBRztnQkFDWDtvQkFDRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDO29CQUM3QixNQUFNLEVBQUUsRUFBRTtpQkFDWDthQUNGO1lBR0QsVUFBVSxFQUFHO2dCQUNYO29CQUNFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUM7b0JBQzdCLE1BQU0sRUFBRSxFQUFFO2lCQUNYO2FBQ0Y7WUFHRCxZQUFZLEVBQUc7Z0JBQ2I7b0JBQ0UsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBRSxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQztvQkFDL0IsTUFBTSxFQUFFLEVBQUU7aUJBQ1g7YUFDRjtZQUVELFlBQVksRUFBRztnQkFDYjtvQkFDRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDO29CQUM3QixNQUFNLEVBQUUsRUFBRTtpQkFDWDthQUNGO1lBRUQsV0FBVyxFQUFHO2dCQUNaO29CQUNFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUM7b0JBQy9CLE1BQU0sRUFBRSxFQUFFO2lCQUNYO2FBQ0Y7WUFHRCxXQUFXLEVBQUc7Z0JBQ1o7b0JBQ0UsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBRSxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQztvQkFDL0IsTUFBTSxFQUFFLEVBQUU7aUJBQ1g7YUFDRjtZQUdELGFBQWEsRUFBRztnQkFDZDtvQkFDRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDO29CQUMvQixNQUFNLEVBQUUsRUFBRTtpQkFDWDthQUNGO1lBRUQsTUFBTSxFQUFHO2dCQUNQO29CQUNJLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQzdCLE1BQU0sRUFBRSxFQUFFO2lCQUNiO2FBS0Y7U0FFRixDQUFBOztJQTB3QkQsQ0FBQztJQXh3QkMsNkJBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsc0JBQXNCLENBQUMscUJBQVcsQ0FBQyxDQUFDO1FBRXhFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQyw4QkFBb0IsQ0FBQyxDQUFDO1FBRW5GLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDeEMsQ0FBQztJQUVELDhCQUFPLEdBQVAsVUFBUSxJQUFJO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBRSxPQUFBLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFkLENBQWMsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCwwQkFBMEI7SUFDMUIsc0NBQWUsR0FBZixVQUFnQixJQUFjOztRQUM1QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkMsTUFBQSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxtQ0FBeUIsQ0FBQywwQ0FBRSxxQkFBcUIsR0FBRztJQUN4RixDQUFDO0lBR00sMENBQW1CLEdBQTFCLFVBQTJCLElBQWM7UUFBekMsaUJBSUM7UUFIQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUMsU0FBUztZQUNyQixLQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHFDQUFjLEdBQWQsVUFBZSxTQUFpQjtRQUFoQyxpQkFZQztRQVhDLElBQUksUUFBUSxHQUFHLHFCQUFXLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDN0QsSUFBSSxPQUFPLEdBQUcsd0JBQWMsQ0FBQyxvQkFBb0IsQ0FBQywyQkFBaUIsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFekgsSUFBSSxRQUFRLEdBQUcsSUFBSSx5Q0FBcUIsRUFBRSxDQUFDO1FBQzNDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQzdCLFFBQVEsQ0FBQyxPQUFPLEdBQUcsVUFBQyxpQkFBaUI7WUFDbkMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3JDLENBQUMsQ0FBQztRQUNGLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDMUIsT0FBTyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxpQ0FBVSxHQUFWLFVBQVcsaUJBQW9DLEVBQUUsT0FBdUI7UUFBdkIsd0JBQUEsRUFBQSxjQUF1QjtRQUN0RSxJQUFHLElBQUksQ0FBQyxXQUFXO1lBQUUsT0FBTztRQUM1QixJQUFHLElBQUksQ0FBQyxtQkFBbUIsSUFBRSxJQUFJO1lBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3RSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsaUJBQWlCLENBQUM7UUFDN0MsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6Qyx3REFBd0Q7UUFDeEQsSUFBRyxPQUFPO1lBQ1IseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHlCQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7SUFFMUQsQ0FBQztJQUVELDBDQUFtQixHQUFuQjtRQUNFLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDaEQsQ0FBQztJQUVELDRDQUFxQixHQUFyQjtRQUNFLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQztRQUMzRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUMsU0FBUyxDQUFDO1FBRTNELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDcEQsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBRTFELElBQUksQ0FBQyxlQUFlLENBQUMseUJBQXlCLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsR0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ2pHLENBQUM7SUFFRCwyQ0FBb0IsR0FBcEI7UUFDRSxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7UUFDM0QsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFDLFNBQVMsQ0FBQztRQUUzRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3BELElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUUxRCxJQUFJLENBQUMsZUFBZSxDQUFDLHlCQUF5QixDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFDLEdBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNqRyxDQUFDO0lBRUQseUNBQWtCLEdBQWxCLFVBQW1CLElBQWE7O1FBQzlCLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEMsSUFBSSxHQUFHLEdBQUcsd0JBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3RSxJQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUUsQ0FBQyxHQUFHLElBQUksR0FBRyxDQUFDLENBQUMsSUFBRSxHQUFHO1lBQUUsT0FBTyxDQUFBLGNBQWM7UUFFbkQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUNwRCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFFMUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekYsTUFBQSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxtQ0FBeUIsQ0FBQywwQ0FBRSxxQkFBcUIsR0FBRztJQUN4RixDQUFDO0lBQ0QsVUFBVTtJQUVWLHNCQUFzQjtJQUNwQjs7VUFFQTtJQUNBLHlDQUFrQixHQUFsQixVQUFtQixlQUFxQztRQUN0RCxJQUFHLElBQUksQ0FBQyxXQUFXO1lBQUUsT0FBTztRQUM1QixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6RiwrRkFBK0Y7SUFDakcsQ0FBQztJQUVILHlDQUFrQixHQUFsQixVQUFtQixRQUF1QjtRQUN4QyxJQUFJLE9BQU8sR0FBcUIsSUFBSSxDQUFDO1FBQ3JDLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLEVBQUU7WUFDaEMsT0FBTyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLENBQUMsWUFBWSxDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDckU7YUFBTTtZQUNMLE9BQU8sR0FBRyx3QkFBYyxDQUFDLG9CQUFvQixDQUFDLDJCQUFpQixFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUN0SDtRQUNELE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDeEMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUUzQixJQUFJLFFBQVEsR0FBRyxJQUFJLHlDQUFxQixFQUFFLENBQUM7UUFDM0MsUUFBUSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDN0IsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkIsT0FBTyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUUxQixnREFBZ0Q7UUFFaEQsdUJBQXVCO1FBQ3ZCLDRCQUE0QjtRQUM1QixPQUFPLE9BQU8sQ0FBQztJQUNqQixDQUFDO0lBRUQsMENBQW1CLEdBQW5CLFVBQW9CLElBQWE7UUFDL0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELG1DQUFZLEdBQVosVUFBYSxlQUFxQztRQUNoRCxJQUFJLE9BQU8sR0FBRyxlQUFlLENBQUMsWUFBWSxDQUFDO1FBQzNDLElBQUksRUFBRSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBRXRCLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUEsdUJBQXVCO1FBQ2pELElBQUcsZUFBZSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsR0FBRztZQUFFLE1BQU0sR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxvQ0FBb0M7UUFDdkcsSUFBRyxlQUFlLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLO1lBQUUsTUFBTSxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxvQ0FBb0M7UUFFeEcsSUFBSSxLQUFLLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUMsQ0FBQyxDQUFDO1FBQzlCLElBQUksS0FBSyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFDLENBQUMsQ0FBQztRQUMvQixFQUFFLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUNaLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsRUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUM3QyxDQUFDO1FBRUYsRUFBRSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUU5QixJQUFJLElBQUksR0FBRyx3QkFBYyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNyRyxFQUFFLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDZCxFQUFFLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDZCxPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRCxrQ0FBVyxHQUFYLFVBQVksR0FBVSxFQUFFLEdBQVU7UUFDaEMsT0FBTyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQzNDLENBQUM7SUFFRCwyQ0FBb0IsR0FBcEIsVUFBcUIsT0FBNkI7UUFDaEQsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQztRQUV0RCxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQztRQUMzQyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDWixJQUFJLEdBQUcsR0FBRyxLQUFLLENBQUM7UUFHaEIseUNBQXlDO1FBQ3pDLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUU7WUFDekMsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDMUQsSUFBRyxLQUFLLElBQUksVUFBVSxFQUFDO2dCQUNyQixHQUFHLEdBQUcsQ0FBQyxDQUFDO2dCQUNSLEdBQUcsR0FBRyxLQUFLLEdBQUcsVUFBVSxDQUFDO2FBQzFCO1NBQ0o7UUFDRCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQ25CLENBQUMsSUFBSSxHQUFHLEdBQUcsV0FBVyxDQUFDO1FBRXZCLE9BQU8sd0JBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqSCxDQUFDO0lBRUQsdUNBQXVDO0lBQ3ZDLG9EQUE2QixHQUE3QixVQUE4QixRQUF1QixFQUFFLGVBQXFDO1FBQzFGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUU3QyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFDMUQsSUFBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBQztZQUN6QyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztTQUNuRTthQUFJO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO1NBQzNEO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUMzRCxlQUFlLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRCwyREFBb0MsR0FBcEMsVUFBcUMsVUFBa0IsRUFBRSxlQUFxQztRQUE5RixpQkFhQztRQVpDLElBQUcsZUFBZSxJQUFJLElBQUksRUFBQztZQUN6QiwrREFBK0Q7WUFDL0QsT0FBTztTQUNSO1FBQ0QsSUFBSSxVQUFVLEdBQUcscUJBQVcsQ0FBQyw2QkFBNkIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXZGLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFXLEVBQUUsR0FBVztZQUMxQyxJQUFJLFFBQVEsR0FBRyxxQkFBVyxDQUFDLHNCQUFzQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzVCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxRQUFRLEVBQUUsZUFBZSxDQUFDLENBQUM7YUFDL0Q7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw4QkFBOEI7SUFDOUIsa0RBQTJCLEdBQTNCLFVBQTRCLFVBQWtCLEVBQUUsSUFBbUIsRUFBRSxlQUFxQztRQUExRyxpQkFlQztRQWYrQyxxQkFBQSxFQUFBLFdBQW1CO1FBQ2pFLElBQUcsZUFBZSxJQUFJLElBQUksRUFBQztZQUN6QixzREFBc0Q7WUFDdEQsT0FBTztTQUNSO1FBRUQsSUFBSSxVQUFVLEdBQUcscUJBQVcsQ0FBQyw2QkFBNkIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXZGLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFXLEVBQUUsR0FBVztZQUMxQywrQ0FBK0M7WUFDL0MsSUFBSSxRQUFRLEdBQUcscUJBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM1QixLQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQzthQUM1RDtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDJDQUFvQixHQUFwQixVQUFxQixRQUF1QixFQUFFLElBQW1CLEVBQUUsZUFBcUM7UUFBeEcsaUJBeUNDO1FBekM2QyxxQkFBQSxFQUFBLFdBQW1CO1FBQy9ELElBQUcsSUFBSSxJQUFJLElBQUk7WUFBRSxJQUFJLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQztRQUV0RCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFN0MsSUFBSSxRQUFRLEdBQUcsd0JBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRXJGLElBQUksU0FBUyxHQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsQ0FBQTtRQUMzRCxJQUFJLFNBQVMsR0FBSSxJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBRXBELFNBQVMsR0FBRyx3QkFBYyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLGVBQWUsQ0FBQyxZQUFZLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDMUcsU0FBUyxHQUFHLHdCQUFjLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsZUFBZSxDQUFDLFlBQVksRUFBRSxTQUFTLENBQUMsQ0FBQztRQUUxRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxlQUFlLENBQUMsWUFBWSxDQUFDO1FBQ2hELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2hDLDRCQUE0QjtRQUM1QixlQUFlLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFeEMsSUFBSSxpQkFBaUIsR0FBRyxHQUFHLENBQUM7UUFDNUIsSUFBSSxZQUFZLEdBQUcsR0FBRyxDQUFDO1FBQ3ZCLElBQUcsZUFBZSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBQztZQUN4RSxTQUFTLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDekIsWUFBWSxHQUFHLENBQUMsQ0FBQztTQUNsQjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFFeEIsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2FBQ2xCLEVBQUUsQ0FBQyxpQkFBaUIsR0FBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLEVBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFDLENBQUM7YUFDakUsS0FBSyxDQUFDLFlBQVksQ0FBQzthQUNuQixFQUFFLENBQUMsaUJBQWlCLEdBQUMsQ0FBQyxFQUFFLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUMsU0FBUyxDQUFDLENBQUMsRUFBQyxDQUFDO1lBQ3BFLHdDQUF3QzthQUN2QyxFQUFFLENBQUMsaUJBQWlCLEdBQUMsQ0FBQyxFQUFFLEVBQUMsT0FBTyxFQUFFLEdBQUcsRUFBQyxDQUFDO2FBQ3ZDLElBQUksQ0FDSDtZQUNFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM3QyxDQUFDLENBQ0Y7YUFDQSxLQUFLLEVBQUUsQ0FBQztJQUVYLENBQUM7SUFHTyxtQ0FBWSxHQUFwQixVQUFxQixLQUFjLEVBQUUsU0FBaUI7UUFDcEQscUJBQVcsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFDRCxZQUFZO0lBRVosb0JBQW9CO0lBQ3BCLG9DQUFvQztJQUNwQyx3Q0FBaUIsR0FBakIsVUFBa0IsUUFBZSxFQUFFLFFBQXNCO1FBQXpELGlCQW9CQztRQXBCa0MseUJBQUEsRUFBQSxjQUFzQjtRQUN2RCxJQUFJLFdBQVcsR0FBRztZQUNoQixRQUFRLEVBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJO1NBRXhCLENBQUE7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87WUFDNUIsSUFBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFFLE9BQUEsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEVBQWpCLENBQWlCLENBQUMsSUFBSSxJQUFJLEVBQUM7Z0JBQy9DLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxlQUFlLENBQUM7Z0JBRXhDLElBQUksTUFBSSxHQUFHLEtBQUksQ0FBQztnQkFDaEIsTUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQUksQ0FBQyxjQUFjLEVBQUUsU0FBUyxFQUN0RCxRQUFRLEVBQUUsQ0FBQyxFQUFFLFdBQVcsRUFDeEIsSUFBSSxFQUFHLFVBQUMsSUFBc0I7b0JBQzlCLGNBQWM7b0JBQ2QseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHlCQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3hELE1BQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3hDLENBQUMsQ0FBQyxDQUFDO2dCQUNILE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2FBQy9CO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsK0JBQStCO0lBQy9CLDZDQUFzQixHQUF0QixVQUF1QixRQUFlLEVBQUUsUUFBcUI7UUFBN0QsaUJBdURHO1FBdkRxQyx5QkFBQSxFQUFBLGFBQXFCO1FBQzNELG1CQUFtQjtRQUNuQixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDO1FBRXRELElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUM5QixJQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztnQkFBRSxPQUFPO1lBQzNDLElBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBRSxPQUFBLENBQUMsSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFqQixDQUFpQixDQUFDLElBQUksSUFBSSxFQUFDO2dCQUMzQyxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDO2dCQUNwQyxPQUFPLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztnQkFFekIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO2dCQUNkLElBQUksTUFBTSxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRW5ELEtBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUU7b0JBQ3JDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQkFFdkIsSUFBSSxTQUFTLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7b0JBQzNELElBQUksUUFBUSxHQUFHLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDO29CQUM1RCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBLHdDQUF3QztvQkFFbEUsSUFBSSxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQzs0Q0FDVixDQUFDO3dCQUNSLElBQUcsS0FBSyxDQUFDLE1BQU0sSUFBRSxDQUFDOzJDQUFRO3dCQUUxQixJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO3dCQUNuQixDQUFDLElBQUksQ0FBQyxHQUFDLFdBQVcsQ0FBQzt3QkFFbkIsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUNuRCxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ3RCLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUVyQixLQUFLLEVBQUUsQ0FBQzt3QkFDUixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFFakMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDOzZCQUNoQixFQUFFLENBQUMsUUFBUSxFQUFHLEVBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsRUFBQyxDQUFDOzZCQUNqRCxJQUFJLENBQ0g7NEJBQ0UsY0FBYzs0QkFDZCx5QkFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMseUJBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDeEQsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUN0QyxDQUFDLENBQ0Y7NkJBQ0EsS0FBSyxFQUFFLENBQUM7O29CQXRCYixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxFQUFFLENBQUMsRUFBRTs4Q0FBekIsQ0FBQzs7O3FCQXVCUDtpQkFDRjtnQkFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDckMsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNwQixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNyQztnQkFDRCw0Q0FBNEM7YUFDN0M7UUFDSCxDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUM7SUFFRCx5Q0FBa0IsR0FBbEIsVUFBbUIsSUFBUztRQUMxQixRQUFRLElBQUksRUFBRTtZQUNaLEtBQUssSUFBSSxDQUFDLFVBQVU7Z0JBQ2xCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQztZQUU1QyxLQUFLLElBQUksQ0FBQyxHQUFHO2dCQUNYLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQztZQUVyQyxLQUFLLElBQUksQ0FBQyxLQUFLO2dCQUNiLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQztZQUV2QyxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUM7WUFBQyxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUM7WUFBQyxLQUFLLElBQUksQ0FBQyxZQUFZO2dCQUNoRSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUM7WUFFN0MsS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQUMsS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQUMsS0FBSyxJQUFJLENBQUMsVUFBVTtnQkFDaEUsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDO1lBRTlDLEtBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQztZQUFDLEtBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLFVBQVU7Z0JBQ3RJLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQztZQUN4QyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUM7WUFDNUQsS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDO1lBQzVELEtBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQztZQUMxRCxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUM7WUFDOUQsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDO1lBQzlELEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQztZQUM1RCxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUM7WUFDMUQsS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDO1lBQ2hFLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQztZQUNoRSxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUM7WUFDcEUsS0FBSyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDO1lBQ3BFLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQztZQUNsRSxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUM7WUFDbEUsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDO1NBQ3ZFO0lBQ0gsQ0FBQztJQUNELDJDQUFvQixHQUFwQixVQUFxQixJQUFTLEVBQUUsR0FBVztRQUN6QyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFDaEQsQ0FBQztJQUVELHlDQUFrQixHQUFsQixVQUFtQixJQUFTO1FBQzFCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFBLENBQUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsb0JBQW9CLENBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQztJQUNoSSxDQUFDO0lBRUQseUNBQWtCLEdBQWxCLFVBQW1CLElBQVMsRUFBRSxHQUFXO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztJQUNuRCxDQUFDO0lBRUQseUNBQWtCLEdBQWxCLFVBQW1CLElBQVM7UUFDMUIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDO0lBQzlDLENBQUM7SUFFRCxzQ0FBZSxHQUFmLFVBQWdCLElBQVM7UUFDdkIsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLElBQUksQ0FBQyxVQUFVO2dCQUNsQixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxzQkFBc0IsQ0FBQztZQUN4RCxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUM7WUFBQyxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUM7WUFBQyxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUM7WUFBQSxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUM7WUFBQyxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUM7WUFBQyxLQUFLLElBQUksQ0FBQyxVQUFVO2dCQUNuSSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQztZQUNwRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUM7WUFDOUQsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLENBQUM7U0FDbkU7UUFFRCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQSxDQUFDLENBQUEsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFBLENBQUMsQ0FBQSxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDO0lBQ2hILENBQUM7SUFFRCxtQ0FBWSxHQUFaLFVBQWEsSUFBUztRQUNwQixPQUFPLElBQUksQ0FBQztRQUNaLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3JCLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQztZQUFBLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLFVBQVU7Z0JBQ25JLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCx1Q0FBZ0IsR0FBaEIsVUFBaUIsSUFBUztRQUN4QixRQUFRLElBQUksRUFBRTtZQUNaLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUFDLEtBQUssSUFBSSxDQUFDLEdBQUc7Z0JBQy9FLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCwyQ0FBb0IsR0FBcEIsVUFBcUIsSUFBUztRQUM1QixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQSxDQUFDLENBQUEsQ0FBQyxDQUFBLENBQUMsQ0FBQSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELHFDQUFxQztJQUNyQywwQ0FBbUIsR0FBbkIsVUFBb0IsUUFBZSxFQUFFLFFBQWlDO1FBQWpDLHlCQUFBLEVBQUEsZUFBaUM7UUFDcEUsT0FBTyxJQUFJLENBQUMsd0JBQXdCLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVELGdEQUF5QixHQUF6QixVQUEwQixRQUFlLEVBQUUsUUFBZSxFQUFFLFFBQWlDO1FBQTdGLGlCQXFCQztRQXJCMkQseUJBQUEsRUFBQSxlQUFpQztRQUMzRixJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87WUFDOUIsSUFBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFFLE9BQUEsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEVBQWpCLENBQWlCLENBQUMsSUFBSSxJQUFJLEVBQUM7Z0JBQzdDLElBQUksTUFBTSxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25ELElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN0RCxJQUFJLE9BQU8sR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDakQsVUFBVSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDO2dCQUNwQyxVQUFVLENBQUMsUUFBUSxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pELE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFFckMsS0FBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1QyxVQUFVLENBQUMsZUFBZSxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFFcEUsRUFBRSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUM7cUJBQ25CLEVBQUUsQ0FBQyxRQUFRLEVBQUcsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxDQUFDO3FCQUNoRSxJQUFJLENBQUM7b0JBQ0osSUFBRyxRQUFRO3dCQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3RDLENBQUMsQ0FBQztxQkFDRCxLQUFLLEVBQUUsQ0FBQzthQUNWO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sK0NBQXdCLEdBQWhDLFVBQWlDLFFBQWUsRUFBRSxvQkFBa0MsRUFBRSxXQUF3QixFQUFFLFFBQWlDO1FBQWpKLGlCQWlEQztRQWpEaUQscUNBQUEsRUFBQSwwQkFBa0M7UUFBRSw0QkFBQSxFQUFBLGdCQUF3QjtRQUFFLHlCQUFBLEVBQUEsZUFBaUM7UUFDL0ksSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDO1FBRXRCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUM5QixJQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQUUsT0FBQSxDQUFDLElBQUksT0FBTyxDQUFDLElBQUksRUFBakIsQ0FBaUIsQ0FBQyxJQUFJLElBQUksRUFBQztnQkFDN0MsSUFBSSxNQUFNLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkQsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3RELElBQUksT0FBTyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNqRCxVQUFVLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ2pDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDNUQsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUVyQywrQ0FBK0M7Z0JBQy9DLFVBQVUsQ0FBQyxlQUFlLENBQUMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUVwRSxJQUFJLGNBQVksR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDO2dCQUN2QyxJQUFJLEtBQUssR0FBRyxvQkFBb0IsQ0FBQztnQkFDakMsSUFBSSxRQUFRLEdBQUcsd0JBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFJLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2dCQUM5RSxhQUFhLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsY0FBWSxDQUFDLE1BQU0sR0FBRyxLQUFLLEdBQUcsV0FBVyxDQUFDLENBQUM7d0NBRTFFLENBQUM7b0JBQ1IsSUFBSSxJQUFJLEdBQUcsY0FBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMzQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO29CQUU1QixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztvQkFDekIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7b0JBRXBCLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQzt5QkFDaEIsS0FBSyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7eUJBQ2hCLElBQUksQ0FDSDt3QkFDRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzt3QkFDbkIsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFHLEVBQUMsUUFBUSxFQUFFLE9BQU8sRUFBQyxDQUFDOzZCQUNqRCxJQUFJLENBQUM7NEJBQ0osY0FBYzs0QkFDZCx5QkFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMseUJBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDeEQsSUFBRyxDQUFDLElBQUksY0FBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUM7Z0NBQzlCLElBQUcsUUFBUTtvQ0FBRSxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUNyQzt3QkFDSCxDQUFDLENBQUM7NkJBQ0QsS0FBSyxFQUFFLENBQUM7b0JBQ2IsQ0FBQyxDQUNGO3lCQUNBLEtBQUssRUFBRSxDQUFDOztnQkF2QmIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGNBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFOzRCQUFuQyxDQUFDO2lCQXdCVDthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPLGFBQWEsQ0FBQztJQUN2QixDQUFDO0lBR0gscUNBQXFDO0lBQ3JDLDBDQUFtQixHQUFuQixVQUFvQixPQUFZLEVBQUUsSUFBYSxFQUFFLFNBQWlCLEVBQUUsUUFBbUI7UUFBdkYsaUJBMkJDO1FBM0JtRSx5QkFBQSxFQUFBLFlBQW1CO1FBQ3JGLElBQUksV0FBVyxHQUFHO1lBQ2hCLFNBQVMsRUFBRSxJQUFJO1lBQ2YsUUFBUSxFQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSTtTQUV4QixDQUFBO1FBRUQsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUVwQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEQsSUFBSSxVQUFVLEdBQXVCLEVBQUUsQ0FBQztRQUN4QyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTtZQUN2QixJQUFJLFFBQVEsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDakQsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQztZQUM1QyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUV0QyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksb0JBQW9CLEdBQUcsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUN0QyxRQUFRLEVBQUUsb0JBQW9CLEVBQUUsV0FBVyxFQUMzQyxJQUFJLEVBQUcsVUFBQyxJQUFzQjtZQUM5QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxDQUFDO1FBQ1AsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCwwQ0FBbUIsR0FBbkIsVUFBb0IsT0FBNkI7UUFBakQsaUJBVUM7UUFUQyxPQUFPLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDbEMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBRTlCLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO1lBQ25DLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBR0QseUNBQWtCLEdBQWxCO1FBQ0UsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2hELElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJOztnQkFDbEMsTUFBQSxJQUFJLENBQUMsSUFBSSwwQ0FBRSxPQUFPLEdBQUc7WUFDdkIsQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztZQUU5QixPQUFPLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTs7Z0JBQ25DLE1BQUEsSUFBSSxDQUFDLElBQUksMENBQUUsT0FBTyxHQUFHO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLG1CQUFtQixFQUFFLENBQUM7U0FDL0I7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFFRCx1Q0FBZ0IsR0FBaEIsVUFBaUIsUUFBZTtRQUM5QixJQUFHLFFBQVEsSUFBRyxJQUFJLEVBQUM7WUFDakIsT0FBTyxDQUFDLDZCQUFhLENBQUMsTUFBTSxFQUFFLDZCQUFhLENBQUMsTUFBTSxFQUFFLDZCQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDM0U7YUFBSyxJQUFHLFFBQVEsSUFBRyxLQUFLLEVBQUM7WUFDeEIsT0FBTyxDQUFDLDZCQUFhLENBQUMsTUFBTSxFQUFFLDZCQUFhLENBQUMsTUFBTSxFQUFFLDZCQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDM0U7YUFBSyxJQUFHLFFBQVEsSUFBRyxNQUFNLEVBQUM7WUFDekIsT0FBTyxDQUFDLDZCQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDakM7YUFBSyxJQUFHLFFBQVEsSUFBRyxPQUFPLEVBQUM7WUFDMUIsT0FBTyxDQUFDLDZCQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDL0I7YUFBSTtZQUNILE9BQU8sQ0FBQyw2QkFBYSxDQUFDLE1BQU0sRUFBRSw2QkFBYSxDQUFDLE1BQU0sRUFBRSw2QkFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzNFO0lBQ0gsQ0FBQztJQUdDLDJDQUFvQixHQUFwQixVQUFxQixPQUE2QjtRQUFsRCxpQkFnQkM7UUFmQyxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNuQixJQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFDO1lBQ2pDLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUU7Z0JBQzNDLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQzthQUN0RDtpQkFBSTtnQkFDSCxTQUFTLEdBQUcsQ0FBQyxDQUFDO2FBQ2Y7WUFFRCxTQUFTLElBQUksQ0FBQyxDQUFDO1NBQ2hCO1FBRUQsSUFBSSxFQUFFLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQy9FLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO1lBQ2IsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw2Q0FBc0IsR0FBdEIsVUFBdUIsVUFBbUIsRUFBRSxRQUE2QixFQUNqRCxRQUFnQixFQUFFLG9CQUFnQyxFQUFFLFdBQWUsRUFDbkUsd0JBQXdDLEVBQUUsVUFBNkM7UUFGL0csaUJBZ0NDO1FBL0J5QyxxQ0FBQSxFQUFBLHdCQUFnQztRQUNsRCx5Q0FBQSxFQUFBLCtCQUF3QztnQ0FDckQsQ0FBQztZQUNSLElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2QixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUVwQyxJQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksRUFBRTs7YUFHNUI7WUFFRCxPQUFLLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBRXpDLElBQUksS0FBSyxHQUFZLElBQUksQ0FBQztZQUMxQixJQUFHLFdBQVcsQ0FBQyxTQUFTLEVBQUM7Z0JBQ3ZCLElBQUksUUFBUSxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZFLElBQUksTUFBTSxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN6RSxLQUFLLEdBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxvQkFBb0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBQyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQzthQUNuSztpQkFBSTtnQkFDSCxLQUFLLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxvQkFBb0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUcsV0FBVyxDQUFDLENBQUM7YUFDeEY7WUFFRCxLQUFLLENBQUMsSUFBSSxDQUNSO2dCQUNFLElBQUcsd0JBQXdCLElBQUksS0FBSztvQkFDbEMsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDO2dCQUM3QyxJQUFHLFVBQVUsSUFBSSxJQUFJO29CQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQyxDQUFDLENBQ0Y7aUJBQ0EsS0FBSyxFQUFFLENBQUM7OztRQTNCWCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUU7b0JBQS9CLENBQUM7U0E0QlQ7SUFDSCxDQUFDO0lBRUQsa0JBQWtCO0lBQ2xCLDRCQUFLLEdBQUwsVUFBTSxJQUFZLEVBQUUsVUFBbUIsRUFBRSxXQUFlLEVBQUUsVUFBc0I7UUFDOUUsSUFBSSxRQUFRLEdBQUcsV0FBVyxDQUFDLFFBQVEsSUFBSSxDQUFDLENBQUM7UUFDekMsSUFBSSxLQUFLLEdBQUcsV0FBVyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUM7UUFDbkMsSUFBSSxTQUFTLEdBQUcsV0FBVyxDQUFDLG9CQUFvQixJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ2pFLElBQUksV0FBVyxHQUFHLFdBQVcsQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFFMUQsU0FBUyxHQUFHLHdCQUFjLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFFbkYsSUFBSSxLQUFLLEdBQVksSUFBSSxDQUFDO1FBRTFCLElBQUksUUFBUSxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdELElBQUksTUFBTSxHQUFHLFNBQVMsQ0FBQztRQUN2QixJQUFHLFdBQVcsQ0FBQyxTQUFTLEVBQUM7WUFDdkIsS0FBSyxHQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztTQUN4RjthQUFJO1lBQ0gsS0FBSyxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUcsRUFBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUM7U0FDL0U7UUFFRCxLQUFLLENBQUMsSUFBSSxDQUNSO1lBQ0UsVUFBVSxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBQzdCLENBQUMsQ0FDRjthQUNBLEtBQUssRUFBRSxDQUFDO0lBQ1gsQ0FBQztJQUNILFVBQVU7SUFFVixzQ0FBc0M7SUFDdEMsNkNBQXNCLEdBQXRCLFVBQXVCLGNBQXVEO1FBQTlFLGlCQUtDO1FBSkMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFNBQVM7WUFDOUIsSUFBSSxJQUFJLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztZQUM3QyxJQUFJLGFBQUosSUFBSSx1QkFBSixJQUFJLENBQUUsYUFBYSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsRUFBRTtRQUNoRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw0Q0FBcUIsR0FBckIsVUFBc0IsZUFBd0Q7UUFBOUUsaUJBS0M7UUFKQyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUEsU0FBUztZQUMvQixJQUFJLElBQUksR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1lBQzdDLElBQUksYUFBSixJQUFJLHVCQUFKLElBQUksQ0FBRSxXQUFXLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxFQUFFO1FBQzlDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFVBQVU7SUFFVixxQkFBcUI7SUFDckIsZ0NBQVMsR0FBVCxVQUFVLE1BQVc7UUFDbkIsSUFBRyxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBQztZQUN6QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUNqQztTQUNGO0lBQ0gsQ0FBQztJQUVELGtDQUFXLEdBQVgsVUFBWSxLQUFpQztRQUMzQyxJQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsSUFBSSxNQUFNLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUUsSUFBSSxNQUFNLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUUsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFDO1lBQzFKLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsK0JBQStCLEVBQUUsQ0FBQyxDQUFDO1NBQ3RFO1FBRUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsaUNBQVUsR0FBVixVQUFXLEtBQWlDLEVBQUUsYUFBcUI7UUFBbkUsaUJBa0JDO1FBakJDLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUUvQixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDO1FBRTVDLElBQUcsTUFBTSxJQUFJLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxTQUFTLEVBQUM7WUFDeEQsS0FBSyxDQUFDLHlCQUF5QixFQUFFLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRztnQkFDM0MsSUFBSSxRQUFRLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztnQkFDM0MsSUFBRyxRQUFRLENBQUMsZUFBZSxDQUFDLE1BQU0sSUFBSSxDQUFDO29CQUNyQyxLQUFJLENBQUMsb0NBQW9DLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzVFLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFFRCxJQUFHLE1BQU0sSUFBSSxNQUFNLENBQUMsT0FBTyxJQUFFLE1BQU0sSUFBSSxNQUFNLENBQUMsU0FBUyxFQUFDO1lBQ3RELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUNqQztTQUNGO0lBQ0gsQ0FBQztJQUdELHNCQUFXLHFDQUFXO1FBRHRCLHFCQUFxQjthQUNyQjtZQUNFLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztRQUMzQixDQUFDO2FBQ0QsVUFBdUIsS0FBYztZQUFyQyxpQkFXQztZQVZDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQkFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLElBQUcsQ0FBQyxLQUFLLEVBQUM7b0JBQ1IsSUFBRyxLQUFJLENBQUMsbUJBQW1CLElBQUUsSUFBSTt3QkFDL0IsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLENBQUM7O3dCQUVqRCxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7aUJBQzdDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDOzs7T0FaQTtJQWNELFdBQVc7SUFDWCx5Q0FBa0IsR0FBbEIsVUFBbUIsUUFBaUI7UUFDbEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDO0lBQy9DLENBQUM7SUFFRCxxQ0FBYyxHQUFkLFVBQWUsUUFBaUI7UUFDOUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDO0lBQzNDLENBQUM7SUFFRCxVQUFVO0lBQ1YsZ0NBQVMsR0FBVDtRQUNFLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsNkJBQU0sR0FBTjtRQUNFLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVwQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDaEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUNoQyxpREFBaUQ7U0FDbEQ7UUFDRCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBaCtCRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDO3lEQUNjO0lBSXRDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7eURBQ2E7SUFHakM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztxREFDVTtJQUc5QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3lEQUNjO0lBR2xDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7c0RBQ1U7SUFJNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztzREFDVTtJQUk1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3dEQUNZO0lBRzlCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7b0RBQ1E7SUE1QlAsWUFBWTtRQURoQyxPQUFPO09BQ2EsWUFBWSxDQXErQmhDO0lBQUQsbUJBQUM7Q0FyK0JELEFBcStCQyxDQXIrQnlDLEVBQUUsQ0FBQyxTQUFTLEdBcStCckQ7a0JBcitCb0IsWUFBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBTaWNib01vZHVsZUFkYXB0ZXIgZnJvbSBcIi4uLy4uLy4uLy4uL1NpY2JvTW9kdWxlQWRhcHRlclwiO1xuXG5jb25zdCB7XG4gIERvb3IsXG4gIFN0YXRlLFxuICBEb29yQW5kQmV0QW1vdW50LFxuICBTdGF0dXN9ID0gU2ljYm9Nb2R1bGVBZGFwdGVyLmdldEFsbFJlZnMoKTtcblxuXG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5pbXBvcnQgU2ljYm9VSUNvbXAgZnJvbSBcIi4vU2ljYm9VSUNvbXBcIjtcbmltcG9ydCBTaWNib0hlbHBlciBmcm9tIFwiLi4vSGVscGVycy9TaWNib0hlbHBlclwiO1xuaW1wb3J0IFNpY2JvQ29pbkl0ZW1Db21wLCB7IFNpY2JvQ29pbkl0ZW1Db21wRGF0YSB9IGZyb20gXCIuL0l0ZW1zL1NpY2JvQ29pbkl0ZW1Db21wXCI7XG5pbXBvcnQgU2ljYm9CZXRTbG90SXRlbUNvbXAgZnJvbSBcIi4vSXRlbXMvU2ljYm9CZXRTbG90SXRlbUNvbXBcIjtcblxuXG5pbXBvcnQgeyBTaWNib1NvdW5kIH0gZnJvbSBcIi4uL1NldHRpbmcvU2ljYm9TZXR0aW5nXCI7XG5pbXBvcnQgSVNpY2JvU3RhdGUgZnJvbSBcIi4vU3RhdGVzL0lTaWNib1N0YXRlXCI7XG5pbXBvcnQgU2ljYm9Db250cm9sbGVyIGZyb20gXCIuLi9Db250cm9sbGVycy9TaWNib0NvbnRyb2xsZXJcIjtcbmltcG9ydCBTaWNib1Njcm9sbFZpZXdXaXRoQnV0dG9uIGZyb20gJy4uL1JOR0NvbW1vbnMvVUlDb21wb25lbnRzL1NpY2JvU2Nyb2xsVmlld1dpdGhCdXR0b24nO1xuaW1wb3J0IFNpY2JvR2FtZVV0aWxzIGZyb20gXCIuLi9STkdDb21tb25zL1V0aWxzL1NpY2JvR2FtZVV0aWxzXCI7XG5pbXBvcnQgeyBTaWNib0NvaW5UeXBlIH0gZnJvbSBcIi4uL1JOR0NvbW1vbnMvU2ljYm9Db2luVHlwZVwiO1xuXG5cblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvQmV0Q29tcCBleHRlbmRzIGNjLkNvbXBvbmVudCBpbXBsZW1lbnRzIElTaWNib1N0YXRleyBcbiAgc2ljYm9VSUNvbXA6U2ljYm9VSUNvbXAgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5TY3JvbGxWaWV3KVxuICBjaGlwc1Njcm9sbFZpZXc6IGNjLlNjcm9sbFZpZXcgPSBudWxsO1xuICBjdXJyZW50U2VsZWN0ZWRDaGlwOiBTaWNib0NvaW5JdGVtQ29tcCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLlByZWZhYilcbiAgbGFyZ2VDaGlwUHJlZmFiOmNjLlByZWZhYiA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLkJ1dHRvbilcbiAgcmViZXRCdXR0b246IGNjLkJ1dHRvbiA9IG51bGw7XG4gIFxuICBAcHJvcGVydHkoY2MuQnV0dG9uKVxuICBkb3VibGVCZXRCdXR0b246IGNjLkJ1dHRvbiA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIGJldFNsb3RzTm9kZTpjYy5Ob2RlID0gbnVsbDtcbiAgYWxsQmV0U2xvdHM6IFNpY2JvQmV0U2xvdEl0ZW1Db21wW10gPSBbXTtcblxuICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgbWluaUNoaXBSb290OmNjLk5vZGUgPSBudWxsO1xuICBtaW5pQ2hpcFBvb2w6Y2MuTm9kZVBvb2wgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBsb3NlQ2hpcFRhcmdldDpjYy5Ob2RlID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgd2luQ2hpcFBvczpjYy5Ob2RlID0gbnVsbDtcblxuICBwcml2YXRlIF9kaXNhYmxlQ2hpcDogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIHByaXZhdGUgY2hpcF9kYXRhID0gW107XG4gIHByaXZhdGUgY2hpcEl0ZW1zIDogU2ljYm9Db2luSXRlbUNvbXBbXSA9IFtdO1xuLy9BTkNIT1IgY2hpcCBzdGFjayBwb3MgY29uZmlnXG4gIGNoaXBTdGFja1Bvc0NvbmZpZyA9IHtcbiAgICBhbnlUcmlwcGxlQ2xvbmVTcGFjaW5nOiAzMCxcbiAgICBzaW5nbGVDbG9uZVNwYWNpbmc6IDMsXG4gICAgc21hbGxDbG9uZVNwYWNpbmc6IDQwLFxuICAgIGJpZ0Nsb25lU3BhY2luZzogLTQwLFxuICAgIGNsb25lU3BhY2luZzogNCxcbiAgICBjaGlwU3BhY2luZzogNCxcblxuICAgIGNoaXBTdGFja09mZnNldFJpZ2h0OiBuZXcgY2MuVmVjMiggLTI3LCAyNSksXG4gICAgY2hpcFN0YWNrT2Zmc2V0TGVmdDogbmV3IGNjLlZlYzIoIDI3LCAyNSksXG5cbiAgICBhbnlUcmlwcGxlIDogW1xuICAgICAge1xuICAgICAgICAgIHBvczogbmV3IGNjLlZlYzIoIC04NCwgLTM0KSxcbiAgICAgICAgICBoZWlnaHQ6IDEwLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtNTYsIC0zMyksXG4gICAgICAgICAgaGVpZ2h0OiAxMCxcbiAgICAgIH1cbiAgICBdLCBcblxuICAgIHRyaXBwbGVMZWZ0IDogW1xuICAgICAge1xuICAgICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtODIsIC04KSxcbiAgICAgICAgaGVpZ2h0OiAxMCxcbiAgICAgIH0sXG4gICAgXSxcblxuICAgIHRyaXBwbGVSaWdodCA6IFtcbiAgICAgIHtcbiAgICAgICAgcG9zOiBuZXcgY2MuVmVjMiggODIsIC04KSxcbiAgICAgICAgaGVpZ2h0OiAxMCxcbiAgICAgIH0sXG4gICAgXSwgXG5cbiAgICBzbWFsbCA6IFtcbiAgICAgIHtcbiAgICAgICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtNzUuNSwgLTUwKSxcbiAgICAgICAgICBoZWlnaHQ6IDEwLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtNDYsIC01MSksXG4gICAgICAgICAgaGVpZ2h0OiAxMCxcbiAgICAgIH0sXG4gICAgICAvLyB7XG4gICAgICAvLyAgICAgcG9zOiBuZXcgY2MuVmVjMiggLTU0LCAtNzQuNSksXG4gICAgICAvLyAgICAgaGVpZ2h0OiA1LFxuICAgICAgLy8gfVxuICAgXSwgXG5cbiAgIGJpZyA6IFtcbiAgICB7XG4gICAgICBwb3M6IG5ldyBjYy5WZWMyKDc2LjUsIC01MSksXG4gICAgICBoZWlnaHQ6IDEwLFxuICAgIH0sXG4gICAge1xuICAgICAgcG9zOiBuZXcgY2MuVmVjMig0NywgLTUwKSxcbiAgICAgIGhlaWdodDogMTAsXG4gICAgfSxcbiAgIFxuICAgIC8vIHtcbiAgICAvLyAgICAgcG9zOiBuZXcgY2MuVmVjMiggNTQuNSwgLTc0LjUpLFxuICAgIC8vICAgICBoZWlnaHQ6IDUsXG4gICAgLy8gfVxuICBdLCBcblxuICBmb3VyX3N1bSA6IFtcbiAgICB7XG4gICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtMjAsIC0yMy41KSwgLy8tNDQuNSksXG4gICAgICBoZWlnaHQ6IDEwLFxuICAgIH0sIFxuICBdLFxuXG4gIGZpdmVfc3VtIDogW1xuICAgIHtcbiAgICAgIHBvczogbmV3IGNjLlZlYzIoIC0yMCwgLTIzLjUpLCAvLy00NC41KSxcbiAgICAgIGhlaWdodDogMTAsXG4gICAgfSwgXG4gIF0sXG5cblxuICBzaXhfc3VtIDogW1xuICAgIHtcbiAgICAgIHBvczogbmV3IGNjLlZlYzIoIC0yMywgLTIzLjUpLCAvLy00NC41KSxcbiAgICAgIGhlaWdodDogMTAsXG4gICAgfSwgXG4gIF0sXG5cblxuICBzZXZlbl9zdW0gOiBbXG4gICAge1xuICAgICAgcG9zOiBuZXcgY2MuVmVjMiggLTIwLCAtMjMuNSksIC8vLTQ0LjUpLFxuICAgICAgaGVpZ2h0OiAxMCxcbiAgICB9LCBcbiAgXSxcblxuXG4gIGVpZ2h0X3N1bSA6IFtcbiAgICB7XG4gICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtMjAsIC0yMy41KSwgLy8tNDQuNSksXG4gICAgICBoZWlnaHQ6IDEwLFxuICAgIH0sIFxuICBdLFxuXG4gIG5pbmVfc3VtIDogW1xuICAgIHtcbiAgICAgIHBvczogbmV3IGNjLlZlYzIoIC0xOSwgLTIzLjUpLCAvLy00NC41KSxcbiAgICAgIGhlaWdodDogMTAsXG4gICAgfSwgXG4gIF0sXG5cbiAgdGVuX3N1bSA6IFtcbiAgICB7XG4gICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtMTksIC0yMy41KSwgLy8tNDQuNSksXG4gICAgICBoZWlnaHQ6IDEwLFxuICAgIH0sIFxuICBdLFxuXG5cbiAgZWxldmVuX3N1bSA6IFtcbiAgICB7XG4gICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtMTgsIC0yMy41KSwgLy8tNDQuNSksXG4gICAgICBoZWlnaHQ6IDEwLFxuICAgIH0sIFxuICBdLFxuXG5cbiAgdHdlbHZlX3N1bSA6IFtcbiAgICB7XG4gICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtMTgsIC0yMy41KSwgLy8tNDQuNSksXG4gICAgICBoZWlnaHQ6IDEwLFxuICAgIH0sIFxuICBdLFxuXG5cbiAgdGhpcnRlZW5fc3VtIDogW1xuICAgIHtcbiAgICAgIHBvczogbmV3IGNjLlZlYzIoIC0xNi41LCAtMjMuNSksIC8vLTQ0LjUpLFxuICAgICAgaGVpZ2h0OiAxMCxcbiAgICB9LCBcbiAgXSxcblxuICBmb3VydGVlbl9zdW0gOiBbXG4gICAge1xuICAgICAgcG9zOiBuZXcgY2MuVmVjMiggLTE2LCAtMjMuNSksIC8vLTQ0LjUpLFxuICAgICAgaGVpZ2h0OiAxMCxcbiAgICB9LCBcbiAgXSxcblxuICBmaWZ0ZWVuX3N1bSA6IFtcbiAgICB7XG4gICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtMTUuNSwgLTIzLjUpLCAvLy00NC41KSxcbiAgICAgIGhlaWdodDogMTAsXG4gICAgfSwgXG4gIF0sXG5cblxuICBzaXh0ZWVuX3N1bSA6IFtcbiAgICB7XG4gICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtMTUuNSwgLTIzLjUpLCAvLy00NC41KSxcbiAgICAgIGhlaWdodDogMTAsXG4gICAgfSwgXG4gIF0sXG5cblxuICBzZXZlbnRlZW5fc3VtIDogW1xuICAgIHtcbiAgICAgIHBvczogbmV3IGNjLlZlYzIoIC0xNC41LCAtMjMuNSksIC8vLTQ0LjUpLFxuICAgICAgaGVpZ2h0OiAxMCxcbiAgICB9LCBcbiAgXSxcblxuICBzaW5nbGUgOiBbXG4gICAge1xuICAgICAgICBwb3M6IG5ldyBjYy5WZWMyKCAtNTcuNSwgLTIyKSxcbiAgICAgICAgaGVpZ2h0OiAxMCxcbiAgICB9LFxuICAgIC8vIHtcbiAgICAvLyAgICAgcG9zOiBuZXcgY2MuVmVjMiggLTI5LjUsIC0yMSksXG4gICAgLy8gICAgIGhlaWdodDogNSxcbiAgICAvLyB9XG4gIF0sIFxuIFxufVxuICBcbiAgb25Mb2FkKCl7XG4gICAgdGhpcy5zaWNib1VJQ29tcCA9IHRoaXMubm9kZS5wYXJlbnQuZ2V0Q29tcG9uZW50SW5DaGlsZHJlbihTaWNib1VJQ29tcCk7XG5cbiAgICB0aGlzLmFsbEJldFNsb3RzID0gdGhpcy5iZXRTbG90c05vZGUuZ2V0Q29tcG9uZW50c0luQ2hpbGRyZW4oU2ljYm9CZXRTbG90SXRlbUNvbXApOyAgICBcblxuICAgIHRoaXMubWluaUNoaXBQb29sID0gbmV3IGNjLk5vZGVQb29sKCk7ICAgICAgICBcbiAgfVxuXG4gIGdldERvb3IoZG9vcik6U2ljYm9CZXRTbG90SXRlbUNvbXB7XG4gICAgcmV0dXJuIHRoaXMuYWxsQmV0U2xvdHMuZmluZCh4PT54LmRvb3IgPT0gZG9vcik7XG4gIH1cbiAgXG4gIC8vU0VDVElPTiBDaGlwIHNjcm9sbCB2aWV3XG4gIGNyZWF0ZUNoaXBJdGVtcyhkYXRhOiBudW1iZXJbXSkge1xuICAgIHRoaXMuY2hpcF9kYXRhID0gZGF0YTtcbiAgICB0aGlzLmNyZWF0ZUl0ZW1DaGlwQ29pbnMoZGF0YSk7XG4gICAgdGhpcy5zZWxlY3RDaGlwKHRoaXMuY2hpcEl0ZW1zWzBdLCBmYWxzZSk7XG4gICAgdGhpcy5jaGlwc1Njcm9sbFZpZXcuc2Nyb2xsVG9Ub3BMZWZ0KCk7XG4gICAgdGhpcy5jaGlwc1Njcm9sbFZpZXcuZ2V0Q29tcG9uZW50KFNpY2JvU2Nyb2xsVmlld1dpdGhCdXR0b24pPy5jaGVja1Njcm9sbFZpZXdCdXR0b24oKTtcbiAgfVxuXG4gIFxuICBwdWJsaWMgY3JlYXRlSXRlbUNoaXBDb2lucyhkYXRhOiBudW1iZXJbXSkge1xuICAgIGRhdGEuZm9yRWFjaCgoY2hpcFZhbHVlKSA9PiB7XG4gICAgICB0aGlzLmNyZWF0ZUl0ZW1Db2luKGNoaXBWYWx1ZSk7XG4gICAgfSk7XG4gIH1cblxuICBjcmVhdGVJdGVtQ29pbihjaGlwVmFsdWU6IG51bWJlcikgeyAgICBcbiAgICBsZXQgY29pblR5cGUgPSBTaWNib0hlbHBlci5jb252ZXJ0VmFsdWVUb0NvaW5UeXBlKGNoaXBWYWx1ZSk7XG4gICAgdmFyIG5ld0l0ZW0gPSBTaWNib0dhbWVVdGlscy5jcmVhdGVJdGVtRnJvbVByZWZhYihTaWNib0NvaW5JdGVtQ29tcCwgdGhpcy5sYXJnZUNoaXBQcmVmYWIsIHRoaXMuY2hpcHNTY3JvbGxWaWV3LmNvbnRlbnQpO1xuXG4gICAgbGV0IGl0ZW1EYXRhID0gbmV3IFNpY2JvQ29pbkl0ZW1Db21wRGF0YSgpO1xuICAgIGl0ZW1EYXRhLmNvaW5UeXBlID0gY29pblR5cGU7ICAgIFxuICAgIGl0ZW1EYXRhLm9uQ2xpY2sgPSAoc2ljYm9Db2luSXRlbUNvbXApID0+IHtcbiAgICAgIHRoaXMuc2VsZWN0Q2hpcChzaWNib0NvaW5JdGVtQ29tcCk7XG4gICAgfTtcbiAgICBuZXdJdGVtLnNldERhdGEoaXRlbURhdGEpOyAgXG4gICAgbmV3SXRlbS5pc0Rpc2FibGUgPSB0aGlzLmRpc2FibGVDaGlwOyAgXG4gICAgdGhpcy5jaGlwSXRlbXMucHVzaChuZXdJdGVtKTtcbiAgfVxuICBcbiAgc2VsZWN0Q2hpcChzaWNib0NvaW5JdGVtQ29tcDogU2ljYm9Db2luSXRlbUNvbXAsIHBsYXlTZng6IGJvb2xlYW4gPSB0cnVlKXtcbiAgICBpZih0aGlzLmRpc2FibGVDaGlwKSByZXR1cm47XG4gICAgaWYodGhpcy5jdXJyZW50U2VsZWN0ZWRDaGlwIT1udWxsKSB0aGlzLmN1cnJlbnRTZWxlY3RlZENoaXAuc2VsZWN0RWZmKGZhbHNlKTtcbiAgICB0aGlzLmN1cnJlbnRTZWxlY3RlZENoaXAgPSBzaWNib0NvaW5JdGVtQ29tcDtcbiAgICB0aGlzLmN1cnJlbnRTZWxlY3RlZENoaXAuc2VsZWN0RWZmKHRydWUpO1xuICAgIC8vVE9ETzogY2hhbmdlIHNvdW5kIGtoaSBiYW0gY2hpcCBiaW5oIHRodW9uZyB2YSBkaXNhYmxlXG4gICAgaWYocGxheVNmeClcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4Q2xpY2spO1xuICAgIFxuICB9XG5cbiAgZ2V0U2VsZWN0ZWRDaGlwVHlwZSgpOlNpY2JvQ29pblR5cGV7XG4gICAgcmV0dXJuIHRoaXMuY3VycmVudFNlbGVjdGVkQ2hpcC5kYXRhLmNvaW5UeXBlO1xuICB9XG5cbiAgb25DbGlja0NoaXBTY3JvbGxOZXh0KCl7XG4gICAgbGV0IG51bU9mSXRlbSA9IHRoaXMuY2hpcHNTY3JvbGxWaWV3LmNvbnRlbnQuY2hpbGRyZW5Db3VudDtcbiAgICBsZXQgc3BhY2luZyA9IHRoaXMuY2hpcHNTY3JvbGxWaWV3LmNvbnRlbnQud2lkdGgvbnVtT2ZJdGVtO1xuXG4gICAgbGV0IG9mZnNldCA9IHRoaXMuY2hpcHNTY3JvbGxWaWV3LmdldFNjcm9sbE9mZnNldCgpO1xuICAgIGxldCBtYXhPZmZzZXQgPSB0aGlzLmNoaXBzU2Nyb2xsVmlldy5nZXRNYXhTY3JvbGxPZmZzZXQoKTtcblxuICAgIHRoaXMuY2hpcHNTY3JvbGxWaWV3LnNjcm9sbFRvUGVyY2VudEhvcml6b250YWwoKE1hdGguYWJzKG9mZnNldC54KSArIHNwYWNpbmcpL21heE9mZnNldC54LCAuNSk7XG4gIH1cblxuICBvbkNsaWNrQ2hpcFNjcm9sbFByZSgpe1xuICAgIGxldCBudW1PZkl0ZW0gPSB0aGlzLmNoaXBzU2Nyb2xsVmlldy5jb250ZW50LmNoaWxkcmVuQ291bnQ7XG4gICAgbGV0IHNwYWNpbmcgPSB0aGlzLmNoaXBzU2Nyb2xsVmlldy5jb250ZW50LndpZHRoL251bU9mSXRlbTtcblxuICAgIGxldCBvZmZzZXQgPSB0aGlzLmNoaXBzU2Nyb2xsVmlldy5nZXRTY3JvbGxPZmZzZXQoKTtcbiAgICBsZXQgbWF4T2Zmc2V0ID0gdGhpcy5jaGlwc1Njcm9sbFZpZXcuZ2V0TWF4U2Nyb2xsT2Zmc2V0KCk7XG5cbiAgICB0aGlzLmNoaXBzU2Nyb2xsVmlldy5zY3JvbGxUb1BlcmNlbnRIb3Jpem9udGFsKChNYXRoLmFicyhvZmZzZXQueCkgLSBzcGFjaW5nKS9tYXhPZmZzZXQueCwgLjUpO1xuICB9XG5cbiAgc2Nyb2xsVG9JbnNpZGVWaWV3KGNoaXA6IGNjLk5vZGUpe1xuICAgIHRoaXMuY2hpcHNTY3JvbGxWaWV3LnN0b3BBdXRvU2Nyb2xsKCk7XG4gICAgbGV0IHBvcyA9IFNpY2JvR2FtZVV0aWxzLmNvbnZlcnRUb090aGVyTm9kZShjaGlwLCB0aGlzLmNoaXBzU2Nyb2xsVmlldy5ub2RlKTtcbiAgICBpZihwb3MueD49LTIzMCAmJiBwb3MueDw9MjAwKSByZXR1cm47Ly8gSW5zaWRlIHZpZXdcblxuICAgIGxldCBvZmZzZXQgPSB0aGlzLmNoaXBzU2Nyb2xsVmlldy5nZXRTY3JvbGxPZmZzZXQoKTtcbiAgICBsZXQgbWF4T2Zmc2V0ID0gdGhpcy5jaGlwc1Njcm9sbFZpZXcuZ2V0TWF4U2Nyb2xsT2Zmc2V0KCk7XG5cbiAgICB0aGlzLmNoaXBzU2Nyb2xsVmlldy5zY3JvbGxUb1BlcmNlbnRIb3Jpem9udGFsKChNYXRoLmFicyhvZmZzZXQueCkgKyBwb3MueCkvbWF4T2Zmc2V0LngpO1xuICAgIHRoaXMuY2hpcHNTY3JvbGxWaWV3LmdldENvbXBvbmVudChTaWNib1Njcm9sbFZpZXdXaXRoQnV0dG9uKT8uY2hlY2tTY3JvbGxWaWV3QnV0dG9uKCk7XG4gIH1cbiAgLy8hU0VDVElPTlxuXG4gIC8vI3JlZ2lvbiBCRVQgSU4gVEFCTEVcbiAgICAvKiogXG4gICAgKiBCYW0gdmFvIG8gZGF0IGN1b2MgdHJlbiBiYW5cblx0XHQqL1xuICAgIG9uQ2xpY2tCZXRTbG90SXRlbShiZXRTbG90SXRlbUNvbXA6IFNpY2JvQmV0U2xvdEl0ZW1Db21wKXtcbiAgICAgIGlmKHRoaXMuZGlzYWJsZUNoaXApIHJldHVybjtcbiAgICAgIHRoaXMuc2Nyb2xsVG9JbnNpZGVWaWV3KHRoaXMuY3VycmVudFNlbGVjdGVkQ2hpcC5ub2RlKTtcbiAgICAgIHRoaXMuc2ljYm9VSUNvbXAubXlVc2VyQmV0KGJldFNsb3RJdGVtQ29tcC5kb29yLCB0aGlzLmN1cnJlbnRTZWxlY3RlZENoaXAuZGF0YS5jb2luVHlwZSk7XG4gICAgICAvLyB0aGlzLnB1dE1pbmlDaGlwT25UYWJsZUltbWVkaWF0ZWx5KHRoaXMuY3VycmVudFNlbGVjdGVkQ2hpcC5kYXRhLmNvaW5UeXBlLCBiZXRTbG90SXRlbUNvbXApO1xuICAgIH1cblxuICBjcmVhdGVJdGVtQ29pbk1pbmkoY29pblR5cGU6IFNpY2JvQ29pblR5cGUpIHsgICAgXG4gICAgbGV0IG5ld0l0ZW06U2ljYm9Db2luSXRlbUNvbXAgPSBudWxsOyAgICBcbiAgICBpZiAodGhpcy5taW5pQ2hpcFBvb2wuc2l6ZSgpID4gMCkgeyBcbiAgICAgIG5ld0l0ZW0gPSB0aGlzLm1pbmlDaGlwUG9vbC5nZXQoKS5nZXRDb21wb25lbnQoXCJTaWNib0NvaW5JdGVtQ29tcFwiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbmV3SXRlbSA9IFNpY2JvR2FtZVV0aWxzLmNyZWF0ZUl0ZW1Gcm9tUHJlZmFiKFNpY2JvQ29pbkl0ZW1Db21wLCB0aGlzLmxhcmdlQ2hpcFByZWZhYiwgdGhpcy5jaGlwc1Njcm9sbFZpZXcuY29udGVudCk7XG4gICAgfVxuICAgIG5ld0l0ZW0ubm9kZS5wYXJlbnQgPSB0aGlzLm1pbmlDaGlwUm9vdDtcbiAgICBuZXdJdGVtLm5vZGUuc2NhbGUgPSAxO1xuICAgIG5ld0l0ZW0ubm9kZS5vcGFjaXR5ID0gMjU1O1xuXG4gICAgbGV0IGl0ZW1EYXRhID0gbmV3IFNpY2JvQ29pbkl0ZW1Db21wRGF0YSgpO1xuICAgIGl0ZW1EYXRhLmNvaW5UeXBlID0gY29pblR5cGU7ICAgIFxuICAgIGl0ZW1EYXRhLmlzTWluaSA9IHRydWU7XG4gICAgbmV3SXRlbS5zZXREYXRhKGl0ZW1EYXRhKTtcbiAgICBcbiAgICAvLyBuZXdJdGVtLnJvdGF0aW9uID0gLXRoaXMucmFuZG9tUmFuZ2UoMCwgMzYwKTtcblxuICAgIC8vQU5DSE9SIG1pbmljaGlwIFNjYWxlXG4gICAgLy8gbmV3SXRlbS5pc09uVGFibGUgPSB0cnVlO1xuICAgIHJldHVybiBuZXdJdGVtO1xuICB9XG5cbiAgcmVjeWNsZUl0ZW1Db2luTWluaShpdGVtOiBjYy5Ob2RlKXtcbiAgICBpdGVtLm9wYWNpdHkgPSAyNTU7XG4gICAgdGhpcy5taW5pQ2hpcFBvb2wucHV0KGl0ZW0pOyAgICBcbiAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQoaXRlbSk7XG4gIH1cblxuICBnZXRSYW5kb21Qb3MoYmV0U2xvdEl0ZW1Db21wOiBTaWNib0JldFNsb3RJdGVtQ29tcCk6Y2MuVmVjMntcbiAgICBsZXQgYmV0QXJlYSA9IGJldFNsb3RJdGVtQ29tcC5taW5pQ2hpcEFyZWE7XG4gICAgbGV0IHJzID0gY2MuVmVjMi5aRVJPO1xuICAgIFxuICAgIGxldCBvZmZzZXQgPSBjYy5WZWMyLlpFUk87Ly8gbmV3IGNjLlZlYzIoMjAsIDIwKTtcbiAgICBpZihiZXRTbG90SXRlbUNvbXAuZG9vciA9PSBEb29yLkJJRykgb2Zmc2V0ID0gbmV3IGNjLlZlYzIoLTIyLCAwKTsgLy9OT1RFIGZvciBsYXllciBmaXggd2hlbiB0aHJvdyBjaGlwXG4gICAgaWYoYmV0U2xvdEl0ZW1Db21wLmRvb3IgPT0gRG9vci5TTUFMTCkgb2Zmc2V0ID0gbmV3IGNjLlZlYzIoMjIsIDApOyAvL05PVEUgZm9yIGxheWVyIGZpeCB3aGVuIHRocm93IGNoaXBcblxuICAgIGxldCBoYWxmVyA9IChiZXRBcmVhLndpZHRoKS8yO1xuICAgIGxldCBoYWxmSCA9IChiZXRBcmVhLmhlaWdodCkvMjtcbiAgICBycyA9IG5ldyBjYy5WZWMyKFxuICAgICAgICB0aGlzLnJhbmRvbVJhbmdlKC1oYWxmVywgaGFsZlcpICsgb2Zmc2V0LngsXG4gICAgICAgIHRoaXMucmFuZG9tUmFuZ2UoLWhhbGZILCBoYWxmSCkgKyBvZmZzZXQueVxuICAgICk7XG4gICAgICAgICAgICBcbiAgICBycy5hZGQoYmV0QXJlYS5nZXRQb3NpdGlvbigpKTtcblxuICAgIGxldCByc1YzID0gU2ljYm9HYW1lVXRpbHMuY29udmVydFRvT3RoZXJOb2RlKGJldEFyZWEsIHRoaXMubWluaUNoaXBSb290LCBuZXcgY2MuVmVjMyhycy54LCBycy55LCAwKSk7XG4gICAgcnMueCA9IHJzVjMueDtcbiAgICBycy55ID0gcnNWMy55O1xuICAgIHJldHVybiBycztcbiAgfVxuXG4gIHJhbmRvbVJhbmdlKG1pbjpudW1iZXIsIG1heDpudW1iZXIpOm51bWJlcntcbiAgICByZXR1cm4gTWF0aC5yYW5kb20oKSAqIChtYXggLSBtaW4pICsgbWluO1xuICB9XG5cbiAgZ2V0TWluaUNoaXBTdGF0aWNQb3MoYmV0U2xvdDogU2ljYm9CZXRTbG90SXRlbUNvbXApe1xuICAgIGxldCBjaGlwU3BhY2luZyA9IHRoaXMuY2hpcFN0YWNrUG9zQ29uZmlnLmNoaXBTcGFjaW5nO1xuXG4gICAgbGV0IGNvdW50ID0gYmV0U2xvdC5taW5pQ2hpcHNPblNsb3QubGVuZ3RoOyAgICAgXG4gICAgbGV0IGNvbCA9IDA7XG4gICAgbGV0IHJvdyA9IGNvdW50OyBcblxuXG4gICAgLy8gaWYgKGJldFNsb3QuZG9vciA9PSBEb29yLkFOWV9UUklQTEUpIHtcbiAgICBpZiAodGhpcy5nZXRDaGlwU3RhY2tNYXhDb2woYmV0U2xvdC5kb29yKT4xKSB7XG4gICAgICAgIGxldCBtYXhIZWlnaHQwID0gdGhpcy5nZXRDaGlwU3RhY2tIZWlnaHQoYmV0U2xvdC5kb29yLCAwKTtcbiAgICAgICAgaWYoY291bnQgPj0gbWF4SGVpZ2h0MCl7XG4gICAgICAgICAgY29sID0gMTtcbiAgICAgICAgICByb3cgPSBjb3VudCAtIG1heEhlaWdodDA7XG4gICAgICAgIH1cbiAgICB9XG4gICAgbGV0IHN0YXJ0UG9zID0gdGhpcy5nZXRDaGlwU3RhY2tTdGFydFBvcyhiZXRTbG90LmRvb3IsIGNvbCk7XG4gICAgbGV0IHkgPSBzdGFydFBvcy55O1xuICAgIHkgKz0gcm93ICogY2hpcFNwYWNpbmc7XG5cbiAgICByZXR1cm4gU2ljYm9HYW1lVXRpbHMuY29udmVydFRvT3RoZXJOb2RlMihiZXRTbG90Lm1pbmlDaGlwQXJlYSwgdGhpcy5taW5pQ2hpcFJvb3QsIG5ldyBjYy5WZWMyKHN0YXJ0UG9zLngsIHkpKTtcbiAgfVxuXG4gIC8vQU5DSE9SICBwdXRNaW5pQ2hpcE9uVGFibGVJbW1lZGlhdGVseVxuICBwdXRNaW5pQ2hpcE9uVGFibGVJbW1lZGlhdGVseShjb2luVHlwZTogU2ljYm9Db2luVHlwZSwgYmV0U2xvdEl0ZW1Db21wOiBTaWNib0JldFNsb3RJdGVtQ29tcCl7IFxuICAgIGxldCBjaGlwID0gdGhpcy5jcmVhdGVJdGVtQ29pbk1pbmkoY29pblR5cGUpOyAgICBcblxuICAgIGNoaXAubm9kZS5zZXRQb3NpdGlvbih0aGlzLmdldFJhbmRvbVBvcyhiZXRTbG90SXRlbUNvbXApKTtcbiAgICBpZih0aGlzLmlzU3RhdGljU2xvdChiZXRTbG90SXRlbUNvbXAuZG9vcikpe1xuICAgICAgY2hpcC5ub2RlLnNldFBvc2l0aW9uKHRoaXMuZ2V0TWluaUNoaXBTdGF0aWNQb3MoYmV0U2xvdEl0ZW1Db21wKSk7XG4gICAgfWVsc2V7XG4gICAgICBjaGlwLm5vZGUuc2V0UG9zaXRpb24odGhpcy5nZXRSYW5kb21Qb3MoYmV0U2xvdEl0ZW1Db21wKSk7XG4gICAgfVxuICAgIHRoaXMuY2hhbmdlUGFyZW50KGNoaXAubm9kZSwgYmV0U2xvdEl0ZW1Db21wLm1pbmlDaGlwQXJlYSk7XG4gICAgYmV0U2xvdEl0ZW1Db21wLmFkZE1pbmlDaGlwVG9TbG90KGNoaXApO1xuICAgIHRoaXMucmVtb3ZlUmVkdW5kYW5jeUNoaXAoYmV0U2xvdEl0ZW1Db21wKTtcbiAgfVxuXG4gIHB1dE1pbmlDaGlwT25UYWJsZUltbWVkaWF0ZWx5QnlWYWx1ZSh0b3RhbFZhbHVlOiBudW1iZXIsIGJldFNsb3RJdGVtQ29tcDogU2ljYm9CZXRTbG90SXRlbUNvbXApe1xuICAgIGlmKGJldFNsb3RJdGVtQ29tcCA9PSBudWxsKXtcbiAgICAgIC8vY2MubG9nKFwicHV0TWluaUNoaXBPblRhYmxlSW1tZWRpYXRlbHlCeVZhbHVlIE5PIERPT1IgRk9VTkRcIik7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGxldCBwYXJzZWRDb2luID0gU2ljYm9IZWxwZXIuY2FsY3VsYXRlTnVtT2ZDaGlwVG9NYWtlVmFsdWUodG90YWxWYWx1ZSwgdGhpcy5jaGlwX2RhdGEpO1xuICAgIFxuICAgIHBhcnNlZENvaW4uZm9yRWFjaCgobnVtOiBudW1iZXIsIHZhbDogbnVtYmVyKSA9PiB7XG4gICAgICBsZXQgY29pblR5cGUgPSBTaWNib0hlbHBlci5jb252ZXJ0VmFsdWVUb0NvaW5UeXBlKHZhbCk7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IG51bTsgaSsrKSB7XG4gICAgICAgIHRoaXMucHV0TWluaUNoaXBPblRhYmxlSW1tZWRpYXRlbHkoY29pblR5cGUsIGJldFNsb3RJdGVtQ29tcCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICAvL0FOQ0hPUiAgdGhyb3dNaW5pQ2hpcE9uVGFibGVcbiAgdGhyb3dNaW5pQ2hpcE9uVGFibGVCeVZhbHVlKHRvdGFsVmFsdWU6IG51bWJlciwgZnJvbTpjYy5Ob2RlID0gbnVsbCwgYmV0U2xvdEl0ZW1Db21wOiBTaWNib0JldFNsb3RJdGVtQ29tcCl7ICBcbiAgICBpZihiZXRTbG90SXRlbUNvbXAgPT0gbnVsbCl7XG4gICAgICAvL2NjLmxvZyhcInRocm93TWluaUNoaXBPblRhYmxlQnlWYWx1ZSBOTyBET09SIEZPVU5EXCIpO1xuICAgICAgcmV0dXJuO1xuICAgIH0gIFxuXG4gICAgbGV0IHBhcnNlZENvaW4gPSBTaWNib0hlbHBlci5jYWxjdWxhdGVOdW1PZkNoaXBUb01ha2VWYWx1ZSh0b3RhbFZhbHVlLCB0aGlzLmNoaXBfZGF0YSk7XG4gICAgXG4gICAgcGFyc2VkQ29pbi5mb3JFYWNoKChudW06IG51bWJlciwgdmFsOiBudW1iZXIpID0+IHtcbiAgICAgIC8vIC8vY29uc29sZS5sb2coXCJ2YWw6IFwiICsgdmFsICsgXCIgLT4gXCIgKyBudW0pO1xuICAgICAgbGV0IGNvaW5UeXBlID0gU2ljYm9IZWxwZXIuY29udmVydFZhbHVlVG9Db2luVHlwZSh2YWwpO1xuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBudW07IGkrKykge1xuICAgICAgICB0aGlzLnRocm93TWluaUNoaXBPblRhYmxlKGNvaW5UeXBlLCBmcm9tLCBiZXRTbG90SXRlbUNvbXApO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgdGhyb3dNaW5pQ2hpcE9uVGFibGUoY29pblR5cGU6IFNpY2JvQ29pblR5cGUsIGZyb206Y2MuTm9kZSA9IG51bGwsIGJldFNsb3RJdGVtQ29tcDogU2ljYm9CZXRTbG90SXRlbUNvbXApe1xuICAgIGlmKGZyb20gPT0gbnVsbCkgZnJvbSA9IHRoaXMuY3VycmVudFNlbGVjdGVkQ2hpcC5ub2RlO1xuXG4gICAgbGV0IGNoaXAgPSB0aGlzLmNyZWF0ZUl0ZW1Db2luTWluaShjb2luVHlwZSk7ICAgIFxuXG4gICAgbGV0IHN0YXJ0UG9zID0gU2ljYm9HYW1lVXRpbHMuY29udmVydFRvT3RoZXJOb2RlKGZyb20sIGJldFNsb3RJdGVtQ29tcC5taW5pQ2hpcEFyZWEpO1xuXG4gICAgbGV0IHRhcmdldFBvcyA9ICB0aGlzLmdldE1pbmlDaGlwU3RhdGljUG9zKGJldFNsb3RJdGVtQ29tcClcbiAgICBsZXQgcmFuZG9tUG9zID0gIHRoaXMuZ2V0UmFuZG9tUG9zKGJldFNsb3RJdGVtQ29tcCk7ICBcblxuICAgIHRhcmdldFBvcyA9IFNpY2JvR2FtZVV0aWxzLmNvbnZlcnRUb090aGVyTm9kZTIoY2hpcC5ub2RlLnBhcmVudCwgYmV0U2xvdEl0ZW1Db21wLm1pbmlDaGlwQXJlYSwgdGFyZ2V0UG9zKTtcbiAgICByYW5kb21Qb3MgPSBTaWNib0dhbWVVdGlscy5jb252ZXJ0VG9PdGhlck5vZGUyKGNoaXAubm9kZS5wYXJlbnQsIGJldFNsb3RJdGVtQ29tcC5taW5pQ2hpcEFyZWEsIHJhbmRvbVBvcyk7XG5cbiAgICBjaGlwLm5vZGUucGFyZW50ID0gYmV0U2xvdEl0ZW1Db21wLm1pbmlDaGlwQXJlYTtcbiAgICBjaGlwLm5vZGUuc2V0UG9zaXRpb24oc3RhcnRQb3MpOyAgIFxuICAgIC8vIGNoaXAubm9kZS5zZXRTaWJsaW5nSW5kZXhcbiAgICBiZXRTbG90SXRlbUNvbXAuYWRkTWluaUNoaXBUb1Nsb3QoY2hpcCk7ICBcblxuICAgIGxldCBjaGlwVGhyb3dEdXJhdGlvbiA9IDAuOTtcbiAgICBsZXQgZmFkZU91dERlbGF5ID0gMC41O1xuICAgIGlmKGJldFNsb3RJdGVtQ29tcC5kb29yICE9IERvb3IuQklHICYmIGJldFNsb3RJdGVtQ29tcC5kb29yICE9IERvb3IuU01BTEwpe1xuICAgICAgcmFuZG9tUG9zID0gY2MuVmVjMi5aRVJPO1xuICAgICAgZmFkZU91dERlbGF5ID0gMDtcbiAgICB9ICBcblxuICAgIGNoaXAubm9kZS5zY2FsZSA9IDEuMjtcbiAgICBjaGlwLm5vZGUub3BhY2l0eSA9IDI1NTtcblxuICAgIGNjLnR3ZWVuKGNoaXAubm9kZSlcbiAgICAudG8oY2hpcFRocm93RHVyYXRpb24vMywge3g6cmFuZG9tUG9zLngsIHk6cmFuZG9tUG9zLnksIHNjYWxlOiAxfSlcbiAgICAuZGVsYXkoZmFkZU91dERlbGF5KVxuICAgIC50byhjaGlwVGhyb3dEdXJhdGlvbi8zLCB7b3BhY2l0eTogMCwgeDp0YXJnZXRQb3MueCwgeTp0YXJnZXRQb3MueX0pXG4gICAgLy8udG8oMCwge3g6dGFyZ2V0UG9zLngsIHk6dGFyZ2V0UG9zLnl9KVxuICAgIC50byhjaGlwVGhyb3dEdXJhdGlvbi8zLCB7b3BhY2l0eTogMjU1fSlcbiAgICAuY2FsbChcbiAgICAgICgpPT57XG4gICAgICAgIHRoaXMucmVtb3ZlUmVkdW5kYW5jeUNoaXAoYmV0U2xvdEl0ZW1Db21wKTtcbiAgICAgIH1cbiAgICApXG4gICAgLnN0YXJ0KCk7XG4gIFxuICB9XG5cblxuICBwcml2YXRlIGNoYW5nZVBhcmVudChjaGlsZDogY2MuTm9kZSwgbmV3UGFyZW50OmNjLk5vZGUpe1xuICAgIFNpY2JvSGVscGVyLmNoYW5nZVBhcmVudChjaGlsZCwgbmV3UGFyZW50KTtcbiAgfVxuICAvLyNlbmRyZWdpb25cblxuICAvL1NFQ1RJT04gV0lOIC0gTE9TRVxuICAvL0FOQ0hPUiBjaGlwIHRvIGxvc2UgaG9sZSBhbmltYXRpb25cbiAgcGxheUxvc2VBbmltYXRpb24od2luRG9vcnM6IGFueVtdLCBkdXJhdGlvbjogbnVtYmVyID0gLjc1KXsgICAgICAgIFxuICAgIGxldCB0d2VlblBhcmFtcyA9IHtcbiAgICAgIHBvc2l0aW9uIDogY2MuVmVjMy5aRVJPLFxuICAgICAgLy8gc2NhbGUgOiAwLFxuICAgIH1cbiAgICB0aGlzLmFsbEJldFNsb3RzLmZvckVhY2goYmV0U2xvdCA9PiB7XG4gICAgICAgIGlmKHdpbkRvb3JzLmZpbmQoeD0+eCA9PSBiZXRTbG90LmRvb3IpID09IG51bGwpe1xuICAgICAgICBsZXQgbG9zdENoaXBzID0gYmV0U2xvdC5taW5pQ2hpcHNPblNsb3Q7XG4gICAgICAgIFxuICAgICAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgICAgIHNlbGYudHdlZW5DaGlwVG9Bbm90aGVyTm9kZShzZWxmLmxvc2VDaGlwVGFyZ2V0LCBsb3N0Q2hpcHNcbiAgICAgICAgICAsIGR1cmF0aW9uLCAwLCB0d2VlblBhcmFtc1xuICAgICAgICAgICwgdHJ1ZSwgIChjaGlwOlNpY2JvQ29pbkl0ZW1Db21wKT0+e1xuICAgICAgICAgICAgLy9BTkNIT1IgU09VTkRcbiAgICAgICAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4Q2hpcEZseSk7XG4gICAgICAgICAgICBzZWxmLnJlY3ljbGVJdGVtQ29pbk1pbmkoY2hpcC5ub2RlKTsgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgfSk7XG4gICAgICAgIGJldFNsb3QuY2xlYXJNaW5pQ2hpcE9uU2xvdCgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgLy9BTkNIT1Igc3RhY2sgY2hpcCBpbnRvIGNvbHVtblxuICBwbGF5Q2hpcFN0YWNrQW5pbWF0aW9uKHdpbkRvb3JzOiBhbnlbXSwgZHVyYXRpb246IG51bWJlciA9IC41KXtcbiAgICAvL2NjLmxvZyhkdXJhdGlvbik7XG4gICAgbGV0IGNoaXBTcGFjaW5nID0gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuY2hpcFNwYWNpbmc7XG5cbiAgICB0aGlzLmFsbEJldFNsb3RzLmZvckVhY2goYmV0U2xvdCA9PiB7XG4gICAgICBpZih0aGlzLmlzU3RhdGljU2xvdChiZXRTbG90LmRvb3IpKSByZXR1cm47XG4gICAgICBpZih3aW5Eb29ycy5maW5kKHg9PnggPT0gYmV0U2xvdC5kb29yKSAhPSBudWxsKXtcbiAgICAgICAgICBsZXQgY2hpcHMgPSBiZXRTbG90Lm1pbmlDaGlwc09uU2xvdDsgICAgICBcbiAgICAgICAgICBiZXRTbG90LmNoaXBDb2xDb3VudCA9IDA7XG5cbiAgICAgICAgICBsZXQgY291bnQgPSAwO1xuICAgICAgICAgIGxldCBtYXhDb2wgPSB0aGlzLmdldENoaXBTdGFja01heENvbChiZXRTbG90LmRvb3IpO1xuXG4gICAgICAgICAgZm9yIChsZXQgY29sID0gMDsgY29sIDwgbWF4Q29sOyBjb2wrKykge1xuICAgICAgICAgICAgYmV0U2xvdC5jaGlwQ29sQ291bnQrKztcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgbGV0IG1heEhlaWdodCA9IHRoaXMuZ2V0Q2hpcFN0YWNrSGVpZ2h0KGJldFNsb3QuZG9vciwgY29sKTtcbiAgICAgICAgICAgIGxldCBzdGFydFBvcyA9IHRoaXMuZ2V0Q2hpcFN0YWNrU3RhcnRQb3MoYmV0U2xvdC5kb29yLCBjb2wpO1xuICAgICAgICAgICAgbGV0IG9mZlNldCA9IGNjLlZlYzIuWkVSTzsvL3RoaXMuZ2V0Q2hpcFN0YWNrT2Zmc2V0KGJldFNsb3QuZG9vcik7XG5cbiAgICAgICAgICAgIGxldCB4ID0gc3RhcnRQb3MueDtcbiAgICAgICAgICAgIGZvciAobGV0IGggPSAwOyBoIDwgbWF4SGVpZ2h0OyBoKyspIHtcbiAgICAgICAgICAgICAgaWYoY2hpcHMubGVuZ3RoPD0wKSBicmVhaztcblxuICAgICAgICAgICAgICBsZXQgeSA9IHN0YXJ0UG9zLnk7XG4gICAgICAgICAgICAgIHkgKz0gaCpjaGlwU3BhY2luZztcblxuICAgICAgICAgICAgICBsZXQgaWR4ID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogY2hpcHMubGVuZ3RoKTtcbiAgICAgICAgICAgICAgbGV0IGNoaXAgPSBjaGlwc1tpZHhdO1xuICAgICAgICAgICAgICBjaGlwcy5zcGxpY2UoaWR4LCAxKTtcbiAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgIGNvdW50Kys7XG4gICAgICAgICAgICAgIGNoaXAubm9kZS5zZXRTaWJsaW5nSW5kZXgoY291bnQpO1xuXG4gICAgICAgICAgICAgIGNjLnR3ZWVuKGNoaXAubm9kZSlcbiAgICAgICAgICAgICAgICAudG8oZHVyYXRpb24gLCB7eDogeCArIG9mZlNldC54LCB5OiB5ICsgb2ZmU2V0Lnl9KVxuICAgICAgICAgICAgICAgIC5jYWxsKFxuICAgICAgICAgICAgICAgICAgKCk9PnsgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIC8vQU5DSE9SIFNPVU5EXG4gICAgICAgICAgICAgICAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4Q2hpcEZseSk7ICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KGNoaXAubm9kZSk7ICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIC5zdGFydCgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpcHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgbGV0IGNoaXAgPSBjaGlwc1tpXTtcbiAgICAgICAgICAgICAgdGhpcy5yZWN5Y2xlSXRlbUNvaW5NaW5pKGNoaXAubm9kZSk7ICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBiZXRTbG90Lm1pbmlDaGlwc09uU2xvdCA9IGNhY2hlZE1pbmlDaGlwO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgKTtcbiAgICB9XG5cbiAgICBnZXRDaGlwU3RhY2tDb25maWcoZG9vcjogYW55KTphbnl7XG4gICAgICBzd2l0Y2ggKGRvb3IpIHtcbiAgICAgICAgY2FzZSBEb29yLkFOWV9UUklQTEU6IFxuICAgICAgICAgIHJldHVybiB0aGlzLmNoaXBTdGFja1Bvc0NvbmZpZy5hbnlUcmlwcGxlO1xuICBcbiAgICAgICAgY2FzZSBEb29yLkJJRzpcbiAgICAgICAgICByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuYmlnO1xuICBcbiAgICAgICAgY2FzZSBEb29yLlNNQUxMOlxuICAgICAgICAgIHJldHVybiB0aGlzLmNoaXBTdGFja1Bvc0NvbmZpZy5zbWFsbDtcbiAgXG4gICAgICAgIGNhc2UgRG9vci5PTkVfVFJJUExFOiBjYXNlIERvb3IuVFdPX1RSSVBMRTogY2FzZSBEb29yLlRIUkVFX1RSSVBMRTpcbiAgICAgICAgICByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcudHJpcHBsZUxlZnQ7XG5cbiAgICAgICAgY2FzZSBEb29yLkZPVVJfVFJJUExFOiBjYXNlIERvb3IuRklWRV9UUklQTEU6IGNhc2UgRG9vci5TSVhfVFJJUExFOlxuICAgICAgICAgIHJldHVybiB0aGlzLmNoaXBTdGFja1Bvc0NvbmZpZy50cmlwcGxlUmlnaHQ7XG5cbiAgICAgICAgY2FzZSAgRG9vci5PTkVfU0lOR0xFOiBjYXNlIERvb3IuVFdPX1NJTkdMRTogY2FzZSBEb29yLlRIUkVFX1NJTkdMRTogY2FzZSAgRG9vci5GT1VSX1NJTkdMRTogY2FzZSBEb29yLkZJVkVfU0lOR0xFOiBjYXNlIERvb3IuU0lYX1NJTkdMRTpcbiAgICAgICAgICByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuc2luZ2xlOyBcbiAgICAgICAgY2FzZSBEb29yLkZPVVJfU1VNOiByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuZm91cl9zdW07IFxuICAgICAgICBjYXNlIERvb3IuRklWRV9TVU06IHJldHVybiB0aGlzLmNoaXBTdGFja1Bvc0NvbmZpZy5maXZlX3N1bTsgXG4gICAgICAgIGNhc2UgRG9vci5TSVhfU1VNOiByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuc2l4X3N1bTsgXG4gICAgICAgIGNhc2UgRG9vci5TRVZFTl9TVU06IHJldHVybiB0aGlzLmNoaXBTdGFja1Bvc0NvbmZpZy5zZXZlbl9zdW07IFxuICAgICAgICBjYXNlIERvb3IuRUlHSFRfU1VNOiByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuZWlnaHRfc3VtOyBcbiAgICAgICAgY2FzZSBEb29yLk5JTkVfU1VNOiByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcubmluZV9zdW07IFxuICAgICAgICBjYXNlIERvb3IuVEVOX1NVTTogcmV0dXJuIHRoaXMuY2hpcFN0YWNrUG9zQ29uZmlnLnRlbl9zdW07IFxuICAgICAgICBjYXNlIERvb3IuRUxFVkVOX1NVTTogcmV0dXJuIHRoaXMuY2hpcFN0YWNrUG9zQ29uZmlnLmVsZXZlbl9zdW07IFxuICAgICAgICBjYXNlIERvb3IuVFdFTFZFX1NVTTogcmV0dXJuIHRoaXMuY2hpcFN0YWNrUG9zQ29uZmlnLnR3ZWx2ZV9zdW07IFxuICAgICAgICBjYXNlIERvb3IuVEhJUlRFRU5fU1VNOiByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcudGhpcnRlZW5fc3VtOyBcbiAgICAgICAgY2FzZSBEb29yLkZPVVJURUVOX1NVTTogcmV0dXJuIHRoaXMuY2hpcFN0YWNrUG9zQ29uZmlnLmZvdXJ0ZWVuX3N1bTsgXG4gICAgICAgIGNhc2UgRG9vci5GSUZURUVOX1NVTTogcmV0dXJuIHRoaXMuY2hpcFN0YWNrUG9zQ29uZmlnLmZpZnRlZW5fc3VtOyBcbiAgICAgICAgY2FzZSBEb29yLlNJWFRFRU5fU1VNOiByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuc2l4dGVlbl9zdW07IFxuICAgICAgICBjYXNlIERvb3IuU0VWRU5URUVOX1NVTTogcmV0dXJuIHRoaXMuY2hpcFN0YWNrUG9zQ29uZmlnLnNldmVudGVlbl9zdW07IFxuICAgICAgfVxuICAgIH1cbiAgICBnZXRDaGlwU3RhY2tTdGFydFBvcyhkb29yOiBhbnksIGNvbDogbnVtYmVyKXtcbiAgICAgIHJldHVybiB0aGlzLmdldENoaXBTdGFja0NvbmZpZyhkb29yKVtjb2xdLnBvcztcbiAgICB9XG4gIFxuICAgIGdldENoaXBTdGFja09mZnNldChkb29yOiBhbnkpe1xuICAgICAgcmV0dXJuIHRoaXMuaXNSaWdodENoaXBTdGFjayhkb29yKT8gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuY2hpcFN0YWNrT2Zmc2V0UmlnaHQ6IHRoaXMuY2hpcFN0YWNrUG9zQ29uZmlnLmNoaXBTdGFja09mZnNldExlZnQ7XG4gICAgfVxuXG4gICAgZ2V0Q2hpcFN0YWNrSGVpZ2h0KGRvb3I6IGFueSwgY29sOiBudW1iZXIpe1xuICAgICAgcmV0dXJuIHRoaXMuZ2V0Q2hpcFN0YWNrQ29uZmlnKGRvb3IpW2NvbF0uaGVpZ2h0O1xuICAgIH1cbiAgXG4gICAgZ2V0Q2hpcFN0YWNrTWF4Q29sKGRvb3I6IGFueSl7XG4gICAgICByZXR1cm4gdGhpcy5nZXRDaGlwU3RhY2tDb25maWcoZG9vcikubGVuZ3RoO1xuICAgIH1cblxuICAgIGdldENsb25lU3BhY2luZyhkb29yOiBhbnkpOm51bWJlcntcbiAgICAgIHN3aXRjaCAoZG9vcikge1xuICAgICAgICBjYXNlIERvb3IuQU5ZX1RSSVBMRTpcbiAgICAgICAgICByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuYW55VHJpcHBsZUNsb25lU3BhY2luZztcbiAgICAgICAgY2FzZSBEb29yLk9ORV9TSU5HTEU6IGNhc2UgRG9vci5UV09fU0lOR0xFOiBjYXNlIERvb3IuVEhSRUVfU0lOR0xFOmNhc2UgRG9vci5GT1VSX1NJTkdMRTogY2FzZSBEb29yLkZJVkVfU0lOR0xFOiBjYXNlIERvb3IuU0lYX1NJTkdMRTpcbiAgICAgICAgICByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuc2luZ2xlQ2xvbmVTcGFjaW5nO1xuICAgICAgICBjYXNlIERvb3IuQklHOiByZXR1cm4gdGhpcy5jaGlwU3RhY2tQb3NDb25maWcuYmlnQ2xvbmVTcGFjaW5nO1xuICAgICAgICBjYXNlIERvb3IuU01BTEw6IHJldHVybiB0aGlzLmNoaXBTdGFja1Bvc0NvbmZpZy5zbWFsbENsb25lU3BhY2luZztcbiAgICAgIH0gICAgICAgXG4gICAgICBcbiAgICAgIHJldHVybiB0aGlzLmlzUmlnaHRDaGlwU3RhY2soZG9vcik/LXRoaXMuY2hpcFN0YWNrUG9zQ29uZmlnLmNsb25lU3BhY2luZzp0aGlzLmNoaXBTdGFja1Bvc0NvbmZpZy5jbG9uZVNwYWNpbmc7XG4gICAgfVxuXG4gICAgaXNTdGF0aWNTbG90KGRvb3I6IGFueSl7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIHN3aXRjaCAoZG9vcikge1xuICAgICAgICBjYXNlIERvb3IuQU5ZX1RSSVBMRTpcbiAgICAgICAgY2FzZSBEb29yLk9ORV9UUklQTEU6IGNhc2UgRG9vci5UV09fVFJJUExFOiBjYXNlIERvb3IuVEhSRUVfVFJJUExFOmNhc2UgRG9vci5GT1VSX1RSSVBMRTogY2FzZSBEb29yLkZJVkVfVFJJUExFOiBjYXNlIERvb3IuU0lYX1RSSVBMRTpcbiAgICAgICAgICByZXR1cm4gdHJ1ZTsgICAgIFxuICAgICAgfSAgICAgICBcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBpc1JpZ2h0Q2hpcFN0YWNrKGRvb3I6IGFueSkgOiBib29sZWFue1xuICAgICAgc3dpdGNoIChkb29yKSB7XG4gICAgICAgIGNhc2UgRG9vci5GT1VSX1RSSVBMRTogY2FzZSBEb29yLkZJVkVfVFJJUExFOiBjYXNlIERvb3IuU0lYX1RSSVBMRTogY2FzZSBEb29yLkJJRzpcbiAgICAgICAgICByZXR1cm4gdHJ1ZTsgICAgIFxuICAgICAgfSAgICAgICBcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBnZXRDbG9uZVN0YWNrU2libGluZyhkb29yOiBhbnkpOm51bWJlcntcbiAgICAgIHJldHVybiB0aGlzLmlzUmlnaHRDaGlwU3RhY2soZG9vcik/MTowO1xuICAgIH1cblxuICAgIC8vQU5DSE9SIHJldHVybiB3b24gY2hpcHMgdG8gc2xvdCAgICBcbiAgICBkZWFsZXJSZXR1cm5XaW5DaGlwKHdpbkRvb3JzOiBhbnlbXSwgb25GaW5pc2g6KGRvb3I6IGFueSk9PnZvaWQgPSBudWxsKXtcbiAgICAgIHJldHVybiB0aGlzLmRlYWxlclJldHVybkNoaXBPbmVCeU9uZSh3aW5Eb29ycywgLjA1LCAuNSwgb25GaW5pc2gpO1xuICAgIH0gICAgXG5cbiAgICBkZWFsZXJSZXR1cm5GdWxsU3RhY2tDaGlwKHdpbkRvb3JzOiBhbnlbXSwgZHVyYXRpb246bnVtYmVyLCBvbkZpbmlzaDooZG9vcjogYW55KT0+dm9pZCA9IG51bGwpe1xuICAgICAgdGhpcy5hbGxCZXRTbG90cy5mb3JFYWNoKGJldFNsb3QgPT4ge1xuICAgICAgICBpZih3aW5Eb29ycy5maW5kKHg9PnggPT0gYmV0U2xvdC5kb29yKSAhPSBudWxsKXsgICAgICBcbiAgICAgICAgICBsZXQgb2Zmc2V0ID0gdGhpcy5nZXRDaGlwU3RhY2tPZmZzZXQoYmV0U2xvdC5kb29yKTtcbiAgICAgICAgICBsZXQgY2xvbmVDaGlwcyA9IGNjLmluc3RhbnRpYXRlKGJldFNsb3QubWluaUNoaXBBcmVhKTtcbiAgICAgICAgICBsZXQgc3BhY2luZyA9IHRoaXMuZ2V0Q2xvbmVTcGFjaW5nKGJldFNsb3QuZG9vcik7XG4gICAgICAgICAgY2xvbmVDaGlwcy5wYXJlbnQgPSB0aGlzLndpbkNoaXBQb3M7XG4gICAgICAgICAgY2xvbmVDaGlwcy5wb3NpdGlvbiA9IG5ldyBjYy5WZWMzKG9mZnNldC54LCBvZmZzZXQueSwgMCk7XG4gICAgICAgICAgYmV0U2xvdC5zZXRTdGFja0NoaXBSb290KGNsb25lQ2hpcHMpO1xuXG4gICAgICAgICAgdGhpcy5jaGFuZ2VQYXJlbnQoY2xvbmVDaGlwcywgYmV0U2xvdC5ub2RlKTtcbiAgICAgICAgICBjbG9uZUNoaXBzLnNldFNpYmxpbmdJbmRleCh0aGlzLmdldENsb25lU3RhY2tTaWJsaW5nKGJldFNsb3QuZG9vcikpO1xuXG4gICAgICAgICAgY2MudHdlZW4oY2xvbmVDaGlwcylcbiAgICAgICAgICAudG8oZHVyYXRpb24gLCB7cG9zaXRpb246IG5ldyBjYy5WZWMzKG9mZnNldC54ICsgc3BhY2luZywgMCwgMCl9KVxuICAgICAgICAgIC5jYWxsKCgpPT57XG4gICAgICAgICAgICBpZihvbkZpbmlzaCkgb25GaW5pc2goYmV0U2xvdC5kb29yKTtcbiAgICAgICAgICB9KVxuICAgICAgICAgIC5zdGFydCgpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGRlYWxlclJldHVybkNoaXBPbmVCeU9uZSh3aW5Eb29yczogYW55W10sIGRlbGF5QmV0d2VlbkNoaXBUaW1lOiBudW1iZXIgPSAuMDUsIGNoaXBGbHlUaW1lOiBudW1iZXIgPSAuNSwgb25GaW5pc2g6KGRvb3I6IGFueSk9PnZvaWQgPSBudWxsKXtcbiAgICAgIGxldCBtYXhFZmZlY3RUaW1lID0gMDtcblxuICAgICAgdGhpcy5hbGxCZXRTbG90cy5mb3JFYWNoKGJldFNsb3QgPT4ge1xuICAgICAgICBpZih3aW5Eb29ycy5maW5kKHg9PnggPT0gYmV0U2xvdC5kb29yKSAhPSBudWxsKXsgICAgICBcbiAgICAgICAgICBsZXQgb2Zmc2V0ID0gdGhpcy5nZXRDaGlwU3RhY2tPZmZzZXQoYmV0U2xvdC5kb29yKTtcbiAgICAgICAgICBsZXQgY2xvbmVDaGlwcyA9IGNjLmluc3RhbnRpYXRlKGJldFNsb3QubWluaUNoaXBBcmVhKTtcbiAgICAgICAgICBsZXQgc3BhY2luZyA9IHRoaXMuZ2V0Q2xvbmVTcGFjaW5nKGJldFNsb3QuZG9vcik7XG4gICAgICAgICAgY2xvbmVDaGlwcy5wYXJlbnQgPSBiZXRTbG90Lm5vZGU7XG4gICAgICAgICAgY2xvbmVDaGlwcy5wb3NpdGlvbiA9IG5ldyBjYy5WZWMzKG9mZnNldC54ICsgc3BhY2luZywgMCwgMCk7XG4gICAgICAgICAgYmV0U2xvdC5zZXRTdGFja0NoaXBSb290KGNsb25lQ2hpcHMpO1xuXG4gICAgICAgICAgLy8gdGhpcy5jaGFuZ2VQYXJlbnQoY2xvbmVDaGlwcywgYmV0U2xvdC5ub2RlKTtcbiAgICAgICAgICBjbG9uZUNoaXBzLnNldFNpYmxpbmdJbmRleCh0aGlzLmdldENsb25lU3RhY2tTaWJsaW5nKGJldFNsb3QuZG9vcikpO1xuXG4gICAgICAgICAgbGV0IGNoaXBzSW5TdGFjayA9IGNsb25lQ2hpcHMuY2hpbGRyZW47XG4gICAgICAgICAgbGV0IGRlbGF5ID0gZGVsYXlCZXR3ZWVuQ2hpcFRpbWU7XG4gICAgICAgICAgbGV0IHN0YXJ0UG9zID0gU2ljYm9HYW1lVXRpbHMuY29udmVydFRvT3RoZXJOb2RlKHRoaXMud2luQ2hpcFBvcywgY2xvbmVDaGlwcyk7XG4gICAgICAgICAgbWF4RWZmZWN0VGltZSA9IE1hdGgubWF4KG1heEVmZmVjdFRpbWUsIGNoaXBzSW5TdGFjay5sZW5ndGggKiBkZWxheSArIGNoaXBGbHlUaW1lKTtcblxuICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpcHNJblN0YWNrLmxlbmd0aDsgaSsrKSB7ICAgICAgICAgICAgXG4gICAgICAgICAgICBsZXQgY2hpcCA9IGNoaXBzSW5TdGFja1tpXTtcbiAgICAgICAgICAgIGxldCBjaGlwUG9zID0gY2hpcC5wb3NpdGlvbjtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgY2hpcC5wb3NpdGlvbiA9IHN0YXJ0UG9zO1xuICAgICAgICAgICAgY2hpcC5hY3RpdmUgPSBmYWxzZTtcblxuICAgICAgICAgICAgY2MudHdlZW4odGhpcy5ub2RlKVxuICAgICAgICAgICAgICAuZGVsYXkoaSAqIGRlbGF5KSAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgIC5jYWxsKFxuICAgICAgICAgICAgICAgICgpPT57ICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICBjaGlwLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICBjYy50d2VlbihjaGlwKS50byhjaGlwRmx5VGltZSAsIHtwb3NpdGlvbjogY2hpcFBvc30pXG4gICAgICAgICAgICAgICAgICAgIC5jYWxsKCgpPT57XG4gICAgICAgICAgICAgICAgICAgICAgLy9BTkNIT1IgU09VTkRcbiAgICAgICAgICAgICAgICAgICAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UucGxheVNmeChTaWNib1NvdW5kLlNmeENoaXBGbHkpO1xuICAgICAgICAgICAgICAgICAgICAgIGlmKGkgPT0gY2hpcHNJblN0YWNrLmxlbmd0aCAtIDEpe1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYob25GaW5pc2gpIG9uRmluaXNoKGJldFNsb3QuZG9vcik7XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAuc3RhcnQoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICkgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAuc3RhcnQoKTtcbiAgICAgICAgICB9ICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIG1heEVmZmVjdFRpbWU7XG4gICAgfVxuXG5cbiAgLy9BTkNIT1IgY2hpcCB0byB3aW4gcGxheWVyIGFuaW1hdGlvblxuICBwbGF5UGF5aW5nQW5pbWF0aW9uKHdpbkRvb3I6IGFueSwgdXNlcjogY2MuTm9kZSwgd29uQW1vdW50OiBudW1iZXIsIGR1cmF0aW9uOm51bWJlciA9IDEpe1xuICAgIGxldCB0d2VlblBhcmFtcyA9IHtcbiAgICAgIHVzZUJlemllcjogdHJ1ZSxcbiAgICAgIHBvc2l0aW9uIDogY2MuVmVjMy5aRVJPLFxuICAgICAgLy8gc2NhbGUgOiAwLFxuICAgIH1cblxuICAgIGxldCBiZXRTbG90ID0gdGhpcy5nZXREb29yKHdpbkRvb3IpO1xuXG4gICAgbGV0IHdpbkNoaXBzID0gdGhpcy5nZXRDaGlwRm9yUGF5aW5nKHdvbkFtb3VudCk7XG4gICAgbGV0IHR3ZWVuQ2hpcHM6U2ljYm9Db2luSXRlbUNvbXBbXSA9IFtdO1xuICAgIHdpbkNoaXBzLmZvckVhY2goY29pblR5cGUgPT4ge1xuICAgICAgbGV0IGNoaXBDb21wID0gdGhpcy5jcmVhdGVJdGVtQ29pbk1pbmkoY29pblR5cGUpO1xuICAgICAgY2hpcENvbXAubm9kZS5wYXJlbnQgPSBiZXRTbG90Lm1pbmlDaGlwQXJlYTtcbiAgICAgIGNoaXBDb21wLm5vZGUucG9zaXRpb24gPSBjYy5WZWMzLlpFUk87XG5cbiAgICAgIHR3ZWVuQ2hpcHMucHVzaChjaGlwQ29tcCk7XG4gICAgfSk7XG4gIFxuICAgIGxldCBzZWxmID0gdGhpcztcbiAgICBsZXQgZGVsYXlCZXR3ZWVuQ2hpcFRpbWUgPSAuMTtcbiAgICBzZWxmLnR3ZWVuQ2hpcFRvQW5vdGhlck5vZGUodXNlciwgdHdlZW5DaGlwc1xuICAgICAgICAsIGR1cmF0aW9uLCBkZWxheUJldHdlZW5DaGlwVGltZSwgdHdlZW5QYXJhbXNcbiAgICAgICAgLCB0cnVlLCAgKGNoaXA6U2ljYm9Db2luSXRlbUNvbXApPT57XG4gICAgICAgICAgc2VsZi5yZWN5Y2xlSXRlbUNvaW5NaW5pKGNoaXAubm9kZSk7ICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICB9KTtcbiAgICB0aGlzLmNsZWFyTWluaUNoaXBPblNsb3QoYmV0U2xvdCk7XG4gIH1cblxuICBjbGVhck1pbmlDaGlwT25TbG90KGJldFNsb3Q6IFNpY2JvQmV0U2xvdEl0ZW1Db21wKXtcbiAgICBiZXRTbG90Lm1pbmlDaGlwc09uU2xvdC5mb3JFYWNoKGNoaXAgPT4ge1xuICAgICAgdGhpcy5yZWN5Y2xlSXRlbUNvaW5NaW5pKGNoaXAubm9kZSk7ICAgXG4gICAgfSk7XG4gICAgYmV0U2xvdC5jbGVhck1pbmlDaGlwT25TbG90KCk7XG5cbiAgICBiZXRTbG90Lm1pbmlDaGlwc09uU3RhY2suZm9yRWFjaChjaGlwID0+IHtcbiAgICAgIHRoaXMucmVjeWNsZUl0ZW1Db2luTWluaShjaGlwLm5vZGUpO1xuICAgIH0pO1xuICAgIGJldFNsb3QuZGVsZXRlU3RhY2tDaGlwUm9vdCgpO1xuICB9XG5cblxuICBkZXN0cm95QWxsTWluaUNoaXAoKXtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuYWxsQmV0U2xvdHMubGVuZ3RoOyBpKyspIHsgICBcbiAgICAgIGxldCBiZXRTbG90ID0gdGhpcy5hbGxCZXRTbG90c1tpXTtcbiAgICAgIGJldFNsb3QubWluaUNoaXBzT25TbG90LmZvckVhY2goY2hpcCA9PiB7XG4gICAgICAgIGNoaXAubm9kZT8uZGVzdHJveSgpOyAgIFxuICAgICAgfSk7XG4gICAgICBiZXRTbG90LmNsZWFyTWluaUNoaXBPblNsb3QoKTtcbiAgXG4gICAgICBiZXRTbG90Lm1pbmlDaGlwc09uU3RhY2suZm9yRWFjaChjaGlwID0+IHtcbiAgICAgICAgY2hpcC5ub2RlPy5kZXN0cm95KCk7ICBcbiAgICAgIH0pO1xuICAgICAgYmV0U2xvdC5kZWxldGVTdGFja0NoaXBSb290KCk7XG4gICAgfVxuICAgIHRoaXMubWluaUNoaXBQb29sLmNsZWFyKCk7XG4gIH1cblxuICBnZXRDaGlwRm9yUGF5aW5nKHdvblZhbHVlOm51bWJlcik6U2ljYm9Db2luVHlwZVtde1xuICAgIGlmKHdvblZhbHVlPD0gNTAwMCl7XG4gICAgICByZXR1cm4gW1NpY2JvQ29pblR5cGUuQ29pbjFrLCBTaWNib0NvaW5UeXBlLkNvaW4xaywgU2ljYm9Db2luVHlwZS5Db2luMWtdO1xuICAgIH1lbHNlIGlmKHdvblZhbHVlPD0gMjAwMDApe1xuICAgICAgcmV0dXJuIFtTaWNib0NvaW5UeXBlLkNvaW41aywgU2ljYm9Db2luVHlwZS5Db2luNWssIFNpY2JvQ29pblR5cGUuQ29pbjVrXTtcbiAgICB9ZWxzZSBpZih3b25WYWx1ZTw9IDEwMDAwMCl7XG4gICAgICByZXR1cm4gW1NpY2JvQ29pblR5cGUuQ29pbjEwMGtdO1xuICAgIH1lbHNlIGlmKHdvblZhbHVlPD0gMTAwMDAwMCl7XG4gICAgICByZXR1cm4gW1NpY2JvQ29pblR5cGUuQ29pbjFNXTtcbiAgICB9ZWxzZXtcbiAgICAgIHJldHVybiBbU2ljYm9Db2luVHlwZS5Db2luNU0sIFNpY2JvQ29pblR5cGUuQ29pbjVNLCBTaWNib0NvaW5UeXBlLkNvaW41TV07XG4gICAgfVxuICB9XG5cblxuICAgIHJlbW92ZVJlZHVuZGFuY3lDaGlwKGJldFNsb3Q6IFNpY2JvQmV0U2xvdEl0ZW1Db21wKXtcbiAgICAgIGxldCBtZXJnZUZyb20gPSAtMTtcbiAgICAgIGlmKHRoaXMuaXNTdGF0aWNTbG90KGJldFNsb3QuZG9vcikpe1xuICAgICAgICBpZiAodGhpcy5nZXRDaGlwU3RhY2tNYXhDb2woYmV0U2xvdC5kb29yKT4xKSB7XG4gICAgICAgICAgbWVyZ2VGcm9tID0gdGhpcy5nZXRDaGlwU3RhY2tIZWlnaHQoYmV0U2xvdC5kb29yLCAwKTtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgbWVyZ2VGcm9tID0gMDtcbiAgICAgICAgfVxuXG4gICAgICAgIG1lcmdlRnJvbSArPSA1O1xuICAgICAgfVxuXG4gICAgICBsZXQgcmMgPSBiZXRTbG90LmdldFJlZHVuZGFuY3lDaGlwKHRoaXMuaXNTdGF0aWNTbG90KGJldFNsb3QuZG9vciksIG1lcmdlRnJvbSk7XG4gICAgICByYy5mb3JFYWNoKGNoaXAgPT4ge1xuICAgICAgICB0aGlzLnJlY3ljbGVJdGVtQ29pbk1pbmkoY2hpcC5ub2RlKTsgICAgICAgICBcbiAgICAgIH0pOyAgICAgIFxuICAgIH0gICAgXG4gICAgXG4gICAgdHdlZW5DaGlwVG9Bbm90aGVyTm9kZSh0YXJnZXROb2RlOiBjYy5Ob2RlLCBjaGlwTGlzdDogU2ljYm9Db2luSXRlbUNvbXBbXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAsIGR1cmF0aW9uOiBudW1iZXIsIGRlbGF5QmV0d2VlbkNoaXBUaW1lOiBudW1iZXIgPSAwLCB0d2VlblBhcmFtczphbnlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLCBjaGFuZ2VQYXJlbnRUb1RhcmdldE5vZGU6IGJvb2xlYW4gPSB0cnVlLCBvbkNvbXBsZXRlOiAoY2hpcDogU2ljYm9Db2luSXRlbUNvbXApID0+IHZvaWQpe1xuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjaGlwTGlzdC5sZW5ndGg7IGkrKykge1xuICAgICAgICBsZXQgY2hpcCA9IGNoaXBMaXN0W2ldO1xuICAgICAgICBsZXQgY2FjaGVkUGFyZW50ID0gY2hpcC5ub2RlLnBhcmVudDtcblxuICAgICAgICBpZihjaGlwLm5vZGUucGFyZW50ID09IG51bGwpIHtcbiAgICAgICAgICAvL2NjLmxvZyhcIkZhaWwgdG8gdHdlZW5DaGlwVG9Bbm90aGVyTm9kZVwiKTtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuY2hhbmdlUGFyZW50KGNoaXAubm9kZSwgdGFyZ2V0Tm9kZSk7ICAgICAgXG4gICAgICAgIFxuICAgICAgICBsZXQgdHdlZW46Y2MuVHdlZW4gPSBudWxsO1xuICAgICAgICBpZih0d2VlblBhcmFtcy51c2VCZXppZXIpe1xuICAgICAgICAgIGxldCBzdGFydFBvcyA9IG5ldyBjYy5WZWMyKGNoaXAubm9kZS5wb3NpdGlvbi54LCBjaGlwLm5vZGUucG9zaXRpb24ueSk7XG4gICAgICAgICAgbGV0IGVuZFBvcyA9IG5ldyBjYy5WZWMyKHR3ZWVuUGFyYW1zLnBvc2l0aW9uLngsIHR3ZWVuUGFyYW1zLnBvc2l0aW9uLnkpO1xuICAgICAgICAgIHR3ZWVuID0gIGNjLnR3ZWVuKGNoaXAubm9kZSkuZGVsYXkoaSAqIGRlbGF5QmV0d2VlbkNoaXBUaW1lKS5iZXppZXJUbyhkdXJhdGlvbiwgc3RhcnRQb3MsIG5ldyBjYy5WZWMyKHN0YXJ0UG9zLngsIHN0YXJ0UG9zLnkgKyAoZW5kUG9zLnktIHN0YXJ0UG9zLnkpKjEpLCBlbmRQb3MpO1xuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICB0d2VlbiA9IGNjLnR3ZWVuKGNoaXAubm9kZSkuZGVsYXkoaSAqIGRlbGF5QmV0d2VlbkNoaXBUaW1lKS50byhkdXJhdGlvbiAsIHR3ZWVuUGFyYW1zKTtcbiAgICAgICAgfVxuICAgICAgIFxuICAgICAgICB0d2Vlbi5jYWxsKFxuICAgICAgICAgICgpPT57XG4gICAgICAgICAgICBpZihjaGFuZ2VQYXJlbnRUb1RhcmdldE5vZGUgPT0gZmFsc2UpIFxuICAgICAgICAgICAgICB0aGlzLmNoYW5nZVBhcmVudChjaGlwLm5vZGUsIGNhY2hlZFBhcmVudCk7ICAgXG4gICAgICAgICAgICBpZihvbkNvbXBsZXRlICE9IG51bGwpIG9uQ29tcGxldGUoY2hpcCk7ICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgfVxuICAgICAgICApXG4gICAgICAgIC5zdGFydCgpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vQU5DSE9SIFR3ZWVuICAgIFxuICAgIHR3ZWVuKG5vZGU6Y2MuTm9kZSwgdGFyZ2V0Tm9kZTogY2MuTm9kZSwgdHdlZW5QYXJhbXM6YW55LCBvbkNvbXBsZXRlOiAoKSA9PiB2b2lkKXtcbiAgICAgIGxldCBkdXJhdGlvbiA9IHR3ZWVuUGFyYW1zLmR1cmF0aW9uIHx8IDA7XG4gICAgICBsZXQgZGVsYXkgPSB0d2VlblBhcmFtcy5kZWxheSB8fCAwO1xuICAgICAgbGV0IHRhcmdldFBvcyA9IHR3ZWVuUGFyYW1zLnBvc2l0aW9uSW5UYXJnZXROb2RlIHx8IGNjLlZlYzIuWkVSTzsgICAgXG4gICAgICBsZXQgYmV6aWVyUG9pbnQgPSB0d2VlblBhcmFtcy5iZXppZXJQb2ludCB8fCBjYy5WZWMyLlpFUk87ICAgIFxuXG4gICAgICB0YXJnZXRQb3MgPSBTaWNib0dhbWVVdGlscy5jb252ZXJ0VG9PdGhlck5vZGUyKG5vZGUucGFyZW50LCB0YXJnZXROb2RlLCB0YXJnZXRQb3MpO1xuICBcbiAgICAgIGxldCB0d2VlbjpjYy5Ud2VlbiA9IG51bGw7XG4gICAgICBcbiAgICAgIGxldCBzdGFydFBvcyA9IG5ldyBjYy5WZWMyKG5vZGUucG9zaXRpb24ueCwgbm9kZS5wb3NpdGlvbi55KTtcbiAgICAgIGxldCBlbmRQb3MgPSB0YXJnZXRQb3M7XG4gICAgICBpZih0d2VlblBhcmFtcy51c2VCZXppZXIpe1xuICAgICAgICB0d2VlbiA9ICBjYy50d2Vlbihub2RlKS5kZWxheShkZWxheSkuYmV6aWVyVG8oZHVyYXRpb24sIHN0YXJ0UG9zLCBiZXppZXJQb2ludCwgZW5kUG9zKTtcbiAgICAgIH1lbHNle1xuICAgICAgICB0d2VlbiA9IGNjLnR3ZWVuKG5vZGUpLmRlbGF5KGRlbGF5KS50byhkdXJhdGlvbiAsIHt4OiBlbmRQb3MueCwgeTogZW5kUG9zLnl9KTtcbiAgICAgIH1cbiAgICAgIFxuICAgICAgdHdlZW4uY2FsbChcbiAgICAgICAgKCk9PnsgICAgICAgICAgXG4gICAgICAgICAgb25Db21wbGV0ZSAmJiBvbkNvbXBsZXRlKCk7ICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICAgIClcbiAgICAgIC5zdGFydCgpO1xuICAgIH1cbiAgLy8hU0VDVElPTlxuICBcbiAgLy9TRUNUSU9OIHVwZGF0ZSBiZXQgbnVtYmVyIGluIHRhYmxlICBcbiAgdXBkYXRlTXlVc2VyQmV0T25UYWJsZSh1c2VyQmV0QW1vdW50czogSW5zdGFuY2VUeXBlPHR5cGVvZiBEb29yQW5kQmV0QW1vdW50PltdKSB7XG4gICAgdXNlckJldEFtb3VudHMuZm9yRWFjaChiZXRBbW91bnQgPT4ge1xuICAgICAgbGV0IGRvb3IgPSB0aGlzLmdldERvb3IoYmV0QW1vdW50LmdldERvb3IoKSk7XG4gICAgICBkb29yPy5zZXRNeVRvdGFsQmV0KGJldEFtb3VudC5nZXRCZXRBbW91bnQoKSk7XG4gICAgfSk7XG4gIH1cblxuICB1cGRhdGVUb3RhbEJldE9uVGFibGUodG90YWxCZXRBbW91bnRzOiBJbnN0YW5jZVR5cGU8dHlwZW9mIERvb3JBbmRCZXRBbW91bnQ+W10pIHtcbiAgICB0b3RhbEJldEFtb3VudHMuZm9yRWFjaChiZXRBbW91bnQgPT4ge1xuICAgICAgbGV0IGRvb3IgPSB0aGlzLmdldERvb3IoYmV0QW1vdW50LmdldERvb3IoKSk7XG4gICAgICBkb29yPy5zZXRUb3RhbEJldChiZXRBbW91bnQuZ2V0QmV0QW1vdW50KCkpO1xuICAgIH0pO1xuICB9XG5cbiAgLy8hU0VDVElPTlxuXG4gIC8vU0VDVElPTiBJU2ljYm9TdGF0ZVxuICBleGl0U3RhdGUoc3RhdHVzOiBhbnkpOiB2b2lkIHsgIFxuICAgIGlmKHN0YXR1cyA9PSBTdGF0dXMuUEFZSU5HKXtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5hbGxCZXRTbG90cy5sZW5ndGg7IGkrKykge1xuICAgICAgICB0aGlzLmFsbEJldFNsb3RzW2ldLmNsZWFyTWluaUNoaXBPblNsb3QoKTtcbiAgICAgICAgdGhpcy5hbGxCZXRTbG90c1tpXS5yZXNldFNsb3QoKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICB1cGRhdGVTdGF0ZShzdGF0ZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGF0ZT4pe1xuICAgIGlmKHN0YXRlLmdldFN0YXR1cygpID09IFN0YXR1cy5CRVRUSU5HIHx8IHN0YXRlLmdldFN0YXR1cygpID09IFN0YXR1cy5FTkRfQkVUIHx8IHN0YXRlLmdldFN0YXR1cygpID09IFN0YXR1cy5SRVNVTFRJTkd8fCBzdGF0ZS5nZXRTdGF0dXMoKSA9PSBTdGF0dXMuUEFZSU5HKXtcbiAgICAgIHRoaXMudXBkYXRlVG90YWxCZXRPblRhYmxlKHN0YXRlLmdldERvb3JzQW5kQmV0QW1vdW50c0xpc3QoKSk7XG4gICAgICB0aGlzLnVwZGF0ZU15VXNlckJldE9uVGFibGUoc3RhdGUuZ2V0RG9vcnNBbmRQbGF5ZXJCZXRBbW91bnRzTGlzdCgpKTtcbiAgICB9XG5cbiAgICB0aGlzLnNldERvdWJsZUJldEFjdGl2ZShzdGF0ZS5nZXRBbGxvd0JldERvdWJsZSgpKTtcbiAgICB0aGlzLnNldFJlYmV0QWN0aXZlKHN0YXRlLmdldEFsbG93UmVCZXQoKSk7XG4gIH1cblxuICBzdGFydFN0YXRlKHN0YXRlOiBJbnN0YW5jZVR5cGU8dHlwZW9mIFN0YXRlPiwgdG90YWxEdXJhdGlvbjogbnVtYmVyKTogdm9pZCB7ICAgXG4gICAgbGV0IHN0YXR1cyA9IHN0YXRlLmdldFN0YXR1cygpO1xuXG4gICAgdGhpcy5kaXNhYmxlQ2hpcCA9IHN0YXR1cyAhPSBTdGF0dXMuQkVUVElORztcbiAgICBcbiAgICBpZihzdGF0dXMgIT0gU3RhdHVzLldBSVRJTkcgJiYgc3RhdHVzICE9IFN0YXR1cy5GSU5JU0hJTkcpe1xuICAgICAgc3RhdGUuZ2V0RG9vcnNBbmRCZXRBbW91bnRzTGlzdCgpLmZvckVhY2goZGFiID0+IHtcbiAgICAgICAgbGV0IGRvb3JDb21wID0gdGhpcy5nZXREb29yKGRhYi5nZXREb29yKCkpO1xuICAgICAgICBpZihkb29yQ29tcC5taW5pQ2hpcHNPblNsb3QubGVuZ3RoID09IDApXG4gICAgICAgICAgdGhpcy5wdXRNaW5pQ2hpcE9uVGFibGVJbW1lZGlhdGVseUJ5VmFsdWUoZGFiLmdldEJldEFtb3VudCgpLCBkb29yQ29tcCk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZihzdGF0dXMgPT0gU3RhdHVzLldBSVRJTkd8fHN0YXR1cyA9PSBTdGF0dXMuRklOSVNISU5HKXtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5hbGxCZXRTbG90cy5sZW5ndGg7IGkrKykgeyAgICAgICAgXG4gICAgICAgIHRoaXMuYWxsQmV0U2xvdHNbaV0ucmVzZXRTbG90KCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLy9BTkNIT1IgZGlzYWJsZSBjaGlwXG4gIHB1YmxpYyBnZXQgZGlzYWJsZUNoaXAoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuX2Rpc2FibGVDaGlwO1xuICB9XG4gIHB1YmxpYyBzZXQgZGlzYWJsZUNoaXAodmFsdWU6IGJvb2xlYW4pIHtcbiAgICB0aGlzLl9kaXNhYmxlQ2hpcCA9IHZhbHVlO1xuICAgIHRoaXMuY2hpcEl0ZW1zLmZvckVhY2goY2hpcCA9PiB7XG4gICAgICBjaGlwLmlzRGlzYWJsZSA9IHZhbHVlO1xuICAgICAgaWYoIXZhbHVlKXtcbiAgICAgICAgaWYodGhpcy5jdXJyZW50U2VsZWN0ZWRDaGlwIT1udWxsKSBcbiAgICAgICAgICB0aGlzLnNlbGVjdENoaXAodGhpcy5jdXJyZW50U2VsZWN0ZWRDaGlwLCBmYWxzZSk7XG4gICAgICAgIGVsc2UgXG4gICAgICAgICAgdGhpcy5zZWxlY3RDaGlwKHRoaXMuY2hpcEl0ZW1zWzBdLCBmYWxzZSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICAvL1F1aWNrIGJldFxuICBzZXREb3VibGVCZXRBY3RpdmUoaXNBY3RpdmU6IGJvb2xlYW4pe1xuICAgIHRoaXMuZG91YmxlQmV0QnV0dG9uLmludGVyYWN0YWJsZSA9IGlzQWN0aXZlO1xuICB9XG5cbiAgc2V0UmViZXRBY3RpdmUoaXNBY3RpdmU6IGJvb2xlYW4pe1xuICAgIHRoaXMucmViZXRCdXR0b24uaW50ZXJhY3RhYmxlID0gaXNBY3RpdmU7XG4gIH1cblxuICAvLyFTRUNUSU9OXG4gIG9uRGVzdHJveSgpe1xuICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLm5vZGUpO1xuICB9XG5cbiAgb25IaWRlKCk6IHZvaWQge1xuICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLm5vZGUpO1xuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmFsbEJldFNsb3RzLmxlbmd0aDsgaSsrKSB7ICAgICAgICBcbiAgICAgIHRoaXMuYWxsQmV0U2xvdHNbaV0ucmVzZXRTbG90KCk7XG4gICAgICAvLyB0aGlzLmNsZWFyTWluaUNoaXBPblNsb3QodGhpcy5hbGxCZXRTbG90c1tpXSk7XG4gICAgfVxuICAgIHRoaXMuZGVzdHJveUFsbE1pbmlDaGlwKCk7XG4gIH1cbn1cbiJdfQ==