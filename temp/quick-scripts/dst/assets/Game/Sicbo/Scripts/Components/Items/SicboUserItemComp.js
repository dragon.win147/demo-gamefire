
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/Items/SicboUserItemComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '006d0fFKAxOp7IdxGpB1jDr', 'SicboUserItemComp');
// Game/Sicbo/Scripts/Components/Items/SicboUserItemComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboUserItemCompData = void 0;
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var Playah = SicboModuleAdapter_1.default.getAllRefs().Playah;
var SicboMoneyFormatComp_1 = require("../../RNGCommons/SicboMoneyFormatComp");
var SicboPortalAdapter_1 = require("../../SicboPortalAdapter");
var SicboAvatarWinMoneyComp_1 = require("../SicboAvatarWinMoneyComp");
var SicboUIComp_1 = require("../SicboUIComp");
var SicboUserItemEffect_1 = require("../SicboUserItemEffect");
var SicboSetting_1 = require("../../Setting/SicboSetting");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboUserItemCompData = /** @class */ (function () {
    function SicboUserItemCompData() {
        this.balance = 0;
        this.chatIndex = 0;
    }
    return SicboUserItemCompData;
}());
exports.SicboUserItemCompData = SicboUserItemCompData;
var SicboUserItemComp = /** @class */ (function (_super) {
    __extends(SicboUserItemComp, _super);
    function SicboUserItemComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.iconSprite = null;
        _this.frame = null;
        _this.nickNameLabel = null;
        _this.userMoneyComp = null;
        _this.betShakeNode = null;
        _this.winMoneyEffect = null;
        _this.data = null;
        return _this;
    }
    SicboUserItemComp.prototype.onLoad = function () {
        var _a;
        this.cachedPos = (_a = this.betShakeNode) === null || _a === void 0 ? void 0 : _a.getPosition();
    };
    SicboUserItemComp.prototype.updateUI = function () {
        if (this.data.userId != "") {
            this.node.active = true;
            this.setIconUI(this.data.avatarId);
            this.setFrame(this.data.framePath);
            this.setNickNameUI(this.data.nickName);
            this.setMoneyUI(this.data.balance);
        }
    };
    SicboUserItemComp.prototype.setData = function (playah, chatIndex) {
        var _a, _b, _c, _d, _e, _f;
        if (playah === void 0) { playah = null; }
        if (chatIndex === void 0) { chatIndex = -1; }
        if (this.data == null)
            this.data = new SicboUserItemCompData();
        var userId = "";
        var basePath = "";
        var framePath = "";
        var nickName = "";
        var balance = 0;
        if (playah) {
            userId = playah.getUserId();
            basePath = SicboPortalAdapter_1.default.getInstance().onGetBasePath((_a = playah.getAvatar()) === null || _a === void 0 ? void 0 : _a.getBaseId());
            framePath = SicboPortalAdapter_1.default.getInstance().onGetFramePath((_b = playah.getAvatar()) === null || _b === void 0 ? void 0 : _b.getFrameId());
            nickName = (_c = playah.getProfile()) === null || _c === void 0 ? void 0 : _c.getDisplayName();
            balance = playah.getWallet() ? (_d = playah.getWallet()) === null || _d === void 0 ? void 0 : _d.getCurrentBalance() : 0;
        }
        this.data.rawPlayah = playah;
        this.data.userId = userId;
        this.data.avatarId = basePath;
        this.data.framePath = framePath;
        this.data.nickName = nickName;
        this.data.balance = balance;
        this.data.chatIndex = chatIndex;
        this.updateUI();
        var animDuration = SicboSetting_1.default.USER_ANIM_DURATION / 2;
        if (playah != null)
            (_e = this.node.getComponent(SicboUserItemEffect_1.default)) === null || _e === void 0 ? void 0 : _e.fadeIn(animDuration);
        else
            (_f = this.node.getComponent(SicboUserItemEffect_1.default)) === null || _f === void 0 ? void 0 : _f.fadeOut(animDuration);
    };
    SicboUserItemComp.prototype.getUserId = function () {
        var _a;
        return (_a = this.data) === null || _a === void 0 ? void 0 : _a.userId;
    };
    SicboUserItemComp.prototype.getChatIndex = function () {
        var _a;
        return (_a = this.data) === null || _a === void 0 ? void 0 : _a.chatIndex;
    };
    SicboUserItemComp.prototype.setMoney = function (money) {
        this.data.balance = money;
        this.setMoneyUI(money, true);
    };
    SicboUserItemComp.prototype.setIconUI = function (avatarId) {
        // SicboResourceManager.loadRemoteImageWithPath(this.iconSprite, avatarId);
    };
    SicboUserItemComp.prototype.setFrame = function (framePath) {
        // if (this.frame) SicboResourceManager.loadRemoteImageWithPath(this.frame, framePath);
    };
    SicboUserItemComp.prototype.setNickNameUI = function (nickName) {
        this.nickNameLabel.string = nickName;
    };
    SicboUserItemComp.prototype.setMoneyUI = function (money, isEffect) {
        if (isEffect === void 0) { isEffect = false; }
        money = Math.max(money, 0);
        if (isEffect)
            this.userMoneyComp.runMoneyTo(money);
        else
            this.userMoneyComp.setMoney(money);
    };
    SicboUserItemComp.prototype.shakeBet = function (target, distance) {
        var _this = this;
        if (distance === void 0) { distance = 15; }
        this.betShakeNode.setPosition(this.cachedPos);
        var tweenNode = this.betShakeNode;
        var point = cc.Vec2.ZERO;
        var targetPoint = target.convertToWorldSpaceAR(cc.Vec2.ZERO);
        var tweenNodePoint = tweenNode.convertToWorldSpaceAR(cc.Vec2.ZERO);
        cc.Vec2.subtract(point, targetPoint, tweenNodePoint);
        point = point.normalize().mul(distance);
        var eff = cc.tween(tweenNode).to(0.1, { x: point.x + this.cachedPos.x, y: point.y + this.cachedPos.y }); //, { easing: 'elasticInOut'}
        var effB = cc
            .tween(tweenNode)
            .to(0.1, { x: this.cachedPos.x, y: this.cachedPos.y })
            .call(function () {
            _this.betShakeNode.setPosition(_this.cachedPos);
        });
        eff.then(effB).start();
    };
    SicboUserItemComp.prototype.showWinMoney = function (amount) {
        var _a;
        (_a = this.winMoneyEffect) === null || _a === void 0 ? void 0 : _a.show(amount);
    };
    SicboUserItemComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.betShakeNode);
    };
    SicboUserItemComp.prototype.onClickAvatar = function () {
        SicboUIComp_1.default.Instance.showUserProfilePopUp(this.data.rawPlayah);
    };
    __decorate([
        property(cc.Sprite)
    ], SicboUserItemComp.prototype, "iconSprite", void 0);
    __decorate([
        property(cc.Sprite)
    ], SicboUserItemComp.prototype, "frame", void 0);
    __decorate([
        property(cc.Label)
    ], SicboUserItemComp.prototype, "nickNameLabel", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboUserItemComp.prototype, "userMoneyComp", void 0);
    __decorate([
        property(cc.Node)
    ], SicboUserItemComp.prototype, "betShakeNode", void 0);
    __decorate([
        property(SicboAvatarWinMoneyComp_1.default)
    ], SicboUserItemComp.prototype, "winMoneyEffect", void 0);
    SicboUserItemComp = __decorate([
        ccclass
    ], SicboUserItemComp);
    return SicboUserItemComp;
}(cc.Component));
exports.default = SicboUserItemComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvL1NjcmlwdHMvQ29tcG9uZW50cy9JdGVtcy9TaWNib1VzZXJJdGVtQ29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYsd0VBQW1FO0FBRTNELElBQUEsTUFBTSxHQUFLLDRCQUFrQixDQUFDLFVBQVUsRUFBRSxPQUFwQyxDQUFxQztBQUNuRCw4RUFBeUU7QUFDekUsK0RBQTBEO0FBQzFELHNFQUFpRTtBQUNqRSw4Q0FBeUM7QUFFekMsOERBQXlEO0FBQ3pELDJEQUFzRDtBQUVoRCxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUU1QztJQUFBO1FBTVMsWUFBTyxHQUFXLENBQUMsQ0FBQztRQUNwQixjQUFTLEdBQVcsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFBRCw0QkFBQztBQUFELENBUkEsQUFRQyxJQUFBO0FBUlksc0RBQXFCO0FBV2xDO0lBQStDLHFDQUFZO0lBQTNEO1FBQUEscUVBcUlDO1FBbklDLGdCQUFVLEdBQWMsSUFBSSxDQUFDO1FBRzdCLFdBQUssR0FBYyxJQUFJLENBQUM7UUFHeEIsbUJBQWEsR0FBYSxJQUFJLENBQUM7UUFHL0IsbUJBQWEsR0FBeUIsSUFBSSxDQUFDO1FBRzNDLGtCQUFZLEdBQVksSUFBSSxDQUFDO1FBRzdCLG9CQUFjLEdBQTRCLElBQUksQ0FBQztRQUl4QyxVQUFJLEdBQTBCLElBQUksQ0FBQzs7SUFnSDVDLENBQUM7SUE5R0Msa0NBQU0sR0FBTjs7UUFDRSxJQUFJLENBQUMsU0FBUyxTQUFHLElBQUksQ0FBQyxZQUFZLDBDQUFFLFdBQVcsRUFBRSxDQUFDO0lBQ3BELENBQUM7SUFFRCxvQ0FBUSxHQUFSO1FBQ0UsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxFQUFFLEVBQUU7WUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNuQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNwQztJQUNILENBQUM7SUFFRCxtQ0FBTyxHQUFQLFVBQVEsTUFBMEMsRUFBRSxTQUFzQjs7UUFBbEUsdUJBQUEsRUFBQSxhQUEwQztRQUFFLDBCQUFBLEVBQUEsYUFBcUIsQ0FBQztRQUN4RSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSTtZQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxxQkFBcUIsRUFBRSxDQUFDO1FBQy9ELElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUNoQixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbEIsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ25CLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNsQixJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFFaEIsSUFBSSxNQUFNLEVBQUU7WUFDVixNQUFNLEdBQUcsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzVCLFFBQVEsR0FBRyw0QkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxhQUFhLE9BQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSwwQ0FBRSxTQUFTLEdBQUcsQ0FBQztZQUMzRixTQUFTLEdBQUcsNEJBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsY0FBYyxPQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsMENBQUUsVUFBVSxHQUFHLENBQUM7WUFDOUYsUUFBUSxTQUFHLE1BQU0sQ0FBQyxVQUFVLEVBQUUsMENBQUUsY0FBYyxFQUFFLENBQUM7WUFDakQsT0FBTyxHQUFHLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLE9BQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSwwQ0FBRSxpQkFBaUIsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzVFO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBRWhDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUVoQixJQUFJLFlBQVksR0FBRyxzQkFBWSxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQztRQUN2RCxJQUFJLE1BQU0sSUFBSSxJQUFJO1lBQUUsTUFBQSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQywwQ0FBRSxNQUFNLENBQUMsWUFBWSxFQUFFOztZQUNqRixNQUFBLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLDBDQUFFLE9BQU8sQ0FBQyxZQUFZLEVBQUU7SUFDMUUsQ0FBQztJQUVELHFDQUFTLEdBQVQ7O1FBQ0UsYUFBTyxJQUFJLENBQUMsSUFBSSwwQ0FBRSxNQUFNLENBQUM7SUFDM0IsQ0FBQztJQUVELHdDQUFZLEdBQVo7O1FBQ0UsYUFBTyxJQUFJLENBQUMsSUFBSSwwQ0FBRSxTQUFTLENBQUM7SUFDOUIsQ0FBQztJQUVELG9DQUFRLEdBQVIsVUFBUyxLQUFhO1FBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRU8scUNBQVMsR0FBakIsVUFBa0IsUUFBZ0I7UUFDaEMsMkVBQTJFO0lBQzdFLENBQUM7SUFFTyxvQ0FBUSxHQUFoQixVQUFpQixTQUFpQjtRQUNoQyx1RkFBdUY7SUFDekYsQ0FBQztJQUVPLHlDQUFhLEdBQXJCLFVBQXNCLFFBQWdCO1FBQ3BDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQztJQUN2QyxDQUFDO0lBRU8sc0NBQVUsR0FBbEIsVUFBbUIsS0FBYSxFQUFFLFFBQXlCO1FBQXpCLHlCQUFBLEVBQUEsZ0JBQXlCO1FBQ3pELEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztRQUUzQixJQUFJLFFBQVE7WUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQzs7WUFDOUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELG9DQUFRLEdBQVIsVUFBUyxNQUFlLEVBQUUsUUFBcUI7UUFBL0MsaUJBb0JDO1FBcEJ5Qix5QkFBQSxFQUFBLGFBQXFCO1FBQzdDLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUU5QyxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ2xDLElBQUksS0FBSyxHQUFZLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBRWxDLElBQUksV0FBVyxHQUFHLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzdELElBQUksY0FBYyxHQUFHLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRW5FLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxXQUFXLEVBQUUsY0FBYyxDQUFDLENBQUM7UUFDckQsS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFeEMsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsNkJBQTZCO1FBQ3RJLElBQUksSUFBSSxHQUFHLEVBQUU7YUFDVixLQUFLLENBQUMsU0FBUyxDQUFDO2FBQ2hCLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDckQsSUFBSSxDQUFDO1lBQ0osS0FBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxDQUFDO1FBQ0wsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRUQsd0NBQVksR0FBWixVQUFhLE1BQWM7O1FBQ3pCLE1BQUEsSUFBSSxDQUFDLGNBQWMsMENBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRTtJQUNwQyxDQUFDO0lBRUQscUNBQVMsR0FBVDtRQUNFLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQseUNBQWEsR0FBYjtRQUNFLHFCQUFXLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDakUsQ0FBQztJQWxJRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3lEQUNTO0lBRzdCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7b0RBQ0k7SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzs0REFDWTtJQUcvQjtRQURDLFFBQVEsQ0FBQyw4QkFBb0IsQ0FBQzs0REFDWTtJQUczQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzJEQUNXO0lBRzdCO1FBREMsUUFBUSxDQUFDLGlDQUF1QixDQUFDOzZEQUNhO0lBakI1QixpQkFBaUI7UUFEckMsT0FBTztPQUNhLGlCQUFpQixDQXFJckM7SUFBRCx3QkFBQztDQXJJRCxBQXFJQyxDQXJJOEMsRUFBRSxDQUFDLFNBQVMsR0FxSTFEO2tCQXJJb0IsaUJBQWlCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBTaWNib01vZHVsZUFkYXB0ZXIgZnJvbSBcIi4uLy4uLy4uLy4uLy4uL1NpY2JvTW9kdWxlQWRhcHRlclwiO1xuXG5jb25zdCB7IFBsYXlhaCB9ID0gU2ljYm9Nb2R1bGVBZGFwdGVyLmdldEFsbFJlZnMoKTtcbmltcG9ydCBTaWNib01vbmV5Rm9ybWF0Q29tcCBmcm9tIFwiLi4vLi4vUk5HQ29tbW9ucy9TaWNib01vbmV5Rm9ybWF0Q29tcFwiO1xuaW1wb3J0IFNpY2JvUG9ydGFsQWRhcHRlciBmcm9tIFwiLi4vLi4vU2ljYm9Qb3J0YWxBZGFwdGVyXCI7XG5pbXBvcnQgU2ljYm9BdmF0YXJXaW5Nb25leUNvbXAgZnJvbSBcIi4uL1NpY2JvQXZhdGFyV2luTW9uZXlDb21wXCI7XG5pbXBvcnQgU2ljYm9VSUNvbXAgZnJvbSBcIi4uL1NpY2JvVUlDb21wXCI7XG5pbXBvcnQgU2ljYm9SZXNvdXJjZU1hbmFnZXIgZnJvbSBcIi4uLy4uL01hbmFnZXJzL1NpY2JvUmVzb3VyY2VNYW5hZ2VyXCI7XG5pbXBvcnQgU2ljYm9Vc2VySXRlbUVmZmVjdCBmcm9tIFwiLi4vU2ljYm9Vc2VySXRlbUVmZmVjdFwiO1xuaW1wb3J0IFNpY2JvU2V0dGluZyBmcm9tIFwiLi4vLi4vU2V0dGluZy9TaWNib1NldHRpbmdcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuZXhwb3J0IGNsYXNzIFNpY2JvVXNlckl0ZW1Db21wRGF0YSB7XG4gIHB1YmxpYyByYXdQbGF5YWg6IEluc3RhbmNlVHlwZTx0eXBlb2YgUGxheWFoPjtcbiAgcHVibGljIHVzZXJJZDogc3RyaW5nO1xuICBwdWJsaWMgYXZhdGFySWQ6IHN0cmluZztcbiAgcHVibGljIGZyYW1lUGF0aDogc3RyaW5nO1xuICBwdWJsaWMgbmlja05hbWU6IHN0cmluZztcbiAgcHVibGljIGJhbGFuY2U6IG51bWJlciA9IDA7XG4gIHB1YmxpYyBjaGF0SW5kZXg6IG51bWJlciA9IDA7XG59XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1VzZXJJdGVtQ29tcCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gIEBwcm9wZXJ0eShjYy5TcHJpdGUpXG4gIGljb25TcHJpdGU6IGNjLlNwcml0ZSA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgZnJhbWU6IGNjLlNwcml0ZSA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICBuaWNrTmFtZUxhYmVsOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KFNpY2JvTW9uZXlGb3JtYXRDb21wKVxuICB1c2VyTW9uZXlDb21wOiBTaWNib01vbmV5Rm9ybWF0Q29tcCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIGJldFNoYWtlTm9kZTogY2MuTm9kZSA9IG51bGw7XG5cbiAgQHByb3BlcnR5KFNpY2JvQXZhdGFyV2luTW9uZXlDb21wKVxuICB3aW5Nb25leUVmZmVjdDogU2ljYm9BdmF0YXJXaW5Nb25leUNvbXAgPSBudWxsO1xuXG4gIGNhY2hlZFBvczogY2MuVmVjMjtcblxuICBwdWJsaWMgZGF0YTogU2ljYm9Vc2VySXRlbUNvbXBEYXRhID0gbnVsbDtcblxuICBvbkxvYWQoKSB7XG4gICAgdGhpcy5jYWNoZWRQb3MgPSB0aGlzLmJldFNoYWtlTm9kZT8uZ2V0UG9zaXRpb24oKTtcbiAgfVxuXG4gIHVwZGF0ZVVJKCkge1xuICAgIGlmICh0aGlzLmRhdGEudXNlcklkICE9IFwiXCIpIHtcbiAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgdGhpcy5zZXRJY29uVUkodGhpcy5kYXRhLmF2YXRhcklkKTtcbiAgICAgIHRoaXMuc2V0RnJhbWUodGhpcy5kYXRhLmZyYW1lUGF0aCk7XG4gICAgICB0aGlzLnNldE5pY2tOYW1lVUkodGhpcy5kYXRhLm5pY2tOYW1lKTtcbiAgICAgIHRoaXMuc2V0TW9uZXlVSSh0aGlzLmRhdGEuYmFsYW5jZSk7XG4gICAgfVxuICB9XG5cbiAgc2V0RGF0YShwbGF5YWg6IEluc3RhbmNlVHlwZTx0eXBlb2YgUGxheWFoPiA9IG51bGwsIGNoYXRJbmRleDogbnVtYmVyID0gLTEpIHtcbiAgICBpZiAodGhpcy5kYXRhID09IG51bGwpIHRoaXMuZGF0YSA9IG5ldyBTaWNib1VzZXJJdGVtQ29tcERhdGEoKTtcbiAgICBsZXQgdXNlcklkID0gXCJcIjtcbiAgICBsZXQgYmFzZVBhdGggPSBcIlwiO1xuICAgIGxldCBmcmFtZVBhdGggPSBcIlwiO1xuICAgIGxldCBuaWNrTmFtZSA9IFwiXCI7XG4gICAgbGV0IGJhbGFuY2UgPSAwO1xuXG4gICAgaWYgKHBsYXlhaCkge1xuICAgICAgdXNlcklkID0gcGxheWFoLmdldFVzZXJJZCgpO1xuICAgICAgYmFzZVBhdGggPSBTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0SW5zdGFuY2UoKS5vbkdldEJhc2VQYXRoKHBsYXlhaC5nZXRBdmF0YXIoKT8uZ2V0QmFzZUlkKCkpO1xuICAgICAgZnJhbWVQYXRoID0gU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkub25HZXRGcmFtZVBhdGgocGxheWFoLmdldEF2YXRhcigpPy5nZXRGcmFtZUlkKCkpO1xuICAgICAgbmlja05hbWUgPSBwbGF5YWguZ2V0UHJvZmlsZSgpPy5nZXREaXNwbGF5TmFtZSgpO1xuICAgICAgYmFsYW5jZSA9IHBsYXlhaC5nZXRXYWxsZXQoKSA/IHBsYXlhaC5nZXRXYWxsZXQoKT8uZ2V0Q3VycmVudEJhbGFuY2UoKSA6IDA7XG4gICAgfVxuXG4gICAgdGhpcy5kYXRhLnJhd1BsYXlhaCA9IHBsYXlhaDtcbiAgICB0aGlzLmRhdGEudXNlcklkID0gdXNlcklkO1xuICAgIHRoaXMuZGF0YS5hdmF0YXJJZCA9IGJhc2VQYXRoO1xuICAgIHRoaXMuZGF0YS5mcmFtZVBhdGggPSBmcmFtZVBhdGg7XG4gICAgdGhpcy5kYXRhLm5pY2tOYW1lID0gbmlja05hbWU7XG4gICAgdGhpcy5kYXRhLmJhbGFuY2UgPSBiYWxhbmNlO1xuICAgIHRoaXMuZGF0YS5jaGF0SW5kZXggPSBjaGF0SW5kZXg7XG5cbiAgICB0aGlzLnVwZGF0ZVVJKCk7XG5cbiAgICBsZXQgYW5pbUR1cmF0aW9uID0gU2ljYm9TZXR0aW5nLlVTRVJfQU5JTV9EVVJBVElPTiAvIDI7XG4gICAgaWYgKHBsYXlhaCAhPSBudWxsKSB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KFNpY2JvVXNlckl0ZW1FZmZlY3QpPy5mYWRlSW4oYW5pbUR1cmF0aW9uKTtcbiAgICBlbHNlIHRoaXMubm9kZS5nZXRDb21wb25lbnQoU2ljYm9Vc2VySXRlbUVmZmVjdCk/LmZhZGVPdXQoYW5pbUR1cmF0aW9uKTtcbiAgfVxuXG4gIGdldFVzZXJJZCgpIHtcbiAgICByZXR1cm4gdGhpcy5kYXRhPy51c2VySWQ7XG4gIH1cblxuICBnZXRDaGF0SW5kZXgoKSB7XG4gICAgcmV0dXJuIHRoaXMuZGF0YT8uY2hhdEluZGV4O1xuICB9XG5cbiAgc2V0TW9uZXkobW9uZXk6IG51bWJlcikge1xuICAgIHRoaXMuZGF0YS5iYWxhbmNlID0gbW9uZXk7XG4gICAgdGhpcy5zZXRNb25leVVJKG1vbmV5LCB0cnVlKTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0SWNvblVJKGF2YXRhcklkOiBzdHJpbmcpIHtcbiAgICAvLyBTaWNib1Jlc291cmNlTWFuYWdlci5sb2FkUmVtb3RlSW1hZ2VXaXRoUGF0aCh0aGlzLmljb25TcHJpdGUsIGF2YXRhcklkKTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0RnJhbWUoZnJhbWVQYXRoOiBzdHJpbmcpIHtcbiAgICAvLyBpZiAodGhpcy5mcmFtZSkgU2ljYm9SZXNvdXJjZU1hbmFnZXIubG9hZFJlbW90ZUltYWdlV2l0aFBhdGgodGhpcy5mcmFtZSwgZnJhbWVQYXRoKTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0Tmlja05hbWVVSShuaWNrTmFtZTogc3RyaW5nKSB7XG4gICAgdGhpcy5uaWNrTmFtZUxhYmVsLnN0cmluZyA9IG5pY2tOYW1lO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRNb25leVVJKG1vbmV5OiBudW1iZXIsIGlzRWZmZWN0OiBib29sZWFuID0gZmFsc2UpIHtcbiAgICBtb25leSA9IE1hdGgubWF4KG1vbmV5LCAwKTtcblxuICAgIGlmIChpc0VmZmVjdCkgdGhpcy51c2VyTW9uZXlDb21wLnJ1bk1vbmV5VG8obW9uZXkpO1xuICAgIGVsc2UgdGhpcy51c2VyTW9uZXlDb21wLnNldE1vbmV5KG1vbmV5KTtcbiAgfVxuXG4gIHNoYWtlQmV0KHRhcmdldDogY2MuTm9kZSwgZGlzdGFuY2U6IG51bWJlciA9IDE1KSB7XG4gICAgdGhpcy5iZXRTaGFrZU5vZGUuc2V0UG9zaXRpb24odGhpcy5jYWNoZWRQb3MpO1xuXG4gICAgbGV0IHR3ZWVuTm9kZSA9IHRoaXMuYmV0U2hha2VOb2RlO1xuICAgIGxldCBwb2ludDogY2MuVmVjMiA9IGNjLlZlYzIuWkVSTztcblxuICAgIGxldCB0YXJnZXRQb2ludCA9IHRhcmdldC5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoY2MuVmVjMi5aRVJPKTtcbiAgICBsZXQgdHdlZW5Ob2RlUG9pbnQgPSB0d2Vlbk5vZGUuY29udmVydFRvV29ybGRTcGFjZUFSKGNjLlZlYzIuWkVSTyk7XG5cbiAgICBjYy5WZWMyLnN1YnRyYWN0KHBvaW50LCB0YXJnZXRQb2ludCwgdHdlZW5Ob2RlUG9pbnQpO1xuICAgIHBvaW50ID0gcG9pbnQubm9ybWFsaXplKCkubXVsKGRpc3RhbmNlKTtcblxuICAgIGxldCBlZmYgPSBjYy50d2Vlbih0d2Vlbk5vZGUpLnRvKDAuMSwgeyB4OiBwb2ludC54ICsgdGhpcy5jYWNoZWRQb3MueCwgeTogcG9pbnQueSArIHRoaXMuY2FjaGVkUG9zLnkgfSk7IC8vLCB7IGVhc2luZzogJ2VsYXN0aWNJbk91dCd9XG4gICAgbGV0IGVmZkIgPSBjY1xuICAgICAgLnR3ZWVuKHR3ZWVuTm9kZSlcbiAgICAgIC50bygwLjEsIHsgeDogdGhpcy5jYWNoZWRQb3MueCwgeTogdGhpcy5jYWNoZWRQb3MueSB9KVxuICAgICAgLmNhbGwoKCkgPT4ge1xuICAgICAgICB0aGlzLmJldFNoYWtlTm9kZS5zZXRQb3NpdGlvbih0aGlzLmNhY2hlZFBvcyk7XG4gICAgICB9KTtcbiAgICBlZmYudGhlbihlZmZCKS5zdGFydCgpO1xuICB9XG5cbiAgc2hvd1dpbk1vbmV5KGFtb3VudDogbnVtYmVyKSB7XG4gICAgdGhpcy53aW5Nb25leUVmZmVjdD8uc2hvdyhhbW91bnQpO1xuICB9XG5cbiAgb25EZXN0cm95KCkge1xuICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLmJldFNoYWtlTm9kZSk7XG4gIH1cblxuICBvbkNsaWNrQXZhdGFyKCkge1xuICAgIFNpY2JvVUlDb21wLkluc3RhbmNlLnNob3dVc2VyUHJvZmlsZVBvcFVwKHRoaXMuZGF0YS5yYXdQbGF5YWgpO1xuICB9XG59XG4iXX0=