
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/States/SicboJackpotStateComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd0485XgZ0VO1YaTAZlg9tTG', 'SicboJackpotStateComp');
// Game/Sicbo_New/Scripts/Components/States/SicboJackpotStateComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Topic = _a.Topic, State = _a.State, Result = _a.Result, Status = _a.Status, ListCurrentJackpotsReply = _a.ListCurrentJackpotsReply;
var SicboSetting_1 = require("../../Setting/SicboSetting");
var SicboController_1 = require("../../Controllers/SicboController");
var SicboHelper_1 = require("../../Helpers/SicboHelper");
var SicboPortalAdapter_1 = require("../../SicboPortalAdapter");
var SicboGameUtils_1 = require("../../RNGCommons/Utils/SicboGameUtils");
var SicboSkeletonUtils_1 = require("../../RNGCommons/Utils/SicboSkeletonUtils");
var SicboMoneyFormatComp_1 = require("../../RNGCommons/SicboMoneyFormatComp");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboJackpotStateComp = /** @class */ (function (_super) {
    __extends(SicboJackpotStateComp, _super);
    function SicboJackpotStateComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //ANCHOR jackpot stand
        _this.jackpotStandNode = null;
        _this.jackPotStandSkeleton = null;
        _this.jackpotStandValueLabel = null;
        //ANCHOR my jackpot animation
        _this.myJackpotFrontSkeleton = null;
        _this.myJackpotBackSkeleton = null;
        _this.particlePrefab = null;
        _this.jackpotRandomLabel = null;
        _this.jackpotNumberLabel = null;
        _this.otherWinnerNameLabel = null;
        _this.jackpotAnimationStartTime = 0;
        // private jackpotAnimationEndTime = this.jackpotAnimationStartTime + 4;
        _this.isJackpotPlaying = false;
        _this.jackpotScaleSfxStartTime = _this.jackpotAnimationStartTime;
        _this.jackpotScaleSfxStopTime = _this.jackpotScaleSfxStartTime + 1.5;
        _this.jackpotFinalSfxStartTime = _this.jackpotScaleSfxStartTime + 1.5;
        _this.particleDelayTime = 1.8;
        _this.particleInstance = null;
        _this.jackpotValue = 0;
        return _this;
    }
    SicboJackpotStateComp.prototype.setResult = function (result) {
        if (SicboGameUtils_1.default.isNullOrUndefined(result))
            return;
        this.result = result;
    };
    SicboJackpotStateComp.prototype.onLoad = function () {
        this.setMyJackpotAnimActive(false);
        //this.jackpotStandNode.active = true;
        this.playJackpotStandAnimation(true);
        this.setOtherWinnerActive(false);
    };
    SicboJackpotStateComp.prototype.exitState = function (status) {
        this.isJackpotPlaying = false;
        // if(status != Status.RESULTING && status != Status.PAYING) {
        if (status != Status.PAYING) {
            this.setMyJackpotAnimActive(false); //NOTE: do anim jackpot hoi dai
            //this.jackpotStandNode.active = true;
        }
        this.stopParticleFX();
        this.playJackpotStandAnimation(true);
    };
    SicboJackpotStateComp.prototype.updateState = function (state) {
        var _this = this;
        if (this.isJackpotPlaying)
            return;
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        if (this.isUpdateJackpotValue(status) || this.jackpotStandValueLabel.moneyValue == 0) {
            // this.updateJackpot(state.getPot() || 0);// tạm ẩn không cập nhật theo state
            this.updateJackpot(this.jackpotValue || 0); // tạm ẩn không cập nhật theo state
        }
        if (SicboGameUtils_1.default.isNullOrUndefined(this.result))
            return;
        if (!this.result.hasLotteryWinner())
            return;
        // if(status == Status.RESULTING && elapseTime >= this.jackpotAnimationStartTime){
        if (status == Status.PAYING && elapseTime >= this.jackpotAnimationStartTime) {
            this.isJackpotPlaying = true;
            this.otherWinnerNameLabel.string = this.result.getLotteryWinner().getDisplayName();
            if (SicboPortalAdapter_1.default.getInstance().getMyUserId() == this.result.getLotteryWinner().getUserId()) {
                this.setMyJackpotAnimActive(true);
                this.playParticleFx(elapseTime - this.jackpotAnimationStartTime);
                //this.jackpotStandNode.active = false;
                SicboSkeletonUtils_1.default.setAnimation(this.myJackpotFrontSkeleton, "animation", false, elapseTime - this.jackpotAnimationStartTime);
                SicboSkeletonUtils_1.default.setAnimation(this.myJackpotBackSkeleton, "animation", false, elapseTime - this.jackpotAnimationStartTime);
                this.myJackpotBackSkeleton.setCompleteListener(function (_) {
                    _this === null || _this === void 0 ? void 0 : _this.setMyJackpotAnimActive(false);
                });
                // let self = this;
                // cc.tween(self.node)
                //     .delay(self.jackpotAnimationEndTime - elapseTime)
                //     .call(()=>self?.setMyJackpotAnimActive(false))
                //     .start();
                this.jackpotNumberLabel.setMoney(0);
                this.jackpotNumberLabel.runMoneyTo(this.result.getLotteryWinner().getAmount(), 3);
                this.setOtherWinnerActive(false);
                this.myJackpotSfx(SicboSetting_1.SicboSound.SfxJackpotScale, elapseTime, this.jackpotScaleSfxStartTime);
                this.myJackpotSfx(SicboSetting_1.SicboSound.SfxJackpotScale, elapseTime, this.jackpotScaleSfxStopTime, false);
                this.myJackpotSfx(SicboSetting_1.SicboSound.SfxJackpotFinal, elapseTime, this.jackpotFinalSfxStartTime);
                //this.jackpotStandNode.active = true;
                this.setOtherWinnerActive(true);
                this.playJackpotStandAnimation(false);
            }
            else {
                //Other user win jackpot
                this.setMyJackpotAnimActive(false);
                //this.jackpotStandNode.active = true;
                this.setOtherWinnerActive(true);
                this.playJackpotStandAnimation(false);
            }
        }
    };
    SicboJackpotStateComp.prototype.isUpdateJackpotValue = function (status) {
        switch (status) {
            case Status.PAYING:
            case Status.END_BET:
            case Status.RESULTING:
                return false;
            default: return true;
        }
    };
    SicboJackpotStateComp.prototype.handleResponseJackpot = function (res) {
        var _this = this;
        var getJackpotsList = res.getJackpotsList();
        // let mapJackpot = new Map<Topic, ListCurrentJackpotsReply.Jackpot[]>();
        getJackpotsList.forEach(function (jackpot) {
            var topic = jackpot.getTopic();
            if (topic == Topic.SICBO) {
                var jackpots = [jackpot];
                var amountJackpot = jackpots[0].getAmount();
                // this.updateJackpot(amountJackpot);
                _this.jackpotValue = amountJackpot;
                return;
            }
        });
    };
    SicboJackpotStateComp.prototype.updateJackpot = function (value) {
        //cc.log("updateJackpot " + value);
        this.jackpotStandValueLabel.runMoneyTo(value, .2);
        this.jackpotRandomLabel.runMoneyTo(value, .2);
    };
    SicboJackpotStateComp.prototype.myJackpotSfx = function (sfxName, elapseTime, time, isPlay) {
        if (elapseTime === void 0) { elapseTime = 0; }
        if (isPlay === void 0) { isPlay = true; }
        var self = this;
        if (elapseTime > time) {
            if (isPlay) {
                //qua tgian thi skip
                if (SicboHelper_1.default.approximatelyEqual(elapseTime, time))
                    SicboController_1.default.Instance.playSfx(sfxName);
            }
            else
                SicboController_1.default.Instance.stopSfx(sfxName);
        }
        else {
            cc.tween(self.node)
                .delay(time - elapseTime)
                .call(function () {
                if (isPlay)
                    SicboController_1.default.Instance.playSfx(sfxName);
                else
                    SicboController_1.default.Instance.stopSfx(sfxName);
            })
                .start();
        }
    };
    SicboJackpotStateComp.prototype.startState = function (state, totalDuration) {
        this.setResult(state.getResult());
        var status = state.getStatus();
        // if(status != Status.RESULTING && status != Status.PAYING)
        if (status != Status.PAYING)
            this.setOtherWinnerActive(false);
        // if( status == Status.PAYING && this.jackpotStandValueLabel.moneyValue != 0) return;
        // this.jackpotStandValueLabel.setMoney(state.getPot()||0);
        // this.jackpotRandomLabel.setMoney(state.getPot()||0);
    };
    SicboJackpotStateComp.prototype.setMyJackpotAnimActive = function (isActive) {
        if (isActive === void 0) { isActive = true; }
        this.myJackpotFrontSkeleton.node.active = isActive;
        this.myJackpotBackSkeleton.node.active = isActive;
        if (!isActive)
            this.stopParticleFX();
    };
    SicboJackpotStateComp.prototype.playParticleFx = function (elapseTime) {
        this.stopParticleFX();
        this.particleInstance = cc.instantiate(this.particlePrefab);
        this.particleInstance.setParent(this.node);
        this.particleInstance.setPosition(cc.Vec2.ZERO);
        var particles = this.particleInstance.getComponentsInChildren(cc.ParticleSystem3D);
        for (var i = 0; i < particles.length; i++) {
            var particle = particles[i];
            particle.startDelay.constant = Math.max(this.particleDelayTime - elapseTime, 0);
        }
    };
    SicboJackpotStateComp.prototype.stopParticleFX = function () {
        if (this.particleInstance)
            this.particleInstance.destroy();
    };
    SicboJackpotStateComp.prototype.onHide = function () {
        if (this.node != null)
            cc.Tween.stopAllByTarget(this.node);
        this.setMyJackpotAnimActive(false);
        //this.jackpotStandNode.active = true;
        this.isJackpotPlaying = false;
        this.jackPotStandPlayingAnim = null;
        this.setOtherWinnerActive(false);
        SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxJackpotFinal);
        SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxJackpotScale);
    };
    SicboJackpotStateComp.prototype.setOtherWinnerActive = function (active) {
        this.otherWinnerNameLabel.node.parent.active = active;
        this.jackpotStandValueLabel.node.active = !active;
    };
    SicboJackpotStateComp.prototype.playJackpotStandAnimation = function (isIdle) {
        var anim = "no_hu";
        if (isIdle) {
            anim = "idle";
        }
        if (this.jackPotStandPlayingAnim == anim)
            return;
        this.jackPotStandPlayingAnim = anim;
        SicboSkeletonUtils_1.default.setAnimation(this.jackPotStandSkeleton, anim, true);
    };
    SicboJackpotStateComp.prototype.onDestroy = function () {
        if (this.node != null)
            cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.Node)
    ], SicboJackpotStateComp.prototype, "jackpotStandNode", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboJackpotStateComp.prototype, "jackPotStandSkeleton", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboJackpotStateComp.prototype, "jackpotStandValueLabel", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboJackpotStateComp.prototype, "myJackpotFrontSkeleton", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboJackpotStateComp.prototype, "myJackpotBackSkeleton", void 0);
    __decorate([
        property(cc.Prefab)
    ], SicboJackpotStateComp.prototype, "particlePrefab", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboJackpotStateComp.prototype, "jackpotRandomLabel", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboJackpotStateComp.prototype, "jackpotNumberLabel", void 0);
    __decorate([
        property(cc.Label)
    ], SicboJackpotStateComp.prototype, "otherWinnerNameLabel", void 0);
    SicboJackpotStateComp = __decorate([
        ccclass
    ], SicboJackpotStateComp);
    return SicboJackpotStateComp;
}(cc.Component));
exports.default = SicboJackpotStateComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU3RhdGVzL1NpY2JvSmFja3BvdFN0YXRlQ29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVsRix3RUFBbUU7QUFFN0QsSUFBQSxLQU1GLDRCQUFrQixDQUFDLFVBQVUsRUFBRSxFQUxqQyxLQUFLLFdBQUEsRUFDTCxLQUFLLFdBQUEsRUFDTCxNQUFNLFlBQUEsRUFDTixNQUFNLFlBQUEsRUFDTix3QkFBd0IsOEJBQ1MsQ0FBQztBQUVwQywyREFBd0Q7QUFDeEQscUVBQWdFO0FBQ2hFLHlEQUFvRDtBQUVwRCwrREFBMEQ7QUFDMUQsd0VBQW1FO0FBQ25FLGdGQUEyRTtBQUMzRSw4RUFBeUU7QUFDbkUsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBbUQseUNBQVk7SUFBL0Q7UUFBQSxxRUF3UEM7UUF2UEMsc0JBQXNCO1FBRXRCLHNCQUFnQixHQUFZLElBQUksQ0FBQztRQUdqQywwQkFBb0IsR0FBZ0IsSUFBSSxDQUFDO1FBR3pDLDRCQUFzQixHQUF5QixJQUFJLENBQUM7UUFFcEQsNkJBQTZCO1FBRTdCLDRCQUFzQixHQUFnQixJQUFJLENBQUM7UUFHM0MsMkJBQXFCLEdBQWdCLElBQUksQ0FBQztRQUcxQyxvQkFBYyxHQUFjLElBQUksQ0FBQztRQUdqQyx3QkFBa0IsR0FBeUIsSUFBSSxDQUFDO1FBR2hELHdCQUFrQixHQUF5QixJQUFJLENBQUM7UUFHaEQsMEJBQW9CLEdBQWEsSUFBSSxDQUFDO1FBQzlCLCtCQUF5QixHQUFHLENBQUMsQ0FBQztRQUN0Qyx3RUFBd0U7UUFFaEUsc0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBR3pCLDhCQUF3QixHQUFXLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQztRQUNsRSw2QkFBdUIsR0FBVyxLQUFJLENBQUMsd0JBQXdCLEdBQUcsR0FBRyxDQUFDO1FBQ3RFLDhCQUF3QixHQUFXLEtBQUksQ0FBQyx3QkFBd0IsR0FBRyxHQUFHLENBQUM7UUFFdkUsdUJBQWlCLEdBQVcsR0FBRyxDQUFDO1FBR2hDLHNCQUFnQixHQUFZLElBQUksQ0FBQztRQUdqQyxrQkFBWSxHQUFXLENBQUMsQ0FBQzs7SUEyTW5DLENBQUM7SUF6TVMseUNBQVMsR0FBakIsVUFBa0IsTUFBbUM7UUFDbkQsSUFBSSx3QkFBYyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQztZQUFFLE9BQU87UUFDckQsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQUVELHNDQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbkMsc0NBQXNDO1FBQ3RDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELHlDQUFTLEdBQVQsVUFBVSxNQUFNO1FBQ2QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUU5Qiw4REFBOEQ7UUFDOUQsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtZQUMzQixJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQywrQkFBK0I7WUFDbkUsc0NBQXNDO1NBQ3ZDO1FBRUQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsMkNBQVcsR0FBWCxVQUFZLEtBQWlDO1FBQTdDLGlCQXVEQztRQXREQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0I7WUFBRSxPQUFPO1FBQ2xDLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUMvQixJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLEdBQUcsSUFBSSxDQUFDO1FBRTdDLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLElBQUksQ0FBQyxFQUFFO1lBQ2xGLDhFQUE4RTtZQUM5RSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQSxtQ0FBbUM7U0FDakY7UUFFRCxJQUFJLHdCQUFjLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUFFLE9BQU87UUFDMUQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUU7WUFBRSxPQUFPO1FBRTVDLGtGQUFrRjtRQUNsRixJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTSxJQUFJLFVBQVUsSUFBSSxJQUFJLENBQUMseUJBQXlCLEVBQUU7WUFDM0UsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUU3QixJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNuRixJQUFJLDRCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsRUFBRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLEVBQUUsRUFBRTtnQkFDaEcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztnQkFDakUsdUNBQXVDO2dCQUV2Qyw0QkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsVUFBVSxHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO2dCQUM5SCw0QkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsVUFBVSxHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO2dCQUM3SCxJQUFJLENBQUMscUJBQXFCLENBQUMsbUJBQW1CLENBQUMsVUFBQyxDQUFDO29CQUMvQyxLQUFJLGFBQUosS0FBSSx1QkFBSixLQUFJLENBQUUsc0JBQXNCLENBQUMsS0FBSyxFQUFFO2dCQUN0QyxDQUFDLENBQUMsQ0FBQztnQkFFSCxtQkFBbUI7Z0JBQ25CLHNCQUFzQjtnQkFDdEIsd0RBQXdEO2dCQUN4RCxxREFBcUQ7Z0JBQ3JELGdCQUFnQjtnQkFFaEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBRWxGLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFakMsSUFBSSxDQUFDLFlBQVksQ0FBQyx5QkFBVSxDQUFDLGVBQWUsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7Z0JBQ3pGLElBQUksQ0FBQyxZQUFZLENBQUMseUJBQVUsQ0FBQyxlQUFlLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDL0YsSUFBSSxDQUFDLFlBQVksQ0FBQyx5QkFBVSxDQUFDLGVBQWUsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7Z0JBRXpGLHNDQUFzQztnQkFDdEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNoQyxJQUFJLENBQUMseUJBQXlCLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdkM7aUJBQU07Z0JBQ0wsd0JBQXdCO2dCQUN4QixJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ25DLHNDQUFzQztnQkFDdEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNoQyxJQUFJLENBQUMseUJBQXlCLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdkM7U0FDRjtJQUNILENBQUM7SUFFRCxvREFBb0IsR0FBcEIsVUFBcUIsTUFBTTtRQUN6QixRQUFPLE1BQU0sRUFBQztZQUNaLEtBQUssTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUNuQixLQUFLLE1BQU0sQ0FBQyxPQUFPLENBQUM7WUFDcEIsS0FBSyxNQUFNLENBQUMsU0FBUztnQkFDbkIsT0FBTyxLQUFLLENBQUE7WUFFZCxPQUFPLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQztTQUN0QjtJQUNILENBQUM7SUFFRCxxREFBcUIsR0FBckIsVUFBc0IsR0FBa0Q7UUFBeEUsaUJBYUM7UUFaQyxJQUFJLGVBQWUsR0FBRyxHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDNUMseUVBQXlFO1FBQ3pFLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUFPO1lBQzlCLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUMvQixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsS0FBSyxFQUFFO2dCQUN4QixJQUFJLFFBQVEsR0FBNEQsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDbEYsSUFBSSxhQUFhLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUM1QyxxQ0FBcUM7Z0JBQ3JDLEtBQUksQ0FBQyxZQUFZLEdBQUcsYUFBYSxDQUFDO2dCQUNsQyxPQUFPO2FBQ1I7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw2Q0FBYSxHQUFiLFVBQWMsS0FBYTtRQUN6QixtQ0FBbUM7UUFDbkMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELDRDQUFZLEdBQVosVUFBYSxPQUFtQixFQUFFLFVBQXNCLEVBQUUsSUFBWSxFQUFFLE1BQXNCO1FBQTVELDJCQUFBLEVBQUEsY0FBc0I7UUFBZ0IsdUJBQUEsRUFBQSxhQUFzQjtRQUM1RixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFFaEIsSUFBSSxVQUFVLEdBQUcsSUFBSSxFQUFFO1lBQ3JCLElBQUksTUFBTSxFQUFFO2dCQUNWLG9CQUFvQjtnQkFDcEIsSUFBSSxxQkFBVyxDQUFDLGtCQUFrQixDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUM7b0JBQUUseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ2pHOztnQkFBTSx5QkFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDbEQ7YUFBTTtZQUNMLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztpQkFDaEIsS0FBSyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUM7aUJBQ3hCLElBQUksQ0FBQztnQkFDSixJQUFJLE1BQU07b0JBQUUseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDOztvQkFDakQseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2pELENBQUMsQ0FBQztpQkFDRCxLQUFLLEVBQUUsQ0FBQztTQUNaO0lBQ0gsQ0FBQztJQUVELDBDQUFVLEdBQVYsVUFBVyxLQUFpQyxFQUFFLGFBQXFCO1FBQ2pFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7UUFDbEMsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQy9CLDREQUE0RDtRQUM1RCxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTTtZQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU5RCxzRkFBc0Y7UUFDdEYsMkRBQTJEO1FBQzNELHVEQUF1RDtJQUN6RCxDQUFDO0lBRU8sc0RBQXNCLEdBQTlCLFVBQStCLFFBQXdCO1FBQXhCLHlCQUFBLEVBQUEsZUFBd0I7UUFDckQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDO1FBQ25ELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQztRQUVsRCxJQUFJLENBQUMsUUFBUTtZQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBRU8sOENBQWMsR0FBdEIsVUFBdUIsVUFBa0I7UUFDdkMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXRCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFaEQsSUFBSSxTQUFTLEdBQTBCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUMxRyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN6QyxJQUFJLFFBQVEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2pGO0lBQ0gsQ0FBQztJQUVPLDhDQUFjLEdBQXRCO1FBQ0UsSUFBSSxJQUFJLENBQUMsZ0JBQWdCO1lBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQzdELENBQUM7SUFFRCxzQ0FBTSxHQUFOO1FBQ0UsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUk7WUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25DLHNDQUFzQztRQUN0QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUM7UUFFcEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRWpDLHlCQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyx5QkFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzdELHlCQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyx5QkFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFTyxvREFBb0IsR0FBNUIsVUFBNkIsTUFBZTtRQUMxQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3RELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsTUFBTSxDQUFDO0lBQ3BELENBQUM7SUFFTyx5REFBeUIsR0FBakMsVUFBa0MsTUFBZTtRQUMvQyxJQUFJLElBQUksR0FBRyxPQUFPLENBQUM7UUFDbkIsSUFBSSxNQUFNLEVBQUU7WUFDVixJQUFJLEdBQUcsTUFBTSxDQUFDO1NBQ2Y7UUFFRCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsSUFBSSxJQUFJO1lBQUUsT0FBTztRQUNqRCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDO1FBQ3BDLDRCQUFrQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRCx5Q0FBUyxHQUFUO1FBQ0UsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUk7WUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQXBQRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21FQUNlO0lBR2pDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7dUVBQ21CO0lBR3pDO1FBREMsUUFBUSxDQUFDLDhCQUFvQixDQUFDO3lFQUNxQjtJQUlwRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO3lFQUNxQjtJQUczQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO3dFQUNvQjtJQUcxQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO2lFQUNhO0lBR2pDO1FBREMsUUFBUSxDQUFDLDhCQUFvQixDQUFDO3FFQUNpQjtJQUdoRDtRQURDLFFBQVEsQ0FBQyw4QkFBb0IsQ0FBQztxRUFDaUI7SUFHaEQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzt1RUFDbUI7SUE1Qm5CLHFCQUFxQjtRQUR6QyxPQUFPO09BQ2EscUJBQXFCLENBd1B6QztJQUFELDRCQUFDO0NBeFBELEFBd1BDLENBeFBrRCxFQUFFLENBQUMsU0FBUyxHQXdQOUQ7a0JBeFBvQixxQkFBcUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFNpY2JvTW9kdWxlQWRhcHRlciBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vU2ljYm9Nb2R1bGVBZGFwdGVyXCI7XG5cbmNvbnN0IHtcbiAgVG9waWMsXG4gIFN0YXRlLFxuICBSZXN1bHQsXG4gIFN0YXR1cyxcbiAgTGlzdEN1cnJlbnRKYWNrcG90c1JlcGx5XG59ID0gU2ljYm9Nb2R1bGVBZGFwdGVyLmdldEFsbFJlZnMoKTtcblxuaW1wb3J0IHsgU2ljYm9Tb3VuZCB9IGZyb20gXCIuLi8uLi9TZXR0aW5nL1NpY2JvU2V0dGluZ1wiO1xuaW1wb3J0IFNpY2JvQ29udHJvbGxlciBmcm9tIFwiLi4vLi4vQ29udHJvbGxlcnMvU2ljYm9Db250cm9sbGVyXCI7XG5pbXBvcnQgU2ljYm9IZWxwZXIgZnJvbSBcIi4uLy4uL0hlbHBlcnMvU2ljYm9IZWxwZXJcIjtcbmltcG9ydCBJU2ljYm9TdGF0ZSBmcm9tIFwiLi9JU2ljYm9TdGF0ZVwiO1xuaW1wb3J0IFNpY2JvUG9ydGFsQWRhcHRlciBmcm9tICcuLi8uLi9TaWNib1BvcnRhbEFkYXB0ZXInO1xuaW1wb3J0IFNpY2JvR2FtZVV0aWxzIGZyb20gXCIuLi8uLi9STkdDb21tb25zL1V0aWxzL1NpY2JvR2FtZVV0aWxzXCI7XG5pbXBvcnQgU2ljYm9Ta2VsZXRvblV0aWxzIGZyb20gXCIuLi8uLi9STkdDb21tb25zL1V0aWxzL1NpY2JvU2tlbGV0b25VdGlsc1wiO1xuaW1wb3J0IFNpY2JvTW9uZXlGb3JtYXRDb21wIGZyb20gXCIuLi8uLi9STkdDb21tb25zL1NpY2JvTW9uZXlGb3JtYXRDb21wXCI7XG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9KYWNrcG90U3RhdGVDb21wIGV4dGVuZHMgY2MuQ29tcG9uZW50IGltcGxlbWVudHMgSVNpY2JvU3RhdGUge1xuICAvL0FOQ0hPUiBqYWNrcG90IHN0YW5kXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBqYWNrcG90U3RhbmROb2RlOiBjYy5Ob2RlID0gbnVsbDtcblxuICBAcHJvcGVydHkoc3AuU2tlbGV0b24pXG4gIGphY2tQb3RTdGFuZFNrZWxldG9uOiBzcC5Ta2VsZXRvbiA9IG51bGw7XG5cbiAgQHByb3BlcnR5KFNpY2JvTW9uZXlGb3JtYXRDb21wKVxuICBqYWNrcG90U3RhbmRWYWx1ZUxhYmVsOiBTaWNib01vbmV5Rm9ybWF0Q29tcCA9IG51bGw7XG5cbiAgLy9BTkNIT1IgbXkgamFja3BvdCBhbmltYXRpb25cbiAgQHByb3BlcnR5KHNwLlNrZWxldG9uKVxuICBteUphY2twb3RGcm9udFNrZWxldG9uOiBzcC5Ta2VsZXRvbiA9IG51bGw7XG5cbiAgQHByb3BlcnR5KHNwLlNrZWxldG9uKVxuICBteUphY2twb3RCYWNrU2tlbGV0b246IHNwLlNrZWxldG9uID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuUHJlZmFiKVxuICBwYXJ0aWNsZVByZWZhYjogY2MuUHJlZmFiID0gbnVsbDtcblxuICBAcHJvcGVydHkoU2ljYm9Nb25leUZvcm1hdENvbXApXG4gIGphY2twb3RSYW5kb21MYWJlbDogU2ljYm9Nb25leUZvcm1hdENvbXAgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShTaWNib01vbmV5Rm9ybWF0Q29tcClcbiAgamFja3BvdE51bWJlckxhYmVsOiBTaWNib01vbmV5Rm9ybWF0Q29tcCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICBvdGhlcldpbm5lck5hbWVMYWJlbDogY2MuTGFiZWwgPSBudWxsO1xuICBwcml2YXRlIGphY2twb3RBbmltYXRpb25TdGFydFRpbWUgPSAwO1xuICAvLyBwcml2YXRlIGphY2twb3RBbmltYXRpb25FbmRUaW1lID0gdGhpcy5qYWNrcG90QW5pbWF0aW9uU3RhcnRUaW1lICsgNDtcblxuICBwcml2YXRlIGlzSmFja3BvdFBsYXlpbmcgPSBmYWxzZTtcbiAgcHJpdmF0ZSBqYWNrUG90U3RhbmRQbGF5aW5nQW5pbTogc3RyaW5nO1xuXG4gIHByaXZhdGUgamFja3BvdFNjYWxlU2Z4U3RhcnRUaW1lOiBudW1iZXIgPSB0aGlzLmphY2twb3RBbmltYXRpb25TdGFydFRpbWU7XG4gIHByaXZhdGUgamFja3BvdFNjYWxlU2Z4U3RvcFRpbWU6IG51bWJlciA9IHRoaXMuamFja3BvdFNjYWxlU2Z4U3RhcnRUaW1lICsgMS41O1xuICBwcml2YXRlIGphY2twb3RGaW5hbFNmeFN0YXJ0VGltZTogbnVtYmVyID0gdGhpcy5qYWNrcG90U2NhbGVTZnhTdGFydFRpbWUgKyAxLjU7XG5cbiAgcHJpdmF0ZSBwYXJ0aWNsZURlbGF5VGltZTogbnVtYmVyID0gMS44O1xuXG4gIHByaXZhdGUgcmVzdWx0OiBJbnN0YW5jZVR5cGU8dHlwZW9mIFJlc3VsdD47XG4gIHByaXZhdGUgcGFydGljbGVJbnN0YW5jZTogY2MuTm9kZSA9IG51bGw7XG5cblxuICBwcml2YXRlIGphY2twb3RWYWx1ZTogbnVtYmVyID0gMDtcblxuICBwcml2YXRlIHNldFJlc3VsdChyZXN1bHQ6IEluc3RhbmNlVHlwZTx0eXBlb2YgUmVzdWx0Pikge1xuICAgIGlmIChTaWNib0dhbWVVdGlscy5pc051bGxPclVuZGVmaW5lZChyZXN1bHQpKSByZXR1cm47XG4gICAgdGhpcy5yZXN1bHQgPSByZXN1bHQ7XG4gIH1cblxuICBvbkxvYWQoKSB7XG4gICAgdGhpcy5zZXRNeUphY2twb3RBbmltQWN0aXZlKGZhbHNlKTtcbiAgICAvL3RoaXMuamFja3BvdFN0YW5kTm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIHRoaXMucGxheUphY2twb3RTdGFuZEFuaW1hdGlvbih0cnVlKTtcbiAgICB0aGlzLnNldE90aGVyV2lubmVyQWN0aXZlKGZhbHNlKTtcbiAgfVxuXG4gIGV4aXRTdGF0ZShzdGF0dXMpOiB2b2lkIHtcbiAgICB0aGlzLmlzSmFja3BvdFBsYXlpbmcgPSBmYWxzZTtcblxuICAgIC8vIGlmKHN0YXR1cyAhPSBTdGF0dXMuUkVTVUxUSU5HICYmIHN0YXR1cyAhPSBTdGF0dXMuUEFZSU5HKSB7XG4gICAgaWYgKHN0YXR1cyAhPSBTdGF0dXMuUEFZSU5HKSB7XG4gICAgICB0aGlzLnNldE15SmFja3BvdEFuaW1BY3RpdmUoZmFsc2UpOyAvL05PVEU6IGRvIGFuaW0gamFja3BvdCBob2kgZGFpXG4gICAgICAvL3RoaXMuamFja3BvdFN0YW5kTm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIH1cblxuICAgIHRoaXMuc3RvcFBhcnRpY2xlRlgoKTtcbiAgICB0aGlzLnBsYXlKYWNrcG90U3RhbmRBbmltYXRpb24odHJ1ZSk7XG4gIH1cblxuICB1cGRhdGVTdGF0ZShzdGF0ZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGF0ZT4pOiB2b2lkIHtcbiAgICBpZiAodGhpcy5pc0phY2twb3RQbGF5aW5nKSByZXR1cm47XG4gICAgbGV0IHN0YXR1cyA9IHN0YXRlLmdldFN0YXR1cygpO1xuICAgIGxldCBlbGFwc2VUaW1lID0gc3RhdGUuZ2V0U3RhZ2VUaW1lKCkgLyAxMDAwO1xuXG4gICAgaWYgKHRoaXMuaXNVcGRhdGVKYWNrcG90VmFsdWUoc3RhdHVzKSB8fCB0aGlzLmphY2twb3RTdGFuZFZhbHVlTGFiZWwubW9uZXlWYWx1ZSA9PSAwKSB7XG4gICAgICAgIC8vIHRoaXMudXBkYXRlSmFja3BvdChzdGF0ZS5nZXRQb3QoKSB8fCAwKTsvLyB04bqhbSDhuqluIGtow7RuZyBj4bqtcCBuaOG6rXQgdGhlbyBzdGF0ZVxuICAgICAgICB0aGlzLnVwZGF0ZUphY2twb3QodGhpcy5qYWNrcG90VmFsdWUgfHwgMCk7Ly8gdOG6oW0g4bqpbiBraMO0bmcgY+G6rXAgbmjhuq10IHRoZW8gc3RhdGVcbiAgICB9XG4gICAgXG4gICAgaWYgKFNpY2JvR2FtZVV0aWxzLmlzTnVsbE9yVW5kZWZpbmVkKHRoaXMucmVzdWx0KSkgcmV0dXJuO1xuICAgIGlmICghdGhpcy5yZXN1bHQuaGFzTG90dGVyeVdpbm5lcigpKSByZXR1cm47XG5cbiAgICAvLyBpZihzdGF0dXMgPT0gU3RhdHVzLlJFU1VMVElORyAmJiBlbGFwc2VUaW1lID49IHRoaXMuamFja3BvdEFuaW1hdGlvblN0YXJ0VGltZSl7XG4gICAgaWYgKHN0YXR1cyA9PSBTdGF0dXMuUEFZSU5HICYmIGVsYXBzZVRpbWUgPj0gdGhpcy5qYWNrcG90QW5pbWF0aW9uU3RhcnRUaW1lKSB7XG4gICAgICB0aGlzLmlzSmFja3BvdFBsYXlpbmcgPSB0cnVlO1xuXG4gICAgICB0aGlzLm90aGVyV2lubmVyTmFtZUxhYmVsLnN0cmluZyA9IHRoaXMucmVzdWx0LmdldExvdHRlcnlXaW5uZXIoKS5nZXREaXNwbGF5TmFtZSgpO1xuICAgICAgaWYgKFNpY2JvUG9ydGFsQWRhcHRlci5nZXRJbnN0YW5jZSgpLmdldE15VXNlcklkKCkgPT0gdGhpcy5yZXN1bHQuZ2V0TG90dGVyeVdpbm5lcigpLmdldFVzZXJJZCgpKSB7XG4gICAgICAgIHRoaXMuc2V0TXlKYWNrcG90QW5pbUFjdGl2ZSh0cnVlKTtcbiAgICAgICAgdGhpcy5wbGF5UGFydGljbGVGeChlbGFwc2VUaW1lIC0gdGhpcy5qYWNrcG90QW5pbWF0aW9uU3RhcnRUaW1lKTtcbiAgICAgICAgLy90aGlzLmphY2twb3RTdGFuZE5vZGUuYWN0aXZlID0gZmFsc2U7XG5cbiAgICAgICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLm15SmFja3BvdEZyb250U2tlbGV0b24sIFwiYW5pbWF0aW9uXCIsIGZhbHNlLCBlbGFwc2VUaW1lIC0gdGhpcy5qYWNrcG90QW5pbWF0aW9uU3RhcnRUaW1lKTtcbiAgICAgICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLm15SmFja3BvdEJhY2tTa2VsZXRvbiwgXCJhbmltYXRpb25cIiwgZmFsc2UsIGVsYXBzZVRpbWUgLSB0aGlzLmphY2twb3RBbmltYXRpb25TdGFydFRpbWUpO1xuICAgICAgICB0aGlzLm15SmFja3BvdEJhY2tTa2VsZXRvbi5zZXRDb21wbGV0ZUxpc3RlbmVyKChfKSA9PiB7XG4gICAgICAgICAgdGhpcz8uc2V0TXlKYWNrcG90QW5pbUFjdGl2ZShmYWxzZSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgLy8gY2MudHdlZW4oc2VsZi5ub2RlKVxuICAgICAgICAvLyAgICAgLmRlbGF5KHNlbGYuamFja3BvdEFuaW1hdGlvbkVuZFRpbWUgLSBlbGFwc2VUaW1lKVxuICAgICAgICAvLyAgICAgLmNhbGwoKCk9PnNlbGY/LnNldE15SmFja3BvdEFuaW1BY3RpdmUoZmFsc2UpKVxuICAgICAgICAvLyAgICAgLnN0YXJ0KCk7XG5cbiAgICAgICAgdGhpcy5qYWNrcG90TnVtYmVyTGFiZWwuc2V0TW9uZXkoMCk7XG4gICAgICAgIHRoaXMuamFja3BvdE51bWJlckxhYmVsLnJ1bk1vbmV5VG8odGhpcy5yZXN1bHQuZ2V0TG90dGVyeVdpbm5lcigpLmdldEFtb3VudCgpLCAzKTtcblxuICAgICAgICB0aGlzLnNldE90aGVyV2lubmVyQWN0aXZlKGZhbHNlKTtcblxuICAgICAgICB0aGlzLm15SmFja3BvdFNmeChTaWNib1NvdW5kLlNmeEphY2twb3RTY2FsZSwgZWxhcHNlVGltZSwgdGhpcy5qYWNrcG90U2NhbGVTZnhTdGFydFRpbWUpO1xuICAgICAgICB0aGlzLm15SmFja3BvdFNmeChTaWNib1NvdW5kLlNmeEphY2twb3RTY2FsZSwgZWxhcHNlVGltZSwgdGhpcy5qYWNrcG90U2NhbGVTZnhTdG9wVGltZSwgZmFsc2UpO1xuICAgICAgICB0aGlzLm15SmFja3BvdFNmeChTaWNib1NvdW5kLlNmeEphY2twb3RGaW5hbCwgZWxhcHNlVGltZSwgdGhpcy5qYWNrcG90RmluYWxTZnhTdGFydFRpbWUpO1xuXG4gICAgICAgIC8vdGhpcy5qYWNrcG90U3RhbmROb2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIHRoaXMuc2V0T3RoZXJXaW5uZXJBY3RpdmUodHJ1ZSk7XG4gICAgICAgIHRoaXMucGxheUphY2twb3RTdGFuZEFuaW1hdGlvbihmYWxzZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvL090aGVyIHVzZXIgd2luIGphY2twb3RcbiAgICAgICAgdGhpcy5zZXRNeUphY2twb3RBbmltQWN0aXZlKGZhbHNlKTtcbiAgICAgICAgLy90aGlzLmphY2twb3RTdGFuZE5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5zZXRPdGhlcldpbm5lckFjdGl2ZSh0cnVlKTtcbiAgICAgICAgdGhpcy5wbGF5SmFja3BvdFN0YW5kQW5pbWF0aW9uKGZhbHNlKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBpc1VwZGF0ZUphY2twb3RWYWx1ZShzdGF0dXMpe1xuICAgIHN3aXRjaChzdGF0dXMpe1xuICAgICAgY2FzZSBTdGF0dXMuUEFZSU5HOlxuICAgICAgY2FzZSBTdGF0dXMuRU5EX0JFVDpcbiAgICAgIGNhc2UgU3RhdHVzLlJFU1VMVElORzpcbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICBcbiAgICAgIGRlZmF1bHQ6IHJldHVybiB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIGhhbmRsZVJlc3BvbnNlSmFja3BvdChyZXM6IEluc3RhbmNlVHlwZTx0eXBlb2YgTGlzdEN1cnJlbnRKYWNrcG90c1JlcGx5Pikge1xuICAgIGxldCBnZXRKYWNrcG90c0xpc3QgPSByZXMuZ2V0SmFja3BvdHNMaXN0KCk7XG4gICAgLy8gbGV0IG1hcEphY2twb3QgPSBuZXcgTWFwPFRvcGljLCBMaXN0Q3VycmVudEphY2twb3RzUmVwbHkuSmFja3BvdFtdPigpO1xuICAgIGdldEphY2twb3RzTGlzdC5mb3JFYWNoKChqYWNrcG90KSA9PiB7XG4gICAgICBsZXQgdG9waWMgPSBqYWNrcG90LmdldFRvcGljKCk7XG4gICAgICBpZiAodG9waWMgPT0gVG9waWMuU0lDQk8pIHtcbiAgICAgICAgbGV0IGphY2twb3RzOiBJbnN0YW5jZVR5cGU8dHlwZW9mIExpc3RDdXJyZW50SmFja3BvdHNSZXBseS5KYWNrcG90PltdID0gW2phY2twb3RdO1xuICAgICAgICBsZXQgYW1vdW50SmFja3BvdCA9IGphY2twb3RzWzBdLmdldEFtb3VudCgpO1xuICAgICAgICAvLyB0aGlzLnVwZGF0ZUphY2twb3QoYW1vdW50SmFja3BvdCk7XG4gICAgICAgIHRoaXMuamFja3BvdFZhbHVlID0gYW1vdW50SmFja3BvdDtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlSmFja3BvdCh2YWx1ZTogbnVtYmVyKSB7XG4gICAgLy9jYy5sb2coXCJ1cGRhdGVKYWNrcG90IFwiICsgdmFsdWUpO1xuICAgIHRoaXMuamFja3BvdFN0YW5kVmFsdWVMYWJlbC5ydW5Nb25leVRvKHZhbHVlLCAuMik7XG4gICAgdGhpcy5qYWNrcG90UmFuZG9tTGFiZWwucnVuTW9uZXlUbyh2YWx1ZSwgLjIpO1xuICB9XG5cbiAgbXlKYWNrcG90U2Z4KHNmeE5hbWU6IFNpY2JvU291bmQsIGVsYXBzZVRpbWU6IG51bWJlciA9IDAsIHRpbWU6IG51bWJlciwgaXNQbGF5OiBib29sZWFuID0gdHJ1ZSkge1xuICAgIGxldCBzZWxmID0gdGhpcztcblxuICAgIGlmIChlbGFwc2VUaW1lID4gdGltZSkge1xuICAgICAgaWYgKGlzUGxheSkge1xuICAgICAgICAvL3F1YSB0Z2lhbiB0aGkgc2tpcFxuICAgICAgICBpZiAoU2ljYm9IZWxwZXIuYXBwcm94aW1hdGVseUVxdWFsKGVsYXBzZVRpbWUsIHRpbWUpKSBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UucGxheVNmeChzZnhOYW1lKTtcbiAgICAgIH0gZWxzZSBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2Uuc3RvcFNmeChzZnhOYW1lKTtcbiAgICB9IGVsc2Uge1xuICAgICAgY2MudHdlZW4oc2VsZi5ub2RlKVxuICAgICAgICAuZGVsYXkodGltZSAtIGVsYXBzZVRpbWUpXG4gICAgICAgIC5jYWxsKCgpID0+IHtcbiAgICAgICAgICBpZiAoaXNQbGF5KSBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UucGxheVNmeChzZnhOYW1lKTtcbiAgICAgICAgICBlbHNlIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5zdG9wU2Z4KHNmeE5hbWUpO1xuICAgICAgICB9KVxuICAgICAgICAuc3RhcnQoKTtcbiAgICB9XG4gIH1cblxuICBzdGFydFN0YXRlKHN0YXRlOiBJbnN0YW5jZVR5cGU8dHlwZW9mIFN0YXRlPiwgdG90YWxEdXJhdGlvbjogbnVtYmVyKTogdm9pZCB7XG4gICAgdGhpcy5zZXRSZXN1bHQoc3RhdGUuZ2V0UmVzdWx0KCkpO1xuICAgIGxldCBzdGF0dXMgPSBzdGF0ZS5nZXRTdGF0dXMoKTtcbiAgICAvLyBpZihzdGF0dXMgIT0gU3RhdHVzLlJFU1VMVElORyAmJiBzdGF0dXMgIT0gU3RhdHVzLlBBWUlORylcbiAgICBpZiAoc3RhdHVzICE9IFN0YXR1cy5QQVlJTkcpIHRoaXMuc2V0T3RoZXJXaW5uZXJBY3RpdmUoZmFsc2UpO1xuXG4gICAgLy8gaWYoIHN0YXR1cyA9PSBTdGF0dXMuUEFZSU5HICYmIHRoaXMuamFja3BvdFN0YW5kVmFsdWVMYWJlbC5tb25leVZhbHVlICE9IDApIHJldHVybjtcbiAgICAvLyB0aGlzLmphY2twb3RTdGFuZFZhbHVlTGFiZWwuc2V0TW9uZXkoc3RhdGUuZ2V0UG90KCl8fDApO1xuICAgIC8vIHRoaXMuamFja3BvdFJhbmRvbUxhYmVsLnNldE1vbmV5KHN0YXRlLmdldFBvdCgpfHwwKTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0TXlKYWNrcG90QW5pbUFjdGl2ZShpc0FjdGl2ZTogYm9vbGVhbiA9IHRydWUpIHtcbiAgICB0aGlzLm15SmFja3BvdEZyb250U2tlbGV0b24ubm9kZS5hY3RpdmUgPSBpc0FjdGl2ZTtcbiAgICB0aGlzLm15SmFja3BvdEJhY2tTa2VsZXRvbi5ub2RlLmFjdGl2ZSA9IGlzQWN0aXZlO1xuXG4gICAgaWYgKCFpc0FjdGl2ZSkgdGhpcy5zdG9wUGFydGljbGVGWCgpO1xuICB9XG5cbiAgcHJpdmF0ZSBwbGF5UGFydGljbGVGeChlbGFwc2VUaW1lOiBudW1iZXIpIHtcbiAgICB0aGlzLnN0b3BQYXJ0aWNsZUZYKCk7XG5cbiAgICB0aGlzLnBhcnRpY2xlSW5zdGFuY2UgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLnBhcnRpY2xlUHJlZmFiKTtcbiAgICB0aGlzLnBhcnRpY2xlSW5zdGFuY2Uuc2V0UGFyZW50KHRoaXMubm9kZSk7XG4gICAgdGhpcy5wYXJ0aWNsZUluc3RhbmNlLnNldFBvc2l0aW9uKGNjLlZlYzIuWkVSTyk7XG5cbiAgICBsZXQgcGFydGljbGVzOiBjYy5QYXJ0aWNsZVN5c3RlbTNEW10gPSB0aGlzLnBhcnRpY2xlSW5zdGFuY2UuZ2V0Q29tcG9uZW50c0luQ2hpbGRyZW4oY2MuUGFydGljbGVTeXN0ZW0zRCk7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwYXJ0aWNsZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGxldCBwYXJ0aWNsZSA9IHBhcnRpY2xlc1tpXTtcbiAgICAgIHBhcnRpY2xlLnN0YXJ0RGVsYXkuY29uc3RhbnQgPSBNYXRoLm1heCh0aGlzLnBhcnRpY2xlRGVsYXlUaW1lIC0gZWxhcHNlVGltZSwgMCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzdG9wUGFydGljbGVGWCgpIHtcbiAgICBpZiAodGhpcy5wYXJ0aWNsZUluc3RhbmNlKSB0aGlzLnBhcnRpY2xlSW5zdGFuY2UuZGVzdHJveSgpO1xuICB9XG5cbiAgb25IaWRlKCk6IHZvaWQge1xuICAgIGlmICh0aGlzLm5vZGUgIT0gbnVsbCkgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMubm9kZSk7XG4gICAgdGhpcy5zZXRNeUphY2twb3RBbmltQWN0aXZlKGZhbHNlKTtcbiAgICAvL3RoaXMuamFja3BvdFN0YW5kTm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIHRoaXMuaXNKYWNrcG90UGxheWluZyA9IGZhbHNlO1xuICAgIHRoaXMuamFja1BvdFN0YW5kUGxheWluZ0FuaW0gPSBudWxsO1xuXG4gICAgdGhpcy5zZXRPdGhlcldpbm5lckFjdGl2ZShmYWxzZSk7XG5cbiAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2Uuc3RvcFNmeChTaWNib1NvdW5kLlNmeEphY2twb3RGaW5hbCk7XG4gICAgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLnN0b3BTZngoU2ljYm9Tb3VuZC5TZnhKYWNrcG90U2NhbGUpO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRPdGhlcldpbm5lckFjdGl2ZShhY3RpdmU6IGJvb2xlYW4pIHtcbiAgICB0aGlzLm90aGVyV2lubmVyTmFtZUxhYmVsLm5vZGUucGFyZW50LmFjdGl2ZSA9IGFjdGl2ZTtcbiAgICB0aGlzLmphY2twb3RTdGFuZFZhbHVlTGFiZWwubm9kZS5hY3RpdmUgPSAhYWN0aXZlO1xuICB9XG5cbiAgcHJpdmF0ZSBwbGF5SmFja3BvdFN0YW5kQW5pbWF0aW9uKGlzSWRsZTogYm9vbGVhbikge1xuICAgIGxldCBhbmltID0gXCJub19odVwiO1xuICAgIGlmIChpc0lkbGUpIHtcbiAgICAgIGFuaW0gPSBcImlkbGVcIjtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5qYWNrUG90U3RhbmRQbGF5aW5nQW5pbSA9PSBhbmltKSByZXR1cm47XG4gICAgdGhpcy5qYWNrUG90U3RhbmRQbGF5aW5nQW5pbSA9IGFuaW07XG4gICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLmphY2tQb3RTdGFuZFNrZWxldG9uLCBhbmltLCB0cnVlKTtcbiAgfVxuXG4gIG9uRGVzdHJveSgpIHtcbiAgICBpZiAodGhpcy5ub2RlICE9IG51bGwpIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLm5vZGUpO1xuICB9XG59XG4iXX0=