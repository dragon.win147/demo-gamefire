
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboMenuDetails.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd485akhFDxBa4C0XZwtUD/c', 'SicboMenuDetails');
// Game/Sicbo_New/Scripts/Components/SicboMenuDetails.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), State = _a.State, Status = _a.Status;
var SicboMessageHandler_1 = require("../SicboMessageHandler");
var SicboController_1 = require("../Controllers/SicboController");
var SicboUIManager_1 = require("../Managers/SicboUIManager");
var SicboInforPopup_1 = require("../PopUps/SicboInforPopup");
var SicboSoundManager_1 = require("../Managers/SicboSoundManager");
var SicboUIComp_1 = require("./SicboUIComp");
var SicboText_1 = require("../Setting/SicboText");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboMenuDetails = /** @class */ (function (_super) {
    __extends(SicboMenuDetails, _super);
    function SicboMenuDetails() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //MENU DETAIL
        _this.isMenuDetailShowed = false;
        _this.lockMenuDetail = false;
        _this.menuDetailRoot = null;
        _this.menuDetail = null;
        _this.bgSoundInactiveNode = null;
        _this.effectSoundInactiveNode = null;
        _this.outsideClickNode = null;
        _this.leaveBookingActiveNode = null;
        _this.leaveBookingDeactiveNode = null;
        _this.totalBet = 0;
        _this.curSessionId = 0;
        _this.isBookingLeave = false;
        _this.bookingLeaveSessionId = 0;
        return _this;
    }
    SicboMenuDetails.prototype.start = function () {
        this.loadMenuBackgroundSoundStatus();
        this.loadMenuSoundFxStatus();
    };
    SicboMenuDetails.prototype.onLeave = function () {
        this.onClickShowMenuDetail();
        SicboController_1.default.Instance.sendCheckServerAvailable();
        if (this.isBookingLeave) {
            this.setLeaveBookingStatus(false, this.curSessionId);
            return;
        }
        if (this.totalBet > 0) {
            this.setLeaveBookingStatus(true, this.curSessionId);
            return;
        }
        this.leave();
    };
    SicboMenuDetails.prototype.leave = function () {
        SicboMessageHandler_1.default.getInstance().sendLeaveRequest(function () {
            var _a;
            (_a = SicboController_1.default.Instance) === null || _a === void 0 ? void 0 : _a.leaveGame();
        });
    };
    SicboMenuDetails.prototype.setLeaveBookingStatus = function (isActive, leaveSession) {
        this.bookingLeaveSessionId = leaveSession;
        this.isBookingLeave = isActive;
        this.leaveBookingActiveNode.active = isActive;
        this.leaveBookingDeactiveNode.active = !isActive;
        var msg = isActive ? SicboText_1.default.leaveBookingActiveMsg : SicboText_1.default.leaveBookingDeactiveMsg;
        SicboUIComp_1.default.Instance.showNotifyMessage(msg);
    };
    SicboMenuDetails.prototype.exitState = function (status) {
        // if(this.isBookingLeave && this.totalBet> 0)
        //   this.leave();
    };
    SicboMenuDetails.prototype.startState = function (state, totalDuration) {
        this.curSessionId = state.getTableSessionId();
        if (!this.isBookingLeave)
            return;
        if (this.curSessionId != this.bookingLeaveSessionId) {
            this.leave();
            return;
        }
        if (state.getStatus() == Status.WAITING) {
            this.leave();
        }
    };
    SicboMenuDetails.prototype.updateState = function (state) {
        this.totalBet = state.getSessionPlayerBettedAmount();
    };
    SicboMenuDetails.prototype.onHide = function () { };
    SicboMenuDetails.prototype.onClickButtonRule = function () {
        SicboUIManager_1.default.getInstance().openPopup(SicboInforPopup_1.default, SicboUIManager_1.default.SicboInforPopup);
        this.onClickShowMenuDetail();
    };
    SicboMenuDetails.prototype.onClickShowMenuDetail = function () {
        var _this = this;
        if (this.lockMenuDetail)
            return;
        this.lockMenuDetail = true;
        this.isMenuDetailShowed = !this.isMenuDetailShowed;
        if (this.isMenuDetailShowed == true) {
            //tween out
            this.menuDetailRoot.active = this.isMenuDetailShowed;
            this.outsideClickNode.active = this.isMenuDetailShowed;
            cc.tween(this.menuDetail)
                .to(0.3, { position: new cc.Vec3(0, 0, 0) })
                .call(function () {
                _this.lockMenuDetail = false;
            })
                .start();
        }
        else {
            //tween in
            cc.tween(this.menuDetail)
                .to(0.1, { position: new cc.Vec3(10, 0, 0) })
                .to(0.5, { position: new cc.Vec3(-720, 0, 0) })
                .call(function () {
                _this.menuDetailRoot.active = _this.isMenuDetailShowed;
                _this.outsideClickNode.active = _this.isMenuDetailShowed;
                _this.lockMenuDetail = false;
            })
                .start();
        }
    };
    SicboMenuDetails.prototype.loadMenuBackgroundSoundStatus = function () {
        var vol = SicboSoundManager_1.default.getInstance().getMusicVolumeFromStorage();
        this.bgSoundInactiveNode.active = vol == 0;
    };
    SicboMenuDetails.prototype.loadMenuSoundFxStatus = function () {
        var vol = SicboSoundManager_1.default.getInstance().getSfxVolumeFromStorage();
        this.effectSoundInactiveNode.active = vol == 0;
    };
    SicboMenuDetails.prototype.onClickMenuBackgroundSound = function () {
        var vol = SicboSoundManager_1.default.getInstance().getMusicVolumeFromStorage();
        if (vol > 0) {
            vol = 0;
        }
        else {
            vol = 1;
        }
        SicboSoundManager_1.default.getInstance().setMusicVolume(vol);
        this.bgSoundInactiveNode.active = vol == 0;
    };
    SicboMenuDetails.prototype.onClickMenuSoundFx = function () {
        var vol = SicboSoundManager_1.default.getInstance().getSfxVolumeFromStorage();
        if (vol > 0) {
            vol = 0;
        }
        else {
            vol = 1;
        }
        SicboSoundManager_1.default.getInstance().setSfxVolume(vol);
        this.effectSoundInactiveNode.active = vol == 0;
    };
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "menuDetailRoot", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "menuDetail", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "bgSoundInactiveNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "effectSoundInactiveNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "outsideClickNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "leaveBookingActiveNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboMenuDetails.prototype, "leaveBookingDeactiveNode", void 0);
    SicboMenuDetails = __decorate([
        ccclass
    ], SicboMenuDetails);
    return SicboMenuDetails;
}(cc.Component));
exports.default = SicboMenuDetails;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9NZW51RGV0YWlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVsRixxRUFBZ0U7QUFDMUQsSUFBQSxLQUFvQiw0QkFBa0IsQ0FBQyxVQUFVLEVBQUUsRUFBakQsS0FBSyxXQUFBLEVBQUUsTUFBTSxZQUFvQyxDQUFDO0FBQzFELDhEQUF5RDtBQUN6RCxrRUFBNkQ7QUFDN0QsNkRBQXdEO0FBQ3hELDZEQUF3RDtBQUN4RCxtRUFBOEQ7QUFFOUQsNkNBQXdDO0FBQ3hDLGtEQUE2QztBQUV2QyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUE4QyxvQ0FBWTtJQUExRDtRQUFBLHFFQStKQztRQTlKQyxhQUFhO1FBQ2Isd0JBQWtCLEdBQVksS0FBSyxDQUFDO1FBQ3BDLG9CQUFjLEdBQVksS0FBSyxDQUFDO1FBR2hDLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRS9CLGdCQUFVLEdBQVksSUFBSSxDQUFDO1FBRzNCLHlCQUFtQixHQUFZLElBQUksQ0FBQztRQUdwQyw2QkFBdUIsR0FBWSxJQUFJLENBQUM7UUFHeEMsc0JBQWdCLEdBQVksSUFBSSxDQUFDO1FBR2pDLDRCQUFzQixHQUFZLElBQUksQ0FBQztRQUd2Qyw4QkFBd0IsR0FBWSxJQUFJLENBQUM7UUFFakMsY0FBUSxHQUFXLENBQUMsQ0FBQztRQUNyQixrQkFBWSxHQUFXLENBQUMsQ0FBQztRQUV6QixvQkFBYyxHQUFZLEtBQUssQ0FBQztRQUNoQywyQkFBcUIsR0FBVyxDQUFDLENBQUM7O0lBa0k1QyxDQUFDO0lBaElDLGdDQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsNkJBQTZCLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsa0NBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQzdCLHlCQUFlLENBQUMsUUFBUSxDQUFDLHdCQUF3QixFQUFFLENBQUM7UUFDcEQsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3JELE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLEVBQUU7WUFDckIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDcEQsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2YsQ0FBQztJQUNPLGdDQUFLLEdBQWI7UUFDRSw2QkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQzs7WUFDakQsTUFBQSx5QkFBZSxDQUFDLFFBQVEsMENBQUUsU0FBUyxHQUFHO1FBQ3hDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGdEQUFxQixHQUFyQixVQUFzQixRQUFpQixFQUFFLFlBQW9CO1FBQzNELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUM7UUFFMUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUM7UUFDL0IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUM7UUFDOUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUVqRCxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLG1CQUFTLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLG1CQUFTLENBQUMsdUJBQXVCLENBQUM7UUFDekYscUJBQVcsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELG9DQUFTLEdBQVQsVUFBVSxNQUFXO1FBQ25CLDhDQUE4QztRQUM5QyxrQkFBa0I7SUFDcEIsQ0FBQztJQUNELHFDQUFVLEdBQVYsVUFBVyxLQUFpQyxFQUFFLGFBQXFCO1FBQ2pFLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDOUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjO1lBQUUsT0FBTztRQUVqQyxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO1lBQ25ELElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNiLE9BQU87U0FDUjtRQUVELElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRSxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7WUFDdkMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2Q7SUFDSCxDQUFDO0lBRUQsc0NBQVcsR0FBWCxVQUFZLEtBQWlDO1FBQzNDLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLDRCQUE0QixFQUFFLENBQUM7SUFDdkQsQ0FBQztJQUVELGlDQUFNLEdBQU4sY0FBZ0IsQ0FBQztJQUVqQiw0Q0FBaUIsR0FBakI7UUFDRSx3QkFBYyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyx5QkFBZSxFQUFFLHdCQUFjLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDeEYsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVELGdEQUFxQixHQUFyQjtRQUFBLGlCQTZCQztRQTVCQyxJQUFJLElBQUksQ0FBQyxjQUFjO1lBQUUsT0FBTztRQUVoQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUUzQixJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFFbkQsSUFBSSxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxFQUFFO1lBQ25DLFdBQVc7WUFDWCxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7WUFDckQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7WUFDdkQsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO2lCQUN0QixFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQzNDLElBQUksQ0FBQztnQkFDSixLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztZQUM5QixDQUFDLENBQUM7aUJBQ0QsS0FBSyxFQUFFLENBQUM7U0FDWjthQUFNO1lBQ0wsVUFBVTtZQUNWLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztpQkFDdEIsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDO2lCQUM1QyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDOUMsSUFBSSxDQUFDO2dCQUNKLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQztnQkFDckQsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUM7Z0JBQ3ZELEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1lBQzlCLENBQUMsQ0FBQztpQkFDRCxLQUFLLEVBQUUsQ0FBQztTQUNaO0lBQ0gsQ0FBQztJQUVELHdEQUE2QixHQUE3QjtRQUNFLElBQUksR0FBRyxHQUFHLDJCQUFpQixDQUFDLFdBQVcsRUFBRSxDQUFDLHlCQUF5QixFQUFFLENBQUM7UUFDdEUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRCxnREFBcUIsR0FBckI7UUFDRSxJQUFJLEdBQUcsR0FBRywyQkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO1FBQ3BFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQscURBQTBCLEdBQTFCO1FBQ0UsSUFBSSxHQUFHLEdBQUcsMkJBQWlCLENBQUMsV0FBVyxFQUFFLENBQUMseUJBQXlCLEVBQUUsQ0FBQztRQUN0RSxJQUFJLEdBQUcsR0FBRyxDQUFDLEVBQUU7WUFDWCxHQUFHLEdBQUcsQ0FBQyxDQUFDO1NBQ1Q7YUFBTTtZQUNMLEdBQUcsR0FBRyxDQUFDLENBQUM7U0FDVDtRQUNELDJCQUFpQixDQUFDLFdBQVcsRUFBRSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVELDZDQUFrQixHQUFsQjtRQUNFLElBQUksR0FBRyxHQUFHLDJCQUFpQixDQUFDLFdBQVcsRUFBRSxDQUFDLHVCQUF1QixFQUFFLENBQUM7UUFDcEUsSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFO1lBQ1gsR0FBRyxHQUFHLENBQUMsQ0FBQztTQUNUO2FBQU07WUFDTCxHQUFHLEdBQUcsQ0FBQyxDQUFDO1NBQ1Q7UUFDRCwyQkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUF4SkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0REFDYTtJQUUvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3dEQUNTO0lBRzNCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aUVBQ2tCO0lBR3BDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7cUVBQ3NCO0lBR3hDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OERBQ2U7SUFHakM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztvRUFDcUI7SUFHdkM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztzRUFDdUI7SUF2QnRCLGdCQUFnQjtRQURwQyxPQUFPO09BQ2EsZ0JBQWdCLENBK0pwQztJQUFELHVCQUFDO0NBL0pELEFBK0pDLENBL0o2QyxFQUFFLENBQUMsU0FBUyxHQStKekQ7a0JBL0pvQixnQkFBZ0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFNpY2JvTW9kdWxlQWRhcHRlciBmcm9tIFwiLi4vLi4vLi4vLi4vU2ljYm9Nb2R1bGVBZGFwdGVyXCI7XG5jb25zdCB7IFN0YXRlLCBTdGF0dXMgfSA9IFNpY2JvTW9kdWxlQWRhcHRlci5nZXRBbGxSZWZzKCk7XG5pbXBvcnQgU2ljYm9NZXNzYWdlSGFuZGxlciBmcm9tIFwiLi4vU2ljYm9NZXNzYWdlSGFuZGxlclwiO1xuaW1wb3J0IFNpY2JvQ29udHJvbGxlciBmcm9tIFwiLi4vQ29udHJvbGxlcnMvU2ljYm9Db250cm9sbGVyXCI7XG5pbXBvcnQgU2ljYm9VSU1hbmFnZXIgZnJvbSBcIi4uL01hbmFnZXJzL1NpY2JvVUlNYW5hZ2VyXCI7XG5pbXBvcnQgU2ljYm9JbmZvclBvcHVwIGZyb20gXCIuLi9Qb3BVcHMvU2ljYm9JbmZvclBvcHVwXCI7XG5pbXBvcnQgU2ljYm9Tb3VuZE1hbmFnZXIgZnJvbSBcIi4uL01hbmFnZXJzL1NpY2JvU291bmRNYW5hZ2VyXCI7XG5pbXBvcnQgSVNpY2JvU3RhdGUgZnJvbSBcIi4vU3RhdGVzL0lTaWNib1N0YXRlXCI7XG5pbXBvcnQgU2ljYm9VSUNvbXAgZnJvbSBcIi4vU2ljYm9VSUNvbXBcIjtcbmltcG9ydCBTaWNib1RleHQgZnJvbSBcIi4uL1NldHRpbmcvU2ljYm9UZXh0XCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib01lbnVEZXRhaWxzIGV4dGVuZHMgY2MuQ29tcG9uZW50IGltcGxlbWVudHMgSVNpY2JvU3RhdGUge1xuICAvL01FTlUgREVUQUlMXG4gIGlzTWVudURldGFpbFNob3dlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBsb2NrTWVudURldGFpbDogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBtZW51RGV0YWlsUm9vdDogY2MuTm9kZSA9IG51bGw7XG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBtZW51RGV0YWlsOiBjYy5Ob2RlID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgYmdTb3VuZEluYWN0aXZlTm9kZTogY2MuTm9kZSA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIGVmZmVjdFNvdW5kSW5hY3RpdmVOb2RlOiBjYy5Ob2RlID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgb3V0c2lkZUNsaWNrTm9kZTogY2MuTm9kZSA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIGxlYXZlQm9va2luZ0FjdGl2ZU5vZGU6IGNjLk5vZGUgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBsZWF2ZUJvb2tpbmdEZWFjdGl2ZU5vZGU6IGNjLk5vZGUgPSBudWxsO1xuXG4gIHByaXZhdGUgdG90YWxCZXQ6IG51bWJlciA9IDA7XG4gIHByaXZhdGUgY3VyU2Vzc2lvbklkOiBudW1iZXIgPSAwO1xuXG4gIHByaXZhdGUgaXNCb29raW5nTGVhdmU6IGJvb2xlYW4gPSBmYWxzZTtcbiAgcHJpdmF0ZSBib29raW5nTGVhdmVTZXNzaW9uSWQ6IG51bWJlciA9IDA7XG5cbiAgc3RhcnQoKSB7XG4gICAgdGhpcy5sb2FkTWVudUJhY2tncm91bmRTb3VuZFN0YXR1cygpO1xuICAgIHRoaXMubG9hZE1lbnVTb3VuZEZ4U3RhdHVzKCk7XG4gIH1cblxuICBvbkxlYXZlKCkge1xuICAgIHRoaXMub25DbGlja1Nob3dNZW51RGV0YWlsKCk7XG4gICAgU2ljYm9Db250cm9sbGVyLkluc3RhbmNlLnNlbmRDaGVja1NlcnZlckF2YWlsYWJsZSgpO1xuICAgIGlmICh0aGlzLmlzQm9va2luZ0xlYXZlKSB7XG4gICAgICB0aGlzLnNldExlYXZlQm9va2luZ1N0YXR1cyhmYWxzZSwgdGhpcy5jdXJTZXNzaW9uSWQpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICh0aGlzLnRvdGFsQmV0ID4gMCkge1xuICAgICAgdGhpcy5zZXRMZWF2ZUJvb2tpbmdTdGF0dXModHJ1ZSwgdGhpcy5jdXJTZXNzaW9uSWQpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMubGVhdmUoKTtcbiAgfVxuICBwcml2YXRlIGxlYXZlKCkge1xuICAgIFNpY2JvTWVzc2FnZUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5zZW5kTGVhdmVSZXF1ZXN0KCgpID0+IHtcbiAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZT8ubGVhdmVHYW1lKCk7XG4gICAgfSk7XG4gIH1cblxuICBzZXRMZWF2ZUJvb2tpbmdTdGF0dXMoaXNBY3RpdmU6IGJvb2xlYW4sIGxlYXZlU2Vzc2lvbjogbnVtYmVyKSB7XG4gICAgdGhpcy5ib29raW5nTGVhdmVTZXNzaW9uSWQgPSBsZWF2ZVNlc3Npb247XG5cbiAgICB0aGlzLmlzQm9va2luZ0xlYXZlID0gaXNBY3RpdmU7XG4gICAgdGhpcy5sZWF2ZUJvb2tpbmdBY3RpdmVOb2RlLmFjdGl2ZSA9IGlzQWN0aXZlO1xuICAgIHRoaXMubGVhdmVCb29raW5nRGVhY3RpdmVOb2RlLmFjdGl2ZSA9ICFpc0FjdGl2ZTtcblxuICAgIGxldCBtc2cgPSBpc0FjdGl2ZSA/IFNpY2JvVGV4dC5sZWF2ZUJvb2tpbmdBY3RpdmVNc2cgOiBTaWNib1RleHQubGVhdmVCb29raW5nRGVhY3RpdmVNc2c7XG4gICAgU2ljYm9VSUNvbXAuSW5zdGFuY2Uuc2hvd05vdGlmeU1lc3NhZ2UobXNnKTtcbiAgfVxuXG4gIGV4aXRTdGF0ZShzdGF0dXM6IGFueSk6IHZvaWQge1xuICAgIC8vIGlmKHRoaXMuaXNCb29raW5nTGVhdmUgJiYgdGhpcy50b3RhbEJldD4gMClcbiAgICAvLyAgIHRoaXMubGVhdmUoKTtcbiAgfVxuICBzdGFydFN0YXRlKHN0YXRlOiBJbnN0YW5jZVR5cGU8dHlwZW9mIFN0YXRlPiwgdG90YWxEdXJhdGlvbjogbnVtYmVyKTogdm9pZCB7XG4gICAgdGhpcy5jdXJTZXNzaW9uSWQgPSBzdGF0ZS5nZXRUYWJsZVNlc3Npb25JZCgpO1xuICAgIGlmICghdGhpcy5pc0Jvb2tpbmdMZWF2ZSkgcmV0dXJuO1xuXG4gICAgaWYgKHRoaXMuY3VyU2Vzc2lvbklkICE9IHRoaXMuYm9va2luZ0xlYXZlU2Vzc2lvbklkKSB7XG4gICAgICB0aGlzLmxlYXZlKCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKHN0YXRlLmdldFN0YXR1cygpID09IFN0YXR1cy5XQUlUSU5HKSB7XG4gICAgICB0aGlzLmxlYXZlKCk7XG4gICAgfVxuICB9XG5cbiAgdXBkYXRlU3RhdGUoc3RhdGU6IEluc3RhbmNlVHlwZTx0eXBlb2YgU3RhdGU+KTogdm9pZCB7XG4gICAgdGhpcy50b3RhbEJldCA9IHN0YXRlLmdldFNlc3Npb25QbGF5ZXJCZXR0ZWRBbW91bnQoKTtcbiAgfVxuXG4gIG9uSGlkZSgpOiB2b2lkIHt9XG5cbiAgb25DbGlja0J1dHRvblJ1bGUoKSB7XG4gICAgU2ljYm9VSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5vcGVuUG9wdXAoU2ljYm9JbmZvclBvcHVwLCBTaWNib1VJTWFuYWdlci5TaWNib0luZm9yUG9wdXApO1xuICAgIHRoaXMub25DbGlja1Nob3dNZW51RGV0YWlsKCk7XG4gIH1cblxuICBvbkNsaWNrU2hvd01lbnVEZXRhaWwoKSB7XG4gICAgaWYgKHRoaXMubG9ja01lbnVEZXRhaWwpIHJldHVybjtcblxuICAgIHRoaXMubG9ja01lbnVEZXRhaWwgPSB0cnVlO1xuXG4gICAgdGhpcy5pc01lbnVEZXRhaWxTaG93ZWQgPSAhdGhpcy5pc01lbnVEZXRhaWxTaG93ZWQ7XG5cbiAgICBpZiAodGhpcy5pc01lbnVEZXRhaWxTaG93ZWQgPT0gdHJ1ZSkge1xuICAgICAgLy90d2VlbiBvdXRcbiAgICAgIHRoaXMubWVudURldGFpbFJvb3QuYWN0aXZlID0gdGhpcy5pc01lbnVEZXRhaWxTaG93ZWQ7XG4gICAgICB0aGlzLm91dHNpZGVDbGlja05vZGUuYWN0aXZlID0gdGhpcy5pc01lbnVEZXRhaWxTaG93ZWQ7XG4gICAgICBjYy50d2Vlbih0aGlzLm1lbnVEZXRhaWwpXG4gICAgICAgIC50bygwLjMsIHsgcG9zaXRpb246IG5ldyBjYy5WZWMzKDAsIDAsIDApIH0pXG4gICAgICAgIC5jYWxsKCgpID0+IHtcbiAgICAgICAgICB0aGlzLmxvY2tNZW51RGV0YWlsID0gZmFsc2U7XG4gICAgICAgIH0pXG4gICAgICAgIC5zdGFydCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICAvL3R3ZWVuIGluXG4gICAgICBjYy50d2Vlbih0aGlzLm1lbnVEZXRhaWwpXG4gICAgICAgIC50bygwLjEsIHsgcG9zaXRpb246IG5ldyBjYy5WZWMzKDEwLCAwLCAwKSB9KVxuICAgICAgICAudG8oMC41LCB7IHBvc2l0aW9uOiBuZXcgY2MuVmVjMygtNzIwLCAwLCAwKSB9KVxuICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgdGhpcy5tZW51RGV0YWlsUm9vdC5hY3RpdmUgPSB0aGlzLmlzTWVudURldGFpbFNob3dlZDtcbiAgICAgICAgICB0aGlzLm91dHNpZGVDbGlja05vZGUuYWN0aXZlID0gdGhpcy5pc01lbnVEZXRhaWxTaG93ZWQ7XG4gICAgICAgICAgdGhpcy5sb2NrTWVudURldGFpbCA9IGZhbHNlO1xuICAgICAgICB9KVxuICAgICAgICAuc3RhcnQoKTtcbiAgICB9XG4gIH1cblxuICBsb2FkTWVudUJhY2tncm91bmRTb3VuZFN0YXR1cygpIHtcbiAgICBsZXQgdm9sID0gU2ljYm9Tb3VuZE1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5nZXRNdXNpY1ZvbHVtZUZyb21TdG9yYWdlKCk7XG4gICAgdGhpcy5iZ1NvdW5kSW5hY3RpdmVOb2RlLmFjdGl2ZSA9IHZvbCA9PSAwO1xuICB9XG5cbiAgbG9hZE1lbnVTb3VuZEZ4U3RhdHVzKCkge1xuICAgIGxldCB2b2wgPSBTaWNib1NvdW5kTWFuYWdlci5nZXRJbnN0YW5jZSgpLmdldFNmeFZvbHVtZUZyb21TdG9yYWdlKCk7XG4gICAgdGhpcy5lZmZlY3RTb3VuZEluYWN0aXZlTm9kZS5hY3RpdmUgPSB2b2wgPT0gMDtcbiAgfVxuXG4gIG9uQ2xpY2tNZW51QmFja2dyb3VuZFNvdW5kKCkge1xuICAgIGxldCB2b2wgPSBTaWNib1NvdW5kTWFuYWdlci5nZXRJbnN0YW5jZSgpLmdldE11c2ljVm9sdW1lRnJvbVN0b3JhZ2UoKTtcbiAgICBpZiAodm9sID4gMCkge1xuICAgICAgdm9sID0gMDtcbiAgICB9IGVsc2Uge1xuICAgICAgdm9sID0gMTtcbiAgICB9XG4gICAgU2ljYm9Tb3VuZE1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5zZXRNdXNpY1ZvbHVtZSh2b2wpO1xuICAgIHRoaXMuYmdTb3VuZEluYWN0aXZlTm9kZS5hY3RpdmUgPSB2b2wgPT0gMDtcbiAgfVxuXG4gIG9uQ2xpY2tNZW51U291bmRGeCgpIHtcbiAgICBsZXQgdm9sID0gU2ljYm9Tb3VuZE1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5nZXRTZnhWb2x1bWVGcm9tU3RvcmFnZSgpO1xuICAgIGlmICh2b2wgPiAwKSB7XG4gICAgICB2b2wgPSAwO1xuICAgIH0gZWxzZSB7XG4gICAgICB2b2wgPSAxO1xuICAgIH1cbiAgICBTaWNib1NvdW5kTWFuYWdlci5nZXRJbnN0YW5jZSgpLnNldFNmeFZvbHVtZSh2b2wpO1xuICAgIHRoaXMuZWZmZWN0U291bmRJbmFjdGl2ZU5vZGUuYWN0aXZlID0gdm9sID09IDA7XG4gIH1cbn1cbiJdfQ==