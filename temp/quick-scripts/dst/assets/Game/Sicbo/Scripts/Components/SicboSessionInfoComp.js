
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboSessionInfoComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0c13aRWN9ZJ6bYY0gIRCgM2', 'SicboSessionInfoComp');
// Game/Sicbo_New/Scripts/Components/SicboSessionInfoComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), State = _a.State, Status = _a.Status;
var SicboMoneyFormatComp_1 = require("../RNGCommons/SicboMoneyFormatComp");
var SicboGameUtils_1 = require("../RNGCommons/Utils/SicboGameUtils");
var SicboText_1 = require("../Setting/SicboText");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboSessionInfoComp = /** @class */ (function (_super) {
    __extends(SicboSessionInfoComp, _super);
    function SicboSessionInfoComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sessionIdLabel = null;
        _this.totalBetLabel = null;
        _this.diceLabel = null;
        _this.md5Desc = null;
        _this.keyDesc = null;
        _this.md5Label = null;
        _this.copyNotiNode = null;
        _this.md5Mask = null;
        _this.md5ParentNode = null;
        _this.layout = null;
        _this.shinnyAnimation = null;
        _this.md5Color = cc.Color.WHITE;
        _this.keyColor = cc.Color.RED;
        _this.md5Cache = null;
        // private MD5_LENGTH = 32;
        _this.MD5_MASK_WIDTH = 310;
        _this.KEY_WIDTH = 275;
        _this.MD5_WIDTH = 295;
        // private MD5_MASK_LEFT_POS = -26.5;
        // private MD5_MASK_RIGHT_POS = this.MD5_MASK_LEFT_POS + this.MD5_MASK_WIDTH;
        _this.md5NewStartTime = .7;
        _this.keyNewStartTime = .6;
        _this.MD5_STR = "MD5 :";
        _this.KEY_STR = "KEY :";
        return _this;
    }
    SicboSessionInfoComp.prototype.onLoad = function () {
        this.shinnyAnimation.node.active = false;
        this.MD5_MASK_WIDTH = this.md5Mask.width;
    };
    SicboSessionInfoComp.prototype.setMaskAnchor = function (isLeft) {
        if (isLeft === void 0) { isLeft = true; }
        // this.md5Mask.y = 0;
        if (isLeft) {
            this.md5Mask.anchorX = 0;
            // this.md5Mask.x = this.MD5_MASK_LEFT_POS;
            this.md5ParentNode.setPosition(cc.Vec2.ZERO);
        }
        else {
            this.md5Mask.anchorX = 1;
            // this.md5Mask.x = this.MD5_MASK_RIGHT_POS;
            this.md5ParentNode.setPosition(cc.Vec2.ZERO);
            this.md5ParentNode.x = -this.MD5_MASK_WIDTH;
        }
    };
    SicboSessionInfoComp.prototype.exitState = function (status) {
    };
    SicboSessionInfoComp.prototype.startState = function (state, totalDuration) {
        this.setSessionId(state.getTableSessionId().toString());
        var status = state.getStatus();
        switch (status) {
            // case Status.WAITING:
            //   this.setNewMd5(state.getPrevMd5Result(), state.getMd5Result(), elapseTime);
            //   break;
            case Status.BETTING:
            case Status.END_BET:
                this.md5Cache = state.getMd5Result();
                this.setMd5Style();
                this.updateMd5Label(this.md5Cache);
                break;
            // case Status.RESULTING:
            //   this.setNewKey(state.getMd5Result(), state.getKeyResult(), elapseTime);
            //   break;
            case Status.PAYING:
            case Status.FINISHING:
                this.md5Cache = state.getKeyResult();
                this.updateMd5Label(this.md5Cache);
                this.setKeyStyle();
                break;
        }
    };
    SicboSessionInfoComp.prototype.updateState = function (state) {
        this.setTotalBet(state.getSessionPlayerBettedAmount());
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        switch (status) {
            case Status.WAITING:
                this.setNewMd5(state.getPrevKeyResult(), state.getMd5Result(), elapseTime);
                break;
            case Status.RESULTING:
                this.setNewKey(state.getMd5Result(), state.getKeyResult(), elapseTime);
                break;
        }
    };
    SicboSessionInfoComp.prototype.onHide = function () {
        this.md5Label.node.width = 0;
        // cc.Tween.stopAllByTarget(this.copyNotiNode);
        // cc.Tween.stopAllByTarget(this.md5Mask);
        // cc.Tween.stopAllByTarget(this.layout);    
        cc.Tween.stopAllByTarget(this.node);
    };
    SicboSessionInfoComp.prototype.setSessionId = function (sessionId) {
        this.sessionIdLabel.string = SicboGameUtils_1.default.FormatString(SicboText_1.default.sessionIdText, sessionId);
    };
    SicboSessionInfoComp.prototype.setTotalBet = function (totalBet) {
        this.totalBetLabel.setMoney(totalBet);
    };
    SicboSessionInfoComp.prototype.setNewMd5 = function (md5PrevRaw, md5NewRaw, elapseTime) {
        if (elapseTime > this.md5NewStartTime) {
            this.md5Cache = md5NewRaw;
            this.updateMd5Label(this.md5Cache);
            this.setMd5Style();
        }
        else if (elapseTime < this.md5NewStartTime) {
            this.md5Cache = md5PrevRaw;
            this.setKeyStyle();
            this.updateMd5Label(this.md5Cache);
        }
        else {
            var self_1 = this;
            cc.tween(self_1.node)
                // .delay(self.md5NewStartTime - elapseTime)
                .call(function () {
                self_1.md5Cache = md5NewRaw;
                self_1.setMd5Style();
                self_1.setMaskAnchor(true);
                self_1.playLeftToRight();
                self_1.updateMd5Label(self_1.md5Cache);
            }).start();
        }
    };
    SicboSessionInfoComp.prototype.setNewKey = function (md5NewRaw, keyNewRaw, elapseTime) {
        if (elapseTime > this.keyNewStartTime) {
            this.md5Cache = keyNewRaw;
            this.updateMd5Label(this.md5Cache);
            this.setKeyStyle();
        }
        else if (elapseTime < this.keyNewStartTime) {
            this.md5Cache = md5NewRaw;
            this.updateMd5Label(this.md5Cache);
            this.setMd5Style();
        }
        else {
            var self_2 = this;
            cc.tween(self_2.node)
                // .delay(self.keyNewStartTime - elapseTime)
                .call(function () {
                self_2.md5Cache = keyNewRaw;
                self_2.updateMd5Label(self_2.md5Cache);
                self_2.setKeyStyle();
                self_2.setMaskAnchor(false);
                self_2.playRightToLeft();
            }).start();
        }
    };
    SicboSessionInfoComp.prototype.setKeyStyle = function () {
        this.md5Desc.active = false;
        this.keyDesc.active = true;
        this.md5Label.node.color = this.keyColor;
        this.md5Label.node.width = this.KEY_WIDTH;
        this.diceLabel.node.active = true;
    };
    SicboSessionInfoComp.prototype.setMd5Style = function () {
        this.md5Desc.active = true;
        this.keyDesc.active = false;
        this.md5Label.node.color = this.md5Color;
        this.md5Label.node.width = this.MD5_WIDTH;
        this.diceLabel.node.active = false;
    };
    SicboSessionInfoComp.prototype.updateMd5Label = function (rawStr) {
        var _a = this.parseMd5(rawStr), diceStr = _a[0], md5Str = _a[1];
        this.md5Label.string = md5Str;
        this.diceLabel.string = diceStr;
    };
    SicboSessionInfoComp.prototype.playRightToLeft = function () {
        // this.setMaskAnchor(false);
        this.playMd5Anim(true);
        this.shinnyAnimation.node.active = true;
        this.shinnyAnimation.play("rightToLeft");
    };
    SicboSessionInfoComp.prototype.playLeftToRight = function () {
        // this.setMaskAnchor(true);
        this.playMd5Anim();
        this.shinnyAnimation.node.active = true;
        this.shinnyAnimation.play("leftToRight");
    };
    SicboSessionInfoComp.prototype.playMd5Anim = function (animLayout, animTime) {
        if (animLayout === void 0) { animLayout = false; }
        if (animTime === void 0) { animTime = 1.15; }
        this.md5Mask.width = 5;
        cc.tween(this.md5Mask)
            .to(animTime, { width: this.MD5_MASK_WIDTH })
            .start();
        if (!animLayout) {
            this.layout.spacingX = 0;
            return;
        }
        this.layout.spacingX = this.MD5_MASK_WIDTH - 5;
        cc.tween(this.layout)
            .to(animTime, { spacingX: 0 })
            .start();
    };
    /**
     *
     * @param md5Raw
     * @returns [diceStr, md5Str]
     */
    SicboSessionInfoComp.prototype.parseMd5 = function (md5Raw) {
        // md5Raw = md5Raw.replace(":", "");
        var md5Str = "";
        var diceStr = "";
        var temp = md5Raw.indexOf("]");
        if (temp < 0) {
            md5Str = md5Raw;
            diceStr = "\n";
        }
        else {
            md5Str = md5Raw.substr(temp + 1);
            diceStr = md5Raw.substr(0, temp + 1);
        }
        return [diceStr, md5Str];
    };
    SicboSessionInfoComp.prototype.onClickCopy = function () {
        SicboGameUtils_1.default.copyToClipboard(this.md5Cache);
        this.playCopyNoti();
    };
    SicboSessionInfoComp.prototype.playCopyNoti = function () {
        cc.Tween.stopAllByTarget(this.copyNotiNode);
        cc.tween(this.copyNotiNode)
            .to(.2, { scale: 1, opacity: 255 })
            .delay(1)
            .to(.2, { scale: 0, opacity: 0 })
            .start();
    };
    SicboSessionInfoComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.copyNotiNode);
        cc.Tween.stopAllByTarget(this.md5Mask);
        cc.Tween.stopAllByTarget(this.layout);
        cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.Label)
    ], SicboSessionInfoComp.prototype, "sessionIdLabel", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboSessionInfoComp.prototype, "totalBetLabel", void 0);
    __decorate([
        property(cc.Label)
    ], SicboSessionInfoComp.prototype, "diceLabel", void 0);
    __decorate([
        property(cc.Node)
    ], SicboSessionInfoComp.prototype, "md5Desc", void 0);
    __decorate([
        property(cc.Node)
    ], SicboSessionInfoComp.prototype, "keyDesc", void 0);
    __decorate([
        property(cc.Label)
    ], SicboSessionInfoComp.prototype, "md5Label", void 0);
    __decorate([
        property(cc.Node)
    ], SicboSessionInfoComp.prototype, "copyNotiNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboSessionInfoComp.prototype, "md5Mask", void 0);
    __decorate([
        property(cc.Node)
    ], SicboSessionInfoComp.prototype, "md5ParentNode", void 0);
    __decorate([
        property(cc.Layout)
    ], SicboSessionInfoComp.prototype, "layout", void 0);
    __decorate([
        property(cc.Animation)
    ], SicboSessionInfoComp.prototype, "shinnyAnimation", void 0);
    __decorate([
        property(cc.Color)
    ], SicboSessionInfoComp.prototype, "md5Color", void 0);
    __decorate([
        property(cc.Color)
    ], SicboSessionInfoComp.prototype, "keyColor", void 0);
    SicboSessionInfoComp = __decorate([
        ccclass
    ], SicboSessionInfoComp);
    return SicboSessionInfoComp;
}(cc.Component));
exports.default = SicboSessionInfoComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9TZXNzaW9uSW5mb0NvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7QUFDbEYscUVBQWdFO0FBRTFELElBQUEsS0FFTSw0QkFBa0IsQ0FBQyxVQUFVLEVBQUUsRUFEekMsS0FBSyxXQUFBLEVBQ0wsTUFBTSxZQUFtQyxDQUFDO0FBRzVDLDJFQUFzRTtBQUN0RSxxRUFBZ0U7QUFFaEUsa0RBQTZDO0FBRXZDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQWtELHdDQUFZO0lBQTlEO1FBQUEscUVBNlJDO1FBMVJDLG9CQUFjLEdBQWEsSUFBSSxDQUFDO1FBR2hDLG1CQUFhLEdBQXlCLElBQUksQ0FBQztRQUczQyxlQUFTLEdBQWEsSUFBSSxDQUFDO1FBRzNCLGFBQU8sR0FBWSxJQUFJLENBQUM7UUFHeEIsYUFBTyxHQUFZLElBQUksQ0FBQztRQUd4QixjQUFRLEdBQWEsSUFBSSxDQUFDO1FBRzFCLGtCQUFZLEdBQVksSUFBSSxDQUFDO1FBRzdCLGFBQU8sR0FBWSxJQUFJLENBQUM7UUFHeEIsbUJBQWEsR0FBWSxJQUFJLENBQUM7UUFHOUIsWUFBTSxHQUFjLElBQUksQ0FBQztRQUd6QixxQkFBZSxHQUFpQixJQUFJLENBQUM7UUFHckMsY0FBUSxHQUFhLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBR3BDLGNBQVEsR0FBYSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUVsQyxjQUFRLEdBQVcsSUFBSSxDQUFDO1FBQ3hCLDJCQUEyQjtRQUNuQixvQkFBYyxHQUFHLEdBQUcsQ0FBQztRQUNyQixlQUFTLEdBQUcsR0FBRyxDQUFDO1FBQ2hCLGVBQVMsR0FBRyxHQUFHLENBQUM7UUFDeEIscUNBQXFDO1FBQ3JDLDZFQUE2RTtRQUNyRSxxQkFBZSxHQUFHLEVBQUUsQ0FBQztRQUNyQixxQkFBZSxHQUFHLEVBQUUsQ0FBQztRQUU3QixhQUFPLEdBQUcsT0FBTyxDQUFBO1FBQ2pCLGFBQU8sR0FBRyxPQUFPLENBQUE7O0lBeU9uQixDQUFDO0lBdk9DLHFDQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7SUFDM0MsQ0FBQztJQUVELDRDQUFhLEdBQWIsVUFBYyxNQUFzQjtRQUF0Qix1QkFBQSxFQUFBLGFBQXNCO1FBQ2xDLHNCQUFzQjtRQUN0QixJQUFHLE1BQU0sRUFBQztZQUNSLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztZQUN6QiwyQ0FBMkM7WUFDM0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5QzthQUFJO1lBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1lBQ3pCLDRDQUE0QztZQUM1QyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztTQUM3QztJQUNILENBQUM7SUFHRCx3Q0FBUyxHQUFULFVBQVUsTUFBTTtJQUNoQixDQUFDO0lBRUQseUNBQVUsR0FBVixVQUFXLEtBQWlDLEVBQUUsYUFBcUI7UUFDakUsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3hELElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUUvQixRQUFRLE1BQU0sRUFBRTtZQUNkLHVCQUF1QjtZQUN2QixnRkFBZ0Y7WUFDaEYsV0FBVztZQUVYLEtBQUssTUFBTSxDQUFDLE9BQU8sQ0FBQztZQUFDLEtBQUssTUFBTSxDQUFDLE9BQU87Z0JBQ3RDLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUNyQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNuQyxNQUFNO1lBRVIseUJBQXlCO1lBQ3pCLDRFQUE0RTtZQUM1RSxXQUFXO1lBRVgsS0FBSyxNQUFNLENBQUMsTUFBTSxDQUFDO1lBQUMsS0FBSyxNQUFNLENBQUMsU0FBUztnQkFDdkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3JDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNuQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ25CLE1BQU07U0FDVDtJQUNILENBQUM7SUFFRCwwQ0FBVyxHQUFYLFVBQVksS0FBaUM7UUFDM0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsNEJBQTRCLEVBQUUsQ0FBQyxDQUFDO1FBRXZELElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUMvQixJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLEdBQUMsSUFBSSxDQUFDO1FBRTNDLFFBQVEsTUFBTSxFQUFFO1lBQ2QsS0FBSyxNQUFNLENBQUMsT0FBTztnQkFDakIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxLQUFLLENBQUMsWUFBWSxFQUFFLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQzNFLE1BQU07WUFFUixLQUFLLE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsRUFBRSxLQUFLLENBQUMsWUFBWSxFQUFFLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQ3ZFLE1BQU07U0FDVDtJQUNILENBQUM7SUFFRCxxQ0FBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUM3QiwrQ0FBK0M7UUFDL0MsMENBQTBDO1FBQzFDLDZDQUE2QztRQUM3QyxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELDJDQUFZLEdBQVosVUFBYSxTQUFpQjtRQUM1QixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyx3QkFBYyxDQUFDLFlBQVksQ0FBQyxtQkFBUyxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUMvRixDQUFDO0lBQ0QsMENBQVcsR0FBWCxVQUFZLFFBQWdCO1FBQzFCLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFHRCx3Q0FBUyxHQUFULFVBQVUsVUFBa0IsRUFBRSxTQUFpQixFQUFFLFVBQWtCO1FBQ2pFLElBQUcsVUFBVSxHQUFDLElBQUksQ0FBQyxlQUFlLEVBQUM7WUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7WUFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BCO2FBQUssSUFBRyxVQUFVLEdBQUcsSUFBSSxDQUFDLGVBQWUsRUFBQztZQUN6QyxJQUFJLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztZQUMzQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDcEM7YUFBSTtZQUNILElBQUksTUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixFQUFFLENBQUMsS0FBSyxDQUFDLE1BQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ2pCLDRDQUE0QztpQkFDM0MsSUFBSSxDQUNIO2dCQUNFLE1BQUksQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO2dCQUMxQixNQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ25CLE1BQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3pCLE1BQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDdkIsTUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDckMsQ0FBQyxDQUNGLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCx3Q0FBUyxHQUFULFVBQVUsU0FBaUIsRUFBRSxTQUFpQixFQUFFLFVBQWtCO1FBQ2hFLElBQUcsVUFBVSxHQUFDLElBQUksQ0FBQyxlQUFlLEVBQUM7WUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7WUFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BCO2FBQUssSUFBRyxVQUFVLEdBQUcsSUFBSSxDQUFDLGVBQWUsRUFBQztZQUN6QyxJQUFJLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQztZQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNuQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEI7YUFBSTtZQUNILElBQUksTUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixFQUFFLENBQUMsS0FBSyxDQUFDLE1BQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ2pCLDRDQUE0QztpQkFDM0MsSUFBSSxDQUNIO2dCQUNFLE1BQUksQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO2dCQUMxQixNQUFJLENBQUMsY0FBYyxDQUFDLE1BQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbkMsTUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUNuQixNQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUMxQixNQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDekIsQ0FBQyxDQUNGLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDYjtJQUNILENBQUM7SUFHTywwQ0FBVyxHQUFuQjtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDekMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDMUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNwQyxDQUFDO0lBRU8sMENBQVcsR0FBbkI7UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFFckMsQ0FBQztJQUVPLDZDQUFjLEdBQXRCLFVBQXVCLE1BQWM7UUFDL0IsSUFBQSxLQUFvQixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUF4QyxPQUFPLFFBQUEsRUFBRSxNQUFNLFFBQXlCLENBQUM7UUFDOUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQzlCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztJQUNsQyxDQUFDO0lBRU8sOENBQWUsR0FBdkI7UUFDRSw2QkFBNkI7UUFDN0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTyw4Q0FBZSxHQUF2QjtRQUNFLDRCQUE0QjtRQUM1QixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBR0QsMENBQVcsR0FBWCxVQUFZLFVBQTJCLEVBQUUsUUFBdUI7UUFBcEQsMkJBQUEsRUFBQSxrQkFBMkI7UUFBRSx5QkFBQSxFQUFBLGVBQXVCO1FBQzlELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUN2QixFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7YUFDbkIsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFDLENBQUM7YUFDMUMsS0FBSyxFQUFFLENBQUM7UUFHWCxJQUFHLENBQUMsVUFBVSxFQUFFO1lBQ2QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1lBQ3pCLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzthQUNwQixFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUMsUUFBUSxFQUFFLENBQUMsRUFBQyxDQUFDO2FBQzNCLEtBQUssRUFBRSxDQUFDO0lBQ1gsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCx1Q0FBUSxHQUFSLFVBQVMsTUFBYztRQUNyQixvQ0FBb0M7UUFDcEMsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUVqQixJQUFJLElBQUksR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLElBQUcsSUFBSSxHQUFHLENBQUMsRUFBQztZQUNWLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFDaEIsT0FBTyxHQUFHLElBQUksQ0FBQztTQUNoQjthQUFJO1lBQ0gsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDdEM7UUFDRCxPQUFPLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCwwQ0FBVyxHQUFYO1FBQ0Usd0JBQWMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsMkNBQVksR0FBWjtRQUNFLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM1QyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7YUFDeEIsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBQyxDQUFDO2FBQ2hDLEtBQUssQ0FBQyxDQUFDLENBQUM7YUFDUixFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUMsS0FBSyxFQUFDLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUM7YUFDN0IsS0FBSyxFQUFFLENBQUM7SUFDYixDQUFDO0lBRUQsd0NBQVMsR0FBVDtRQUNFLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM1QyxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkMsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3RDLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUV0QyxDQUFDO0lBelJEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7Z0VBQ2E7SUFHaEM7UUFEQyxRQUFRLENBQUMsOEJBQW9CLENBQUM7K0RBQ1k7SUFHM0M7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzsyREFDUTtJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3lEQUNNO0lBR3hCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7eURBQ007SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzswREFDTztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzhEQUNXO0lBRzdCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7eURBQ007SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsrREFDWTtJQUc5QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3dEQUNLO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7aUVBQ2M7SUFHckM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzswREFDaUI7SUFHcEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzswREFDZTtJQXZDZixvQkFBb0I7UUFEeEMsT0FBTztPQUNhLG9CQUFvQixDQTZSeEM7SUFBRCwyQkFBQztDQTdSRCxBQTZSQyxDQTdSaUQsRUFBRSxDQUFDLFNBQVMsR0E2UjdEO2tCQTdSb0Isb0JBQW9CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5pbXBvcnQgU2ljYm9Nb2R1bGVBZGFwdGVyIGZyb20gXCIuLi8uLi8uLi8uLi9TaWNib01vZHVsZUFkYXB0ZXJcIjtcblxuY29uc3Qge1xuICBTdGF0ZSxcbiAgU3RhdHVzfSA9IFNpY2JvTW9kdWxlQWRhcHRlci5nZXRBbGxSZWZzKCk7XG5cbmltcG9ydCBTaWNib1NldHRpbmcgZnJvbSBcIi4uL1NldHRpbmcvU2ljYm9TZXR0aW5nXCI7XG5pbXBvcnQgU2ljYm9Nb25leUZvcm1hdENvbXAgZnJvbSBcIi4uL1JOR0NvbW1vbnMvU2ljYm9Nb25leUZvcm1hdENvbXBcIjtcbmltcG9ydCBTaWNib0dhbWVVdGlscyBmcm9tIFwiLi4vUk5HQ29tbW9ucy9VdGlscy9TaWNib0dhbWVVdGlsc1wiO1xuaW1wb3J0IElTaWNib1N0YXRlIGZyb20gXCIuL1N0YXRlcy9JU2ljYm9TdGF0ZVwiO1xuaW1wb3J0IFNpY2JvVGV4dCBmcm9tICcuLi9TZXR0aW5nL1NpY2JvVGV4dCc7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1Nlc3Npb25JbmZvQ29tcCBleHRlbmRzIGNjLkNvbXBvbmVudCBpbXBsZW1lbnRzIElTaWNib1N0YXRle1xuXG4gIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgc2Vzc2lvbklkTGFiZWw6IGNjLkxhYmVsID0gbnVsbDtcblxuICBAcHJvcGVydHkoU2ljYm9Nb25leUZvcm1hdENvbXApXG4gIHRvdGFsQmV0TGFiZWw6IFNpY2JvTW9uZXlGb3JtYXRDb21wID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gIGRpY2VMYWJlbDogY2MuTGFiZWwgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBtZDVEZXNjOiBjYy5Ob2RlID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuTm9kZSlcbiAga2V5RGVzYzogY2MuTm9kZSA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICBtZDVMYWJlbDogY2MuTGFiZWwgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBjb3B5Tm90aU5vZGU6IGNjLk5vZGUgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICBtZDVNYXNrOiBjYy5Ob2RlID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgbWQ1UGFyZW50Tm9kZTogY2MuTm9kZSA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLkxheW91dClcbiAgbGF5b3V0OiBjYy5MYXlvdXQgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5BbmltYXRpb24pXG4gIHNoaW5ueUFuaW1hdGlvbjogY2MuQW5pbWF0aW9uID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuQ29sb3IpXG4gIG1kNUNvbG9yOiBjYy5Db2xvciA9IGNjLkNvbG9yLldISVRFO1xuXG4gIEBwcm9wZXJ0eShjYy5Db2xvcilcbiAga2V5Q29sb3I6IGNjLkNvbG9yID0gY2MuQ29sb3IuUkVEO1xuXG4gIG1kNUNhY2hlOiBzdHJpbmcgPSBudWxsO1xuICAvLyBwcml2YXRlIE1ENV9MRU5HVEggPSAzMjtcbiAgcHJpdmF0ZSBNRDVfTUFTS19XSURUSCA9IDMxMDtcbiAgcHJpdmF0ZSBLRVlfV0lEVEggPSAyNzU7XG4gIHByaXZhdGUgTUQ1X1dJRFRIID0gMjk1O1xuICAvLyBwcml2YXRlIE1ENV9NQVNLX0xFRlRfUE9TID0gLTI2LjU7XG4gIC8vIHByaXZhdGUgTUQ1X01BU0tfUklHSFRfUE9TID0gdGhpcy5NRDVfTUFTS19MRUZUX1BPUyArIHRoaXMuTUQ1X01BU0tfV0lEVEg7XG4gIHByaXZhdGUgbWQ1TmV3U3RhcnRUaW1lID0gLjc7XG4gIHByaXZhdGUga2V5TmV3U3RhcnRUaW1lID0gLjY7XG5cbiAgTUQ1X1NUUiA9IFwiTUQ1IDpcIlxuICBLRVlfU1RSID0gXCJLRVkgOlwiXG5cbiAgb25Mb2FkKCl7XG4gICAgdGhpcy5zaGlubnlBbmltYXRpb24ubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB0aGlzLk1ENV9NQVNLX1dJRFRIID0gdGhpcy5tZDVNYXNrLndpZHRoO1xuICB9XG5cbiAgc2V0TWFza0FuY2hvcihpc0xlZnQ6IGJvb2xlYW4gPSB0cnVlKXtcbiAgICAvLyB0aGlzLm1kNU1hc2sueSA9IDA7XG4gICAgaWYoaXNMZWZ0KXtcbiAgICAgIHRoaXMubWQ1TWFzay5hbmNob3JYID0gMDtcbiAgICAgIC8vIHRoaXMubWQ1TWFzay54ID0gdGhpcy5NRDVfTUFTS19MRUZUX1BPUztcbiAgICAgIHRoaXMubWQ1UGFyZW50Tm9kZS5zZXRQb3NpdGlvbihjYy5WZWMyLlpFUk8pO1xuICAgIH1lbHNle1xuICAgICAgdGhpcy5tZDVNYXNrLmFuY2hvclggPSAxO1xuICAgICAgLy8gdGhpcy5tZDVNYXNrLnggPSB0aGlzLk1ENV9NQVNLX1JJR0hUX1BPUztcbiAgICAgIHRoaXMubWQ1UGFyZW50Tm9kZS5zZXRQb3NpdGlvbihjYy5WZWMyLlpFUk8pO1xuICAgICAgdGhpcy5tZDVQYXJlbnROb2RlLnggPSAtdGhpcy5NRDVfTUFTS19XSURUSDtcbiAgICB9XG4gIH1cblxuXG4gIGV4aXRTdGF0ZShzdGF0dXMpOiB2b2lkIHtcbiAgfVxuXG4gIHN0YXJ0U3RhdGUoc3RhdGU6IEluc3RhbmNlVHlwZTx0eXBlb2YgU3RhdGU+LCB0b3RhbER1cmF0aW9uOiBudW1iZXIpOiB2b2lkIHtcbiAgICB0aGlzLnNldFNlc3Npb25JZChzdGF0ZS5nZXRUYWJsZVNlc3Npb25JZCgpLnRvU3RyaW5nKCkpO1xuICAgIGxldCBzdGF0dXMgPSBzdGF0ZS5nZXRTdGF0dXMoKTtcblxuICAgIHN3aXRjaCAoc3RhdHVzKSB7XG4gICAgICAvLyBjYXNlIFN0YXR1cy5XQUlUSU5HOlxuICAgICAgLy8gICB0aGlzLnNldE5ld01kNShzdGF0ZS5nZXRQcmV2TWQ1UmVzdWx0KCksIHN0YXRlLmdldE1kNVJlc3VsdCgpLCBlbGFwc2VUaW1lKTtcbiAgICAgIC8vICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgU3RhdHVzLkJFVFRJTkc6IGNhc2UgU3RhdHVzLkVORF9CRVQ6XG4gICAgICAgIHRoaXMubWQ1Q2FjaGUgPSBzdGF0ZS5nZXRNZDVSZXN1bHQoKTtcbiAgICAgICAgdGhpcy5zZXRNZDVTdHlsZSgpO1xuICAgICAgICB0aGlzLnVwZGF0ZU1kNUxhYmVsKHRoaXMubWQ1Q2FjaGUpO1xuICAgICAgICBicmVhaztcblxuICAgICAgLy8gY2FzZSBTdGF0dXMuUkVTVUxUSU5HOlxuICAgICAgLy8gICB0aGlzLnNldE5ld0tleShzdGF0ZS5nZXRNZDVSZXN1bHQoKSwgc3RhdGUuZ2V0S2V5UmVzdWx0KCksIGVsYXBzZVRpbWUpO1xuICAgICAgLy8gICBicmVhaztcblxuICAgICAgY2FzZSBTdGF0dXMuUEFZSU5HOiBjYXNlIFN0YXR1cy5GSU5JU0hJTkc6XG4gICAgICAgIHRoaXMubWQ1Q2FjaGUgPSBzdGF0ZS5nZXRLZXlSZXN1bHQoKTtcbiAgICAgICAgdGhpcy51cGRhdGVNZDVMYWJlbCh0aGlzLm1kNUNhY2hlKTtcbiAgICAgICAgdGhpcy5zZXRLZXlTdHlsZSgpO1xuICAgICAgICBicmVhazsgXG4gICAgfVxuICB9XG5cbiAgdXBkYXRlU3RhdGUoc3RhdGU6IEluc3RhbmNlVHlwZTx0eXBlb2YgU3RhdGU+KTogdm9pZCB7XG4gICAgdGhpcy5zZXRUb3RhbEJldChzdGF0ZS5nZXRTZXNzaW9uUGxheWVyQmV0dGVkQW1vdW50KCkpO1xuXG4gICAgbGV0IHN0YXR1cyA9IHN0YXRlLmdldFN0YXR1cygpO1xuICAgIGxldCBlbGFwc2VUaW1lID0gc3RhdGUuZ2V0U3RhZ2VUaW1lKCkvMTAwMDtcblxuICAgIHN3aXRjaCAoc3RhdHVzKSB7XG4gICAgICBjYXNlIFN0YXR1cy5XQUlUSU5HOlxuICAgICAgICB0aGlzLnNldE5ld01kNShzdGF0ZS5nZXRQcmV2S2V5UmVzdWx0KCksIHN0YXRlLmdldE1kNVJlc3VsdCgpLCBlbGFwc2VUaW1lKTtcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgU3RhdHVzLlJFU1VMVElORzpcbiAgICAgICAgdGhpcy5zZXROZXdLZXkoc3RhdGUuZ2V0TWQ1UmVzdWx0KCksIHN0YXRlLmdldEtleVJlc3VsdCgpLCBlbGFwc2VUaW1lKTtcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuICB9XG5cbiAgb25IaWRlKCk6IHZvaWQge1xuICAgIHRoaXMubWQ1TGFiZWwubm9kZS53aWR0aCA9IDA7XG4gICAgLy8gY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMuY29weU5vdGlOb2RlKTtcbiAgICAvLyBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5tZDVNYXNrKTtcbiAgICAvLyBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5sYXlvdXQpOyAgICBcbiAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5ub2RlKTsgICAgXG4gIH1cblxuICBzZXRTZXNzaW9uSWQoc2Vzc2lvbklkOiBzdHJpbmcpIHtcbiAgICB0aGlzLnNlc3Npb25JZExhYmVsLnN0cmluZyA9IFNpY2JvR2FtZVV0aWxzLkZvcm1hdFN0cmluZyhTaWNib1RleHQuc2Vzc2lvbklkVGV4dCwgc2Vzc2lvbklkKTtcbiAgfVxuICBzZXRUb3RhbEJldCh0b3RhbEJldDogbnVtYmVyKSB7XG4gICAgdGhpcy50b3RhbEJldExhYmVsLnNldE1vbmV5KHRvdGFsQmV0KTtcbiAgfVxuXG5cbiAgc2V0TmV3TWQ1KG1kNVByZXZSYXc6IHN0cmluZywgbWQ1TmV3UmF3OiBzdHJpbmcsIGVsYXBzZVRpbWU6IG51bWJlcil7XG4gICAgaWYoZWxhcHNlVGltZT50aGlzLm1kNU5ld1N0YXJ0VGltZSl7ICAgXG4gICAgICB0aGlzLm1kNUNhY2hlID0gbWQ1TmV3UmF3OyAgIFxuICAgICAgdGhpcy51cGRhdGVNZDVMYWJlbCh0aGlzLm1kNUNhY2hlKTtcbiAgICAgIHRoaXMuc2V0TWQ1U3R5bGUoKTtcbiAgICB9ZWxzZSBpZihlbGFwc2VUaW1lIDwgdGhpcy5tZDVOZXdTdGFydFRpbWUpe1xuICAgICAgdGhpcy5tZDVDYWNoZSA9IG1kNVByZXZSYXc7XG4gICAgICB0aGlzLnNldEtleVN0eWxlKCk7XG4gICAgICB0aGlzLnVwZGF0ZU1kNUxhYmVsKHRoaXMubWQ1Q2FjaGUpO1xuICAgIH1lbHNle1xuICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgY2MudHdlZW4oc2VsZi5ub2RlKVxuICAgICAgICAvLyAuZGVsYXkoc2VsZi5tZDVOZXdTdGFydFRpbWUgLSBlbGFwc2VUaW1lKVxuICAgICAgICAuY2FsbChcbiAgICAgICAgICAoKT0+e1xuICAgICAgICAgICAgc2VsZi5tZDVDYWNoZSA9IG1kNU5ld1JhdzsgICAgICAgICAgICAgXG4gICAgICAgICAgICBzZWxmLnNldE1kNVN0eWxlKCk7XG4gICAgICAgICAgICBzZWxmLnNldE1hc2tBbmNob3IodHJ1ZSk7XG4gICAgICAgICAgICBzZWxmLnBsYXlMZWZ0VG9SaWdodCgpO1xuICAgICAgICAgICAgc2VsZi51cGRhdGVNZDVMYWJlbChzZWxmLm1kNUNhY2hlKTtcbiAgICAgICAgICB9XG4gICAgICAgICkuc3RhcnQoKTsgICBcbiAgICB9IFxuICB9XG5cbiAgc2V0TmV3S2V5KG1kNU5ld1Jhdzogc3RyaW5nLCBrZXlOZXdSYXc6IHN0cmluZywgZWxhcHNlVGltZTogbnVtYmVyKXtcbiAgICBpZihlbGFwc2VUaW1lPnRoaXMua2V5TmV3U3RhcnRUaW1lKXsgICBcbiAgICAgIHRoaXMubWQ1Q2FjaGUgPSBrZXlOZXdSYXc7ICAgXG4gICAgICB0aGlzLnVwZGF0ZU1kNUxhYmVsKHRoaXMubWQ1Q2FjaGUpO1xuICAgICAgdGhpcy5zZXRLZXlTdHlsZSgpO1xuICAgIH1lbHNlIGlmKGVsYXBzZVRpbWUgPCB0aGlzLmtleU5ld1N0YXJ0VGltZSl7XG4gICAgICB0aGlzLm1kNUNhY2hlID0gbWQ1TmV3UmF3OyAgICAgIFxuICAgICAgdGhpcy51cGRhdGVNZDVMYWJlbCh0aGlzLm1kNUNhY2hlKTtcbiAgICAgIHRoaXMuc2V0TWQ1U3R5bGUoKTtcbiAgICB9ZWxzZXtcbiAgICAgIGxldCBzZWxmID0gdGhpcztcbiAgICAgIGNjLnR3ZWVuKHNlbGYubm9kZSlcbiAgICAgICAgLy8gLmRlbGF5KHNlbGYua2V5TmV3U3RhcnRUaW1lIC0gZWxhcHNlVGltZSlcbiAgICAgICAgLmNhbGwoXG4gICAgICAgICAgKCk9PntcbiAgICAgICAgICAgIHNlbGYubWQ1Q2FjaGUgPSBrZXlOZXdSYXc7ICAgXG4gICAgICAgICAgICBzZWxmLnVwZGF0ZU1kNUxhYmVsKHNlbGYubWQ1Q2FjaGUpO1xuICAgICAgICAgICAgc2VsZi5zZXRLZXlTdHlsZSgpO1xuICAgICAgICAgICAgc2VsZi5zZXRNYXNrQW5jaG9yKGZhbHNlKTtcbiAgICAgICAgICAgIHNlbGYucGxheVJpZ2h0VG9MZWZ0KCk7XG4gICAgICAgICAgfVxuICAgICAgICApLnN0YXJ0KCk7ICAgXG4gICAgfSBcbiAgfVxuXG5cbiAgcHJpdmF0ZSBzZXRLZXlTdHlsZSgpe1xuICAgIHRoaXMubWQ1RGVzYy5hY3RpdmUgPSBmYWxzZTtcbiAgICB0aGlzLmtleURlc2MuYWN0aXZlID0gdHJ1ZTtcbiAgICB0aGlzLm1kNUxhYmVsLm5vZGUuY29sb3IgPSB0aGlzLmtleUNvbG9yO1xuICAgIHRoaXMubWQ1TGFiZWwubm9kZS53aWR0aCA9IHRoaXMuS0VZX1dJRFRIO1xuICAgIHRoaXMuZGljZUxhYmVsLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0TWQ1U3R5bGUoKXtcbiAgICB0aGlzLm1kNURlc2MuYWN0aXZlID0gdHJ1ZTtcbiAgICB0aGlzLmtleURlc2MuYWN0aXZlID0gZmFsc2U7XG4gICAgdGhpcy5tZDVMYWJlbC5ub2RlLmNvbG9yID0gdGhpcy5tZDVDb2xvcjtcbiAgICB0aGlzLm1kNUxhYmVsLm5vZGUud2lkdGggPSB0aGlzLk1ENV9XSURUSDtcbiAgICB0aGlzLmRpY2VMYWJlbC5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuXG4gIH1cblxuICBwcml2YXRlIHVwZGF0ZU1kNUxhYmVsKHJhd1N0cjogc3RyaW5nKXtcbiAgICBsZXQgW2RpY2VTdHIsIG1kNVN0cl0gPSB0aGlzLnBhcnNlTWQ1KHJhd1N0cik7ICAgXG4gICAgdGhpcy5tZDVMYWJlbC5zdHJpbmcgPSBtZDVTdHI7XG4gICAgdGhpcy5kaWNlTGFiZWwuc3RyaW5nID0gZGljZVN0cjtcbiAgfVxuXG4gIHByaXZhdGUgcGxheVJpZ2h0VG9MZWZ0KCl7XG4gICAgLy8gdGhpcy5zZXRNYXNrQW5jaG9yKGZhbHNlKTtcbiAgICB0aGlzLnBsYXlNZDVBbmltKHRydWUpO1xuICAgIHRoaXMuc2hpbm55QW5pbWF0aW9uLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICB0aGlzLnNoaW5ueUFuaW1hdGlvbi5wbGF5KFwicmlnaHRUb0xlZnRcIik7XG4gIH1cblxuICBwcml2YXRlIHBsYXlMZWZ0VG9SaWdodCgpe1xuICAgIC8vIHRoaXMuc2V0TWFza0FuY2hvcih0cnVlKTtcbiAgICB0aGlzLnBsYXlNZDVBbmltKCk7XG4gICAgdGhpcy5zaGlubnlBbmltYXRpb24ubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIHRoaXMuc2hpbm55QW5pbWF0aW9uLnBsYXkoXCJsZWZ0VG9SaWdodFwiKTtcbiAgfVxuXG5cbiAgcGxheU1kNUFuaW0oYW5pbUxheW91dDogYm9vbGVhbiA9IGZhbHNlLCBhbmltVGltZTogbnVtYmVyID0gMS4xNSl7XG4gICAgdGhpcy5tZDVNYXNrLndpZHRoID0gNTtcbiAgICBjYy50d2Vlbih0aGlzLm1kNU1hc2spXG4gICAgICAudG8oYW5pbVRpbWUsIHt3aWR0aDogdGhpcy5NRDVfTUFTS19XSURUSH0pXG4gICAgICAuc3RhcnQoKTtcblxuXG4gICAgaWYoIWFuaW1MYXlvdXQpIHtcbiAgICAgIHRoaXMubGF5b3V0LnNwYWNpbmdYID0gMDtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5sYXlvdXQuc3BhY2luZ1ggPSB0aGlzLk1ENV9NQVNLX1dJRFRIIC0gNTtcbiAgICBjYy50d2Vlbih0aGlzLmxheW91dClcbiAgICAudG8oYW5pbVRpbWUsIHtzcGFjaW5nWDogMH0pXG4gICAgLnN0YXJ0KCk7XG4gIH1cblxuICAvKipcbiAgICogXG4gICAqIEBwYXJhbSBtZDVSYXcgXG4gICAqIEByZXR1cm5zIFtkaWNlU3RyLCBtZDVTdHJdXG4gICAqL1xuICBwYXJzZU1kNShtZDVSYXc6IHN0cmluZyl7XG4gICAgLy8gbWQ1UmF3ID0gbWQ1UmF3LnJlcGxhY2UoXCI6XCIsIFwiXCIpO1xuICAgIGxldCBtZDVTdHIgPSBcIlwiO1xuICAgIGxldCBkaWNlU3RyID0gXCJcIjtcblxuICAgIGxldCB0ZW1wID0gbWQ1UmF3LmluZGV4T2YoXCJdXCIpO1xuICAgIGlmKHRlbXAgPCAwKXtcbiAgICAgIG1kNVN0ciA9IG1kNVJhdztcbiAgICAgIGRpY2VTdHIgPSBcIlxcblwiO1xuICAgIH1lbHNle1xuICAgICAgbWQ1U3RyID0gbWQ1UmF3LnN1YnN0cih0ZW1wICsgMSk7XG4gICAgICBkaWNlU3RyID0gbWQ1UmF3LnN1YnN0cigwLCB0ZW1wICsgMSk7XG4gICAgfVxuICAgIHJldHVybiBbZGljZVN0ciwgbWQ1U3RyXTtcbiAgfVxuXG4gIG9uQ2xpY2tDb3B5KCkge1xuICAgIFNpY2JvR2FtZVV0aWxzLmNvcHlUb0NsaXBib2FyZCh0aGlzLm1kNUNhY2hlKTtcbiAgICB0aGlzLnBsYXlDb3B5Tm90aSgpO1xuICB9XG5cbiAgcGxheUNvcHlOb3RpKCl7XG4gICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMuY29weU5vdGlOb2RlKTtcbiAgICBjYy50d2Vlbih0aGlzLmNvcHlOb3RpTm9kZSlcbiAgICAgIC50byguMiwge3NjYWxlOiAxLCBvcGFjaXR5OiAyNTV9KSAgICAgIFxuICAgICAgLmRlbGF5KDEpXG4gICAgICAudG8oLjIsIHtzY2FsZTowLCBvcGFjaXR5OiAwfSlcbiAgICAgIC5zdGFydCgpO1xuICB9XG5cbiAgb25EZXN0cm95KCl7XG4gICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMuY29weU5vdGlOb2RlKTtcbiAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5tZDVNYXNrKTtcbiAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5sYXlvdXQpOyAgICBcbiAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5ub2RlKTsgICAgXG5cbiAgfVxufVxuIl19