
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboNotifyComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '74b43csqj1LFYozQkxS+7XD', 'SicboNotifyComp');
// Game/Sicbo_New/Scripts/Components/SicboNotifyComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboController_1 = require("../Controllers/SicboController");
var SicboSkeletonUtils_1 = require("../RNGCommons/Utils/SicboSkeletonUtils");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboNotifyComp = /** @class */ (function (_super) {
    __extends(SicboNotifyComp, _super);
    function SicboNotifyComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //SMALL NOTI
        _this.smallNotiRoot = null;
        _this.smallNotiMessage = null;
        _this.skipAllNextNoti = false;
        //LARGE NOTI
        _this.largeNotiSkeleton = null;
        _this.largeNotiMessage = null;
        _this.bettingMessageSprite = null;
        _this.stopBettingMessageSprite = null;
        //RESULT NOTI
        _this.resultNotiSkeleton = null;
        _this.resultNotiMessage = null;
        return _this;
    }
    SicboNotifyComp.prototype.onLoad = function () {
        this.smallNotiRoot.opacity = 0;
        this.largeNotiSkeleton.node.opacity = 0;
    };
    //SECTION SMALL
    SicboNotifyComp.prototype.showSmallNoti = function (message, enterTime, showTime, exitTime, skipAllNextNoti) {
        if (enterTime === void 0) { enterTime = .5; }
        if (showTime === void 0) { showTime = 2; }
        if (exitTime === void 0) { exitTime = .5; }
        if (skipAllNextNoti === void 0) { skipAllNextNoti = false; }
        if (this.skipAllNextNoti)
            return;
        var self = this;
        cc.Tween.stopAllByTarget(this.smallNotiRoot);
        self.smallNotiMessage.string = message;
        self.smallNotiRoot.active = true;
        cc.tween(self.smallNotiRoot)
            .to(enterTime, { opacity: 255, scale: 1 })
            .delay(showTime)
            .to(exitTime, { opacity: 150 })
            .call(function () {
            self.skipAllNextNoti = false;
            self.smallNotiRoot.active = false;
            self.smallNotiRoot.scale = 2;
        })
            .start();
        this.skipAllNextNoti = skipAllNextNoti;
    };
    SicboNotifyComp.prototype.updateSmallNotiMessage = function (message) {
        this.smallNotiMessage.string = message;
    };
    SicboNotifyComp.prototype.forceHideSmallNoti = function () {
        cc.Tween.stopAllByTarget(this.smallNotiRoot);
        this.smallNotiRoot.opacity = 150;
        this.smallNotiRoot.active = false;
        this.smallNotiRoot.scale = 2;
        this.skipAllNextNoti = false;
    };
    //!SECTION
    //SECTION LARGE
    SicboNotifyComp.prototype.showLargeNoti = function (enterTime, showTime, exitTime) {
        if (enterTime === void 0) { enterTime = 0; }
        if (showTime === void 0) { showTime = 2.5; }
        if (exitTime === void 0) { exitTime = .5; }
        cc.Tween.stopAllByTarget(this.largeNotiSkeleton.node);
        SicboSkeletonUtils_1.default.setAnimation(this.largeNotiSkeleton, "noti_2");
        this.largeNotiSkeleton.node.opacity = 255;
        cc.tween(this.largeNotiSkeleton.node)
            .to(enterTime, { opacity: 255 })
            .delay(showTime)
            .to(exitTime, { opacity: 0 })
            .start();
        SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxMessage);
    };
    SicboNotifyComp.prototype.showBettingMessage = function () {
        this.largeNotiMessage.spriteFrame = this.bettingMessageSprite;
        this.showLargeNoti();
    };
    SicboNotifyComp.prototype.showStopBettingMessage = function () {
        this.largeNotiMessage.spriteFrame = this.stopBettingMessageSprite;
        this.showLargeNoti();
    };
    SicboNotifyComp.prototype.forceHideLargeNoti = function () {
        cc.Tween.stopAllByTarget(this.largeNotiSkeleton.node);
        this.largeNotiSkeleton.node.opacity = 0;
    };
    //!SECTION
    //SECTION RESULT
    SicboNotifyComp.prototype.showResultNoti = function (message, enterTime, showTime, exitTime) {
        var _this = this;
        if (enterTime === void 0) { enterTime = .34; }
        if (showTime === void 0) { showTime = 2.5; }
        if (exitTime === void 0) { exitTime = .34; }
        cc.Tween.stopAllByTarget(this.resultNotiSkeleton.node);
        this.resultNotiMessage.string = message;
        SicboSkeletonUtils_1.default.setAnimation(this.resultNotiSkeleton, "in");
        this.resultNotiSkeleton.node.opacity = 255;
        cc.tween(this.resultNotiSkeleton.node)
            .delay(enterTime)
            .call(function () {
            SicboSkeletonUtils_1.default.setAnimation(_this.resultNotiSkeleton, "loop", true);
        })
            .delay(showTime)
            .call(function () {
            SicboSkeletonUtils_1.default.setAnimation(_this.resultNotiSkeleton, "out", false);
        })
            .delay(exitTime)
            .call(function () {
            _this.resultNotiSkeleton.node.opacity = 0;
        })
            .start();
        this.resultNotiMessage.node.opacity = 0;
        cc.tween(this.resultNotiMessage.node)
            .delay(enterTime / 2)
            .to(enterTime / 2, { opacity: 255 })
            .delay(showTime)
            .to(exitTime, { opacity: 0 })
            .start();
        // SicboController.Instance.playSfx(SicboSound.SfxMessage);
    };
    SicboNotifyComp.prototype.forceHideResultNoti = function () {
        cc.Tween.stopAllByTarget(this.resultNotiSkeleton.node);
        this.resultNotiSkeleton.node.opacity = 0;
    };
    //!SECTION
    SicboNotifyComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.smallNotiRoot);
        cc.Tween.stopAllByTarget(this.largeNotiSkeleton.node);
        cc.Tween.stopAllByTarget(this.resultNotiSkeleton.node);
        cc.Tween.stopAllByTarget(this.resultNotiMessage.node);
    };
    __decorate([
        property(cc.Node)
    ], SicboNotifyComp.prototype, "smallNotiRoot", void 0);
    __decorate([
        property(cc.Label)
    ], SicboNotifyComp.prototype, "smallNotiMessage", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboNotifyComp.prototype, "largeNotiSkeleton", void 0);
    __decorate([
        property(cc.Sprite)
    ], SicboNotifyComp.prototype, "largeNotiMessage", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], SicboNotifyComp.prototype, "bettingMessageSprite", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], SicboNotifyComp.prototype, "stopBettingMessageSprite", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboNotifyComp.prototype, "resultNotiSkeleton", void 0);
    __decorate([
        property(cc.Label)
    ], SicboNotifyComp.prototype, "resultNotiMessage", void 0);
    SicboNotifyComp = __decorate([
        ccclass
    ], SicboNotifyComp);
    return SicboNotifyComp;
}(cc.Component));
exports.default = SicboNotifyComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9Ob3RpZnlDb21wLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWxGLHdEQUFxRDtBQUNyRCxrRUFBNkQ7QUFDN0QsNkVBQXdFO0FBRWxFLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQTZDLG1DQUFZO0lBQXpEO1FBQUEscUVBZ0pDO1FBL0lHLFlBQVk7UUFFWixtQkFBYSxHQUFZLElBQUksQ0FBQztRQUU5QixzQkFBZ0IsR0FBYSxJQUFJLENBQUM7UUFDbEMscUJBQWUsR0FBWSxLQUFLLENBQUM7UUFFakMsWUFBWTtRQUVaLHVCQUFpQixHQUFnQixJQUFJLENBQUM7UUFFdEMsc0JBQWdCLEdBQWMsSUFBSSxDQUFDO1FBR25DLDBCQUFvQixHQUFtQixJQUFJLENBQUM7UUFFNUMsOEJBQXdCLEdBQW1CLElBQUksQ0FBQztRQUVoRCxhQUFhO1FBRWIsd0JBQWtCLEdBQWdCLElBQUksQ0FBQztRQUV2Qyx1QkFBaUIsR0FBYSxJQUFJLENBQUM7O0lBeUh2QyxDQUFDO0lBdkhHLGdDQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxlQUFlO0lBQ2YsdUNBQWEsR0FBYixVQUFjLE9BQWUsRUFBRSxTQUFzQixFQUFFLFFBQW9CLEVBQUUsUUFBcUIsRUFBRSxlQUFnQztRQUFyRywwQkFBQSxFQUFBLGNBQXNCO1FBQUUseUJBQUEsRUFBQSxZQUFvQjtRQUFFLHlCQUFBLEVBQUEsYUFBcUI7UUFBRSxnQ0FBQSxFQUFBLHVCQUFnQztRQUNoSSxJQUFHLElBQUksQ0FBQyxlQUFlO1lBQUUsT0FBTztRQUNoQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNqQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7YUFDdkIsRUFBRSxDQUFDLFNBQVMsRUFBRSxFQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBQyxDQUFDO2FBQ3ZDLEtBQUssQ0FBQyxRQUFRLENBQUM7YUFDZixFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUMsT0FBTyxFQUFFLEdBQUcsRUFBQyxDQUFDO2FBQzVCLElBQUksQ0FDRDtZQUNJLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1lBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNsQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDakMsQ0FBQyxDQUNKO2FBQ0EsS0FBSyxFQUFFLENBQUM7UUFDYixJQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQztJQUMzQyxDQUFDO0lBRUQsZ0RBQXNCLEdBQXRCLFVBQXVCLE9BQWU7UUFDbEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7SUFDM0MsQ0FBQztJQUVELDRDQUFrQixHQUFsQjtRQUNJLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDakMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUNqQyxDQUFDO0lBQ0QsVUFBVTtJQUVWLGVBQWU7SUFDZix1Q0FBYSxHQUFiLFVBQWMsU0FBcUIsRUFBRSxRQUFzQixFQUFFLFFBQXFCO1FBQXBFLDBCQUFBLEVBQUEsYUFBcUI7UUFBRSx5QkFBQSxFQUFBLGNBQXNCO1FBQUUseUJBQUEsRUFBQSxhQUFxQjtRQUM5RSxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFdEQsNEJBQWtCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUVsRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDMUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO2FBQ2hDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsRUFBQyxPQUFPLEVBQUUsR0FBRyxFQUFDLENBQUM7YUFDN0IsS0FBSyxDQUFDLFFBQVEsQ0FBQzthQUNmLEVBQUUsQ0FBQyxRQUFRLEVBQUUsRUFBQyxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUM7YUFDMUIsS0FBSyxFQUFFLENBQUM7UUFFYix5QkFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMseUJBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUM1RCxDQUFDO0lBRUQsNENBQWtCLEdBQWxCO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7UUFDOUQsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxnREFBc0IsR0FBdEI7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztRQUNsRSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVELDRDQUFrQixHQUFsQjtRQUNJLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUNELFVBQVU7SUFFVCxnQkFBZ0I7SUFDaEIsd0NBQWMsR0FBZCxVQUFlLE9BQWUsRUFBRSxTQUF1QixFQUFFLFFBQXNCLEVBQUUsUUFBc0I7UUFBdkcsaUJBK0JBO1FBL0JnQywwQkFBQSxFQUFBLGVBQXVCO1FBQUUseUJBQUEsRUFBQSxjQUFzQjtRQUFFLHlCQUFBLEVBQUEsY0FBc0I7UUFDcEcsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXZELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO1FBQ3hDLDRCQUFrQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFL0QsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQzNDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQzthQUNqQyxLQUFLLENBQUMsU0FBUyxDQUFDO2FBQ2hCLElBQUksQ0FBQztZQUNGLDRCQUFrQixDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzNFLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxRQUFRLENBQUM7YUFDZixJQUFJLENBQUM7WUFDRiw0QkFBa0IsQ0FBQyxZQUFZLENBQUMsS0FBSSxDQUFDLGtCQUFrQixFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMzRSxDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsUUFBUSxDQUFDO2FBQ2YsSUFBSSxDQUFDO1lBQ0YsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQzdDLENBQUMsQ0FBQzthQUNELEtBQUssRUFBRSxDQUFDO1FBRWIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ3hDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQzthQUNoQyxLQUFLLENBQUMsU0FBUyxHQUFDLENBQUMsQ0FBQzthQUNsQixFQUFFLENBQUMsU0FBUyxHQUFDLENBQUMsRUFBRyxFQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUMsQ0FBQzthQUNoQyxLQUFLLENBQUMsUUFBUSxDQUFDO2FBQ2YsRUFBRSxDQUFDLFFBQVEsRUFBRyxFQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQzthQUMzQixLQUFLLEVBQUUsQ0FBQztRQUViLDJEQUEyRDtJQUMvRCxDQUFDO0lBR0QsNkNBQW1CLEdBQW5CO1FBQ0ksRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBQ0QsVUFBVTtJQUVWLG1DQUFTLEdBQVQ7UUFDSSxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDN0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RELEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2RCxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQTVJRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzBEQUNZO0lBRTlCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7NkRBQ2U7SUFLbEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQzs4REFDZ0I7SUFFdEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs2REFDZTtJQUduQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO2lFQUNtQjtJQUU1QztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO3FFQUN1QjtJQUloRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDOytEQUNpQjtJQUV2QztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzhEQUNnQjtJQXZCbEIsZUFBZTtRQURuQyxPQUFPO09BQ2EsZUFBZSxDQWdKbkM7SUFBRCxzQkFBQztDQWhKRCxBQWdKQyxDQWhKNEMsRUFBRSxDQUFDLFNBQVMsR0FnSnhEO2tCQWhKb0IsZUFBZSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgeyBTaWNib1NvdW5kIH0gZnJvbSBcIi4uL1NldHRpbmcvU2ljYm9TZXR0aW5nXCI7XG5pbXBvcnQgU2ljYm9Db250cm9sbGVyIGZyb20gXCIuLi9Db250cm9sbGVycy9TaWNib0NvbnRyb2xsZXJcIjtcbmltcG9ydCBTaWNib1NrZWxldG9uVXRpbHMgZnJvbSBcIi4uL1JOR0NvbW1vbnMvVXRpbHMvU2ljYm9Ta2VsZXRvblV0aWxzXCI7XG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9Ob3RpZnlDb21wIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICAvL1NNQUxMIE5PVElcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBzbWFsbE5vdGlSb290OiBjYy5Ob2RlID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgc21hbGxOb3RpTWVzc2FnZTogY2MuTGFiZWwgPSBudWxsO1xuICAgIHNraXBBbGxOZXh0Tm90aTogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgLy9MQVJHRSBOT1RJXG4gICAgQHByb3BlcnR5KHNwLlNrZWxldG9uKVxuICAgIGxhcmdlTm90aVNrZWxldG9uOiBzcC5Ta2VsZXRvbiA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBsYXJnZU5vdGlNZXNzYWdlOiBjYy5TcHJpdGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxuICAgIGJldHRpbmdNZXNzYWdlU3ByaXRlOiBjYy5TcHJpdGVGcmFtZSA9IG51bGw7XG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxuICAgIHN0b3BCZXR0aW5nTWVzc2FnZVNwcml0ZTogY2MuU3ByaXRlRnJhbWUgPSBudWxsO1xuXG4gICAgLy9SRVNVTFQgTk9USVxuICAgIEBwcm9wZXJ0eShzcC5Ta2VsZXRvbilcbiAgICByZXN1bHROb3RpU2tlbGV0b246IHNwLlNrZWxldG9uID0gbnVsbDtcbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgcmVzdWx0Tm90aU1lc3NhZ2U6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIG9uTG9hZCgpe1xuICAgICAgICB0aGlzLnNtYWxsTm90aVJvb3Qub3BhY2l0eSA9IDA7XG4gICAgICAgIHRoaXMubGFyZ2VOb3RpU2tlbGV0b24ubm9kZS5vcGFjaXR5ID0gMDsgICAgICAgIFxuICAgIH1cblxuICAgIC8vU0VDVElPTiBTTUFMTFxuICAgIHNob3dTbWFsbE5vdGkobWVzc2FnZTogc3RyaW5nLCBlbnRlclRpbWU6IG51bWJlciA9IC41LCBzaG93VGltZTogbnVtYmVyID0gMiwgZXhpdFRpbWU6IG51bWJlciA9IC41LCBza2lwQWxsTmV4dE5vdGk6IGJvb2xlYW4gPSBmYWxzZSl7ICAgICAgICBcbiAgICAgICAgaWYodGhpcy5za2lwQWxsTmV4dE5vdGkpIHJldHVybjtcbiAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5zbWFsbE5vdGlSb290KTtcbiAgICAgICAgc2VsZi5zbWFsbE5vdGlNZXNzYWdlLnN0cmluZyA9IG1lc3NhZ2U7XG4gICAgICAgIHNlbGYuc21hbGxOb3RpUm9vdC5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBjYy50d2VlbihzZWxmLnNtYWxsTm90aVJvb3QpXG4gICAgICAgICAgICAudG8oZW50ZXJUaW1lLCB7b3BhY2l0eTogMjU1LCBzY2FsZTogMX0pXG4gICAgICAgICAgICAuZGVsYXkoc2hvd1RpbWUpXG4gICAgICAgICAgICAudG8oZXhpdFRpbWUsIHtvcGFjaXR5OiAxNTB9KVxuICAgICAgICAgICAgLmNhbGwoXG4gICAgICAgICAgICAgICAgKCk9PntcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5za2lwQWxsTmV4dE5vdGkgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5zbWFsbE5vdGlSb290LmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICBzZWxmLnNtYWxsTm90aVJvb3Quc2NhbGUgPSAyO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIClcbiAgICAgICAgICAgIC5zdGFydCgpO1xuICAgICAgICB0aGlzLnNraXBBbGxOZXh0Tm90aSA9IHNraXBBbGxOZXh0Tm90aTtcbiAgICB9XG5cbiAgICB1cGRhdGVTbWFsbE5vdGlNZXNzYWdlKG1lc3NhZ2U6IHN0cmluZyl7XG4gICAgICAgIHRoaXMuc21hbGxOb3RpTWVzc2FnZS5zdHJpbmcgPSBtZXNzYWdlO1xuICAgIH1cblxuICAgIGZvcmNlSGlkZVNtYWxsTm90aSgpe1xuICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5zbWFsbE5vdGlSb290KTtcbiAgICAgICAgdGhpcy5zbWFsbE5vdGlSb290Lm9wYWNpdHkgPSAxNTA7XG4gICAgICAgIHRoaXMuc21hbGxOb3RpUm9vdC5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zbWFsbE5vdGlSb290LnNjYWxlID0gMjtcbiAgICAgICAgdGhpcy5za2lwQWxsTmV4dE5vdGkgPSBmYWxzZTtcbiAgICB9XG4gICAgLy8hU0VDVElPTlxuXG4gICAgLy9TRUNUSU9OIExBUkdFXG4gICAgc2hvd0xhcmdlTm90aShlbnRlclRpbWU6IG51bWJlciA9IDAsIHNob3dUaW1lOiBudW1iZXIgPSAyLjUsIGV4aXRUaW1lOiBudW1iZXIgPSAuNSl7ICAgICAgICBcbiAgICAgICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMubGFyZ2VOb3RpU2tlbGV0b24ubm9kZSk7ICAgICAgIFxuICAgICAgICBcbiAgICAgICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLmxhcmdlTm90aVNrZWxldG9uLCBcIm5vdGlfMlwiKTtcblxuICAgICAgICB0aGlzLmxhcmdlTm90aVNrZWxldG9uLm5vZGUub3BhY2l0eSA9IDI1NTtcbiAgICAgICAgY2MudHdlZW4odGhpcy5sYXJnZU5vdGlTa2VsZXRvbi5ub2RlKVxuICAgICAgICAgICAgLnRvKGVudGVyVGltZSwge29wYWNpdHk6IDI1NX0pXG4gICAgICAgICAgICAuZGVsYXkoc2hvd1RpbWUpXG4gICAgICAgICAgICAudG8oZXhpdFRpbWUsIHtvcGFjaXR5OiAwfSlcbiAgICAgICAgICAgIC5zdGFydCgpOyAgICAgICAgICAgIFxuXG4gICAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4TWVzc2FnZSk7XG4gICAgfVxuXG4gICAgc2hvd0JldHRpbmdNZXNzYWdlKCl7XG4gICAgICAgIHRoaXMubGFyZ2VOb3RpTWVzc2FnZS5zcHJpdGVGcmFtZSA9IHRoaXMuYmV0dGluZ01lc3NhZ2VTcHJpdGU7XG4gICAgICAgIHRoaXMuc2hvd0xhcmdlTm90aSgpO1xuICAgIH1cblxuICAgIHNob3dTdG9wQmV0dGluZ01lc3NhZ2UoKXtcbiAgICAgICAgdGhpcy5sYXJnZU5vdGlNZXNzYWdlLnNwcml0ZUZyYW1lID0gdGhpcy5zdG9wQmV0dGluZ01lc3NhZ2VTcHJpdGU7XG4gICAgICAgIHRoaXMuc2hvd0xhcmdlTm90aSgpO1xuICAgIH1cblxuICAgIGZvcmNlSGlkZUxhcmdlTm90aSgpe1xuICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5sYXJnZU5vdGlTa2VsZXRvbi5ub2RlKTsgICAgIFxuICAgICAgICB0aGlzLmxhcmdlTm90aVNrZWxldG9uLm5vZGUub3BhY2l0eSA9IDA7XG4gICAgfVxuICAgIC8vIVNFQ1RJT05cbiAgXG4gICAgIC8vU0VDVElPTiBSRVNVTFRcbiAgICAgc2hvd1Jlc3VsdE5vdGkobWVzc2FnZTogc3RyaW5nLCBlbnRlclRpbWU6IG51bWJlciA9IC4zNCwgc2hvd1RpbWU6IG51bWJlciA9IDIuNSwgZXhpdFRpbWU6IG51bWJlciA9IC4zNCl7ICAgICAgICBcbiAgICAgICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMucmVzdWx0Tm90aVNrZWxldG9uLm5vZGUpOyAgICAgICBcbiAgICAgICAgXG4gICAgICAgIHRoaXMucmVzdWx0Tm90aU1lc3NhZ2Uuc3RyaW5nID0gbWVzc2FnZTtcbiAgICAgICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLnJlc3VsdE5vdGlTa2VsZXRvbiwgXCJpblwiKTtcblxuICAgICAgICB0aGlzLnJlc3VsdE5vdGlTa2VsZXRvbi5ub2RlLm9wYWNpdHkgPSAyNTU7XG4gICAgICAgIGNjLnR3ZWVuKHRoaXMucmVzdWx0Tm90aVNrZWxldG9uLm5vZGUpXG4gICAgICAgICAgICAuZGVsYXkoZW50ZXJUaW1lKVxuICAgICAgICAgICAgLmNhbGwoKCk9PntcbiAgICAgICAgICAgICAgICBTaWNib1NrZWxldG9uVXRpbHMuc2V0QW5pbWF0aW9uKHRoaXMucmVzdWx0Tm90aVNrZWxldG9uLCBcImxvb3BcIiwgdHJ1ZSk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmRlbGF5KHNob3dUaW1lKVxuICAgICAgICAgICAgLmNhbGwoKCk9PntcbiAgICAgICAgICAgICAgICBTaWNib1NrZWxldG9uVXRpbHMuc2V0QW5pbWF0aW9uKHRoaXMucmVzdWx0Tm90aVNrZWxldG9uLCBcIm91dFwiLCBmYWxzZSk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmRlbGF5KGV4aXRUaW1lKVxuICAgICAgICAgICAgLmNhbGwoKCk9PntcbiAgICAgICAgICAgICAgICB0aGlzLnJlc3VsdE5vdGlTa2VsZXRvbi5ub2RlLm9wYWNpdHkgPSAwO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5zdGFydCgpOyAgICAgICAgICAgIFxuXG4gICAgICAgIHRoaXMucmVzdWx0Tm90aU1lc3NhZ2Uubm9kZS5vcGFjaXR5ID0gMDtcbiAgICAgICAgY2MudHdlZW4odGhpcy5yZXN1bHROb3RpTWVzc2FnZS5ub2RlKVxuICAgICAgICAgICAgLmRlbGF5KGVudGVyVGltZS8yKVxuICAgICAgICAgICAgLnRvKGVudGVyVGltZS8yLCAge29wYWNpdHk6IDI1NX0pXG4gICAgICAgICAgICAuZGVsYXkoc2hvd1RpbWUpXG4gICAgICAgICAgICAudG8oZXhpdFRpbWUsICB7b3BhY2l0eTogMH0pXG4gICAgICAgICAgICAuc3RhcnQoKTsgICAgICAgICAgICBcblxuICAgICAgICAvLyBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UucGxheVNmeChTaWNib1NvdW5kLlNmeE1lc3NhZ2UpO1xuICAgIH1cblxuXG4gICAgZm9yY2VIaWRlUmVzdWx0Tm90aSgpe1xuICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5yZXN1bHROb3RpU2tlbGV0b24ubm9kZSk7ICAgICBcbiAgICAgICAgdGhpcy5yZXN1bHROb3RpU2tlbGV0b24ubm9kZS5vcGFjaXR5ID0gMDtcbiAgICB9XG4gICAgLy8hU0VDVElPTlxuXG4gICAgb25EZXN0cm95KCl7XG4gICAgICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLnNtYWxsTm90aVJvb3QpO1xuICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5sYXJnZU5vdGlTa2VsZXRvbi5ub2RlKTsgICAgICAgIFxuICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5yZXN1bHROb3RpU2tlbGV0b24ubm9kZSk7ICAgICAgICBcbiAgICAgICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMucmVzdWx0Tm90aU1lc3NhZ2Uubm9kZSk7ICAgICAgICBcbiAgICB9XG59XG4iXX0=