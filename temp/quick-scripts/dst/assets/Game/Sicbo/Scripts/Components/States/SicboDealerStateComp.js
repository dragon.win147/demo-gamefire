
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/States/SicboDealerStateComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c0493/P1A5GR54lnExm4v/l', 'SicboDealerStateComp');
// Game/Sicbo_New/Scripts/Components/States/SicboDealerStateComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), State = _a.State, Status = _a.Status;
var SicboGameUtils_1 = require("../../RNGCommons/Utils/SicboGameUtils");
var SicboSkeletonUtils_1 = require("../../RNGCommons/Utils/SicboSkeletonUtils");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboDealerStateComp = /** @class */ (function (_super) {
    __extends(SicboDealerStateComp, _super);
    function SicboDealerStateComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.dealerSkeleton = null;
        _this.skins = ["default"];
        _this.idleArr = ["idle"];
        // private shakeAnimation = "shake";
        // private openAnimation = "open";
        _this.betAnimation = "bet";
        _this.status = Status.STATUS_UNSPECIFIED;
        return _this;
    }
    SicboDealerStateComp.prototype.onLoad = function () {
        this.nextIdleAnimation();
        this.setRandomSkin();
        // SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.openAnimation, false, 100);
    };
    SicboDealerStateComp.prototype.exitState = function (status) { };
    SicboDealerStateComp.prototype.updateState = function (state) { };
    SicboDealerStateComp.prototype.startState = function (state, totalDuration) {
        this.status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        switch (this.status) {
            // case Status.WAITING:                
            //     SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.shakeAnimation, false, elapseTime);
            //     SicboController.Instance.playSfx(SicboSound.SfxDealerShake);
            //     break;
            case Status.BETTING:
                SicboSkeletonUtils_1.default.setAnimation(this.dealerSkeleton, this.betAnimation, false, elapseTime);
                break;
            // case Status.SHOWING_RESULT: 
            //     SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.openAnimation, true);
            default:
                // SicboSkeletonUtils.setAnimation(this.dealerSkeleton, "Waiting Idle", false, elapseTime/1000);
                break;
        }
    };
    SicboDealerStateComp.prototype.update = function (dt) {
        if (this.isAnimationComplete() && this.status != Status.RESULTING)
            this.nextIdleAnimation();
    };
    SicboDealerStateComp.prototype.nextIdleAnimation = function () {
        var idx = SicboGameUtils_1.default.getRandomInt(this.idleArr.length, 0);
        SicboSkeletonUtils_1.default.setAnimation(this.dealerSkeleton, this.idleArr[idx]);
        //cc.log("Dealer Idle");
    };
    SicboDealerStateComp.prototype.isAnimationComplete = function () {
        var trackEntry = this.dealerSkeleton.getCurrent(0);
        // //cc.log("[track %s][isComplete %s]end.", trackEntry.trackIndex, trackEntry.isComplete());
        return trackEntry.isComplete();
    };
    SicboDealerStateComp.prototype.setRandomSkin = function () {
        var idx = SicboGameUtils_1.default.getRandomInt(this.skins.length, 0);
        this.dealerSkeleton.setSkin(this.skins[idx]);
    };
    // setMixAnimation (anim1, anim2, mixTime = 0.5) {
    //     this.dealerSkeleton.setMix(anim1, anim2, mixTime);
    //     this.dealerSkeleton.setMix(anim2, anim1, mixTime);
    // }
    SicboDealerStateComp.prototype.onHide = function () {
        this.nextIdleAnimation();
        this.setRandomSkin();
        // SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.openAnimation, false, 100);
    };
    __decorate([
        property(sp.Skeleton)
    ], SicboDealerStateComp.prototype, "dealerSkeleton", void 0);
    SicboDealerStateComp = __decorate([
        ccclass
    ], SicboDealerStateComp);
    return SicboDealerStateComp;
}(cc.Component));
exports.default = SicboDealerStateComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU3RhdGVzL1NpY2JvRGVhbGVyU3RhdGVDb21wLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR2xGLHdFQUFtRTtBQUU3RCxJQUFBLEtBRU0sNEJBQWtCLENBQUMsVUFBVSxFQUFFLEVBRHpDLEtBQUssV0FBQSxFQUNMLE1BQU0sWUFBbUMsQ0FBQztBQUM1Qyx3RUFBbUU7QUFDbkUsZ0ZBQTJFO0FBR3JFLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQWtELHdDQUFZO0lBQTlEO1FBQUEscUVBNEVDO1FBMUVHLG9CQUFjLEdBQWdCLElBQUksQ0FBQztRQUUzQixXQUFLLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNwQixhQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUUzQixvQ0FBb0M7UUFDcEMsa0NBQWtDO1FBQzFCLGtCQUFZLEdBQUcsS0FBSyxDQUFDO1FBRTdCLFlBQU0sR0FBRyxNQUFNLENBQUMsa0JBQWtCLENBQUM7O0lBaUV2QyxDQUFDO0lBaEVHLHFDQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsd0ZBQXdGO0lBQzVGLENBQUM7SUFFRCx3Q0FBUyxHQUFULFVBQVUsTUFBTSxJQUFTLENBQUM7SUFFMUIsMENBQVcsR0FBWCxVQUFZLEtBQWlDLElBQUUsQ0FBQztJQUVoRCx5Q0FBVSxHQUFWLFVBQVcsS0FBaUMsRUFBRSxhQUFxQjtRQUMvRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNoQyxJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLEdBQUMsSUFBSSxDQUFDO1FBQzNDLFFBQVEsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNqQix1Q0FBdUM7WUFDdkMsb0dBQW9HO1lBQ3BHLG1FQUFtRTtZQUNuRSxhQUFhO1lBRWIsS0FBSyxNQUFNLENBQUMsT0FBTztnQkFDZiw0QkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLEtBQUssRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDM0YsTUFBTTtZQUVWLCtCQUErQjtZQUMvQixzRkFBc0Y7WUFDdEY7Z0JBQ0ksZ0dBQWdHO2dCQUNoRyxNQUFNO1NBQ2I7SUFFTCxDQUFDO0lBRUQscUNBQU0sR0FBTixVQUFPLEVBQUU7UUFDTCxJQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUcsTUFBTSxDQUFDLFNBQVM7WUFDM0QsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDakMsQ0FBQztJQUVELGdEQUFpQixHQUFqQjtRQUNJLElBQUksR0FBRyxHQUFHLHdCQUFjLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlELDRCQUFrQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4RSx3QkFBd0I7SUFDNUIsQ0FBQztJQUVELGtEQUFtQixHQUFuQjtRQUNJLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBd0IsQ0FBQztRQUMxRSw2RkFBNkY7UUFDN0YsT0FBTyxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVELDRDQUFhLEdBQWI7UUFDSSxJQUFJLEdBQUcsR0FBRyx3QkFBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELGtEQUFrRDtJQUNsRCx5REFBeUQ7SUFDekQseURBQXlEO0lBQ3pELElBQUk7SUFFSixxQ0FBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLHdGQUF3RjtJQUM1RixDQUFDO0lBekVEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0VBQ2E7SUFGbEIsb0JBQW9CO1FBRHhDLE9BQU87T0FDYSxvQkFBb0IsQ0E0RXhDO0lBQUQsMkJBQUM7Q0E1RUQsQUE0RUMsQ0E1RWlELEVBQUUsQ0FBQyxTQUFTLEdBNEU3RDtrQkE1RW9CLG9CQUFvQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5cbmltcG9ydCBTaWNib01vZHVsZUFkYXB0ZXIgZnJvbSBcIi4uLy4uLy4uLy4uLy4uL1NpY2JvTW9kdWxlQWRhcHRlclwiO1xuXG5jb25zdCB7XG4gIFN0YXRlLFxuICBTdGF0dXN9ID0gU2ljYm9Nb2R1bGVBZGFwdGVyLmdldEFsbFJlZnMoKTtcbmltcG9ydCBTaWNib0dhbWVVdGlscyBmcm9tIFwiLi4vLi4vUk5HQ29tbW9ucy9VdGlscy9TaWNib0dhbWVVdGlsc1wiO1xuaW1wb3J0IFNpY2JvU2tlbGV0b25VdGlscyBmcm9tIFwiLi4vLi4vUk5HQ29tbW9ucy9VdGlscy9TaWNib1NrZWxldG9uVXRpbHNcIjtcbmltcG9ydCBJU2ljYm9TdGF0ZSBmcm9tIFwiLi9JU2ljYm9TdGF0ZVwiO1xuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvRGVhbGVyU3RhdGVDb21wIGV4dGVuZHMgY2MuQ29tcG9uZW50IGltcGxlbWVudHMgSVNpY2JvU3RhdGUge1xuICAgIEBwcm9wZXJ0eShzcC5Ta2VsZXRvbilcbiAgICBkZWFsZXJTa2VsZXRvbjogc3AuU2tlbGV0b24gPSBudWxsO1xuXG4gICAgcHJpdmF0ZSBza2lucyA9IFtcImRlZmF1bHRcIl07XG4gICAgcHJpdmF0ZSBpZGxlQXJyID0gW1wiaWRsZVwiXTtcblxuICAgIC8vIHByaXZhdGUgc2hha2VBbmltYXRpb24gPSBcInNoYWtlXCI7XG4gICAgLy8gcHJpdmF0ZSBvcGVuQW5pbWF0aW9uID0gXCJvcGVuXCI7XG4gICAgcHJpdmF0ZSBiZXRBbmltYXRpb24gPSBcImJldFwiO1xuICAgIFxuICAgIHN0YXR1cyA9IFN0YXR1cy5TVEFUVVNfVU5TUEVDSUZJRUQ7XG4gICAgb25Mb2FkKCl7XG4gICAgICAgIHRoaXMubmV4dElkbGVBbmltYXRpb24oKTtcbiAgICAgICAgdGhpcy5zZXRSYW5kb21Ta2luKCk7ICAgICAgICBcbiAgICAgICAgLy8gU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLmRlYWxlclNrZWxldG9uLCB0aGlzLm9wZW5BbmltYXRpb24sIGZhbHNlLCAxMDApO1xuICAgIH1cblxuICAgIGV4aXRTdGF0ZShzdGF0dXMpOiB2b2lkIHt9XG5cbiAgICB1cGRhdGVTdGF0ZShzdGF0ZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGF0ZT4pe31cblxuICAgIHN0YXJ0U3RhdGUoc3RhdGU6IEluc3RhbmNlVHlwZTx0eXBlb2YgU3RhdGU+LCB0b3RhbER1cmF0aW9uOiBudW1iZXIpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zdGF0dXMgPSBzdGF0ZS5nZXRTdGF0dXMoKTtcbiAgICAgICAgbGV0IGVsYXBzZVRpbWUgPSBzdGF0ZS5nZXRTdGFnZVRpbWUoKS8xMDAwO1xuICAgICAgICBzd2l0Y2ggKHRoaXMuc3RhdHVzKSB7XG4gICAgICAgICAgICAvLyBjYXNlIFN0YXR1cy5XQUlUSU5HOiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIC8vICAgICBTaWNib1NrZWxldG9uVXRpbHMuc2V0QW5pbWF0aW9uKHRoaXMuZGVhbGVyU2tlbGV0b24sIHRoaXMuc2hha2VBbmltYXRpb24sIGZhbHNlLCBlbGFwc2VUaW1lKTtcbiAgICAgICAgICAgIC8vICAgICBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2UucGxheVNmeChTaWNib1NvdW5kLlNmeERlYWxlclNoYWtlKTtcbiAgICAgICAgICAgIC8vICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBTdGF0dXMuQkVUVElORzpcbiAgICAgICAgICAgICAgICBTaWNib1NrZWxldG9uVXRpbHMuc2V0QW5pbWF0aW9uKHRoaXMuZGVhbGVyU2tlbGV0b24sIHRoaXMuYmV0QW5pbWF0aW9uLCBmYWxzZSwgZWxhcHNlVGltZSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIGNhc2UgU3RhdHVzLlNIT1dJTkdfUkVTVUxUOiBcbiAgICAgICAgICAgIC8vICAgICBTaWNib1NrZWxldG9uVXRpbHMuc2V0QW5pbWF0aW9uKHRoaXMuZGVhbGVyU2tlbGV0b24sIHRoaXMub3BlbkFuaW1hdGlvbiwgdHJ1ZSk7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIC8vIFNpY2JvU2tlbGV0b25VdGlscy5zZXRBbmltYXRpb24odGhpcy5kZWFsZXJTa2VsZXRvbiwgXCJXYWl0aW5nIElkbGVcIiwgZmFsc2UsIGVsYXBzZVRpbWUvMTAwMCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgfVxuXG4gICAgdXBkYXRlKGR0KXsgICAgICAgICAgICAgICAgXG4gICAgICAgIGlmKHRoaXMuaXNBbmltYXRpb25Db21wbGV0ZSgpICYmIHRoaXMuc3RhdHVzIT0gU3RhdHVzLlJFU1VMVElORykgXG4gICAgICAgICAgICB0aGlzLm5leHRJZGxlQW5pbWF0aW9uKCk7ICAgICAgICAgICAgXG4gICAgfVxuXG4gICAgbmV4dElkbGVBbmltYXRpb24oKXtcbiAgICAgICAgbGV0IGlkeCA9IFNpY2JvR2FtZVV0aWxzLmdldFJhbmRvbUludCh0aGlzLmlkbGVBcnIubGVuZ3RoLCAwKTtcbiAgICAgICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLmRlYWxlclNrZWxldG9uLCB0aGlzLmlkbGVBcnJbaWR4XSk7IFxuICAgICAgICAvL2NjLmxvZyhcIkRlYWxlciBJZGxlXCIpO1xuICAgIH1cblxuICAgIGlzQW5pbWF0aW9uQ29tcGxldGUoKXtcbiAgICAgICAgbGV0IHRyYWNrRW50cnkgPSB0aGlzLmRlYWxlclNrZWxldG9uLmdldEN1cnJlbnQoMCkgYXMgc3Auc3BpbmUuVHJhY2tFbnRyeTtcbiAgICAgICAgLy8gLy9jYy5sb2coXCJbdHJhY2sgJXNdW2lzQ29tcGxldGUgJXNdZW5kLlwiLCB0cmFja0VudHJ5LnRyYWNrSW5kZXgsIHRyYWNrRW50cnkuaXNDb21wbGV0ZSgpKTtcbiAgICAgICAgcmV0dXJuIHRyYWNrRW50cnkuaXNDb21wbGV0ZSgpO1xuICAgIH1cblxuICAgIHNldFJhbmRvbVNraW4oKXtcbiAgICAgICAgbGV0IGlkeCA9IFNpY2JvR2FtZVV0aWxzLmdldFJhbmRvbUludCh0aGlzLnNraW5zLmxlbmd0aCwgMCk7XG4gICAgICAgIHRoaXMuZGVhbGVyU2tlbGV0b24uc2V0U2tpbih0aGlzLnNraW5zW2lkeF0pO1xuICAgIH1cblxuICAgIC8vIHNldE1peEFuaW1hdGlvbiAoYW5pbTEsIGFuaW0yLCBtaXhUaW1lID0gMC41KSB7XG4gICAgLy8gICAgIHRoaXMuZGVhbGVyU2tlbGV0b24uc2V0TWl4KGFuaW0xLCBhbmltMiwgbWl4VGltZSk7XG4gICAgLy8gICAgIHRoaXMuZGVhbGVyU2tlbGV0b24uc2V0TWl4KGFuaW0yLCBhbmltMSwgbWl4VGltZSk7XG4gICAgLy8gfVxuXG4gICAgb25IaWRlKCk6IHZvaWQgeyAgICBcbiAgICAgICAgdGhpcy5uZXh0SWRsZUFuaW1hdGlvbigpO1xuICAgICAgICB0aGlzLnNldFJhbmRvbVNraW4oKTsgICAgICAgIFxuICAgICAgICAvLyBTaWNib1NrZWxldG9uVXRpbHMuc2V0QW5pbWF0aW9uKHRoaXMuZGVhbGVyU2tlbGV0b24sIHRoaXMub3BlbkFuaW1hdGlvbiwgZmFsc2UsIDEwMCk7XG4gICAgfVxufVxuIl19