
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboChangeTabComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9af5cFBdOlLlo4u/K26s3g4', 'SicboChangeTabComp');
// Game/Sicbo_New/Scripts/Components/SicboChangeTabComp.js

"use strict";

var _SicboSoundManager = _interopRequireDefault(require("../Managers/SicboSoundManager"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var customLog = function customLog(logString) {
  cc.log("SicboChangeTabComponent - " + logString);
};

cc.Class({
  "extends": cc.Component,
  properties: {
    timeMinute: 0,
    closeGameEvent: {
      "default": [],
      type: cc.Component.EventHandler
    }
  },
  onDestroy: function onDestroy() {
    cc.game.off(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
    cc.game.off(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    this.timeEnterBg = 0;
    cc.game.on(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
    cc.game.on(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
  },
  onChangeTabHide: function onChangeTabHide() {
    var onmessage = function onmessage(g) {
      var a,
          b,
          c,
          d = Date.now(),
          e = g.data[0],
          f = function f() {
        clearTimeout(a);
        b = Date.now();
        c = b - d;
        d = b;
        postMessage(c);
        a = setTimeout(f, e);
      };

      a = setTimeout(f, e);
    };

    var onmessageFunction = "onmessage = " + onmessage.toString().replace(/\n/g, "");

    if (!cc.sys.isNative && !window.webWorker) {
      this.checkTimerEnterBg(); // cc.game.pause();

      this.onPauseMusic();
      window.webWorker = new Worker(URL.createObjectURL(new Blob([onmessageFunction], {
        type: "text/javascript"
      })));

      window.webWorker.onmessage = function (event) {
        cc.director.mainLoop(); // TweenMax.ticker.tick()
      };

      window.webWorker.postMessage([Math.floor(cc.game.getFrameRate())]);
    }
  },
  onChangeTabShow: function onChangeTabShow() {
    if (!cc.sys.isNative && window.webWorker) {
      cc.game.resume();
      this.onResumeMusic();
      this.timeEnterBg = 0;
      clearInterval(this.timeInterval);
      window.webWorker.terminate();
      delete window.webWorker;
    }
  },
  onPauseMusic: function onPauseMusic() {
    _SicboSoundManager["default"].getInstance().pauseAllEffects();

    _SicboSoundManager["default"].getInstance().setMusicVolume(0, false);

    _SicboSoundManager["default"].getInstance().setSfxVolume(0, false);
  },
  onResumeMusic: function onResumeMusic() {
    _SicboSoundManager["default"].getInstance().resumeAllEffects();

    _SicboSoundManager["default"].getInstance().loadConfigFromFromStorage();
  },
  checkTimerEnterBg: function checkTimerEnterBg() {
    if (this.timeMinute <= 0) return;
    this.timeInterval = setInterval(function () {
      this.timeEnterBg++;

      if (this.timeEnterBg >= this.timeMinute * 60) {
        this.closeGameEvent.forEach(function (element) {
          element.emit();
        });
        this.timeEnterBg = 0;
        clearInterval(this.timeInterval);
      }
    }.bind(this), 1000);
  },
  start: function start() {}
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9DaGFuZ2VUYWJDb21wLmpzIl0sIm5hbWVzIjpbImN1c3RvbUxvZyIsImxvZ1N0cmluZyIsImNjIiwibG9nIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwidGltZU1pbnV0ZSIsImNsb3NlR2FtZUV2ZW50IiwidHlwZSIsIkV2ZW50SGFuZGxlciIsIm9uRGVzdHJveSIsImdhbWUiLCJvZmYiLCJFVkVOVF9ISURFIiwib25DaGFuZ2VUYWJIaWRlIiwiRVZFTlRfU0hPVyIsIm9uQ2hhbmdlVGFiU2hvdyIsIm9uTG9hZCIsInRpbWVFbnRlckJnIiwib24iLCJvbm1lc3NhZ2UiLCJnIiwiYSIsImIiLCJjIiwiZCIsIkRhdGUiLCJub3ciLCJlIiwiZGF0YSIsImYiLCJjbGVhclRpbWVvdXQiLCJwb3N0TWVzc2FnZSIsInNldFRpbWVvdXQiLCJvbm1lc3NhZ2VGdW5jdGlvbiIsInRvU3RyaW5nIiwicmVwbGFjZSIsInN5cyIsImlzTmF0aXZlIiwid2luZG93Iiwid2ViV29ya2VyIiwiY2hlY2tUaW1lckVudGVyQmciLCJvblBhdXNlTXVzaWMiLCJXb3JrZXIiLCJVUkwiLCJjcmVhdGVPYmplY3RVUkwiLCJCbG9iIiwiZXZlbnQiLCJkaXJlY3RvciIsIm1haW5Mb29wIiwiTWF0aCIsImZsb29yIiwiZ2V0RnJhbWVSYXRlIiwicmVzdW1lIiwib25SZXN1bWVNdXNpYyIsImNsZWFySW50ZXJ2YWwiLCJ0aW1lSW50ZXJ2YWwiLCJ0ZXJtaW5hdGUiLCJTaWNib1NvdW5kTWFuYWdlciIsImdldEluc3RhbmNlIiwicGF1c2VBbGxFZmZlY3RzIiwic2V0TXVzaWNWb2x1bWUiLCJzZXRTZnhWb2x1bWUiLCJyZXN1bWVBbGxFZmZlY3RzIiwibG9hZENvbmZpZ0Zyb21Gcm9tU3RvcmFnZSIsInNldEludGVydmFsIiwiZm9yRWFjaCIsImVsZW1lbnQiLCJlbWl0IiwiYmluZCIsInN0YXJ0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUlBOzs7O0FBSkEsSUFBSUEsU0FBUyxHQUFHLFNBQVpBLFNBQVksQ0FBVUMsU0FBVixFQUFxQjtBQUNuQ0MsRUFBQUEsRUFBRSxDQUFDQyxHQUFILENBQU8sK0JBQStCRixTQUF0QztBQUNELENBRkQ7O0FBS0FDLEVBQUUsQ0FBQ0UsS0FBSCxDQUFTO0FBQ1AsYUFBU0YsRUFBRSxDQUFDRyxTQURMO0FBR1BDLEVBQUFBLFVBQVUsRUFBRTtBQUNWQyxJQUFBQSxVQUFVLEVBQUUsQ0FERjtBQUVWQyxJQUFBQSxjQUFjLEVBQUU7QUFBRSxpQkFBUyxFQUFYO0FBQWVDLE1BQUFBLElBQUksRUFBRVAsRUFBRSxDQUFDRyxTQUFILENBQWFLO0FBQWxDO0FBRk4sR0FITDtBQVFQQyxFQUFBQSxTQVJPLHVCQVFLO0FBQ1ZULElBQUFBLEVBQUUsQ0FBQ1UsSUFBSCxDQUFRQyxHQUFSLENBQVlYLEVBQUUsQ0FBQ1UsSUFBSCxDQUFRRSxVQUFwQixFQUFnQyxLQUFLQyxlQUFyQyxFQUFzRCxJQUF0RDtBQUNBYixJQUFBQSxFQUFFLENBQUNVLElBQUgsQ0FBUUMsR0FBUixDQUFZWCxFQUFFLENBQUNVLElBQUgsQ0FBUUksVUFBcEIsRUFBZ0MsS0FBS0MsZUFBckMsRUFBc0QsSUFBdEQ7QUFDRCxHQVhNO0FBWVA7QUFDQUMsRUFBQUEsTUFiTyxvQkFhRTtBQUNQLFNBQUtDLFdBQUwsR0FBbUIsQ0FBbkI7QUFFQWpCLElBQUFBLEVBQUUsQ0FBQ1UsSUFBSCxDQUFRUSxFQUFSLENBQVdsQixFQUFFLENBQUNVLElBQUgsQ0FBUUUsVUFBbkIsRUFBK0IsS0FBS0MsZUFBcEMsRUFBcUQsSUFBckQ7QUFDQWIsSUFBQUEsRUFBRSxDQUFDVSxJQUFILENBQVFRLEVBQVIsQ0FBV2xCLEVBQUUsQ0FBQ1UsSUFBSCxDQUFRSSxVQUFuQixFQUErQixLQUFLQyxlQUFwQyxFQUFxRCxJQUFyRDtBQUNELEdBbEJNO0FBcUJQRixFQUFBQSxlQXJCTyw2QkFxQlc7QUFDaEIsUUFBSU0sU0FBUyxHQUFHLFNBQVpBLFNBQVksQ0FBVUMsQ0FBVixFQUFhO0FBQzNCLFVBQUlDLENBQUo7QUFBQSxVQUNFQyxDQURGO0FBQUEsVUFFRUMsQ0FGRjtBQUFBLFVBR0VDLENBQUMsR0FBR0MsSUFBSSxDQUFDQyxHQUFMLEVBSE47QUFBQSxVQUlFQyxDQUFDLEdBQUdQLENBQUMsQ0FBQ1EsSUFBRixDQUFPLENBQVAsQ0FKTjtBQUFBLFVBS0VDLENBQUMsR0FBRyxTQUFKQSxDQUFJLEdBQVk7QUFDZEMsUUFBQUEsWUFBWSxDQUFDVCxDQUFELENBQVo7QUFDQUMsUUFBQUEsQ0FBQyxHQUFHRyxJQUFJLENBQUNDLEdBQUwsRUFBSjtBQUNBSCxRQUFBQSxDQUFDLEdBQUdELENBQUMsR0FBR0UsQ0FBUjtBQUNBQSxRQUFBQSxDQUFDLEdBQUdGLENBQUo7QUFDQVMsUUFBQUEsV0FBVyxDQUFDUixDQUFELENBQVg7QUFDQUYsUUFBQUEsQ0FBQyxHQUFHVyxVQUFVLENBQUNILENBQUQsRUFBSUYsQ0FBSixDQUFkO0FBQ0QsT0FaSDs7QUFhQU4sTUFBQUEsQ0FBQyxHQUFHVyxVQUFVLENBQUNILENBQUQsRUFBSUYsQ0FBSixDQUFkO0FBQ0QsS0FmRDs7QUFnQkEsUUFBSU0saUJBQWlCLG9CQUFrQmQsU0FBUyxDQUFDZSxRQUFWLEdBQXFCQyxPQUFyQixDQUE2QixLQUE3QixFQUFvQyxFQUFwQyxDQUF2Qzs7QUFDRSxRQUFJLENBQUNuQyxFQUFFLENBQUNvQyxHQUFILENBQU9DLFFBQVIsSUFBb0IsQ0FBQ0MsTUFBTSxDQUFDQyxTQUFoQyxFQUEyQztBQUN6QyxXQUFLQyxpQkFBTCxHQUR5QyxDQUV6Qzs7QUFDQSxXQUFLQyxZQUFMO0FBQ0FILE1BQUFBLE1BQU0sQ0FBQ0MsU0FBUCxHQUFtQixJQUFJRyxNQUFKLENBQVdDLEdBQUcsQ0FBQ0MsZUFBSixDQUFvQixJQUFJQyxJQUFKLENBQVMsQ0FBQ1osaUJBQUQsQ0FBVCxFQUE4QjtBQUFFMUIsUUFBQUEsSUFBSSxFQUFFO0FBQVIsT0FBOUIsQ0FBcEIsQ0FBWCxDQUFuQjs7QUFDQStCLE1BQUFBLE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQnBCLFNBQWpCLEdBQTZCLFVBQVUyQixLQUFWLEVBQWlCO0FBQzVDOUMsUUFBQUEsRUFBRSxDQUFDK0MsUUFBSCxDQUFZQyxRQUFaLEdBRDRDLENBRTVDO0FBQ0QsT0FIRDs7QUFJQVYsTUFBQUEsTUFBTSxDQUFDQyxTQUFQLENBQWlCUixXQUFqQixDQUE2QixDQUFDa0IsSUFBSSxDQUFDQyxLQUFMLENBQVdsRCxFQUFFLENBQUNVLElBQUgsQ0FBUXlDLFlBQVIsRUFBWCxDQUFELENBQTdCO0FBQ0Q7QUFDRixHQWxESTtBQW9EUHBDLEVBQUFBLGVBcERPLDZCQW9EVztBQUNoQixRQUFJLENBQUNmLEVBQUUsQ0FBQ29DLEdBQUgsQ0FBT0MsUUFBUixJQUFvQkMsTUFBTSxDQUFDQyxTQUEvQixFQUEwQztBQUN4Q3ZDLE1BQUFBLEVBQUUsQ0FBQ1UsSUFBSCxDQUFRMEMsTUFBUjtBQUNBLFdBQUtDLGFBQUw7QUFDQSxXQUFLcEMsV0FBTCxHQUFtQixDQUFuQjtBQUNBcUMsTUFBQUEsYUFBYSxDQUFDLEtBQUtDLFlBQU4sQ0FBYjtBQUNBakIsTUFBQUEsTUFBTSxDQUFDQyxTQUFQLENBQWlCaUIsU0FBakI7QUFDQSxhQUFPbEIsTUFBTSxDQUFDQyxTQUFkO0FBQ0Q7QUFDRixHQTdETTtBQStEUEUsRUFBQUEsWUFBWSxFQUFFLHdCQUFZO0FBQ3hCZ0Isa0NBQWtCQyxXQUFsQixHQUFnQ0MsZUFBaEM7O0FBQ0FGLGtDQUFrQkMsV0FBbEIsR0FBZ0NFLGNBQWhDLENBQStDLENBQS9DLEVBQWtELEtBQWxEOztBQUNBSCxrQ0FBa0JDLFdBQWxCLEdBQWdDRyxZQUFoQyxDQUE2QyxDQUE3QyxFQUFnRCxLQUFoRDtBQUNELEdBbkVNO0FBcUVQUixFQUFBQSxhQUFhLEVBQUUseUJBQVk7QUFDekJJLGtDQUFrQkMsV0FBbEIsR0FBZ0NJLGdCQUFoQzs7QUFDQUwsa0NBQWtCQyxXQUFsQixHQUFnQ0sseUJBQWhDO0FBQ0QsR0F4RU07QUEwRVB2QixFQUFBQSxpQkFBaUIsRUFBRSw2QkFBWTtBQUM3QixRQUFJLEtBQUtuQyxVQUFMLElBQW1CLENBQXZCLEVBQTBCO0FBQzFCLFNBQUtrRCxZQUFMLEdBQW9CUyxXQUFXLENBQzdCLFlBQVk7QUFDVixXQUFLL0MsV0FBTDs7QUFDQSxVQUFJLEtBQUtBLFdBQUwsSUFBb0IsS0FBS1osVUFBTCxHQUFrQixFQUExQyxFQUE4QztBQUM1QyxhQUFLQyxjQUFMLENBQW9CMkQsT0FBcEIsQ0FBNEIsVUFBQ0MsT0FBRCxFQUFhO0FBQ3ZDQSxVQUFBQSxPQUFPLENBQUNDLElBQVI7QUFDRCxTQUZEO0FBR0EsYUFBS2xELFdBQUwsR0FBbUIsQ0FBbkI7QUFDQXFDLFFBQUFBLGFBQWEsQ0FBQyxLQUFLQyxZQUFOLENBQWI7QUFDRDtBQUNGLEtBVEQsQ0FTRWEsSUFURixDQVNPLElBVFAsQ0FENkIsRUFXN0IsSUFYNkIsQ0FBL0I7QUFhRCxHQXpGTTtBQTJGUEMsRUFBQUEsS0EzRk8sbUJBMkZDLENBQUU7QUEzRkgsQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsidmFyIGN1c3RvbUxvZyA9IGZ1bmN0aW9uIChsb2dTdHJpbmcpIHtcbiAgY2MubG9nKFwiU2ljYm9DaGFuZ2VUYWJDb21wb25lbnQgLSBcIiArIGxvZ1N0cmluZyk7XG59O1xuXG5pbXBvcnQgU2ljYm9Tb3VuZE1hbmFnZXIgZnJvbSBcIi4uL01hbmFnZXJzL1NpY2JvU291bmRNYW5hZ2VyXCJcbmNjLkNsYXNzKHtcbiAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gIHByb3BlcnRpZXM6IHtcbiAgICB0aW1lTWludXRlOiAwLFxuICAgIGNsb3NlR2FtZUV2ZW50OiB7IGRlZmF1bHQ6IFtdLCB0eXBlOiBjYy5Db21wb25lbnQuRXZlbnRIYW5kbGVyIH0sXG4gIH0sXG5cbiAgb25EZXN0cm95KCkge1xuICAgIGNjLmdhbWUub2ZmKGNjLmdhbWUuRVZFTlRfSElERSwgdGhpcy5vbkNoYW5nZVRhYkhpZGUsIHRoaXMpO1xuICAgIGNjLmdhbWUub2ZmKGNjLmdhbWUuRVZFTlRfU0hPVywgdGhpcy5vbkNoYW5nZVRhYlNob3csIHRoaXMpO1xuICB9LFxuICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcbiAgb25Mb2FkKCkge1xuICAgIHRoaXMudGltZUVudGVyQmcgPSAwO1xuICAgIFxuICAgIGNjLmdhbWUub24oY2MuZ2FtZS5FVkVOVF9ISURFLCB0aGlzLm9uQ2hhbmdlVGFiSGlkZSwgdGhpcyk7XG4gICAgY2MuZ2FtZS5vbihjYy5nYW1lLkVWRU5UX1NIT1csIHRoaXMub25DaGFuZ2VUYWJTaG93LCB0aGlzKTtcbiAgfSxcblxuXG4gIG9uQ2hhbmdlVGFiSGlkZSgpIHtcbiAgICBsZXQgb25tZXNzYWdlID0gZnVuY3Rpb24gKGcpIHtcbiAgICAgIGxldCBhLFxuICAgICAgICBiLFxuICAgICAgICBjLFxuICAgICAgICBkID0gRGF0ZS5ub3coKSxcbiAgICAgICAgZSA9IGcuZGF0YVswXSxcbiAgICAgICAgZiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBjbGVhclRpbWVvdXQoYSk7XG4gICAgICAgICAgYiA9IERhdGUubm93KCk7XG4gICAgICAgICAgYyA9IGIgLSBkO1xuICAgICAgICAgIGQgPSBiO1xuICAgICAgICAgIHBvc3RNZXNzYWdlKGMpO1xuICAgICAgICAgIGEgPSBzZXRUaW1lb3V0KGYsIGUpO1xuICAgICAgICB9O1xuICAgICAgYSA9IHNldFRpbWVvdXQoZiwgZSk7XG4gICAgfTtcbiAgICBsZXQgb25tZXNzYWdlRnVuY3Rpb24gPSBgb25tZXNzYWdlID0gJHtvbm1lc3NhZ2UudG9TdHJpbmcoKS5yZXBsYWNlKC9cXG4vZywgXCJcIil9YDtcbiAgICAgIGlmICghY2Muc3lzLmlzTmF0aXZlICYmICF3aW5kb3cud2ViV29ya2VyKSB7XG4gICAgICAgIHRoaXMuY2hlY2tUaW1lckVudGVyQmcoKTtcbiAgICAgICAgLy8gY2MuZ2FtZS5wYXVzZSgpO1xuICAgICAgICB0aGlzLm9uUGF1c2VNdXNpYygpO1xuICAgICAgICB3aW5kb3cud2ViV29ya2VyID0gbmV3IFdvcmtlcihVUkwuY3JlYXRlT2JqZWN0VVJMKG5ldyBCbG9iKFtvbm1lc3NhZ2VGdW5jdGlvbl0sIHsgdHlwZTogXCJ0ZXh0L2phdmFzY3JpcHRcIiB9KSkpO1xuICAgICAgICB3aW5kb3cud2ViV29ya2VyLm9ubWVzc2FnZSA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgIGNjLmRpcmVjdG9yLm1haW5Mb29wKCk7XG4gICAgICAgICAgLy8gVHdlZW5NYXgudGlja2VyLnRpY2soKVxuICAgICAgICB9O1xuICAgICAgICB3aW5kb3cud2ViV29ya2VyLnBvc3RNZXNzYWdlKFtNYXRoLmZsb29yKGNjLmdhbWUuZ2V0RnJhbWVSYXRlKCkpXSk7XG4gICAgICB9XG4gICAgfSxcblxuICBvbkNoYW5nZVRhYlNob3coKSB7XG4gICAgaWYgKCFjYy5zeXMuaXNOYXRpdmUgJiYgd2luZG93LndlYldvcmtlcikge1xuICAgICAgY2MuZ2FtZS5yZXN1bWUoKTtcbiAgICAgIHRoaXMub25SZXN1bWVNdXNpYygpO1xuICAgICAgdGhpcy50aW1lRW50ZXJCZyA9IDA7XG4gICAgICBjbGVhckludGVydmFsKHRoaXMudGltZUludGVydmFsKTtcbiAgICAgIHdpbmRvdy53ZWJXb3JrZXIudGVybWluYXRlKCk7XG4gICAgICBkZWxldGUgd2luZG93LndlYldvcmtlcjtcbiAgICB9XG4gIH0sXG5cbiAgb25QYXVzZU11c2ljOiBmdW5jdGlvbiAoKSB7XG4gICAgU2ljYm9Tb3VuZE1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5wYXVzZUFsbEVmZmVjdHMoKTtcbiAgICBTaWNib1NvdW5kTWFuYWdlci5nZXRJbnN0YW5jZSgpLnNldE11c2ljVm9sdW1lKDAsIGZhbHNlKTtcbiAgICBTaWNib1NvdW5kTWFuYWdlci5nZXRJbnN0YW5jZSgpLnNldFNmeFZvbHVtZSgwLCBmYWxzZSk7XG4gIH0sXG5cbiAgb25SZXN1bWVNdXNpYzogZnVuY3Rpb24gKCkge1xuICAgIFNpY2JvU291bmRNYW5hZ2VyLmdldEluc3RhbmNlKCkucmVzdW1lQWxsRWZmZWN0cygpO1xuICAgIFNpY2JvU291bmRNYW5hZ2VyLmdldEluc3RhbmNlKCkubG9hZENvbmZpZ0Zyb21Gcm9tU3RvcmFnZSgpO1xuICB9LFxuXG4gIGNoZWNrVGltZXJFbnRlckJnOiBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHRoaXMudGltZU1pbnV0ZSA8PSAwKSByZXR1cm47XG4gICAgdGhpcy50aW1lSW50ZXJ2YWwgPSBzZXRJbnRlcnZhbChcbiAgICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy50aW1lRW50ZXJCZysrO1xuICAgICAgICBpZiAodGhpcy50aW1lRW50ZXJCZyA+PSB0aGlzLnRpbWVNaW51dGUgKiA2MCkge1xuICAgICAgICAgIHRoaXMuY2xvc2VHYW1lRXZlbnQuZm9yRWFjaCgoZWxlbWVudCkgPT4ge1xuICAgICAgICAgICAgZWxlbWVudC5lbWl0KCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy50aW1lRW50ZXJCZyA9IDA7XG4gICAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLnRpbWVJbnRlcnZhbCk7XG4gICAgICAgIH1cbiAgICAgIH0uYmluZCh0aGlzKSxcbiAgICAgIDEwMDBcbiAgICApO1xuICB9LFxuXG4gIHN0YXJ0KCkge30sXG59KTtcbiJdfQ==