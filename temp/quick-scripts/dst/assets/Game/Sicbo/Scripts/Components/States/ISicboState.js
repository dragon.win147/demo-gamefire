
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/States/ISicboState.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cf7ebK9S1FAppbc28n3QcGd', 'ISicboState');
// Game/Sicbo_New/Scripts/Components/States/ISicboState.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var State = SicboModuleAdapter_1.default.getAllRefs().State;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU3RhdGVzL0lTaWNib1N0YXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0Esd0VBQW1FO0FBR2pFLElBQUEsS0FBSyxHQUFJLDRCQUFrQixDQUFDLFVBQVUsRUFBRSxNQUFuQyxDQUFvQyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IFNpY2JvTW9kdWxlQWRhcHRlciBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vU2ljYm9Nb2R1bGVBZGFwdGVyXCI7XG5cbmNvbnN0IHtcbiAgU3RhdGV9ID0gU2ljYm9Nb2R1bGVBZGFwdGVyLmdldEFsbFJlZnMoKTtcblxuXG5leHBvcnQgZGVmYXVsdCBpbnRlcmZhY2UgSVNpY2JvU3RhdGUge1xuICAgIGV4aXRTdGF0ZShzdGF0dXMpOnZvaWQ7XG4gICAgc3RhcnRTdGF0ZShzdGF0ZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGF0ZT4sIHRvdGFsRHVyYXRpb246IG51bWJlcik6dm9pZDtcbiAgICB1cGRhdGVTdGF0ZShzdGF0ZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGF0ZT4pOnZvaWQ7XG4gICAgb25IaWRlKCk6dm9pZDtcbiAgfVxuICAiXX0=