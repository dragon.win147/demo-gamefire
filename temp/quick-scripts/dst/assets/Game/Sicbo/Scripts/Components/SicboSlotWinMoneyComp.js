
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboSlotWinMoneyComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f89f30q9vVLVa7ysvZpvj9X', 'SicboSlotWinMoneyComp');
// Game/Sicbo_New/Scripts/Components/SicboSlotWinMoneyComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboMoneyFormatComp_1 = require("../RNGCommons/SicboMoneyFormatComp");
var SicboSkeletonUtils_1 = require("../RNGCommons/Utils/SicboSkeletonUtils");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboSlotWinMoneyComp = /** @class */ (function (_super) {
    __extends(SicboSlotWinMoneyComp, _super);
    function SicboSlotWinMoneyComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.animation = null;
        _this.money = null;
        _this.showAnimation = "win_in";
        _this.idleAnimation = "win_loop";
        _this.hideAnimation = "win_out";
        _this.inTime = 0;
        _this.idleTime = .5;
        _this.outTime = 5.5 - 1;
        _this.elapseTime = 0;
        _this.isShow = false;
        _this.isIdle = false;
        _this.isHide = false;
        return _this;
    }
    SicboSlotWinMoneyComp.prototype.show = function (money, elapseTime) {
        if (elapseTime === void 0) { elapseTime = 0; }
        this.node.opacity = 0;
        // //cc.log("DKM", elapseTime)    ;
        this.money.setMoney(money);
        this.elapseTime = elapseTime;
    };
    SicboSlotWinMoneyComp.prototype.hide = function () {
        this.node.opacity = 255;
        SicboSkeletonUtils_1.default.setAnimation(this.animation, this.hideAnimation, false, this.elapseTime - this.outTime);
        this.isHide = true;
    };
    SicboSlotWinMoneyComp.prototype.update = function (dt) {
        this.elapseTime += dt;
        if (this.elapseTime > this.outTime && this.isHide == false) {
            this.node.opacity = 255;
            SicboSkeletonUtils_1.default.setAnimation(this.animation, this.hideAnimation, false, this.elapseTime - this.outTime);
            this.isHide = true;
        }
        else if (this.elapseTime > this.idleTime && this.isIdle == false) {
            this.node.opacity = 255;
            SicboSkeletonUtils_1.default.setAnimation(this.animation, this.idleAnimation, true, this.elapseTime - this.idleTime);
            this.isIdle = true;
        }
        else if (this.elapseTime > this.inTime && this.isShow == false) {
            this.node.opacity = 255;
            SicboSkeletonUtils_1.default.setAnimation(this.animation, this.showAnimation, false, this.elapseTime - this.inTime);
            this.isShow = true;
        }
        // if(this.elapseTime> this.outTime + 1) this.node.destroy();
    };
    __decorate([
        property(sp.Skeleton)
    ], SicboSlotWinMoneyComp.prototype, "animation", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboSlotWinMoneyComp.prototype, "money", void 0);
    SicboSlotWinMoneyComp = __decorate([
        ccclass
    ], SicboSlotWinMoneyComp);
    return SicboSlotWinMoneyComp;
}(cc.Component));
exports.default = SicboSlotWinMoneyComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9TbG90V2luTW9uZXlDb21wLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWxGLDJFQUFzRTtBQUN0RSw2RUFBd0U7QUFFbEUsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBbUQseUNBQVk7SUFBL0Q7UUFBQSxxRUFxREM7UUFuREcsZUFBUyxHQUFnQixJQUFJLENBQUM7UUFHOUIsV0FBSyxHQUF5QixJQUFJLENBQUM7UUFFbkMsbUJBQWEsR0FBRyxRQUFRLENBQUM7UUFDekIsbUJBQWEsR0FBRyxVQUFVLENBQUM7UUFDM0IsbUJBQWEsR0FBRyxTQUFTLENBQUM7UUFFbEIsWUFBTSxHQUFHLENBQUMsQ0FBQztRQUNYLGNBQVEsR0FBRyxFQUFFLENBQUM7UUFDZCxhQUFPLEdBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztRQUVuQixnQkFBVSxHQUFHLENBQUMsQ0FBQztRQUV2QixZQUFNLEdBQVcsS0FBSyxDQUFDO1FBQ3ZCLFlBQU0sR0FBVyxLQUFLLENBQUM7UUFDdkIsWUFBTSxHQUFXLEtBQUssQ0FBQzs7SUFrQzNCLENBQUM7SUFoQ0csb0NBQUksR0FBSixVQUFLLEtBQWEsRUFBRSxVQUFzQjtRQUF0QiwyQkFBQSxFQUFBLGNBQXNCO1FBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUN0QixtQ0FBbUM7UUFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7SUFDakMsQ0FBQztJQUVELG9DQUFJLEdBQUo7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDeEIsNEJBQWtCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0csSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUVELHNDQUFNLEdBQU4sVUFBTyxFQUFFO1FBQ0wsSUFBSSxDQUFDLFVBQVUsSUFBRSxFQUFFLENBQUM7UUFFcEIsSUFBRyxJQUFJLENBQUMsVUFBVSxHQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUM7WUFDcEQsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ3hCLDRCQUFrQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzNHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO2FBQUssSUFBRyxJQUFJLENBQUMsVUFBVSxHQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUM7WUFDM0QsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ3hCLDRCQUFrQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzNHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO2FBQUssSUFBRyxJQUFJLENBQUMsVUFBVSxHQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUM7WUFDekQsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ3hCLDRCQUFrQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzFHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO1FBRUQsNkRBQTZEO0lBQ2pFLENBQUM7SUFsREQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQzs0REFDUTtJQUc5QjtRQURDLFFBQVEsQ0FBQyw4QkFBb0IsQ0FBQzt3REFDSTtJQUxsQixxQkFBcUI7UUFEekMsT0FBTztPQUNhLHFCQUFxQixDQXFEekM7SUFBRCw0QkFBQztDQXJERCxBQXFEQyxDQXJEa0QsRUFBRSxDQUFDLFNBQVMsR0FxRDlEO2tCQXJEb0IscUJBQXFCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBTaWNib01vbmV5Rm9ybWF0Q29tcCBmcm9tIFwiLi4vUk5HQ29tbW9ucy9TaWNib01vbmV5Rm9ybWF0Q29tcFwiO1xuaW1wb3J0IFNpY2JvU2tlbGV0b25VdGlscyBmcm9tIFwiLi4vUk5HQ29tbW9ucy9VdGlscy9TaWNib1NrZWxldG9uVXRpbHNcIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1Nsb3RXaW5Nb25leUNvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIEBwcm9wZXJ0eShzcC5Ta2VsZXRvbilcbiAgICBhbmltYXRpb246IHNwLlNrZWxldG9uID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShTaWNib01vbmV5Rm9ybWF0Q29tcClcbiAgICBtb25leTogU2ljYm9Nb25leUZvcm1hdENvbXAgPSBudWxsO1xuXG4gICAgc2hvd0FuaW1hdGlvbiA9IFwid2luX2luXCI7XG4gICAgaWRsZUFuaW1hdGlvbiA9IFwid2luX2xvb3BcIjtcbiAgICBoaWRlQW5pbWF0aW9uID0gXCJ3aW5fb3V0XCI7XG5cbiAgICBwcml2YXRlIGluVGltZSA9IDA7XG4gICAgcHJpdmF0ZSBpZGxlVGltZSA9IC41O1xuICAgIHByaXZhdGUgb3V0VGltZSA9ICA1LjUgLSAxO1xuXG4gICAgcHJpdmF0ZSBlbGFwc2VUaW1lID0gMDtcblxuICAgIGlzU2hvdzpib29sZWFuID0gZmFsc2U7XG4gICAgaXNJZGxlOmJvb2xlYW4gPSBmYWxzZTtcbiAgICBpc0hpZGU6Ym9vbGVhbiA9IGZhbHNlOyAgIFxuXG4gICAgc2hvdyhtb25leTogbnVtYmVyLCBlbGFwc2VUaW1lOiBudW1iZXIgPSAwKXsgICBcbiAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPSAwO1xuICAgICAgICAvLyAvL2NjLmxvZyhcIkRLTVwiLCBlbGFwc2VUaW1lKSAgICA7XG4gICAgICAgIHRoaXMubW9uZXkuc2V0TW9uZXkobW9uZXkpO1xuICAgICAgICB0aGlzLmVsYXBzZVRpbWUgPSBlbGFwc2VUaW1lO1xuICAgIH1cblxuICAgIGhpZGUoKXtcbiAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPSAyNTU7XG4gICAgICAgIFNpY2JvU2tlbGV0b25VdGlscy5zZXRBbmltYXRpb24odGhpcy5hbmltYXRpb24sIHRoaXMuaGlkZUFuaW1hdGlvbiwgZmFsc2UsIHRoaXMuZWxhcHNlVGltZSAtIHRoaXMub3V0VGltZSk7XG4gICAgICAgIHRoaXMuaXNIaWRlID0gdHJ1ZTtcbiAgICB9XG5cbiAgICB1cGRhdGUoZHQpe1xuICAgICAgICB0aGlzLmVsYXBzZVRpbWUrPWR0O1xuICAgICAgICBcbiAgICAgICAgaWYodGhpcy5lbGFwc2VUaW1lPnRoaXMub3V0VGltZSAmJiB0aGlzLmlzSGlkZSA9PSBmYWxzZSl7XG4gICAgICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDI1NTtcbiAgICAgICAgICAgIFNpY2JvU2tlbGV0b25VdGlscy5zZXRBbmltYXRpb24odGhpcy5hbmltYXRpb24sIHRoaXMuaGlkZUFuaW1hdGlvbiwgZmFsc2UsIHRoaXMuZWxhcHNlVGltZSAtIHRoaXMub3V0VGltZSk7XG4gICAgICAgICAgICB0aGlzLmlzSGlkZSA9IHRydWU7XG4gICAgICAgIH1lbHNlIGlmKHRoaXMuZWxhcHNlVGltZT50aGlzLmlkbGVUaW1lICYmIHRoaXMuaXNJZGxlID09IGZhbHNlKXtcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMjU1O1xuICAgICAgICAgICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLmFuaW1hdGlvbiwgdGhpcy5pZGxlQW5pbWF0aW9uLCB0cnVlLCB0aGlzLmVsYXBzZVRpbWUgLSB0aGlzLmlkbGVUaW1lKTtcbiAgICAgICAgICAgIHRoaXMuaXNJZGxlID0gdHJ1ZTtcbiAgICAgICAgfWVsc2UgaWYodGhpcy5lbGFwc2VUaW1lPnRoaXMuaW5UaW1lICYmIHRoaXMuaXNTaG93ID09IGZhbHNlKXtcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMjU1O1xuICAgICAgICAgICAgU2ljYm9Ta2VsZXRvblV0aWxzLnNldEFuaW1hdGlvbih0aGlzLmFuaW1hdGlvbiwgdGhpcy5zaG93QW5pbWF0aW9uLCBmYWxzZSwgdGhpcy5lbGFwc2VUaW1lIC0gdGhpcy5pblRpbWUpO1xuICAgICAgICAgICAgdGhpcy5pc1Nob3cgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gaWYodGhpcy5lbGFwc2VUaW1lPiB0aGlzLm91dFRpbWUgKyAxKSB0aGlzLm5vZGUuZGVzdHJveSgpO1xuICAgIH1cbn1cbiJdfQ==