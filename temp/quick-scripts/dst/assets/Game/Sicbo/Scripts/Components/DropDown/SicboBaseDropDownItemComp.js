
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/DropDown/SicboBaseDropDownItemComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '06843iasppHJo0zhoSoKUvG', 'SicboBaseDropDownItemComp');
// Game/Sicbo_New/Scripts/Components/DropDown/SicboBaseDropDownItemComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboBaseDropDownItemComp = /** @class */ (function (_super) {
    __extends(SicboBaseDropDownItemComp, _super);
    function SicboBaseDropDownItemComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = undefined;
        _this.sprite = undefined;
        _this.toggle = undefined;
        return _this;
    }
    __decorate([
        property(cc.Label)
    ], SicboBaseDropDownItemComp.prototype, "label", void 0);
    __decorate([
        property(cc.Sprite)
    ], SicboBaseDropDownItemComp.prototype, "sprite", void 0);
    __decorate([
        property(cc.Toggle)
    ], SicboBaseDropDownItemComp.prototype, "toggle", void 0);
    SicboBaseDropDownItemComp = __decorate([
        ccclass
    ], SicboBaseDropDownItemComp);
    return SicboBaseDropDownItemComp;
}(cc.Component));
exports.default = SicboBaseDropDownItemComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvRHJvcERvd24vU2ljYm9CYXNlRHJvcERvd25JdGVtQ29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUU1QztJQUF1RCw2Q0FBWTtJQUFuRTtRQUFBLHFFQU9DO1FBTFEsV0FBSyxHQUFhLFNBQVMsQ0FBQztRQUU1QixZQUFNLEdBQWMsU0FBUyxDQUFDO1FBRTlCLFlBQU0sR0FBYyxTQUFTLENBQUM7O0lBQ3ZDLENBQUM7SUFMQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzREQUNnQjtJQUVuQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzZEQUNpQjtJQUVyQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzZEQUNpQjtJQU5sQix5QkFBeUI7UUFEN0MsT0FBTztPQUNhLHlCQUF5QixDQU83QztJQUFELGdDQUFDO0NBUEQsQUFPQyxDQVBzRCxFQUFFLENBQUMsU0FBUyxHQU9sRTtrQkFQb0IseUJBQXlCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib0Jhc2VEcm9wRG93bkl0ZW1Db21wIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICBwdWJsaWMgbGFiZWw6IGNjLkxhYmVsID0gdW5kZWZpbmVkO1xuICBAcHJvcGVydHkoY2MuU3ByaXRlKVxuICBwdWJsaWMgc3ByaXRlOiBjYy5TcHJpdGUgPSB1bmRlZmluZWQ7XG4gIEBwcm9wZXJ0eShjYy5Ub2dnbGUpXG4gIHB1YmxpYyB0b2dnbGU6IGNjLlRvZ2dsZSA9IHVuZGVmaW5lZDtcbn1cbiJdfQ==