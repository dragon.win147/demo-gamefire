
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/States/SicboNotifyStateComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0697aIKvhxHYZzn8G1dG+8R', 'SicboNotifyStateComp');
// Game/Sicbo_New/Scripts/Components/States/SicboNotifyStateComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), State = _a.State, Status = _a.Status;
var SicboSetting_1 = require("../../Setting/SicboSetting");
var SicboController_1 = require("../../Controllers/SicboController");
var SicboHelper_1 = require("../../Helpers/SicboHelper");
var SicboGameUtils_1 = require("../../RNGCommons/Utils/SicboGameUtils");
var SicboNotifyComp_1 = require("../SicboNotifyComp");
var SicboText_1 = require("../../Setting/SicboText");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboNotifyStateComp = /** @class */ (function (_super) {
    __extends(SicboNotifyStateComp, _super);
    function SicboNotifyStateComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.notifyComp = null;
        _this.elapseTime = 0;
        _this.totalDuration = 0;
        _this.isWaiting = false;
        return _this;
    }
    SicboNotifyStateComp.prototype.exitState = function (status) {
        if (status == Status.FINISHING)
            this.notifyComp.forceHideSmallNoti();
        this.isWaiting = false;
    };
    SicboNotifyStateComp.prototype.updateState = function (state) {
        if (this.isWaiting == false)
            return;
        this.elapseTime = state.getStageTime();
        var remainingTime = Math.max(Math.ceil((this.totalDuration - this.elapseTime) / 1000), 0);
        this.notifyComp.updateSmallNotiMessage(SicboGameUtils_1.default.FormatString(SicboText_1.default.waitingRemainingTimeMsg, remainingTime));
    };
    SicboNotifyStateComp.prototype.startState = function (state, totalDuration) {
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        this.elapseTime = elapseTime * 1000;
        this.totalDuration = totalDuration;
        switch (status) {
            case Status.FINISHING:
                if (SicboSetting_1.default.getStatusDuration(Status.FINISHING) <= 1 * 1000)
                    return;
                this.isWaiting = true;
                var remainingTime = Math.max(Math.floor((this.totalDuration - this.elapseTime) / 1000), 0);
                var showTime = Math.max(Math.min(totalDuration - 1, totalDuration - this.elapseTime - 1), 0);
                this.notifyComp.showSmallNoti(SicboGameUtils_1.default.FormatString(SicboText_1.default.waitingRemainingTimeMsg, remainingTime), .5, showTime, .5, true);
                SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxMessage);
                break;
            case Status.END_BET:
                if (SicboHelper_1.default.approximatelyEqual(this.elapseTime, 0, 100))
                    this.notifyComp.showStopBettingMessage();
                break;
            case Status.BETTING:
                if (SicboHelper_1.default.approximatelyEqual(this.elapseTime, 0, 100))
                    this.notifyComp.showBettingMessage();
                break;
            default:
                break;
        }
    };
    SicboNotifyStateComp.prototype.onHide = function () {
        this.notifyComp.forceHideLargeNoti();
        this.notifyComp.forceHideResultNoti();
        this.notifyComp.forceHideSmallNoti();
    };
    __decorate([
        property(SicboNotifyComp_1.default)
    ], SicboNotifyStateComp.prototype, "notifyComp", void 0);
    SicboNotifyStateComp = __decorate([
        ccclass
    ], SicboNotifyStateComp);
    return SicboNotifyStateComp;
}(cc.Component));
exports.default = SicboNotifyStateComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU3RhdGVzL1NpY2JvTm90aWZ5U3RhdGVDb21wLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGO0FBQ2xGLHdFQUFtRTtBQUU3RCxJQUFBLEtBRU0sNEJBQWtCLENBQUMsVUFBVSxFQUFFLEVBRHpDLEtBQUssV0FBQSxFQUNMLE1BQU0sWUFBbUMsQ0FBQztBQUU1QywyREFBc0U7QUFDdEUscUVBQWdFO0FBQ2hFLHlEQUFvRDtBQUNwRCx3RUFBbUU7QUFDbkUsc0RBQWlEO0FBRWpELHFEQUFnRDtBQUUxQyxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFrRCx3Q0FBWTtJQUE5RDtRQUFBLHFFQXdEQztRQXRERyxnQkFBVSxHQUFvQixJQUFJLENBQUM7UUFFbkMsZ0JBQVUsR0FBVyxDQUFDLENBQUM7UUFDdkIsbUJBQWEsR0FBVyxDQUFDLENBQUM7UUFFMUIsZUFBUyxHQUFZLEtBQUssQ0FBQzs7SUFpRC9CLENBQUM7SUEvQ0csd0NBQVMsR0FBVCxVQUFVLE1BQU07UUFDZCxJQUFHLE1BQU0sSUFBSSxNQUFNLENBQUMsU0FBUztZQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQztJQUVELDBDQUFXLEdBQVgsVUFBWSxLQUFpQztRQUMzQyxJQUFHLElBQUksQ0FBQyxTQUFTLElBQUksS0FBSztZQUFFLE9BQU87UUFFbkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDdkMsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDekYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyx3QkFBYyxDQUFDLFlBQVksQ0FBQyxtQkFBUyxDQUFDLHVCQUF1QixFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7SUFDeEgsQ0FBQztJQUVELHlDQUFVLEdBQVYsVUFBVyxLQUFpQyxFQUFFLGFBQXFCO1FBQ2pFLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUMvQixJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLEdBQUMsSUFBSSxDQUFDO1FBRTNDLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxHQUFHLElBQUksQ0FBQztRQUNwQyxJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztRQUNqQyxRQUFRLE1BQU0sRUFBRTtZQUNaLEtBQUssTUFBTSxDQUFDLFNBQVM7Z0JBQ25CLElBQUcsc0JBQVksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUcsQ0FBQyxHQUFDLElBQUk7b0JBQUUsT0FBTztnQkFDckUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7Z0JBQ3RCLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUMxRixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxHQUFHLENBQUMsRUFBRSxhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDN0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsd0JBQWMsQ0FBQyxZQUFZLENBQUMsbUJBQVMsQ0FBQyx1QkFBdUIsRUFBRSxhQUFhLENBQUMsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDckkseUJBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHlCQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3hELE1BQU07WUFDUixLQUFLLE1BQU0sQ0FBQyxPQUFPO2dCQUNqQixJQUFHLHFCQUFXLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDO29CQUN4RCxJQUFJLENBQUMsVUFBVSxDQUFDLHNCQUFzQixFQUFFLENBQUM7Z0JBQzNDLE1BQU07WUFDUixLQUFLLE1BQU0sQ0FBQyxPQUFPO2dCQUNqQixJQUFHLHFCQUFXLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDO29CQUN4RCxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUM7Z0JBQ3ZDLE1BQU07WUFDUjtnQkFDRSxNQUFNO1NBQ1Q7SUFDUCxDQUFDO0lBRUQscUNBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDdEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0lBQ3ZDLENBQUM7SUFyREQ7UUFEQyxRQUFRLENBQUMseUJBQWUsQ0FBQzs0REFDUztJQUZsQixvQkFBb0I7UUFEeEMsT0FBTztPQUNhLG9CQUFvQixDQXdEeEM7SUFBRCwyQkFBQztDQXhERCxBQXdEQyxDQXhEaUQsRUFBRSxDQUFDLFNBQVMsR0F3RDdEO2tCQXhEb0Isb0JBQW9CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5pbXBvcnQgU2ljYm9Nb2R1bGVBZGFwdGVyIGZyb20gXCIuLi8uLi8uLi8uLi8uLi9TaWNib01vZHVsZUFkYXB0ZXJcIjtcblxuY29uc3Qge1xuICBTdGF0ZSxcbiAgU3RhdHVzfSA9IFNpY2JvTW9kdWxlQWRhcHRlci5nZXRBbGxSZWZzKCk7XG5cbmltcG9ydCBTaWNib1NldHRpbmcsIHsgU2ljYm9Tb3VuZCB9IGZyb20gXCIuLi8uLi9TZXR0aW5nL1NpY2JvU2V0dGluZ1wiO1xuaW1wb3J0IFNpY2JvQ29udHJvbGxlciBmcm9tIFwiLi4vLi4vQ29udHJvbGxlcnMvU2ljYm9Db250cm9sbGVyXCI7XG5pbXBvcnQgU2ljYm9IZWxwZXIgZnJvbSBcIi4uLy4uL0hlbHBlcnMvU2ljYm9IZWxwZXJcIjtcbmltcG9ydCBTaWNib0dhbWVVdGlscyBmcm9tIFwiLi4vLi4vUk5HQ29tbW9ucy9VdGlscy9TaWNib0dhbWVVdGlsc1wiO1xuaW1wb3J0IFNpY2JvTm90aWZ5Q29tcCBmcm9tIFwiLi4vU2ljYm9Ob3RpZnlDb21wXCI7XG5pbXBvcnQgSVNpY2JvU3RhdGUgZnJvbSBcIi4vSVNpY2JvU3RhdGVcIjtcbmltcG9ydCBTaWNib1RleHQgZnJvbSAnLi4vLi4vU2V0dGluZy9TaWNib1RleHQnO1xuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvTm90aWZ5U3RhdGVDb21wIGV4dGVuZHMgY2MuQ29tcG9uZW50IGltcGxlbWVudHMgSVNpY2JvU3RhdGV7XG4gICAgQHByb3BlcnR5KFNpY2JvTm90aWZ5Q29tcClcbiAgICBub3RpZnlDb21wOiBTaWNib05vdGlmeUNvbXAgPSBudWxsO1xuXG4gICAgZWxhcHNlVGltZTogbnVtYmVyID0gMDtcbiAgICB0b3RhbER1cmF0aW9uOiBudW1iZXIgPSAwO1xuXG4gICAgaXNXYWl0aW5nOiBib29sZWFuID0gZmFsc2U7XG5cbiAgICBleGl0U3RhdGUoc3RhdHVzKTogdm9pZCB7XG4gICAgICBpZihzdGF0dXMgPT0gU3RhdHVzLkZJTklTSElORylcbiAgICAgICAgdGhpcy5ub3RpZnlDb21wLmZvcmNlSGlkZVNtYWxsTm90aSgpO1xuICAgICAgdGhpcy5pc1dhaXRpbmcgPSBmYWxzZTtcbiAgICB9XG5cbiAgICB1cGRhdGVTdGF0ZShzdGF0ZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGF0ZT4pOiB2b2lkIHtcbiAgICAgIGlmKHRoaXMuaXNXYWl0aW5nID09IGZhbHNlKSByZXR1cm47XG5cbiAgICAgIHRoaXMuZWxhcHNlVGltZSA9IHN0YXRlLmdldFN0YWdlVGltZSgpO1xuICAgICAgbGV0IHJlbWFpbmluZ1RpbWUgPSBNYXRoLm1heChNYXRoLmNlaWwoKHRoaXMudG90YWxEdXJhdGlvbiAtIHRoaXMuZWxhcHNlVGltZSkvIDEwMDApLCAwKTsgICAgICBcbiAgICAgIHRoaXMubm90aWZ5Q29tcC51cGRhdGVTbWFsbE5vdGlNZXNzYWdlKFNpY2JvR2FtZVV0aWxzLkZvcm1hdFN0cmluZyhTaWNib1RleHQud2FpdGluZ1JlbWFpbmluZ1RpbWVNc2csIHJlbWFpbmluZ1RpbWUpKTsgICAgICBcbiAgICB9XG5cbiAgICBzdGFydFN0YXRlKHN0YXRlOiBJbnN0YW5jZVR5cGU8dHlwZW9mIFN0YXRlPiwgdG90YWxEdXJhdGlvbjogbnVtYmVyKTogdm9pZCB7XG4gICAgICBsZXQgc3RhdHVzID0gc3RhdGUuZ2V0U3RhdHVzKCk7XG4gICAgICBsZXQgZWxhcHNlVGltZSA9IHN0YXRlLmdldFN0YWdlVGltZSgpLzEwMDA7XG4gICAgICBcbiAgICAgIHRoaXMuZWxhcHNlVGltZSA9IGVsYXBzZVRpbWUgKiAxMDAwO1xuICAgICAgdGhpcy50b3RhbER1cmF0aW9uID0gdG90YWxEdXJhdGlvbjsgICAgICBcbiAgICAgICAgc3dpdGNoIChzdGF0dXMpIHtcbiAgICAgICAgICAgIGNhc2UgU3RhdHVzLkZJTklTSElORzogXG4gICAgICAgICAgICAgIGlmKFNpY2JvU2V0dGluZy5nZXRTdGF0dXNEdXJhdGlvbihTdGF0dXMuRklOSVNISU5HKTw9IDEqMTAwMCkgcmV0dXJuO1xuICAgICAgICAgICAgICB0aGlzLmlzV2FpdGluZyA9IHRydWU7XG4gICAgICAgICAgICAgIGxldCByZW1haW5pbmdUaW1lID0gTWF0aC5tYXgoTWF0aC5mbG9vcigodGhpcy50b3RhbER1cmF0aW9uIC0gdGhpcy5lbGFwc2VUaW1lKS8gMTAwMCksIDApOyAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgIGxldCBzaG93VGltZSA9IE1hdGgubWF4KE1hdGgubWluKHRvdGFsRHVyYXRpb24gLSAxLCB0b3RhbER1cmF0aW9uIC0gdGhpcy5lbGFwc2VUaW1lIC0gMSksIDApO1xuICAgICAgICAgICAgICB0aGlzLm5vdGlmeUNvbXAuc2hvd1NtYWxsTm90aShTaWNib0dhbWVVdGlscy5Gb3JtYXRTdHJpbmcoU2ljYm9UZXh0LndhaXRpbmdSZW1haW5pbmdUaW1lTXNnLCByZW1haW5pbmdUaW1lKSwgLjUsIHNob3dUaW1lLCAuNSwgdHJ1ZSk7XG4gICAgICAgICAgICAgIFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5wbGF5U2Z4KFNpY2JvU291bmQuU2Z4TWVzc2FnZSk7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBTdGF0dXMuRU5EX0JFVDogICAgICAgICAgICAgIFxuICAgICAgICAgICAgICBpZihTaWNib0hlbHBlci5hcHByb3hpbWF0ZWx5RXF1YWwodGhpcy5lbGFwc2VUaW1lLCAwLCAxMDApKVxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZ5Q29tcC5zaG93U3RvcEJldHRpbmdNZXNzYWdlKCk7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBTdGF0dXMuQkVUVElORzogICAgICAgICAgICAgIFxuICAgICAgICAgICAgICBpZihTaWNib0hlbHBlci5hcHByb3hpbWF0ZWx5RXF1YWwodGhpcy5lbGFwc2VUaW1lLCAwLCAxMDApKVxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZ5Q29tcC5zaG93QmV0dGluZ01lc3NhZ2UoKTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIG9uSGlkZSgpOiB2b2lkIHtcbiAgICAgIHRoaXMubm90aWZ5Q29tcC5mb3JjZUhpZGVMYXJnZU5vdGkoKTtcbiAgICAgIHRoaXMubm90aWZ5Q29tcC5mb3JjZUhpZGVSZXN1bHROb3RpKCk7XG4gICAgICB0aGlzLm5vdGlmeUNvbXAuZm9yY2VIaWRlU21hbGxOb3RpKCk7XG4gICAgfVxufVxuIl19