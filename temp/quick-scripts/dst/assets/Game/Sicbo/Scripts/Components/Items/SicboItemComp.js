
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/Items/SicboItemComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3de95q9UaVJCpt8GjaCsJP7', 'SicboItemComp');
// Game/Sicbo_New/Scripts/Components/Items/SicboItemComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboItemComp = /** @class */ (function (_super) {
    __extends(SicboItemComp, _super);
    function SicboItemComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.iconSprite = null;
        _this.itemIconSprite = [];
        return _this;
    }
    SicboItemComp.prototype.setData = function (item, isAnimated) {
        if (isAnimated === void 0) { isAnimated = false; }
        var self = this;
        cc.tween(self.node)
            .to(isAnimated ? .5 : 0, { scale: 0 })
            .call(function () {
            var index = item;
            self.iconSprite.spriteFrame = self.itemIconSprite[index - 1];
        })
            .to(isAnimated ? 0.5 : 0, { scale: 1 })
            .start();
    };
    SicboItemComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.Sprite)
    ], SicboItemComp.prototype, "iconSprite", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], SicboItemComp.prototype, "itemIconSprite", void 0);
    SicboItemComp = __decorate([
        ccclass
    ], SicboItemComp);
    return SicboItemComp;
}(cc.Component));
exports.default = SicboItemComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvSXRlbXMvU2ljYm9JdGVtQ29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDTSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUEyQyxpQ0FBWTtJQUF2RDtRQUFBLHFFQXlCQztRQXZCQyxnQkFBVSxHQUFjLElBQUksQ0FBQztRQUc3QixvQkFBYyxHQUFxQixFQUFFLENBQUM7O0lBb0J4QyxDQUFDO0lBbEJDLCtCQUFPLEdBQVAsVUFBUSxJQUFJLEVBQUUsVUFBMkI7UUFBM0IsMkJBQUEsRUFBQSxrQkFBMkI7UUFDdkMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBRWhCLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzthQUNsQixFQUFFLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQzthQUNyQyxJQUFJLENBQ0g7WUFDRSxJQUFJLEtBQUssR0FBRyxJQUFjLENBQUM7WUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDL0QsQ0FBQyxDQUNGO2FBQ0EsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUM7YUFDdEMsS0FBSyxFQUFFLENBQUM7SUFDWCxDQUFDO0lBRUQsaUNBQVMsR0FBVDtRQUNFLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBdEJEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7cURBQ1M7SUFHN0I7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUM7eURBQ1c7SUFMbkIsYUFBYTtRQURqQyxPQUFPO09BQ2EsYUFBYSxDQXlCakM7SUFBRCxvQkFBQztDQXpCRCxBQXlCQyxDQXpCMEMsRUFBRSxDQUFDLFNBQVMsR0F5QnREO2tCQXpCb0IsYUFBYSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvSXRlbUNvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICBAcHJvcGVydHkoY2MuU3ByaXRlKVxuICBpY29uU3ByaXRlOiBjYy5TcHJpdGUgPSBudWxsOyAgXG5cbiAgQHByb3BlcnR5KFtjYy5TcHJpdGVGcmFtZV0pXG4gIGl0ZW1JY29uU3ByaXRlOiBjYy5TcHJpdGVGcmFtZVtdID0gW107XG5cbiAgc2V0RGF0YShpdGVtLCBpc0FuaW1hdGVkOiBib29sZWFuID0gZmFsc2Upe1xuICAgIGxldCBzZWxmID0gdGhpcztcbiAgICBcbiAgICBjYy50d2VlbihzZWxmLm5vZGUpXG4gICAgLnRvKGlzQW5pbWF0ZWQgPyAuNSA6IDAsIHsgc2NhbGU6IDAgfSlcbiAgICAuY2FsbChcbiAgICAgICgpID0+IHtcbiAgICAgICAgbGV0IGluZGV4ID0gaXRlbSBhcyBudW1iZXI7XG4gICAgICAgIHNlbGYuaWNvblNwcml0ZS5zcHJpdGVGcmFtZSA9IHNlbGYuaXRlbUljb25TcHJpdGVbaW5kZXggLSAxXTtcbiAgICAgIH1cbiAgICApXG4gICAgLnRvKGlzQW5pbWF0ZWQgPyAwLjUgOiAwLCB7IHNjYWxlOiAxIH0pXG4gICAgLnN0YXJ0KCk7XG4gIH1cblxuICBvbkRlc3Ryb3koKXtcbiAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5ub2RlKTtcbiAgfVxufVxuIl19