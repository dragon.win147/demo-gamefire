
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Components/SicboUserComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8a68bsnskZMI6KaDiaYGseu', 'SicboUserComp');
// Game/Sicbo_New/Scripts/Components/SicboUserComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var Playah = SicboModuleAdapter_1.default.getAllRefs().Playah;
var SicboUserItemComp_1 = require("./Items/SicboUserItemComp");
var SicboPortalAdapter_1 = require("../SicboPortalAdapter");
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboText_1 = require("../Setting/SicboText");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboUserComp = /** @class */ (function (_super) {
    __extends(SicboUserComp, _super);
    function SicboUserComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //MY USER UI
        _this.myUserItem = null;
        _this.otherUserGroup = null;
        _this.remainingUsersLabel = null;
        _this.usersInRoomNode = null;
        _this.usersInRoom = [];
        _this.remainingPlayersCount = 0;
        _this.usersCachePos = [];
        _this.isUserInRoomAnimPlaying = false;
        _this.userCompAnimMap = new Map(); //co data - co slot
        return _this;
    }
    SicboUserComp.prototype.onLoad = function () {
        this.usersInRoom = this.usersInRoomNode.getComponentsInChildren(SicboUserItemComp_1.default);
        for (var i = 0; i < this.usersInRoom.length; i++) {
            this.usersCachePos.push(this.usersInRoom[i].node.getPosition());
            this.userCompAnimMap.set(this.usersInRoom[i], null);
        }
        this.setMyUserItemUI();
    };
    SicboUserComp.prototype.setMyUserItemUI = function () {
        var playah = SicboPortalAdapter_1.default.getInstance().getMyPlayAh();
        this.myUserItem.setData(playah);
    };
    // getMyUserItemComp(): SicboUserItemComp{
    //     return this.myUserItem;
    // }
    SicboUserComp.prototype.findUser = function (userId) {
        var user = this.getUserInRoom(userId);
        if (user == null) {
            var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
            if (myUserId == userId)
                user = this.myUserItem;
            else
                user = this.otherUserGroup;
        }
        return user;
    };
    SicboUserComp.prototype.getUserInRoom = function (userId) {
        return this.usersInRoom.find(function (x) { return x.data != null && x.data.userId == userId; });
    };
    SicboUserComp.prototype.getMyUser = function () {
        return this.myUserItem;
    };
    SicboUserComp.prototype.getOtherUserGroup = function () {
        return this.otherUserGroup;
    };
    SicboUserComp.prototype.playBetAnimation = function (user, slotNode) {
        user.shakeBet(slotNode);
    };
    SicboUserComp.prototype.updateUsersInRoom = function (displayPlayers, remainingPlayersCount) {
        this.remainingPlayersCount = Math.max(remainingPlayersCount, 0);
        this.otherUserGroup.node.active = remainingPlayersCount > 0;
        if (remainingPlayersCount > 999)
            this.remainingUsersLabel.string = SicboText_1.default.lotsOfUserText;
        else
            this.remainingUsersLabel.string = remainingPlayersCount.toString();
        if (this.isSameUserData(displayPlayers))
            return;
        if (this.isUserInRoomAnimPlaying)
            return;
        this.cacheDisplayPlayers = displayPlayers;
        this.playUserInRoomAnimation(displayPlayers);
    };
    SicboUserComp.prototype.playUserInRoomAnimation = function (displayPlayers) {
        var _this = this;
        var animDuration = SicboSetting_1.default.USER_ANIM_DURATION;
        var self = this;
        var animCount = 0;
        this.userCompAnimMap.forEach(function (targetIndex, userComp) {
            _this.userCompAnimMap.set(userComp, null);
        });
        var _loop_1 = function (i) {
            var userId = displayPlayers[i].getUserId();
            var userComp = this_1.usersInRoom.find(function (x) { return x.getUserId() == userId; });
            if (userComp != null) {
                //console.error("anim", userId, displayPlayers[i].getProfile().getDisplayName(), i)
                self.userCompAnimMap.set(userComp, i);
                if (this_1.isUseLinearAnim(userComp.getChatIndex(), i)) {
                    cc.tween(userComp.node).to(animDuration, { x: self.usersCachePos[i].x, y: self.usersCachePos[i].y }, { easing: "cubicInOut" }).start();
                }
                else {
                    var startPos = new cc.Vec2(userComp.node.x, userComp.node.position.y);
                    var endPos = new cc.Vec2(self.usersCachePos[i].x, self.usersCachePos[i].y);
                    cc.tween(userComp.node).bezierTo(animDuration, startPos, new cc.Vec2(0, -170), endPos).start();
                }
                userComp.setData(displayPlayers[i], i);
                animCount++;
            }
        };
        var this_1 = this;
        //console.error("-------------", displayPlayers.length)
        // for (let i = 0; i < displayPlayers.length; i++) {console.error(displayPlayers[i].getUserId(), displayPlayers[i].getProfile().getDisplayName())}
        //user da co - co slot moi
        for (var i = 0; i < displayPlayers.length; i++) {
            _loop_1(i);
        }
        var freeComps = [];
        this.userCompAnimMap.forEach(function (i, userComp) {
            if (i == null)
                freeComps.push(userComp);
        });
        //NO PLAYAH COMP
        for (var i = 0; i < freeComps.length; i++) {
            freeComps[i].setData();
        }
        cc.tween(this.node)
            .delay(animDuration / 2)
            .call(function () {
            var _loop_2 = function (i) {
                var userId = displayPlayers[i].getUserId();
                var userComp = self.usersInRoom.find(function (x) { return x.getUserId() == userId; });
                if (userComp == null) {
                    //console.error("set", userId, displayPlayers[i].getProfile().getDisplayName(), i)
                    userComp = freeComps.pop();
                    if (userComp != null) {
                        userComp.setData(displayPlayers[i], i);
                        cc.tween(userComp.node).to(0, { x: self.usersCachePos[i].x, y: self.usersCachePos[i].y }).start();
                    }
                }
            };
            //NEW Playah
            for (var i = 0; i < displayPlayers.length; i++) {
                _loop_2(i);
            }
        })
            .start();
        //ANIM DELAY
        if (animCount > 0) {
            self.isUserInRoomAnimPlaying = true;
            cc.tween(this.node)
                .delay(animDuration)
                .call(function () {
                self.isUserInRoomAnimPlaying = false;
            })
                .start();
        }
        else {
            self.isUserInRoomAnimPlaying = false;
        }
    };
    SicboUserComp.prototype.isSameUserData = function (displayPlayers) {
        return false;
        // if(this.cacheDisplayPlayers == null || this.cacheDisplayPlayers.length == 0) return false;
        // if(this.cacheDisplayPlayers.length != displayPlayers.length) return false;
        // for (let i = 0; i < displayPlayers.length; i++) {
        //     if(this.cacheDisplayPlayers[i].getUserId() != displayPlayers[i].getUserId()) return false;
        // }
        // return true;
    };
    SicboUserComp.prototype.isUseLinearAnim = function (myIndex, otherIndex) {
        var isSameSide = Math.floor(myIndex / 3) == Math.floor(otherIndex / 3);
        var isSameRow = myIndex % 3 == otherIndex % 3;
        return isSameSide || isSameRow;
    };
    __decorate([
        property(SicboUserItemComp_1.default)
    ], SicboUserComp.prototype, "myUserItem", void 0);
    __decorate([
        property(SicboUserItemComp_1.default)
    ], SicboUserComp.prototype, "otherUserGroup", void 0);
    __decorate([
        property(cc.Label)
    ], SicboUserComp.prototype, "remainingUsersLabel", void 0);
    __decorate([
        property(cc.Node)
    ], SicboUserComp.prototype, "usersInRoomNode", void 0);
    SicboUserComp = __decorate([
        ccclass
    ], SicboUserComp);
    return SicboUserComp;
}(cc.Component));
exports.default = SicboUserComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0NvbXBvbmVudHMvU2ljYm9Vc2VyQ29tcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVsRixxRUFBZ0U7QUFFeEQsSUFBQSxNQUFNLEdBQUssNEJBQWtCLENBQUMsVUFBVSxFQUFFLE9BQXBDLENBQXFDO0FBQ25ELCtEQUEwRDtBQUMxRCw0REFBdUQ7QUFDdkQsd0RBQW1EO0FBQ25ELGtEQUE2QztBQUV2QyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUEyQyxpQ0FBWTtJQUF2RDtRQUFBLHFFQThLQztRQTdLQyxZQUFZO1FBRVosZ0JBQVUsR0FBc0IsSUFBSSxDQUFDO1FBR3JDLG9CQUFjLEdBQXNCLElBQUksQ0FBQztRQUV6Qyx5QkFBbUIsR0FBYSxJQUFJLENBQUM7UUFHckMscUJBQWUsR0FBWSxJQUFJLENBQUM7UUFDaEMsaUJBQVcsR0FBd0IsRUFBRSxDQUFDO1FBRXRDLDJCQUFxQixHQUFXLENBQUMsQ0FBQztRQUMxQixtQkFBYSxHQUFjLEVBQUUsQ0FBQztRQUU5Qiw2QkFBdUIsR0FBRyxLQUFLLENBQUM7UUFDaEMscUJBQWUsR0FBbUMsSUFBSSxHQUFHLEVBQTZCLENBQUMsQ0FBQyxtQkFBbUI7O0lBNEpySCxDQUFDO0lBeEpDLDhCQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsdUJBQXVCLENBQUMsMkJBQWlCLENBQUMsQ0FBQztRQUNuRixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDaEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUNoRSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3JEO1FBQ0QsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCx1Q0FBZSxHQUFmO1FBQ0UsSUFBSSxNQUFNLEdBQUcsNEJBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDNUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELDBDQUEwQztJQUMxQyw4QkFBOEI7SUFDOUIsSUFBSTtJQUVKLGdDQUFRLEdBQVIsVUFBUyxNQUFjO1FBQ3JCLElBQUksSUFBSSxHQUFzQixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXpELElBQUksSUFBSSxJQUFJLElBQUksRUFBRTtZQUNoQixJQUFJLFFBQVEsR0FBRyw0QkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5RCxJQUFJLFFBQVEsSUFBSSxNQUFNO2dCQUFFLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDOztnQkFDMUMsSUFBSSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7U0FDakM7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxxQ0FBYSxHQUFiLFVBQWMsTUFBYztRQUMxQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksTUFBTSxFQUF6QyxDQUF5QyxDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVELGlDQUFTLEdBQVQ7UUFDRSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDekIsQ0FBQztJQUVELHlDQUFpQixHQUFqQjtRQUNFLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUM3QixDQUFDO0lBRUQsd0NBQWdCLEdBQWhCLFVBQWlCLElBQXVCLEVBQUUsUUFBaUI7UUFDekQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRUQseUNBQWlCLEdBQWpCLFVBQWtCLGNBQTZDLEVBQUUscUJBQTZCO1FBQzVGLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxxQkFBcUIsR0FBRyxDQUFDLENBQUM7UUFDNUQsSUFBSSxxQkFBcUIsR0FBRyxHQUFHO1lBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxtQkFBUyxDQUFDLGNBQWMsQ0FBQzs7WUFDdkYsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxxQkFBcUIsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUV4RSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDO1lBQUUsT0FBTztRQUNoRCxJQUFJLElBQUksQ0FBQyx1QkFBdUI7WUFBRSxPQUFPO1FBRXpDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxjQUFjLENBQUM7UUFFMUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFTywrQ0FBdUIsR0FBL0IsVUFBZ0MsY0FBNkM7UUFBN0UsaUJBeUVDO1FBeEVDLElBQUksWUFBWSxHQUFHLHNCQUFZLENBQUMsa0JBQWtCLENBQUM7UUFDbkQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBRWhCLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQztRQUNsQixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFdBQW1CLEVBQUUsUUFBMkI7WUFDNUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO2dDQUtNLENBQUM7WUFDUixJQUFJLE1BQU0sR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDM0MsSUFBSSxRQUFRLEdBQUcsT0FBSyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLFNBQVMsRUFBRSxJQUFJLE1BQU0sRUFBdkIsQ0FBdUIsQ0FBQyxDQUFDO1lBQ3JFLElBQUksUUFBUSxJQUFJLElBQUksRUFBRTtnQkFDcEIsbUZBQW1GO2dCQUNuRixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RDLElBQUksT0FBSyxlQUFlLENBQUMsUUFBUSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFO29CQUNwRCxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7aUJBQ3hJO3FCQUFNO29CQUNMLElBQUksUUFBUSxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDdEUsSUFBSSxNQUFNLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzNFLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxZQUFZLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDaEc7Z0JBQ0QsUUFBUSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZDLFNBQVMsRUFBRSxDQUFDO2FBQ2I7OztRQWxCSCx1REFBdUQ7UUFDdkQsa0pBQWtKO1FBQ2xKLDBCQUEwQjtRQUMxQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUU7b0JBQXJDLENBQUM7U0FnQlQ7UUFFRCxJQUFJLFNBQVMsR0FBd0IsRUFBRSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBUyxFQUFFLFFBQTJCO1lBQ2xFLElBQUksQ0FBQyxJQUFJLElBQUk7Z0JBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQUMsQ0FBQztRQUVILGdCQUFnQjtRQUNoQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN6QyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDeEI7UUFFRCxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDaEIsS0FBSyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7YUFDdkIsSUFBSSxDQUFDO29DQUVLLENBQUM7Z0JBQ1IsSUFBSSxNQUFNLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUMzQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxNQUFNLEVBQXZCLENBQXVCLENBQUMsQ0FBQztnQkFDckUsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO29CQUNwQixrRkFBa0Y7b0JBRWxGLFFBQVEsR0FBRyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUM7b0JBQzNCLElBQUksUUFBUSxJQUFJLElBQUksRUFBRTt3QkFDcEIsUUFBUSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBRXZDLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztxQkFDbkc7aUJBQ0Y7O1lBYkgsWUFBWTtZQUNaLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxjQUFjLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRTt3QkFBckMsQ0FBQzthQWFUO1FBQ0gsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxFQUFFLENBQUM7UUFFWCxZQUFZO1FBQ1osSUFBSSxTQUFTLEdBQUcsQ0FBQyxFQUFFO1lBQ2pCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUM7WUFDcEMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUNoQixLQUFLLENBQUMsWUFBWSxDQUFDO2lCQUNuQixJQUFJLENBQUM7Z0JBQ0osSUFBSSxDQUFDLHVCQUF1QixHQUFHLEtBQUssQ0FBQztZQUN2QyxDQUFDLENBQUM7aUJBQ0QsS0FBSyxFQUFFLENBQUM7U0FDWjthQUFNO1lBQ0wsSUFBSSxDQUFDLHVCQUF1QixHQUFHLEtBQUssQ0FBQztTQUN0QztJQUNILENBQUM7SUFFRCxzQ0FBYyxHQUFkLFVBQWUsY0FBNkM7UUFDMUQsT0FBTyxLQUFLLENBQUM7UUFFYiw2RkFBNkY7UUFDN0YsNkVBQTZFO1FBRTdFLG9EQUFvRDtRQUNwRCxpR0FBaUc7UUFDakcsSUFBSTtRQUNKLGVBQWU7SUFDakIsQ0FBQztJQUVELHVDQUFlLEdBQWYsVUFBZ0IsT0FBZSxFQUFFLFVBQWtCO1FBQ2pELElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksU0FBUyxHQUFHLE9BQU8sR0FBRyxDQUFDLElBQUksVUFBVSxHQUFHLENBQUMsQ0FBQztRQUU5QyxPQUFPLFVBQVUsSUFBSSxTQUFTLENBQUM7SUFDakMsQ0FBQztJQTFLRDtRQURDLFFBQVEsQ0FBQywyQkFBaUIsQ0FBQztxREFDUztJQUdyQztRQURDLFFBQVEsQ0FBQywyQkFBaUIsQ0FBQzt5REFDYTtJQUV6QztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzhEQUNrQjtJQUdyQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzBEQUNjO0lBWGIsYUFBYTtRQURqQyxPQUFPO09BQ2EsYUFBYSxDQThLakM7SUFBRCxvQkFBQztDQTlLRCxBQThLQyxDQTlLMEMsRUFBRSxDQUFDLFNBQVMsR0E4S3REO2tCQTlLb0IsYUFBYSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgU2ljYm9Nb2R1bGVBZGFwdGVyIGZyb20gXCIuLi8uLi8uLi8uLi9TaWNib01vZHVsZUFkYXB0ZXJcIjtcblxuY29uc3QgeyBQbGF5YWggfSA9IFNpY2JvTW9kdWxlQWRhcHRlci5nZXRBbGxSZWZzKCk7XG5pbXBvcnQgU2ljYm9Vc2VySXRlbUNvbXAgZnJvbSBcIi4vSXRlbXMvU2ljYm9Vc2VySXRlbUNvbXBcIjtcbmltcG9ydCBTaWNib1BvcnRhbEFkYXB0ZXIgZnJvbSBcIi4uL1NpY2JvUG9ydGFsQWRhcHRlclwiO1xuaW1wb3J0IFNpY2JvU2V0dGluZyBmcm9tIFwiLi4vU2V0dGluZy9TaWNib1NldHRpbmdcIjtcbmltcG9ydCBTaWNib1RleHQgZnJvbSBcIi4uL1NldHRpbmcvU2ljYm9UZXh0XCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1VzZXJDb21wIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgLy9NWSBVU0VSIFVJXG4gIEBwcm9wZXJ0eShTaWNib1VzZXJJdGVtQ29tcClcbiAgbXlVc2VySXRlbTogU2ljYm9Vc2VySXRlbUNvbXAgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShTaWNib1VzZXJJdGVtQ29tcClcbiAgb3RoZXJVc2VyR3JvdXA6IFNpY2JvVXNlckl0ZW1Db21wID0gbnVsbDtcbiAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICByZW1haW5pbmdVc2Vyc0xhYmVsOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIHVzZXJzSW5Sb29tTm9kZTogY2MuTm9kZSA9IG51bGw7XG4gIHVzZXJzSW5Sb29tOiBTaWNib1VzZXJJdGVtQ29tcFtdID0gW107XG5cbiAgcmVtYWluaW5nUGxheWVyc0NvdW50OiBudW1iZXIgPSAwO1xuICBwcml2YXRlIHVzZXJzQ2FjaGVQb3M6IGNjLlZlYzJbXSA9IFtdO1xuXG4gIHByaXZhdGUgaXNVc2VySW5Sb29tQW5pbVBsYXlpbmcgPSBmYWxzZTtcbiAgcHJpdmF0ZSB1c2VyQ29tcEFuaW1NYXA6IE1hcDxTaWNib1VzZXJJdGVtQ29tcCwgbnVtYmVyPiA9IG5ldyBNYXA8U2ljYm9Vc2VySXRlbUNvbXAsIG51bWJlcj4oKTsgLy9jbyBkYXRhIC0gY28gc2xvdFxuXG4gIHByaXZhdGUgY2FjaGVEaXNwbGF5UGxheWVyczogSW5zdGFuY2VUeXBlPHR5cGVvZiBQbGF5YWg+W107XG5cbiAgb25Mb2FkKCkge1xuICAgIHRoaXMudXNlcnNJblJvb20gPSB0aGlzLnVzZXJzSW5Sb29tTm9kZS5nZXRDb21wb25lbnRzSW5DaGlsZHJlbihTaWNib1VzZXJJdGVtQ29tcCk7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnVzZXJzSW5Sb29tLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLnVzZXJzQ2FjaGVQb3MucHVzaCh0aGlzLnVzZXJzSW5Sb29tW2ldLm5vZGUuZ2V0UG9zaXRpb24oKSk7XG4gICAgICB0aGlzLnVzZXJDb21wQW5pbU1hcC5zZXQodGhpcy51c2Vyc0luUm9vbVtpXSwgbnVsbCk7XG4gICAgfVxuICAgIHRoaXMuc2V0TXlVc2VySXRlbVVJKCk7XG4gIH1cblxuICBzZXRNeVVzZXJJdGVtVUkoKSB7XG4gICAgbGV0IHBsYXlhaCA9IFNpY2JvUG9ydGFsQWRhcHRlci5nZXRJbnN0YW5jZSgpLmdldE15UGxheUFoKCk7XG4gICAgdGhpcy5teVVzZXJJdGVtLnNldERhdGEocGxheWFoKTtcbiAgfVxuXG4gIC8vIGdldE15VXNlckl0ZW1Db21wKCk6IFNpY2JvVXNlckl0ZW1Db21we1xuICAvLyAgICAgcmV0dXJuIHRoaXMubXlVc2VySXRlbTtcbiAgLy8gfVxuXG4gIGZpbmRVc2VyKHVzZXJJZDogc3RyaW5nKTogU2ljYm9Vc2VySXRlbUNvbXAge1xuICAgIGxldCB1c2VyOiBTaWNib1VzZXJJdGVtQ29tcCA9IHRoaXMuZ2V0VXNlckluUm9vbSh1c2VySWQpO1xuXG4gICAgaWYgKHVzZXIgPT0gbnVsbCkge1xuICAgICAgbGV0IG15VXNlcklkID0gU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuZ2V0TXlVc2VySWQoKTtcbiAgICAgIGlmIChteVVzZXJJZCA9PSB1c2VySWQpIHVzZXIgPSB0aGlzLm15VXNlckl0ZW07XG4gICAgICBlbHNlIHVzZXIgPSB0aGlzLm90aGVyVXNlckdyb3VwO1xuICAgIH1cbiAgICByZXR1cm4gdXNlcjtcbiAgfVxuXG4gIGdldFVzZXJJblJvb20odXNlcklkOiBzdHJpbmcpOiBTaWNib1VzZXJJdGVtQ29tcCB7XG4gICAgcmV0dXJuIHRoaXMudXNlcnNJblJvb20uZmluZCgoeCkgPT4geC5kYXRhICE9IG51bGwgJiYgeC5kYXRhLnVzZXJJZCA9PSB1c2VySWQpO1xuICB9XG5cbiAgZ2V0TXlVc2VyKCkge1xuICAgIHJldHVybiB0aGlzLm15VXNlckl0ZW07XG4gIH1cblxuICBnZXRPdGhlclVzZXJHcm91cCgpIHtcbiAgICByZXR1cm4gdGhpcy5vdGhlclVzZXJHcm91cDtcbiAgfVxuXG4gIHBsYXlCZXRBbmltYXRpb24odXNlcjogU2ljYm9Vc2VySXRlbUNvbXAsIHNsb3ROb2RlOiBjYy5Ob2RlKSB7XG4gICAgdXNlci5zaGFrZUJldChzbG90Tm9kZSk7XG4gIH1cblxuICB1cGRhdGVVc2Vyc0luUm9vbShkaXNwbGF5UGxheWVyczogSW5zdGFuY2VUeXBlPHR5cGVvZiBQbGF5YWg+W10sIHJlbWFpbmluZ1BsYXllcnNDb3VudDogbnVtYmVyKSB7XG4gICAgdGhpcy5yZW1haW5pbmdQbGF5ZXJzQ291bnQgPSBNYXRoLm1heChyZW1haW5pbmdQbGF5ZXJzQ291bnQsIDApO1xuICAgIHRoaXMub3RoZXJVc2VyR3JvdXAubm9kZS5hY3RpdmUgPSByZW1haW5pbmdQbGF5ZXJzQ291bnQgPiAwO1xuICAgIGlmIChyZW1haW5pbmdQbGF5ZXJzQ291bnQgPiA5OTkpIHRoaXMucmVtYWluaW5nVXNlcnNMYWJlbC5zdHJpbmcgPSBTaWNib1RleHQubG90c09mVXNlclRleHQ7XG4gICAgZWxzZSB0aGlzLnJlbWFpbmluZ1VzZXJzTGFiZWwuc3RyaW5nID0gcmVtYWluaW5nUGxheWVyc0NvdW50LnRvU3RyaW5nKCk7XG5cbiAgICBpZiAodGhpcy5pc1NhbWVVc2VyRGF0YShkaXNwbGF5UGxheWVycykpIHJldHVybjtcbiAgICBpZiAodGhpcy5pc1VzZXJJblJvb21BbmltUGxheWluZykgcmV0dXJuO1xuXG4gICAgdGhpcy5jYWNoZURpc3BsYXlQbGF5ZXJzID0gZGlzcGxheVBsYXllcnM7XG5cbiAgICB0aGlzLnBsYXlVc2VySW5Sb29tQW5pbWF0aW9uKGRpc3BsYXlQbGF5ZXJzKTtcbiAgfVxuXG4gIHByaXZhdGUgcGxheVVzZXJJblJvb21BbmltYXRpb24oZGlzcGxheVBsYXllcnM6IEluc3RhbmNlVHlwZTx0eXBlb2YgUGxheWFoPltdKSB7XG4gICAgbGV0IGFuaW1EdXJhdGlvbiA9IFNpY2JvU2V0dGluZy5VU0VSX0FOSU1fRFVSQVRJT047XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuXG4gICAgbGV0IGFuaW1Db3VudCA9IDA7XG4gICAgdGhpcy51c2VyQ29tcEFuaW1NYXAuZm9yRWFjaCgodGFyZ2V0SW5kZXg6IG51bWJlciwgdXNlckNvbXA6IFNpY2JvVXNlckl0ZW1Db21wKSA9PiB7XG4gICAgICB0aGlzLnVzZXJDb21wQW5pbU1hcC5zZXQodXNlckNvbXAsIG51bGwpO1xuICAgIH0pO1xuXG4gICAgLy9jb25zb2xlLmVycm9yKFwiLS0tLS0tLS0tLS0tLVwiLCBkaXNwbGF5UGxheWVycy5sZW5ndGgpXG4gICAgLy8gZm9yIChsZXQgaSA9IDA7IGkgPCBkaXNwbGF5UGxheWVycy5sZW5ndGg7IGkrKykge2NvbnNvbGUuZXJyb3IoZGlzcGxheVBsYXllcnNbaV0uZ2V0VXNlcklkKCksIGRpc3BsYXlQbGF5ZXJzW2ldLmdldFByb2ZpbGUoKS5nZXREaXNwbGF5TmFtZSgpKX1cbiAgICAvL3VzZXIgZGEgY28gLSBjbyBzbG90IG1vaVxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZGlzcGxheVBsYXllcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGxldCB1c2VySWQgPSBkaXNwbGF5UGxheWVyc1tpXS5nZXRVc2VySWQoKTtcbiAgICAgIGxldCB1c2VyQ29tcCA9IHRoaXMudXNlcnNJblJvb20uZmluZCgoeCkgPT4geC5nZXRVc2VySWQoKSA9PSB1c2VySWQpO1xuICAgICAgaWYgKHVzZXJDb21wICE9IG51bGwpIHtcbiAgICAgICAgLy9jb25zb2xlLmVycm9yKFwiYW5pbVwiLCB1c2VySWQsIGRpc3BsYXlQbGF5ZXJzW2ldLmdldFByb2ZpbGUoKS5nZXREaXNwbGF5TmFtZSgpLCBpKVxuICAgICAgICBzZWxmLnVzZXJDb21wQW5pbU1hcC5zZXQodXNlckNvbXAsIGkpO1xuICAgICAgICBpZiAodGhpcy5pc1VzZUxpbmVhckFuaW0odXNlckNvbXAuZ2V0Q2hhdEluZGV4KCksIGkpKSB7XG4gICAgICAgICAgY2MudHdlZW4odXNlckNvbXAubm9kZSkudG8oYW5pbUR1cmF0aW9uLCB7IHg6IHNlbGYudXNlcnNDYWNoZVBvc1tpXS54LCB5OiBzZWxmLnVzZXJzQ2FjaGVQb3NbaV0ueSB9LCB7IGVhc2luZzogXCJjdWJpY0luT3V0XCIgfSkuc3RhcnQoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBsZXQgc3RhcnRQb3MgPSBuZXcgY2MuVmVjMih1c2VyQ29tcC5ub2RlLngsIHVzZXJDb21wLm5vZGUucG9zaXRpb24ueSk7XG4gICAgICAgICAgbGV0IGVuZFBvcyA9IG5ldyBjYy5WZWMyKHNlbGYudXNlcnNDYWNoZVBvc1tpXS54LCBzZWxmLnVzZXJzQ2FjaGVQb3NbaV0ueSk7XG4gICAgICAgICAgY2MudHdlZW4odXNlckNvbXAubm9kZSkuYmV6aWVyVG8oYW5pbUR1cmF0aW9uLCBzdGFydFBvcywgbmV3IGNjLlZlYzIoMCwgLTE3MCksIGVuZFBvcykuc3RhcnQoKTtcbiAgICAgICAgfVxuICAgICAgICB1c2VyQ29tcC5zZXREYXRhKGRpc3BsYXlQbGF5ZXJzW2ldLCBpKTtcbiAgICAgICAgYW5pbUNvdW50Kys7XG4gICAgICB9XG4gICAgfVxuXG4gICAgbGV0IGZyZWVDb21wczogU2ljYm9Vc2VySXRlbUNvbXBbXSA9IFtdO1xuICAgIHRoaXMudXNlckNvbXBBbmltTWFwLmZvckVhY2goKGk6IG51bWJlciwgdXNlckNvbXA6IFNpY2JvVXNlckl0ZW1Db21wKSA9PiB7XG4gICAgICBpZiAoaSA9PSBudWxsKSBmcmVlQ29tcHMucHVzaCh1c2VyQ29tcCk7XG4gICAgfSk7XG5cbiAgICAvL05PIFBMQVlBSCBDT01QXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBmcmVlQ29tcHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGZyZWVDb21wc1tpXS5zZXREYXRhKCk7XG4gICAgfVxuXG4gICAgY2MudHdlZW4odGhpcy5ub2RlKVxuICAgICAgLmRlbGF5KGFuaW1EdXJhdGlvbiAvIDIpXG4gICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgIC8vTkVXIFBsYXlhaFxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGRpc3BsYXlQbGF5ZXJzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgbGV0IHVzZXJJZCA9IGRpc3BsYXlQbGF5ZXJzW2ldLmdldFVzZXJJZCgpO1xuICAgICAgICAgIGxldCB1c2VyQ29tcCA9IHNlbGYudXNlcnNJblJvb20uZmluZCgoeCkgPT4geC5nZXRVc2VySWQoKSA9PSB1c2VySWQpO1xuICAgICAgICAgIGlmICh1c2VyQ29tcCA9PSBudWxsKSB7XG4gICAgICAgICAgICAvL2NvbnNvbGUuZXJyb3IoXCJzZXRcIiwgdXNlcklkLCBkaXNwbGF5UGxheWVyc1tpXS5nZXRQcm9maWxlKCkuZ2V0RGlzcGxheU5hbWUoKSwgaSlcblxuICAgICAgICAgICAgdXNlckNvbXAgPSBmcmVlQ29tcHMucG9wKCk7XG4gICAgICAgICAgICBpZiAodXNlckNvbXAgIT0gbnVsbCkge1xuICAgICAgICAgICAgICB1c2VyQ29tcC5zZXREYXRhKGRpc3BsYXlQbGF5ZXJzW2ldLCBpKTtcblxuICAgICAgICAgICAgICBjYy50d2Vlbih1c2VyQ29tcC5ub2RlKS50bygwLCB7IHg6IHNlbGYudXNlcnNDYWNoZVBvc1tpXS54LCB5OiBzZWxmLnVzZXJzQ2FjaGVQb3NbaV0ueSB9KS5zdGFydCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICAgIC5zdGFydCgpO1xuXG4gICAgLy9BTklNIERFTEFZXG4gICAgaWYgKGFuaW1Db3VudCA+IDApIHtcbiAgICAgIHNlbGYuaXNVc2VySW5Sb29tQW5pbVBsYXlpbmcgPSB0cnVlO1xuICAgICAgY2MudHdlZW4odGhpcy5ub2RlKVxuICAgICAgICAuZGVsYXkoYW5pbUR1cmF0aW9uKVxuICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgc2VsZi5pc1VzZXJJblJvb21BbmltUGxheWluZyA9IGZhbHNlO1xuICAgICAgICB9KVxuICAgICAgICAuc3RhcnQoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgc2VsZi5pc1VzZXJJblJvb21BbmltUGxheWluZyA9IGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIGlzU2FtZVVzZXJEYXRhKGRpc3BsYXlQbGF5ZXJzOiBJbnN0YW5jZVR5cGU8dHlwZW9mIFBsYXlhaD5bXSkge1xuICAgIHJldHVybiBmYWxzZTtcblxuICAgIC8vIGlmKHRoaXMuY2FjaGVEaXNwbGF5UGxheWVycyA9PSBudWxsIHx8IHRoaXMuY2FjaGVEaXNwbGF5UGxheWVycy5sZW5ndGggPT0gMCkgcmV0dXJuIGZhbHNlO1xuICAgIC8vIGlmKHRoaXMuY2FjaGVEaXNwbGF5UGxheWVycy5sZW5ndGggIT0gZGlzcGxheVBsYXllcnMubGVuZ3RoKSByZXR1cm4gZmFsc2U7XG5cbiAgICAvLyBmb3IgKGxldCBpID0gMDsgaSA8IGRpc3BsYXlQbGF5ZXJzLmxlbmd0aDsgaSsrKSB7XG4gICAgLy8gICAgIGlmKHRoaXMuY2FjaGVEaXNwbGF5UGxheWVyc1tpXS5nZXRVc2VySWQoKSAhPSBkaXNwbGF5UGxheWVyc1tpXS5nZXRVc2VySWQoKSkgcmV0dXJuIGZhbHNlO1xuICAgIC8vIH1cbiAgICAvLyByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGlzVXNlTGluZWFyQW5pbShteUluZGV4OiBudW1iZXIsIG90aGVySW5kZXg6IG51bWJlcikge1xuICAgIGxldCBpc1NhbWVTaWRlID0gTWF0aC5mbG9vcihteUluZGV4IC8gMykgPT0gTWF0aC5mbG9vcihvdGhlckluZGV4IC8gMyk7XG4gICAgbGV0IGlzU2FtZVJvdyA9IG15SW5kZXggJSAzID09IG90aGVySW5kZXggJSAzO1xuXG4gICAgcmV0dXJuIGlzU2FtZVNpZGUgfHwgaXNTYW1lUm93O1xuICB9XG59XG4iXX0=