
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Setting/SicboSetting.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '59f0d6hAmZPSZ8S1RAl3kdw', 'SicboSetting');
// Game/Sicbo_New/Scripts/Setting/SicboSetting.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboSound = exports.SicboScene = void 0;
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Code = _a.Code, Status = _a.Status, ChipLevels = _a.ChipLevels, StageTime = _a.StageTime;
var SicboScene;
(function (SicboScene) {
    SicboScene["Loading"] = "SicboLoadingScene";
    SicboScene["Main"] = "SicboScene";
})(SicboScene = exports.SicboScene || (exports.SicboScene = {}));
var SicboSetting = /** @class */ (function () {
    function SicboSetting() {
    }
    SicboSetting.loadDataFromForge = function (onProgress, onComplete) {
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (onProgress)
            onProgress(0, 0);
        if (onComplete)
            onComplete();
    };
    Object.defineProperty(SicboSetting, "chipLevels", {
        get: function () {
            return SicboSetting._chipLevels;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboSetting, "statusAndDuration", {
        get: function () {
            return SicboSetting._statusAndDuration;
        },
        enumerable: false,
        configurable: true
    });
    SicboSetting.getStatusDuration = function (status) {
        switch (status) {
            case Status.WAITING:
                return SicboSetting._statusAndDuration.getWaiting();
            case Status.BETTING:
                return SicboSetting._statusAndDuration.getBetting();
            case Status.END_BET:
                return SicboSetting._statusAndDuration.getEndBet();
            case Status.RESULTING:
                return SicboSetting._statusAndDuration.getResulting();
            case Status.PAYING:
                return SicboSetting._statusAndDuration.getPaying();
            case Status.FINISHING:
                return SicboSetting._statusAndDuration.getFinishing();
            default:
                return 0;
        }
    };
    SicboSetting.getChipLevelList = function () {
        return SicboSetting._chipLevels.getChipLevelList();
    };
    SicboSetting.setChipAndStatusDurationConfig = function (chipLevels, statusAndDuration) {
        SicboSetting._chipLevels = chipLevels;
        SicboSetting._statusAndDuration = statusAndDuration;
    };
    SicboSetting.setStatusDurationConfig = function (statusAndDuration) {
        SicboSetting._statusAndDuration = statusAndDuration;
    };
    SicboSetting.getSoundName = function (sound) {
        return SicboSound[sound];
        // switch (sound) {
        //   case SicboSound.BGM: return "bgm-main_game1";
        //   case SicboSound.SfxDealerShake: return "sfx-dice_shake_long";
        //   case SicboSound.SfxClick: return "SFX_ButtonTap";
        //   case SicboSound.SfxBet: return "sfx-bet";
        //   case SicboSound.SfxRush: return "sfx-rush";
        //   case SicboSound.SfxTictoc: return "sfx_tictoc";
        //   case SicboSound.SfxStopBetting: return "sfx-stop_betting1";
        //   case SicboSound.SfxResult: return "sfx-result";
        //   case SicboSound.SfxJackpotScale: return "sfx_jackpot_scale";
        //   case SicboSound.SfxJackpotFinal: return "sfx_jackpot_final";
        //   case SicboSound.SfxJackpotCoin: return "sfx-jackpot_coin1";
        //   case SicboSound.SfxWin: return "sfx-win";
        //   case SicboSound.SfxTotalWin: return "sfx-total_win2";
        //   case SicboSound.SfxMessage: return "sfx-message2";
        //   case SicboSound.SfxChipFly: return "sfx-bet";
        // }
    };
    SicboSetting.getSoundDuration = function (sound) {
        switch (sound) {
            // case SicboSound.BGM: return "bgm-main_game1";
            // case SicboSound.SfxDealerShake: return "sfx-dice_shake_long";
            // case SicboSound.SfxClick: return "sfx-click2";
            // case SicboSound.SfxBet: return "sfx-bet";
            // case SicboSound.SfxRush: return "sfx-rush";
            // case SicboSound.SfxTictoc: return "sfx-tictoc2";
            // case SicboSound.SfxStopBetting: return "sfx-stop_betting1";
            // case SicboSound.SfxResult: return "sfx-result";
            // case SicboSound.SfxJackpotScale: return "sfx-jackpot_scale1";
            // case SicboSound.SfxJackpotFinal: return "sfx-jackpot_final1";
            // case SicboSound.SfxJackpotCoin: return "sfx-jackpot_coin1";
            // case SicboSound.SfxWin: return "sfx-win";
            // case SicboSound.SfxTotalWin: return "sfx-total_win2";
            // case SicboSound.SfxMessage: return "sfx-message";
            case SicboSound.SfxChipFly:
                return 0.134;
            default:
                //cc.log("BCF WARNING: NOT HAVE DURATION SETTING")
                return 0;
        }
    };
    SicboSetting.USER_ANIM_DURATION = 0.77;
    SicboSetting.resourceLocalPaths = [];
    SicboSetting.resourceRemotePaths = [];
    SicboSetting.loadingBackgroundLocalPath = "";
    SicboSetting.loadingBackgroundOverrideProgressBar = false;
    SicboSetting._chipLevels = null;
    SicboSetting._statusAndDuration = null;
    return SicboSetting;
}());
exports.default = SicboSetting;
var SicboSound;
(function (SicboSound) {
    SicboSound[SicboSound["BGM"] = 0] = "BGM";
    SicboSound[SicboSound["SfxDealerShake"] = 1] = "SfxDealerShake";
    SicboSound[SicboSound["SfxClick"] = 2] = "SfxClick";
    SicboSound[SicboSound["SfxBet"] = 3] = "SfxBet";
    SicboSound[SicboSound["SfxRush"] = 4] = "SfxRush";
    SicboSound[SicboSound["SfxTictoc"] = 5] = "SfxTictoc";
    SicboSound[SicboSound["SfxStopBetting"] = 6] = "SfxStopBetting";
    SicboSound[SicboSound["SfxResult"] = 7] = "SfxResult";
    SicboSound[SicboSound["SfxJackpotScale"] = 8] = "SfxJackpotScale";
    SicboSound[SicboSound["SfxJackpotFinal"] = 9] = "SfxJackpotFinal";
    SicboSound[SicboSound["SfxJackpotCoin"] = 10] = "SfxJackpotCoin";
    SicboSound[SicboSound["SfxWin"] = 11] = "SfxWin";
    SicboSound[SicboSound["SfxTotalWin"] = 12] = "SfxTotalWin";
    SicboSound[SicboSound["SfxMessage"] = 13] = "SfxMessage";
    SicboSound[SicboSound["SfxChipFly"] = 14] = "SfxChipFly";
})(SicboSound = exports.SicboSound || (exports.SicboSound = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1NldHRpbmcvU2ljYm9TZXR0aW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHFFQUFnRTtBQUUxRCxJQUFBLEtBQTBDLDRCQUFrQixDQUFDLFVBQVUsRUFBRSxFQUF2RSxJQUFJLFVBQUEsRUFBRSxNQUFNLFlBQUEsRUFBRSxVQUFVLGdCQUFBLEVBQUUsU0FBUyxlQUFvQyxDQUFDO0FBRWhGLElBQVksVUFHWDtBQUhELFdBQVksVUFBVTtJQUNwQiwyQ0FBNkIsQ0FBQTtJQUM3QixpQ0FBbUIsQ0FBQTtBQUNyQixDQUFDLEVBSFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFHckI7QUFFRDtJQUFBO0lBK0dBLENBQUM7SUF4R1EsOEJBQWlCLEdBQXhCLFVBQ0UsVUFBMkMsRUFDM0MsVUFBNkI7UUFEN0IsMkJBQUEsRUFBQSxpQkFBMkM7UUFDM0MsMkJBQUEsRUFBQSxpQkFBNkI7UUFFN0IsSUFBSSxVQUFVO1lBQUUsVUFBVSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNqQyxJQUFJLFVBQVU7WUFBRSxVQUFVLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBR0Qsc0JBQWtCLDBCQUFVO2FBQTVCO1lBQ0UsT0FBTyxZQUFZLENBQUMsV0FBVyxDQUFDO1FBQ2xDLENBQUM7OztPQUFBO0lBR0Qsc0JBQWtCLGlDQUFpQjthQUFuQztZQUNFLE9BQU8sWUFBWSxDQUFDLGtCQUFrQixDQUFDO1FBQ3pDLENBQUM7OztPQUFBO0lBRWEsOEJBQWlCLEdBQS9CLFVBQWdDLE1BQU07UUFDcEMsUUFBUSxNQUFNLEVBQUU7WUFDZCxLQUFLLE1BQU0sQ0FBQyxPQUFPO2dCQUNqQixPQUFPLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUV0RCxLQUFLLE1BQU0sQ0FBQyxPQUFPO2dCQUNqQixPQUFPLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUV0RCxLQUFLLE1BQU0sQ0FBQyxPQUFPO2dCQUNqQixPQUFPLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUVyRCxLQUFLLE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixPQUFPLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUV4RCxLQUFLLE1BQU0sQ0FBQyxNQUFNO2dCQUNoQixPQUFPLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUVyRCxLQUFLLE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixPQUFPLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUN4RDtnQkFDRSxPQUFPLENBQUMsQ0FBQztTQUNaO0lBQ0gsQ0FBQztJQUVhLDZCQUFnQixHQUE5QjtRQUNFLE9BQU8sWUFBWSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ3JELENBQUM7SUFDYSwyQ0FBOEIsR0FBNUMsVUFDRSxVQUEyQyxFQUMzQyxpQkFBaUQ7UUFFakQsWUFBWSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUM7UUFDdEMsWUFBWSxDQUFDLGtCQUFrQixHQUFHLGlCQUFpQixDQUFDO0lBQ3RELENBQUM7SUFFYSxvQ0FBdUIsR0FBckMsVUFDRSxpQkFBaUQ7UUFFakQsWUFBWSxDQUFDLGtCQUFrQixHQUFHLGlCQUFpQixDQUFDO0lBQ3RELENBQUM7SUFFYSx5QkFBWSxHQUExQixVQUEyQixLQUFpQjtRQUMxQyxPQUFPLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixtQkFBbUI7UUFDbkIsa0RBQWtEO1FBQ2xELGtFQUFrRTtRQUNsRSxzREFBc0Q7UUFDdEQsOENBQThDO1FBQzlDLGdEQUFnRDtRQUNoRCxvREFBb0Q7UUFDcEQsZ0VBQWdFO1FBQ2hFLG9EQUFvRDtRQUNwRCxpRUFBaUU7UUFDakUsaUVBQWlFO1FBQ2pFLGdFQUFnRTtRQUNoRSw4Q0FBOEM7UUFDOUMsMERBQTBEO1FBQzFELHVEQUF1RDtRQUN2RCxrREFBa0Q7UUFDbEQsSUFBSTtJQUNOLENBQUM7SUFFYSw2QkFBZ0IsR0FBOUIsVUFBK0IsS0FBaUI7UUFDOUMsUUFBUSxLQUFLLEVBQUU7WUFDYixnREFBZ0Q7WUFDaEQsZ0VBQWdFO1lBQ2hFLGlEQUFpRDtZQUNqRCw0Q0FBNEM7WUFDNUMsOENBQThDO1lBQzlDLG1EQUFtRDtZQUNuRCw4REFBOEQ7WUFDOUQsa0RBQWtEO1lBQ2xELGdFQUFnRTtZQUNoRSxnRUFBZ0U7WUFDaEUsOERBQThEO1lBQzlELDRDQUE0QztZQUM1Qyx3REFBd0Q7WUFDeEQsb0RBQW9EO1lBQ3BELEtBQUssVUFBVSxDQUFDLFVBQVU7Z0JBQ3hCLE9BQU8sS0FBSyxDQUFDO1lBRWY7Z0JBQ0Usa0RBQWtEO2dCQUNsRCxPQUFPLENBQUMsQ0FBQztTQUNaO0lBQ0gsQ0FBQztJQTdHYSwrQkFBa0IsR0FBRyxJQUFJLENBQUM7SUFDakMsK0JBQWtCLEdBQWEsRUFBRSxDQUFDO0lBQ2xDLGdDQUFtQixHQUFhLEVBQUUsQ0FBQztJQUNuQyx1Q0FBMEIsR0FBVyxFQUFFLENBQUM7SUFDeEMsaURBQW9DLEdBQVksS0FBSyxDQUFDO0lBVTlDLHdCQUFXLEdBQW9DLElBQUksQ0FBQztJQUtwRCwrQkFBa0IsR0FBbUMsSUFBSSxDQUFDO0lBMkYzRSxtQkFBQztDQS9HRCxBQStHQyxJQUFBO2tCQS9Hb0IsWUFBWTtBQWlIakMsSUFBWSxVQWdCWDtBQWhCRCxXQUFZLFVBQVU7SUFDcEIseUNBQU8sQ0FBQTtJQUNQLCtEQUFrQixDQUFBO0lBQ2xCLG1EQUFZLENBQUE7SUFDWiwrQ0FBVSxDQUFBO0lBQ1YsaURBQVcsQ0FBQTtJQUNYLHFEQUFhLENBQUE7SUFDYiwrREFBa0IsQ0FBQTtJQUNsQixxREFBYSxDQUFBO0lBQ2IsaUVBQW1CLENBQUE7SUFDbkIsaUVBQW1CLENBQUE7SUFDbkIsZ0VBQW1CLENBQUE7SUFDbkIsZ0RBQVcsQ0FBQTtJQUNYLDBEQUFnQixDQUFBO0lBQ2hCLHdEQUFlLENBQUE7SUFDZix3REFBZSxDQUFBO0FBQ2pCLENBQUMsRUFoQlcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFnQnJCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFNpY2JvTW9kdWxlQWRhcHRlciBmcm9tIFwiLi4vLi4vLi4vLi4vU2ljYm9Nb2R1bGVBZGFwdGVyXCI7XG5cbmNvbnN0IHsgQ29kZSwgU3RhdHVzLCBDaGlwTGV2ZWxzLCBTdGFnZVRpbWUgfSA9IFNpY2JvTW9kdWxlQWRhcHRlci5nZXRBbGxSZWZzKCk7XG5cbmV4cG9ydCBlbnVtIFNpY2JvU2NlbmUge1xuICBMb2FkaW5nID0gXCJTaWNib0xvYWRpbmdTY2VuZVwiLFxuICBNYWluID0gXCJTaWNib1NjZW5lXCIsXG59XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvU2V0dGluZyB7XG4gIHB1YmxpYyBzdGF0aWMgVVNFUl9BTklNX0RVUkFUSU9OID0gMC43NztcbiAgc3RhdGljIHJlc291cmNlTG9jYWxQYXRoczogc3RyaW5nW10gPSBbXTtcbiAgc3RhdGljIHJlc291cmNlUmVtb3RlUGF0aHM6IHN0cmluZ1tdID0gW107XG4gIHN0YXRpYyBsb2FkaW5nQmFja2dyb3VuZExvY2FsUGF0aDogc3RyaW5nID0gXCJcIjtcbiAgc3RhdGljIGxvYWRpbmdCYWNrZ3JvdW5kT3ZlcnJpZGVQcm9ncmVzc0JhcjogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIHN0YXRpYyBsb2FkRGF0YUZyb21Gb3JnZShcbiAgICBvblByb2dyZXNzOiAoY3VycmVudCwgdG90YWwpID0+IHZvaWQgPSBudWxsLFxuICAgIG9uQ29tcGxldGU6ICgpID0+IHZvaWQgPSBudWxsXG4gICkge1xuICAgIGlmIChvblByb2dyZXNzKSBvblByb2dyZXNzKDAsIDApO1xuICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKCk7XG4gIH1cblxuICBwcml2YXRlIHN0YXRpYyBfY2hpcExldmVsczogSW5zdGFuY2VUeXBlPHR5cGVvZiBDaGlwTGV2ZWxzPiA9IG51bGw7XG4gIHB1YmxpYyBzdGF0aWMgZ2V0IGNoaXBMZXZlbHMoKTogSW5zdGFuY2VUeXBlPHR5cGVvZiBDaGlwTGV2ZWxzPiB7XG4gICAgcmV0dXJuIFNpY2JvU2V0dGluZy5fY2hpcExldmVscztcbiAgfVxuXG4gIHByaXZhdGUgc3RhdGljIF9zdGF0dXNBbmREdXJhdGlvbjogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGFnZVRpbWU+ID0gbnVsbDtcbiAgcHVibGljIHN0YXRpYyBnZXQgc3RhdHVzQW5kRHVyYXRpb24oKTogSW5zdGFuY2VUeXBlPHR5cGVvZiBTdGFnZVRpbWU+IHtcbiAgICByZXR1cm4gU2ljYm9TZXR0aW5nLl9zdGF0dXNBbmREdXJhdGlvbjtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZ2V0U3RhdHVzRHVyYXRpb24oc3RhdHVzKTogbnVtYmVyIHtcbiAgICBzd2l0Y2ggKHN0YXR1cykge1xuICAgICAgY2FzZSBTdGF0dXMuV0FJVElORzpcbiAgICAgICAgcmV0dXJuIFNpY2JvU2V0dGluZy5fc3RhdHVzQW5kRHVyYXRpb24uZ2V0V2FpdGluZygpO1xuXG4gICAgICBjYXNlIFN0YXR1cy5CRVRUSU5HOlxuICAgICAgICByZXR1cm4gU2ljYm9TZXR0aW5nLl9zdGF0dXNBbmREdXJhdGlvbi5nZXRCZXR0aW5nKCk7XG5cbiAgICAgIGNhc2UgU3RhdHVzLkVORF9CRVQ6XG4gICAgICAgIHJldHVybiBTaWNib1NldHRpbmcuX3N0YXR1c0FuZER1cmF0aW9uLmdldEVuZEJldCgpO1xuXG4gICAgICBjYXNlIFN0YXR1cy5SRVNVTFRJTkc6XG4gICAgICAgIHJldHVybiBTaWNib1NldHRpbmcuX3N0YXR1c0FuZER1cmF0aW9uLmdldFJlc3VsdGluZygpO1xuXG4gICAgICBjYXNlIFN0YXR1cy5QQVlJTkc6XG4gICAgICAgIHJldHVybiBTaWNib1NldHRpbmcuX3N0YXR1c0FuZER1cmF0aW9uLmdldFBheWluZygpO1xuXG4gICAgICBjYXNlIFN0YXR1cy5GSU5JU0hJTkc6XG4gICAgICAgIHJldHVybiBTaWNib1NldHRpbmcuX3N0YXR1c0FuZER1cmF0aW9uLmdldEZpbmlzaGluZygpO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIDA7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIHN0YXRpYyBnZXRDaGlwTGV2ZWxMaXN0KCkge1xuICAgIHJldHVybiBTaWNib1NldHRpbmcuX2NoaXBMZXZlbHMuZ2V0Q2hpcExldmVsTGlzdCgpO1xuICB9XG4gIHB1YmxpYyBzdGF0aWMgc2V0Q2hpcEFuZFN0YXR1c0R1cmF0aW9uQ29uZmlnKFxuICAgIGNoaXBMZXZlbHM6IEluc3RhbmNlVHlwZTx0eXBlb2YgQ2hpcExldmVscz4sXG4gICAgc3RhdHVzQW5kRHVyYXRpb246IEluc3RhbmNlVHlwZTx0eXBlb2YgU3RhZ2VUaW1lPlxuICApIHtcbiAgICBTaWNib1NldHRpbmcuX2NoaXBMZXZlbHMgPSBjaGlwTGV2ZWxzO1xuICAgIFNpY2JvU2V0dGluZy5fc3RhdHVzQW5kRHVyYXRpb24gPSBzdGF0dXNBbmREdXJhdGlvbjtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgc2V0U3RhdHVzRHVyYXRpb25Db25maWcoXG4gICAgc3RhdHVzQW5kRHVyYXRpb246IEluc3RhbmNlVHlwZTx0eXBlb2YgU3RhZ2VUaW1lPlxuICApIHtcbiAgICBTaWNib1NldHRpbmcuX3N0YXR1c0FuZER1cmF0aW9uID0gc3RhdHVzQW5kRHVyYXRpb247XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGdldFNvdW5kTmFtZShzb3VuZDogU2ljYm9Tb3VuZCkge1xuICAgIHJldHVybiBTaWNib1NvdW5kW3NvdW5kXTtcbiAgICAvLyBzd2l0Y2ggKHNvdW5kKSB7XG4gICAgLy8gICBjYXNlIFNpY2JvU291bmQuQkdNOiByZXR1cm4gXCJiZ20tbWFpbl9nYW1lMVwiO1xuICAgIC8vICAgY2FzZSBTaWNib1NvdW5kLlNmeERlYWxlclNoYWtlOiByZXR1cm4gXCJzZngtZGljZV9zaGFrZV9sb25nXCI7XG4gICAgLy8gICBjYXNlIFNpY2JvU291bmQuU2Z4Q2xpY2s6IHJldHVybiBcIlNGWF9CdXR0b25UYXBcIjtcbiAgICAvLyAgIGNhc2UgU2ljYm9Tb3VuZC5TZnhCZXQ6IHJldHVybiBcInNmeC1iZXRcIjtcbiAgICAvLyAgIGNhc2UgU2ljYm9Tb3VuZC5TZnhSdXNoOiByZXR1cm4gXCJzZngtcnVzaFwiO1xuICAgIC8vICAgY2FzZSBTaWNib1NvdW5kLlNmeFRpY3RvYzogcmV0dXJuIFwic2Z4X3RpY3RvY1wiO1xuICAgIC8vICAgY2FzZSBTaWNib1NvdW5kLlNmeFN0b3BCZXR0aW5nOiByZXR1cm4gXCJzZngtc3RvcF9iZXR0aW5nMVwiO1xuICAgIC8vICAgY2FzZSBTaWNib1NvdW5kLlNmeFJlc3VsdDogcmV0dXJuIFwic2Z4LXJlc3VsdFwiO1xuICAgIC8vICAgY2FzZSBTaWNib1NvdW5kLlNmeEphY2twb3RTY2FsZTogcmV0dXJuIFwic2Z4X2phY2twb3Rfc2NhbGVcIjtcbiAgICAvLyAgIGNhc2UgU2ljYm9Tb3VuZC5TZnhKYWNrcG90RmluYWw6IHJldHVybiBcInNmeF9qYWNrcG90X2ZpbmFsXCI7XG4gICAgLy8gICBjYXNlIFNpY2JvU291bmQuU2Z4SmFja3BvdENvaW46IHJldHVybiBcInNmeC1qYWNrcG90X2NvaW4xXCI7XG4gICAgLy8gICBjYXNlIFNpY2JvU291bmQuU2Z4V2luOiByZXR1cm4gXCJzZngtd2luXCI7XG4gICAgLy8gICBjYXNlIFNpY2JvU291bmQuU2Z4VG90YWxXaW46IHJldHVybiBcInNmeC10b3RhbF93aW4yXCI7XG4gICAgLy8gICBjYXNlIFNpY2JvU291bmQuU2Z4TWVzc2FnZTogcmV0dXJuIFwic2Z4LW1lc3NhZ2UyXCI7XG4gICAgLy8gICBjYXNlIFNpY2JvU291bmQuU2Z4Q2hpcEZseTogcmV0dXJuIFwic2Z4LWJldFwiO1xuICAgIC8vIH1cbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZ2V0U291bmREdXJhdGlvbihzb3VuZDogU2ljYm9Tb3VuZCkge1xuICAgIHN3aXRjaCAoc291bmQpIHtcbiAgICAgIC8vIGNhc2UgU2ljYm9Tb3VuZC5CR006IHJldHVybiBcImJnbS1tYWluX2dhbWUxXCI7XG4gICAgICAvLyBjYXNlIFNpY2JvU291bmQuU2Z4RGVhbGVyU2hha2U6IHJldHVybiBcInNmeC1kaWNlX3NoYWtlX2xvbmdcIjtcbiAgICAgIC8vIGNhc2UgU2ljYm9Tb3VuZC5TZnhDbGljazogcmV0dXJuIFwic2Z4LWNsaWNrMlwiO1xuICAgICAgLy8gY2FzZSBTaWNib1NvdW5kLlNmeEJldDogcmV0dXJuIFwic2Z4LWJldFwiO1xuICAgICAgLy8gY2FzZSBTaWNib1NvdW5kLlNmeFJ1c2g6IHJldHVybiBcInNmeC1ydXNoXCI7XG4gICAgICAvLyBjYXNlIFNpY2JvU291bmQuU2Z4VGljdG9jOiByZXR1cm4gXCJzZngtdGljdG9jMlwiO1xuICAgICAgLy8gY2FzZSBTaWNib1NvdW5kLlNmeFN0b3BCZXR0aW5nOiByZXR1cm4gXCJzZngtc3RvcF9iZXR0aW5nMVwiO1xuICAgICAgLy8gY2FzZSBTaWNib1NvdW5kLlNmeFJlc3VsdDogcmV0dXJuIFwic2Z4LXJlc3VsdFwiO1xuICAgICAgLy8gY2FzZSBTaWNib1NvdW5kLlNmeEphY2twb3RTY2FsZTogcmV0dXJuIFwic2Z4LWphY2twb3Rfc2NhbGUxXCI7XG4gICAgICAvLyBjYXNlIFNpY2JvU291bmQuU2Z4SmFja3BvdEZpbmFsOiByZXR1cm4gXCJzZngtamFja3BvdF9maW5hbDFcIjtcbiAgICAgIC8vIGNhc2UgU2ljYm9Tb3VuZC5TZnhKYWNrcG90Q29pbjogcmV0dXJuIFwic2Z4LWphY2twb3RfY29pbjFcIjtcbiAgICAgIC8vIGNhc2UgU2ljYm9Tb3VuZC5TZnhXaW46IHJldHVybiBcInNmeC13aW5cIjtcbiAgICAgIC8vIGNhc2UgU2ljYm9Tb3VuZC5TZnhUb3RhbFdpbjogcmV0dXJuIFwic2Z4LXRvdGFsX3dpbjJcIjtcbiAgICAgIC8vIGNhc2UgU2ljYm9Tb3VuZC5TZnhNZXNzYWdlOiByZXR1cm4gXCJzZngtbWVzc2FnZVwiO1xuICAgICAgY2FzZSBTaWNib1NvdW5kLlNmeENoaXBGbHk6XG4gICAgICAgIHJldHVybiAwLjEzNDtcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgLy9jYy5sb2coXCJCQ0YgV0FSTklORzogTk9UIEhBVkUgRFVSQVRJT04gU0VUVElOR1wiKVxuICAgICAgICByZXR1cm4gMDtcbiAgICB9XG4gIH1cbn1cblxuZXhwb3J0IGVudW0gU2ljYm9Tb3VuZCB7XG4gIEJHTSA9IDAsXG4gIFNmeERlYWxlclNoYWtlID0gMSxcbiAgU2Z4Q2xpY2sgPSAyLFxuICBTZnhCZXQgPSAzLFxuICBTZnhSdXNoID0gNCxcbiAgU2Z4VGljdG9jID0gNSxcbiAgU2Z4U3RvcEJldHRpbmcgPSA2LFxuICBTZnhSZXN1bHQgPSA3LFxuICBTZnhKYWNrcG90U2NhbGUgPSA4LFxuICBTZnhKYWNrcG90RmluYWwgPSA5LFxuICBTZnhKYWNrcG90Q29pbiA9IDEwLFxuICBTZnhXaW4gPSAxMSxcbiAgU2Z4VG90YWxXaW4gPSAxMixcbiAgU2Z4TWVzc2FnZSA9IDEzLFxuICBTZnhDaGlwRmx5ID0gMTQsXG59XG4iXX0=