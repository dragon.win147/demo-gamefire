
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Setting/SicboText.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '50345ucc0NOfoRgTEOcmgYI', 'SicboText');
// Game/Sicbo_New/Scripts/Setting/SicboText.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var Code = SicboModuleAdapter_1.default.getAllRefs().Code;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboText = /** @class */ (function () {
    function SicboText() {
    }
    Object.defineProperty(SicboText, "handlerDefaultErrMsg", {
        get: function () {
            return "Có lỗi xảy ra xin vui lòng thử lại!";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "sessionCopyMessage", {
        get: function () {
            return "Sao Chép Thành Công Mã MD5";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "loadingMsg", {
        get: function () {
            return "Loading... {0}%";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "lotsOfUserText", {
        get: function () {
            return "999+";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "sessionIdText", {
        get: function () {
            return "#{0}";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "leaderboardUpdateTime", {
        get: function () {
            return "{0} phút";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "noLotteryValue", {
        get: function () {
            return "-";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "sessionDataBetHistoryDetail", {
        get: function () {
            return "#{0} ({1})";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "sessionLastFormat", {
        get: function () {
            return "Phiên gần đây nhất (#{0}) -";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "valueLastFormat", {
        get: function () {
            return "{0} {1}: ({2}-{3}-{4})";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "getCannotJoinMessageWhenNotIngame", {
        get: function () {
            return "Game Đang Tiến Hành Nâng Cấp Hệ Thống.\nQuý khách vui lòng quay lại sau!";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "getCannotJoinMessageWhenIngame", {
        get: function () {
            return "Xin lỗi quý khách, hiện tại hệ thống đang gặp sự cố.\nChúng tôi sẽ cố gắng khắc phục trong thời gian sớm nhất.\nChân thành xin lỗi vì sự bất tiện này.";
        },
        enumerable: false,
        configurable: true
    });
    SicboText.getErrorMessage = function (errorCode) {
        switch (errorCode) {
            case Code.SICBO_BET_LIMIT_REACHED:
                // return "Quý Khách Đã Đặt Mức Cược Tối Đa Cho Tụ Này";
                return "Số Tiền Đặt Cược Không Thể Vượt Mức Quy Định Của Tụ!";
            case Code.SICBO_BET_NOT_ENOUGH_MONEY:
                return "Số Dư Của Quý Khách Không Đủ Để Đặt Cược";
            default:
                return null;
        }
    };
    Object.defineProperty(SicboText, "waitingRemainingTimeMsg", {
        get: function () {
            return "Ván mới bắt đầu sau {0}s";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "kickMsg", {
        get: function () {
            return "Xin Mời Quý Khách Đặt Cược\nĐể Tiếp Tục Tham Gia Bàn Chơi.";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "leaveBookingActiveMsg", {
        get: function () {
            return "Quý Khách sẽ rời phòng sau khi ván chơi kết thúc.";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "leaveBookingDeactiveMsg", {
        get: function () {
            return "Quý Khách sẽ tiếp tục chơi.";
        },
        enumerable: false,
        configurable: true
    });
    SicboText = __decorate([
        ccclass
    ], SicboText);
    return SicboText;
}());
exports.default = SicboText;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1NldHRpbmcvU2ljYm9UZXh0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7QUFFbEYscUVBQWdFO0FBRXhELElBQUEsSUFBSSxHQUFLLDRCQUFrQixDQUFDLFVBQVUsRUFBRSxLQUFwQyxDQUFxQztBQUUzQyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFBO0lBK0VBLENBQUM7SUE5RUMsc0JBQVcsaUNBQW9CO2FBQS9CO1lBQ0UsT0FBTyxxQ0FBcUMsQ0FBQztRQUMvQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFXLCtCQUFrQjthQUE3QjtZQUNFLE9BQU8sNEJBQTRCLENBQUM7UUFDdEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBVyx1QkFBVTthQUFyQjtZQUNFLE9BQU8saUJBQWlCLENBQUM7UUFDM0IsQ0FBQzs7O09BQUE7SUFFRCxzQkFBVywyQkFBYzthQUF6QjtZQUNFLE9BQU8sTUFBTSxDQUFDO1FBQ2hCLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsMEJBQWE7YUFBeEI7WUFDRSxPQUFPLE1BQU0sQ0FBQztRQUNoQixDQUFDOzs7T0FBQTtJQUVELHNCQUFXLGtDQUFxQjthQUFoQztZQUNFLE9BQU8sVUFBVSxDQUFDO1FBQ3BCLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsMkJBQWM7YUFBekI7WUFDRSxPQUFPLEdBQUcsQ0FBQztRQUNiLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsd0NBQTJCO2FBQXRDO1lBQ0UsT0FBTyxZQUFZLENBQUM7UUFDdEIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBVyw4QkFBaUI7YUFBNUI7WUFDRSxPQUFPLDZCQUE2QixDQUFDO1FBQ3ZDLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsNEJBQWU7YUFBMUI7WUFDRSxPQUFPLHdCQUF3QixDQUFDO1FBQ2xDLENBQUM7OztPQUFBO0lBRUQsc0JBQVcsOENBQWlDO2FBQTVDO1lBQ0UsT0FBTywwRUFBMEUsQ0FBQTtRQUNuRixDQUFDOzs7T0FBQTtJQUVELHNCQUFXLDJDQUE4QjthQUF6QztZQUNFLE9BQU8sd0pBQXdKLENBQUE7UUFDakssQ0FBQzs7O09BQUE7SUFHYSx5QkFBZSxHQUE3QixVQUE4QixTQUFTO1FBQ3JDLFFBQVEsU0FBUyxFQUFFO1lBQ2pCLEtBQUssSUFBSSxDQUFDLHVCQUF1QjtnQkFDL0Isd0RBQXdEO2dCQUN4RCxPQUFPLHNEQUFzRCxDQUFDO1lBRWhFLEtBQUssSUFBSSxDQUFDLDBCQUEwQjtnQkFDbEMsT0FBTywwQ0FBMEMsQ0FBQztZQUVwRDtnQkFDRSxPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0gsQ0FBQztJQUVELHNCQUFrQixvQ0FBdUI7YUFBekM7WUFDRSxPQUFPLDBCQUEwQixDQUFDO1FBQ3BDLENBQUM7OztPQUFBO0lBRUQsc0JBQWtCLG9CQUFPO2FBQXpCO1lBQ0UsT0FBTyw0REFBNEQsQ0FBQztRQUN0RSxDQUFDOzs7T0FBQTtJQUVELHNCQUFrQixrQ0FBcUI7YUFBdkM7WUFDRSxPQUFPLG1EQUFtRCxDQUFDO1FBQzdELENBQUM7OztPQUFBO0lBRUQsc0JBQWtCLG9DQUF1QjthQUF6QztZQUNFLE9BQU8sNkJBQTZCLENBQUM7UUFDdkMsQ0FBQzs7O09BQUE7SUE5RWtCLFNBQVM7UUFEN0IsT0FBTztPQUNhLFNBQVMsQ0ErRTdCO0lBQUQsZ0JBQUM7Q0EvRUQsQUErRUMsSUFBQTtrQkEvRW9CLFNBQVMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFNpY2JvTW9kdWxlQWRhcHRlciBmcm9tIFwiLi4vLi4vLi4vLi4vU2ljYm9Nb2R1bGVBZGFwdGVyXCI7XG5cbmNvbnN0IHsgQ29kZSB9ID0gU2ljYm9Nb2R1bGVBZGFwdGVyLmdldEFsbFJlZnMoKTtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvVGV4dCB7XG4gIHN0YXRpYyBnZXQgaGFuZGxlckRlZmF1bHRFcnJNc2coKSB7XG4gICAgcmV0dXJuIFwiQ8OzIGzhu5dpIHjhuqN5IHJhIHhpbiB2dWkgbMOybmcgdGjhu60gbOG6oWkhXCI7XG4gIH1cblxuICBzdGF0aWMgZ2V0IHNlc3Npb25Db3B5TWVzc2FnZSgpOiBzdHJpbmcge1xuICAgIHJldHVybiBcIlNhbyBDaMOpcCBUaMOgbmggQ8O0bmcgTcOjIE1ENVwiO1xuICB9XG5cbiAgc3RhdGljIGdldCBsb2FkaW5nTXNnKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIFwiTG9hZGluZy4uLiB7MH0lXCI7XG4gIH1cblxuICBzdGF0aWMgZ2V0IGxvdHNPZlVzZXJUZXh0KCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIFwiOTk5K1wiO1xuICB9XG5cbiAgc3RhdGljIGdldCBzZXNzaW9uSWRUZXh0KCkge1xuICAgIHJldHVybiBcIiN7MH1cIjtcbiAgfVxuXG4gIHN0YXRpYyBnZXQgbGVhZGVyYm9hcmRVcGRhdGVUaW1lKCkge1xuICAgIHJldHVybiBcInswfSBwaMO6dFwiO1xuICB9XG5cbiAgc3RhdGljIGdldCBub0xvdHRlcnlWYWx1ZSgpIHtcbiAgICByZXR1cm4gXCItXCI7XG4gIH1cblxuICBzdGF0aWMgZ2V0IHNlc3Npb25EYXRhQmV0SGlzdG9yeURldGFpbCgpIHtcbiAgICByZXR1cm4gXCIjezB9ICh7MX0pXCI7XG4gIH1cblxuICBzdGF0aWMgZ2V0IHNlc3Npb25MYXN0Rm9ybWF0KCkge1xuICAgIHJldHVybiBcIlBoacOqbiBn4bqnbiDEkcOieSBuaOG6pXQgKCN7MH0pIC1cIjtcbiAgfVxuXG4gIHN0YXRpYyBnZXQgdmFsdWVMYXN0Rm9ybWF0KCkge1xuICAgIHJldHVybiBcInswfSB7MX06ICh7Mn0tezN9LXs0fSlcIjtcbiAgfVxuXG4gIHN0YXRpYyBnZXQgZ2V0Q2Fubm90Sm9pbk1lc3NhZ2VXaGVuTm90SW5nYW1lKCl7XG4gICAgcmV0dXJuIFwiR2FtZSDEkGFuZyBUaeG6v24gSMOgbmggTsOibmcgQ+G6pXAgSOG7hyBUaOG7kW5nLlxcblF1w70ga2jDoWNoIHZ1aSBsw7JuZyBxdWF5IGzhuqFpIHNhdSFcIlxuICB9XG5cbiAgc3RhdGljIGdldCBnZXRDYW5ub3RKb2luTWVzc2FnZVdoZW5JbmdhbWUoKXtcbiAgICByZXR1cm4gXCJYaW4gbOG7l2kgcXXDvSBraMOhY2gsIGhp4buHbiB04bqhaSBo4buHIHRo4buRbmcgxJFhbmcgZ+G6t3Agc+G7sSBj4buRLlxcbkNow7puZyB0w7RpIHPhur0gY+G7kSBn4bqvbmcga2jhuq9jIHBo4bulYyB0cm9uZyB0aOG7nWkgZ2lhbiBz4bubbSBuaOG6pXQuXFxuQ2jDom4gdGjDoG5oIHhpbiBs4buXaSB2w6wgc+G7sSBi4bqldCB0aeG7h24gbsOgeS5cIlxuICB9XG5cblxuICBwdWJsaWMgc3RhdGljIGdldEVycm9yTWVzc2FnZShlcnJvckNvZGUpIHtcbiAgICBzd2l0Y2ggKGVycm9yQ29kZSkge1xuICAgICAgY2FzZSBDb2RlLlNJQ0JPX0JFVF9MSU1JVF9SRUFDSEVEOlxuICAgICAgICAvLyByZXR1cm4gXCJRdcO9IEtow6FjaCDEkMOjIMSQ4bq3dCBN4bupYyBDxrDhu6NjIFThu5FpIMSQYSBDaG8gVOG7pSBOw6B5XCI7XG4gICAgICAgIHJldHVybiBcIlPhu5EgVGnhu4FuIMSQ4bq3dCBDxrDhu6NjIEtow7RuZyBUaOG7gyBWxrDhu6N0IE3hu6ljIFF1eSDEkOG7i25oIEPhu6dhIFThu6UhXCI7XG5cbiAgICAgIGNhc2UgQ29kZS5TSUNCT19CRVRfTk9UX0VOT1VHSF9NT05FWTpcbiAgICAgICAgcmV0dXJuIFwiU+G7kSBExrAgQ+G7p2EgUXXDvSBLaMOhY2ggS2jDtG5nIMSQ4bunIMSQ4buDIMSQ4bq3dCBDxrDhu6NjXCI7XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZ2V0IHdhaXRpbmdSZW1haW5pbmdUaW1lTXNnKCkge1xuICAgIHJldHVybiBcIlbDoW4gbeG7m2kgYuG6r3QgxJHhuqd1IHNhdSB7MH1zXCI7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGdldCBraWNrTXNnKCkge1xuICAgIHJldHVybiBcIlhpbiBN4budaSBRdcO9IEtow6FjaCDEkOG6t3QgQ8aw4bujY1xcbsSQ4buDIFRp4bq/cCBU4bulYyBUaGFtIEdpYSBCw6BuIENoxqFpLlwiO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBnZXQgbGVhdmVCb29raW5nQWN0aXZlTXNnKCkge1xuICAgIHJldHVybiBcIlF1w70gS2jDoWNoIHPhur0gcuG7nWkgcGjDsm5nIHNhdSBraGkgdsOhbiBjaMahaSBr4bq/dCB0aMO6Yy5cIjtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZ2V0IGxlYXZlQm9va2luZ0RlYWN0aXZlTXNnKCkge1xuICAgIHJldHVybiBcIlF1w70gS2jDoWNoIHPhur0gdGnhur9wIHThu6VjIGNoxqFpLlwiO1xuICB9XG59XG4iXX0=