
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Helpers/SicboConsoleHelper.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '7c1b9ftcGVFqpCx2TVNhNKl', 'SicboConsoleHelper');
// Game/Sicbo_New/Scripts/Helpers/SicboConsoleHelper.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboController_1 = require("../Controllers/SicboController");
var SicboGameUtils_1 = require("../RNGCommons/Utils/SicboGameUtils");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboConsoleHelper = /** @class */ (function (_super) {
    __extends(SicboConsoleHelper, _super);
    function SicboConsoleHelper() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SicboConsoleHelper.prototype.start = function () {
        if (window) {
            window.packageJson = this.packageJson;
            window.show = SicboController_1.default.Instance.onShowEvent;
            window.hide = SicboController_1.default.Instance.onHideEvent;
        }
    };
    SicboConsoleHelper.prototype.packageJson = function () {
        var data = "";
        if (CC_BUILD) {
            if (SicboGameUtils_1.default.isBrowser()) {
                var text = window.localStorage.getItem("package");
                if (text) {
                    data = JSON.parse(text);
                    //console.log(data);
                }
            }
            else {
                // if (cc.sys.os == cc.sys.OS_ANDROID) {
                //   ResourceManager.loadAsset("android_app_info", cc.JsonAsset, (jsonAsset: cc.JsonAsset) => {
                //     //console.log(jsonAsset.json);
                //   });
                // } else if (cc.sys.os == cc.sys.OS_IOS) {
                //   ResourceManager.loadAsset("ios_app_info", cc.JsonAsset, (jsonAsset: cc.JsonAsset) => {
                //     //console.log(jsonAsset.json);
                //   });
                // }
            }
        }
    };
    SicboConsoleHelper = __decorate([
        ccclass
    ], SicboConsoleHelper);
    return SicboConsoleHelper;
}(cc.Component));
exports.default = SicboConsoleHelper;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0hlbHBlcnMvU2ljYm9Db25zb2xlSGVscGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWxGLGtFQUE2RDtBQUM3RCxxRUFBZ0U7QUFHMUQsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBZ0Qsc0NBQVk7SUFBNUQ7O0lBK0JBLENBQUM7SUE5Qkcsa0NBQUssR0FBTDtRQUNJLElBQUcsTUFBTSxFQUFDO1lBQ04sTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ3RDLE1BQU0sQ0FBQyxJQUFJLEdBQUcseUJBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1lBQ25ELE1BQU0sQ0FBQyxJQUFJLEdBQUcseUJBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1NBQ3REO0lBQ0wsQ0FBQztJQUVELHdDQUFXLEdBQVg7UUFDSSxJQUFJLElBQUksR0FBRyxFQUFFLENBQUM7UUFDZCxJQUFJLFFBQVEsRUFBRTtZQUNaLElBQUksd0JBQWMsQ0FBQyxTQUFTLEVBQUUsRUFBRTtnQkFDOUIsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ2xELElBQUksSUFBSSxFQUFFO29CQUNSLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN4QixvQkFBb0I7aUJBQ3JCO2FBQ0Y7aUJBQU07Z0JBQ0wsd0NBQXdDO2dCQUN4QywrRkFBK0Y7Z0JBQy9GLHFDQUFxQztnQkFDckMsUUFBUTtnQkFDUiwyQ0FBMkM7Z0JBQzNDLDJGQUEyRjtnQkFDM0YscUNBQXFDO2dCQUNyQyxRQUFRO2dCQUNSLElBQUk7YUFDTDtTQUNGO0lBQ0wsQ0FBQztJQTlCZ0Isa0JBQWtCO1FBRHRDLE9BQU87T0FDYSxrQkFBa0IsQ0ErQnRDO0lBQUQseUJBQUM7Q0EvQkQsQUErQkMsQ0EvQitDLEVBQUUsQ0FBQyxTQUFTLEdBK0IzRDtrQkEvQm9CLGtCQUFrQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgU2ljYm9Db250cm9sbGVyIGZyb20gXCIuLi9Db250cm9sbGVycy9TaWNib0NvbnRyb2xsZXJcIjtcbmltcG9ydCBTaWNib0dhbWVVdGlscyBmcm9tIFwiLi4vUk5HQ29tbW9ucy9VdGlscy9TaWNib0dhbWVVdGlsc1wiO1xuXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2ljYm9Db25zb2xlSGVscGVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBzdGFydCAoKSB7XG4gICAgICAgIGlmKHdpbmRvdyl7XG4gICAgICAgICAgICB3aW5kb3cucGFja2FnZUpzb24gPSB0aGlzLnBhY2thZ2VKc29uO1xuICAgICAgICAgICAgd2luZG93LnNob3cgPSBTaWNib0NvbnRyb2xsZXIuSW5zdGFuY2Uub25TaG93RXZlbnQ7XG4gICAgICAgICAgICB3aW5kb3cuaGlkZSA9IFNpY2JvQ29udHJvbGxlci5JbnN0YW5jZS5vbkhpZGVFdmVudDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHBhY2thZ2VKc29uKCl7ICAgICAgICBcbiAgICAgICAgbGV0IGRhdGEgPSBcIlwiO1xuICAgICAgICBpZiAoQ0NfQlVJTEQpIHtcbiAgICAgICAgICBpZiAoU2ljYm9HYW1lVXRpbHMuaXNCcm93c2VyKCkpIHtcbiAgICAgICAgICAgIHZhciB0ZXh0ID0gd2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicGFja2FnZVwiKTtcbiAgICAgICAgICAgIGlmICh0ZXh0KSB7XG4gICAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRleHQpO1xuICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKGRhdGEpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAvLyBpZiAoY2Muc3lzLm9zID09IGNjLnN5cy5PU19BTkRST0lEKSB7XG4gICAgICAgICAgICAvLyAgIFJlc291cmNlTWFuYWdlci5sb2FkQXNzZXQoXCJhbmRyb2lkX2FwcF9pbmZvXCIsIGNjLkpzb25Bc3NldCwgKGpzb25Bc3NldDogY2MuSnNvbkFzc2V0KSA9PiB7XG4gICAgICAgICAgICAvLyAgICAgLy9jb25zb2xlLmxvZyhqc29uQXNzZXQuanNvbik7XG4gICAgICAgICAgICAvLyAgIH0pO1xuICAgICAgICAgICAgLy8gfSBlbHNlIGlmIChjYy5zeXMub3MgPT0gY2Muc3lzLk9TX0lPUykge1xuICAgICAgICAgICAgLy8gICBSZXNvdXJjZU1hbmFnZXIubG9hZEFzc2V0KFwiaW9zX2FwcF9pbmZvXCIsIGNjLkpzb25Bc3NldCwgKGpzb25Bc3NldDogY2MuSnNvbkFzc2V0KSA9PiB7XG4gICAgICAgICAgICAvLyAgICAgLy9jb25zb2xlLmxvZyhqc29uQXNzZXQuanNvbik7XG4gICAgICAgICAgICAvLyAgIH0pO1xuICAgICAgICAgICAgLy8gfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==