
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/Helpers/SicboHelper.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0487eM8/p9Ch4itwVWXz9Cs', 'SicboHelper');
// Game/Sicbo_New/Scripts/Helpers/SicboHelper.ts

"use strict";
/* eslint-disable */
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Result = _a.Result, Item = _a.Item;
var SicboHelper = /** @class */ (function () {
    function SicboHelper() {
    }
    //SECTION Old
    SicboHelper.getResultTypeName = function (sicboResultType, upperAll) {
        if (upperAll === void 0) { upperAll = true; }
        switch (sicboResultType) {
            case SicboResultType_1.SicboResultType.TAI:
                return upperAll ? "TÀI" : "Tài";
            case SicboResultType_1.SicboResultType.XIU:
                return upperAll ? "XỈU" : "Xỉu";
            case SicboResultType_1.SicboResultType.BAO:
                return upperAll ? "BÃO" : "Bão";
        }
    };
    SicboHelper.getResultType = function (items) {
        var resultType = SicboResultType_1.SicboResultType.TAI;
        if (items[0] == items[1] && items[1] == items[2]) {
            resultType = SicboResultType_1.SicboResultType.BAO;
        }
        else {
            var totalValue = this.getResultTotalValue(items);
            if (totalValue > 3 && totalValue <= 10) {
                resultType = SicboResultType_1.SicboResultType.XIU;
            }
        }
        return resultType;
    };
    SicboHelper.getResultBrightType = function (items) {
        var resultType = SicboResultType_1.SicboResultType.TAI;
        var totalValue = this.getResultTotalValue(items);
        if (totalValue >= 3 && totalValue <= 10) {
            resultType = SicboResultType_1.SicboResultType.XIU;
        }
        return resultType;
    };
    SicboHelper.getResultTotalValue = function (items) {
        var totalValue = 0;
        items.forEach(function (item) {
            totalValue += item;
        });
        return totalValue;
    };
    // static loadSicboDice(item: DICE, onComplete: (spriteFrame) => void) {
    //   if (item == DICE.DICE_UNSPECIFIED) item = DICE.ONE;
    //   let path = GameUtils.FormatString("Images/Game/Sicbo/Items/XucXac_{0}", item.toString());
    //   ResourceManager.loadAsset(path, cc.SpriteFrame, (spriteFrame: cc.SpriteFrame) => {
    //     if (onComplete) onComplete(spriteFrame);
    //   });
    // }
    //!SECTION
    SicboHelper.convertCoinTypeToValue = function (coinType) {
        switch (coinType) {
            case SicboCoinType_1.SicboCoinType.Coin100:
                return 100;
            case SicboCoinType_1.SicboCoinType.Coin200:
                return 200;
            case SicboCoinType_1.SicboCoinType.Coin500:
                return 500;
            case SicboCoinType_1.SicboCoinType.Coin1k:
                return 1000;
            case SicboCoinType_1.SicboCoinType.Coin2k:
                return 2000;
            case SicboCoinType_1.SicboCoinType.Coin5k:
                return 5000;
            case SicboCoinType_1.SicboCoinType.Coin10k:
                return 10000;
            case SicboCoinType_1.SicboCoinType.Coin20k:
                return 20000;
            case SicboCoinType_1.SicboCoinType.Coin50k:
                return 50000;
            case SicboCoinType_1.SicboCoinType.Coin100k:
                return 100000;
            case SicboCoinType_1.SicboCoinType.Coin200k:
                return 200000;
            case SicboCoinType_1.SicboCoinType.Coin500k:
                return 500000;
            case SicboCoinType_1.SicboCoinType.Coin1M:
                return 1000000;
            case SicboCoinType_1.SicboCoinType.Coin2M:
                return 2000000;
            case SicboCoinType_1.SicboCoinType.Coin5M:
                return 5000000;
            case SicboCoinType_1.SicboCoinType.Coin10M:
                return 10000000;
            case SicboCoinType_1.SicboCoinType.Coin20M:
                return 20000000;
            case SicboCoinType_1.SicboCoinType.Coin50M:
                return 50000000;
        }
        return 100;
    };
    SicboHelper.convertValueToCoinType = function (value) {
        switch (value) {
            case 100:
                return SicboCoinType_1.SicboCoinType.Coin100;
            case 200:
                return SicboCoinType_1.SicboCoinType.Coin200;
            case 500:
                return SicboCoinType_1.SicboCoinType.Coin500;
            case 1000:
                return SicboCoinType_1.SicboCoinType.Coin1k;
            case 2000:
                return SicboCoinType_1.SicboCoinType.Coin2k;
            case 5000:
                return SicboCoinType_1.SicboCoinType.Coin5k;
            case 10000:
                return SicboCoinType_1.SicboCoinType.Coin10k;
            case 20000:
                return SicboCoinType_1.SicboCoinType.Coin20k;
            case 50000:
                return SicboCoinType_1.SicboCoinType.Coin50k;
            case 100000:
                return SicboCoinType_1.SicboCoinType.Coin100k;
            case 200000:
                return SicboCoinType_1.SicboCoinType.Coin200k;
            case 500000:
                return SicboCoinType_1.SicboCoinType.Coin500k;
            case 1000000:
                return SicboCoinType_1.SicboCoinType.Coin1M;
            case 2000000:
                return SicboCoinType_1.SicboCoinType.Coin2M;
            case 5000000:
                return SicboCoinType_1.SicboCoinType.Coin5M;
            case 10000000:
                return SicboCoinType_1.SicboCoinType.Coin10M;
            case 20000000:
                return SicboCoinType_1.SicboCoinType.Coin20M;
            case 50000000:
                return SicboCoinType_1.SicboCoinType.Coin50M;
        }
    };
    SicboHelper.convertValueToChips = function (value, coinType) {
        if (value <= 0)
            return coinType;
        var chipMax = this.convertValueToChipMax(value);
        coinType[coinType.length] = chipMax;
        var valueTemp = value - this.convertCoinTypeToValue(chipMax);
        return this.convertValueToChips(valueTemp, coinType);
    };
    SicboHelper.convertValueToChipMax = function (value) {
        if (value >= 50000000)
            return SicboCoinType_1.SicboCoinType.Coin50M;
        if (value >= 20000000)
            return SicboCoinType_1.SicboCoinType.Coin20M;
        if (value >= 10000000)
            return SicboCoinType_1.SicboCoinType.Coin10M;
        if (value >= 5000000)
            return SicboCoinType_1.SicboCoinType.Coin5M;
        if (value >= 2000000)
            return SicboCoinType_1.SicboCoinType.Coin2M;
        if (value >= 1000000)
            return SicboCoinType_1.SicboCoinType.Coin1M;
        if (value >= 500000)
            return SicboCoinType_1.SicboCoinType.Coin500k;
        if (value >= 200000)
            return SicboCoinType_1.SicboCoinType.Coin200k;
        if (value >= 100000)
            return SicboCoinType_1.SicboCoinType.Coin100k;
        if (value >= 50000)
            return SicboCoinType_1.SicboCoinType.Coin50k;
        if (value >= 20000)
            return SicboCoinType_1.SicboCoinType.Coin20k;
        if (value >= 10000)
            return SicboCoinType_1.SicboCoinType.Coin10k;
        if (value >= 5000)
            return SicboCoinType_1.SicboCoinType.Coin5k;
        if (value >= 2000)
            return SicboCoinType_1.SicboCoinType.Coin2k;
        if (value >= 1000)
            return SicboCoinType_1.SicboCoinType.Coin1k;
        if (value >= 500)
            return SicboCoinType_1.SicboCoinType.Coin500;
        if (value >= 200)
            return SicboCoinType_1.SicboCoinType.Coin200;
        return SicboCoinType_1.SicboCoinType.Coin100;
    };
    SicboHelper.convertLuckyNumToString = function (value) {
        if (value < 10)
            return "00000" + value;
        if (value < 100)
            return "0000" + value;
        if (value < 1000)
            return "000" + value;
        if (value < 10000)
            return "00" + value;
        if (value < 100000)
            return "0" + value;
        return value.toString();
    };
    // static getAllBetTypeWithMinBet(
    //   value: number,
    //   channelType: CHANNEL_TYPE
    // ): number[] {
    //   let maxValue = this.getMaxBetWithMinBet(value, channelType);
    //   let values = this.getAllBuyIn().filter((x) => x >= value && x <= maxValue);
    //   return values;
    // }
    // static getAllBetTypeWithMinBetSicboMini(
    //   value: number,
    //   channelType: CHANNEL_TYPE
    // ): CoinType[] {
    //   let maxValue = this.getMaxBetWithMinBet(value, channelType);
    //   let values = this.getAllBuyInSicboMini().filter(
    //     (x) => x >= value && x <= maxValue
    //   );
    //   let ItemTypes: CoinType[] = [];
    //   values.forEach((value, index) => {
    //     let coinType = this.convertValueToItemType(value);
    //     ItemTypes[index] = coinType;
    //   });
    //   return ItemTypes;
    // }
    SicboHelper.convertSicboItemToString = function (item) {
        var ItemName = "";
        switch (item) {
            case Item.ONE:
                ItemName = "Một";
                break;
            case Item.TWO:
                ItemName = "Hai";
                break;
            case Item.THREE:
                ItemName = "Ba";
                break;
            case Item.FOUR:
                ItemName = "Bốn";
                break;
            case Item.FIVE:
                ItemName = "Năm";
                break;
            case Item.SIX:
                ItemName = "Sáu";
                break;
        }
        return ItemName;
    };
    SicboHelper.StatusToString = function (status) {
        var StatusString = [
            "STATUS_UNSPECIFIED",
            "WAITING",
            "BETTING",
            "WAITING_RESULT",
            "RESULTING",
            "SHOWING_JACKPOT",
            "SHOWING_RESULT",
            "PAYING",
            "FINISHING",
        ];
        return StatusString[status];
    };
    SicboHelper.getResultMessage = function () {
        var msg = "";
        // if (result) {
        //   let ItemWins = result.getItemsList();
        //   switch (result.getType()) {
        //     case Result.TYPE.JACKPOT:
        //     case Result.TYPE.PUT2POT:
        //     case Result.TYPE.NORMAL:
        //       ItemWins.forEach((Item, index) => {
        //         if (index == ItemWins.length - 1) {
        //           msg = msg + this.convertSicboItemToString(Item);
        //         } else {
        //           msg = msg + this.convertSicboItemToString(Item) + " ";
        //         }
        //       });
        //       break;
        //   }
        // }
        return msg;
    };
    SicboHelper.getResultJackpotMessage = function (result) {
        var msg = "";
        if (result) {
            // switch (result.getType()) {
            //   case Result.TYPE.JACKPOT:
            //     msg = "Bão " + this.convertSicboItemToString(ItemWins[0]);
            //     break;
            // }
        }
        return msg;
    };
    SicboHelper.convertItemKeyToItems = function (key) {
        return key.split(",").map(function (tmp) { return parseInt(tmp); });
    };
    SicboHelper.getMyJackPot = function () {
        var money = 0;
        // if (result && result.getWinnersList()) {
        //   result.getWinnersList().forEach((record) => {
        //     if (record != null && record.hasBet()) {
        //       let userId = record.getUserId();
        //       if (userId == UserManager.getInstance().userData.userId) {
        //         money = money + record.getAmountFromPot();
        //       }
        //     }
        //   });
        // }
        return money;
    };
    SicboHelper.convertItemsToItemKey = function (Items) {
        var key = "";
        Items.forEach(function (Item, index) {
            if (index == Items.length - 1) {
                key += Item;
            }
            else {
                key += Item + ",";
            }
        });
        return key;
    };
    //EX: let rs = calculateNumOfChipToMakeValue(15156000, chip_level);
    // rs.forEach((num: number, val: number) => {
    //   //console.log("val: " + val + " -> " + num);
    // });
    //
    //Result:   
    // [LOG]: "val: 5000000 -> 3" 
    // [LOG]: "val: 100000 -> 1" 
    // [LOG]: "val: 50000 -> 1" 
    // [LOG]: "val: 5000 -> 1" 
    // [LOG]: "val: 1000 -> 1" 
    SicboHelper.calculateNumOfChipToMakeValue = function (input, config) {
        var resultMap = new Map();
        var index = config.length - 1;
        while (index >= 0) {
            var val = config[index];
            var num = Math.floor(input / val);
            if (num > 0) {
                resultMap.set(val, num);
                input -= val * num;
            }
            index--;
        }
        return resultMap;
    };
    SicboHelper.convertItemToDiceSpriteName = function (item) {
        var name = "SB_maintable_" + item;
        return name;
    };
    SicboHelper.convertCoinTypeToSpriteName = function (coinType, isDisable, isMini) {
        if (isDisable === void 0) { isDisable = false; }
        if (isMini === void 0) { isMini = false; }
        var spriteName = "SB_Main_Chip_";
        var disableSuffix = "_Disable";
        var miniSuffix = "_Mini";
        switch (coinType) {
            case SicboCoinType_1.SicboCoinType.Coin1k:
                spriteName += "1K";
                break;
            case SicboCoinType_1.SicboCoinType.Coin5k:
                spriteName += "5K";
                break;
            case SicboCoinType_1.SicboCoinType.Coin10k:
                spriteName += "10K";
                break;
            case SicboCoinType_1.SicboCoinType.Coin50k:
                spriteName += "50K";
                break;
            case SicboCoinType_1.SicboCoinType.Coin100k:
                spriteName += "100K";
                break;
            case SicboCoinType_1.SicboCoinType.Coin500k:
                spriteName += "500K";
                break;
            case SicboCoinType_1.SicboCoinType.Coin1M:
                spriteName += "1M";
                break;
            case SicboCoinType_1.SicboCoinType.Coin5M:
                spriteName += "5M";
                break;
        }
        if (isDisable)
            return spriteName + disableSuffix;
        if (isMini)
            return spriteName + miniSuffix;
        return spriteName;
    };
    SicboHelper.approximatelyEqual = function (comparer, compareTo, delta) {
        if (compareTo === void 0) { compareTo = 0; }
        if (delta === void 0) { delta = .1; }
        return comparer >= compareTo && (comparer - compareTo) <= delta;
    };
    SicboHelper.doorToItems = function (door) {
        switch (door) {
            // case Door.BAU_ONE: return [Item.BAU]; 
            // case Door.CUA_ONE: return [Item.CUA];
            // case Door.TOM_ONE: return [Item.TOM];
            // case Door.CA_ONE: return [Item.CA];
            // case Door.NAI_ONE: return [Item.NAI];
            // case Door.GA_ONE: return [Item.GA];
            // case Door.BAU_TOM: return [Item.BAU, Item.TOM];
            // case Door.BAU_CA: return [Item.BAU, Item.CA];
            // case Door.BAU_GA: return [Item.BAU, Item.GA];
            // case Door.BAU_CUA: return [Item.BAU, Item.CUA];
            // case Door.BAU_NAI: return [Item.BAU, Item.NAI];
            // case Door.TOM_CA: return [Item.TOM, Item.CA];
            // case Door.TOM_GA: return [Item.TOM, Item.GA];
            // case Door.TOM_CUA: return [Item.TOM, Item.CUA];
            // case Door.TOM_NAI: return [Item.TOM, Item.NAI];
            // case Door.CA_GA: return [Item.CA, Item.GA];
            // case Door.CA_CUA: return [Item.CA, Item.CUA];
            // case Door.CA_NAI: return [Item.CA, Item.NAI];
            // case Door.GA_CUA: return [Item.GA, Item.CUA];
            // case Door.GA_NAI: return [Item.GA, Item.NAI];
            // case Door.CUA_NAI: return [Item.CUA, Item.NAI];
        }
        return [];
    };
    SicboHelper.changeParent = function (child, newParent) {
        if (child.parent == newParent)
            return;
        var p = child.getPosition();
        var pos = SicboGameUtils_1.default.convertToOtherNode(child.parent, newParent, new cc.Vec3(p.x, p.y, 0));
        child.parent = newParent;
        child.setPosition(pos);
    };
    SicboHelper.fixString = function (input, maxLength, checkChar) {
        if (maxLength === void 0) { maxLength = 15; }
        if (checkChar === void 0) { checkChar = 7; }
        if (input.length <= maxLength)
            return input;
        var substr = input.substr(0, maxLength);
        var rs = "";
        // if(input[maxLength + 1] == ' '||input[maxLength + 1] == '\n'){
        //   rs = substr;
        //   input = input.substr(maxLength);
        // }else
        {
            for (var i = substr.length - 1; i >= 0; i--) {
                if (substr[i] == ' ') {
                    rs = substr.substr(0, i);
                    input = input.substr(i + 1);
                    break;
                }
            }
            if (rs == "") {
                rs = substr;
                input = input.substr(maxLength);
            }
        }
        return rs + "\n" + SicboHelper.fixString(input);
    };
    return SicboHelper;
}());
exports.default = SicboHelper;
var SicboResultType_1 = require("../Enums/SicboResultType");
var SicboCoinType_1 = require("../RNGCommons/SicboCoinType");
var SicboGameUtils_1 = require("../RNGCommons/Utils/SicboGameUtils");

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL0hlbHBlcnMvU2ljYm9IZWxwZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjs7QUFFcEIscUVBQWdFO0FBRTFELElBQUEsS0FFSSw0QkFBa0IsQ0FBQyxVQUFVLEVBQUUsRUFEdkMsTUFBTSxZQUFBLEVBQ04sSUFBSSxVQUFtQyxDQUFDO0FBRTFDO0lBQUE7SUF3YkEsQ0FBQztJQXZiQyxhQUFhO0lBQ04sNkJBQWlCLEdBQXhCLFVBQXlCLGVBQWdDLEVBQUUsUUFBd0I7UUFBeEIseUJBQUEsRUFBQSxlQUF3QjtRQUNqRixRQUFRLGVBQWUsRUFBRTtZQUN2QixLQUFLLGlDQUFlLENBQUMsR0FBRztnQkFDdEIsT0FBTyxRQUFRLENBQUEsQ0FBQyxDQUFDLEtBQUssQ0FBQSxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ2hDLEtBQUssaUNBQWUsQ0FBQyxHQUFHO2dCQUN0QixPQUFPLFFBQVEsQ0FBQSxDQUFDLENBQUMsS0FBSyxDQUFBLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDaEMsS0FBSyxpQ0FBZSxDQUFDLEdBQUc7Z0JBQ3RCLE9BQU8sUUFBUSxDQUFBLENBQUMsQ0FBQyxLQUFLLENBQUEsQ0FBQyxDQUFDLEtBQUssQ0FBQztTQUNqQztJQUNILENBQUM7SUFFTSx5QkFBYSxHQUFwQixVQUFxQixLQUFZO1FBQy9CLElBQUksVUFBVSxHQUFHLGlDQUFlLENBQUMsR0FBRyxDQUFDO1FBQ3JDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ2hELFVBQVUsR0FBRyxpQ0FBZSxDQUFDLEdBQUcsQ0FBQztTQUNsQzthQUFNO1lBQ0wsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pELElBQUksVUFBVSxHQUFHLENBQUMsSUFBSSxVQUFVLElBQUksRUFBRSxFQUFFO2dCQUN0QyxVQUFVLEdBQUcsaUNBQWUsQ0FBQyxHQUFHLENBQUM7YUFDbEM7U0FDRjtRQUNELE9BQU8sVUFBVSxDQUFDO0lBQ3BCLENBQUM7SUFFTSwrQkFBbUIsR0FBMUIsVUFBMkIsS0FBWTtRQUNyQyxJQUFJLFVBQVUsR0FBRyxpQ0FBZSxDQUFDLEdBQUcsQ0FBQztRQUNyQyxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDakQsSUFBSSxVQUFVLElBQUksQ0FBQyxJQUFJLFVBQVUsSUFBSSxFQUFFLEVBQUU7WUFDdkMsVUFBVSxHQUFHLGlDQUFlLENBQUMsR0FBRyxDQUFDO1NBQ2xDO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQztJQUVNLCtCQUFtQixHQUExQixVQUEyQixLQUFZO1FBQ3JDLElBQUksVUFBVSxHQUFHLENBQUMsQ0FBQztRQUNuQixLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtZQUNqQixVQUFVLElBQUksSUFBYyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQztJQUVELHdFQUF3RTtJQUN4RSx3REFBd0Q7SUFDeEQsOEZBQThGO0lBQzlGLHVGQUF1RjtJQUN2RiwrQ0FBK0M7SUFDL0MsUUFBUTtJQUNSLElBQUk7SUFDSixVQUFVO0lBRUgsa0NBQXNCLEdBQTdCLFVBQThCLFFBQXVCO1FBQ25ELFFBQVEsUUFBUSxFQUFFO1lBQ2hCLEtBQUssNkJBQWEsQ0FBQyxPQUFPO2dCQUN4QixPQUFPLEdBQUcsQ0FBQztZQUNiLEtBQUssNkJBQWEsQ0FBQyxPQUFPO2dCQUN4QixPQUFPLEdBQUcsQ0FBQztZQUNiLEtBQUssNkJBQWEsQ0FBQyxPQUFPO2dCQUN4QixPQUFPLEdBQUcsQ0FBQztZQUNiLEtBQUssNkJBQWEsQ0FBQyxNQUFNO2dCQUN2QixPQUFPLElBQUksQ0FBQztZQUNkLEtBQUssNkJBQWEsQ0FBQyxNQUFNO2dCQUN2QixPQUFPLElBQUksQ0FBQztZQUNkLEtBQUssNkJBQWEsQ0FBQyxNQUFNO2dCQUN2QixPQUFPLElBQUksQ0FBQztZQUNkLEtBQUssNkJBQWEsQ0FBQyxPQUFPO2dCQUN4QixPQUFPLEtBQUssQ0FBQztZQUNmLEtBQUssNkJBQWEsQ0FBQyxPQUFPO2dCQUN4QixPQUFPLEtBQUssQ0FBQztZQUNmLEtBQUssNkJBQWEsQ0FBQyxPQUFPO2dCQUN4QixPQUFPLEtBQUssQ0FBQztZQUNmLEtBQUssNkJBQWEsQ0FBQyxRQUFRO2dCQUN6QixPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLDZCQUFhLENBQUMsUUFBUTtnQkFDekIsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyw2QkFBYSxDQUFDLFFBQVE7Z0JBQ3pCLE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssNkJBQWEsQ0FBQyxNQUFNO2dCQUN2QixPQUFPLE9BQU8sQ0FBQztZQUNqQixLQUFLLDZCQUFhLENBQUMsTUFBTTtnQkFDdkIsT0FBTyxPQUFPLENBQUM7WUFDakIsS0FBSyw2QkFBYSxDQUFDLE1BQU07Z0JBQ3ZCLE9BQU8sT0FBTyxDQUFDO1lBQ2pCLEtBQUssNkJBQWEsQ0FBQyxPQUFPO2dCQUN4QixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLDZCQUFhLENBQUMsT0FBTztnQkFDeEIsT0FBTyxRQUFRLENBQUM7WUFDbEIsS0FBSyw2QkFBYSxDQUFDLE9BQU87Z0JBQ3hCLE9BQU8sUUFBUSxDQUFDO1NBQ25CO1FBQ0QsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDO0lBRU0sa0NBQXNCLEdBQTdCLFVBQThCLEtBQWE7UUFDekMsUUFBUSxLQUFLLEVBQUU7WUFDYixLQUFLLEdBQUc7Z0JBQ04sT0FBTyw2QkFBYSxDQUFDLE9BQU8sQ0FBQztZQUMvQixLQUFLLEdBQUc7Z0JBQ04sT0FBTyw2QkFBYSxDQUFDLE9BQU8sQ0FBQztZQUMvQixLQUFLLEdBQUc7Z0JBQ04sT0FBTyw2QkFBYSxDQUFDLE9BQU8sQ0FBQztZQUMvQixLQUFLLElBQUk7Z0JBQ1AsT0FBTyw2QkFBYSxDQUFDLE1BQU0sQ0FBQztZQUM5QixLQUFLLElBQUk7Z0JBQ1AsT0FBTyw2QkFBYSxDQUFDLE1BQU0sQ0FBQztZQUM5QixLQUFLLElBQUk7Z0JBQ1AsT0FBTyw2QkFBYSxDQUFDLE1BQU0sQ0FBQztZQUM5QixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyw2QkFBYSxDQUFDLE9BQU8sQ0FBQztZQUMvQixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyw2QkFBYSxDQUFDLE9BQU8sQ0FBQztZQUMvQixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyw2QkFBYSxDQUFDLE9BQU8sQ0FBQztZQUMvQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyw2QkFBYSxDQUFDLFFBQVEsQ0FBQztZQUNoQyxLQUFLLE1BQU07Z0JBQ1QsT0FBTyw2QkFBYSxDQUFDLFFBQVEsQ0FBQztZQUNoQyxLQUFLLE1BQU07Z0JBQ1QsT0FBTyw2QkFBYSxDQUFDLFFBQVEsQ0FBQztZQUNoQyxLQUFLLE9BQU87Z0JBQ1YsT0FBTyw2QkFBYSxDQUFDLE1BQU0sQ0FBQztZQUM5QixLQUFLLE9BQU87Z0JBQ1YsT0FBTyw2QkFBYSxDQUFDLE1BQU0sQ0FBQztZQUM5QixLQUFLLE9BQU87Z0JBQ1YsT0FBTyw2QkFBYSxDQUFDLE1BQU0sQ0FBQztZQUM5QixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyw2QkFBYSxDQUFDLE9BQU8sQ0FBQztZQUMvQixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyw2QkFBYSxDQUFDLE9BQU8sQ0FBQztZQUMvQixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyw2QkFBYSxDQUFDLE9BQU8sQ0FBQztTQUNoQztJQUNILENBQUM7SUFHTSwrQkFBbUIsR0FBMUIsVUFBMkIsS0FBYSxFQUFFLFFBQXlCO1FBQ2pFLElBQUksS0FBSyxJQUFJLENBQUM7WUFBRSxPQUFPLFFBQVEsQ0FBQztRQUNoQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEQsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxPQUFPLENBQUM7UUFDcEMsSUFBSSxTQUFTLEdBQUcsS0FBSyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM3RCxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVjLGlDQUFxQixHQUFwQyxVQUFxQyxLQUFhO1FBQ2hELElBQUksS0FBSyxJQUFJLFFBQVE7WUFBRSxPQUFPLDZCQUFhLENBQUMsT0FBTyxDQUFDO1FBQ3BELElBQUksS0FBSyxJQUFJLFFBQVE7WUFBRSxPQUFPLDZCQUFhLENBQUMsT0FBTyxDQUFDO1FBQ3BELElBQUksS0FBSyxJQUFJLFFBQVE7WUFBRSxPQUFPLDZCQUFhLENBQUMsT0FBTyxDQUFDO1FBQ3BELElBQUksS0FBSyxJQUFJLE9BQU87WUFBRSxPQUFPLDZCQUFhLENBQUMsTUFBTSxDQUFDO1FBQ2xELElBQUksS0FBSyxJQUFJLE9BQU87WUFBRSxPQUFPLDZCQUFhLENBQUMsTUFBTSxDQUFDO1FBQ2xELElBQUksS0FBSyxJQUFJLE9BQU87WUFBRSxPQUFPLDZCQUFhLENBQUMsTUFBTSxDQUFDO1FBQ2xELElBQUksS0FBSyxJQUFJLE1BQU07WUFBRSxPQUFPLDZCQUFhLENBQUMsUUFBUSxDQUFDO1FBQ25ELElBQUksS0FBSyxJQUFJLE1BQU07WUFBRSxPQUFPLDZCQUFhLENBQUMsUUFBUSxDQUFDO1FBQ25ELElBQUksS0FBSyxJQUFJLE1BQU07WUFBRSxPQUFPLDZCQUFhLENBQUMsUUFBUSxDQUFDO1FBQ25ELElBQUksS0FBSyxJQUFJLEtBQUs7WUFBRSxPQUFPLDZCQUFhLENBQUMsT0FBTyxDQUFDO1FBQ2pELElBQUksS0FBSyxJQUFJLEtBQUs7WUFBRSxPQUFPLDZCQUFhLENBQUMsT0FBTyxDQUFDO1FBQ2pELElBQUksS0FBSyxJQUFJLEtBQUs7WUFBRSxPQUFPLDZCQUFhLENBQUMsT0FBTyxDQUFDO1FBQ2pELElBQUksS0FBSyxJQUFJLElBQUk7WUFBRSxPQUFPLDZCQUFhLENBQUMsTUFBTSxDQUFDO1FBQy9DLElBQUksS0FBSyxJQUFJLElBQUk7WUFBRSxPQUFPLDZCQUFhLENBQUMsTUFBTSxDQUFDO1FBQy9DLElBQUksS0FBSyxJQUFJLElBQUk7WUFBRSxPQUFPLDZCQUFhLENBQUMsTUFBTSxDQUFDO1FBQy9DLElBQUksS0FBSyxJQUFJLEdBQUc7WUFBRSxPQUFPLDZCQUFhLENBQUMsT0FBTyxDQUFDO1FBQy9DLElBQUksS0FBSyxJQUFJLEdBQUc7WUFBRSxPQUFPLDZCQUFhLENBQUMsT0FBTyxDQUFDO1FBRS9DLE9BQU8sNkJBQWEsQ0FBQyxPQUFPLENBQUM7SUFDL0IsQ0FBQztJQUNNLG1DQUF1QixHQUE5QixVQUErQixLQUFhO1FBQzFDLElBQUksS0FBSyxHQUFHLEVBQUU7WUFBRSxPQUFPLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDdkMsSUFBSSxLQUFLLEdBQUcsR0FBRztZQUFFLE9BQU8sTUFBTSxHQUFHLEtBQUssQ0FBQztRQUN2QyxJQUFJLEtBQUssR0FBRyxJQUFJO1lBQUUsT0FBTyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ3ZDLElBQUksS0FBSyxHQUFHLEtBQUs7WUFBRSxPQUFPLElBQUksR0FBRyxLQUFLLENBQUM7UUFDdkMsSUFBSSxLQUFLLEdBQUcsTUFBTTtZQUFFLE9BQU8sR0FBRyxHQUFHLEtBQUssQ0FBQztRQUN2QyxPQUFPLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBQ0Qsa0NBQWtDO0lBQ2xDLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsZ0JBQWdCO0lBQ2hCLGlFQUFpRTtJQUNqRSxnRkFBZ0Y7SUFDaEYsbUJBQW1CO0lBQ25CLElBQUk7SUFFSiwyQ0FBMkM7SUFDM0MsbUJBQW1CO0lBQ25CLDhCQUE4QjtJQUM5QixrQkFBa0I7SUFDbEIsaUVBQWlFO0lBQ2pFLHFEQUFxRDtJQUNyRCx5Q0FBeUM7SUFDekMsT0FBTztJQUVQLG9DQUFvQztJQUNwQyx1Q0FBdUM7SUFDdkMseURBQXlEO0lBRXpELG1DQUFtQztJQUNuQyxRQUFRO0lBQ1Isc0JBQXNCO0lBQ3RCLElBQUk7SUFHRyxvQ0FBd0IsR0FBL0IsVUFBZ0MsSUFBSTtRQUNsQyxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbEIsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLElBQUksQ0FBQyxHQUFHO2dCQUNYLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ2pCLE1BQU07WUFDUixLQUFLLElBQUksQ0FBQyxHQUFHO2dCQUNYLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ2pCLE1BQU07WUFDUixLQUFLLElBQUksQ0FBQyxLQUFLO2dCQUNiLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ2hCLE1BQU07WUFDUixLQUFLLElBQUksQ0FBQyxJQUFJO2dCQUNaLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ2pCLE1BQU07WUFDUixLQUFLLElBQUksQ0FBQyxJQUFJO2dCQUNaLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ2pCLE1BQU07WUFDUixLQUFLLElBQUksQ0FBQyxHQUFHO2dCQUNYLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ2pCLE1BQU07U0FDVDtRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ2xCLENBQUM7SUFFTSwwQkFBYyxHQUFyQixVQUFzQixNQUFNO1FBQzFCLElBQUksWUFBWSxHQUFHO1lBQ2pCLG9CQUFvQjtZQUNwQixTQUFTO1lBQ1QsU0FBUztZQUNULGdCQUFnQjtZQUNoQixXQUFXO1lBQ1gsaUJBQWlCO1lBQ2pCLGdCQUFnQjtZQUNoQixRQUFRO1lBQ1IsV0FBVztTQUNaLENBQUE7UUFDRCxPQUFPLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBR00sNEJBQWdCLEdBQXZCO1FBQ0UsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO1FBQ2IsZ0JBQWdCO1FBQ2hCLDBDQUEwQztRQUMxQyxnQ0FBZ0M7UUFDaEMsZ0NBQWdDO1FBQ2hDLGdDQUFnQztRQUNoQywrQkFBK0I7UUFDL0IsNENBQTRDO1FBQzVDLDhDQUE4QztRQUM5Qyw2REFBNkQ7UUFDN0QsbUJBQW1CO1FBQ25CLG1FQUFtRTtRQUNuRSxZQUFZO1FBQ1osWUFBWTtRQUNaLGVBQWU7UUFDZixNQUFNO1FBQ04sSUFBSTtRQUNKLE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQUVNLG1DQUF1QixHQUE5QixVQUErQixNQUFtQztRQUNoRSxJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7UUFDYixJQUFJLE1BQU0sRUFBRTtZQUNWLDhCQUE4QjtZQUM5Qiw4QkFBOEI7WUFDOUIsaUVBQWlFO1lBQ2pFLGFBQWE7WUFDYixJQUFJO1NBQ0w7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNiLENBQUM7SUFFTSxpQ0FBcUIsR0FBNUIsVUFBNkIsR0FBVztRQUN0QyxPQUFPLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFiLENBQWEsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFHTSx3QkFBWSxHQUFuQjtRQUNFLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLDJDQUEyQztRQUMzQyxrREFBa0Q7UUFDbEQsK0NBQStDO1FBQy9DLHlDQUF5QztRQUN6QyxtRUFBbUU7UUFDbkUscURBQXFEO1FBQ3JELFVBQVU7UUFDVixRQUFRO1FBQ1IsUUFBUTtRQUNSLElBQUk7UUFDSixPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFTSxpQ0FBcUIsR0FBNUIsVUFBNkIsS0FBWTtRQUN2QyxJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7UUFDYixLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSSxFQUFFLEtBQUs7WUFDeEIsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzdCLEdBQUcsSUFBSSxJQUFJLENBQUM7YUFDYjtpQkFBTTtnQkFDTCxHQUFHLElBQUksSUFBSSxHQUFHLEdBQUcsQ0FBQzthQUNuQjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDO0lBRUQsbUVBQW1FO0lBQ25FLDZDQUE2QztJQUM3QyxpREFBaUQ7SUFDakQsTUFBTTtJQUNOLEVBQUU7SUFDRixZQUFZO0lBQ1osOEJBQThCO0lBQzlCLDZCQUE2QjtJQUM3Qiw0QkFBNEI7SUFDNUIsMkJBQTJCO0lBQzNCLDJCQUEyQjtJQUNwQix5Q0FBNkIsR0FBcEMsVUFBcUMsS0FBYSxFQUFFLE1BQWdCO1FBQ2xFLElBQUksU0FBUyxHQUFHLElBQUksR0FBRyxFQUFrQixDQUFDO1FBQzFDLElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLE9BQU0sS0FBSyxJQUFFLENBQUMsRUFBQztZQUNYLElBQUksR0FBRyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4QixJQUFLLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNuQyxJQUFHLEdBQUcsR0FBQyxDQUFDLEVBQUM7Z0JBQ0wsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3hCLEtBQUssSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDO2FBQ3RCO1lBQ0EsS0FBSyxFQUFFLENBQUM7U0FDWjtRQUNELE9BQU8sU0FBUyxDQUFDO0lBQ3BCLENBQUM7SUFFTSx1Q0FBMkIsR0FBbEMsVUFBbUMsSUFBUztRQUMzQyxJQUFJLElBQUksR0FBVyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzFDLE9BQU8sSUFBSSxDQUFDO0lBQ2IsQ0FBQztJQUdNLHVDQUEyQixHQUFsQyxVQUFtQyxRQUF1QixFQUFFLFNBQTBCLEVBQUUsTUFBdUI7UUFBbkQsMEJBQUEsRUFBQSxpQkFBMEI7UUFBRSx1QkFBQSxFQUFBLGNBQXVCO1FBQzlHLElBQUksVUFBVSxHQUFHLGVBQWUsQ0FBQztRQUNqQyxJQUFJLGFBQWEsR0FBRyxVQUFVLENBQUM7UUFDL0IsSUFBSSxVQUFVLEdBQUcsT0FBTyxDQUFDO1FBRXpCLFFBQVEsUUFBUSxFQUFFO1lBQ2hCLEtBQUssNkJBQWEsQ0FBQyxNQUFNO2dCQUN2QixVQUFVLElBQUksSUFBSSxDQUFDO2dCQUNuQixNQUFNO1lBQ1IsS0FBSyw2QkFBYSxDQUFDLE1BQU07Z0JBQ3ZCLFVBQVUsSUFBSSxJQUFJLENBQUM7Z0JBQ25CLE1BQU07WUFDUixLQUFLLDZCQUFhLENBQUMsT0FBTztnQkFDeEIsVUFBVSxJQUFJLEtBQUssQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssNkJBQWEsQ0FBQyxPQUFPO2dCQUN4QixVQUFVLElBQUksS0FBSyxDQUFDO2dCQUNwQixNQUFNO1lBQ1IsS0FBSyw2QkFBYSxDQUFDLFFBQVE7Z0JBQ3pCLFVBQVUsSUFBSSxNQUFNLENBQUM7Z0JBQ3JCLE1BQU07WUFDUixLQUFLLDZCQUFhLENBQUMsUUFBUTtnQkFDekIsVUFBVSxJQUFJLE1BQU0sQ0FBQztnQkFDckIsTUFBTTtZQUNSLEtBQUssNkJBQWEsQ0FBQyxNQUFNO2dCQUN2QixVQUFVLElBQUksSUFBSSxDQUFDO2dCQUNuQixNQUFNO1lBQ1IsS0FBSyw2QkFBYSxDQUFDLE1BQU07Z0JBQ3ZCLFVBQVUsSUFBSSxJQUFJLENBQUM7Z0JBQ25CLE1BQU07U0FDVDtRQUVELElBQUcsU0FBUztZQUFFLE9BQU8sVUFBVSxHQUFHLGFBQWEsQ0FBQztRQUNoRCxJQUFHLE1BQU07WUFBRSxPQUFPLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDMUMsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQztJQUVNLDhCQUFrQixHQUF6QixVQUEwQixRQUFnQixFQUFFLFNBQXFCLEVBQUUsS0FBa0I7UUFBekMsMEJBQUEsRUFBQSxhQUFxQjtRQUFFLHNCQUFBLEVBQUEsVUFBa0I7UUFDbkYsT0FBTyxRQUFRLElBQUksU0FBUyxJQUFJLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxJQUFHLEtBQUssQ0FBQztJQUNqRSxDQUFDO0lBRU8sdUJBQVcsR0FBbEIsVUFBbUIsSUFBSTtRQUN0QixRQUFPLElBQUksRUFBQztZQUNSLHlDQUF5QztZQUN6Qyx3Q0FBd0M7WUFDeEMsd0NBQXdDO1lBQ3hDLHNDQUFzQztZQUN0Qyx3Q0FBd0M7WUFDeEMsc0NBQXNDO1lBRXRDLGtEQUFrRDtZQUNsRCxnREFBZ0Q7WUFDaEQsZ0RBQWdEO1lBQ2hELGtEQUFrRDtZQUNsRCxrREFBa0Q7WUFDbEQsZ0RBQWdEO1lBQ2hELGdEQUFnRDtZQUNoRCxrREFBa0Q7WUFDbEQsa0RBQWtEO1lBQ2xELDhDQUE4QztZQUM5QyxnREFBZ0Q7WUFDaEQsZ0RBQWdEO1lBQ2hELGdEQUFnRDtZQUNoRCxnREFBZ0Q7WUFDaEQsa0RBQWtEO1NBQ3JEO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRU8sd0JBQVksR0FBbkIsVUFBb0IsS0FBYyxFQUFFLFNBQWlCO1FBQ2xELElBQUcsS0FBSyxDQUFDLE1BQU0sSUFBSSxTQUFTO1lBQUUsT0FBTztRQUNyQyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDNUIsSUFBSSxHQUFHLEdBQUcsd0JBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDL0YsS0FBSyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUM7UUFDekIsS0FBSyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRU8scUJBQVMsR0FBakIsVUFBa0IsS0FBYSxFQUFFLFNBQWMsRUFBRSxTQUFxQjtRQUFyQywwQkFBQSxFQUFBLGNBQWM7UUFBRSwwQkFBQSxFQUFBLGFBQXFCO1FBQ3JFLElBQUcsS0FBSyxDQUFDLE1BQU0sSUFBRyxTQUFTO1lBQUUsT0FBTyxLQUFLLENBQUM7UUFDekMsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDeEMsSUFBSSxFQUFFLEdBQUMsRUFBRSxDQUFDO1FBQ1YsaUVBQWlFO1FBQ2pFLGlCQUFpQjtRQUNqQixxQ0FBcUM7UUFDckMsUUFBUTtRQUNSO1lBQ0UsS0FBSSxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFDO2dCQUN2QyxJQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUM7b0JBQ2hCLEVBQUUsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDekIsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUM1QixNQUFNO2lCQUNUO2FBQ0o7WUFDRCxJQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUM7Z0JBQ1IsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDWixLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNuQztTQUNGO1FBRUQsT0FBTyxFQUFFLEdBQUcsSUFBSSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUNILGtCQUFDO0FBQUQsQ0F4YkEsQUF3YkMsSUFBQTs7QUFLRCw0REFBMkQ7QUFDM0QsNkRBQTREO0FBQzVELHFFQUFnRSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludC1kaXNhYmxlICovXG5cbmltcG9ydCBTaWNib01vZHVsZUFkYXB0ZXIgZnJvbSBcIi4uLy4uLy4uLy4uL1NpY2JvTW9kdWxlQWRhcHRlclwiO1xuXG5jb25zdCB7XG4gIFJlc3VsdCxcbiAgSXRlbX0gPSBTaWNib01vZHVsZUFkYXB0ZXIuZ2V0QWxsUmVmcygpO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib0hlbHBlciB7XG4gIC8vU0VDVElPTiBPbGRcbiAgc3RhdGljIGdldFJlc3VsdFR5cGVOYW1lKHNpY2JvUmVzdWx0VHlwZTogU2ljYm9SZXN1bHRUeXBlLCB1cHBlckFsbDogYm9vbGVhbiA9IHRydWUpOiBzdHJpbmcge1xuICAgIHN3aXRjaCAoc2ljYm9SZXN1bHRUeXBlKSB7XG4gICAgICBjYXNlIFNpY2JvUmVzdWx0VHlwZS5UQUk6XG4gICAgICAgIHJldHVybiB1cHBlckFsbD8gXCJUw4BJXCI6IFwiVMOgaVwiO1xuICAgICAgY2FzZSBTaWNib1Jlc3VsdFR5cGUuWElVOlxuICAgICAgICByZXR1cm4gdXBwZXJBbGw/IFwiWOG7iFVcIjogXCJY4buJdVwiO1xuICAgICAgY2FzZSBTaWNib1Jlc3VsdFR5cGUuQkFPOlxuICAgICAgICByZXR1cm4gdXBwZXJBbGw/IFwiQsODT1wiOiBcIkLDo29cIjtcbiAgICB9XG4gIH1cblxuICBzdGF0aWMgZ2V0UmVzdWx0VHlwZShpdGVtczogYW55W10pOiBTaWNib1Jlc3VsdFR5cGUge1xuICAgIGxldCByZXN1bHRUeXBlID0gU2ljYm9SZXN1bHRUeXBlLlRBSTtcbiAgICBpZiAoaXRlbXNbMF0gPT0gaXRlbXNbMV0gJiYgaXRlbXNbMV0gPT0gaXRlbXNbMl0pIHtcbiAgICAgIHJlc3VsdFR5cGUgPSBTaWNib1Jlc3VsdFR5cGUuQkFPO1xuICAgIH0gZWxzZSB7XG4gICAgICBsZXQgdG90YWxWYWx1ZSA9IHRoaXMuZ2V0UmVzdWx0VG90YWxWYWx1ZShpdGVtcyk7XG4gICAgICBpZiAodG90YWxWYWx1ZSA+IDMgJiYgdG90YWxWYWx1ZSA8PSAxMCkge1xuICAgICAgICByZXN1bHRUeXBlID0gU2ljYm9SZXN1bHRUeXBlLlhJVTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdFR5cGU7XG4gIH1cblxuICBzdGF0aWMgZ2V0UmVzdWx0QnJpZ2h0VHlwZShpdGVtczogYW55W10pOiBTaWNib1Jlc3VsdFR5cGUge1xuICAgIGxldCByZXN1bHRUeXBlID0gU2ljYm9SZXN1bHRUeXBlLlRBSTtcbiAgICBsZXQgdG90YWxWYWx1ZSA9IHRoaXMuZ2V0UmVzdWx0VG90YWxWYWx1ZShpdGVtcyk7XG4gICAgaWYgKHRvdGFsVmFsdWUgPj0gMyAmJiB0b3RhbFZhbHVlIDw9IDEwKSB7XG4gICAgICByZXN1bHRUeXBlID0gU2ljYm9SZXN1bHRUeXBlLlhJVTtcbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdFR5cGU7XG4gIH1cblxuICBzdGF0aWMgZ2V0UmVzdWx0VG90YWxWYWx1ZShpdGVtczogYW55W10pOiBudW1iZXIge1xuICAgIGxldCB0b3RhbFZhbHVlID0gMDtcbiAgICBpdGVtcy5mb3JFYWNoKChpdGVtKSA9PiB7XG4gICAgICB0b3RhbFZhbHVlICs9IGl0ZW0gYXMgbnVtYmVyO1xuICAgIH0pO1xuICAgIHJldHVybiB0b3RhbFZhbHVlO1xuICB9XG5cbiAgLy8gc3RhdGljIGxvYWRTaWNib0RpY2UoaXRlbTogRElDRSwgb25Db21wbGV0ZTogKHNwcml0ZUZyYW1lKSA9PiB2b2lkKSB7XG4gIC8vICAgaWYgKGl0ZW0gPT0gRElDRS5ESUNFX1VOU1BFQ0lGSUVEKSBpdGVtID0gRElDRS5PTkU7XG4gIC8vICAgbGV0IHBhdGggPSBHYW1lVXRpbHMuRm9ybWF0U3RyaW5nKFwiSW1hZ2VzL0dhbWUvU2ljYm8vSXRlbXMvWHVjWGFjX3swfVwiLCBpdGVtLnRvU3RyaW5nKCkpO1xuICAvLyAgIFJlc291cmNlTWFuYWdlci5sb2FkQXNzZXQocGF0aCwgY2MuU3ByaXRlRnJhbWUsIChzcHJpdGVGcmFtZTogY2MuU3ByaXRlRnJhbWUpID0+IHtcbiAgLy8gICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKHNwcml0ZUZyYW1lKTtcbiAgLy8gICB9KTtcbiAgLy8gfVxuICAvLyFTRUNUSU9OXG5cbiAgc3RhdGljIGNvbnZlcnRDb2luVHlwZVRvVmFsdWUoY29pblR5cGU6IFNpY2JvQ29pblR5cGUpIHtcbiAgICBzd2l0Y2ggKGNvaW5UeXBlKSB7XG4gICAgICBjYXNlIFNpY2JvQ29pblR5cGUuQ29pbjEwMDpcbiAgICAgICAgcmV0dXJuIDEwMDtcbiAgICAgIGNhc2UgU2ljYm9Db2luVHlwZS5Db2luMjAwOlxuICAgICAgICByZXR1cm4gMjAwO1xuICAgICAgY2FzZSBTaWNib0NvaW5UeXBlLkNvaW41MDA6XG4gICAgICAgIHJldHVybiA1MDA7XG4gICAgICBjYXNlIFNpY2JvQ29pblR5cGUuQ29pbjFrOlxuICAgICAgICByZXR1cm4gMTAwMDtcbiAgICAgIGNhc2UgU2ljYm9Db2luVHlwZS5Db2luMms6XG4gICAgICAgIHJldHVybiAyMDAwO1xuICAgICAgY2FzZSBTaWNib0NvaW5UeXBlLkNvaW41azpcbiAgICAgICAgcmV0dXJuIDUwMDA7XG4gICAgICBjYXNlIFNpY2JvQ29pblR5cGUuQ29pbjEwazpcbiAgICAgICAgcmV0dXJuIDEwMDAwO1xuICAgICAgY2FzZSBTaWNib0NvaW5UeXBlLkNvaW4yMGs6XG4gICAgICAgIHJldHVybiAyMDAwMDtcbiAgICAgIGNhc2UgU2ljYm9Db2luVHlwZS5Db2luNTBrOlxuICAgICAgICByZXR1cm4gNTAwMDA7XG4gICAgICBjYXNlIFNpY2JvQ29pblR5cGUuQ29pbjEwMGs6XG4gICAgICAgIHJldHVybiAxMDAwMDA7XG4gICAgICBjYXNlIFNpY2JvQ29pblR5cGUuQ29pbjIwMGs6XG4gICAgICAgIHJldHVybiAyMDAwMDA7XG4gICAgICBjYXNlIFNpY2JvQ29pblR5cGUuQ29pbjUwMGs6XG4gICAgICAgIHJldHVybiA1MDAwMDA7XG4gICAgICBjYXNlIFNpY2JvQ29pblR5cGUuQ29pbjFNOlxuICAgICAgICByZXR1cm4gMTAwMDAwMDtcbiAgICAgIGNhc2UgU2ljYm9Db2luVHlwZS5Db2luMk06XG4gICAgICAgIHJldHVybiAyMDAwMDAwO1xuICAgICAgY2FzZSBTaWNib0NvaW5UeXBlLkNvaW41TTpcbiAgICAgICAgcmV0dXJuIDUwMDAwMDA7XG4gICAgICBjYXNlIFNpY2JvQ29pblR5cGUuQ29pbjEwTTpcbiAgICAgICAgcmV0dXJuIDEwMDAwMDAwO1xuICAgICAgY2FzZSBTaWNib0NvaW5UeXBlLkNvaW4yME06XG4gICAgICAgIHJldHVybiAyMDAwMDAwMDtcbiAgICAgIGNhc2UgU2ljYm9Db2luVHlwZS5Db2luNTBNOlxuICAgICAgICByZXR1cm4gNTAwMDAwMDA7XG4gICAgfVxuICAgIHJldHVybiAxMDA7XG4gIH1cblxuICBzdGF0aWMgY29udmVydFZhbHVlVG9Db2luVHlwZSh2YWx1ZTogbnVtYmVyKSB7XG4gICAgc3dpdGNoICh2YWx1ZSkge1xuICAgICAgY2FzZSAxMDA6XG4gICAgICAgIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4xMDA7XG4gICAgICBjYXNlIDIwMDpcbiAgICAgICAgcmV0dXJuIFNpY2JvQ29pblR5cGUuQ29pbjIwMDtcbiAgICAgIGNhc2UgNTAwOlxuICAgICAgICByZXR1cm4gU2ljYm9Db2luVHlwZS5Db2luNTAwO1xuICAgICAgY2FzZSAxMDAwOlxuICAgICAgICByZXR1cm4gU2ljYm9Db2luVHlwZS5Db2luMWs7XG4gICAgICBjYXNlIDIwMDA6XG4gICAgICAgIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4yaztcbiAgICAgIGNhc2UgNTAwMDpcbiAgICAgICAgcmV0dXJuIFNpY2JvQ29pblR5cGUuQ29pbjVrO1xuICAgICAgY2FzZSAxMDAwMDpcbiAgICAgICAgcmV0dXJuIFNpY2JvQ29pblR5cGUuQ29pbjEwaztcbiAgICAgIGNhc2UgMjAwMDA6XG4gICAgICAgIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4yMGs7XG4gICAgICBjYXNlIDUwMDAwOlxuICAgICAgICByZXR1cm4gU2ljYm9Db2luVHlwZS5Db2luNTBrO1xuICAgICAgY2FzZSAxMDAwMDA6XG4gICAgICAgIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4xMDBrO1xuICAgICAgY2FzZSAyMDAwMDA6XG4gICAgICAgIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4yMDBrO1xuICAgICAgY2FzZSA1MDAwMDA6XG4gICAgICAgIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW41MDBrO1xuICAgICAgY2FzZSAxMDAwMDAwOlxuICAgICAgICByZXR1cm4gU2ljYm9Db2luVHlwZS5Db2luMU07XG4gICAgICBjYXNlIDIwMDAwMDA6XG4gICAgICAgIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4yTTtcbiAgICAgIGNhc2UgNTAwMDAwMDpcbiAgICAgICAgcmV0dXJuIFNpY2JvQ29pblR5cGUuQ29pbjVNO1xuICAgICAgY2FzZSAxMDAwMDAwMDpcbiAgICAgICAgcmV0dXJuIFNpY2JvQ29pblR5cGUuQ29pbjEwTTtcbiAgICAgIGNhc2UgMjAwMDAwMDA6XG4gICAgICAgIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4yME07XG4gICAgICBjYXNlIDUwMDAwMDAwOlxuICAgICAgICByZXR1cm4gU2ljYm9Db2luVHlwZS5Db2luNTBNO1xuICAgIH1cbiAgfVxuXG5cbiAgc3RhdGljIGNvbnZlcnRWYWx1ZVRvQ2hpcHModmFsdWU6IG51bWJlciwgY29pblR5cGU6IFNpY2JvQ29pblR5cGVbXSkge1xuICAgIGlmICh2YWx1ZSA8PSAwKSByZXR1cm4gY29pblR5cGU7XG4gICAgbGV0IGNoaXBNYXggPSB0aGlzLmNvbnZlcnRWYWx1ZVRvQ2hpcE1heCh2YWx1ZSk7XG4gICAgY29pblR5cGVbY29pblR5cGUubGVuZ3RoXSA9IGNoaXBNYXg7XG4gICAgbGV0IHZhbHVlVGVtcCA9IHZhbHVlIC0gdGhpcy5jb252ZXJ0Q29pblR5cGVUb1ZhbHVlKGNoaXBNYXgpO1xuICAgIHJldHVybiB0aGlzLmNvbnZlcnRWYWx1ZVRvQ2hpcHModmFsdWVUZW1wLCBjb2luVHlwZSk7XG4gIH1cblxuICBwcml2YXRlIHN0YXRpYyBjb252ZXJ0VmFsdWVUb0NoaXBNYXgodmFsdWU6IG51bWJlcikge1xuICAgIGlmICh2YWx1ZSA+PSA1MDAwMDAwMCkgcmV0dXJuIFNpY2JvQ29pblR5cGUuQ29pbjUwTTtcbiAgICBpZiAodmFsdWUgPj0gMjAwMDAwMDApIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4yME07XG4gICAgaWYgKHZhbHVlID49IDEwMDAwMDAwKSByZXR1cm4gU2ljYm9Db2luVHlwZS5Db2luMTBNO1xuICAgIGlmICh2YWx1ZSA+PSA1MDAwMDAwKSByZXR1cm4gU2ljYm9Db2luVHlwZS5Db2luNU07XG4gICAgaWYgKHZhbHVlID49IDIwMDAwMDApIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4yTTtcbiAgICBpZiAodmFsdWUgPj0gMTAwMDAwMCkgcmV0dXJuIFNpY2JvQ29pblR5cGUuQ29pbjFNO1xuICAgIGlmICh2YWx1ZSA+PSA1MDAwMDApIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW41MDBrO1xuICAgIGlmICh2YWx1ZSA+PSAyMDAwMDApIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4yMDBrO1xuICAgIGlmICh2YWx1ZSA+PSAxMDAwMDApIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4xMDBrO1xuICAgIGlmICh2YWx1ZSA+PSA1MDAwMCkgcmV0dXJuIFNpY2JvQ29pblR5cGUuQ29pbjUwaztcbiAgICBpZiAodmFsdWUgPj0gMjAwMDApIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4yMGs7XG4gICAgaWYgKHZhbHVlID49IDEwMDAwKSByZXR1cm4gU2ljYm9Db2luVHlwZS5Db2luMTBrO1xuICAgIGlmICh2YWx1ZSA+PSA1MDAwKSByZXR1cm4gU2ljYm9Db2luVHlwZS5Db2luNWs7XG4gICAgaWYgKHZhbHVlID49IDIwMDApIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4yaztcbiAgICBpZiAodmFsdWUgPj0gMTAwMCkgcmV0dXJuIFNpY2JvQ29pblR5cGUuQ29pbjFrO1xuICAgIGlmICh2YWx1ZSA+PSA1MDApIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW41MDA7XG4gICAgaWYgKHZhbHVlID49IDIwMCkgcmV0dXJuIFNpY2JvQ29pblR5cGUuQ29pbjIwMDtcblxuICAgIHJldHVybiBTaWNib0NvaW5UeXBlLkNvaW4xMDA7XG4gIH1cbiAgc3RhdGljIGNvbnZlcnRMdWNreU51bVRvU3RyaW5nKHZhbHVlOiBudW1iZXIpOiBzdHJpbmcge1xuICAgIGlmICh2YWx1ZSA8IDEwKSByZXR1cm4gXCIwMDAwMFwiICsgdmFsdWU7XG4gICAgaWYgKHZhbHVlIDwgMTAwKSByZXR1cm4gXCIwMDAwXCIgKyB2YWx1ZTtcbiAgICBpZiAodmFsdWUgPCAxMDAwKSByZXR1cm4gXCIwMDBcIiArIHZhbHVlO1xuICAgIGlmICh2YWx1ZSA8IDEwMDAwKSByZXR1cm4gXCIwMFwiICsgdmFsdWU7XG4gICAgaWYgKHZhbHVlIDwgMTAwMDAwKSByZXR1cm4gXCIwXCIgKyB2YWx1ZTtcbiAgICByZXR1cm4gdmFsdWUudG9TdHJpbmcoKTtcbiAgfVxuICAvLyBzdGF0aWMgZ2V0QWxsQmV0VHlwZVdpdGhNaW5CZXQoXG4gIC8vICAgdmFsdWU6IG51bWJlcixcbiAgLy8gICBjaGFubmVsVHlwZTogQ0hBTk5FTF9UWVBFXG4gIC8vICk6IG51bWJlcltdIHtcbiAgLy8gICBsZXQgbWF4VmFsdWUgPSB0aGlzLmdldE1heEJldFdpdGhNaW5CZXQodmFsdWUsIGNoYW5uZWxUeXBlKTtcbiAgLy8gICBsZXQgdmFsdWVzID0gdGhpcy5nZXRBbGxCdXlJbigpLmZpbHRlcigoeCkgPT4geCA+PSB2YWx1ZSAmJiB4IDw9IG1heFZhbHVlKTtcbiAgLy8gICByZXR1cm4gdmFsdWVzO1xuICAvLyB9XG5cbiAgLy8gc3RhdGljIGdldEFsbEJldFR5cGVXaXRoTWluQmV0U2ljYm9NaW5pKFxuICAvLyAgIHZhbHVlOiBudW1iZXIsXG4gIC8vICAgY2hhbm5lbFR5cGU6IENIQU5ORUxfVFlQRVxuICAvLyApOiBDb2luVHlwZVtdIHtcbiAgLy8gICBsZXQgbWF4VmFsdWUgPSB0aGlzLmdldE1heEJldFdpdGhNaW5CZXQodmFsdWUsIGNoYW5uZWxUeXBlKTtcbiAgLy8gICBsZXQgdmFsdWVzID0gdGhpcy5nZXRBbGxCdXlJblNpY2JvTWluaSgpLmZpbHRlcihcbiAgLy8gICAgICh4KSA9PiB4ID49IHZhbHVlICYmIHggPD0gbWF4VmFsdWVcbiAgLy8gICApO1xuXG4gIC8vICAgbGV0IEl0ZW1UeXBlczogQ29pblR5cGVbXSA9IFtdO1xuICAvLyAgIHZhbHVlcy5mb3JFYWNoKCh2YWx1ZSwgaW5kZXgpID0+IHtcbiAgLy8gICAgIGxldCBjb2luVHlwZSA9IHRoaXMuY29udmVydFZhbHVlVG9JdGVtVHlwZSh2YWx1ZSk7XG5cbiAgLy8gICAgIEl0ZW1UeXBlc1tpbmRleF0gPSBjb2luVHlwZTtcbiAgLy8gICB9KTtcbiAgLy8gICByZXR1cm4gSXRlbVR5cGVzO1xuICAvLyB9XG4gIFxuXG4gIHN0YXRpYyBjb252ZXJ0U2ljYm9JdGVtVG9TdHJpbmcoaXRlbSkge1xuICAgIHZhciBJdGVtTmFtZSA9IFwiXCI7XG4gICAgc3dpdGNoIChpdGVtKSB7XG4gICAgICBjYXNlIEl0ZW0uT05FOlxuICAgICAgICBJdGVtTmFtZSA9IFwiTeG7mXRcIjtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIEl0ZW0uVFdPOlxuICAgICAgICBJdGVtTmFtZSA9IFwiSGFpXCI7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBJdGVtLlRIUkVFOlxuICAgICAgICBJdGVtTmFtZSA9IFwiQmFcIjtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIEl0ZW0uRk9VUjpcbiAgICAgICAgSXRlbU5hbWUgPSBcIkLhu5FuXCI7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBJdGVtLkZJVkU6XG4gICAgICAgIEl0ZW1OYW1lID0gXCJOxINtXCI7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBJdGVtLlNJWDpcbiAgICAgICAgSXRlbU5hbWUgPSBcIlPDoXVcIjtcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuICAgIHJldHVybiBJdGVtTmFtZTtcbiAgfVxuXG4gIHN0YXRpYyBTdGF0dXNUb1N0cmluZyhzdGF0dXMpOnN0cmluZ3sgICAgXG4gICAgbGV0IFN0YXR1c1N0cmluZyA9IFsgXG4gICAgICBcIlNUQVRVU19VTlNQRUNJRklFRFwiLFxuICAgICAgXCJXQUlUSU5HXCIsXG4gICAgICBcIkJFVFRJTkdcIixcbiAgICAgIFwiV0FJVElOR19SRVNVTFRcIixcbiAgICAgIFwiUkVTVUxUSU5HXCIsXG4gICAgICBcIlNIT1dJTkdfSkFDS1BPVFwiLFxuICAgICAgXCJTSE9XSU5HX1JFU1VMVFwiLFxuICAgICAgXCJQQVlJTkdcIixcbiAgICAgIFwiRklOSVNISU5HXCIsXG4gICAgXVxuICAgIHJldHVybiBTdGF0dXNTdHJpbmdbc3RhdHVzXTtcbiAgfVxuXG5cbiAgc3RhdGljIGdldFJlc3VsdE1lc3NhZ2UoKSB7XG4gICAgbGV0IG1zZyA9IFwiXCI7XG4gICAgLy8gaWYgKHJlc3VsdCkge1xuICAgIC8vICAgbGV0IEl0ZW1XaW5zID0gcmVzdWx0LmdldEl0ZW1zTGlzdCgpO1xuICAgIC8vICAgc3dpdGNoIChyZXN1bHQuZ2V0VHlwZSgpKSB7XG4gICAgLy8gICAgIGNhc2UgUmVzdWx0LlRZUEUuSkFDS1BPVDpcbiAgICAvLyAgICAgY2FzZSBSZXN1bHQuVFlQRS5QVVQyUE9UOlxuICAgIC8vICAgICBjYXNlIFJlc3VsdC5UWVBFLk5PUk1BTDpcbiAgICAvLyAgICAgICBJdGVtV2lucy5mb3JFYWNoKChJdGVtLCBpbmRleCkgPT4ge1xuICAgIC8vICAgICAgICAgaWYgKGluZGV4ID09IEl0ZW1XaW5zLmxlbmd0aCAtIDEpIHtcbiAgICAvLyAgICAgICAgICAgbXNnID0gbXNnICsgdGhpcy5jb252ZXJ0U2ljYm9JdGVtVG9TdHJpbmcoSXRlbSk7XG4gICAgLy8gICAgICAgICB9IGVsc2Uge1xuICAgIC8vICAgICAgICAgICBtc2cgPSBtc2cgKyB0aGlzLmNvbnZlcnRTaWNib0l0ZW1Ub1N0cmluZyhJdGVtKSArIFwiIFwiO1xuICAgIC8vICAgICAgICAgfVxuICAgIC8vICAgICAgIH0pO1xuICAgIC8vICAgICAgIGJyZWFrO1xuICAgIC8vICAgfVxuICAgIC8vIH1cbiAgICByZXR1cm4gbXNnO1xuICB9XG5cbiAgc3RhdGljIGdldFJlc3VsdEphY2twb3RNZXNzYWdlKHJlc3VsdDogSW5zdGFuY2VUeXBlPHR5cGVvZiBSZXN1bHQ+KSB7XG4gICAgbGV0IG1zZyA9IFwiXCI7XG4gICAgaWYgKHJlc3VsdCkge1xuICAgICAgLy8gc3dpdGNoIChyZXN1bHQuZ2V0VHlwZSgpKSB7XG4gICAgICAvLyAgIGNhc2UgUmVzdWx0LlRZUEUuSkFDS1BPVDpcbiAgICAgIC8vICAgICBtc2cgPSBcIkLDo28gXCIgKyB0aGlzLmNvbnZlcnRTaWNib0l0ZW1Ub1N0cmluZyhJdGVtV2luc1swXSk7XG4gICAgICAvLyAgICAgYnJlYWs7XG4gICAgICAvLyB9XG4gICAgfVxuICAgIHJldHVybiBtc2c7XG4gIH1cblxuICBzdGF0aWMgY29udmVydEl0ZW1LZXlUb0l0ZW1zKGtleTogc3RyaW5nKTogbnVtYmVyW10ge1xuICAgIHJldHVybiBrZXkuc3BsaXQoXCIsXCIpLm1hcCgodG1wKSA9PiBwYXJzZUludCh0bXApKTtcbiAgfVxuXG5cbiAgc3RhdGljIGdldE15SmFja1BvdCgpOiBudW1iZXIge1xuICAgIGxldCBtb25leSA9IDA7XG4gICAgLy8gaWYgKHJlc3VsdCAmJiByZXN1bHQuZ2V0V2lubmVyc0xpc3QoKSkge1xuICAgIC8vICAgcmVzdWx0LmdldFdpbm5lcnNMaXN0KCkuZm9yRWFjaCgocmVjb3JkKSA9PiB7XG4gICAgLy8gICAgIGlmIChyZWNvcmQgIT0gbnVsbCAmJiByZWNvcmQuaGFzQmV0KCkpIHtcbiAgICAvLyAgICAgICBsZXQgdXNlcklkID0gcmVjb3JkLmdldFVzZXJJZCgpO1xuICAgIC8vICAgICAgIGlmICh1c2VySWQgPT0gVXNlck1hbmFnZXIuZ2V0SW5zdGFuY2UoKS51c2VyRGF0YS51c2VySWQpIHtcbiAgICAvLyAgICAgICAgIG1vbmV5ID0gbW9uZXkgKyByZWNvcmQuZ2V0QW1vdW50RnJvbVBvdCgpO1xuICAgIC8vICAgICAgIH1cbiAgICAvLyAgICAgfVxuICAgIC8vICAgfSk7XG4gICAgLy8gfVxuICAgIHJldHVybiBtb25leTtcbiAgfVxuXG4gIHN0YXRpYyBjb252ZXJ0SXRlbXNUb0l0ZW1LZXkoSXRlbXM6IGFueVtdKTogc3RyaW5nIHtcbiAgICBsZXQga2V5ID0gXCJcIjtcbiAgICBJdGVtcy5mb3JFYWNoKChJdGVtLCBpbmRleCkgPT4ge1xuICAgICAgaWYgKGluZGV4ID09IEl0ZW1zLmxlbmd0aCAtIDEpIHtcbiAgICAgICAga2V5ICs9IEl0ZW07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBrZXkgKz0gSXRlbSArIFwiLFwiO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBrZXk7XG4gIH1cblxuICAvL0VYOiBsZXQgcnMgPSBjYWxjdWxhdGVOdW1PZkNoaXBUb01ha2VWYWx1ZSgxNTE1NjAwMCwgY2hpcF9sZXZlbCk7XG4gIC8vIHJzLmZvckVhY2goKG51bTogbnVtYmVyLCB2YWw6IG51bWJlcikgPT4ge1xuICAvLyAgIC8vY29uc29sZS5sb2coXCJ2YWw6IFwiICsgdmFsICsgXCIgLT4gXCIgKyBudW0pO1xuICAvLyB9KTtcbiAgLy9cbiAgLy9SZXN1bHQ6ICAgXG4gIC8vIFtMT0ddOiBcInZhbDogNTAwMDAwMCAtPiAzXCIgXG4gIC8vIFtMT0ddOiBcInZhbDogMTAwMDAwIC0+IDFcIiBcbiAgLy8gW0xPR106IFwidmFsOiA1MDAwMCAtPiAxXCIgXG4gIC8vIFtMT0ddOiBcInZhbDogNTAwMCAtPiAxXCIgXG4gIC8vIFtMT0ddOiBcInZhbDogMTAwMCAtPiAxXCIgXG4gIHN0YXRpYyBjYWxjdWxhdGVOdW1PZkNoaXBUb01ha2VWYWx1ZShpbnB1dDogbnVtYmVyLCBjb25maWc6IG51bWJlcltdKXtcbiAgICBsZXQgcmVzdWx0TWFwID0gbmV3IE1hcDxudW1iZXIsIG51bWJlcj4oKTtcbiAgICBsZXQgaW5kZXggPSBjb25maWcubGVuZ3RoIC0gMTsgICAgXG4gICAgd2hpbGUoaW5kZXg+PTApeyAgICAgICBcbiAgICAgICAgbGV0IHZhbCA9IGNvbmZpZ1tpbmRleF07XG4gICAgICAgIGxldCAgbnVtID0gTWF0aC5mbG9vcihpbnB1dCAvIHZhbCk7ICAgICAgICBcbiAgICAgICAgaWYobnVtPjApe1xuICAgICAgICAgICAgcmVzdWx0TWFwLnNldCh2YWwsIG51bSk7XG4gICAgICAgICAgICBpbnB1dCAtPSB2YWwgKiBudW07XG4gICAgICAgIH1cbiAgICAgICAgIGluZGV4LS07XG4gICAgfVxuICAgIHJldHVybiByZXN1bHRNYXA7XG4gfVxuXG4gc3RhdGljIGNvbnZlcnRJdGVtVG9EaWNlU3ByaXRlTmFtZShpdGVtOiBhbnkpe1xuICBsZXQgbmFtZTogc3RyaW5nID0gXCJTQl9tYWludGFibGVfXCIgKyBpdGVtO1xuICByZXR1cm4gbmFtZTtcbiB9XG5cblxuIHN0YXRpYyBjb252ZXJ0Q29pblR5cGVUb1Nwcml0ZU5hbWUoY29pblR5cGU6IFNpY2JvQ29pblR5cGUsIGlzRGlzYWJsZTogYm9vbGVhbiA9IGZhbHNlLCBpc01pbmk6IGJvb2xlYW4gPSBmYWxzZSkge1xuICBsZXQgc3ByaXRlTmFtZSA9IFwiU0JfTWFpbl9DaGlwX1wiO1xuICBsZXQgZGlzYWJsZVN1ZmZpeCA9IFwiX0Rpc2FibGVcIjtcbiAgbGV0IG1pbmlTdWZmaXggPSBcIl9NaW5pXCI7XG5cbiAgc3dpdGNoIChjb2luVHlwZSkge1xuICAgIGNhc2UgU2ljYm9Db2luVHlwZS5Db2luMWs6XG4gICAgICBzcHJpdGVOYW1lICs9IFwiMUtcIjtcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgU2ljYm9Db2luVHlwZS5Db2luNWs6XG4gICAgICBzcHJpdGVOYW1lICs9IFwiNUtcIjtcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgU2ljYm9Db2luVHlwZS5Db2luMTBrOlxuICAgICAgc3ByaXRlTmFtZSArPSBcIjEwS1wiO1xuICAgICAgYnJlYWs7ICAgICAgXG4gICAgY2FzZSBTaWNib0NvaW5UeXBlLkNvaW41MGs6XG4gICAgICBzcHJpdGVOYW1lICs9IFwiNTBLXCI7XG4gICAgICBicmVhaztcbiAgICBjYXNlIFNpY2JvQ29pblR5cGUuQ29pbjEwMGs6XG4gICAgICBzcHJpdGVOYW1lICs9IFwiMTAwS1wiO1xuICAgICAgYnJlYWs7ICAgICAgXG4gICAgY2FzZSBTaWNib0NvaW5UeXBlLkNvaW41MDBrOlxuICAgICAgc3ByaXRlTmFtZSArPSBcIjUwMEtcIjtcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgU2ljYm9Db2luVHlwZS5Db2luMU06XG4gICAgICBzcHJpdGVOYW1lICs9IFwiMU1cIjtcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgU2ljYm9Db2luVHlwZS5Db2luNU06XG4gICAgICBzcHJpdGVOYW1lICs9IFwiNU1cIjtcbiAgICAgIGJyZWFrOyAgICAgIFxuICB9XG4gIFxuICBpZihpc0Rpc2FibGUpIHJldHVybiBzcHJpdGVOYW1lICsgZGlzYWJsZVN1ZmZpeDtcbiAgaWYoaXNNaW5pKSByZXR1cm4gc3ByaXRlTmFtZSArIG1pbmlTdWZmaXg7XG4gIHJldHVybiBzcHJpdGVOYW1lO1xufVxuXG5zdGF0aWMgYXBwcm94aW1hdGVseUVxdWFsKGNvbXBhcmVyOiBudW1iZXIsIGNvbXBhcmVUbzogbnVtYmVyID0gMCwgZGVsdGE6IG51bWJlciA9IC4xKTogYm9vbGVhbntcbiAgcmV0dXJuIGNvbXBhcmVyID49IGNvbXBhcmVUbyAmJiAoY29tcGFyZXIgLSBjb21wYXJlVG8pPD0gZGVsdGE7XG59XG5cbiBzdGF0aWMgZG9vclRvSXRlbXMoZG9vcik6IGFueVtdey8vTk9URTogVEhSRUVfS0lORFMgd2lsbCBub3QgYmUgY2hlY2tlZFxuICBzd2l0Y2goZG9vcil7XG4gICAgICAvLyBjYXNlIERvb3IuQkFVX09ORTogcmV0dXJuIFtJdGVtLkJBVV07IFxuICAgICAgLy8gY2FzZSBEb29yLkNVQV9PTkU6IHJldHVybiBbSXRlbS5DVUFdO1xuICAgICAgLy8gY2FzZSBEb29yLlRPTV9PTkU6IHJldHVybiBbSXRlbS5UT01dO1xuICAgICAgLy8gY2FzZSBEb29yLkNBX09ORTogcmV0dXJuIFtJdGVtLkNBXTtcbiAgICAgIC8vIGNhc2UgRG9vci5OQUlfT05FOiByZXR1cm4gW0l0ZW0uTkFJXTtcbiAgICAgIC8vIGNhc2UgRG9vci5HQV9PTkU6IHJldHVybiBbSXRlbS5HQV07XG5cbiAgICAgIC8vIGNhc2UgRG9vci5CQVVfVE9NOiByZXR1cm4gW0l0ZW0uQkFVLCBJdGVtLlRPTV07XG4gICAgICAvLyBjYXNlIERvb3IuQkFVX0NBOiByZXR1cm4gW0l0ZW0uQkFVLCBJdGVtLkNBXTtcbiAgICAgIC8vIGNhc2UgRG9vci5CQVVfR0E6IHJldHVybiBbSXRlbS5CQVUsIEl0ZW0uR0FdO1xuICAgICAgLy8gY2FzZSBEb29yLkJBVV9DVUE6IHJldHVybiBbSXRlbS5CQVUsIEl0ZW0uQ1VBXTtcbiAgICAgIC8vIGNhc2UgRG9vci5CQVVfTkFJOiByZXR1cm4gW0l0ZW0uQkFVLCBJdGVtLk5BSV07XG4gICAgICAvLyBjYXNlIERvb3IuVE9NX0NBOiByZXR1cm4gW0l0ZW0uVE9NLCBJdGVtLkNBXTtcbiAgICAgIC8vIGNhc2UgRG9vci5UT01fR0E6IHJldHVybiBbSXRlbS5UT00sIEl0ZW0uR0FdO1xuICAgICAgLy8gY2FzZSBEb29yLlRPTV9DVUE6IHJldHVybiBbSXRlbS5UT00sIEl0ZW0uQ1VBXTtcbiAgICAgIC8vIGNhc2UgRG9vci5UT01fTkFJOiByZXR1cm4gW0l0ZW0uVE9NLCBJdGVtLk5BSV07XG4gICAgICAvLyBjYXNlIERvb3IuQ0FfR0E6IHJldHVybiBbSXRlbS5DQSwgSXRlbS5HQV07XG4gICAgICAvLyBjYXNlIERvb3IuQ0FfQ1VBOiByZXR1cm4gW0l0ZW0uQ0EsIEl0ZW0uQ1VBXTtcbiAgICAgIC8vIGNhc2UgRG9vci5DQV9OQUk6IHJldHVybiBbSXRlbS5DQSwgSXRlbS5OQUldO1xuICAgICAgLy8gY2FzZSBEb29yLkdBX0NVQTogcmV0dXJuIFtJdGVtLkdBLCBJdGVtLkNVQV07XG4gICAgICAvLyBjYXNlIERvb3IuR0FfTkFJOiByZXR1cm4gW0l0ZW0uR0EsIEl0ZW0uTkFJXTtcbiAgICAgIC8vIGNhc2UgRG9vci5DVUFfTkFJOiByZXR1cm4gW0l0ZW0uQ1VBLCBJdGVtLk5BSV07XG4gIH1cbiAgcmV0dXJuIFtdO1xufVxuIFxuIHN0YXRpYyBjaGFuZ2VQYXJlbnQoY2hpbGQ6IGNjLk5vZGUsIG5ld1BhcmVudDpjYy5Ob2RlKXtcbiAgICBpZihjaGlsZC5wYXJlbnQgPT0gbmV3UGFyZW50KSByZXR1cm47XG4gICAgbGV0IHAgPSBjaGlsZC5nZXRQb3NpdGlvbigpO1xuICAgIGxldCBwb3MgPSBTaWNib0dhbWVVdGlscy5jb252ZXJ0VG9PdGhlck5vZGUoY2hpbGQucGFyZW50LCBuZXdQYXJlbnQsIG5ldyBjYy5WZWMzKHAueCwgcC55LCAwKSk7XG4gICAgY2hpbGQucGFyZW50ID0gbmV3UGFyZW50O1xuICAgIGNoaWxkLnNldFBvc2l0aW9uKHBvcyk7ICAgIFxuICB9XG5cbiAgc3RhdGljICBmaXhTdHJpbmcoaW5wdXQ6IHN0cmluZywgbWF4TGVuZ3RoID0gMTUsIGNoZWNrQ2hhcjogbnVtYmVyID0gNyk6IHN0cmluZ3sgICAgXG4gICBpZihpbnB1dC5sZW5ndGg8PSBtYXhMZW5ndGgpIHJldHVybiBpbnB1dDtcbiAgICBsZXQgc3Vic3RyID0gaW5wdXQuc3Vic3RyKDAsIG1heExlbmd0aCk7XG4gICAgbGV0IHJzPVwiXCI7XG4gICAgLy8gaWYoaW5wdXRbbWF4TGVuZ3RoICsgMV0gPT0gJyAnfHxpbnB1dFttYXhMZW5ndGggKyAxXSA9PSAnXFxuJyl7XG4gICAgLy8gICBycyA9IHN1YnN0cjtcbiAgICAvLyAgIGlucHV0ID0gaW5wdXQuc3Vic3RyKG1heExlbmd0aCk7XG4gICAgLy8gfWVsc2VcbiAgICB7ICAgICAgICBcbiAgICAgIGZvcihsZXQgaSA9IHN1YnN0ci5sZW5ndGggLSAxOyBpID49IDA7IGktLSl7XG4gICAgICAgICAgaWYoc3Vic3RyW2ldID09ICcgJyl7XG4gICAgICAgICAgICAgIHJzID0gc3Vic3RyLnN1YnN0cigwLCBpKTtcbiAgICAgICAgICAgICAgaW5wdXQgPSBpbnB1dC5zdWJzdHIoaSArIDEpO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZihycyA9PSBcIlwiKXtcbiAgICAgICAgICBycyA9IHN1YnN0cjtcbiAgICAgICAgICBpbnB1dCA9IGlucHV0LnN1YnN0cihtYXhMZW5ndGgpO1xuICAgICAgfVxuICAgIH1cbiAgICBcbiAgICByZXR1cm4gcnMgKyBcIlxcblwiICsgU2ljYm9IZWxwZXIuZml4U3RyaW5nKGlucHV0KTtcbiAgfVxufVxuXG5cblxuXG5pbXBvcnQgeyBTaWNib1Jlc3VsdFR5cGUgfSBmcm9tIFwiLi4vRW51bXMvU2ljYm9SZXN1bHRUeXBlXCI7XG5pbXBvcnQgeyBTaWNib0NvaW5UeXBlIH0gZnJvbSBcIi4uL1JOR0NvbW1vbnMvU2ljYm9Db2luVHlwZVwiO1xuaW1wb3J0IFNpY2JvR2FtZVV0aWxzIGZyb20gJy4uL1JOR0NvbW1vbnMvVXRpbHMvU2ljYm9HYW1lVXRpbHMnO1xuXG4iXX0=