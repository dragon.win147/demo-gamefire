
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/SicboTracking.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9a88d1UPeNGFLkf8Qr5yCoY', 'SicboTracking');
// Game/Sicbo_New/Scripts/SicboTracking.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
//SECTION - FIREBASE const
var FIREBASE_TRACKING_EVENT = "FIREBASE_TRACKING_EVENT";
var FIREBASE_BET_EVENT = "gp_sicbo_"; //gp_sicbo_{action_name}
var FirebaseParamVersion = "Version";
var FirebaseParamSessionId = "SessionId";
//!SECTION
//SECTION - APPFLYER Const
var APPS_FLYER_TRACKING_EVENT = "APPS_FLYER_TRACKING_EVENT";
var AF_EVENT_BET = "af_placed_bet_sicbo";
var AF_EVENT_REBET = "af_placed_bet_sicbo";
var AF_EVENT_DOUBLEBET = "af_placed_bet_sicbo";
var AF_CURRENCY = "VND";
var AF_ParamPrice = "af_price";
var AF_ParamCurrency = "af_currency";
var AF_ParamSession = "event_number";
var AF_ParamDate = "event_date";
var AF_ParamTime = "event_time";
var AF_ParamTransactionId = "transaction_id";
//!SECTION
var SicboTracking = /** @class */ (function () {
    function SicboTracking() {
    }
    SicboTracking_1 = SicboTracking;
    SicboTracking.setGameVersion = function (ver) {
        SicboTracking_1.version = ver;
    };
    SicboTracking.startSession = function (sesssionId) {
        SicboTracking_1.sesssionId = sesssionId;
    };
    SicboTracking.trackingBet = function (transactionId, amount) {
        SicboTracking_1.firebaseTrackingBet();
        SicboTracking_1.appflyerTrackingBet(transactionId, amount);
    };
    SicboTracking.trackingReBet = function (transactionId, amount) {
        SicboTracking_1.firebaseTrackingReBet();
        SicboTracking_1.appflyerTrackingReBet(transactionId, amount);
    };
    SicboTracking.trackingDoubleBet = function (transactionId, amount) {
        SicboTracking_1.firebaseTrackingDoubleBet();
        SicboTracking_1.appflyerTrackingDoubleBet(transactionId, amount);
    };
    //SECTION Appflyer
    SicboTracking.appflyerTrackingBet = function (transactionId, amount) {
        var eventName = AF_EVENT_BET;
        var eventValue = {};
        eventValue[AF_ParamPrice] = Math.abs(amount);
        eventValue[AF_ParamTransactionId] = transactionId;
        eventValue[AF_ParamCurrency] = AF_CURRENCY;
        SicboTracking_1.sendTrackingAppflyer(eventName, eventValue);
    };
    SicboTracking.appflyerTrackingReBet = function (transactionId, amount) {
        var eventName = AF_EVENT_REBET;
        var eventValue = {};
        eventValue[AF_ParamPrice] = Math.abs(amount);
        eventValue[AF_ParamTransactionId] = transactionId;
        eventValue[AF_ParamCurrency] = AF_CURRENCY;
        SicboTracking_1.sendTrackingAppflyer(eventName, eventValue);
    };
    SicboTracking.appflyerTrackingDoubleBet = function (transactionId, amount) {
        var eventName = AF_EVENT_DOUBLEBET;
        var eventValue = {};
        eventValue[AF_ParamPrice] = Math.abs(amount);
        eventValue[AF_ParamTransactionId] = transactionId;
        eventValue[AF_ParamCurrency] = AF_CURRENCY;
        SicboTracking_1.sendTrackingAppflyer(eventName, eventValue);
    };
    SicboTracking.sendTrackingAppflyer = function (eventName, eventValue) {
        eventValue[AF_ParamSession] = SicboTracking_1.sesssionId;
        eventValue[AF_ParamDate] = SicboTracking_1.getCurrentDate();
        eventValue[AF_ParamTime] = SicboTracking_1.getCurrentTime();
        var evt = {
            eventType: "EVENT",
            eventName: eventName,
            eventValue: eventValue,
        };
        //send event
        cc.systemEvent.emit(APPS_FLYER_TRACKING_EVENT, evt);
    };
    //!SECTION
    //SECTION Firebase
    SicboTracking.firebaseTrackingBet = function () {
        var eventName = FIREBASE_BET_EVENT + "bet";
        var params = {};
        SicboTracking_1.sendTrackingFirebase(eventName, params);
    };
    SicboTracking.firebaseTrackingReBet = function () {
        var eventName = FIREBASE_BET_EVENT + "re_bet";
        var params = {};
        SicboTracking_1.sendTrackingFirebase(eventName, params);
    };
    SicboTracking.firebaseTrackingDoubleBet = function () {
        var eventName = FIREBASE_BET_EVENT + "double_bet";
        var params = {};
        SicboTracking_1.sendTrackingFirebase(eventName, params);
    };
    SicboTracking.sendTrackingFirebase = function (eventName, params) {
        params[FirebaseParamVersion] = SicboTracking_1.version;
        params[FirebaseParamSessionId] = SicboTracking_1.sesssionId;
        cc.systemEvent.emit(FIREBASE_TRACKING_EVENT, eventName, params);
    };
    //!SECTION
    /**
     * get current date as DDMMYYYY
     */
    SicboTracking.getCurrentDate = function () {
        var currentDate = new Date();
        // Get day, month, and year
        var day = currentDate.getDate().toString().padStart(2, "0");
        var month = (currentDate.getMonth() + 1).toString().padStart(2, "0"); // Month is zero-based
        var year = currentDate.getFullYear().toString();
        // Concatenate the date parts
        var formattedDate = day + month + year;
        return formattedDate;
    };
    /**
     * get current time as HH:MM
     */
    SicboTracking.getCurrentTime = function () {
        var currentDate = new Date();
        // Get hours and minutes
        var hours = currentDate.getHours().toString().padStart(2, "0");
        var minutes = currentDate.getMinutes().toString().padStart(2, "0");
        // Concatenate the time parts
        var formattedTime = hours + ":" + minutes;
        return formattedTime;
    };
    var SicboTracking_1;
    SicboTracking = SicboTracking_1 = __decorate([
        ccclass
    ], SicboTracking);
    return SicboTracking;
}());
exports.default = SicboTracking;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1NpY2JvVHJhY2tpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7OztBQUU1RSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUU1QywwQkFBMEI7QUFDMUIsSUFBTSx1QkFBdUIsR0FBRyx5QkFBeUIsQ0FBQztBQUMxRCxJQUFNLGtCQUFrQixHQUFHLFdBQVcsQ0FBQyxDQUFBLHdCQUF3QjtBQUMvRCxJQUFNLG9CQUFvQixHQUFHLFNBQVMsQ0FBQztBQUN2QyxJQUFNLHNCQUFzQixHQUFHLFdBQVcsQ0FBQztBQUMzQyxVQUFVO0FBR1YsMEJBQTBCO0FBQzFCLElBQU0seUJBQXlCLEdBQUcsMkJBQTJCLENBQUE7QUFDN0QsSUFBTSxZQUFZLEdBQUcscUJBQXFCLENBQUE7QUFDMUMsSUFBTSxjQUFjLEdBQUcscUJBQXFCLENBQUE7QUFDNUMsSUFBTSxrQkFBa0IsR0FBRyxxQkFBcUIsQ0FBQTtBQUVoRCxJQUFNLFdBQVcsR0FBRyxLQUFLLENBQUE7QUFFekIsSUFBTSxhQUFhLEdBQUcsVUFBVSxDQUFBO0FBQ2hDLElBQU0sZ0JBQWdCLEdBQUcsYUFBYSxDQUFBO0FBQ3RDLElBQU0sZUFBZSxHQUFHLGNBQWMsQ0FBQTtBQUN0QyxJQUFNLFlBQVksR0FBRyxZQUFZLENBQUE7QUFDakMsSUFBTSxZQUFZLEdBQUcsWUFBWSxDQUFBO0FBQ2pDLElBQU0scUJBQXFCLEdBQUcsZ0JBQWdCLENBQUE7QUFDOUMsVUFBVTtBQUlWO0lBQUE7SUF3SUEsQ0FBQztzQkF4SW9CLGFBQWE7SUFJekIsNEJBQWMsR0FBckIsVUFBc0IsR0FBVztRQUMvQixlQUFhLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztJQUM5QixDQUFDO0lBRU0sMEJBQVksR0FBbkIsVUFBb0IsVUFBa0I7UUFDcEMsZUFBYSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7SUFDeEMsQ0FBQztJQUdNLHlCQUFXLEdBQWxCLFVBQW1CLGFBQWEsRUFBRSxNQUFNO1FBQ3RDLGVBQWEsQ0FBQyxtQkFBbUIsRUFBRSxDQUFBO1FBQ25DLGVBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLENBQUE7SUFDMUQsQ0FBQztJQUVNLDJCQUFhLEdBQXBCLFVBQXFCLGFBQWEsRUFBRSxNQUFNO1FBQ3hDLGVBQWEsQ0FBQyxxQkFBcUIsRUFBRSxDQUFBO1FBQ3JDLGVBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLENBQUE7SUFDNUQsQ0FBQztJQUVNLCtCQUFpQixHQUF4QixVQUF5QixhQUFhLEVBQUUsTUFBTTtRQUM1QyxlQUFhLENBQUMseUJBQXlCLEVBQUUsQ0FBQztRQUMxQyxlQUFhLENBQUMseUJBQXlCLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFBO0lBQ2hFLENBQUM7SUFFRCxrQkFBa0I7SUFDWCxpQ0FBbUIsR0FBMUIsVUFBMkIsYUFBYSxFQUFFLE1BQU07UUFDOUMsSUFBSSxTQUFTLEdBQUcsWUFBWSxDQUFDO1FBQzdCLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUVwQixVQUFVLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3QyxVQUFVLENBQUMscUJBQXFCLENBQUMsR0FBRyxhQUFhLENBQUM7UUFDbEQsVUFBVSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsV0FBVyxDQUFDO1FBRTNDLGVBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVNLG1DQUFxQixHQUE1QixVQUE2QixhQUFhLEVBQUUsTUFBTTtRQUNoRCxJQUFJLFNBQVMsR0FBRyxjQUFjLENBQUM7UUFDL0IsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBRXBCLFVBQVUsQ0FBQyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzdDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLGFBQWEsQ0FBQztRQUNsRCxVQUFVLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxXQUFXLENBQUM7UUFFM0MsZUFBYSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUM1RCxDQUFDO0lBRU0sdUNBQXlCLEdBQWhDLFVBQWlDLGFBQWEsRUFBRSxNQUFNO1FBQ3BELElBQUksU0FBUyxHQUFHLGtCQUFrQixDQUFDO1FBQ25DLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUVwQixVQUFVLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3QyxVQUFVLENBQUMscUJBQXFCLENBQUMsR0FBRyxhQUFhLENBQUM7UUFDbEQsVUFBVSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsV0FBVyxDQUFDO1FBRTNDLGVBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVjLGtDQUFvQixHQUFuQyxVQUFvQyxTQUFTLEVBQUUsVUFBVTtRQUN2RCxVQUFVLENBQUMsZUFBZSxDQUFDLEdBQUcsZUFBYSxDQUFDLFVBQVUsQ0FBQztRQUN2RCxVQUFVLENBQUMsWUFBWSxDQUFDLEdBQUcsZUFBYSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzFELFVBQVUsQ0FBQyxZQUFZLENBQUMsR0FBRyxlQUFhLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDMUQsSUFBTSxHQUFHLEdBQUc7WUFDTixTQUFTLEVBQUUsT0FBTztZQUNsQixTQUFTLEVBQUUsU0FBUztZQUNwQixVQUFVLEVBQUUsVUFBVTtTQUN2QixDQUFDO1FBRU4sWUFBWTtRQUNaLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFDSCxVQUFVO0lBRVIsa0JBQWtCO0lBQ1gsaUNBQW1CLEdBQTFCO1FBQ0UsSUFBSSxTQUFTLEdBQUcsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzNDLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUNoQixlQUFhLENBQUMsb0JBQW9CLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFTSxtQ0FBcUIsR0FBNUI7UUFDRSxJQUFJLFNBQVMsR0FBRyxrQkFBa0IsR0FBRyxRQUFRLENBQUM7UUFDOUMsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLGVBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVNLHVDQUF5QixHQUFoQztRQUNFLElBQUksU0FBUyxHQUFHLGtCQUFrQixHQUFHLFlBQVksQ0FBQztRQUNsRCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDaEIsZUFBYSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRWMsa0NBQW9CLEdBQW5DLFVBQW9DLFNBQVMsRUFBRSxNQUFNO1FBQ25ELE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLGVBQWEsQ0FBQyxPQUFPLENBQUM7UUFDckQsTUFBTSxDQUFDLHNCQUFzQixDQUFDLEdBQUcsZUFBYSxDQUFDLFVBQVUsQ0FBQztRQUMxRCxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUVILFVBQVU7SUFHUjs7T0FFRztJQUNXLDRCQUFjLEdBQTVCO1FBQ0UsSUFBTSxXQUFXLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUUvQiwyQkFBMkI7UUFDM0IsSUFBTSxHQUFHLEdBQUcsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUQsSUFBTSxLQUFLLEdBQUcsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLHNCQUFzQjtRQUM5RixJQUFNLElBQUksR0FBRyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFbEQsNkJBQTZCO1FBQzdCLElBQU0sYUFBYSxHQUFHLEdBQUcsR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBRXpDLE9BQU8sYUFBYSxDQUFDO0lBQ3ZCLENBQUM7SUFDRDs7T0FFRztJQUNXLDRCQUFjLEdBQTVCO1FBQ0UsSUFBTSxXQUFXLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUUvQix3QkFBd0I7UUFDeEIsSUFBTSxLQUFLLEdBQUcsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDakUsSUFBTSxPQUFPLEdBQUcsV0FBVyxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFckUsNkJBQTZCO1FBQzdCLElBQU0sYUFBYSxHQUFHLEtBQUssR0FBRyxHQUFHLEdBQUcsT0FBTyxDQUFDO1FBRTVDLE9BQU8sYUFBYSxDQUFDO0lBQ3ZCLENBQUM7O0lBdklrQixhQUFhO1FBRGpDLE9BQU87T0FDYSxhQUFhLENBd0lqQztJQUFELG9CQUFDO0NBeElELEFBd0lDLElBQUE7a0JBeElvQixhQUFhIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbi8vU0VDVElPTiAtIEZJUkVCQVNFIGNvbnN0XG5jb25zdCBGSVJFQkFTRV9UUkFDS0lOR19FVkVOVCA9IFwiRklSRUJBU0VfVFJBQ0tJTkdfRVZFTlRcIjtcbmNvbnN0IEZJUkVCQVNFX0JFVF9FVkVOVCA9IFwiZ3Bfc2ljYm9fXCI7Ly9ncF9zaWNib197YWN0aW9uX25hbWV9XG5jb25zdCBGaXJlYmFzZVBhcmFtVmVyc2lvbiA9IFwiVmVyc2lvblwiO1xuY29uc3QgRmlyZWJhc2VQYXJhbVNlc3Npb25JZCA9IFwiU2Vzc2lvbklkXCI7XG4vLyFTRUNUSU9OXG5cblxuLy9TRUNUSU9OIC0gQVBQRkxZRVIgQ29uc3RcbmNvbnN0IEFQUFNfRkxZRVJfVFJBQ0tJTkdfRVZFTlQgPSBcIkFQUFNfRkxZRVJfVFJBQ0tJTkdfRVZFTlRcIlxuY29uc3QgQUZfRVZFTlRfQkVUID0gXCJhZl9wbGFjZWRfYmV0X3NpY2JvXCJcbmNvbnN0IEFGX0VWRU5UX1JFQkVUID0gXCJhZl9wbGFjZWRfYmV0X3NpY2JvXCJcbmNvbnN0IEFGX0VWRU5UX0RPVUJMRUJFVCA9IFwiYWZfcGxhY2VkX2JldF9zaWNib1wiXG5cbmNvbnN0IEFGX0NVUlJFTkNZID0gXCJWTkRcIlxuXG5jb25zdCBBRl9QYXJhbVByaWNlID0gXCJhZl9wcmljZVwiXG5jb25zdCBBRl9QYXJhbUN1cnJlbmN5ID0gXCJhZl9jdXJyZW5jeVwiXG5jb25zdCBBRl9QYXJhbVNlc3Npb24gPSBcImV2ZW50X251bWJlclwiXG5jb25zdCBBRl9QYXJhbURhdGUgPSBcImV2ZW50X2RhdGVcIlxuY29uc3QgQUZfUGFyYW1UaW1lID0gXCJldmVudF90aW1lXCJcbmNvbnN0IEFGX1BhcmFtVHJhbnNhY3Rpb25JZCA9IFwidHJhbnNhY3Rpb25faWRcIlxuLy8hU0VDVElPTlxuXG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib1RyYWNraW5nIHtcbiAgcHJpdmF0ZSBzdGF0aWMgdmVyc2lvbjogc3RyaW5nO1xuICBwcml2YXRlIHN0YXRpYyBzZXNzc2lvbklkOiBzdHJpbmc7XG5cbiAgc3RhdGljIHNldEdhbWVWZXJzaW9uKHZlcjogc3RyaW5nKSB7XG4gICAgU2ljYm9UcmFja2luZy52ZXJzaW9uID0gdmVyO1xuICB9XG5cbiAgc3RhdGljIHN0YXJ0U2Vzc2lvbihzZXNzc2lvbklkOiBzdHJpbmcpIHtcbiAgICBTaWNib1RyYWNraW5nLnNlc3NzaW9uSWQgPSBzZXNzc2lvbklkO1xuICB9XG5cbiAgXG4gIHN0YXRpYyB0cmFja2luZ0JldCh0cmFuc2FjdGlvbklkLCBhbW91bnQpIHtcbiAgICBTaWNib1RyYWNraW5nLmZpcmViYXNlVHJhY2tpbmdCZXQoKVxuICAgIFNpY2JvVHJhY2tpbmcuYXBwZmx5ZXJUcmFja2luZ0JldCh0cmFuc2FjdGlvbklkLCBhbW91bnQpXG4gIH1cblxuICBzdGF0aWMgdHJhY2tpbmdSZUJldCh0cmFuc2FjdGlvbklkLCBhbW91bnQpIHtcbiAgICBTaWNib1RyYWNraW5nLmZpcmViYXNlVHJhY2tpbmdSZUJldCgpXG4gICAgU2ljYm9UcmFja2luZy5hcHBmbHllclRyYWNraW5nUmVCZXQodHJhbnNhY3Rpb25JZCwgYW1vdW50KVxuICB9XG5cbiAgc3RhdGljIHRyYWNraW5nRG91YmxlQmV0KHRyYW5zYWN0aW9uSWQsIGFtb3VudCkge1xuICAgIFNpY2JvVHJhY2tpbmcuZmlyZWJhc2VUcmFja2luZ0RvdWJsZUJldCgpO1xuICAgIFNpY2JvVHJhY2tpbmcuYXBwZmx5ZXJUcmFja2luZ0RvdWJsZUJldCh0cmFuc2FjdGlvbklkLCBhbW91bnQpXG4gIH1cblxuICAvL1NFQ1RJT04gQXBwZmx5ZXJcbiAgc3RhdGljIGFwcGZseWVyVHJhY2tpbmdCZXQodHJhbnNhY3Rpb25JZCwgYW1vdW50KSB7XG4gICAgbGV0IGV2ZW50TmFtZSA9IEFGX0VWRU5UX0JFVDtcbiAgICBsZXQgZXZlbnRWYWx1ZSA9IHt9O1xuXG4gICAgZXZlbnRWYWx1ZVtBRl9QYXJhbVByaWNlXSA9IE1hdGguYWJzKGFtb3VudCk7XG4gICAgZXZlbnRWYWx1ZVtBRl9QYXJhbVRyYW5zYWN0aW9uSWRdID0gdHJhbnNhY3Rpb25JZDtcbiAgICBldmVudFZhbHVlW0FGX1BhcmFtQ3VycmVuY3ldID0gQUZfQ1VSUkVOQ1k7XG5cbiAgICBTaWNib1RyYWNraW5nLnNlbmRUcmFja2luZ0FwcGZseWVyKGV2ZW50TmFtZSwgZXZlbnRWYWx1ZSk7XG4gIH1cblxuICBzdGF0aWMgYXBwZmx5ZXJUcmFja2luZ1JlQmV0KHRyYW5zYWN0aW9uSWQsIGFtb3VudCkge1xuICAgIGxldCBldmVudE5hbWUgPSBBRl9FVkVOVF9SRUJFVDtcbiAgICBsZXQgZXZlbnRWYWx1ZSA9IHt9O1xuXG4gICAgZXZlbnRWYWx1ZVtBRl9QYXJhbVByaWNlXSA9IE1hdGguYWJzKGFtb3VudCk7XG4gICAgZXZlbnRWYWx1ZVtBRl9QYXJhbVRyYW5zYWN0aW9uSWRdID0gdHJhbnNhY3Rpb25JZDtcbiAgICBldmVudFZhbHVlW0FGX1BhcmFtQ3VycmVuY3ldID0gQUZfQ1VSUkVOQ1k7XG5cbiAgICBTaWNib1RyYWNraW5nLnNlbmRUcmFja2luZ0FwcGZseWVyKGV2ZW50TmFtZSwgZXZlbnRWYWx1ZSk7XG4gIH1cblxuICBzdGF0aWMgYXBwZmx5ZXJUcmFja2luZ0RvdWJsZUJldCh0cmFuc2FjdGlvbklkLCBhbW91bnQpIHtcbiAgICBsZXQgZXZlbnROYW1lID0gQUZfRVZFTlRfRE9VQkxFQkVUO1xuICAgIGxldCBldmVudFZhbHVlID0ge307XG5cbiAgICBldmVudFZhbHVlW0FGX1BhcmFtUHJpY2VdID0gTWF0aC5hYnMoYW1vdW50KTtcbiAgICBldmVudFZhbHVlW0FGX1BhcmFtVHJhbnNhY3Rpb25JZF0gPSB0cmFuc2FjdGlvbklkO1xuICAgIGV2ZW50VmFsdWVbQUZfUGFyYW1DdXJyZW5jeV0gPSBBRl9DVVJSRU5DWTtcblxuICAgIFNpY2JvVHJhY2tpbmcuc2VuZFRyYWNraW5nQXBwZmx5ZXIoZXZlbnROYW1lLCBldmVudFZhbHVlKTtcbiAgfVxuXG4gIHByaXZhdGUgc3RhdGljIHNlbmRUcmFja2luZ0FwcGZseWVyKGV2ZW50TmFtZSwgZXZlbnRWYWx1ZSkge1xuICAgIGV2ZW50VmFsdWVbQUZfUGFyYW1TZXNzaW9uXSA9IFNpY2JvVHJhY2tpbmcuc2Vzc3Npb25JZDtcbiAgICBldmVudFZhbHVlW0FGX1BhcmFtRGF0ZV0gPSBTaWNib1RyYWNraW5nLmdldEN1cnJlbnREYXRlKCk7XG4gICAgZXZlbnRWYWx1ZVtBRl9QYXJhbVRpbWVdID0gU2ljYm9UcmFja2luZy5nZXRDdXJyZW50VGltZSgpO1xuICAgIGNvbnN0IGV2dCA9IHtcbiAgICAgICAgICBldmVudFR5cGU6IFwiRVZFTlRcIixcbiAgICAgICAgICBldmVudE5hbWU6IGV2ZW50TmFtZSxcbiAgICAgICAgICBldmVudFZhbHVlOiBldmVudFZhbHVlLFxuICAgICAgICB9O1xuICAgICAgXG4gICAgLy9zZW5kIGV2ZW50XG4gICAgY2Muc3lzdGVtRXZlbnQuZW1pdChBUFBTX0ZMWUVSX1RSQUNLSU5HX0VWRU5ULCBldnQpO1xuICB9XG4vLyFTRUNUSU9OXG5cbiAgLy9TRUNUSU9OIEZpcmViYXNlXG4gIHN0YXRpYyBmaXJlYmFzZVRyYWNraW5nQmV0KCkge1xuICAgIGxldCBldmVudE5hbWUgPSBGSVJFQkFTRV9CRVRfRVZFTlQgKyBcImJldFwiO1xuICAgIGxldCBwYXJhbXMgPSB7fTtcbiAgICBTaWNib1RyYWNraW5nLnNlbmRUcmFja2luZ0ZpcmViYXNlKGV2ZW50TmFtZSwgcGFyYW1zKTtcbiAgfVxuXG4gIHN0YXRpYyBmaXJlYmFzZVRyYWNraW5nUmVCZXQoKSB7XG4gICAgbGV0IGV2ZW50TmFtZSA9IEZJUkVCQVNFX0JFVF9FVkVOVCArIFwicmVfYmV0XCI7XG4gICAgbGV0IHBhcmFtcyA9IHt9O1xuICAgIFNpY2JvVHJhY2tpbmcuc2VuZFRyYWNraW5nRmlyZWJhc2UoZXZlbnROYW1lLCBwYXJhbXMpO1xuICB9XG5cbiAgc3RhdGljIGZpcmViYXNlVHJhY2tpbmdEb3VibGVCZXQoKSB7XG4gICAgbGV0IGV2ZW50TmFtZSA9IEZJUkVCQVNFX0JFVF9FVkVOVCArIFwiZG91YmxlX2JldFwiO1xuICAgIGxldCBwYXJhbXMgPSB7fTtcbiAgICBTaWNib1RyYWNraW5nLnNlbmRUcmFja2luZ0ZpcmViYXNlKGV2ZW50TmFtZSwgcGFyYW1zKTtcbiAgfVxuXG4gIHByaXZhdGUgc3RhdGljIHNlbmRUcmFja2luZ0ZpcmViYXNlKGV2ZW50TmFtZSwgcGFyYW1zKSB7XG4gICAgcGFyYW1zW0ZpcmViYXNlUGFyYW1WZXJzaW9uXSA9IFNpY2JvVHJhY2tpbmcudmVyc2lvbjtcbiAgICBwYXJhbXNbRmlyZWJhc2VQYXJhbVNlc3Npb25JZF0gPSBTaWNib1RyYWNraW5nLnNlc3NzaW9uSWQ7XG4gICAgY2Muc3lzdGVtRXZlbnQuZW1pdChGSVJFQkFTRV9UUkFDS0lOR19FVkVOVCwgZXZlbnROYW1lLCBwYXJhbXMpO1xuICB9XG4gIFxuLy8hU0VDVElPTlxuXG5cbiAgLyoqXG4gICAqIGdldCBjdXJyZW50IGRhdGUgYXMgRERNTVlZWVlcbiAgICovXG4gIHB1YmxpYyBzdGF0aWMgZ2V0Q3VycmVudERhdGUoKTogc3RyaW5nIHtcbiAgICBjb25zdCBjdXJyZW50RGF0ZSA9IG5ldyBEYXRlKCk7XG5cbiAgICAvLyBHZXQgZGF5LCBtb250aCwgYW5kIHllYXJcbiAgICBjb25zdCBkYXkgPSBjdXJyZW50RGF0ZS5nZXREYXRlKCkudG9TdHJpbmcoKS5wYWRTdGFydCgyLCBcIjBcIik7XG4gICAgY29uc3QgbW9udGggPSAoY3VycmVudERhdGUuZ2V0TW9udGgoKSArIDEpLnRvU3RyaW5nKCkucGFkU3RhcnQoMiwgXCIwXCIpOyAvLyBNb250aCBpcyB6ZXJvLWJhc2VkXG4gICAgY29uc3QgeWVhciA9IGN1cnJlbnREYXRlLmdldEZ1bGxZZWFyKCkudG9TdHJpbmcoKTtcblxuICAgIC8vIENvbmNhdGVuYXRlIHRoZSBkYXRlIHBhcnRzXG4gICAgY29uc3QgZm9ybWF0dGVkRGF0ZSA9IGRheSArIG1vbnRoICsgeWVhcjtcblxuICAgIHJldHVybiBmb3JtYXR0ZWREYXRlO1xuICB9XG4gIC8qKlxuICAgKiBnZXQgY3VycmVudCB0aW1lIGFzIEhIOk1NXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGdldEN1cnJlbnRUaW1lKCk6IHN0cmluZyB7XG4gICAgY29uc3QgY3VycmVudERhdGUgPSBuZXcgRGF0ZSgpO1xuXG4gICAgLy8gR2V0IGhvdXJzIGFuZCBtaW51dGVzXG4gICAgY29uc3QgaG91cnMgPSBjdXJyZW50RGF0ZS5nZXRIb3VycygpLnRvU3RyaW5nKCkucGFkU3RhcnQoMiwgXCIwXCIpO1xuICAgIGNvbnN0IG1pbnV0ZXMgPSBjdXJyZW50RGF0ZS5nZXRNaW51dGVzKCkudG9TdHJpbmcoKS5wYWRTdGFydCgyLCBcIjBcIik7XG5cbiAgICAvLyBDb25jYXRlbmF0ZSB0aGUgdGltZSBwYXJ0c1xuICAgIGNvbnN0IGZvcm1hdHRlZFRpbWUgPSBob3VycyArIFwiOlwiICsgbWludXRlcztcblxuICAgIHJldHVybiBmb3JtYXR0ZWRUaW1lO1xuICB9XG59XG4iXX0=