
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Sicbo/Scripts/SicboMessageHandler.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'dacc33J/ZdEZpZw5bNV0ZB+', 'SicboMessageHandler');
// Game/Sicbo_New/Scripts/SicboMessageHandler.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Code = _a.Code, Empty = _a.Empty, BenTauMessage = _a.BenTauMessage, Topic = _a.Topic, SicboClient = _a.SicboClient, BetDoubleReply = _a.BetDoubleReply, BetDoubleRequest = _a.BetDoubleRequest, BetReply = _a.BetReply, BetRequest = _a.BetRequest, GetOthersReply = _a.GetOthersReply, GetOthersRequest = _a.GetOthersRequest, LeaveRequest = _a.LeaveRequest, Message = _a.Message, ReBetReply = _a.ReBetReply, ReBetRequest = _a.ReBetRequest, State = _a.State, HandlerClientBenTauBase = _a.HandlerClientBenTauBase, AutoJoinReply = _a.AutoJoinReply, LeaveReply = _a.LeaveReply, HealthCheckRequest = _a.HealthCheckRequest, HealthCheckReply = _a.HealthCheckReply, EventKey = _a.EventKey;
var SicboPortalAdapter_1 = require("./SicboPortalAdapter");
var SicboSetting_1 = require("./Setting/SicboSetting");
var SicboText_1 = require("./Setting/SicboText");
var SicboUIManager_1 = require("./Managers/SicboUIManager");
var SicboCannotJoinPopup_1 = require("./PopUps/SicboCannotJoinPopup");
var SicboTracking_1 = require("./SicboTracking");
var SicboMessageHandler = /** @class */ (function (_super) {
    __extends(SicboMessageHandler, _super);
    function SicboMessageHandler() {
        //#region  Instance
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.client = null;
        _this.receiveState = null;
        _this.onErrorCustom = null;
        _this.curChannelId = "sicbov2";
        return _this;
    }
    SicboMessageHandler.getInstance = function () {
        if (!SicboMessageHandler.instance) {
            SicboMessageHandler.instance = new SicboMessageHandler(SicboPortalAdapter_1.default.getBenTauHandler());
            SicboMessageHandler.instance.curChannelId = "sicbov2";
            SicboMessageHandler.instance.client = new SicboClient(SicboPortalAdapter_1.default.addressBenTau(), {}, null);
            SicboMessageHandler.instance.topic = Topic.SICBO;
            SicboMessageHandler.instance.onSubscribe();
        }
        return SicboMessageHandler.instance;
    };
    SicboMessageHandler.destroy = function () {
        if (SicboMessageHandler.instance) {
            SicboMessageHandler.instance.onUnSubscribe();
            SicboMessageHandler.instance.onClearAllMsg();
        }
        SicboMessageHandler.instance = null;
    };
    Object.defineProperty(SicboMessageHandler.prototype, "metaData", {
        get: function () {
            return {
                Authorization: "Bearer " + SicboPortalAdapter_1.default.getToken(),
            };
        },
        enumerable: false,
        configurable: true
    });
    SicboMessageHandler.prototype.showLoading = function (callback) {
        SicboPortalAdapter_1.default.getInstance().showHandlerLoading(callback);
    };
    SicboMessageHandler.prototype.hideLoading = function () {
        SicboPortalAdapter_1.default.getInstance().hideHandlerLoading();
    };
    SicboMessageHandler.prototype.onShowErr = function (str, onClick) {
        //cc.log("SICBO onShowErr", str);
        if (cc.director.getScene().name == SicboSetting_1.SicboScene.Loading) {
            SicboUIManager_1.default.getInstance().openPopup(SicboCannotJoinPopup_1.default, SicboUIManager_1.default.SicboCannotJoinPopup, null);
        }
        else {
            SicboPortalAdapter_1.default.getInstance().onShowHandlerErr(SicboText_1.default.handlerDefaultErrMsg, onClick);
        }
    };
    SicboMessageHandler.prototype.onDisconnect = function (CodeSocket, messError) {
        SicboPortalAdapter_1.default.getInstance().onHandlerDisconnect(CodeSocket, messError);
    };
    SicboMessageHandler.prototype.getChannelId = function () {
        return this.curChannelId;
    };
    SicboMessageHandler.prototype.closeHandler = function () {
        if (SicboMessageHandler.instance)
            SicboMessageHandler.instance.onUnSubscribe();
        delete SicboMessageHandler.instance;
        SicboMessageHandler.instance = null;
    };
    SicboMessageHandler.prototype.onDestroy = function () {
        this.closeHandler();
    };
    //#endregion
    //SECTION implement BaseHandler
    SicboMessageHandler.prototype.onReceive = function (msg) {
        _super.prototype.onReceive.call(this, msg);
        var message = Message.deserializeBinary(msg.getPayload_asU8());
        switch (message.getPayloadCase()) {
            case Message.PayloadCase.STATE:
                var state = message.getState();
                if (this.receiveState)
                    this.receiveState(state);
                break;
            default:
                break;
        }
    };
    //#endregion
    SicboMessageHandler.prototype.handleCustomError = function (errCode, err) {
        if (this.isServerDieCode(errCode)) {
            SicboUIManager_1.default.getInstance().openPopup(SicboCannotJoinPopup_1.default, SicboUIManager_1.default.SicboCannotJoinPopup, null);
        }
        else if (this.onErrorCustom)
            this.onErrorCustom(errCode, err);
    };
    SicboMessageHandler.prototype.isServerDieCode = function (errCode) {
        if (SicboMessageHandler.IS_WS_DISCONNECTED || (!window.navigator || !window.navigator.onLine))
            return false;
        return (errCode == Code.SICBO_CANNOT_JOIN
            || errCode == Code.SICBO_REQUEST_TIMEOUT
            || errCode == Code.UNAVAILABLE
            || errCode == Code.SICBO_SESSION_NOT_FOUND
            || errCode == Code.SICBO_ACTOR_NOT_FOUND
            || errCode == Code.ABORTED
            || errCode == Code.UNAUTHENTICATED
            || errCode == Code.UNKNOWN);
    };
    //!SECTION
    //ANCHOR Bet
    SicboMessageHandler.prototype.isNotEnoughMoney = function (errCode) {
        return (errCode == Code.SICBO_BET_NOT_ENOUGH_MONEY
            || errCode == Code.SICBO_REBET_NOT_ENOUGH_MONEY
            || errCode == Code.SICBO_BET_DOUBLE_NOT_ENOUGH_MONEY);
    };
    SicboMessageHandler.prototype.sendBetRequest = function (door, chip, response) {
        if (response === void 0) { response = null; }
        var self = this;
        var request = new BetRequest();
        request.setDoor(door);
        request.setChip(chip);
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.bet(request, self.metaData, function (err, res) {
                var _a, _b;
                self.onSendReply(err, res, msgId);
                //cc.log("SICBO BetReply", res?.getTx());
                SicboTracking_1.default.trackingBet((_a = res === null || res === void 0 ? void 0 : res.getTx()) === null || _a === void 0 ? void 0 : _a.getLastTxId(), (_b = res === null || res === void 0 ? void 0 : res.getTx()) === null || _b === void 0 ? void 0 : _b.getAmount());
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            if (self.isNotEnoughMoney(code)) {
                cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, SicboMessageHandler.instance.topic, chip);
            }
            else {
                self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
            }
            // self?.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR Rebet
    SicboMessageHandler.prototype.sendRebetRequest = function (response) {
        if (response === void 0) { response = null; }
        var self = this;
        var request = new ReBetRequest();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.rebet(request, self.metaData, function (err, res) {
                var _a, _b;
                self.onSendReply(err, res, msgId);
                //cc.log("SICBO ReBetReply", res?.getTx());
                SicboTracking_1.default.trackingReBet((_a = res === null || res === void 0 ? void 0 : res.getTx()) === null || _a === void 0 ? void 0 : _a.getLastTxId(), (_b = res === null || res === void 0 ? void 0 : res.getTx()) === null || _b === void 0 ? void 0 : _b.getAmount());
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            if (self.isNotEnoughMoney(code)) {
                cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, SicboMessageHandler.instance.topic, 0);
            }
            else {
                self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
            }
            // self?.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR Double bet
    SicboMessageHandler.prototype.sendBetDoubleRequest = function (response) {
        if (response === void 0) { response = null; }
        var self = this;
        var request = new BetDoubleRequest();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.betDouble(request, self.metaData, function (err, res) {
                var _a, _b;
                self.onSendReply(err, res, msgId);
                //cc.log("SICBO BetDoubleReply", res?.getTx());
                SicboTracking_1.default.trackingDoubleBet((_a = res === null || res === void 0 ? void 0 : res.getTx()) === null || _a === void 0 ? void 0 : _a.getLastTxId(), (_b = res === null || res === void 0 ? void 0 : res.getTx()) === null || _b === void 0 ? void 0 : _b.getAmount());
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            if (self.isNotEnoughMoney(code)) {
                cc.systemEvent.emit(EventKey.HANDLE_OUT_OF_BALANCE_WHEN_BETTING_GAME, SicboMessageHandler.instance.topic, 0);
            }
            else {
                self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
            }
            // self?.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR AutoJoin
    SicboMessageHandler.prototype.sendAutoJoinRequest = function (response) {
        var self = this;
        var request = new Empty();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.autoJoin(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR Leave
    SicboMessageHandler.prototype.sendLeaveRequest = function (response) {
        var self = this;
        var request = new LeaveRequest();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.leave(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR get Others
    SicboMessageHandler.prototype.sendGetOthersRequest = function (from, limit, response) {
        var self = this;
        var request = new GetOthersRequest();
        request.setCursor(from);
        request.setLimit(limit);
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.getOthers(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
        }, false);
    };
    //ANCHOR - Health check
    SicboMessageHandler.prototype.sendHealthCheck = function (response) {
        var self = this;
        var request = new HealthCheckRequest();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.healthCheck(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, function (err, code) {
            self === null || self === void 0 ? void 0 : self.handleCustomError(code, err);
        }, false);
    };
    SicboMessageHandler.IS_WS_DISCONNECTED = false;
    SicboMessageHandler.benTauHandler = null;
    return SicboMessageHandler;
}(HandlerClientBenTauBase));
exports.default = SicboMessageHandler;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL1NpY2JvX05ldy9TY3JpcHRzL1NpY2JvTWVzc2FnZUhhbmRsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsa0VBQTZEO0FBRXZELElBQUEsS0FxQkYsNEJBQWtCLENBQUMsVUFBVSxFQUFFLEVBcEJqQyxJQUFJLFVBQUEsRUFDSixLQUFLLFdBQUEsRUFDTCxhQUFhLG1CQUFBLEVBQ2IsS0FBSyxXQUFBLEVBQ0wsV0FBVyxpQkFBQSxFQUNYLGNBQWMsb0JBQUEsRUFDZCxnQkFBZ0Isc0JBQUEsRUFDaEIsUUFBUSxjQUFBLEVBQ1IsVUFBVSxnQkFBQSxFQUNWLGNBQWMsb0JBQUEsRUFDZCxnQkFBZ0Isc0JBQUEsRUFDaEIsWUFBWSxrQkFBQSxFQUNaLE9BQU8sYUFBQSxFQUNQLFVBQVUsZ0JBQUEsRUFDVixZQUFZLGtCQUFBLEVBQ1osS0FBSyxXQUFBLEVBQ0wsdUJBQXVCLDZCQUFBLEVBQ3ZCLGFBQWEsbUJBQUEsRUFBRSxVQUFVLGdCQUFBLEVBQ3pCLGtCQUFrQix3QkFBQSxFQUFFLGdCQUFnQixzQkFBQSxFQUNwQyxRQUFRLGNBQ3lCLENBQUM7QUFHcEMsMkRBQXNEO0FBQ3RELHVEQUFrRTtBQUNsRSxpREFBNEM7QUFDNUMsNERBQXVEO0FBQ3ZELHNFQUFpRTtBQUNqRSxpREFBNEM7QUFDNUM7SUFBaUQsdUNBQXVCO0lBQXhFO1FBQ0UsbUJBQW1CO1FBRHJCLHFFQXFRQztRQWpRUyxZQUFNLEdBQXFDLElBQUksQ0FBQztRQUdqRCxrQkFBWSxHQUFnRCxJQUFJLENBQUM7UUFDakUsbUJBQWEsR0FBbUMsSUFBSSxDQUFDO1FBc0JwRCxrQkFBWSxHQUFXLFNBQVMsQ0FBQzs7SUF1TzNDLENBQUM7SUExUFEsK0JBQVcsR0FBbEI7UUFDRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFO1lBQ2pDLG1CQUFtQixDQUFDLFFBQVEsR0FBRyxJQUFJLG1CQUFtQixDQUFDLDRCQUFrQixDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztZQUM5RixtQkFBbUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQztZQUN0RCxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksV0FBVyxDQUFDLDRCQUFrQixDQUFDLGFBQWEsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNwRyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDakQsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzVDO1FBQ0QsT0FBTyxtQkFBbUIsQ0FBQyxRQUFRLENBQUM7SUFDdEMsQ0FBQztJQUVNLDJCQUFPLEdBQWQ7UUFDRSxJQUFJLG1CQUFtQixDQUFDLFFBQVEsRUFBRTtZQUNoQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDN0MsbUJBQW1CLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQzlDO1FBQ0QsbUJBQW1CLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN0QyxDQUFDO0lBSUQsc0JBQVkseUNBQVE7YUFBcEI7WUFDRSxPQUFPO2dCQUNMLGFBQWEsRUFBRSxTQUFTLEdBQUcsNEJBQWtCLENBQUMsUUFBUSxFQUFFO2FBQ3pELENBQUM7UUFDSixDQUFDOzs7T0FBQTtJQUVTLHlDQUFXLEdBQXJCLFVBQXNCLFFBQWtCO1FBQ3RDLDRCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFUyx5Q0FBVyxHQUFyQjtRQUNFLDRCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDeEQsQ0FBQztJQUVTLHVDQUFTLEdBQW5CLFVBQW9CLEdBQVcsRUFBRSxPQUFvQjtRQUNuRCxpQ0FBaUM7UUFDakMsSUFBRyxFQUFFLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLElBQUksSUFBSSx5QkFBVSxDQUFDLE9BQU8sRUFBQztZQUNuRCx3QkFBYyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyw4QkFBb0IsRUFBRSx3QkFBYyxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3pHO2FBQUk7WUFDSCw0QkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBUyxDQUFDLG9CQUFvQixFQUFFLE9BQU8sQ0FBQyxDQUFDO1NBQzVGO0lBQ0gsQ0FBQztJQUVTLDBDQUFZLEdBQXRCLFVBQXVCLFVBQVUsRUFBRSxTQUFpQjtRQUNsRCw0QkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDOUUsQ0FBQztJQUVELDBDQUFZLEdBQVo7UUFDRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDM0IsQ0FBQztJQUVELDBDQUFZLEdBQVo7UUFDRSxJQUFJLG1CQUFtQixDQUFDLFFBQVE7WUFBRSxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDL0UsT0FBTyxtQkFBbUIsQ0FBQyxRQUFRLENBQUM7UUFDcEMsbUJBQW1CLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN0QyxDQUFDO0lBRUQsdUNBQVMsR0FBVDtRQUNFLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBQ0QsWUFBWTtJQUNaLCtCQUErQjtJQUMvQix1Q0FBUyxHQUFULFVBQVUsR0FBdUM7UUFDL0MsaUJBQU0sU0FBUyxZQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JCLElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztRQUMvRCxRQUFRLE9BQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRTtZQUNoQyxLQUFLLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSztnQkFDNUIsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUMvQixJQUFJLElBQUksQ0FBQyxZQUFZO29CQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2hELE1BQU07WUFDUjtnQkFDRSxNQUFNO1NBQ1Q7SUFDSCxDQUFDO0lBRUQsWUFBWTtJQUNaLCtDQUFpQixHQUFqQixVQUFrQixPQUFPLEVBQUUsR0FBVztRQUNwQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLEVBQUM7WUFDOUIsd0JBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsOEJBQW9CLEVBQUUsd0JBQWMsQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUMzRzthQUNHLElBQUksSUFBSSxDQUFDLGFBQWE7WUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRVMsNkNBQWUsR0FBdkIsVUFBd0IsT0FBTztRQUM3QixJQUFHLG1CQUFtQixDQUFDLGtCQUFrQixJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUN6RyxPQUFPLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxpQkFBaUI7ZUFDbEMsT0FBTyxJQUFJLElBQUksQ0FBQyxxQkFBcUI7ZUFDckMsT0FBTyxJQUFJLElBQUksQ0FBQyxXQUFXO2VBQzNCLE9BQU8sSUFBSSxJQUFJLENBQUMsdUJBQXVCO2VBQ3ZDLE9BQU8sSUFBSSxJQUFJLENBQUMscUJBQXFCO2VBQ3JDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTztlQUN2QixPQUFPLElBQUksSUFBSSxDQUFDLGVBQWU7ZUFDL0IsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQTtJQUNuQyxDQUFDO0lBRUQsVUFBVTtJQUVWLFlBQVk7SUFDSiw4Q0FBZ0IsR0FBeEIsVUFBeUIsT0FBTztRQUM5QixPQUFPLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQywwQkFBMEI7ZUFDM0MsT0FBTyxJQUFJLElBQUksQ0FBQyw0QkFBNEI7ZUFDNUMsT0FBTyxJQUFJLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFBO0lBQzNELENBQUM7SUFFRCw0Q0FBYyxHQUFkLFVBQWUsSUFBSSxFQUFFLElBQVksRUFBRSxRQUFrRTtRQUFsRSx5QkFBQSxFQUFBLGVBQWtFO1FBQ25HLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUVoQixJQUFJLE9BQU8sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEIsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV0QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDL0IsSUFBSSxXQUFXLEdBQUc7WUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBQyxHQUFHLEVBQUUsR0FBRzs7Z0JBQy9DLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDbEMseUNBQXlDO2dCQUN6Qyx1QkFBYSxDQUFDLFdBQVcsT0FBQyxHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsS0FBSyw0Q0FBSSxXQUFXLFVBQUksR0FBRyxhQUFILEdBQUcsdUJBQUgsR0FBRyxDQUFFLEtBQUssNENBQUksU0FBUyxHQUFHLENBQUM7WUFDcEYsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFVBQUMsR0FBRyxFQUFFLElBQUk7WUFDekQsSUFBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQUM7Z0JBQzdCLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyx1Q0FBdUMsRUFBRSxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ2pIO2lCQUFJO2dCQUNELElBQUksYUFBSixJQUFJLHVCQUFKLElBQUksQ0FBRSxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFO2FBQ3RDO1lBQ0Qsc0NBQXNDO1FBQ3hDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCxjQUFjO0lBQ2QsOENBQWdCLEdBQWhCLFVBQWlCLFFBQW9FO1FBQXBFLHlCQUFBLEVBQUEsZUFBb0U7UUFDbkYsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksT0FBTyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDakMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRS9CLElBQUksV0FBVyxHQUFHO1lBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7O2dCQUNqRCxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ2xDLDJDQUEyQztnQkFDM0MsdUJBQWEsQ0FBQyxhQUFhLE9BQUMsR0FBRyxhQUFILEdBQUcsdUJBQUgsR0FBRyxDQUFFLEtBQUssNENBQUksV0FBVyxVQUFJLEdBQUcsYUFBSCxHQUFHLHVCQUFILEdBQUcsQ0FBRSxLQUFLLDRDQUFJLFNBQVMsR0FBRyxDQUFDO1lBRXRGLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRUYsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxVQUFDLEdBQUcsRUFBRSxJQUFJO1lBQ3pELElBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUFDO2dCQUM3QixFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsdUNBQXVDLEVBQUUsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQzthQUM5RztpQkFBSTtnQkFDRCxJQUFJLGFBQUosSUFBSSx1QkFBSixJQUFJLENBQUUsaUJBQWlCLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRTthQUN0QztZQUNELHNDQUFzQztRQUN4QyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDWixDQUFDO0lBRUQsbUJBQW1CO0lBQ25CLGtEQUFvQixHQUFwQixVQUFxQixRQUF3RTtRQUF4RSx5QkFBQSxFQUFBLGVBQXdFO1FBQzNGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLE9BQU8sR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUM7UUFDckMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRS9CLElBQUksV0FBVyxHQUFHO1lBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7O2dCQUNyRCxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ2xDLCtDQUErQztnQkFDL0MsdUJBQWEsQ0FBQyxpQkFBaUIsT0FBQyxHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsS0FBSyw0Q0FBSSxXQUFXLFVBQUksR0FBRyxhQUFILEdBQUcsdUJBQUgsR0FBRyxDQUFFLEtBQUssNENBQUksU0FBUyxHQUFHLENBQUM7WUFFMUYsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFVBQUMsR0FBRyxFQUFFLElBQUk7WUFDekQsSUFBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQUM7Z0JBQzdCLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyx1Q0FBdUMsRUFBRSxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQzlHO2lCQUFJO2dCQUNELElBQUksYUFBSixJQUFJLHVCQUFKLElBQUksQ0FBRSxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFO2FBQ3RDO1lBQ0Qsc0NBQXNDO1FBQ3hDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCxpQkFBaUI7SUFDakIsaURBQW1CLEdBQW5CLFVBQW9CLFFBQWdFO1FBQ2xGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLE9BQU8sR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDO1FBRTFCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMvQixJQUFJLFdBQVcsR0FBRztZQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFHO2dCQUNwRCxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFVBQUMsR0FBRyxFQUFFLElBQUk7WUFDekQsSUFBSSxhQUFKLElBQUksdUJBQUosSUFBSSxDQUFFLGlCQUFpQixDQUFDLElBQUksRUFBRSxHQUFHLEVBQUU7UUFDckMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ1osQ0FBQztJQUNELGNBQWM7SUFDZCw4Q0FBZ0IsR0FBaEIsVUFBaUIsUUFBNkQ7UUFDNUUsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksT0FBTyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFFakMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQy9CLElBQUksV0FBVyxHQUFHO1lBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7Z0JBQ2pELElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNwQyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVGLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsVUFBQyxHQUFHLEVBQUUsSUFBSTtZQUN6RCxJQUFJLGFBQUosSUFBSSx1QkFBSixJQUFJLENBQUUsaUJBQWlCLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRTtRQUNyQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDWixDQUFDO0lBRUQsbUJBQW1CO0lBQ25CLGtEQUFvQixHQUFwQixVQUFxQixJQUFZLEVBQUUsS0FBYSxFQUFFLFFBQWlFO1FBQ2pILElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLE9BQU8sR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUM7UUFDckMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QixPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXhCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMvQixJQUFJLFdBQVcsR0FBRztZQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFHO2dCQUNyRCxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFVBQUMsR0FBRyxFQUFFLElBQUk7WUFDekQsSUFBSSxhQUFKLElBQUksdUJBQUosSUFBSSxDQUFFLGlCQUFpQixDQUFDLElBQUksRUFBRSxHQUFHLEVBQUU7UUFDckMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ1osQ0FBQztJQUVDLHVCQUF1QjtJQUN2Qiw2Q0FBZSxHQUFmLFVBQWdCLFFBQW1FO1FBQ2pGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQTtRQUNmLElBQUksT0FBTyxHQUFHLElBQUksa0JBQWtCLEVBQUUsQ0FBQztRQUV2QyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDL0IsSUFBSSxXQUFXLEdBQUc7WUFDZCxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFHO2dCQUNyRCxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDdEMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFHLFVBQUMsR0FBRyxFQUFFLElBQUk7WUFDeEQsSUFBSSxhQUFKLElBQUksdUJBQUosSUFBSSxDQUFFLGlCQUFpQixDQUFDLElBQUksRUFBRSxHQUFHLEVBQUU7UUFDdkMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2QsQ0FBQztJQTlQYSxzQ0FBa0IsR0FBWSxLQUFLLENBQUM7SUFJcEMsaUNBQWEsR0FBRyxJQUFJLENBQUM7SUE0UHJDLDBCQUFDO0NBclFELEFBcVFDLENBclFnRCx1QkFBdUIsR0FxUXZFO2tCQXJRb0IsbUJBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFNpY2JvTW9kdWxlQWRhcHRlciBmcm9tIFwiLi4vLi4vLi4vU2ljYm9Nb2R1bGVBZGFwdGVyXCI7XG5cbmNvbnN0IHtcbiAgQ29kZSxcbiAgRW1wdHksXG4gIEJlblRhdU1lc3NhZ2UsXG4gIFRvcGljLFxuICBTaWNib0NsaWVudCxcbiAgQmV0RG91YmxlUmVwbHksXG4gIEJldERvdWJsZVJlcXVlc3QsXG4gIEJldFJlcGx5LFxuICBCZXRSZXF1ZXN0LFxuICBHZXRPdGhlcnNSZXBseSxcbiAgR2V0T3RoZXJzUmVxdWVzdCxcbiAgTGVhdmVSZXF1ZXN0LFxuICBNZXNzYWdlLFxuICBSZUJldFJlcGx5LFxuICBSZUJldFJlcXVlc3QsXG4gIFN0YXRlLFxuICBIYW5kbGVyQ2xpZW50QmVuVGF1QmFzZSxcbiAgQXV0b0pvaW5SZXBseSwgTGVhdmVSZXBseSxcbiAgSGVhbHRoQ2hlY2tSZXF1ZXN0LCBIZWFsdGhDaGVja1JlcGx5LFxuICBFdmVudEtleVxufSA9IFNpY2JvTW9kdWxlQWRhcHRlci5nZXRBbGxSZWZzKCk7XG5cblxuaW1wb3J0IFNpY2JvUG9ydGFsQWRhcHRlciBmcm9tICcuL1NpY2JvUG9ydGFsQWRhcHRlcic7XG5pbXBvcnQgU2ljYm9TZXR0aW5nLCB7IFNpY2JvU2NlbmUgfSBmcm9tICcuL1NldHRpbmcvU2ljYm9TZXR0aW5nJztcbmltcG9ydCBTaWNib1RleHQgZnJvbSAnLi9TZXR0aW5nL1NpY2JvVGV4dCc7XG5pbXBvcnQgU2ljYm9VSU1hbmFnZXIgZnJvbSBcIi4vTWFuYWdlcnMvU2ljYm9VSU1hbmFnZXJcIjtcbmltcG9ydCBTaWNib0Nhbm5vdEpvaW5Qb3B1cCBmcm9tIFwiLi9Qb3BVcHMvU2ljYm9DYW5ub3RKb2luUG9wdXBcIjtcbmltcG9ydCBTaWNib1RyYWNraW5nIGZyb20gXCIuL1NpY2JvVHJhY2tpbmdcIjtcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNpY2JvTWVzc2FnZUhhbmRsZXIgZXh0ZW5kcyBIYW5kbGVyQ2xpZW50QmVuVGF1QmFzZSB7XG4gIC8vI3JlZ2lvbiAgSW5zdGFuY2VcblxuICBwcm90ZWN0ZWQgc3RhdGljIGluc3RhbmNlOiBTaWNib01lc3NhZ2VIYW5kbGVyO1xuICBwcml2YXRlIGNsaWVudDogSW5zdGFuY2VUeXBlPHR5cGVvZiBTaWNib0NsaWVudD4gPSBudWxsO1xuICBwdWJsaWMgc3RhdGljIElTX1dTX0RJU0NPTk5FQ1RFRDogYm9vbGVhbiA9IGZhbHNlOyAgXG5cbiAgcHVibGljIHJlY2VpdmVTdGF0ZTogKHN0YXRlOiBJbnN0YW5jZVR5cGU8dHlwZW9mIFN0YXRlPikgPT4gdm9pZCA9IG51bGw7XG4gIHB1YmxpYyBvbkVycm9yQ3VzdG9tOiAoZXJyQ29kZSwgZXJyOiBzdHJpbmcpID0+IHZvaWQgPSBudWxsO1xuICBwdWJsaWMgc3RhdGljIGJlblRhdUhhbmRsZXIgPSBudWxsO1xuXG4gIHN0YXRpYyBnZXRJbnN0YW5jZSgpOiBTaWNib01lc3NhZ2VIYW5kbGVyIHtcbiAgICBpZiAoIVNpY2JvTWVzc2FnZUhhbmRsZXIuaW5zdGFuY2UpIHtcbiAgICAgIFNpY2JvTWVzc2FnZUhhbmRsZXIuaW5zdGFuY2UgPSBuZXcgU2ljYm9NZXNzYWdlSGFuZGxlcihTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0QmVuVGF1SGFuZGxlcigpKTtcbiAgICAgIFNpY2JvTWVzc2FnZUhhbmRsZXIuaW5zdGFuY2UuY3VyQ2hhbm5lbElkID0gXCJzaWNib3YyXCI7XG4gICAgICBTaWNib01lc3NhZ2VIYW5kbGVyLmluc3RhbmNlLmNsaWVudCA9IG5ldyBTaWNib0NsaWVudChTaWNib1BvcnRhbEFkYXB0ZXIuYWRkcmVzc0JlblRhdSgpLCB7fSwgbnVsbCk7XG4gICAgICBTaWNib01lc3NhZ2VIYW5kbGVyLmluc3RhbmNlLnRvcGljID0gVG9waWMuU0lDQk87XG4gICAgICBTaWNib01lc3NhZ2VIYW5kbGVyLmluc3RhbmNlLm9uU3Vic2NyaWJlKCk7XG4gICAgfVxuICAgIHJldHVybiBTaWNib01lc3NhZ2VIYW5kbGVyLmluc3RhbmNlO1xuICB9XG5cbiAgc3RhdGljIGRlc3Ryb3koKSB7XG4gICAgaWYgKFNpY2JvTWVzc2FnZUhhbmRsZXIuaW5zdGFuY2UpIHtcbiAgICAgIFNpY2JvTWVzc2FnZUhhbmRsZXIuaW5zdGFuY2Uub25VblN1YnNjcmliZSgpO1xuICAgICAgU2ljYm9NZXNzYWdlSGFuZGxlci5pbnN0YW5jZS5vbkNsZWFyQWxsTXNnKCk7XG4gICAgfVxuICAgIFNpY2JvTWVzc2FnZUhhbmRsZXIuaW5zdGFuY2UgPSBudWxsO1xuICB9XG5cbiAgcHJpdmF0ZSBjdXJDaGFubmVsSWQ6IHN0cmluZyA9IFwic2ljYm92MlwiO1xuXG4gIHByaXZhdGUgZ2V0IG1ldGFEYXRhKCkge1xuICAgIHJldHVybiB7XG4gICAgICBBdXRob3JpemF0aW9uOiBcIkJlYXJlciBcIiArIFNpY2JvUG9ydGFsQWRhcHRlci5nZXRUb2tlbigpLFxuICAgIH07XG4gIH1cblxuICBwcm90ZWN0ZWQgc2hvd0xvYWRpbmcoY2FsbGJhY2s6IEZ1bmN0aW9uKSB7XG4gICAgU2ljYm9Qb3J0YWxBZGFwdGVyLmdldEluc3RhbmNlKCkuc2hvd0hhbmRsZXJMb2FkaW5nKGNhbGxiYWNrKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBoaWRlTG9hZGluZygpIHtcbiAgICBTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0SW5zdGFuY2UoKS5oaWRlSGFuZGxlckxvYWRpbmcoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBvblNob3dFcnIoc3RyOiBzdHJpbmcsIG9uQ2xpY2s/OiAoKSA9PiB2b2lkKSB7XG4gICAgLy9jYy5sb2coXCJTSUNCTyBvblNob3dFcnJcIiwgc3RyKTtcbiAgICBpZihjYy5kaXJlY3Rvci5nZXRTY2VuZSgpLm5hbWUgPT0gU2ljYm9TY2VuZS5Mb2FkaW5nKXtcbiAgICAgIFNpY2JvVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkub3BlblBvcHVwKFNpY2JvQ2Fubm90Sm9pblBvcHVwLCBTaWNib1VJTWFuYWdlci5TaWNib0Nhbm5vdEpvaW5Qb3B1cCwgbnVsbCk7XG4gICAgfWVsc2V7XG4gICAgICBTaWNib1BvcnRhbEFkYXB0ZXIuZ2V0SW5zdGFuY2UoKS5vblNob3dIYW5kbGVyRXJyKFNpY2JvVGV4dC5oYW5kbGVyRGVmYXVsdEVyck1zZywgb25DbGljayk7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIG9uRGlzY29ubmVjdChDb2RlU29ja2V0LCBtZXNzRXJyb3I6IHN0cmluZykge1xuICAgIFNpY2JvUG9ydGFsQWRhcHRlci5nZXRJbnN0YW5jZSgpLm9uSGFuZGxlckRpc2Nvbm5lY3QoQ29kZVNvY2tldCwgbWVzc0Vycm9yKTtcbiAgfVxuXG4gIGdldENoYW5uZWxJZCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmN1ckNoYW5uZWxJZDtcbiAgfVxuXG4gIGNsb3NlSGFuZGxlcigpIHtcbiAgICBpZiAoU2ljYm9NZXNzYWdlSGFuZGxlci5pbnN0YW5jZSkgU2ljYm9NZXNzYWdlSGFuZGxlci5pbnN0YW5jZS5vblVuU3Vic2NyaWJlKCk7XG4gICAgZGVsZXRlIFNpY2JvTWVzc2FnZUhhbmRsZXIuaW5zdGFuY2U7XG4gICAgU2ljYm9NZXNzYWdlSGFuZGxlci5pbnN0YW5jZSA9IG51bGw7XG4gIH1cblxuICBvbkRlc3Ryb3koKSB7XG4gICAgdGhpcy5jbG9zZUhhbmRsZXIoKTtcbiAgfVxuICAvLyNlbmRyZWdpb25cbiAgLy9TRUNUSU9OIGltcGxlbWVudCBCYXNlSGFuZGxlclxuICBvblJlY2VpdmUobXNnOiBJbnN0YW5jZVR5cGU8dHlwZW9mIEJlblRhdU1lc3NhZ2U+KSB7XG4gICAgc3VwZXIub25SZWNlaXZlKG1zZyk7XG4gICAgbGV0IG1lc3NhZ2UgPSBNZXNzYWdlLmRlc2VyaWFsaXplQmluYXJ5KG1zZy5nZXRQYXlsb2FkX2FzVTgoKSk7XG4gICAgc3dpdGNoIChtZXNzYWdlLmdldFBheWxvYWRDYXNlKCkpIHtcbiAgICAgIGNhc2UgTWVzc2FnZS5QYXlsb2FkQ2FzZS5TVEFURTpcbiAgICAgICAgbGV0IHN0YXRlID0gbWVzc2FnZS5nZXRTdGF0ZSgpO1xuICAgICAgICBpZiAodGhpcy5yZWNlaXZlU3RhdGUpIHRoaXMucmVjZWl2ZVN0YXRlKHN0YXRlKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICBicmVhaztcbiAgICB9XG4gIH1cblxuICAvLyNlbmRyZWdpb25cbiAgaGFuZGxlQ3VzdG9tRXJyb3IoZXJyQ29kZSwgZXJyOiBzdHJpbmcpIHtcbiAgICBpZiAodGhpcy5pc1NlcnZlckRpZUNvZGUoZXJyQ29kZSkpe1xuICAgICAgICBTaWNib1VJTWFuYWdlci5nZXRJbnN0YW5jZSgpLm9wZW5Qb3B1cChTaWNib0Nhbm5vdEpvaW5Qb3B1cCwgU2ljYm9VSU1hbmFnZXIuU2ljYm9DYW5ub3RKb2luUG9wdXAsIG51bGwpO1xuICAgIH1lbHNlXG4gICAgICAgIGlmICh0aGlzLm9uRXJyb3JDdXN0b20pIHRoaXMub25FcnJvckN1c3RvbShlcnJDb2RlLCBlcnIpO1xufVxuXG4gIHByaXZhdGUgaXNTZXJ2ZXJEaWVDb2RlKGVyckNvZGUpOiBib29sZWFue1xuICAgIGlmKFNpY2JvTWVzc2FnZUhhbmRsZXIuSVNfV1NfRElTQ09OTkVDVEVEIHx8ICghd2luZG93Lm5hdmlnYXRvciB8fCAhd2luZG93Lm5hdmlnYXRvci5vbkxpbmUpKSByZXR1cm4gZmFsc2U7XG4gICAgICByZXR1cm4gKGVyckNvZGUgPT0gQ29kZS5TSUNCT19DQU5OT1RfSk9JTiBcbiAgICAgICAgICB8fCBlcnJDb2RlID09IENvZGUuU0lDQk9fUkVRVUVTVF9USU1FT1VUIFxuICAgICAgICAgIHx8IGVyckNvZGUgPT0gQ29kZS5VTkFWQUlMQUJMRSBcbiAgICAgICAgICB8fCBlcnJDb2RlID09IENvZGUuU0lDQk9fU0VTU0lPTl9OT1RfRk9VTkRcbiAgICAgICAgICB8fCBlcnJDb2RlID09IENvZGUuU0lDQk9fQUNUT1JfTk9UX0ZPVU5EXG4gICAgICAgICAgfHwgZXJyQ29kZSA9PSBDb2RlLkFCT1JURURcbiAgICAgICAgICB8fCBlcnJDb2RlID09IENvZGUuVU5BVVRIRU5USUNBVEVEXG4gICAgICAgICAgfHwgZXJyQ29kZSA9PSBDb2RlLlVOS05PV04pXG4gIH1cblxuICAvLyFTRUNUSU9OXG5cbiAgLy9BTkNIT1IgQmV0XG4gIHByaXZhdGUgaXNOb3RFbm91Z2hNb25leShlcnJDb2RlKTogYm9vbGVhbntcbiAgICByZXR1cm4gKGVyckNvZGUgPT0gQ29kZS5TSUNCT19CRVRfTk9UX0VOT1VHSF9NT05FWSBcbiAgICAgICAgfHwgZXJyQ29kZSA9PSBDb2RlLlNJQ0JPX1JFQkVUX05PVF9FTk9VR0hfTU9ORVkgXG4gICAgICAgIHx8IGVyckNvZGUgPT0gQ29kZS5TSUNCT19CRVRfRE9VQkxFX05PVF9FTk9VR0hfTU9ORVkpXG4gIH1cblxuICBzZW5kQmV0UmVxdWVzdChkb29yLCBjaGlwOiBudW1iZXIsIHJlc3BvbnNlOiAocmVzcG9uc2U6IEluc3RhbmNlVHlwZTx0eXBlb2YgQmV0UmVwbHk+KSA9PiB2b2lkID0gbnVsbCkge1xuICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgIGxldCByZXF1ZXN0ID0gbmV3IEJldFJlcXVlc3QoKTtcbiAgICByZXF1ZXN0LnNldERvb3IoZG9vcik7XG4gICAgcmVxdWVzdC5zZXRDaGlwKGNoaXApO1xuXG4gICAgdmFyIG1zZ0lkID0gc2VsZi5nZXRVbmlxdWVJZCgpO1xuICAgIGxldCBzZW5kUmVxdWVzdCA9ICgpID0+IHtcbiAgICAgIHNlbGYuY2xpZW50LmJldChyZXF1ZXN0LCBzZWxmLm1ldGFEYXRhLCAoZXJyLCByZXMpID0+IHtcbiAgICAgICAgc2VsZi5vblNlbmRSZXBseShlcnIsIHJlcywgbXNnSWQpO1xuICAgICAgICAvL2NjLmxvZyhcIlNJQ0JPIEJldFJlcGx5XCIsIHJlcz8uZ2V0VHgoKSk7XG4gICAgICAgIFNpY2JvVHJhY2tpbmcudHJhY2tpbmdCZXQocmVzPy5nZXRUeCgpPy5nZXRMYXN0VHhJZCgpLCByZXM/LmdldFR4KCk/LmdldEFtb3VudCgpKTtcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICB0aGlzLm9uU2VuZFJlcXVlc3QobXNnSWQsIHNlbmRSZXF1ZXN0LCByZXNwb25zZSwgKGVyciwgY29kZSk9PntcbiAgICAgIGlmKHNlbGYuaXNOb3RFbm91Z2hNb25leShjb2RlKSl7XG4gICAgICAgIGNjLnN5c3RlbUV2ZW50LmVtaXQoRXZlbnRLZXkuSEFORExFX09VVF9PRl9CQUxBTkNFX1dIRU5fQkVUVElOR19HQU1FLCBTaWNib01lc3NhZ2VIYW5kbGVyLmluc3RhbmNlLnRvcGljLCBjaGlwKTtcbiAgICAgIH1lbHNle1xuICAgICAgICAgIHNlbGY/LmhhbmRsZUN1c3RvbUVycm9yKGNvZGUsIGVycik7XG4gICAgICB9XG4gICAgICAvLyBzZWxmPy5oYW5kbGVDdXN0b21FcnJvcihjb2RlLCBlcnIpO1xuICAgIH0sIGZhbHNlKTtcbiAgfVxuXG4gIC8vQU5DSE9SIFJlYmV0XG4gIHNlbmRSZWJldFJlcXVlc3QocmVzcG9uc2U6IChyZXNwb25zZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBSZUJldFJlcGx5PikgPT4gdm9pZCA9IG51bGwpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgbGV0IHJlcXVlc3QgPSBuZXcgUmVCZXRSZXF1ZXN0KCk7XG4gICAgdmFyIG1zZ0lkID0gc2VsZi5nZXRVbmlxdWVJZCgpO1xuXG4gICAgbGV0IHNlbmRSZXF1ZXN0ID0gKCkgPT4ge1xuICAgICAgc2VsZi5jbGllbnQucmViZXQocmVxdWVzdCwgc2VsZi5tZXRhRGF0YSwgKGVyciwgcmVzKSA9PiB7XG4gICAgICAgIHNlbGYub25TZW5kUmVwbHkoZXJyLCByZXMsIG1zZ0lkKTtcbiAgICAgICAgLy9jYy5sb2coXCJTSUNCTyBSZUJldFJlcGx5XCIsIHJlcz8uZ2V0VHgoKSk7XG4gICAgICAgIFNpY2JvVHJhY2tpbmcudHJhY2tpbmdSZUJldChyZXM/LmdldFR4KCk/LmdldExhc3RUeElkKCksIHJlcz8uZ2V0VHgoKT8uZ2V0QW1vdW50KCkpO1xuXG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgdGhpcy5vblNlbmRSZXF1ZXN0KG1zZ0lkLCBzZW5kUmVxdWVzdCwgcmVzcG9uc2UsIChlcnIsIGNvZGUpPT57XG4gICAgICBpZihzZWxmLmlzTm90RW5vdWdoTW9uZXkoY29kZSkpe1xuICAgICAgICBjYy5zeXN0ZW1FdmVudC5lbWl0KEV2ZW50S2V5LkhBTkRMRV9PVVRfT0ZfQkFMQU5DRV9XSEVOX0JFVFRJTkdfR0FNRSwgU2ljYm9NZXNzYWdlSGFuZGxlci5pbnN0YW5jZS50b3BpYywgMCk7XG4gICAgICB9ZWxzZXtcbiAgICAgICAgICBzZWxmPy5oYW5kbGVDdXN0b21FcnJvcihjb2RlLCBlcnIpO1xuICAgICAgfVxuICAgICAgLy8gc2VsZj8uaGFuZGxlQ3VzdG9tRXJyb3IoY29kZSwgZXJyKTtcbiAgICB9LCBmYWxzZSk7XG4gIH1cblxuICAvL0FOQ0hPUiBEb3VibGUgYmV0XG4gIHNlbmRCZXREb3VibGVSZXF1ZXN0KHJlc3BvbnNlOiAocmVzcG9uc2U6IEluc3RhbmNlVHlwZTx0eXBlb2YgQmV0RG91YmxlUmVwbHk+KSA9PiB2b2lkID0gbnVsbCkge1xuICAgIHZhciBzZWxmID0gdGhpcztcbiAgICBsZXQgcmVxdWVzdCA9IG5ldyBCZXREb3VibGVSZXF1ZXN0KCk7XG4gICAgdmFyIG1zZ0lkID0gc2VsZi5nZXRVbmlxdWVJZCgpO1xuXG4gICAgbGV0IHNlbmRSZXF1ZXN0ID0gKCkgPT4ge1xuICAgICAgc2VsZi5jbGllbnQuYmV0RG91YmxlKHJlcXVlc3QsIHNlbGYubWV0YURhdGEsIChlcnIsIHJlcykgPT4ge1xuICAgICAgICBzZWxmLm9uU2VuZFJlcGx5KGVyciwgcmVzLCBtc2dJZCk7XG4gICAgICAgIC8vY2MubG9nKFwiU0lDQk8gQmV0RG91YmxlUmVwbHlcIiwgcmVzPy5nZXRUeCgpKTtcbiAgICAgICAgU2ljYm9UcmFja2luZy50cmFja2luZ0RvdWJsZUJldChyZXM/LmdldFR4KCk/LmdldExhc3RUeElkKCksIHJlcz8uZ2V0VHgoKT8uZ2V0QW1vdW50KCkpO1xuXG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgdGhpcy5vblNlbmRSZXF1ZXN0KG1zZ0lkLCBzZW5kUmVxdWVzdCwgcmVzcG9uc2UsIChlcnIsIGNvZGUpPT57XG4gICAgICBpZihzZWxmLmlzTm90RW5vdWdoTW9uZXkoY29kZSkpe1xuICAgICAgICBjYy5zeXN0ZW1FdmVudC5lbWl0KEV2ZW50S2V5LkhBTkRMRV9PVVRfT0ZfQkFMQU5DRV9XSEVOX0JFVFRJTkdfR0FNRSwgU2ljYm9NZXNzYWdlSGFuZGxlci5pbnN0YW5jZS50b3BpYywgMCk7XG4gICAgICB9ZWxzZXtcbiAgICAgICAgICBzZWxmPy5oYW5kbGVDdXN0b21FcnJvcihjb2RlLCBlcnIpO1xuICAgICAgfVxuICAgICAgLy8gc2VsZj8uaGFuZGxlQ3VzdG9tRXJyb3IoY29kZSwgZXJyKTtcbiAgICB9LCBmYWxzZSk7XG4gIH1cblxuICAvL0FOQ0hPUiBBdXRvSm9pblxuICBzZW5kQXV0b0pvaW5SZXF1ZXN0KHJlc3BvbnNlOiAocmVzcG9uc2U6IEluc3RhbmNlVHlwZTx0eXBlb2YgQXV0b0pvaW5SZXBseT4pID0+IHZvaWQpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgbGV0IHJlcXVlc3QgPSBuZXcgRW1wdHkoKTtcblxuICAgIHZhciBtc2dJZCA9IHNlbGYuZ2V0VW5pcXVlSWQoKTtcbiAgICBsZXQgc2VuZFJlcXVlc3QgPSAoKSA9PiB7XG4gICAgICBzZWxmLmNsaWVudC5hdXRvSm9pbihyZXF1ZXN0LCBzZWxmLm1ldGFEYXRhLCAoZXJyLCByZXMpID0+IHtcbiAgICAgICAgc2VsZi5vblNlbmRSZXBseShlcnIsIHJlcywgbXNnSWQpO1xuICAgICAgfSk7XG4gICAgfTtcblxuICAgIHRoaXMub25TZW5kUmVxdWVzdChtc2dJZCwgc2VuZFJlcXVlc3QsIHJlc3BvbnNlLCAoZXJyLCBjb2RlKT0+e1xuICAgICAgc2VsZj8uaGFuZGxlQ3VzdG9tRXJyb3IoY29kZSwgZXJyKTtcbiAgICB9LCBmYWxzZSk7XG4gIH1cbiAgLy9BTkNIT1IgTGVhdmVcbiAgc2VuZExlYXZlUmVxdWVzdChyZXNwb25zZTogKHJlc3BvbnNlOiBJbnN0YW5jZVR5cGU8dHlwZW9mIExlYXZlUmVwbHk+KSA9PiB2b2lkKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIGxldCByZXF1ZXN0ID0gbmV3IExlYXZlUmVxdWVzdCgpO1xuXG4gICAgdmFyIG1zZ0lkID0gc2VsZi5nZXRVbmlxdWVJZCgpO1xuICAgIGxldCBzZW5kUmVxdWVzdCA9ICgpID0+IHtcbiAgICAgIHNlbGYuY2xpZW50LmxlYXZlKHJlcXVlc3QsIHNlbGYubWV0YURhdGEsIChlcnIsIHJlcykgPT4ge1xuICAgICAgICBzZWxmLm9uU2VuZFJlcGx5KGVyciwgcmVzLCBtc2dJZCk7XG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgdGhpcy5vblNlbmRSZXF1ZXN0KG1zZ0lkLCBzZW5kUmVxdWVzdCwgcmVzcG9uc2UsIChlcnIsIGNvZGUpPT57XG4gICAgICBzZWxmPy5oYW5kbGVDdXN0b21FcnJvcihjb2RlLCBlcnIpO1xuICAgIH0sIGZhbHNlKTtcbiAgfVxuXG4gIC8vQU5DSE9SIGdldCBPdGhlcnNcbiAgc2VuZEdldE90aGVyc1JlcXVlc3QoZnJvbTogbnVtYmVyLCBsaW1pdDogbnVtYmVyLCByZXNwb25zZTogKHJlc3BvbnNlOiBJbnN0YW5jZVR5cGU8dHlwZW9mIEdldE90aGVyc1JlcGx5PikgPT4gdm9pZCkge1xuICAgIHZhciBzZWxmID0gdGhpcztcbiAgICBsZXQgcmVxdWVzdCA9IG5ldyBHZXRPdGhlcnNSZXF1ZXN0KCk7XG4gICAgcmVxdWVzdC5zZXRDdXJzb3IoZnJvbSk7XG4gICAgcmVxdWVzdC5zZXRMaW1pdChsaW1pdCk7XG5cbiAgICB2YXIgbXNnSWQgPSBzZWxmLmdldFVuaXF1ZUlkKCk7XG4gICAgbGV0IHNlbmRSZXF1ZXN0ID0gKCkgPT4ge1xuICAgICAgc2VsZi5jbGllbnQuZ2V0T3RoZXJzKHJlcXVlc3QsIHNlbGYubWV0YURhdGEsIChlcnIsIHJlcykgPT4ge1xuICAgICAgICBzZWxmLm9uU2VuZFJlcGx5KGVyciwgcmVzLCBtc2dJZCk7XG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgdGhpcy5vblNlbmRSZXF1ZXN0KG1zZ0lkLCBzZW5kUmVxdWVzdCwgcmVzcG9uc2UsIChlcnIsIGNvZGUpPT57XG4gICAgICBzZWxmPy5oYW5kbGVDdXN0b21FcnJvcihjb2RlLCBlcnIpO1xuICAgIH0sIGZhbHNlKTtcbiAgfVxuXG4gICAgLy9BTkNIT1IgLSBIZWFsdGggY2hlY2tcbiAgICBzZW5kSGVhbHRoQ2hlY2socmVzcG9uc2U6IChyZXNwb25zZTogSW5zdGFuY2VUeXBlPHR5cGVvZiBIZWFsdGhDaGVja1JlcGx5PikgPT4gdm9pZCl7XG4gICAgICBsZXQgc2VsZiA9IHRoaXNcbiAgICAgIGxldCByZXF1ZXN0ID0gbmV3IEhlYWx0aENoZWNrUmVxdWVzdCgpO1xuXG4gICAgICB2YXIgbXNnSWQgPSBzZWxmLmdldFVuaXF1ZUlkKCk7XG4gICAgICBsZXQgc2VuZFJlcXVlc3QgPSAoKSA9PiB7XG4gICAgICAgICAgc2VsZi5jbGllbnQuaGVhbHRoQ2hlY2socmVxdWVzdCwgc2VsZi5tZXRhRGF0YSwgKGVyciwgcmVzKSA9PiB7XG4gICAgICAgICAgICAgIHNlbGYub25TZW5kUmVwbHkoZXJyLCByZXMsIG1zZ0lkKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH07XG5cbiAgICAgIHRoaXMub25TZW5kUmVxdWVzdChtc2dJZCwgc2VuZFJlcXVlc3QsIHJlc3BvbnNlLCAgKGVyciwgY29kZSk9PntcbiAgICAgICAgICBzZWxmPy5oYW5kbGVDdXN0b21FcnJvcihjb2RlLCBlcnIpO1xuICAgICAgfSwgZmFsc2UpO1xuICB9XG5cbn1cblxuXG4iXX0=