
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Define/GameDefine.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f97e6MRpsVAiYn+ZLhBPZJS', 'GameDefine');
// Portal/Common/Scripts/Defines/GameDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameDefine = /** @class */ (function () {
    function GameDefine() {
    }
    GameDefine.Sicbo = "Sicbo";
    GameDefine.XocDia = "XocDia";
    GameDefine.BauCua = "BauCua";
    GameDefine.Phom = "Phom";
    GameDefine.TienLenMienNam = "TienLenMienNam";
    GameDefine.MauBinh = "MauBinh";
    //#region ======== ON/OFF ========
    GameDefine.CC_LOG_ENABLE = true;
    // Tạm sửa đẻ quay video xong mở lại
    GameDefine.LOG_VERSION_ENABLE = false; // Use For Drawcall/Version
    GameDefine.IS_PROD_VERSION = false;
    //#region ======== scene ========
    GameDefine.SicboScene = "SicboScene";
    GameDefine.XocDiaScene = "XocDiaScene";
    GameDefine.ThanTaiScene = "ThanTaiScene";
    GameDefine.ChienThanScene = "ChienThanScene";
    GameDefine.LoDeScene = "LoDeScene";
    GameDefine.SplashGame = "SplashScene";
    GameDefine.MainScene = "MainScene";
    GameDefine.LobbyTLMNScene = "TLMNLobbyScene";
    GameDefine.BauCuaScene = "BauCuaScene";
    GameDefine.LoadingChangeScene = "LoadingChangeScene";
    GameDefine.MauBinhScene = "MauBinhLobbyScene";
    GameDefine.PokerScene = "PokerLobbyScene";
    GameDefine.PhomScene = "PhomLobbyScene";
    GameDefine.LobbyCardGame = "LobbyCardGame";
    GameDefine.KBDDScene = "KBDDScene";
    GameDefine.KBDDLobbyScene = "KBDDLobbyScene";
    //#endregion
    //#region ======== element ========
    GameDefine.PortalBundle = "PortalResources";
    GameDefine.LobbyCardBundle = "LobbyCardBundle";
    GameDefine.PaymentBundle = "Payment";
    //#endregion
    //#region ======== element ========
    GameDefine.HeaderMainGameElement = "HeaderMainGameElement";
    //#endregion
    //#region ======== popup ========
    GameDefine.ProfilePopup = "ProfilePopup";
    GameDefine.InfoGamePopup = "InfoGamePopup";
    GameDefine.NotifyPopup = "NotifyPopup";
    GameDefine.ChatPopup = "ChatPopupNew";
    GameDefine.CardGameChatPopup = "CardGameChatPopup";
    GameDefine.TableGameChatPopup = "TableGameChatPopup";
    GameDefine.LoginPopup = "LoginPopup";
    GameDefine.ChatSettingPopup = "ChatSettingPopup";
    GameDefine.Confirm2ButtonPopup = "Confirm2ButtonPopup";
    GameDefine.CreateTablePopup = "CreateTablePopup";
    GameDefine.LeaderBoardPopup = "LeaderBoardPopup";
    // public static PhomLeaderBoardPopup: string = "PhomVinhDanhPopup";
    GameDefine.AllPlayerInTablePopup = "AllPlayerInTablePopup";
    GameDefine.UserProfilePopup = "UserProfilePopup";
    GameDefine.UserProfilePopupNew = "UserProfilePopupNew";
    GameDefine.UpdateNickNamePopup = "UpdateNickNamePopup";
    GameDefine.ChangeAvatarPopup = "ChangeAvatarPopup";
    GameDefine.SettingPopup = "SettingPopup";
    GameDefine.ThanTaiSettingPopup = "ThanTaiSettingPopup";
    GameDefine.PasswordPopup = "PasswordPopup";
    GameDefine.WalletPopup = "WalletPopup";
    GameDefine.ShareTablePopup = "ShareTablePopup";
    GameDefine.BetHistoryPopup = "BetHistoryPopup";
    GameDefine.ChangeLocationPopup = "ChangeLocationPopup";
    GameDefine.ChangeMailPopup = "ChangeMailPopup";
    GameDefine.ChangePasswordPopup = "ChangePasswordPopup";
    GameDefine.ChangePhonePopup = "ChangePhonePopup";
    GameDefine.LogoutPopup = "LogoutPopup";
    GameDefine.MainSettingPopup = "MainSettingPopupV2";
    GameDefine.VerifyEmailSuccessPopup = "VerifyEmailSuccessPopup";
    GameDefine.GlobalInviteSettingPopup = "GlobalInviteSettingPopup";
    GameDefine.GlobalInviteNotify = "NotifyInviteGlobal";
    GameDefine.PortalHistoryPopup = "PortalHistoryPopup";
    GameDefine.PortalVipGuidePopup = "PortalVipGuidePopup";
    GameDefine.PortalMessagePopup = "PortalMessagePopup";
    GameDefine.DeleteMessagePopup = "DeleteMessagePopup";
    GameDefine.PortalReferralPopup = "PortalReferralPopup";
    GameDefine.TLMNBetHistoryPopup = "TLMNHistoryPopup";
    GameDefine.PhomHistoryPopup = "PhomHistoryPopup";
    GameDefine.CardGameSettingPopup = "CardGameSettingPopup";
    GameDefine.MauBinhHistoryPopup = "MauBinhHistoryPopup";
    GameDefine.CardGameInfoGamePopup = "CardGameInfoGamePopup";
    GameDefine.CustomAvatarPopup = "CustomAvatarPopupNew";
    GameDefine.EventPopup = "EventPopup";
    GameDefine.PortalLeaderBoardPopup = "PortalLeaderBoardPopupV2";
    GameDefine.TopHuBigPopup = "TopHuBigPopup";
    GameDefine.GiftCodeHistoryPopup = "GiftCodeHistoryPopup";
    GameDefine.AnimationLevelUpPopup = "AnimationLevelUpPopup";
    GameDefine.RewardPopup = "RewardPopup";
    GameDefine.ConfirmVaultPopup = "ConfirmVaultPopup";
    GameDefine.MissionGuidePopup = "MissionGuidePopup";
    GameDefine.TXMNPromotionPopup = "TXMNPromotionPopup";
    GameDefine.TopupPromotionPopup = "TopupPromotionPopup";
    GameDefine.CardGamePromotionPopup = "CardGamePromotionPopup";
    GameDefine.AFFCupEventPromotionPopup = "AFFCupEventPromotionPopup";
    GameDefine.SpringRotationPromotionPopup = "SpringRotationPromotionPopup";
    GameDefine.LoDeGuidePopup = "LoDeGuidePopup";
    GameDefine.LoDeLeaderBoardPopup = "LoDeLeaderBoardPopup";
    GameDefine.LoDeStatisticPopup = "LoDeStatisticPopup";
    GameDefine.LoDeBetHistoryPopup = "LoDeBetHistoryPopup";
    GameDefine.LoDeBetHistoryDetailPopup = "LoDeBetHistoryDetailPopup";
    GameDefine.ListCsAccountPopup = "ListCsAccountPopup";
    GameDefine.EventGoingPopup = "EventGoingPopup";
    GameDefine.GiftCodeNotifyPopup = "GiftCodeNotifyPopup";
    GameDefine.GiftCodePopup = "GiftCodePopup";
    //#endregion
    //#region WorldCupEvent
    GameDefine.PortalWorldCupEventPopup = "PortalWorldCupEventPopup";
    GameDefine.WorldCupEventPromotionPopup = "WorldCupEventPromotionPopup";
    GameDefine.WorldCupEventPopupGuide = "WorldCupEvent/WCEventPopupGuide";
    GameDefine.WorldCupEventPopupHistory = "WorldCupEvent/WCEventPopupHistory";
    GameDefine.WorldCupEventPopupLeaderboard = "WorldCupEvent/WCEventPopupLeaderboard";
    //#endregion
    //#region AFFCupEvent
    GameDefine.PortalAFFCupEventPopup = "PortalAFFCupEventPopup";
    GameDefine.AFFCupEventPopupGuide = "AFFCupEvent/AFFEventPopupGuide";
    GameDefine.AFFCupEventPopupHistory = "AFFCupEvent/AFFEventPopupHistory";
    GameDefine.AFFCupEventPopupLeaderboard = "AFFCupEvent/AFFEventPopupLeaderboard";
    //#endregion
    //#region AFFCupEvent
    GameDefine.PortalSpringRotationEventPopup = "PortalSpringRotationEventPopup";
    GameDefine.SpringRotationEventPopupGuide = "SpringRotationEvent/SpringRotationEventPopupGuide";
    GameDefine.SpringRotationEventPopupHistory = "SpringRotationEvent/SpringRotationEventPopupHistory";
    //#endregion
    //#region ======== notify ========
    GameDefine.TextNotifyDisconnect = "TextNotifyDisconnect";
    GameDefine.TextNotify = "TextNotify";
    GameDefine.TextBauCuaNotify = "TextBauCuaNotify";
    GameDefine.TextSicboNotify = "TextSicboNotify";
    GameDefine.NotifyClaimAttachmentPopup = "NotifyClaimAttachmentPopup";
    //#endregion
    //#region ======== popup system ========
    GameDefine.ConfirmPopupSystem = "ConfirmPopupSystem";
    GameDefine.LoadingPopupSystem = "LoadingPopupSystem";
    GameDefine.WarningPopupSystem = "WarningPopupSystem";
    GameDefine.RetryPopupSystem = "RetryPopupSystem";
    GameDefine.MaintenacePopupSystem = "MaintenacePopupSystem";
    //#endregion
    ////#region ========MissionGameNamePortal==============
    GameDefine.generalMission = "general";
    GameDefine.bauCuaMission = "baucua";
    GameDefine.tienLenMienNamMission = "tienlenmiennam";
    GameDefine.sicBoMission = "sicbo";
    GameDefine.thanTaiMission = "thantai";
    GameDefine.pokerMission = "poker";
    GameDefine.slotMission = "slot"; //#endregion
    ////#region ========promotionPopupDefine==============
    GameDefine.promotionPopupDefine = [
        "TXMNPromotionPopup",
        "TopupPromotionPopup",
        "CardGamePromotionPopup",
        "WorldCupEventPromotionPopup",
        "AFFCupEventPromotionPopup",
        "SpringRotationPromotionPopup"
    ];
    //#region ======== fight data test ========
    GameDefine.fightTestData = [
        { id: 0, icon: "item_001_2", amount: 5, price: 100, name: "Con Dán", rotate: false },
        { id: 1, icon: "item_002_2", amount: 10, price: 200, name: "Trứng Gà", rotate: false },
        //{ id: 2, icon: "item_003_2", amount: 15, price: 300 },
        { id: 3, icon: "item_004_2", amount: 20, price: 400, name: "Lựu Đạn", rotate: true },
        { id: 4, icon: "item_005_2", amount: 25, price: 500, name: "Trứng Thúi", rotate: true },
        //{ id: 5, icon: "item_006_2", amount: 0, price: 600 },
        { id: 6, icon: "item_007_2", amount: 0, price: 700, name: "Boom", rotate: true },
    ];
    //#end
    //#region ======== gift data test ========
    GameDefine.giftTestData = [
        { id: 0, icon: "giftitems_001", amount: 0, price: 100, name: "Quả Bóng", rotate: true },
        { id: 1, icon: "giftitems_002", amount: 0, price: 200, name: "Bánh Kem", rotate: false },
        { id: 2, icon: "giftitems_003", amount: 0, price: 300, name: "Hộp Sửa", rotate: false },
        { id: 3, icon: "giftitems_004", amount: 0, price: 400, name: "Tách Cafe", rotate: false },
        {
            id: 4,
            icon: "giftitems_005",
            amount: 999,
            price: 500,
            name: "1 Đóa Hồng",
            rotate: false,
        },
    ];
    //#end
    //#region ======== gift data test ========
    GameDefine.tipsTestData = [
        {
            id: 0,
            icon: "dealeritems_001_2",
            amount: 0,
            price: 100,
            name: "Con Gấu",
            rotate: false,
        },
        {
            id: 1,
            icon: "dealeritems_002_2",
            amount: 0,
            price: 200,
            name: "Tách Cafe",
            rotate: false,
        },
        {
            id: 2,
            icon: "dealeritems_003_2",
            amount: 0,
            price: 300,
            name: "Cocktail",
            rotate: false,
        },
        {
            id: 3,
            icon: "dealeritems_004_2",
            amount: 0,
            price: 400,
            name: "Đôi Guốc",
            rotate: false,
        },
        {
            id: 4,
            icon: "dealeritems_005_2",
            amount: 10,
            price: 500,
            name: "Tim Pha Lê",
            rotate: false,
        },
        {
            id: 5,
            icon: "dealeritems_006_2",
            amount: 0,
            price: 600,
            name: "Thỏi Son",
            rotate: false,
        },
        {
            id: 6,
            icon: "dealeritems_007_2",
            amount: 0,
            price: 700,
            name: "Ly Rượu Hồng",
            rotate: false,
        },
        {
            id: 7,
            icon: "dealeritems_008_2",
            amount: 0,
            price: 700,
            name: "1 Đóa Hồng",
            rotate: false,
        },
        {
            id: 8,
            icon: "dealeritems_009_2",
            amount: 0,
            price: 700,
            name: "Bức Thư Tình",
            rotate: false,
        },
        {
            id: 9,
            icon: "dealeritems_0010_2",
            amount: 0,
            price: 700,
            name: "Nụ Hôn",
            rotate: false,
        },
    ];
    return GameDefine;
}());
exports.default = GameDefine;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9Qb3J0YWwvQ29tbW9uL1NjcmlwdHMvRGVmaW5lcy9HYW1lRGVmaW5lLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7SUFBQTtJQXdUQSxDQUFDO0lBdlRlLGdCQUFLLEdBQVcsT0FBTyxDQUFDO0lBQ3hCLGlCQUFNLEdBQVcsUUFBUSxDQUFDO0lBQzFCLGlCQUFNLEdBQVcsUUFBUSxDQUFDO0lBQzFCLGVBQUksR0FBVyxNQUFNLENBQUM7SUFDdEIseUJBQWMsR0FBVyxnQkFBZ0IsQ0FBQztJQUMxQyxrQkFBTyxHQUFXLFNBQVMsQ0FBQztJQUUxQyxrQ0FBa0M7SUFDcEIsd0JBQWEsR0FBWSxJQUFJLENBQUM7SUFDNUMsb0NBQW9DO0lBQ3RCLDZCQUFrQixHQUFZLEtBQUssQ0FBQyxDQUFDLDJCQUEyQjtJQUNoRSwwQkFBZSxHQUFZLEtBQUssQ0FBQztJQUUvQyxpQ0FBaUM7SUFDbkIscUJBQVUsR0FBVyxZQUFZLENBQUM7SUFDbEMsc0JBQVcsR0FBVyxhQUFhLENBQUM7SUFDcEMsdUJBQVksR0FBVyxjQUFjLENBQUM7SUFDdEMseUJBQWMsR0FBVyxnQkFBZ0IsQ0FBQztJQUMxQyxvQkFBUyxHQUFXLFdBQVcsQ0FBQztJQUNoQyxxQkFBVSxHQUFXLGFBQWEsQ0FBQztJQUNuQyxvQkFBUyxHQUFXLFdBQVcsQ0FBQztJQUNoQyx5QkFBYyxHQUFXLGdCQUFnQixDQUFDO0lBQzFDLHNCQUFXLEdBQVcsYUFBYSxDQUFDO0lBQ3BDLDZCQUFrQixHQUFXLG9CQUFvQixDQUFDO0lBQ2xELHVCQUFZLEdBQVcsbUJBQW1CLENBQUM7SUFDM0MscUJBQVUsR0FBVyxpQkFBaUIsQ0FBQztJQUN2QyxvQkFBUyxHQUFXLGdCQUFnQixDQUFDO0lBQ3JDLHdCQUFhLEdBQVcsZUFBZSxDQUFDO0lBQ3hDLG9CQUFTLEdBQVcsV0FBVyxDQUFDO0lBQ2hDLHlCQUFjLEdBQVcsZ0JBQWdCLENBQUM7SUFFeEQsWUFBWTtJQUVaLG1DQUFtQztJQUNyQix1QkFBWSxHQUFXLGlCQUFpQixDQUFDO0lBQ3pDLDBCQUFlLEdBQVcsaUJBQWlCLENBQUM7SUFDNUMsd0JBQWEsR0FBVyxTQUFTLENBQUM7SUFDaEQsWUFBWTtJQUVaLG1DQUFtQztJQUNyQixnQ0FBcUIsR0FBVyx1QkFBdUIsQ0FBQztJQUN0RSxZQUFZO0lBRVosaUNBQWlDO0lBQ25CLHVCQUFZLEdBQVcsY0FBYyxDQUFDO0lBQ3RDLHdCQUFhLEdBQVcsZUFBZSxDQUFDO0lBQ3hDLHNCQUFXLEdBQVcsYUFBYSxDQUFDO0lBQ3BDLG9CQUFTLEdBQVcsY0FBYyxDQUFDO0lBQ25DLDRCQUFpQixHQUFXLG1CQUFtQixDQUFDO0lBQ2hELDZCQUFrQixHQUFXLG9CQUFvQixDQUFDO0lBQ2xELHFCQUFVLEdBQVcsWUFBWSxDQUFDO0lBQ2xDLDJCQUFnQixHQUFXLGtCQUFrQixDQUFDO0lBQzlDLDhCQUFtQixHQUFXLHFCQUFxQixDQUFDO0lBQ3BELDJCQUFnQixHQUFXLGtCQUFrQixDQUFDO0lBRTlDLDJCQUFnQixHQUFXLGtCQUFrQixDQUFDO0lBQzVELG9FQUFvRTtJQUN0RCxnQ0FBcUIsR0FBVyx1QkFBdUIsQ0FBQztJQUN4RCwyQkFBZ0IsR0FBVyxrQkFBa0IsQ0FBQztJQUM5Qyw4QkFBbUIsR0FBVyxxQkFBcUIsQ0FBQztJQUNwRCw4QkFBbUIsR0FBVyxxQkFBcUIsQ0FBQztJQUNwRCw0QkFBaUIsR0FBVyxtQkFBbUIsQ0FBQztJQUNoRCx1QkFBWSxHQUFXLGNBQWMsQ0FBQztJQUN0Qyw4QkFBbUIsR0FBVyxxQkFBcUIsQ0FBQztJQUNwRCx3QkFBYSxHQUFXLGVBQWUsQ0FBQztJQUN4QyxzQkFBVyxHQUFXLGFBQWEsQ0FBQztJQUNwQywwQkFBZSxHQUFXLGlCQUFpQixDQUFDO0lBRTVDLDBCQUFlLEdBQVcsaUJBQWlCLENBQUM7SUFDNUMsOEJBQW1CLEdBQVcscUJBQXFCLENBQUM7SUFDcEQsMEJBQWUsR0FBVyxpQkFBaUIsQ0FBQztJQUM1Qyw4QkFBbUIsR0FBVyxxQkFBcUIsQ0FBQztJQUNwRCwyQkFBZ0IsR0FBVyxrQkFBa0IsQ0FBQztJQUM5QyxzQkFBVyxHQUFXLGFBQWEsQ0FBQztJQUNwQywyQkFBZ0IsR0FBVyxvQkFBb0IsQ0FBQztJQUNoRCxrQ0FBdUIsR0FBVyx5QkFBeUIsQ0FBQztJQUM1RCxtQ0FBd0IsR0FBVywwQkFBMEIsQ0FBQztJQUM5RCw2QkFBa0IsR0FBVyxvQkFBb0IsQ0FBQztJQUNsRCw2QkFBa0IsR0FBVyxvQkFBb0IsQ0FBQztJQUNsRCw4QkFBbUIsR0FBVyxxQkFBcUIsQ0FBQztJQUNwRCw2QkFBa0IsR0FBVyxvQkFBb0IsQ0FBQztJQUNsRCw2QkFBa0IsR0FBVyxvQkFBb0IsQ0FBQztJQUNsRCw4QkFBbUIsR0FBVyxxQkFBcUIsQ0FBQztJQUVwRCw4QkFBbUIsR0FBVyxrQkFBa0IsQ0FBQztJQUVqRCwyQkFBZ0IsR0FBVyxrQkFBa0IsQ0FBQztJQUM5QywrQkFBb0IsR0FBVyxzQkFBc0IsQ0FBQztJQUV0RCw4QkFBbUIsR0FBVyxxQkFBcUIsQ0FBQztJQUNwRCxnQ0FBcUIsR0FBVyx1QkFBdUIsQ0FBQztJQUV4RCw0QkFBaUIsR0FBVyxzQkFBc0IsQ0FBQztJQUNuRCxxQkFBVSxHQUFXLFlBQVksQ0FBQztJQUNsQyxpQ0FBc0IsR0FBVywwQkFBMEIsQ0FBQztJQUM1RCx3QkFBYSxHQUFXLGVBQWUsQ0FBQztJQUN4QywrQkFBb0IsR0FBVyxzQkFBc0IsQ0FBQztJQUN0RCxnQ0FBcUIsR0FBVyx1QkFBdUIsQ0FBQztJQUN4RCxzQkFBVyxHQUFXLGFBQWEsQ0FBQztJQUNwQyw0QkFBaUIsR0FBVyxtQkFBbUIsQ0FBQztJQUNoRCw0QkFBaUIsR0FBVyxtQkFBbUIsQ0FBQztJQUNoRCw2QkFBa0IsR0FBVyxvQkFBb0IsQ0FBQztJQUNsRCw4QkFBbUIsR0FBVyxxQkFBcUIsQ0FBQztJQUNwRCxpQ0FBc0IsR0FBVyx3QkFBd0IsQ0FBQTtJQUN6RCxvQ0FBeUIsR0FBVywyQkFBMkIsQ0FBQztJQUNoRSx1Q0FBNEIsR0FBVyw4QkFBOEIsQ0FBQztJQUN0RSx5QkFBYyxHQUFXLGdCQUFnQixDQUFDO0lBQzFDLCtCQUFvQixHQUFXLHNCQUFzQixDQUFDO0lBQ3RELDZCQUFrQixHQUFXLG9CQUFvQixDQUFDO0lBQ2xELDhCQUFtQixHQUFXLHFCQUFxQixDQUFDO0lBQ3BELG9DQUF5QixHQUFXLDJCQUEyQixDQUFDO0lBQ2hFLDZCQUFrQixHQUFXLG9CQUFvQixDQUFDO0lBQ2xELDBCQUFlLEdBQVcsaUJBQWlCLENBQUM7SUFDNUMsOEJBQW1CLEdBQVcscUJBQXFCLENBQUM7SUFDcEQsd0JBQWEsR0FBVyxlQUFlLENBQUM7SUFFdEQsWUFBWTtJQUVaLHVCQUF1QjtJQUNULG1DQUF3QixHQUFXLDBCQUEwQixDQUFDO0lBQzlELHNDQUEyQixHQUFXLDZCQUE2QixDQUFDO0lBQ3BFLGtDQUF1QixHQUFXLGlDQUFpQyxDQUFDO0lBQ3BFLG9DQUF5QixHQUFXLG1DQUFtQyxDQUFDO0lBQ3hFLHdDQUE2QixHQUFXLHVDQUF1QyxDQUFDO0lBQzlGLFlBQVk7SUFFWixxQkFBcUI7SUFDUCxpQ0FBc0IsR0FBVyx3QkFBd0IsQ0FBQztJQUMxRCxnQ0FBcUIsR0FBVyxnQ0FBZ0MsQ0FBQztJQUNqRSxrQ0FBdUIsR0FBVyxrQ0FBa0MsQ0FBQztJQUNyRSxzQ0FBMkIsR0FBVyxzQ0FBc0MsQ0FBQztJQUMzRixZQUFZO0lBR1oscUJBQXFCO0lBQ1AseUNBQThCLEdBQVcsZ0NBQWdDLENBQUM7SUFDMUUsd0NBQTZCLEdBQVcsbURBQW1ELENBQUM7SUFDNUYsMENBQStCLEdBQVcscURBQXFELENBQUM7SUFFOUcsWUFBWTtJQUVaLGtDQUFrQztJQUNwQiwrQkFBb0IsR0FBVyxzQkFBc0IsQ0FBQztJQUN0RCxxQkFBVSxHQUFXLFlBQVksQ0FBQztJQUNsQywyQkFBZ0IsR0FBVyxrQkFBa0IsQ0FBQztJQUM5QywwQkFBZSxHQUFXLGlCQUFpQixDQUFDO0lBQzVDLHFDQUEwQixHQUFXLDRCQUE0QixDQUFDO0lBQ2hGLFlBQVk7SUFFWix3Q0FBd0M7SUFDMUIsNkJBQWtCLEdBQVcsb0JBQW9CLENBQUM7SUFDbEQsNkJBQWtCLEdBQVcsb0JBQW9CLENBQUM7SUFDbEQsNkJBQWtCLEdBQVcsb0JBQW9CLENBQUM7SUFDbEQsMkJBQWdCLEdBQVcsa0JBQWtCLENBQUM7SUFDOUMsZ0NBQXFCLEdBQVcsdUJBQXVCLENBQUM7SUFDdEUsWUFBWTtJQUVaLHVEQUF1RDtJQUN6Qyx5QkFBYyxHQUFHLFNBQVMsQ0FBQztJQUMzQix3QkFBYSxHQUFHLFFBQVEsQ0FBQztJQUN6QixnQ0FBcUIsR0FBRyxnQkFBZ0IsQ0FBQztJQUN6Qyx1QkFBWSxHQUFHLE9BQU8sQ0FBQztJQUN2Qix5QkFBYyxHQUFHLFNBQVMsQ0FBQztJQUMzQix1QkFBWSxHQUFHLE9BQU8sQ0FBQztJQUN2QixzQkFBVyxHQUFHLE1BQU0sQ0FBQyxDQUFDLFlBQVk7SUFFaEQsc0RBQXNEO0lBRXhDLCtCQUFvQixHQUFhO1FBQzdDLG9CQUFvQjtRQUNwQixxQkFBcUI7UUFDckIsd0JBQXdCO1FBQ3hCLDZCQUE2QjtRQUM3QiwyQkFBMkI7UUFDM0IsOEJBQThCO0tBQy9CLENBQUM7SUFFRiwyQ0FBMkM7SUFDN0Isd0JBQWEsR0FPckI7UUFDRixFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFO1FBQ3BGLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUU7UUFDdEYsd0RBQXdEO1FBQ3hELEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7UUFDcEYsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTtRQUN2Rix1REFBdUQ7UUFDdkQsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTtLQUNqRixDQUFDO0lBQ0osTUFBTTtJQUVOLDBDQUEwQztJQUM1Qix1QkFBWSxHQU9wQjtRQUNGLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7UUFDdkYsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTtRQUN4RixFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFO1FBQ3ZGLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUU7UUFDekY7WUFDRSxFQUFFLEVBQUUsQ0FBQztZQUNMLElBQUksRUFBRSxlQUFlO1lBQ3JCLE1BQU0sRUFBRSxHQUFHO1lBQ1gsS0FBSyxFQUFFLEdBQUc7WUFDVixJQUFJLEVBQUUsWUFBWTtZQUNsQixNQUFNLEVBQUUsS0FBSztTQUNkO0tBQ0YsQ0FBQztJQUNKLE1BQU07SUFFTiwwQ0FBMEM7SUFDNUIsdUJBQVksR0FPcEI7UUFDRjtZQUNFLEVBQUUsRUFBRSxDQUFDO1lBQ0wsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixNQUFNLEVBQUUsQ0FBQztZQUNULEtBQUssRUFBRSxHQUFHO1lBQ1YsSUFBSSxFQUFFLFNBQVM7WUFDZixNQUFNLEVBQUUsS0FBSztTQUNkO1FBQ0Q7WUFDRSxFQUFFLEVBQUUsQ0FBQztZQUNMLElBQUksRUFBRSxtQkFBbUI7WUFDekIsTUFBTSxFQUFFLENBQUM7WUFDVCxLQUFLLEVBQUUsR0FBRztZQUNWLElBQUksRUFBRSxXQUFXO1lBQ2pCLE1BQU0sRUFBRSxLQUFLO1NBQ2Q7UUFDRDtZQUNFLEVBQUUsRUFBRSxDQUFDO1lBQ0wsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixNQUFNLEVBQUUsQ0FBQztZQUNULEtBQUssRUFBRSxHQUFHO1lBQ1YsSUFBSSxFQUFFLFVBQVU7WUFDaEIsTUFBTSxFQUFFLEtBQUs7U0FDZDtRQUNEO1lBQ0UsRUFBRSxFQUFFLENBQUM7WUFDTCxJQUFJLEVBQUUsbUJBQW1CO1lBQ3pCLE1BQU0sRUFBRSxDQUFDO1lBQ1QsS0FBSyxFQUFFLEdBQUc7WUFDVixJQUFJLEVBQUUsVUFBVTtZQUNoQixNQUFNLEVBQUUsS0FBSztTQUNkO1FBQ0Q7WUFDRSxFQUFFLEVBQUUsQ0FBQztZQUNMLElBQUksRUFBRSxtQkFBbUI7WUFDekIsTUFBTSxFQUFFLEVBQUU7WUFDVixLQUFLLEVBQUUsR0FBRztZQUNWLElBQUksRUFBRSxZQUFZO1lBQ2xCLE1BQU0sRUFBRSxLQUFLO1NBQ2Q7UUFDRDtZQUNFLEVBQUUsRUFBRSxDQUFDO1lBQ0wsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixNQUFNLEVBQUUsQ0FBQztZQUNULEtBQUssRUFBRSxHQUFHO1lBQ1YsSUFBSSxFQUFFLFVBQVU7WUFDaEIsTUFBTSxFQUFFLEtBQUs7U0FDZDtRQUNEO1lBQ0UsRUFBRSxFQUFFLENBQUM7WUFDTCxJQUFJLEVBQUUsbUJBQW1CO1lBQ3pCLE1BQU0sRUFBRSxDQUFDO1lBQ1QsS0FBSyxFQUFFLEdBQUc7WUFDVixJQUFJLEVBQUUsY0FBYztZQUNwQixNQUFNLEVBQUUsS0FBSztTQUNkO1FBQ0Q7WUFDRSxFQUFFLEVBQUUsQ0FBQztZQUNMLElBQUksRUFBRSxtQkFBbUI7WUFDekIsTUFBTSxFQUFFLENBQUM7WUFDVCxLQUFLLEVBQUUsR0FBRztZQUNWLElBQUksRUFBRSxZQUFZO1lBQ2xCLE1BQU0sRUFBRSxLQUFLO1NBQ2Q7UUFDRDtZQUNFLEVBQUUsRUFBRSxDQUFDO1lBQ0wsSUFBSSxFQUFFLG1CQUFtQjtZQUN6QixNQUFNLEVBQUUsQ0FBQztZQUNULEtBQUssRUFBRSxHQUFHO1lBQ1YsSUFBSSxFQUFFLGNBQWM7WUFDcEIsTUFBTSxFQUFFLEtBQUs7U0FDZDtRQUNEO1lBQ0UsRUFBRSxFQUFFLENBQUM7WUFDTCxJQUFJLEVBQUUsb0JBQW9CO1lBQzFCLE1BQU0sRUFBRSxDQUFDO1lBQ1QsS0FBSyxFQUFFLEdBQUc7WUFDVixJQUFJLEVBQUUsUUFBUTtZQUNkLE1BQU0sRUFBRSxLQUFLO1NBQ2Q7S0FDRixDQUFDO0lBRU4saUJBQUM7Q0F4VEQsQUF3VEMsSUFBQTtrQkF4VG9CLFVBQVUiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCBjbGFzcyBHYW1lRGVmaW5lIHtcbiAgcHVibGljIHN0YXRpYyBTaWNibzogc3RyaW5nID0gXCJTaWNib1wiO1xuICBwdWJsaWMgc3RhdGljIFhvY0RpYTogc3RyaW5nID0gXCJYb2NEaWFcIjtcbiAgcHVibGljIHN0YXRpYyBCYXVDdWE6IHN0cmluZyA9IFwiQmF1Q3VhXCI7XG4gIHB1YmxpYyBzdGF0aWMgUGhvbTogc3RyaW5nID0gXCJQaG9tXCI7XG4gIHB1YmxpYyBzdGF0aWMgVGllbkxlbk1pZW5OYW06IHN0cmluZyA9IFwiVGllbkxlbk1pZW5OYW1cIjtcbiAgcHVibGljIHN0YXRpYyBNYXVCaW5oOiBzdHJpbmcgPSBcIk1hdUJpbmhcIjtcblxuICAvLyNyZWdpb24gPT09PT09PT0gT04vT0ZGID09PT09PT09XG4gIHB1YmxpYyBzdGF0aWMgQ0NfTE9HX0VOQUJMRTogYm9vbGVhbiA9IHRydWU7XG4gIC8vIFThuqFtIHPhu61hIMSR4bq7IHF1YXkgdmlkZW8geG9uZyBt4bufIGzhuqFpXG4gIHB1YmxpYyBzdGF0aWMgTE9HX1ZFUlNJT05fRU5BQkxFOiBib29sZWFuID0gZmFsc2U7IC8vIFVzZSBGb3IgRHJhd2NhbGwvVmVyc2lvblxuICBwdWJsaWMgc3RhdGljIElTX1BST0RfVkVSU0lPTjogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIC8vI3JlZ2lvbiA9PT09PT09PSBzY2VuZSA9PT09PT09PVxuICBwdWJsaWMgc3RhdGljIFNpY2JvU2NlbmU6IHN0cmluZyA9IFwiU2ljYm9TY2VuZVwiO1xuICBwdWJsaWMgc3RhdGljIFhvY0RpYVNjZW5lOiBzdHJpbmcgPSBcIlhvY0RpYVNjZW5lXCI7XG4gIHB1YmxpYyBzdGF0aWMgVGhhblRhaVNjZW5lOiBzdHJpbmcgPSBcIlRoYW5UYWlTY2VuZVwiO1xuICBwdWJsaWMgc3RhdGljIENoaWVuVGhhblNjZW5lOiBzdHJpbmcgPSBcIkNoaWVuVGhhblNjZW5lXCI7XG4gIHB1YmxpYyBzdGF0aWMgTG9EZVNjZW5lOiBzdHJpbmcgPSBcIkxvRGVTY2VuZVwiO1xuICBwdWJsaWMgc3RhdGljIFNwbGFzaEdhbWU6IHN0cmluZyA9IFwiU3BsYXNoU2NlbmVcIjtcbiAgcHVibGljIHN0YXRpYyBNYWluU2NlbmU6IHN0cmluZyA9IFwiTWFpblNjZW5lXCI7XG4gIHB1YmxpYyBzdGF0aWMgTG9iYnlUTE1OU2NlbmU6IHN0cmluZyA9IFwiVExNTkxvYmJ5U2NlbmVcIjtcbiAgcHVibGljIHN0YXRpYyBCYXVDdWFTY2VuZTogc3RyaW5nID0gXCJCYXVDdWFTY2VuZVwiO1xuICBwdWJsaWMgc3RhdGljIExvYWRpbmdDaGFuZ2VTY2VuZTogc3RyaW5nID0gXCJMb2FkaW5nQ2hhbmdlU2NlbmVcIjtcbiAgcHVibGljIHN0YXRpYyBNYXVCaW5oU2NlbmU6IHN0cmluZyA9IFwiTWF1QmluaExvYmJ5U2NlbmVcIjtcbiAgcHVibGljIHN0YXRpYyBQb2tlclNjZW5lOiBzdHJpbmcgPSBcIlBva2VyTG9iYnlTY2VuZVwiO1xuICBwdWJsaWMgc3RhdGljIFBob21TY2VuZTogc3RyaW5nID0gXCJQaG9tTG9iYnlTY2VuZVwiO1xuICBwdWJsaWMgc3RhdGljIExvYmJ5Q2FyZEdhbWU6IHN0cmluZyA9IFwiTG9iYnlDYXJkR2FtZVwiO1xuICBwdWJsaWMgc3RhdGljIEtCRERTY2VuZTogc3RyaW5nID0gXCJLQkREU2NlbmVcIjtcbiAgcHVibGljIHN0YXRpYyBLQkRETG9iYnlTY2VuZTogc3RyaW5nID0gXCJLQkRETG9iYnlTY2VuZVwiO1xuXG4gIC8vI2VuZHJlZ2lvblxuXG4gIC8vI3JlZ2lvbiA9PT09PT09PSBlbGVtZW50ID09PT09PT09XG4gIHB1YmxpYyBzdGF0aWMgUG9ydGFsQnVuZGxlOiBzdHJpbmcgPSBcIlBvcnRhbFJlc291cmNlc1wiO1xuICBwdWJsaWMgc3RhdGljIExvYmJ5Q2FyZEJ1bmRsZTogc3RyaW5nID0gXCJMb2JieUNhcmRCdW5kbGVcIjtcbiAgcHVibGljIHN0YXRpYyBQYXltZW50QnVuZGxlOiBzdHJpbmcgPSBcIlBheW1lbnRcIjtcbiAgLy8jZW5kcmVnaW9uXG5cbiAgLy8jcmVnaW9uID09PT09PT09IGVsZW1lbnQgPT09PT09PT1cbiAgcHVibGljIHN0YXRpYyBIZWFkZXJNYWluR2FtZUVsZW1lbnQ6IHN0cmluZyA9IFwiSGVhZGVyTWFpbkdhbWVFbGVtZW50XCI7XG4gIC8vI2VuZHJlZ2lvblxuXG4gIC8vI3JlZ2lvbiA9PT09PT09PSBwb3B1cCA9PT09PT09PVxuICBwdWJsaWMgc3RhdGljIFByb2ZpbGVQb3B1cDogc3RyaW5nID0gXCJQcm9maWxlUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBJbmZvR2FtZVBvcHVwOiBzdHJpbmcgPSBcIkluZm9HYW1lUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBOb3RpZnlQb3B1cDogc3RyaW5nID0gXCJOb3RpZnlQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIENoYXRQb3B1cDogc3RyaW5nID0gXCJDaGF0UG9wdXBOZXdcIjtcbiAgcHVibGljIHN0YXRpYyBDYXJkR2FtZUNoYXRQb3B1cDogc3RyaW5nID0gXCJDYXJkR2FtZUNoYXRQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFRhYmxlR2FtZUNoYXRQb3B1cDogc3RyaW5nID0gXCJUYWJsZUdhbWVDaGF0UG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBMb2dpblBvcHVwOiBzdHJpbmcgPSBcIkxvZ2luUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBDaGF0U2V0dGluZ1BvcHVwOiBzdHJpbmcgPSBcIkNoYXRTZXR0aW5nUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBDb25maXJtMkJ1dHRvblBvcHVwOiBzdHJpbmcgPSBcIkNvbmZpcm0yQnV0dG9uUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBDcmVhdGVUYWJsZVBvcHVwOiBzdHJpbmcgPSBcIkNyZWF0ZVRhYmxlUG9wdXBcIjtcblxuICBwdWJsaWMgc3RhdGljIExlYWRlckJvYXJkUG9wdXA6IHN0cmluZyA9IFwiTGVhZGVyQm9hcmRQb3B1cFwiO1xuICAvLyBwdWJsaWMgc3RhdGljIFBob21MZWFkZXJCb2FyZFBvcHVwOiBzdHJpbmcgPSBcIlBob21WaW5oRGFuaFBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgQWxsUGxheWVySW5UYWJsZVBvcHVwOiBzdHJpbmcgPSBcIkFsbFBsYXllckluVGFibGVQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFVzZXJQcm9maWxlUG9wdXA6IHN0cmluZyA9IFwiVXNlclByb2ZpbGVQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFVzZXJQcm9maWxlUG9wdXBOZXc6IHN0cmluZyA9IFwiVXNlclByb2ZpbGVQb3B1cE5ld1wiO1xuICBwdWJsaWMgc3RhdGljIFVwZGF0ZU5pY2tOYW1lUG9wdXA6IHN0cmluZyA9IFwiVXBkYXRlTmlja05hbWVQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIENoYW5nZUF2YXRhclBvcHVwOiBzdHJpbmcgPSBcIkNoYW5nZUF2YXRhclBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgU2V0dGluZ1BvcHVwOiBzdHJpbmcgPSBcIlNldHRpbmdQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFRoYW5UYWlTZXR0aW5nUG9wdXA6IHN0cmluZyA9IFwiVGhhblRhaVNldHRpbmdQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFBhc3N3b3JkUG9wdXA6IHN0cmluZyA9IFwiUGFzc3dvcmRQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFdhbGxldFBvcHVwOiBzdHJpbmcgPSBcIldhbGxldFBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgU2hhcmVUYWJsZVBvcHVwOiBzdHJpbmcgPSBcIlNoYXJlVGFibGVQb3B1cFwiO1xuXG4gIHB1YmxpYyBzdGF0aWMgQmV0SGlzdG9yeVBvcHVwOiBzdHJpbmcgPSBcIkJldEhpc3RvcnlQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIENoYW5nZUxvY2F0aW9uUG9wdXA6IHN0cmluZyA9IFwiQ2hhbmdlTG9jYXRpb25Qb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIENoYW5nZU1haWxQb3B1cDogc3RyaW5nID0gXCJDaGFuZ2VNYWlsUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBDaGFuZ2VQYXNzd29yZFBvcHVwOiBzdHJpbmcgPSBcIkNoYW5nZVBhc3N3b3JkUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBDaGFuZ2VQaG9uZVBvcHVwOiBzdHJpbmcgPSBcIkNoYW5nZVBob25lUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBMb2dvdXRQb3B1cDogc3RyaW5nID0gXCJMb2dvdXRQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIE1haW5TZXR0aW5nUG9wdXA6IHN0cmluZyA9IFwiTWFpblNldHRpbmdQb3B1cFYyXCI7XG4gIHB1YmxpYyBzdGF0aWMgVmVyaWZ5RW1haWxTdWNjZXNzUG9wdXA6IHN0cmluZyA9IFwiVmVyaWZ5RW1haWxTdWNjZXNzUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBHbG9iYWxJbnZpdGVTZXR0aW5nUG9wdXA6IHN0cmluZyA9IFwiR2xvYmFsSW52aXRlU2V0dGluZ1BvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgR2xvYmFsSW52aXRlTm90aWZ5OiBzdHJpbmcgPSBcIk5vdGlmeUludml0ZUdsb2JhbFwiO1xuICBwdWJsaWMgc3RhdGljIFBvcnRhbEhpc3RvcnlQb3B1cDogc3RyaW5nID0gXCJQb3J0YWxIaXN0b3J5UG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBQb3J0YWxWaXBHdWlkZVBvcHVwOiBzdHJpbmcgPSBcIlBvcnRhbFZpcEd1aWRlUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBQb3J0YWxNZXNzYWdlUG9wdXA6IHN0cmluZyA9IFwiUG9ydGFsTWVzc2FnZVBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgRGVsZXRlTWVzc2FnZVBvcHVwOiBzdHJpbmcgPSBcIkRlbGV0ZU1lc3NhZ2VQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFBvcnRhbFJlZmVycmFsUG9wdXA6IHN0cmluZyA9IFwiUG9ydGFsUmVmZXJyYWxQb3B1cFwiO1xuXG4gIHB1YmxpYyBzdGF0aWMgVExNTkJldEhpc3RvcnlQb3B1cDogc3RyaW5nID0gXCJUTE1OSGlzdG9yeVBvcHVwXCI7XG5cbiAgcHVibGljIHN0YXRpYyBQaG9tSGlzdG9yeVBvcHVwOiBzdHJpbmcgPSBcIlBob21IaXN0b3J5UG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBDYXJkR2FtZVNldHRpbmdQb3B1cDogc3RyaW5nID0gXCJDYXJkR2FtZVNldHRpbmdQb3B1cFwiO1xuXG4gIHB1YmxpYyBzdGF0aWMgTWF1QmluaEhpc3RvcnlQb3B1cDogc3RyaW5nID0gXCJNYXVCaW5oSGlzdG9yeVBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgQ2FyZEdhbWVJbmZvR2FtZVBvcHVwOiBzdHJpbmcgPSBcIkNhcmRHYW1lSW5mb0dhbWVQb3B1cFwiO1xuXG4gIHB1YmxpYyBzdGF0aWMgQ3VzdG9tQXZhdGFyUG9wdXA6IHN0cmluZyA9IFwiQ3VzdG9tQXZhdGFyUG9wdXBOZXdcIjtcbiAgcHVibGljIHN0YXRpYyBFdmVudFBvcHVwOiBzdHJpbmcgPSBcIkV2ZW50UG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBQb3J0YWxMZWFkZXJCb2FyZFBvcHVwOiBzdHJpbmcgPSBcIlBvcnRhbExlYWRlckJvYXJkUG9wdXBWMlwiO1xuICBwdWJsaWMgc3RhdGljIFRvcEh1QmlnUG9wdXA6IHN0cmluZyA9IFwiVG9wSHVCaWdQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIEdpZnRDb2RlSGlzdG9yeVBvcHVwOiBzdHJpbmcgPSBcIkdpZnRDb2RlSGlzdG9yeVBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgQW5pbWF0aW9uTGV2ZWxVcFBvcHVwOiBzdHJpbmcgPSBcIkFuaW1hdGlvbkxldmVsVXBQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFJld2FyZFBvcHVwOiBzdHJpbmcgPSBcIlJld2FyZFBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgQ29uZmlybVZhdWx0UG9wdXA6IHN0cmluZyA9IFwiQ29uZmlybVZhdWx0UG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBNaXNzaW9uR3VpZGVQb3B1cDogc3RyaW5nID0gXCJNaXNzaW9uR3VpZGVQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFRYTU5Qcm9tb3Rpb25Qb3B1cDogc3RyaW5nID0gXCJUWE1OUHJvbW90aW9uUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBUb3B1cFByb21vdGlvblBvcHVwOiBzdHJpbmcgPSBcIlRvcHVwUHJvbW90aW9uUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBDYXJkR2FtZVByb21vdGlvblBvcHVwOiBzdHJpbmcgPSBcIkNhcmRHYW1lUHJvbW90aW9uUG9wdXBcIlxuICBwdWJsaWMgc3RhdGljIEFGRkN1cEV2ZW50UHJvbW90aW9uUG9wdXA6IHN0cmluZyA9IFwiQUZGQ3VwRXZlbnRQcm9tb3Rpb25Qb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFNwcmluZ1JvdGF0aW9uUHJvbW90aW9uUG9wdXA6IHN0cmluZyA9IFwiU3ByaW5nUm90YXRpb25Qcm9tb3Rpb25Qb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIExvRGVHdWlkZVBvcHVwOiBzdHJpbmcgPSBcIkxvRGVHdWlkZVBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgTG9EZUxlYWRlckJvYXJkUG9wdXA6IHN0cmluZyA9IFwiTG9EZUxlYWRlckJvYXJkUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBMb0RlU3RhdGlzdGljUG9wdXA6IHN0cmluZyA9IFwiTG9EZVN0YXRpc3RpY1BvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgTG9EZUJldEhpc3RvcnlQb3B1cDogc3RyaW5nID0gXCJMb0RlQmV0SGlzdG9yeVBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgTG9EZUJldEhpc3RvcnlEZXRhaWxQb3B1cDogc3RyaW5nID0gXCJMb0RlQmV0SGlzdG9yeURldGFpbFBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgTGlzdENzQWNjb3VudFBvcHVwOiBzdHJpbmcgPSBcIkxpc3RDc0FjY291bnRQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIEV2ZW50R29pbmdQb3B1cDogc3RyaW5nID0gXCJFdmVudEdvaW5nUG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBHaWZ0Q29kZU5vdGlmeVBvcHVwOiBzdHJpbmcgPSBcIkdpZnRDb2RlTm90aWZ5UG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBHaWZ0Q29kZVBvcHVwOiBzdHJpbmcgPSBcIkdpZnRDb2RlUG9wdXBcIjtcblxuICAvLyNlbmRyZWdpb25cblxuICAvLyNyZWdpb24gV29ybGRDdXBFdmVudFxuICBwdWJsaWMgc3RhdGljIFBvcnRhbFdvcmxkQ3VwRXZlbnRQb3B1cDogc3RyaW5nID0gXCJQb3J0YWxXb3JsZEN1cEV2ZW50UG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBXb3JsZEN1cEV2ZW50UHJvbW90aW9uUG9wdXA6IHN0cmluZyA9IFwiV29ybGRDdXBFdmVudFByb21vdGlvblBvcHVwXCI7XG4gIHB1YmxpYyBzdGF0aWMgV29ybGRDdXBFdmVudFBvcHVwR3VpZGU6IHN0cmluZyA9IFwiV29ybGRDdXBFdmVudC9XQ0V2ZW50UG9wdXBHdWlkZVwiO1xuICBwdWJsaWMgc3RhdGljIFdvcmxkQ3VwRXZlbnRQb3B1cEhpc3Rvcnk6IHN0cmluZyA9IFwiV29ybGRDdXBFdmVudC9XQ0V2ZW50UG9wdXBIaXN0b3J5XCI7XG4gIHB1YmxpYyBzdGF0aWMgV29ybGRDdXBFdmVudFBvcHVwTGVhZGVyYm9hcmQ6IHN0cmluZyA9IFwiV29ybGRDdXBFdmVudC9XQ0V2ZW50UG9wdXBMZWFkZXJib2FyZFwiO1xuICAvLyNlbmRyZWdpb25cblxuICAvLyNyZWdpb24gQUZGQ3VwRXZlbnRcbiAgcHVibGljIHN0YXRpYyBQb3J0YWxBRkZDdXBFdmVudFBvcHVwOiBzdHJpbmcgPSBcIlBvcnRhbEFGRkN1cEV2ZW50UG9wdXBcIjtcbiAgcHVibGljIHN0YXRpYyBBRkZDdXBFdmVudFBvcHVwR3VpZGU6IHN0cmluZyA9IFwiQUZGQ3VwRXZlbnQvQUZGRXZlbnRQb3B1cEd1aWRlXCI7XG4gIHB1YmxpYyBzdGF0aWMgQUZGQ3VwRXZlbnRQb3B1cEhpc3Rvcnk6IHN0cmluZyA9IFwiQUZGQ3VwRXZlbnQvQUZGRXZlbnRQb3B1cEhpc3RvcnlcIjtcbiAgcHVibGljIHN0YXRpYyBBRkZDdXBFdmVudFBvcHVwTGVhZGVyYm9hcmQ6IHN0cmluZyA9IFwiQUZGQ3VwRXZlbnQvQUZGRXZlbnRQb3B1cExlYWRlcmJvYXJkXCI7XG4gIC8vI2VuZHJlZ2lvblxuXG5cbiAgLy8jcmVnaW9uIEFGRkN1cEV2ZW50XG4gIHB1YmxpYyBzdGF0aWMgUG9ydGFsU3ByaW5nUm90YXRpb25FdmVudFBvcHVwOiBzdHJpbmcgPSBcIlBvcnRhbFNwcmluZ1JvdGF0aW9uRXZlbnRQb3B1cFwiO1xuICBwdWJsaWMgc3RhdGljIFNwcmluZ1JvdGF0aW9uRXZlbnRQb3B1cEd1aWRlOiBzdHJpbmcgPSBcIlNwcmluZ1JvdGF0aW9uRXZlbnQvU3ByaW5nUm90YXRpb25FdmVudFBvcHVwR3VpZGVcIjtcbiAgcHVibGljIHN0YXRpYyBTcHJpbmdSb3RhdGlvbkV2ZW50UG9wdXBIaXN0b3J5OiBzdHJpbmcgPSBcIlNwcmluZ1JvdGF0aW9uRXZlbnQvU3ByaW5nUm90YXRpb25FdmVudFBvcHVwSGlzdG9yeVwiO1xuXG4gIC8vI2VuZHJlZ2lvblxuXG4gIC8vI3JlZ2lvbiA9PT09PT09PSBub3RpZnkgPT09PT09PT1cbiAgcHVibGljIHN0YXRpYyBUZXh0Tm90aWZ5RGlzY29ubmVjdDogc3RyaW5nID0gXCJUZXh0Tm90aWZ5RGlzY29ubmVjdFwiO1xuICBwdWJsaWMgc3RhdGljIFRleHROb3RpZnk6IHN0cmluZyA9IFwiVGV4dE5vdGlmeVwiO1xuICBwdWJsaWMgc3RhdGljIFRleHRCYXVDdWFOb3RpZnk6IHN0cmluZyA9IFwiVGV4dEJhdUN1YU5vdGlmeVwiO1xuICBwdWJsaWMgc3RhdGljIFRleHRTaWNib05vdGlmeTogc3RyaW5nID0gXCJUZXh0U2ljYm9Ob3RpZnlcIjtcbiAgcHVibGljIHN0YXRpYyBOb3RpZnlDbGFpbUF0dGFjaG1lbnRQb3B1cDogc3RyaW5nID0gXCJOb3RpZnlDbGFpbUF0dGFjaG1lbnRQb3B1cFwiO1xuICAvLyNlbmRyZWdpb25cblxuICAvLyNyZWdpb24gPT09PT09PT0gcG9wdXAgc3lzdGVtID09PT09PT09XG4gIHB1YmxpYyBzdGF0aWMgQ29uZmlybVBvcHVwU3lzdGVtOiBzdHJpbmcgPSBcIkNvbmZpcm1Qb3B1cFN5c3RlbVwiO1xuICBwdWJsaWMgc3RhdGljIExvYWRpbmdQb3B1cFN5c3RlbTogc3RyaW5nID0gXCJMb2FkaW5nUG9wdXBTeXN0ZW1cIjtcbiAgcHVibGljIHN0YXRpYyBXYXJuaW5nUG9wdXBTeXN0ZW06IHN0cmluZyA9IFwiV2FybmluZ1BvcHVwU3lzdGVtXCI7XG4gIHB1YmxpYyBzdGF0aWMgUmV0cnlQb3B1cFN5c3RlbTogc3RyaW5nID0gXCJSZXRyeVBvcHVwU3lzdGVtXCI7XG4gIHB1YmxpYyBzdGF0aWMgTWFpbnRlbmFjZVBvcHVwU3lzdGVtOiBzdHJpbmcgPSBcIk1haW50ZW5hY2VQb3B1cFN5c3RlbVwiO1xuICAvLyNlbmRyZWdpb25cblxuICAvLy8vI3JlZ2lvbiA9PT09PT09PU1pc3Npb25HYW1lTmFtZVBvcnRhbD09PT09PT09PT09PT09XG4gIHB1YmxpYyBzdGF0aWMgZ2VuZXJhbE1pc3Npb24gPSBcImdlbmVyYWxcIjtcbiAgcHVibGljIHN0YXRpYyBiYXVDdWFNaXNzaW9uID0gXCJiYXVjdWFcIjtcbiAgcHVibGljIHN0YXRpYyB0aWVuTGVuTWllbk5hbU1pc3Npb24gPSBcInRpZW5sZW5taWVubmFtXCI7XG4gIHB1YmxpYyBzdGF0aWMgc2ljQm9NaXNzaW9uID0gXCJzaWNib1wiO1xuICBwdWJsaWMgc3RhdGljIHRoYW5UYWlNaXNzaW9uID0gXCJ0aGFudGFpXCI7XG4gIHB1YmxpYyBzdGF0aWMgcG9rZXJNaXNzaW9uID0gXCJwb2tlclwiO1xuICBwdWJsaWMgc3RhdGljIHNsb3RNaXNzaW9uID0gXCJzbG90XCI7IC8vI2VuZHJlZ2lvblxuXG4gIC8vLy8jcmVnaW9uID09PT09PT09cHJvbW90aW9uUG9wdXBEZWZpbmU9PT09PT09PT09PT09PVxuXG4gIHB1YmxpYyBzdGF0aWMgcHJvbW90aW9uUG9wdXBEZWZpbmU6IHN0cmluZ1tdID0gW1xuICAgIFwiVFhNTlByb21vdGlvblBvcHVwXCIsXG4gICAgXCJUb3B1cFByb21vdGlvblBvcHVwXCIsXG4gICAgXCJDYXJkR2FtZVByb21vdGlvblBvcHVwXCIsXG4gICAgXCJXb3JsZEN1cEV2ZW50UHJvbW90aW9uUG9wdXBcIixcbiAgICBcIkFGRkN1cEV2ZW50UHJvbW90aW9uUG9wdXBcIixcbiAgICBcIlNwcmluZ1JvdGF0aW9uUHJvbW90aW9uUG9wdXBcIlxuICBdO1xuXG4gIC8vI3JlZ2lvbiA9PT09PT09PSBmaWdodCBkYXRhIHRlc3QgPT09PT09PT1cbiAgcHVibGljIHN0YXRpYyBmaWdodFRlc3REYXRhOiB7XG4gICAgaWQ6IG51bWJlcjtcbiAgICBpY29uOiBzdHJpbmc7XG4gICAgYW1vdW50OiBudW1iZXI7XG4gICAgcHJpY2U6IG51bWJlcjtcbiAgICBuYW1lOiBzdHJpbmc7XG4gICAgcm90YXRlOiBib29sZWFuO1xuICB9W10gPSBbXG4gICAgICB7IGlkOiAwLCBpY29uOiBcIml0ZW1fMDAxXzJcIiwgYW1vdW50OiA1LCBwcmljZTogMTAwLCBuYW1lOiBcIkNvbiBEw6FuXCIsIHJvdGF0ZTogZmFsc2UgfSxcbiAgICAgIHsgaWQ6IDEsIGljb246IFwiaXRlbV8wMDJfMlwiLCBhbW91bnQ6IDEwLCBwcmljZTogMjAwLCBuYW1lOiBcIlRy4bupbmcgR8OgXCIsIHJvdGF0ZTogZmFsc2UgfSxcbiAgICAgIC8veyBpZDogMiwgaWNvbjogXCJpdGVtXzAwM18yXCIsIGFtb3VudDogMTUsIHByaWNlOiAzMDAgfSxcbiAgICAgIHsgaWQ6IDMsIGljb246IFwiaXRlbV8wMDRfMlwiLCBhbW91bnQ6IDIwLCBwcmljZTogNDAwLCBuYW1lOiBcIkzhu7F1IMSQ4bqhblwiLCByb3RhdGU6IHRydWUgfSxcbiAgICAgIHsgaWQ6IDQsIGljb246IFwiaXRlbV8wMDVfMlwiLCBhbW91bnQ6IDI1LCBwcmljZTogNTAwLCBuYW1lOiBcIlRy4bupbmcgVGjDumlcIiwgcm90YXRlOiB0cnVlIH0sXG4gICAgICAvL3sgaWQ6IDUsIGljb246IFwiaXRlbV8wMDZfMlwiLCBhbW91bnQ6IDAsIHByaWNlOiA2MDAgfSxcbiAgICAgIHsgaWQ6IDYsIGljb246IFwiaXRlbV8wMDdfMlwiLCBhbW91bnQ6IDAsIHByaWNlOiA3MDAsIG5hbWU6IFwiQm9vbVwiLCByb3RhdGU6IHRydWUgfSxcbiAgICBdO1xuICAvLyNlbmRcblxuICAvLyNyZWdpb24gPT09PT09PT0gZ2lmdCBkYXRhIHRlc3QgPT09PT09PT1cbiAgcHVibGljIHN0YXRpYyBnaWZ0VGVzdERhdGE6IHtcbiAgICBpZDogbnVtYmVyO1xuICAgIGljb246IHN0cmluZztcbiAgICBhbW91bnQ6IG51bWJlcjtcbiAgICBwcmljZTogbnVtYmVyO1xuICAgIG5hbWU6IHN0cmluZztcbiAgICByb3RhdGU6IGJvb2xlYW47XG4gIH1bXSA9IFtcbiAgICAgIHsgaWQ6IDAsIGljb246IFwiZ2lmdGl0ZW1zXzAwMVwiLCBhbW91bnQ6IDAsIHByaWNlOiAxMDAsIG5hbWU6IFwiUXXhuqMgQsOzbmdcIiwgcm90YXRlOiB0cnVlIH0sXG4gICAgICB7IGlkOiAxLCBpY29uOiBcImdpZnRpdGVtc18wMDJcIiwgYW1vdW50OiAwLCBwcmljZTogMjAwLCBuYW1lOiBcIkLDoW5oIEtlbVwiLCByb3RhdGU6IGZhbHNlIH0sXG4gICAgICB7IGlkOiAyLCBpY29uOiBcImdpZnRpdGVtc18wMDNcIiwgYW1vdW50OiAwLCBwcmljZTogMzAwLCBuYW1lOiBcIkjhu5lwIFPhu61hXCIsIHJvdGF0ZTogZmFsc2UgfSxcbiAgICAgIHsgaWQ6IDMsIGljb246IFwiZ2lmdGl0ZW1zXzAwNFwiLCBhbW91bnQ6IDAsIHByaWNlOiA0MDAsIG5hbWU6IFwiVMOhY2ggQ2FmZVwiLCByb3RhdGU6IGZhbHNlIH0sXG4gICAgICB7XG4gICAgICAgIGlkOiA0LFxuICAgICAgICBpY29uOiBcImdpZnRpdGVtc18wMDVcIixcbiAgICAgICAgYW1vdW50OiA5OTksXG4gICAgICAgIHByaWNlOiA1MDAsXG4gICAgICAgIG5hbWU6IFwiMSDEkMOzYSBI4buTbmdcIixcbiAgICAgICAgcm90YXRlOiBmYWxzZSxcbiAgICAgIH0sXG4gICAgXTtcbiAgLy8jZW5kXG5cbiAgLy8jcmVnaW9uID09PT09PT09IGdpZnQgZGF0YSB0ZXN0ID09PT09PT09XG4gIHB1YmxpYyBzdGF0aWMgdGlwc1Rlc3REYXRhOiB7XG4gICAgaWQ6IG51bWJlcjtcbiAgICBpY29uOiBzdHJpbmc7XG4gICAgYW1vdW50OiBudW1iZXI7XG4gICAgcHJpY2U6IG51bWJlcjtcbiAgICBuYW1lOiBzdHJpbmc7XG4gICAgcm90YXRlOiBib29sZWFuO1xuICB9W10gPSBbXG4gICAgICB7XG4gICAgICAgIGlkOiAwLFxuICAgICAgICBpY29uOiBcImRlYWxlcml0ZW1zXzAwMV8yXCIsXG4gICAgICAgIGFtb3VudDogMCxcbiAgICAgICAgcHJpY2U6IDEwMCxcbiAgICAgICAgbmFtZTogXCJDb24gR+G6pXVcIixcbiAgICAgICAgcm90YXRlOiBmYWxzZSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGlkOiAxLFxuICAgICAgICBpY29uOiBcImRlYWxlcml0ZW1zXzAwMl8yXCIsXG4gICAgICAgIGFtb3VudDogMCxcbiAgICAgICAgcHJpY2U6IDIwMCxcbiAgICAgICAgbmFtZTogXCJUw6FjaCBDYWZlXCIsXG4gICAgICAgIHJvdGF0ZTogZmFsc2UsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpZDogMixcbiAgICAgICAgaWNvbjogXCJkZWFsZXJpdGVtc18wMDNfMlwiLFxuICAgICAgICBhbW91bnQ6IDAsXG4gICAgICAgIHByaWNlOiAzMDAsXG4gICAgICAgIG5hbWU6IFwiQ29ja3RhaWxcIixcbiAgICAgICAgcm90YXRlOiBmYWxzZSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGlkOiAzLFxuICAgICAgICBpY29uOiBcImRlYWxlcml0ZW1zXzAwNF8yXCIsXG4gICAgICAgIGFtb3VudDogMCxcbiAgICAgICAgcHJpY2U6IDQwMCxcbiAgICAgICAgbmFtZTogXCLEkMO0aSBHdeG7kWNcIixcbiAgICAgICAgcm90YXRlOiBmYWxzZSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGlkOiA0LFxuICAgICAgICBpY29uOiBcImRlYWxlcml0ZW1zXzAwNV8yXCIsXG4gICAgICAgIGFtb3VudDogMTAsXG4gICAgICAgIHByaWNlOiA1MDAsXG4gICAgICAgIG5hbWU6IFwiVGltIFBoYSBMw6pcIixcbiAgICAgICAgcm90YXRlOiBmYWxzZSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGlkOiA1LFxuICAgICAgICBpY29uOiBcImRlYWxlcml0ZW1zXzAwNl8yXCIsXG4gICAgICAgIGFtb3VudDogMCxcbiAgICAgICAgcHJpY2U6IDYwMCxcbiAgICAgICAgbmFtZTogXCJUaOG7j2kgU29uXCIsXG4gICAgICAgIHJvdGF0ZTogZmFsc2UsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpZDogNixcbiAgICAgICAgaWNvbjogXCJkZWFsZXJpdGVtc18wMDdfMlwiLFxuICAgICAgICBhbW91bnQ6IDAsXG4gICAgICAgIHByaWNlOiA3MDAsXG4gICAgICAgIG5hbWU6IFwiTHkgUsaw4bujdSBI4buTbmdcIixcbiAgICAgICAgcm90YXRlOiBmYWxzZSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGlkOiA3LFxuICAgICAgICBpY29uOiBcImRlYWxlcml0ZW1zXzAwOF8yXCIsXG4gICAgICAgIGFtb3VudDogMCxcbiAgICAgICAgcHJpY2U6IDcwMCxcbiAgICAgICAgbmFtZTogXCIxIMSQw7NhIEjhu5NuZ1wiLFxuICAgICAgICByb3RhdGU6IGZhbHNlLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWQ6IDgsXG4gICAgICAgIGljb246IFwiZGVhbGVyaXRlbXNfMDA5XzJcIixcbiAgICAgICAgYW1vdW50OiAwLFxuICAgICAgICBwcmljZTogNzAwLFxuICAgICAgICBuYW1lOiBcIkLhu6ljIFRoxrAgVMOsbmhcIixcbiAgICAgICAgcm90YXRlOiBmYWxzZSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGlkOiA5LFxuICAgICAgICBpY29uOiBcImRlYWxlcml0ZW1zXzAwMTBfMlwiLFxuICAgICAgICBhbW91bnQ6IDAsXG4gICAgICAgIHByaWNlOiA3MDAsXG4gICAgICAgIG5hbWU6IFwiTuG7pSBIw7RuXCIsXG4gICAgICAgIHJvdGF0ZTogZmFsc2UsXG4gICAgICB9LFxuICAgIF07XG4gIC8vI2VuZFxufVxuIl19