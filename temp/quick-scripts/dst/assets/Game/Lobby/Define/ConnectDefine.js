
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Define/ConnectDefine.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b99371yEmdJ8b3rgU1/QWYQ', 'ConnectDefine');
// Game/Lobby/Manager/ConnectDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ConnectDefine = /** @class */ (function () {
    function ConnectDefine() {
    }
    ConnectDefine.fbAppId = "";
    ConnectDefine.addressConfig = ["https://firebasestorage.googleapis.com/v0/b/gameeagles-66e75.appspot.com/o/config_stg_default.json?alt=media"];
    ConnectDefine.addressURL = "https://play.henxui.fun/";
    ConnectDefine.addressURLAndParameter = "https://bentau.henxui.fun";
    ConnectDefine.addressSanBay = "https://sanbay.henxui.fun";
    ConnectDefine.addressBenTau = "https://bentau.henxui.fun";
    ConnectDefine.addressBenTauWS = "wss://bentau.henxui.fun/gameloot.bentau.BenTau/WS";
    ConnectDefine.addressResources = "https://hose.henxui.fun";
    return ConnectDefine;
}());
exports.default = ConnectDefine;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L01hbmFnZXIvQ29ubmVjdERlZmluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0lBQUE7SUFTQSxDQUFDO0lBUmUscUJBQU8sR0FBRyxFQUFFLENBQUM7SUFDYiwyQkFBYSxHQUFhLENBQUMsOEdBQThHLENBQUMsQ0FBQztJQUMzSSx3QkFBVSxHQUFXLDBCQUEwQixDQUFDO0lBQ2hELG9DQUFzQixHQUFXLDJCQUEyQixDQUFDO0lBQzdELDJCQUFhLEdBQVcsMkJBQTJCLENBQUM7SUFDcEQsMkJBQWEsR0FBVywyQkFBMkIsQ0FBQztJQUNwRCw2QkFBZSxHQUFXLG1EQUFtRCxDQUFDO0lBQzlFLDhCQUFnQixHQUFXLHlCQUF5QixDQUFDO0lBQ3JFLG9CQUFDO0NBVEQsQUFTQyxJQUFBO2tCQVRvQixhQUFhIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29ubmVjdERlZmluZSB7XG4gIHB1YmxpYyBzdGF0aWMgZmJBcHBJZCA9IFwiXCI7XG4gIHB1YmxpYyBzdGF0aWMgYWRkcmVzc0NvbmZpZzogc3RyaW5nW10gPSBbXCJodHRwczovL2ZpcmViYXNlc3RvcmFnZS5nb29nbGVhcGlzLmNvbS92MC9iL2dhbWVlYWdsZXMtNjZlNzUuYXBwc3BvdC5jb20vby9jb25maWdfc3RnX2RlZmF1bHQuanNvbj9hbHQ9bWVkaWFcIl07XG4gIHB1YmxpYyBzdGF0aWMgYWRkcmVzc1VSTDogc3RyaW5nID0gXCJodHRwczovL3BsYXkuaGVueHVpLmZ1bi9cIjtcbiAgcHVibGljIHN0YXRpYyBhZGRyZXNzVVJMQW5kUGFyYW1ldGVyOiBzdHJpbmcgPSBcImh0dHBzOi8vYmVudGF1Lmhlbnh1aS5mdW5cIjtcbiAgcHVibGljIHN0YXRpYyBhZGRyZXNzU2FuQmF5OiBzdHJpbmcgPSBcImh0dHBzOi8vc2FuYmF5Lmhlbnh1aS5mdW5cIjtcbiAgcHVibGljIHN0YXRpYyBhZGRyZXNzQmVuVGF1OiBzdHJpbmcgPSBcImh0dHBzOi8vYmVudGF1Lmhlbnh1aS5mdW5cIjtcbiAgcHVibGljIHN0YXRpYyBhZGRyZXNzQmVuVGF1V1M6IHN0cmluZyA9IFwid3NzOi8vYmVudGF1Lmhlbnh1aS5mdW4vZ2FtZWxvb3QuYmVudGF1LkJlblRhdS9XU1wiO1xuICBwdWJsaWMgc3RhdGljIGFkZHJlc3NSZXNvdXJjZXM6IHN0cmluZyA9IFwiaHR0cHM6Ly9ob3NlLmhlbnh1aS5mdW5cIjtcbn1cbiJdfQ==