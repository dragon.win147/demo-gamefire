
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Utils/EnumGame.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '19421DZWcFOwo3CKvx1snzI', 'EnumGame');
// Portal/Common/Scripts/Enums/EnumGame.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeSocket = exports.SoundCard = exports.TypeChatHeo = exports.TypeLoadingChangeScene = exports.typeScene = exports.GameType = void 0;
var GameType;
(function (GameType) {
    GameType[GameType["LandingPage"] = 0] = "LandingPage";
    GameType[GameType["BauCua"] = 1] = "BauCua";
    GameType[GameType["TLMN"] = 2] = "TLMN";
    GameType[GameType["Sicbo"] = 3] = "Sicbo";
    GameType[GameType["TienLenMN"] = 4] = "TienLenMN";
    GameType[GameType["Tala"] = 6] = "Tala";
    GameType[GameType["Fishing"] = 7] = "Fishing";
    GameType[GameType["ThanTai"] = 8] = "ThanTai";
    GameType[GameType["Loading"] = 9] = "Loading";
    GameType[GameType["ChienThan"] = 10] = "ChienThan";
    GameType[GameType["MauBinh"] = 11] = "MauBinh";
    GameType[GameType["XocDia"] = 12] = "XocDia";
    GameType[GameType["LoDe"] = 13] = "LoDe";
    GameType[GameType["Poker"] = 14] = "Poker";
})(GameType = exports.GameType || (exports.GameType = {}));
var typeScene;
(function (typeScene) {
    typeScene[typeScene["release"] = 0] = "release";
    typeScene[typeScene["lobby"] = 1] = "lobby";
    typeScene[typeScene["loadingScene"] = 2] = "loadingScene";
    typeScene[typeScene["home"] = 3] = "home";
})(typeScene = exports.typeScene || (exports.typeScene = {}));
var TypeLoadingChangeScene;
(function (TypeLoadingChangeScene) {
    TypeLoadingChangeScene[TypeLoadingChangeScene["NORMAL"] = 0] = "NORMAL";
    TypeLoadingChangeScene[TypeLoadingChangeScene["SLOT"] = 1] = "SLOT";
    TypeLoadingChangeScene[TypeLoadingChangeScene["BET"] = 2] = "BET";
    TypeLoadingChangeScene[TypeLoadingChangeScene["CARD"] = 3] = "CARD";
})(TypeLoadingChangeScene = exports.TypeLoadingChangeScene || (exports.TypeLoadingChangeScene = {}));
var TypeChatHeo;
(function (TypeChatHeo) {
    TypeChatHeo[TypeChatHeo["NONE"] = -1] = "NONE";
    TypeChatHeo[TypeChatHeo["HEO"] = 0] = "HEO";
    TypeChatHeo[TypeChatHeo["CHAT_HEO"] = 1] = "CHAT_HEO";
    TypeChatHeo[TypeChatHeo["CHAT_DOI_HEO"] = 2] = "CHAT_DOI_HEO";
    TypeChatHeo[TypeChatHeo["DOI_HEO_DE"] = 3] = "DOI_HEO_DE";
    TypeChatHeo[TypeChatHeo["DE_DOI_HEO"] = 4] = "DE_DOI_HEO";
    TypeChatHeo[TypeChatHeo["DOI_HEO"] = 5] = "DOI_HEO";
    TypeChatHeo[TypeChatHeo["BA_HEO"] = 6] = "BA_HEO";
    TypeChatHeo[TypeChatHeo["BA_DOI_THONG"] = 7] = "BA_DOI_THONG";
    TypeChatHeo[TypeChatHeo["BA_DOI_THONG_DE"] = 8] = "BA_DOI_THONG_DE";
    TypeChatHeo[TypeChatHeo["DE_BA_DOI_THONG"] = 9] = "DE_BA_DOI_THONG";
    TypeChatHeo[TypeChatHeo["CHAT_BA_DOI_THONG"] = 10] = "CHAT_BA_DOI_THONG";
    TypeChatHeo[TypeChatHeo["TU_QUY"] = 11] = "TU_QUY";
    TypeChatHeo[TypeChatHeo["TU_QUY_DE"] = 12] = "TU_QUY_DE";
    TypeChatHeo[TypeChatHeo["DE_TU_QUY"] = 13] = "DE_TU_QUY";
    TypeChatHeo[TypeChatHeo["CHAT_TU_QUY"] = 14] = "CHAT_TU_QUY";
    TypeChatHeo[TypeChatHeo["BON_DOI_THONG"] = 15] = "BON_DOI_THONG";
    TypeChatHeo[TypeChatHeo["BON_DOI_THONG_DE"] = 16] = "BON_DOI_THONG_DE";
    TypeChatHeo[TypeChatHeo["DE_BON_DOI_THONG"] = 17] = "DE_BON_DOI_THONG";
})(TypeChatHeo = exports.TypeChatHeo || (exports.TypeChatHeo = {}));
var SoundCard;
(function (SoundCard) {
    SoundCard[SoundCard["DEAL"] = 0] = "DEAL";
    SoundCard[SoundCard["HIT"] = 1] = "HIT";
    SoundCard[SoundCard["CHAT"] = 2] = "CHAT";
    SoundCard[SoundCard["BI_CHAT"] = 3] = "BI_CHAT";
    SoundCard[SoundCard["WIN"] = 4] = "WIN";
    SoundCard[SoundCard["LOSE"] = 5] = "LOSE";
    SoundCard[SoundCard["REFUND"] = 6] = "REFUND";
    SoundCard[SoundCard["LOST_MONEY"] = 7] = "LOST_MONEY";
    SoundCard[SoundCard["MONEY_CUT_PIG"] = 8] = "MONEY_CUT_PIG";
    SoundCard[SoundCard["AUTO_WIN"] = 9] = "AUTO_WIN";
    SoundCard[SoundCard["AUTO_LOSE"] = 10] = "AUTO_LOSE";
})(SoundCard = exports.SoundCard || (exports.SoundCard = {}));
var CodeSocket;
(function (CodeSocket) {
    CodeSocket[CodeSocket["FORCE_CLOSE"] = 4999] = "FORCE_CLOSE";
    CodeSocket[CodeSocket["KOTIC"] = 1001] = "KOTIC";
    CodeSocket[CodeSocket["AUTO_CLOSE"] = 1000] = "AUTO_CLOSE";
    CodeSocket[CodeSocket["RETRY"] = 3006] = "RETRY";
})(CodeSocket = exports.CodeSocket || (exports.CodeSocket = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9Qb3J0YWwvQ29tbW9uL1NjcmlwdHMvRW51bXMvRW51bUdhbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBa0IsUUFlakI7QUFmRCxXQUFrQixRQUFRO0lBQ3hCLHFEQUFlLENBQUE7SUFDZiwyQ0FBVSxDQUFBO0lBQ1YsdUNBQVEsQ0FBQTtJQUNSLHlDQUFTLENBQUE7SUFDVCxpREFBYSxDQUFBO0lBQ2IsdUNBQVEsQ0FBQTtJQUNSLDZDQUFXLENBQUE7SUFDWCw2Q0FBVyxDQUFBO0lBQ1gsNkNBQVcsQ0FBQTtJQUNYLGtEQUFjLENBQUE7SUFDZCw4Q0FBWSxDQUFBO0lBQ1osNENBQVcsQ0FBQTtJQUNYLHdDQUFTLENBQUE7SUFDVCwwQ0FBVSxDQUFBO0FBQ1osQ0FBQyxFQWZpQixRQUFRLEdBQVIsZ0JBQVEsS0FBUixnQkFBUSxRQWV6QjtBQUNELElBQWtCLFNBS2pCO0FBTEQsV0FBa0IsU0FBUztJQUN6QiwrQ0FBVyxDQUFBO0lBQ1gsMkNBQVMsQ0FBQTtJQUNULHlEQUFnQixDQUFBO0lBQ2hCLHlDQUFRLENBQUE7QUFDVixDQUFDLEVBTGlCLFNBQVMsR0FBVCxpQkFBUyxLQUFULGlCQUFTLFFBSzFCO0FBQ0QsSUFBWSxzQkFLWDtBQUxELFdBQVksc0JBQXNCO0lBQ2hDLHVFQUFVLENBQUE7SUFDVixtRUFBUSxDQUFBO0lBQ1IsaUVBQU8sQ0FBQTtJQUNQLG1FQUFRLENBQUE7QUFDVixDQUFDLEVBTFcsc0JBQXNCLEdBQXRCLDhCQUFzQixLQUF0Qiw4QkFBc0IsUUFLakM7QUFFRCxJQUFZLFdBd0JYO0FBeEJELFdBQVksV0FBVztJQUNyQiw4Q0FBUyxDQUFBO0lBQ1QsMkNBQU8sQ0FBQTtJQUVQLHFEQUFZLENBQUE7SUFDWiw2REFBZ0IsQ0FBQTtJQUNoQix5REFBYyxDQUFBO0lBQ2QseURBQWMsQ0FBQTtJQUNkLG1EQUFXLENBQUE7SUFDWCxpREFBVSxDQUFBO0lBRVYsNkRBQWdCLENBQUE7SUFDaEIsbUVBQW1CLENBQUE7SUFDbkIsbUVBQW1CLENBQUE7SUFDbkIsd0VBQXNCLENBQUE7SUFFdEIsa0RBQVcsQ0FBQTtJQUNYLHdEQUFjLENBQUE7SUFDZCx3REFBYyxDQUFBO0lBQ2QsNERBQWdCLENBQUE7SUFFaEIsZ0VBQWtCLENBQUE7SUFDbEIsc0VBQXFCLENBQUE7SUFDckIsc0VBQXFCLENBQUE7QUFDdkIsQ0FBQyxFQXhCVyxXQUFXLEdBQVgsbUJBQVcsS0FBWCxtQkFBVyxRQXdCdEI7QUFFRCxJQUFZLFNBWVg7QUFaRCxXQUFZLFNBQVM7SUFDbkIseUNBQVEsQ0FBQTtJQUNSLHVDQUFPLENBQUE7SUFDUCx5Q0FBUSxDQUFBO0lBQ1IsK0NBQVcsQ0FBQTtJQUNYLHVDQUFPLENBQUE7SUFDUCx5Q0FBUSxDQUFBO0lBQ1IsNkNBQVUsQ0FBQTtJQUNWLHFEQUFjLENBQUE7SUFDZCwyREFBaUIsQ0FBQTtJQUNqQixpREFBWSxDQUFBO0lBQ1osb0RBQWMsQ0FBQTtBQUNoQixDQUFDLEVBWlcsU0FBUyxHQUFULGlCQUFTLEtBQVQsaUJBQVMsUUFZcEI7QUFFRCxJQUFZLFVBS1g7QUFMRCxXQUFZLFVBQVU7SUFDcEIsNERBQWtCLENBQUE7SUFDbEIsZ0RBQVksQ0FBQTtJQUNaLDBEQUFpQixDQUFBO0lBQ2pCLGdEQUFZLENBQUE7QUFDZCxDQUFDLEVBTFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFLckIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgZW51bSBHYW1lVHlwZSB7XG4gIExhbmRpbmdQYWdlID0gMCxcbiAgQmF1Q3VhID0gMSxcbiAgVExNTiA9IDIsXG4gIFNpY2JvID0gMyxcbiAgVGllbkxlbk1OID0gNCxcbiAgVGFsYSA9IDYsXG4gIEZpc2hpbmcgPSA3LFxuICBUaGFuVGFpID0gOCxcbiAgTG9hZGluZyA9IDksXG4gIENoaWVuVGhhbiA9IDEwLFxuICBNYXVCaW5oID0gMTEsXG4gIFhvY0RpYSA9IDEyLFxuICBMb0RlID0gMTMsXG4gIFBva2VyID0gMTQsXG59XG5leHBvcnQgY29uc3QgZW51bSB0eXBlU2NlbmUge1xuICByZWxlYXNlID0gMCxcbiAgbG9iYnkgPSAxLFxuICBsb2FkaW5nU2NlbmUgPSAyLFxuICBob21lID0gMyxcbn1cbmV4cG9ydCBlbnVtIFR5cGVMb2FkaW5nQ2hhbmdlU2NlbmUge1xuICBOT1JNQUwgPSAwLFxuICBTTE9UID0gMSxcbiAgQkVUID0gMixcbiAgQ0FSRCA9IDMsXG59XG5cbmV4cG9ydCBlbnVtIFR5cGVDaGF0SGVvIHtcbiAgTk9ORSA9IC0xLFxuICBIRU8gPSAwLFxuXG4gIENIQVRfSEVPID0gMSxcbiAgQ0hBVF9ET0lfSEVPID0gMixcbiAgRE9JX0hFT19ERSA9IDMsXG4gIERFX0RPSV9IRU8gPSA0LFxuICBET0lfSEVPID0gNSxcbiAgQkFfSEVPID0gNixcblxuICBCQV9ET0lfVEhPTkcgPSA3LFxuICBCQV9ET0lfVEhPTkdfREUgPSA4LFxuICBERV9CQV9ET0lfVEhPTkcgPSA5LFxuICBDSEFUX0JBX0RPSV9USE9ORyA9IDEwLFxuXG4gIFRVX1FVWSA9IDExLFxuICBUVV9RVVlfREUgPSAxMixcbiAgREVfVFVfUVVZID0gMTMsXG4gIENIQVRfVFVfUVVZID0gMTQsXG5cbiAgQk9OX0RPSV9USE9ORyA9IDE1LFxuICBCT05fRE9JX1RIT05HX0RFID0gMTYsXG4gIERFX0JPTl9ET0lfVEhPTkcgPSAxNyxcbn1cblxuZXhwb3J0IGVudW0gU291bmRDYXJkIHtcbiAgREVBTCA9IDAsXG4gIEhJVCA9IDEsXG4gIENIQVQgPSAyLFxuICBCSV9DSEFUID0gMyxcbiAgV0lOID0gNCxcbiAgTE9TRSA9IDUsXG4gIFJFRlVORCA9IDYsXG4gIExPU1RfTU9ORVkgPSA3LFxuICBNT05FWV9DVVRfUElHID0gOCxcbiAgQVVUT19XSU4gPSA5LFxuICBBVVRPX0xPU0UgPSAxMCxcbn1cblxuZXhwb3J0IGVudW0gQ29kZVNvY2tldCB7XG4gIEZPUkNFX0NMT1NFID0gNDk5OSxcbiAgS09USUMgPSAxMDAxLFxuICBBVVRPX0NMT1NFID0gMTAwMCxcbiAgUkVUUlkgPSAzMDA2LFxufVxuIl19