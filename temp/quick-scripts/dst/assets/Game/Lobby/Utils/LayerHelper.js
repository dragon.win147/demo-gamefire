
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Utils/LayerHelper.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'dccc2miBSBC7YcT+QCUEci0', 'LayerHelper');
// Portal/Common/Scripts/Helpers/LayerHelper.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LayerHelper = /** @class */ (function () {
    function LayerHelper() {
    }
    LayerHelper.miniGameManager = 1;
    LayerHelper.uiManager = 2;
    LayerHelper.cheatManager = 3;
    LayerHelper.netWorkManager = 4;
    return LayerHelper;
}());
exports.default = LayerHelper;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9Qb3J0YWwvQ29tbW9uL1NjcmlwdHMvSGVscGVycy9MYXllckhlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUNBO0lBQUE7SUFLQSxDQUFDO0lBSlEsMkJBQWUsR0FBVyxDQUFDLENBQUM7SUFDNUIscUJBQVMsR0FBVyxDQUFDLENBQUM7SUFDdEIsd0JBQVksR0FBVyxDQUFDLENBQUM7SUFDekIsMEJBQWMsR0FBVyxDQUFDLENBQUM7SUFDcEMsa0JBQUM7Q0FMRCxBQUtDLElBQUE7a0JBTG9CLFdBQVciLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIExheWVySGVscGVyIHtcbiAgc3RhdGljIG1pbmlHYW1lTWFuYWdlcjogbnVtYmVyID0gMTtcbiAgc3RhdGljIHVpTWFuYWdlcjogbnVtYmVyID0gMjtcbiAgc3RhdGljIGNoZWF0TWFuYWdlcjogbnVtYmVyID0gMztcbiAgc3RhdGljIG5ldFdvcmtNYW5hZ2VyOiBudW1iZXIgPSA0O1xufVxuIl19