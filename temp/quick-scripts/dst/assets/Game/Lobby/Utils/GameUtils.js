
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Utils/GameUtils.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b661ahVspxAsrdgxc1sIhaf', 'GameUtils');
// Game/Lobby/Utils/GameUtils.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Badge = void 0;
var EnumGame_1 = require("../Utils/EnumGame");
var GameDefine_1 = require("../Define/GameDefine");
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var Badge;
(function (Badge) {
    Badge["gold"] = "_gold";
    Badge["platinum"] = "_platinum";
    Badge["diamond"] = "_diamond";
    Badge["ruby"] = "_ruby";
    Badge["emerald"] = "_emerald";
})(Badge = exports.Badge || (exports.Badge = {}));
var GameUtils = /** @class */ (function () {
    function GameUtils() {
    }
    GameUtils.createClickBtnEvent = function (node, componentName, handlerEvent, custom) {
        if (custom === void 0) { custom = ""; }
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = node; //This node is the node to which your event handler code component belongs
        clickEventHandler.component = componentName; //This is the code file name
        clickEventHandler.handler = handlerEvent;
        clickEventHandler.customEventData = custom;
        return clickEventHandler;
    };
    GameUtils.getUrlParameter = function (name, url) {
        name = name.replace("/[[]/", "\\[").replace("/[]]/", "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        var results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    GameUtils.removeParameterUrl = function (sParam, sourceURL) {
        var rtn = sourceURL.split("?")[0], param, params_arr = [], queryString = sourceURL.indexOf("?") !== -1 ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === sParam) {
                    params_arr.splice(i, 1);
                }
            }
            if (params_arr.length)
                rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    };
    GameUtils.removeAllParameterUrl = function (sourceURL) {
        var rtn = sourceURL.split("?")[0], param, params_arr = [], queryString = sourceURL.indexOf("?") !== -1 ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                // if (param === sParam) {
                params_arr.splice(i, 1);
                // }
            }
            if (params_arr.length)
                rtn = rtn + "?" + params_arr.join("&");
        }
        window.history.replaceState(null, null, rtn);
        // return rtn;
    };
    GameUtils.convertToOtherNode = function (fromNode, targetNode, pos) {
        if (pos === void 0) { pos = null; }
        var parent = fromNode;
        if (fromNode.parent)
            parent = fromNode.parent;
        var worldSpace = parent.convertToWorldSpaceAR(fromNode.position);
        if (pos) {
            worldSpace = fromNode.convertToWorldSpaceAR(pos);
        }
        return targetNode.convertToNodeSpaceAR(worldSpace);
    };
    GameUtils.convertToOtherNode2 = function (fromNode, targetNode, pos) {
        if (pos === void 0) { pos = null; }
        var parent = fromNode;
        if (fromNode.parent)
            parent = fromNode.parent;
        var worldSpace = parent.convertToWorldSpaceAR(fromNode.getPosition());
        if (pos) {
            worldSpace = fromNode.convertToWorldSpaceAR(pos);
        }
        return targetNode.convertToNodeSpaceAR(worldSpace);
    };
    GameUtils.rectContainsPoint = function (rect, point) {
        var result = point.x >= rect.origin.x && point.x <= rect.origin.x + rect.size.width && point.y >= rect.origin.y && point.y <= rect.origin.y + rect.size.height;
        return result;
    };
    GameUtils.createItemFromNode = function (item, node, parentNode, insert) {
        if (parentNode === void 0) { parentNode = null; }
        if (insert === void 0) { insert = false; }
        if (!node)
            return null;
        var parent = parentNode ? parentNode : node.parent;
        var newNode = cc.instantiate(node);
        if (insert) {
            parent.insertChild(newNode, 0);
        }
        else {
            parent.addChild(newNode);
        }
        var newItem = newNode.getComponent(item);
        newItem.node.active = true;
        node.active = false;
        return newItem;
    };
    GameUtils.cloneItemFromNode = function (item, node) {
        if (!node)
            return null;
        var newNode = cc.instantiate(node);
        var newItem = newNode.getComponent(item);
        newItem.node.active = true;
        node.active = false;
        return newItem;
    };
    GameUtils.createItemFromPrefab = function (item, prefab, parentNode) {
        if (!prefab)
            return null;
        var node = cc.instantiate(prefab);
        parentNode.addChild(node);
        var newItem = node.getComponent(item);
        newItem.node.active = true;
        return newItem;
    };
    GameUtils.createNodeFromPrefab = function (prefab, parentNode) {
        if (!prefab)
            return null;
        var node = cc.instantiate(prefab);
        parentNode.addChild(node);
        return node;
    };
    GameUtils.formatDate = function (date, format) {
        format = format.replace("%Y", date.getFullYear().toString());
        var month = date.getMonth() + 1;
        format = format.replace("%M", month.toString().length < 2 ? "0" + month.toString() : month.toString());
        format = format.replace("%d", date.getDate().toString().length < 2 ? "0" + date.getDate().toString() : date.getDate().toString());
        format = format.replace("%h", date.getHours().toString().length < 2 ? "0" + date.getHours().toString() : date.getHours().toString());
        format = format.replace("%m", date.getMinutes().toString().length < 2 ? "0" + date.getMinutes().toString() : date.getMinutes().toString());
        format = format.replace("%s", date.getSeconds().toString().length < 2 ? "0" + date.getSeconds().toString() : date.getSeconds().toString());
        return format;
    };
    GameUtils.formatMoreDate = function (startDate, endDate, format) {
        format = format.replace("%Y1", startDate.getFullYear().toString());
        var monthFrom = startDate.getMonth() + 1;
        format = format.replace("%M1", monthFrom.toString().length < 2 ? "0" + monthFrom.toString() : monthFrom.toString());
        format = format.replace("%d1", startDate.getDate().toString().length < 2 ? "0" + startDate.getDate().toString() : startDate.getDate().toString());
        format = format.replace("%h1", startDate.getHours().toString().length < 2 ? "0" + startDate.getHours().toString() : startDate.getHours().toString());
        format = format.replace("%m1", startDate.getMinutes().toString().length < 2 ? "0" + startDate.getMinutes().toString() : startDate.getMinutes().toString());
        // end date
        format = format.replace("%Y2", endDate.getFullYear().toString());
        var monthTo = startDate.getMonth() + 1;
        format = format.replace("%M2", monthTo.toString().length < 2 ? "0" + monthTo.toString() : monthTo.toString());
        format = format.replace("%d2", endDate.getDate().toString().length < 2 ? "0" + endDate.getDate().toString() : endDate.getDate().toString());
        format = format.replace("%h2", endDate.getHours().toString().length < 2 ? "0" + endDate.getHours().toString() : endDate.getHours().toString());
        format = format.replace("%m2", endDate.getMinutes().toString().length < 2 ? "0" + endDate.getMinutes().toString() : endDate.getMinutes().toString());
        return format;
    };
    GameUtils.formatMoneyNumberMyUser = function (money) {
        var s = "$ " + this.numberWithCommas(money);
        return s;
    };
    GameUtils.formatMoneyNumberMyUserNotIcon = function (money) {
        return this.numberWithCommas(money);
    };
    GameUtils.formatMoneyNumberUser = function (money) {
        var s = "$ " + this.formatMoneyNumber(money);
        return s;
    };
    GameUtils.formatMoneyNumber = function (money) {
        var sign = 1;
        var value = money;
        if (money < 0) {
            sign = -1;
            value = value * -1;
        }
        var format = "";
        if (value >= 1000000000.0) {
            value /= 1000000000.0;
            format = "B";
        }
        else if (value >= 1000000.0) {
            value /= 1000000.0;
            format = "M";
        }
        else if (value >= 1000.0) {
            value /= 1000.0;
            format = "K";
        }
        value = (Math.floor(value * 100 + 0.00000001) / 100) * sign;
        return value + format;
    };
    GameUtils.formatMoneyNumber_v2 = function (money) {
        var sign = 1;
        var value = money;
        if (money < 0) {
            sign = -1;
            value = value * -1;
        }
        var format = "";
        if (value >= 1000000000.0) {
            value /= 1000000000.0;
            format = " Tỉ";
        }
        else if (value >= 1000000.0) {
            value /= 1000000.0;
            format = " Triệu";
        }
        else if (value >= 1000.0) {
            value /= 1000.0;
            format = " Ngàn";
        }
        value = (Math.floor(value * 100 + 0.00000001) / 100) * sign;
        return value + format;
    };
    GameUtils.numberWithCommasMoney = function (number) {
        var s = "$ " + this.numberWithCommas(number);
        return s;
    };
    GameUtils.numberWithCommas = function (number) {
        if (number) {
            var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
            result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return result[1] !== "00" ? result.join(",") : result[0];
        }
        return "0";
    };
    GameUtils.numberWithCommasV2 = function (number) {
        if (number) {
            var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
            result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return result[1] !== "00" ? result.join(",") : result[0];
        }
        return "0";
    };
    GameUtils.numberWithDot = function (number) {
        if (number) {
            var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
            result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return result[1] !== "00" ? result.join(",") : result[0];
        }
        return "0";
    };
    GameUtils.formatMoneyWithRange = function (money, shortenStart) {
        if (shortenStart === void 0) { shortenStart = 100000000; }
        var rs = "";
        if (money >= shortenStart)
            rs = GameUtils.formatMoneyNumber(money);
        else
            rs = GameUtils.numberWithCommas(money);
        return rs;
    };
    GameUtils.roundNumber = function (num, roundMin) {
        if (roundMin === void 0) { roundMin = 1000; }
        var temp = parseInt((num / roundMin).toString());
        if (num % roundMin != 0)
            temp += 1;
        return temp * roundMin;
    };
    GameUtils.getRandomInt = function (max, min) {
        if (min === void 0) { min = 0; }
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };
    GameUtils.randomInsideCircle = function (R) {
        if (R === void 0) { R = 1; }
        var r = R * Math.sqrt(Math.random());
        var theta = Math.random() * 2 * Math.PI;
        var x = r * Math.cos(theta);
        var y = r * Math.sin(theta);
        return new cc.Vec2(x, y);
    };
    GameUtils.getRandomFloat = function (max) {
        return Math.random() * max;
    };
    GameUtils.playAnimOnce = function (skeleton, toAnimation, backAnimation, onCompleted) {
        if (onCompleted === void 0) { onCompleted = null; }
        if (skeleton == null || skeleton == undefined)
            return;
        // if (skeleton.getCurrent(0) != null && skeleton.getCurrent(0).animation.name == toAnimation) {
        //   return;
        // }
        skeleton.setCompleteListener(function (trackEntry) {
            if (backAnimation != "" && backAnimation != null)
                skeleton.setAnimation(0, backAnimation, true);
            if (trackEntry.animation && trackEntry.animation.name == toAnimation)
                onCompleted && onCompleted();
        });
        skeleton.setAnimation(0, toAnimation, false);
    };
    GameUtils.sum = function (array) {
        if (array.length <= 0)
            return 0;
        return array.reduce(function (total, val) { return total + val; });
    };
    GameUtils.setContentLabelAutoSize = function (label, content) {
        if (content.length > 30) {
            label.node.width = (30 * label.fontSize) / 2;
            label.overflow = cc.Label.Overflow.RESIZE_HEIGHT;
        }
        else {
            label.overflow = cc.Label.Overflow.NONE;
        }
        label.string = content;
    };
    GameUtils.FormatString = function (str) {
        var val = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            val[_i - 1] = arguments[_i];
        }
        for (var index = 0; index < val.length; index++) {
            str = str.replace("{" + index + "}", val[index].toString());
        }
        return str;
    };
    GameUtils.setPlayEffect = function (nodeEff) {
        if (!nodeEff)
            return;
        nodeEff.getComponentsInChildren(cc.ParticleSystem3D).forEach(function (x) {
            x.stop();
            x.play();
        });
        nodeEff.getComponentsInChildren(cc.Animation).forEach(function (x) {
            x.stop();
            x.play();
        });
        nodeEff.getComponentsInChildren(sp.Skeleton).forEach(function (x) {
            x.setAnimation(0, x.defaultAnimation, x.loop);
        });
    };
    GameUtils.setParentTemp = function (nameTempNode, node, parentOfTemp, zIndex) {
        if (nameTempNode === void 0) { nameTempNode = "Temp"; }
        if (zIndex === void 0) { zIndex = 90; }
        var temp = parentOfTemp.getChildByName(nameTempNode);
        if (!temp) {
            temp = new cc.Node(nameTempNode);
            parentOfTemp.addChild(temp);
            temp.zIndex = zIndex;
            parentOfTemp.sortAllChildren();
        }
        var widget = node.getComponent(cc.Widget);
        if (widget)
            widget.enabled = false;
        var startPos = GameUtils.convertToOtherNode(node.parent, temp, node.position);
        node.setParent(temp);
        node.position = startPos;
        node.zIndex = 0;
    };
    GameUtils.getParentTemp = function (nameTempNode, parentOfTemp, zIndex) {
        if (nameTempNode === void 0) { nameTempNode = "Temp"; }
        if (zIndex === void 0) { zIndex = 80; }
        var temp = parentOfTemp.getChildByName(nameTempNode);
        if (!temp) {
            temp = new cc.Node(nameTempNode);
            parentOfTemp.addChild(temp);
            temp.zIndex = zIndex;
        }
        return temp;
    };
    GameUtils.getTimeSince = function (timeStamp) {
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;
        var elapsed = Date.now() - timeStamp.toDate().getTime();
        cc.log("elapsed", elapsed);
        if (elapsed < msPerMinute) {
            return Math.round(elapsed / 1000) + " giây trước";
        }
        else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + " phút trước";
        }
        else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + " giờ trước";
        }
        else if (elapsed < msPerMonth) {
            return "khoảng " + Math.round(elapsed / msPerDay) + " ngày trước";
        }
        else if (elapsed < msPerYear) {
            return "khoảng " + Math.round(elapsed / msPerMonth) + " tháng trước";
        }
        else {
            return "khoảng " + Math.round(elapsed / msPerYear) + " năm trước";
        }
    };
    GameUtils.getKeyByValue = function (object, value) {
        var key = undefined;
        object.forEach(function (v, k) {
            if (value == v) {
                key = k;
            }
        });
        return key;
    };
    GameUtils.isPlatformIOS = function () {
        return cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD;
    };
    GameUtils.isBrowser = function () {
        return cc.sys.isBrowser;
    };
    GameUtils.isNative = function () {
        return cc.sys.isNative;
    };
    GameUtils.isAndroid = function () {
        return cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID;
    };
    GameUtils.isIOS = function () {
        return cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS;
    };
    GameUtils.isLogVersion = function () {
        return GameDefine_1.default.LOG_VERSION_ENABLE;
    };
    GameUtils.isNullOrUndefined = function (object) {
        return object == null || object == undefined;
    };
    GameUtils.convertLotteryNumberToString = function (lotteryNumber) {
        var lotteryNumberString = lotteryNumber.toString();
        var remainZeroNumber = 6 - lotteryNumberString.length;
        for (var i = 0; i < remainZeroNumber; i++) {
            lotteryNumberString = "0" + lotteryNumberString;
        }
        return lotteryNumberString;
    };
    GameUtils.isWebMobile = function () {
        return cc.sys.isBrowser && cc.sys.isMobile;
    };
    GameUtils.getDeviceId = function () {
        var id = cc.sys.localStorage.getItem(GameUtils.deviceIdKey);
        if (id == null || id == "") {
            var rawId = cc.sys.os +
                cc.sys.osMainVersion +
                cc.sys.language +
                cc.sys.languageCode +
                cc.sys.osVersion +
                cc.sys.isNative +
                cc.sys.windowPixelResolution.height +
                cc.sys.platform +
                cc.sys.windowPixelResolution.width +
                cc.sys.isMobile +
                cc.sys.now();
            id = btoa(rawId);
            cc.sys.localStorage.setItem(GameUtils.deviceIdKey, id);
        }
        GameUtils.deviceID = id;
    };
    GameUtils.getPlatformName = function () {
        if (GameUtils.isWebMobile()) {
            return "WEBM";
        }
        else if (cc.sys.isBrowser) {
            return "WEBPC";
        }
        else if (cc.sys.os == cc.sys.OS_ANDROID) {
            return "ANDROID";
        }
        else if (cc.sys.os == cc.sys.OS_IOS) {
            return "IOS";
        }
        else
            return "UNKNOWN";
    };
    GameUtils.qrToString = function (qrcode) {
        var rs = "";
        for (var row = 0; row < qrcode.getModuleCount(); row++) {
            for (var col = 0; col < qrcode.getModuleCount(); col++) {
                rs = rs + (qrcode.isDark(row, col) ? "1" : "0");
            }
        }
        return rs;
    };
    GameUtils.IS_DEV = false;
    GameUtils.count = 0;
    GameUtils.gameType = EnumGame_1.GameType.BauCua;
    GameUtils.gameTopic = topic_pb_1.Topic.TINA;
    GameUtils.isStatusPing = true;
    GameUtils.isMainScene = true;
    GameUtils.isFirstLoadGame = true;
    GameUtils.deviceName = "";
    GameUtils.deviceID = "";
    GameUtils.utmSource = "";
    GameUtils.deviceIdKey = "DEVICE_ID";
    return GameUtils;
}());
exports.default = GameUtils;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L1V0aWxzL0dhbWVVdGlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSw4Q0FBd0Q7QUFFeEQsbURBQThDO0FBQzlDLHFEQUFpRDtBQUtqRCxJQUFZLEtBTVg7QUFORCxXQUFZLEtBQUs7SUFDZix1QkFBYyxDQUFBO0lBQ2QsK0JBQXNCLENBQUE7SUFDdEIsNkJBQW9CLENBQUE7SUFDcEIsdUJBQWMsQ0FBQTtJQUNkLDZCQUFvQixDQUFBO0FBQ3RCLENBQUMsRUFOVyxLQUFLLEdBQUwsYUFBSyxLQUFMLGFBQUssUUFNaEI7QUFDRDtJQUFBO0lBa2VBLENBQUM7SUFyZGUsNkJBQW1CLEdBQWpDLFVBQWtDLElBQWEsRUFBRSxhQUFxQixFQUFFLFlBQW9CLEVBQUUsTUFBbUI7UUFBbkIsdUJBQUEsRUFBQSxXQUFtQjtRQUMvRyxJQUFNLGlCQUFpQixHQUFHLElBQUksRUFBRSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUMxRCxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUMsMEVBQTBFO1FBQzNHLGlCQUFpQixDQUFDLFNBQVMsR0FBRyxhQUFhLENBQUMsQ0FBQyw0QkFBNEI7UUFDekUsaUJBQWlCLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQztRQUN6QyxpQkFBaUIsQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDO1FBQzNDLE9BQU8saUJBQWlCLENBQUM7SUFDM0IsQ0FBQztJQUVhLHlCQUFlLEdBQTdCLFVBQThCLElBQUksRUFBRSxHQUFHO1FBQ3JDLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzVELElBQUksS0FBSyxHQUFHLElBQUksTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLEdBQUcsV0FBVyxDQUFDLENBQUM7UUFDdEQsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM5QixPQUFPLE9BQU8sS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUNwRixDQUFDO0lBRWEsNEJBQWtCLEdBQWhDLFVBQWlDLE1BQU0sRUFBRSxTQUFTO1FBQ2hELElBQUksR0FBRyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQy9CLEtBQUssRUFDTCxVQUFVLEdBQUcsRUFBRSxFQUNmLFdBQVcsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDN0UsSUFBSSxXQUFXLEtBQUssRUFBRSxFQUFFO1lBQ3RCLFVBQVUsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BDLEtBQUssSUFBSSxDQUFDLEdBQUcsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNsRCxLQUFLLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEMsSUFBSSxLQUFLLEtBQUssTUFBTSxFQUFFO29CQUNwQixVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztpQkFDekI7YUFDRjtZQUNELElBQUksVUFBVSxDQUFDLE1BQU07Z0JBQUUsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUMvRDtRQUNELE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQUVhLCtCQUFxQixHQUFuQyxVQUFvQyxTQUFTO1FBQzNDLElBQUksR0FBRyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQy9CLEtBQUssRUFDTCxVQUFVLEdBQUcsRUFBRSxFQUNmLFdBQVcsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDN0UsSUFBSSxXQUFXLEtBQUssRUFBRSxFQUFFO1lBQ3RCLFVBQVUsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BDLEtBQUssSUFBSSxDQUFDLEdBQUcsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNsRCxLQUFLLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEMsMEJBQTBCO2dCQUMxQixVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEIsSUFBSTthQUNMO1lBQ0QsSUFBSSxVQUFVLENBQUMsTUFBTTtnQkFBRSxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQy9EO1FBQ0QsTUFBTSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM3QyxjQUFjO0lBQ2hCLENBQUM7SUFFYSw0QkFBa0IsR0FBaEMsVUFBaUMsUUFBaUIsRUFBRSxVQUFtQixFQUFFLEdBQW1CO1FBQW5CLG9CQUFBLEVBQUEsVUFBbUI7UUFDMUYsSUFBSSxNQUFNLEdBQUcsUUFBUSxDQUFDO1FBQ3RCLElBQUksUUFBUSxDQUFDLE1BQU07WUFBRSxNQUFNLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQztRQUM5QyxJQUFJLFVBQVUsR0FBRyxNQUFNLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2pFLElBQUksR0FBRyxFQUFFO1lBQ1AsVUFBVSxHQUFHLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNsRDtRQUNELE9BQU8sVUFBVSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFYSw2QkFBbUIsR0FBakMsVUFBa0MsUUFBaUIsRUFBRSxVQUFtQixFQUFFLEdBQW1CO1FBQW5CLG9CQUFBLEVBQUEsVUFBbUI7UUFDM0YsSUFBSSxNQUFNLEdBQUcsUUFBUSxDQUFDO1FBQ3RCLElBQUksUUFBUSxDQUFDLE1BQU07WUFBRSxNQUFNLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQztRQUM5QyxJQUFJLFVBQVUsR0FBRyxNQUFNLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7UUFDdEUsSUFBSSxHQUFHLEVBQUU7WUFDUCxVQUFVLEdBQUcsUUFBUSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ2xEO1FBQ0QsT0FBTyxVQUFVLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVhLDJCQUFpQixHQUEvQixVQUFnQyxJQUFhLEVBQUUsS0FBYztRQUMzRCxJQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2pLLE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFYSw0QkFBa0IsR0FBaEMsVUFBb0MsSUFBaUIsRUFBRSxJQUFhLEVBQUUsVUFBMEIsRUFBRSxNQUF1QjtRQUFuRCwyQkFBQSxFQUFBLGlCQUEwQjtRQUFFLHVCQUFBLEVBQUEsY0FBdUI7UUFDdkgsSUFBSSxDQUFDLElBQUk7WUFBRSxPQUFPLElBQUksQ0FBQztRQUN2QixJQUFJLE1BQU0sR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNuRCxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRW5DLElBQUksTUFBTSxFQUFFO1lBQ1YsTUFBTSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDaEM7YUFBTTtZQUNMLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDMUI7UUFDRCxJQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixPQUFPLE9BQU8sQ0FBQztJQUNqQixDQUFDO0lBRWEsMkJBQWlCLEdBQS9CLFVBQW1DLElBQWlCLEVBQUUsSUFBYTtRQUNqRSxJQUFJLENBQUMsSUFBSTtZQUFFLE9BQU8sSUFBSSxDQUFDO1FBQ3ZCLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFbkMsSUFBSSxPQUFPLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUVhLDhCQUFvQixHQUFsQyxVQUFzQyxJQUFpQixFQUFFLE1BQWlCLEVBQUUsVUFBbUI7UUFDN0YsSUFBSSxDQUFDLE1BQU07WUFBRSxPQUFPLElBQUksQ0FBQztRQUN6QixJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2xDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDM0IsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUVhLDhCQUFvQixHQUFsQyxVQUFzQyxNQUFpQixFQUFFLFVBQW1CO1FBQzFFLElBQUksQ0FBQyxNQUFNO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFDekIsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVhLG9CQUFVLEdBQXhCLFVBQXlCLElBQVUsRUFBRSxNQUFjO1FBQ2pELE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUU3RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDdkcsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNsSSxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3JJLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDM0ksTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUMzSSxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRWEsd0JBQWMsR0FBNUIsVUFBNkIsU0FBZSxFQUFFLE9BQWEsRUFBRSxNQUFjO1FBQ3pFLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNuRSxJQUFJLFNBQVMsR0FBRyxTQUFTLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3pDLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDcEgsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsU0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNsSixNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3JKLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsVUFBVSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDM0osV0FBVztRQUNYLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNqRSxJQUFJLE9BQU8sR0FBRyxTQUFTLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZDLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDOUcsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUM1SSxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQy9JLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDckosT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUVhLGlDQUF1QixHQUFyQyxVQUFzQyxLQUFhO1FBQ2pELElBQUksQ0FBQyxHQUFHLElBQUksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUMsT0FBTyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRWEsd0NBQThCLEdBQTVDLFVBQTZDLEtBQWE7UUFDeEQsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVhLCtCQUFxQixHQUFuQyxVQUFvQyxLQUFhO1FBQy9DLElBQUksQ0FBQyxHQUFHLElBQUksR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0MsT0FBTyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBQ2EsMkJBQWlCLEdBQS9CLFVBQWdDLEtBQWE7UUFDM0MsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQ2IsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRTtZQUNiLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNWLEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDcEI7UUFFRCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxLQUFLLElBQUksWUFBWSxFQUFFO1lBQ3pCLEtBQUssSUFBSSxZQUFZLENBQUM7WUFDdEIsTUFBTSxHQUFHLEdBQUcsQ0FBQztTQUNkO2FBQU0sSUFBSSxLQUFLLElBQUksU0FBUyxFQUFFO1lBQzdCLEtBQUssSUFBSSxTQUFTLENBQUM7WUFDbkIsTUFBTSxHQUFHLEdBQUcsQ0FBQztTQUNkO2FBQU0sSUFBSSxLQUFLLElBQUksTUFBTSxFQUFFO1lBQzFCLEtBQUssSUFBSSxNQUFNLENBQUM7WUFDaEIsTUFBTSxHQUFHLEdBQUcsQ0FBQztTQUNkO1FBRUQsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxHQUFHLFVBQVUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUM1RCxPQUFPLEtBQUssR0FBRyxNQUFNLENBQUM7SUFDeEIsQ0FBQztJQUVhLDhCQUFvQixHQUFsQyxVQUFtQyxLQUFhO1FBQzlDLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQztRQUNiLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNsQixJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUU7WUFDYixJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDVixLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQ3BCO1FBRUQsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksS0FBSyxJQUFJLFlBQVksRUFBRTtZQUN6QixLQUFLLElBQUksWUFBWSxDQUFDO1lBQ3RCLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDaEI7YUFBTSxJQUFJLEtBQUssSUFBSSxTQUFTLEVBQUU7WUFDN0IsS0FBSyxJQUFJLFNBQVMsQ0FBQztZQUNuQixNQUFNLEdBQUcsUUFBUSxDQUFDO1NBQ25CO2FBQU0sSUFBSSxLQUFLLElBQUksTUFBTSxFQUFFO1lBQzFCLEtBQUssSUFBSSxNQUFNLENBQUM7WUFDaEIsTUFBTSxHQUFHLE9BQU8sQ0FBQztTQUNsQjtRQUVELEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsR0FBRyxVQUFVLENBQUMsR0FBRyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDNUQsT0FBTyxLQUFLLEdBQUcsTUFBTSxDQUFDO0lBQ3hCLENBQUM7SUFFYSwrQkFBcUIsR0FBbkMsVUFBb0MsTUFBTTtRQUN4QyxJQUFJLENBQUMsR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzdDLE9BQU8sQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVhLDBCQUFnQixHQUE5QixVQUErQixNQUFNO1FBQ25DLElBQUksTUFBTSxFQUFFO1lBQ1YsSUFBSSxNQUFNLEdBQUcsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1RSxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUU1RCxPQUFPLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUMxRDtRQUNELE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQUVhLDRCQUFrQixHQUFoQyxVQUFpQyxNQUFNO1FBQ3JDLElBQUksTUFBTSxFQUFFO1lBQ1YsSUFBSSxNQUFNLEdBQUcsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1RSxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUU1RCxPQUFPLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUMxRDtRQUNELE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQUVhLHVCQUFhLEdBQTNCLFVBQTRCLE1BQU07UUFDaEMsSUFBSSxNQUFNLEVBQUU7WUFDVixJQUFJLE1BQU0sR0FBRyxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVFLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLHVCQUF1QixFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBRTVELE9BQU8sTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzFEO1FBQ0QsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDO0lBRWEsOEJBQW9CLEdBQWxDLFVBQW1DLEtBQWEsRUFBRSxZQUFnQztRQUFoQyw2QkFBQSxFQUFBLHdCQUFnQztRQUNoRixJQUFJLEVBQUUsR0FBRyxFQUFFLENBQUM7UUFDWixJQUFJLEtBQUssSUFBSSxZQUFZO1lBQUUsRUFBRSxHQUFHLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQzs7WUFDOUQsRUFBRSxHQUFHLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM1QyxPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFYSxxQkFBVyxHQUF6QixVQUEwQixHQUFXLEVBQUUsUUFBdUI7UUFBdkIseUJBQUEsRUFBQSxlQUF1QjtRQUM1RCxJQUFJLElBQUksR0FBRyxRQUFRLENBQUMsQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNqRCxJQUFJLEdBQUcsR0FBRyxRQUFRLElBQUksQ0FBQztZQUFFLElBQUksSUFBSSxDQUFDLENBQUM7UUFDbkMsT0FBTyxJQUFJLEdBQUcsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFFYSxzQkFBWSxHQUExQixVQUEyQixHQUFXLEVBQUUsR0FBZTtRQUFmLG9CQUFBLEVBQUEsT0FBZTtRQUNyRCxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQixHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN0QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQ3ZELENBQUM7SUFFYSw0QkFBa0IsR0FBaEMsVUFBaUMsQ0FBYTtRQUFiLGtCQUFBLEVBQUEsS0FBYTtRQUM1QyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUNyQyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7UUFDeEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUIsT0FBTyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFYSx3QkFBYyxHQUE1QixVQUE2QixHQUFHO1FBQzlCLE9BQU8sSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQztJQUM3QixDQUFDO0lBRWEsc0JBQVksR0FBMUIsVUFBMkIsUUFBcUIsRUFBRSxXQUFtQixFQUFFLGFBQXFCLEVBQUUsV0FBNEI7UUFBNUIsNEJBQUEsRUFBQSxrQkFBNEI7UUFDeEgsSUFBSSxRQUFRLElBQUksSUFBSSxJQUFJLFFBQVEsSUFBSSxTQUFTO1lBQUUsT0FBTztRQUN0RCxnR0FBZ0c7UUFDaEcsWUFBWTtRQUNaLElBQUk7UUFDSixRQUFRLENBQUMsbUJBQW1CLENBQUMsVUFBQyxVQUErQjtZQUMzRCxJQUFJLGFBQWEsSUFBSSxFQUFFLElBQUksYUFBYSxJQUFJLElBQUk7Z0JBQUUsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2hHLElBQUksVUFBVSxDQUFDLFNBQVMsSUFBSSxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxXQUFXO2dCQUFFLFdBQVcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUNyRyxDQUFDLENBQUMsQ0FBQztRQUNILFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRWEsYUFBRyxHQUFqQixVQUFrQixLQUFlO1FBQy9CLElBQUksS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFDaEMsT0FBTyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQUMsS0FBSyxFQUFFLEdBQUcsSUFBSyxPQUFBLEtBQUssR0FBRyxHQUFHLEVBQVgsQ0FBVyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVhLGlDQUF1QixHQUFyQyxVQUFzQyxLQUFlLEVBQUUsT0FBZTtRQUNwRSxJQUFJLE9BQU8sQ0FBQyxNQUFNLEdBQUcsRUFBRSxFQUFFO1lBQ3ZCLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDN0MsS0FBSyxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7U0FDbEQ7YUFBTTtZQUNMLEtBQUssQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1NBQ3pDO1FBQ0QsS0FBSyxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7SUFDekIsQ0FBQztJQUVNLHNCQUFZLEdBQW5CLFVBQW9CLEdBQVc7UUFBRSxhQUFhO2FBQWIsVUFBYSxFQUFiLHFCQUFhLEVBQWIsSUFBYTtZQUFiLDRCQUFhOztRQUM1QyxLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsR0FBRyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUMvQyxHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFJLEtBQUssTUFBRyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1NBQ3hEO1FBQ0QsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDO0lBRU0sdUJBQWEsR0FBcEIsVUFBcUIsT0FBZ0I7UUFDbkMsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPO1FBQ3JCLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDO1lBQzdELENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNULENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLHVCQUF1QixDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDO1lBQ3RELENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNULENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLHVCQUF1QixDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDO1lBQ3JELENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sdUJBQWEsR0FBcEIsVUFBcUIsWUFBNkIsRUFBRSxJQUFhLEVBQUUsWUFBcUIsRUFBRSxNQUFtQjtRQUF4Riw2QkFBQSxFQUFBLHFCQUE2QjtRQUF3Qyx1QkFBQSxFQUFBLFdBQW1CO1FBQzNHLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNULElBQUksR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDakMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUNyQixZQUFZLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDaEM7UUFDRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxQyxJQUFJLE1BQU07WUFBRSxNQUFNLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUVuQyxJQUFJLFFBQVEsR0FBRyxTQUFTLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDbEIsQ0FBQztJQUVNLHVCQUFhLEdBQXBCLFVBQXFCLFlBQTZCLEVBQUUsWUFBcUIsRUFBRSxNQUFtQjtRQUF6RSw2QkFBQSxFQUFBLHFCQUE2QjtRQUF5Qix1QkFBQSxFQUFBLFdBQW1CO1FBQzVGLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNULElBQUksR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDakMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztTQUN0QjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVNLHNCQUFZLEdBQW5CLFVBQW9CLFNBQW9CO1FBQ3RDLElBQUksV0FBVyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUM7UUFDNUIsSUFBSSxTQUFTLEdBQUcsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQyxJQUFJLFFBQVEsR0FBRyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQzlCLElBQUksVUFBVSxHQUFHLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDL0IsSUFBSSxTQUFTLEdBQUcsUUFBUSxHQUFHLEdBQUcsQ0FBQztRQUUvQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3hELEVBQUUsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQzNCLElBQUksT0FBTyxHQUFHLFdBQVcsRUFBRTtZQUN6QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLGFBQWEsQ0FBQztTQUNuRDthQUFNLElBQUksT0FBTyxHQUFHLFNBQVMsRUFBRTtZQUM5QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQyxHQUFHLGFBQWEsQ0FBQztTQUMxRDthQUFNLElBQUksT0FBTyxHQUFHLFFBQVEsRUFBRTtZQUM3QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUN2RDthQUFNLElBQUksT0FBTyxHQUFHLFVBQVUsRUFBRTtZQUMvQixPQUFPLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsR0FBRyxhQUFhLENBQUM7U0FDbkU7YUFBTSxJQUFJLE9BQU8sR0FBRyxTQUFTLEVBQUU7WUFDOUIsT0FBTyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsVUFBVSxDQUFDLEdBQUcsY0FBYyxDQUFDO1NBQ3RFO2FBQU07WUFDTCxPQUFPLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUMsR0FBRyxZQUFZLENBQUM7U0FDbkU7SUFDSCxDQUFDO0lBRWEsdUJBQWEsR0FBM0IsVUFBNEIsTUFBTSxFQUFFLEtBQUs7UUFDdkMsSUFBSSxHQUFHLEdBQUcsU0FBUyxDQUFDO1FBQ3BCLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQztZQUNsQixJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQ2QsR0FBRyxHQUFHLENBQUMsQ0FBQzthQUNUO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLEdBQUcsQ0FBQztJQUNiLENBQUM7SUFFYSx1QkFBYSxHQUEzQjtRQUNFLE9BQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7SUFDNUUsQ0FBQztJQUVhLG1CQUFTLEdBQXZCO1FBQ0UsT0FBTyxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBRWEsa0JBQVEsR0FBdEI7UUFDRSxPQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFFYSxtQkFBUyxHQUF2QjtRQUNFLE9BQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUM7SUFDM0QsQ0FBQztJQUVhLGVBQUssR0FBbkI7UUFDRSxPQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDO0lBQ3ZELENBQUM7SUFFYSxzQkFBWSxHQUExQjtRQUNFLE9BQU8sb0JBQVUsQ0FBQyxrQkFBa0IsQ0FBQztJQUN2QyxDQUFDO0lBRWEsMkJBQWlCLEdBQS9CLFVBQWdDLE1BQU07UUFDcEMsT0FBTyxNQUFNLElBQUksSUFBSSxJQUFJLE1BQU0sSUFBSSxTQUFTLENBQUM7SUFDL0MsQ0FBQztJQUNhLHNDQUE0QixHQUExQyxVQUEyQyxhQUFxQjtRQUM5RCxJQUFJLG1CQUFtQixHQUFHLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNuRCxJQUFJLGdCQUFnQixHQUFHLENBQUMsR0FBRyxtQkFBbUIsQ0FBQyxNQUFNLENBQUM7UUFDdEQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGdCQUFnQixFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3pDLG1CQUFtQixHQUFHLEdBQUcsR0FBRyxtQkFBbUIsQ0FBQztTQUNqRDtRQUNELE9BQU8sbUJBQW1CLENBQUM7SUFDN0IsQ0FBQztJQUVhLHFCQUFXLEdBQXpCO1FBQ0UsT0FBTyxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQztJQUM3QyxDQUFDO0lBRWEscUJBQVcsR0FBekI7UUFDRSxJQUFJLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRTVELElBQUksRUFBRSxJQUFJLElBQUksSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO1lBQzFCLElBQUksS0FBSyxHQUNQLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDVCxFQUFFLENBQUMsR0FBRyxDQUFDLGFBQWE7Z0JBQ3BCLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUTtnQkFDZixFQUFFLENBQUMsR0FBRyxDQUFDLFlBQVk7Z0JBQ25CLEVBQUUsQ0FBQyxHQUFHLENBQUMsU0FBUztnQkFDaEIsRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRO2dCQUNmLEVBQUUsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsTUFBTTtnQkFDbkMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRO2dCQUNmLEVBQUUsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsS0FBSztnQkFDbEMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRO2dCQUNmLEVBQUUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7WUFFZixFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pCLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ3hEO1FBQ0QsU0FBUyxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUNhLHlCQUFlLEdBQTdCO1FBQ0UsSUFBSSxTQUFTLENBQUMsV0FBVyxFQUFFLEVBQUU7WUFDM0IsT0FBTyxNQUFNLENBQUM7U0FDZjthQUFNLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUU7WUFDM0IsT0FBTyxPQUFPLENBQUM7U0FDaEI7YUFBTSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFO1lBQ3pDLE9BQU8sU0FBUyxDQUFDO1NBQ2xCO2FBQU0sSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRTtZQUNyQyxPQUFPLEtBQUssQ0FBQztTQUNkOztZQUFNLE9BQU8sU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFFYyxvQkFBVSxHQUF6QixVQUEwQixNQUFNO1FBQzlCLElBQUksRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUNaLEtBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxNQUFNLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUU7WUFDdEQsS0FBSyxJQUFJLEdBQUcsR0FBRyxDQUFDLEVBQUUsR0FBRyxHQUFHLE1BQU0sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRTtnQkFDdEQsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2pEO1NBQ0Y7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFoZWEsZ0JBQU0sR0FBWSxLQUFLLENBQUM7SUFDeEIsZUFBSyxHQUFXLENBQUMsQ0FBQztJQUNsQixrQkFBUSxHQUFhLG1CQUFRLENBQUMsTUFBTSxDQUFDO0lBQ3JDLG1CQUFTLEdBQVUsZ0JBQUssQ0FBQyxJQUFJLENBQUM7SUFDOUIsc0JBQVksR0FBWSxJQUFJLENBQUM7SUFDN0IscUJBQVcsR0FBWSxJQUFJLENBQUM7SUFDNUIseUJBQWUsR0FBWSxJQUFJLENBQUM7SUFDaEMsb0JBQVUsR0FBVyxFQUFFLENBQUM7SUFDeEIsa0JBQVEsR0FBVyxFQUFFLENBQUM7SUFDdEIsbUJBQVMsR0FBVyxFQUFFLENBQUM7SUFDYixxQkFBVyxHQUFHLFdBQVcsQ0FBQztJQXVkcEQsZ0JBQUM7Q0FsZUQsQUFrZUMsSUFBQTtrQkFsZW9CLFNBQVMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBHYW1lVHlwZSwgdHlwZVNjZW5lIH0gZnJvbSBcIi4uL1V0aWxzL0VudW1HYW1lXCI7XG5pbXBvcnQgeyBUaW1lc3RhbXAgfSBmcm9tIFwiZ29vZ2xlLXByb3RvYnVmL2dvb2dsZS9wcm90b2J1Zi90aW1lc3RhbXBfcGJcIjtcbmltcG9ydCBHYW1lRGVmaW5lIGZyb20gXCIuLi9EZWZpbmUvR2FtZURlZmluZVwiO1xuaW1wb3J0IHsgVG9waWMgfSBmcm9tIFwiQGdhbWVsb290L3RvcGljL3RvcGljX3BiXCI7XG5pbXBvcnQgVGV4dE5vdGlmeSwgeyBUZXh0Tm90aWZ5RGF0YSB9IGZyb20gXCIuLi9NYW5hZ2VyL1RleHROb3RpZnlcIjtcbmltcG9ydCBVSU1hbmFnZXIgZnJvbSBcIi4uL01hbmFnZXIvVUlNYW5hZ2VyXCI7XG5pbXBvcnQgUG9ydGFsVGV4dCBmcm9tIFwiLi4vVXRpbHMvUG9ydGFsVGV4dFwiO1xuXG5leHBvcnQgZW51bSBCYWRnZSB7XG4gIGdvbGQgPSBcIl9nb2xkXCIsXG4gIHBsYXRpbnVtID0gXCJfcGxhdGludW1cIixcbiAgZGlhbW9uZCA9IFwiX2RpYW1vbmRcIixcbiAgcnVieSA9IFwiX3J1YnlcIixcbiAgZW1lcmFsZCA9IFwiX2VtZXJhbGRcIixcbn1cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEdhbWVVdGlscyB7XG4gIHB1YmxpYyBzdGF0aWMgSVNfREVWOiBib29sZWFuID0gZmFsc2U7XG4gIHB1YmxpYyBzdGF0aWMgY291bnQ6IG51bWJlciA9IDA7XG4gIHB1YmxpYyBzdGF0aWMgZ2FtZVR5cGU6IEdhbWVUeXBlID0gR2FtZVR5cGUuQmF1Q3VhO1xuICBwdWJsaWMgc3RhdGljIGdhbWVUb3BpYzogVG9waWMgPSBUb3BpYy5USU5BO1xuICBwdWJsaWMgc3RhdGljIGlzU3RhdHVzUGluZzogYm9vbGVhbiA9IHRydWU7XG4gIHB1YmxpYyBzdGF0aWMgaXNNYWluU2NlbmU6IGJvb2xlYW4gPSB0cnVlO1xuICBwdWJsaWMgc3RhdGljIGlzRmlyc3RMb2FkR2FtZTogYm9vbGVhbiA9IHRydWU7XG4gIHB1YmxpYyBzdGF0aWMgZGV2aWNlTmFtZTogc3RyaW5nID0gXCJcIjtcbiAgcHVibGljIHN0YXRpYyBkZXZpY2VJRDogc3RyaW5nID0gXCJcIjtcbiAgcHVibGljIHN0YXRpYyB1dG1Tb3VyY2U6IHN0cmluZyA9IFwiXCI7XG4gIHByaXZhdGUgc3RhdGljIHJlYWRvbmx5IGRldmljZUlkS2V5ID0gXCJERVZJQ0VfSURcIjtcblxuICBwdWJsaWMgc3RhdGljIGNyZWF0ZUNsaWNrQnRuRXZlbnQobm9kZTogY2MuTm9kZSwgY29tcG9uZW50TmFtZTogc3RyaW5nLCBoYW5kbGVyRXZlbnQ6IHN0cmluZywgY3VzdG9tOiBzdHJpbmcgPSBcIlwiKSB7XG4gICAgY29uc3QgY2xpY2tFdmVudEhhbmRsZXIgPSBuZXcgY2MuQ29tcG9uZW50LkV2ZW50SGFuZGxlcigpO1xuICAgIGNsaWNrRXZlbnRIYW5kbGVyLnRhcmdldCA9IG5vZGU7IC8vVGhpcyBub2RlIGlzIHRoZSBub2RlIHRvIHdoaWNoIHlvdXIgZXZlbnQgaGFuZGxlciBjb2RlIGNvbXBvbmVudCBiZWxvbmdzXG4gICAgY2xpY2tFdmVudEhhbmRsZXIuY29tcG9uZW50ID0gY29tcG9uZW50TmFtZTsgLy9UaGlzIGlzIHRoZSBjb2RlIGZpbGUgbmFtZVxuICAgIGNsaWNrRXZlbnRIYW5kbGVyLmhhbmRsZXIgPSBoYW5kbGVyRXZlbnQ7XG4gICAgY2xpY2tFdmVudEhhbmRsZXIuY3VzdG9tRXZlbnREYXRhID0gY3VzdG9tO1xuICAgIHJldHVybiBjbGlja0V2ZW50SGFuZGxlcjtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZ2V0VXJsUGFyYW1ldGVyKG5hbWUsIHVybCkge1xuICAgIG5hbWUgPSBuYW1lLnJlcGxhY2UoXCIvW1tdL1wiLCBcIlxcXFxbXCIpLnJlcGxhY2UoXCIvW11dL1wiLCBcIlxcXFxdXCIpO1xuICAgIGxldCByZWdleCA9IG5ldyBSZWdFeHAoXCJbXFxcXD8mXVwiICsgbmFtZSArIFwiPShbXiYjXSopXCIpO1xuICAgIGxldCByZXN1bHRzID0gcmVnZXguZXhlYyh1cmwpO1xuICAgIHJldHVybiByZXN1bHRzID09PSBudWxsID8gXCJcIiA6IGRlY29kZVVSSUNvbXBvbmVudChyZXN1bHRzWzFdLnJlcGxhY2UoL1xcKy9nLCBcIiBcIikpO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyByZW1vdmVQYXJhbWV0ZXJVcmwoc1BhcmFtLCBzb3VyY2VVUkwpOiBzdHJpbmcge1xuICAgIHZhciBydG4gPSBzb3VyY2VVUkwuc3BsaXQoXCI/XCIpWzBdLFxuICAgICAgcGFyYW0sXG4gICAgICBwYXJhbXNfYXJyID0gW10sXG4gICAgICBxdWVyeVN0cmluZyA9IHNvdXJjZVVSTC5pbmRleE9mKFwiP1wiKSAhPT0gLTEgPyBzb3VyY2VVUkwuc3BsaXQoXCI/XCIpWzFdIDogXCJcIjtcbiAgICBpZiAocXVlcnlTdHJpbmcgIT09IFwiXCIpIHtcbiAgICAgIHBhcmFtc19hcnIgPSBxdWVyeVN0cmluZy5zcGxpdChcIiZcIik7XG4gICAgICBmb3IgKHZhciBpID0gcGFyYW1zX2Fyci5sZW5ndGggLSAxOyBpID49IDA7IGkgLT0gMSkge1xuICAgICAgICBwYXJhbSA9IHBhcmFtc19hcnJbaV0uc3BsaXQoXCI9XCIpWzBdO1xuICAgICAgICBpZiAocGFyYW0gPT09IHNQYXJhbSkge1xuICAgICAgICAgIHBhcmFtc19hcnIuc3BsaWNlKGksIDEpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAocGFyYW1zX2Fyci5sZW5ndGgpIHJ0biA9IHJ0biArIFwiP1wiICsgcGFyYW1zX2Fyci5qb2luKFwiJlwiKTtcbiAgICB9XG4gICAgcmV0dXJuIHJ0bjtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgcmVtb3ZlQWxsUGFyYW1ldGVyVXJsKHNvdXJjZVVSTCkge1xuICAgIHZhciBydG4gPSBzb3VyY2VVUkwuc3BsaXQoXCI/XCIpWzBdLFxuICAgICAgcGFyYW0sXG4gICAgICBwYXJhbXNfYXJyID0gW10sXG4gICAgICBxdWVyeVN0cmluZyA9IHNvdXJjZVVSTC5pbmRleE9mKFwiP1wiKSAhPT0gLTEgPyBzb3VyY2VVUkwuc3BsaXQoXCI/XCIpWzFdIDogXCJcIjtcbiAgICBpZiAocXVlcnlTdHJpbmcgIT09IFwiXCIpIHtcbiAgICAgIHBhcmFtc19hcnIgPSBxdWVyeVN0cmluZy5zcGxpdChcIiZcIik7XG4gICAgICBmb3IgKHZhciBpID0gcGFyYW1zX2Fyci5sZW5ndGggLSAxOyBpID49IDA7IGkgLT0gMSkge1xuICAgICAgICBwYXJhbSA9IHBhcmFtc19hcnJbaV0uc3BsaXQoXCI9XCIpWzBdO1xuICAgICAgICAvLyBpZiAocGFyYW0gPT09IHNQYXJhbSkge1xuICAgICAgICBwYXJhbXNfYXJyLnNwbGljZShpLCAxKTtcbiAgICAgICAgLy8gfVxuICAgICAgfVxuICAgICAgaWYgKHBhcmFtc19hcnIubGVuZ3RoKSBydG4gPSBydG4gKyBcIj9cIiArIHBhcmFtc19hcnIuam9pbihcIiZcIik7XG4gICAgfVxuICAgIHdpbmRvdy5oaXN0b3J5LnJlcGxhY2VTdGF0ZShudWxsLCBudWxsLCBydG4pO1xuICAgIC8vIHJldHVybiBydG47XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGNvbnZlcnRUb090aGVyTm9kZShmcm9tTm9kZTogY2MuTm9kZSwgdGFyZ2V0Tm9kZTogY2MuTm9kZSwgcG9zOiBjYy5WZWMzID0gbnVsbCkge1xuICAgIGxldCBwYXJlbnQgPSBmcm9tTm9kZTtcbiAgICBpZiAoZnJvbU5vZGUucGFyZW50KSBwYXJlbnQgPSBmcm9tTm9kZS5wYXJlbnQ7XG4gICAgbGV0IHdvcmxkU3BhY2UgPSBwYXJlbnQuY29udmVydFRvV29ybGRTcGFjZUFSKGZyb21Ob2RlLnBvc2l0aW9uKTtcbiAgICBpZiAocG9zKSB7XG4gICAgICB3b3JsZFNwYWNlID0gZnJvbU5vZGUuY29udmVydFRvV29ybGRTcGFjZUFSKHBvcyk7XG4gICAgfVxuICAgIHJldHVybiB0YXJnZXROb2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKHdvcmxkU3BhY2UpO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBjb252ZXJ0VG9PdGhlck5vZGUyKGZyb21Ob2RlOiBjYy5Ob2RlLCB0YXJnZXROb2RlOiBjYy5Ob2RlLCBwb3M6IGNjLlZlYzIgPSBudWxsKSB7XG4gICAgbGV0IHBhcmVudCA9IGZyb21Ob2RlO1xuICAgIGlmIChmcm9tTm9kZS5wYXJlbnQpIHBhcmVudCA9IGZyb21Ob2RlLnBhcmVudDtcbiAgICBsZXQgd29ybGRTcGFjZSA9IHBhcmVudC5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoZnJvbU5vZGUuZ2V0UG9zaXRpb24oKSk7XG4gICAgaWYgKHBvcykge1xuICAgICAgd29ybGRTcGFjZSA9IGZyb21Ob2RlLmNvbnZlcnRUb1dvcmxkU3BhY2VBUihwb3MpO1xuICAgIH1cbiAgICByZXR1cm4gdGFyZ2V0Tm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUih3b3JsZFNwYWNlKTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgcmVjdENvbnRhaW5zUG9pbnQocmVjdDogY2MuUmVjdCwgcG9pbnQ6IGNjLlZlYzIpIHtcbiAgICBjb25zdCByZXN1bHQgPSBwb2ludC54ID49IHJlY3Qub3JpZ2luLnggJiYgcG9pbnQueCA8PSByZWN0Lm9yaWdpbi54ICsgcmVjdC5zaXplLndpZHRoICYmIHBvaW50LnkgPj0gcmVjdC5vcmlnaW4ueSAmJiBwb2ludC55IDw9IHJlY3Qub3JpZ2luLnkgKyByZWN0LnNpemUuaGVpZ2h0O1xuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGNyZWF0ZUl0ZW1Gcm9tTm9kZTxUPihpdGVtOiBuZXcgKCkgPT4gVCwgbm9kZTogY2MuTm9kZSwgcGFyZW50Tm9kZTogY2MuTm9kZSA9IG51bGwsIGluc2VydDogYm9vbGVhbiA9IGZhbHNlKTogVCB7XG4gICAgaWYgKCFub2RlKSByZXR1cm4gbnVsbDtcbiAgICBsZXQgcGFyZW50ID0gcGFyZW50Tm9kZSA/IHBhcmVudE5vZGUgOiBub2RlLnBhcmVudDtcbiAgICBsZXQgbmV3Tm9kZSA9IGNjLmluc3RhbnRpYXRlKG5vZGUpO1xuXG4gICAgaWYgKGluc2VydCkge1xuICAgICAgcGFyZW50Lmluc2VydENoaWxkKG5ld05vZGUsIDApO1xuICAgIH0gZWxzZSB7XG4gICAgICBwYXJlbnQuYWRkQ2hpbGQobmV3Tm9kZSk7XG4gICAgfVxuICAgIGxldCBuZXdJdGVtID0gbmV3Tm9kZS5nZXRDb21wb25lbnQoaXRlbSk7XG4gICAgbmV3SXRlbS5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgbm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICByZXR1cm4gbmV3SXRlbTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgY2xvbmVJdGVtRnJvbU5vZGU8VD4oaXRlbTogbmV3ICgpID0+IFQsIG5vZGU6IGNjLk5vZGUpOiBUIHtcbiAgICBpZiAoIW5vZGUpIHJldHVybiBudWxsO1xuICAgIGxldCBuZXdOb2RlID0gY2MuaW5zdGFudGlhdGUobm9kZSk7XG5cbiAgICBsZXQgbmV3SXRlbSA9IG5ld05vZGUuZ2V0Q29tcG9uZW50KGl0ZW0pO1xuICAgIG5ld0l0ZW0ubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIG5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgcmV0dXJuIG5ld0l0ZW07XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGNyZWF0ZUl0ZW1Gcm9tUHJlZmFiPFQ+KGl0ZW06IG5ldyAoKSA9PiBULCBwcmVmYWI6IGNjLlByZWZhYiwgcGFyZW50Tm9kZTogY2MuTm9kZSk6IFQge1xuICAgIGlmICghcHJlZmFiKSByZXR1cm4gbnVsbDtcbiAgICBsZXQgbm9kZSA9IGNjLmluc3RhbnRpYXRlKHByZWZhYik7XG4gICAgcGFyZW50Tm9kZS5hZGRDaGlsZChub2RlKTtcbiAgICBsZXQgbmV3SXRlbSA9IG5vZGUuZ2V0Q29tcG9uZW50KGl0ZW0pO1xuICAgIG5ld0l0ZW0ubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIHJldHVybiBuZXdJdGVtO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBjcmVhdGVOb2RlRnJvbVByZWZhYjxUPihwcmVmYWI6IGNjLlByZWZhYiwgcGFyZW50Tm9kZTogY2MuTm9kZSk6IGNjLk5vZGUge1xuICAgIGlmICghcHJlZmFiKSByZXR1cm4gbnVsbDtcbiAgICBsZXQgbm9kZSA9IGNjLmluc3RhbnRpYXRlKHByZWZhYik7XG4gICAgcGFyZW50Tm9kZS5hZGRDaGlsZChub2RlKTtcbiAgICByZXR1cm4gbm9kZTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZm9ybWF0RGF0ZShkYXRlOiBEYXRlLCBmb3JtYXQ6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgZm9ybWF0ID0gZm9ybWF0LnJlcGxhY2UoXCIlWVwiLCBkYXRlLmdldEZ1bGxZZWFyKCkudG9TdHJpbmcoKSk7XG5cbiAgICBsZXQgbW9udGggPSBkYXRlLmdldE1vbnRoKCkgKyAxO1xuICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKFwiJU1cIiwgbW9udGgudG9TdHJpbmcoKS5sZW5ndGggPCAyID8gXCIwXCIgKyBtb250aC50b1N0cmluZygpIDogbW9udGgudG9TdHJpbmcoKSk7XG4gICAgZm9ybWF0ID0gZm9ybWF0LnJlcGxhY2UoXCIlZFwiLCBkYXRlLmdldERhdGUoKS50b1N0cmluZygpLmxlbmd0aCA8IDIgPyBcIjBcIiArIGRhdGUuZ2V0RGF0ZSgpLnRvU3RyaW5nKCkgOiBkYXRlLmdldERhdGUoKS50b1N0cmluZygpKTtcbiAgICBmb3JtYXQgPSBmb3JtYXQucmVwbGFjZShcIiVoXCIsIGRhdGUuZ2V0SG91cnMoKS50b1N0cmluZygpLmxlbmd0aCA8IDIgPyBcIjBcIiArIGRhdGUuZ2V0SG91cnMoKS50b1N0cmluZygpIDogZGF0ZS5nZXRIb3VycygpLnRvU3RyaW5nKCkpO1xuICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKFwiJW1cIiwgZGF0ZS5nZXRNaW51dGVzKCkudG9TdHJpbmcoKS5sZW5ndGggPCAyID8gXCIwXCIgKyBkYXRlLmdldE1pbnV0ZXMoKS50b1N0cmluZygpIDogZGF0ZS5nZXRNaW51dGVzKCkudG9TdHJpbmcoKSk7XG4gICAgZm9ybWF0ID0gZm9ybWF0LnJlcGxhY2UoXCIlc1wiLCBkYXRlLmdldFNlY29uZHMoKS50b1N0cmluZygpLmxlbmd0aCA8IDIgPyBcIjBcIiArIGRhdGUuZ2V0U2Vjb25kcygpLnRvU3RyaW5nKCkgOiBkYXRlLmdldFNlY29uZHMoKS50b1N0cmluZygpKTtcbiAgICByZXR1cm4gZm9ybWF0O1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBmb3JtYXRNb3JlRGF0ZShzdGFydERhdGU6IERhdGUsIGVuZERhdGU6IERhdGUsIGZvcm1hdDogc3RyaW5nKTogc3RyaW5nIHtcbiAgICBmb3JtYXQgPSBmb3JtYXQucmVwbGFjZShcIiVZMVwiLCBzdGFydERhdGUuZ2V0RnVsbFllYXIoKS50b1N0cmluZygpKTtcbiAgICBsZXQgbW9udGhGcm9tID0gc3RhcnREYXRlLmdldE1vbnRoKCkgKyAxO1xuICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKFwiJU0xXCIsIG1vbnRoRnJvbS50b1N0cmluZygpLmxlbmd0aCA8IDIgPyBcIjBcIiArIG1vbnRoRnJvbS50b1N0cmluZygpIDogbW9udGhGcm9tLnRvU3RyaW5nKCkpO1xuICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKFwiJWQxXCIsIHN0YXJ0RGF0ZS5nZXREYXRlKCkudG9TdHJpbmcoKS5sZW5ndGggPCAyID8gXCIwXCIgKyBzdGFydERhdGUuZ2V0RGF0ZSgpLnRvU3RyaW5nKCkgOiBzdGFydERhdGUuZ2V0RGF0ZSgpLnRvU3RyaW5nKCkpO1xuICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKFwiJWgxXCIsIHN0YXJ0RGF0ZS5nZXRIb3VycygpLnRvU3RyaW5nKCkubGVuZ3RoIDwgMiA/IFwiMFwiICsgc3RhcnREYXRlLmdldEhvdXJzKCkudG9TdHJpbmcoKSA6IHN0YXJ0RGF0ZS5nZXRIb3VycygpLnRvU3RyaW5nKCkpO1xuICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKFwiJW0xXCIsIHN0YXJ0RGF0ZS5nZXRNaW51dGVzKCkudG9TdHJpbmcoKS5sZW5ndGggPCAyID8gXCIwXCIgKyBzdGFydERhdGUuZ2V0TWludXRlcygpLnRvU3RyaW5nKCkgOiBzdGFydERhdGUuZ2V0TWludXRlcygpLnRvU3RyaW5nKCkpO1xuICAgIC8vIGVuZCBkYXRlXG4gICAgZm9ybWF0ID0gZm9ybWF0LnJlcGxhY2UoXCIlWTJcIiwgZW5kRGF0ZS5nZXRGdWxsWWVhcigpLnRvU3RyaW5nKCkpO1xuICAgIGxldCBtb250aFRvID0gc3RhcnREYXRlLmdldE1vbnRoKCkgKyAxO1xuICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKFwiJU0yXCIsIG1vbnRoVG8udG9TdHJpbmcoKS5sZW5ndGggPCAyID8gXCIwXCIgKyBtb250aFRvLnRvU3RyaW5nKCkgOiBtb250aFRvLnRvU3RyaW5nKCkpO1xuICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKFwiJWQyXCIsIGVuZERhdGUuZ2V0RGF0ZSgpLnRvU3RyaW5nKCkubGVuZ3RoIDwgMiA/IFwiMFwiICsgZW5kRGF0ZS5nZXREYXRlKCkudG9TdHJpbmcoKSA6IGVuZERhdGUuZ2V0RGF0ZSgpLnRvU3RyaW5nKCkpO1xuICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKFwiJWgyXCIsIGVuZERhdGUuZ2V0SG91cnMoKS50b1N0cmluZygpLmxlbmd0aCA8IDIgPyBcIjBcIiArIGVuZERhdGUuZ2V0SG91cnMoKS50b1N0cmluZygpIDogZW5kRGF0ZS5nZXRIb3VycygpLnRvU3RyaW5nKCkpO1xuICAgIGZvcm1hdCA9IGZvcm1hdC5yZXBsYWNlKFwiJW0yXCIsIGVuZERhdGUuZ2V0TWludXRlcygpLnRvU3RyaW5nKCkubGVuZ3RoIDwgMiA/IFwiMFwiICsgZW5kRGF0ZS5nZXRNaW51dGVzKCkudG9TdHJpbmcoKSA6IGVuZERhdGUuZ2V0TWludXRlcygpLnRvU3RyaW5nKCkpO1xuICAgIHJldHVybiBmb3JtYXQ7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGZvcm1hdE1vbmV5TnVtYmVyTXlVc2VyKG1vbmV5OiBudW1iZXIpOiBzdHJpbmcge1xuICAgIGxldCBzID0gXCIkIFwiICsgdGhpcy5udW1iZXJXaXRoQ29tbWFzKG1vbmV5KTtcbiAgICByZXR1cm4gcztcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZm9ybWF0TW9uZXlOdW1iZXJNeVVzZXJOb3RJY29uKG1vbmV5OiBudW1iZXIpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLm51bWJlcldpdGhDb21tYXMobW9uZXkpO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBmb3JtYXRNb25leU51bWJlclVzZXIobW9uZXk6IG51bWJlcik6IHN0cmluZyB7XG4gICAgbGV0IHMgPSBcIiQgXCIgKyB0aGlzLmZvcm1hdE1vbmV5TnVtYmVyKG1vbmV5KTtcbiAgICByZXR1cm4gcztcbiAgfVxuICBwdWJsaWMgc3RhdGljIGZvcm1hdE1vbmV5TnVtYmVyKG1vbmV5OiBudW1iZXIpOiBzdHJpbmcge1xuICAgIGxldCBzaWduID0gMTtcbiAgICBsZXQgdmFsdWUgPSBtb25leTtcbiAgICBpZiAobW9uZXkgPCAwKSB7XG4gICAgICBzaWduID0gLTE7XG4gICAgICB2YWx1ZSA9IHZhbHVlICogLTE7XG4gICAgfVxuXG4gICAgdmFyIGZvcm1hdCA9IFwiXCI7XG4gICAgaWYgKHZhbHVlID49IDEwMDAwMDAwMDAuMCkge1xuICAgICAgdmFsdWUgLz0gMTAwMDAwMDAwMC4wO1xuICAgICAgZm9ybWF0ID0gXCJCXCI7XG4gICAgfSBlbHNlIGlmICh2YWx1ZSA+PSAxMDAwMDAwLjApIHtcbiAgICAgIHZhbHVlIC89IDEwMDAwMDAuMDtcbiAgICAgIGZvcm1hdCA9IFwiTVwiO1xuICAgIH0gZWxzZSBpZiAodmFsdWUgPj0gMTAwMC4wKSB7XG4gICAgICB2YWx1ZSAvPSAxMDAwLjA7XG4gICAgICBmb3JtYXQgPSBcIktcIjtcbiAgICB9XG5cbiAgICB2YWx1ZSA9IChNYXRoLmZsb29yKHZhbHVlICogMTAwICsgMC4wMDAwMDAwMSkgLyAxMDApICogc2lnbjtcbiAgICByZXR1cm4gdmFsdWUgKyBmb3JtYXQ7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGZvcm1hdE1vbmV5TnVtYmVyX3YyKG1vbmV5OiBudW1iZXIpOiBzdHJpbmcge1xuICAgIGxldCBzaWduID0gMTtcbiAgICBsZXQgdmFsdWUgPSBtb25leTtcbiAgICBpZiAobW9uZXkgPCAwKSB7XG4gICAgICBzaWduID0gLTE7XG4gICAgICB2YWx1ZSA9IHZhbHVlICogLTE7XG4gICAgfVxuXG4gICAgdmFyIGZvcm1hdCA9IFwiXCI7XG4gICAgaWYgKHZhbHVlID49IDEwMDAwMDAwMDAuMCkge1xuICAgICAgdmFsdWUgLz0gMTAwMDAwMDAwMC4wO1xuICAgICAgZm9ybWF0ID0gXCIgVOG7iVwiO1xuICAgIH0gZWxzZSBpZiAodmFsdWUgPj0gMTAwMDAwMC4wKSB7XG4gICAgICB2YWx1ZSAvPSAxMDAwMDAwLjA7XG4gICAgICBmb3JtYXQgPSBcIiBUcmnhu4d1XCI7XG4gICAgfSBlbHNlIGlmICh2YWx1ZSA+PSAxMDAwLjApIHtcbiAgICAgIHZhbHVlIC89IDEwMDAuMDtcbiAgICAgIGZvcm1hdCA9IFwiIE5nw6BuXCI7XG4gICAgfVxuXG4gICAgdmFsdWUgPSAoTWF0aC5mbG9vcih2YWx1ZSAqIDEwMCArIDAuMDAwMDAwMDEpIC8gMTAwKSAqIHNpZ247XG4gICAgcmV0dXJuIHZhbHVlICsgZm9ybWF0O1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBudW1iZXJXaXRoQ29tbWFzTW9uZXkobnVtYmVyKSB7XG4gICAgbGV0IHMgPSBcIiQgXCIgKyB0aGlzLm51bWJlcldpdGhDb21tYXMobnVtYmVyKTtcbiAgICByZXR1cm4gcztcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgbnVtYmVyV2l0aENvbW1hcyhudW1iZXIpIHtcbiAgICBpZiAobnVtYmVyKSB7XG4gICAgICB2YXIgcmVzdWx0ID0gKG51bWJlciA9IHBhcnNlRmxvYXQobnVtYmVyKSkudG9GaXhlZCgyKS50b1N0cmluZygpLnNwbGl0KFwiLlwiKTtcbiAgICAgIHJlc3VsdFswXSA9IHJlc3VsdFswXS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIixcIik7XG5cbiAgICAgIHJldHVybiByZXN1bHRbMV0gIT09IFwiMDBcIiA/IHJlc3VsdC5qb2luKFwiLFwiKSA6IHJlc3VsdFswXTtcbiAgICB9XG4gICAgcmV0dXJuIFwiMFwiO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBudW1iZXJXaXRoQ29tbWFzVjIobnVtYmVyKSB7XG4gICAgaWYgKG51bWJlcikge1xuICAgICAgdmFyIHJlc3VsdCA9IChudW1iZXIgPSBwYXJzZUZsb2F0KG51bWJlcikpLnRvRml4ZWQoMikudG9TdHJpbmcoKS5zcGxpdChcIi5cIik7XG4gICAgICByZXN1bHRbMF0gPSByZXN1bHRbMF0ucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIuXCIpO1xuXG4gICAgICByZXR1cm4gcmVzdWx0WzFdICE9PSBcIjAwXCIgPyByZXN1bHQuam9pbihcIixcIikgOiByZXN1bHRbMF07XG4gICAgfVxuICAgIHJldHVybiBcIjBcIjtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgbnVtYmVyV2l0aERvdChudW1iZXIpIHtcbiAgICBpZiAobnVtYmVyKSB7XG4gICAgICB2YXIgcmVzdWx0ID0gKG51bWJlciA9IHBhcnNlRmxvYXQobnVtYmVyKSkudG9GaXhlZCgyKS50b1N0cmluZygpLnNwbGl0KFwiLlwiKTtcbiAgICAgIHJlc3VsdFswXSA9IHJlc3VsdFswXS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIi5cIik7XG5cbiAgICAgIHJldHVybiByZXN1bHRbMV0gIT09IFwiMDBcIiA/IHJlc3VsdC5qb2luKFwiLFwiKSA6IHJlc3VsdFswXTtcbiAgICB9XG4gICAgcmV0dXJuIFwiMFwiO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBmb3JtYXRNb25leVdpdGhSYW5nZShtb25leTogbnVtYmVyLCBzaG9ydGVuU3RhcnQ6IG51bWJlciA9IDEwMDAwMDAwMCk6IHN0cmluZyB7XG4gICAgbGV0IHJzID0gXCJcIjtcbiAgICBpZiAobW9uZXkgPj0gc2hvcnRlblN0YXJ0KSBycyA9IEdhbWVVdGlscy5mb3JtYXRNb25leU51bWJlcihtb25leSk7XG4gICAgZWxzZSBycyA9IEdhbWVVdGlscy5udW1iZXJXaXRoQ29tbWFzKG1vbmV5KTtcbiAgICByZXR1cm4gcnM7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIHJvdW5kTnVtYmVyKG51bTogbnVtYmVyLCByb3VuZE1pbjogbnVtYmVyID0gMTAwMCk6IG51bWJlciB7XG4gICAgbGV0IHRlbXAgPSBwYXJzZUludCgobnVtIC8gcm91bmRNaW4pLnRvU3RyaW5nKCkpO1xuICAgIGlmIChudW0gJSByb3VuZE1pbiAhPSAwKSB0ZW1wICs9IDE7XG4gICAgcmV0dXJuIHRlbXAgKiByb3VuZE1pbjtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZ2V0UmFuZG9tSW50KG1heDogbnVtYmVyLCBtaW46IG51bWJlciA9IDApOiBudW1iZXIge1xuICAgIG1pbiA9IE1hdGguY2VpbChtaW4pO1xuICAgIG1heCA9IE1hdGguZmxvb3IobWF4KTtcbiAgICByZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKG1heCAtIG1pbikpICsgbWluO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyByYW5kb21JbnNpZGVDaXJjbGUoUjogbnVtYmVyID0gMSk6IGNjLlZlYzIge1xuICAgIGxldCByID0gUiAqIE1hdGguc3FydChNYXRoLnJhbmRvbSgpKTtcbiAgICBsZXQgdGhldGEgPSBNYXRoLnJhbmRvbSgpICogMiAqIE1hdGguUEk7XG4gICAgbGV0IHggPSByICogTWF0aC5jb3ModGhldGEpO1xuICAgIGxldCB5ID0gciAqIE1hdGguc2luKHRoZXRhKTtcbiAgICByZXR1cm4gbmV3IGNjLlZlYzIoeCwgeSk7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGdldFJhbmRvbUZsb2F0KG1heCkge1xuICAgIHJldHVybiBNYXRoLnJhbmRvbSgpICogbWF4O1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBwbGF5QW5pbU9uY2Uoc2tlbGV0b246IHNwLlNrZWxldG9uLCB0b0FuaW1hdGlvbjogc3RyaW5nLCBiYWNrQW5pbWF0aW9uOiBzdHJpbmcsIG9uQ29tcGxldGVkOiBGdW5jdGlvbiA9IG51bGwpIHtcbiAgICBpZiAoc2tlbGV0b24gPT0gbnVsbCB8fCBza2VsZXRvbiA9PSB1bmRlZmluZWQpIHJldHVybjtcbiAgICAvLyBpZiAoc2tlbGV0b24uZ2V0Q3VycmVudCgwKSAhPSBudWxsICYmIHNrZWxldG9uLmdldEN1cnJlbnQoMCkuYW5pbWF0aW9uLm5hbWUgPT0gdG9BbmltYXRpb24pIHtcbiAgICAvLyAgIHJldHVybjtcbiAgICAvLyB9XG4gICAgc2tlbGV0b24uc2V0Q29tcGxldGVMaXN0ZW5lcigodHJhY2tFbnRyeTogc3Auc3BpbmUuVHJhY2tFbnRyeSkgPT4ge1xuICAgICAgaWYgKGJhY2tBbmltYXRpb24gIT0gXCJcIiAmJiBiYWNrQW5pbWF0aW9uICE9IG51bGwpIHNrZWxldG9uLnNldEFuaW1hdGlvbigwLCBiYWNrQW5pbWF0aW9uLCB0cnVlKTtcbiAgICAgIGlmICh0cmFja0VudHJ5LmFuaW1hdGlvbiAmJiB0cmFja0VudHJ5LmFuaW1hdGlvbi5uYW1lID09IHRvQW5pbWF0aW9uKSBvbkNvbXBsZXRlZCAmJiBvbkNvbXBsZXRlZCgpO1xuICAgIH0pO1xuICAgIHNrZWxldG9uLnNldEFuaW1hdGlvbigwLCB0b0FuaW1hdGlvbiwgZmFsc2UpO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBzdW0oYXJyYXk6IG51bWJlcltdKSB7XG4gICAgaWYgKGFycmF5Lmxlbmd0aCA8PSAwKSByZXR1cm4gMDtcbiAgICByZXR1cm4gYXJyYXkucmVkdWNlKCh0b3RhbCwgdmFsKSA9PiB0b3RhbCArIHZhbCk7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIHNldENvbnRlbnRMYWJlbEF1dG9TaXplKGxhYmVsOiBjYy5MYWJlbCwgY29udGVudDogc3RyaW5nKSB7XG4gICAgaWYgKGNvbnRlbnQubGVuZ3RoID4gMzApIHtcbiAgICAgIGxhYmVsLm5vZGUud2lkdGggPSAoMzAgKiBsYWJlbC5mb250U2l6ZSkgLyAyO1xuICAgICAgbGFiZWwub3ZlcmZsb3cgPSBjYy5MYWJlbC5PdmVyZmxvdy5SRVNJWkVfSEVJR0hUO1xuICAgIH0gZWxzZSB7XG4gICAgICBsYWJlbC5vdmVyZmxvdyA9IGNjLkxhYmVsLk92ZXJmbG93Lk5PTkU7XG4gICAgfVxuICAgIGxhYmVsLnN0cmluZyA9IGNvbnRlbnQ7XG4gIH1cblxuICBzdGF0aWMgRm9ybWF0U3RyaW5nKHN0cjogc3RyaW5nLCAuLi52YWw6IGFueVtdKSB7XG4gICAgZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IHZhbC5sZW5ndGg7IGluZGV4KyspIHtcbiAgICAgIHN0ciA9IHN0ci5yZXBsYWNlKGB7JHtpbmRleH19YCwgdmFsW2luZGV4XS50b1N0cmluZygpKTtcbiAgICB9XG4gICAgcmV0dXJuIHN0cjtcbiAgfVxuXG4gIHN0YXRpYyBzZXRQbGF5RWZmZWN0KG5vZGVFZmY6IGNjLk5vZGUpIHtcbiAgICBpZiAoIW5vZGVFZmYpIHJldHVybjtcbiAgICBub2RlRWZmLmdldENvbXBvbmVudHNJbkNoaWxkcmVuKGNjLlBhcnRpY2xlU3lzdGVtM0QpLmZvckVhY2goKHgpID0+IHtcbiAgICAgIHguc3RvcCgpO1xuICAgICAgeC5wbGF5KCk7XG4gICAgfSk7XG4gICAgbm9kZUVmZi5nZXRDb21wb25lbnRzSW5DaGlsZHJlbihjYy5BbmltYXRpb24pLmZvckVhY2goKHgpID0+IHtcbiAgICAgIHguc3RvcCgpO1xuICAgICAgeC5wbGF5KCk7XG4gICAgfSk7XG4gICAgbm9kZUVmZi5nZXRDb21wb25lbnRzSW5DaGlsZHJlbihzcC5Ta2VsZXRvbikuZm9yRWFjaCgoeCkgPT4ge1xuICAgICAgeC5zZXRBbmltYXRpb24oMCwgeC5kZWZhdWx0QW5pbWF0aW9uLCB4Lmxvb3ApO1xuICAgIH0pO1xuICB9XG5cbiAgc3RhdGljIHNldFBhcmVudFRlbXAobmFtZVRlbXBOb2RlOiBzdHJpbmcgPSBcIlRlbXBcIiwgbm9kZTogY2MuTm9kZSwgcGFyZW50T2ZUZW1wOiBjYy5Ob2RlLCB6SW5kZXg6IG51bWJlciA9IDkwKSB7XG4gICAgbGV0IHRlbXAgPSBwYXJlbnRPZlRlbXAuZ2V0Q2hpbGRCeU5hbWUobmFtZVRlbXBOb2RlKTtcbiAgICBpZiAoIXRlbXApIHtcbiAgICAgIHRlbXAgPSBuZXcgY2MuTm9kZShuYW1lVGVtcE5vZGUpO1xuICAgICAgcGFyZW50T2ZUZW1wLmFkZENoaWxkKHRlbXApO1xuICAgICAgdGVtcC56SW5kZXggPSB6SW5kZXg7XG4gICAgICBwYXJlbnRPZlRlbXAuc29ydEFsbENoaWxkcmVuKCk7XG4gICAgfVxuICAgIGxldCB3aWRnZXQgPSBub2RlLmdldENvbXBvbmVudChjYy5XaWRnZXQpO1xuICAgIGlmICh3aWRnZXQpIHdpZGdldC5lbmFibGVkID0gZmFsc2U7XG5cbiAgICBsZXQgc3RhcnRQb3MgPSBHYW1lVXRpbHMuY29udmVydFRvT3RoZXJOb2RlKG5vZGUucGFyZW50LCB0ZW1wLCBub2RlLnBvc2l0aW9uKTtcbiAgICBub2RlLnNldFBhcmVudCh0ZW1wKTtcbiAgICBub2RlLnBvc2l0aW9uID0gc3RhcnRQb3M7XG4gICAgbm9kZS56SW5kZXggPSAwO1xuICB9XG5cbiAgc3RhdGljIGdldFBhcmVudFRlbXAobmFtZVRlbXBOb2RlOiBzdHJpbmcgPSBcIlRlbXBcIiwgcGFyZW50T2ZUZW1wOiBjYy5Ob2RlLCB6SW5kZXg6IG51bWJlciA9IDgwKSB7XG4gICAgbGV0IHRlbXAgPSBwYXJlbnRPZlRlbXAuZ2V0Q2hpbGRCeU5hbWUobmFtZVRlbXBOb2RlKTtcbiAgICBpZiAoIXRlbXApIHtcbiAgICAgIHRlbXAgPSBuZXcgY2MuTm9kZShuYW1lVGVtcE5vZGUpO1xuICAgICAgcGFyZW50T2ZUZW1wLmFkZENoaWxkKHRlbXApO1xuICAgICAgdGVtcC56SW5kZXggPSB6SW5kZXg7XG4gICAgfVxuICAgIHJldHVybiB0ZW1wO1xuICB9XG5cbiAgc3RhdGljIGdldFRpbWVTaW5jZSh0aW1lU3RhbXA6IFRpbWVzdGFtcCkge1xuICAgIGxldCBtc1Blck1pbnV0ZSA9IDYwICogMTAwMDtcbiAgICBsZXQgbXNQZXJIb3VyID0gbXNQZXJNaW51dGUgKiA2MDtcbiAgICBsZXQgbXNQZXJEYXkgPSBtc1BlckhvdXIgKiAyNDtcbiAgICBsZXQgbXNQZXJNb250aCA9IG1zUGVyRGF5ICogMzA7XG4gICAgbGV0IG1zUGVyWWVhciA9IG1zUGVyRGF5ICogMzY1O1xuXG4gICAgbGV0IGVsYXBzZWQgPSBEYXRlLm5vdygpIC0gdGltZVN0YW1wLnRvRGF0ZSgpLmdldFRpbWUoKTtcbiAgICBjYy5sb2coXCJlbGFwc2VkXCIsIGVsYXBzZWQpO1xuICAgIGlmIChlbGFwc2VkIDwgbXNQZXJNaW51dGUpIHtcbiAgICAgIHJldHVybiBNYXRoLnJvdW5kKGVsYXBzZWQgLyAxMDAwKSArIFwiIGdpw6J5IHRyxrDhu5tjXCI7XG4gICAgfSBlbHNlIGlmIChlbGFwc2VkIDwgbXNQZXJIb3VyKSB7XG4gICAgICByZXR1cm4gTWF0aC5yb3VuZChlbGFwc2VkIC8gbXNQZXJNaW51dGUpICsgXCIgcGjDunQgdHLGsOG7m2NcIjtcbiAgICB9IGVsc2UgaWYgKGVsYXBzZWQgPCBtc1BlckRheSkge1xuICAgICAgcmV0dXJuIE1hdGgucm91bmQoZWxhcHNlZCAvIG1zUGVySG91cikgKyBcIiBnaeG7nSB0csaw4bubY1wiO1xuICAgIH0gZWxzZSBpZiAoZWxhcHNlZCA8IG1zUGVyTW9udGgpIHtcbiAgICAgIHJldHVybiBcImtob+G6o25nIFwiICsgTWF0aC5yb3VuZChlbGFwc2VkIC8gbXNQZXJEYXkpICsgXCIgbmfDoHkgdHLGsOG7m2NcIjtcbiAgICB9IGVsc2UgaWYgKGVsYXBzZWQgPCBtc1BlclllYXIpIHtcbiAgICAgIHJldHVybiBcImtob+G6o25nIFwiICsgTWF0aC5yb3VuZChlbGFwc2VkIC8gbXNQZXJNb250aCkgKyBcIiB0aMOhbmcgdHLGsOG7m2NcIjtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIFwia2hv4bqjbmcgXCIgKyBNYXRoLnJvdW5kKGVsYXBzZWQgLyBtc1BlclllYXIpICsgXCIgbsSDbSB0csaw4bubY1wiO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZ2V0S2V5QnlWYWx1ZShvYmplY3QsIHZhbHVlKSB7XG4gICAgbGV0IGtleSA9IHVuZGVmaW5lZDtcbiAgICBvYmplY3QuZm9yRWFjaCgodiwgaykgPT4ge1xuICAgICAgaWYgKHZhbHVlID09IHYpIHtcbiAgICAgICAga2V5ID0gaztcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4ga2V5O1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBpc1BsYXRmb3JtSU9TKCkge1xuICAgIHJldHVybiBjYy5zeXMucGxhdGZvcm0gPT0gY2Muc3lzLklQSE9ORSB8fCBjYy5zeXMucGxhdGZvcm0gPT0gY2Muc3lzLklQQUQ7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGlzQnJvd3NlcigpIHtcbiAgICByZXR1cm4gY2Muc3lzLmlzQnJvd3NlcjtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgaXNOYXRpdmUoKSB7XG4gICAgcmV0dXJuIGNjLnN5cy5pc05hdGl2ZTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgaXNBbmRyb2lkKCkge1xuICAgIHJldHVybiBjYy5zeXMuaXNOYXRpdmUgJiYgY2Muc3lzLm9zID09IGNjLnN5cy5PU19BTkRST0lEO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBpc0lPUygpIHtcbiAgICByZXR1cm4gY2Muc3lzLmlzTmF0aXZlICYmIGNjLnN5cy5vcyA9PSBjYy5zeXMuT1NfSU9TO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBpc0xvZ1ZlcnNpb24oKSB7XG4gICAgcmV0dXJuIEdhbWVEZWZpbmUuTE9HX1ZFUlNJT05fRU5BQkxFO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBpc051bGxPclVuZGVmaW5lZChvYmplY3QpIHtcbiAgICByZXR1cm4gb2JqZWN0ID09IG51bGwgfHwgb2JqZWN0ID09IHVuZGVmaW5lZDtcbiAgfVxuICBwdWJsaWMgc3RhdGljIGNvbnZlcnRMb3R0ZXJ5TnVtYmVyVG9TdHJpbmcobG90dGVyeU51bWJlcjogbnVtYmVyKTogc3RyaW5nIHtcbiAgICBsZXQgbG90dGVyeU51bWJlclN0cmluZyA9IGxvdHRlcnlOdW1iZXIudG9TdHJpbmcoKTtcbiAgICBsZXQgcmVtYWluWmVyb051bWJlciA9IDYgLSBsb3R0ZXJ5TnVtYmVyU3RyaW5nLmxlbmd0aDtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlbWFpblplcm9OdW1iZXI7IGkrKykge1xuICAgICAgbG90dGVyeU51bWJlclN0cmluZyA9IFwiMFwiICsgbG90dGVyeU51bWJlclN0cmluZztcbiAgICB9XG4gICAgcmV0dXJuIGxvdHRlcnlOdW1iZXJTdHJpbmc7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGlzV2ViTW9iaWxlKCkge1xuICAgIHJldHVybiBjYy5zeXMuaXNCcm93c2VyICYmIGNjLnN5cy5pc01vYmlsZTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZ2V0RGV2aWNlSWQoKSB7XG4gICAgbGV0IGlkID0gY2Muc3lzLmxvY2FsU3RvcmFnZS5nZXRJdGVtKEdhbWVVdGlscy5kZXZpY2VJZEtleSk7XG5cbiAgICBpZiAoaWQgPT0gbnVsbCB8fCBpZCA9PSBcIlwiKSB7XG4gICAgICBsZXQgcmF3SWQgPVxuICAgICAgICBjYy5zeXMub3MgK1xuICAgICAgICBjYy5zeXMub3NNYWluVmVyc2lvbiArXG4gICAgICAgIGNjLnN5cy5sYW5ndWFnZSArXG4gICAgICAgIGNjLnN5cy5sYW5ndWFnZUNvZGUgK1xuICAgICAgICBjYy5zeXMub3NWZXJzaW9uICtcbiAgICAgICAgY2Muc3lzLmlzTmF0aXZlICtcbiAgICAgICAgY2Muc3lzLndpbmRvd1BpeGVsUmVzb2x1dGlvbi5oZWlnaHQgK1xuICAgICAgICBjYy5zeXMucGxhdGZvcm0gK1xuICAgICAgICBjYy5zeXMud2luZG93UGl4ZWxSZXNvbHV0aW9uLndpZHRoICtcbiAgICAgICAgY2Muc3lzLmlzTW9iaWxlICtcbiAgICAgICAgY2Muc3lzLm5vdygpO1xuXG4gICAgICBpZCA9IGJ0b2EocmF3SWQpO1xuICAgICAgY2Muc3lzLmxvY2FsU3RvcmFnZS5zZXRJdGVtKEdhbWVVdGlscy5kZXZpY2VJZEtleSwgaWQpO1xuICAgIH1cbiAgICBHYW1lVXRpbHMuZGV2aWNlSUQgPSBpZDtcbiAgfVxuICBwdWJsaWMgc3RhdGljIGdldFBsYXRmb3JtTmFtZSgpOiBzdHJpbmcge1xuICAgIGlmIChHYW1lVXRpbHMuaXNXZWJNb2JpbGUoKSkge1xuICAgICAgcmV0dXJuIFwiV0VCTVwiO1xuICAgIH0gZWxzZSBpZiAoY2Muc3lzLmlzQnJvd3Nlcikge1xuICAgICAgcmV0dXJuIFwiV0VCUENcIjtcbiAgICB9IGVsc2UgaWYgKGNjLnN5cy5vcyA9PSBjYy5zeXMuT1NfQU5EUk9JRCkge1xuICAgICAgcmV0dXJuIFwiQU5EUk9JRFwiO1xuICAgIH0gZWxzZSBpZiAoY2Muc3lzLm9zID09IGNjLnN5cy5PU19JT1MpIHtcbiAgICAgIHJldHVybiBcIklPU1wiO1xuICAgIH0gZWxzZSByZXR1cm4gXCJVTktOT1dOXCI7XG4gIH1cblxuICBwcml2YXRlIHN0YXRpYyBxclRvU3RyaW5nKHFyY29kZSkge1xuICAgIGxldCBycyA9IFwiXCI7XG4gICAgZm9yIChsZXQgcm93ID0gMDsgcm93IDwgcXJjb2RlLmdldE1vZHVsZUNvdW50KCk7IHJvdysrKSB7XG4gICAgICBmb3IgKGxldCBjb2wgPSAwOyBjb2wgPCBxcmNvZGUuZ2V0TW9kdWxlQ291bnQoKTsgY29sKyspIHtcbiAgICAgICAgcnMgPSBycyArIChxcmNvZGUuaXNEYXJrKHJvdywgY29sKSA/IFwiMVwiIDogXCIwXCIpO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gcnM7XG4gIH1cbn1cbiJdfQ==