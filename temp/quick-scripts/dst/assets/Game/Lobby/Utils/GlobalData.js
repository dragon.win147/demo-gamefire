
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Utils/GlobalData.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8591eaK+vpMdJBcaNjpTQ69', 'GlobalData');
// Game/Lobby/Utils/GlobalData.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventKey_1 = require("@gameloot/client-base-handler/build/EventKey");
var ConnectDefine_1 = require("../Define/ConnectDefine");
var BenTauHandler_1 = require("../Network/BenTauHandler");
var IdentityHandler_1 = require("../Network/IdentityHandler");
var MyAccountHandler_1 = require("../Network/MyAccountHandler");
// import SoundManager from "../Managers/SoundManager";
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GlobalData = /** @class */ (function (_super) {
    __extends(GlobalData, _super);
    function GlobalData() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.map = null;
        return _this;
    }
    GlobalData.prototype.onLoad = function () {
        this.map = new Map();
        cc.game.addPersistRootNode(this.node);
    };
    GlobalData.prototype.set = function (key, val) {
        this.map.set(key, val);
    };
    GlobalData.prototype.get = function (key) {
        return this.map.get(key);
    };
    GlobalData.prototype.clear = function () {
        this.map.clear();
    };
    GlobalData.prototype.setBundleData = function () {
        this.set(EventKey_1.default.ADDRESS_BEN_TAU, ConnectDefine_1.default.addressBenTau);
        this.set(EventKey_1.default.ADDRESS_BEN_TAU_WS, ConnectDefine_1.default.addressBenTauWS);
        this.set(EventKey_1.default.ADDRESS_SAN_BAY, ConnectDefine_1.default.addressSanBay);
        this.set(EventKey_1.default.ADDRESS_RESOURCE, ConnectDefine_1.default.addressResources);
        this.set(EventKey_1.default.FB_APP_ID, ConnectDefine_1.default.fbAppId);
        this.set(EventKey_1.default.GET_BENTAU_HANDLER, BenTauHandler_1.default.getInstance());
        // this.set(EventKey.GET_POTTER_HANDLER, PotterHandler.getInstance());
        // this.set(EventKey.GET_MARKETPLACE_HANDLER, MarketPlaceHandler.getInstance());
        // this.set(EventKey.GET_LEADER_BOARD_HANDLER, LeaderBoardHandler.getInstance());
        // this.set(EventKey.GET_SOUND_MANAGER, SoundManager.getInstance());
        this.set(EventKey_1.default.GET_TOKEN, IdentityHandler_1.default.getInstance().token);
        // this.set(EventKey.IS_LOG_VERSION, GameUtils.isLogVersion());
        this.set(EventKey_1.default.GET_FRAME_PATH, MyAccountHandler_1.default.getInstance().Frames);
        this.set(EventKey_1.default.GET_ICON_PATH, MyAccountHandler_1.default.getInstance().Bases);
        this.set(EventKey_1.default.GET_PLAYAH, MyAccountHandler_1.default.getInstance().Playah);
        // this.set(EventKey.GET_GAME_INFO, MarketPlace.games);
        cc.log("setBundleData Playah ", MyAccountHandler_1.default.getInstance().Playah.toObject());
    };
    GlobalData.prototype.setPaymentBundle = function (bundle) {
        this.set(EventKey_1.default.PAYMENT_BUNDLE, bundle);
    };
    GlobalData.prototype.setTopHuBetLevel = function (topic, gameBetLevel) {
        var mapData = this.get(EventKey_1.default.TOP_HU_BET_LEVEL);
        if (mapData) {
            mapData.set(topic, gameBetLevel);
        }
        else {
            this.set(EventKey_1.default.TOP_HU_BET_LEVEL, new Map().set(topic, gameBetLevel));
        }
    };
    GlobalData.prototype.clearTopHuBetLevel = function (topic) {
        if (topic === void 0) { topic = null; }
        if (topic) {
            var mapData = this.get(EventKey_1.default.TOP_HU_BET_LEVEL);
            if (mapData && mapData.has(topic)) {
                mapData.delete(topic);
            }
        }
        else {
            this.set(EventKey_1.default.TOP_HU_BET_LEVEL, null);
        }
    };
    GlobalData = __decorate([
        ccclass
    ], GlobalData);
    return GlobalData;
}(cc.Component));
exports.default = GlobalData;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L1V0aWxzL0dsb2JhbERhdGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EseUVBQW9FO0FBQ3BFLHlEQUFvRDtBQUNwRCwwREFBcUQ7QUFDckQsOERBQXlEO0FBQ3pELGdFQUEyRDtBQUMzRCx1REFBdUQ7QUFFakQsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBd0MsOEJBQVk7SUFBcEQ7UUFBQSxxRUFnRUM7UUEvREMsU0FBRyxHQUFxQixJQUFJLENBQUM7O0lBK0QvQixDQUFDO0lBN0RDLDJCQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksR0FBRyxFQUFlLENBQUM7UUFDbEMsRUFBRSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVELHdCQUFHLEdBQUgsVUFBSSxHQUFHLEVBQUUsR0FBRztRQUNWLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRUQsd0JBQUcsR0FBSCxVQUFJLEdBQUc7UUFDTCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCwwQkFBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQsa0NBQWEsR0FBYjtRQUNFLElBQUksQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxlQUFlLEVBQUUsdUJBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsa0JBQWtCLEVBQUUsdUJBQWEsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsZUFBZSxFQUFFLHVCQUFhLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLGdCQUFnQixFQUFFLHVCQUFhLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsU0FBUyxFQUFFLHVCQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFcEQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLGtCQUFrQixFQUFFLHVCQUFhLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUNuRSxzRUFBc0U7UUFDdEUsZ0ZBQWdGO1FBQ2hGLGlGQUFpRjtRQUNqRixvRUFBb0U7UUFDcEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSx5QkFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xFLCtEQUErRDtRQUUvRCxJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsY0FBYyxFQUFFLDBCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxhQUFhLEVBQUUsMEJBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFVBQVUsRUFBRSwwQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyRSx1REFBdUQ7UUFDdkQsRUFBRSxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsRUFBRSwwQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUNwRixDQUFDO0lBRUQscUNBQWdCLEdBQWhCLFVBQWlCLE1BQThCO1FBQzdDLElBQUksQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxjQUFjLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELHFDQUFnQixHQUFoQixVQUFpQixLQUFZLEVBQUUsWUFBb0I7UUFDakQsSUFBSSxPQUFPLEdBQXVCLElBQUksQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3RFLElBQUksT0FBTyxFQUFFO1lBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLENBQUM7U0FDbEM7YUFBTTtZQUNMLElBQUksQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLEdBQUcsRUFBaUIsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUM7U0FDeEY7SUFDSCxDQUFDO0lBQ0QsdUNBQWtCLEdBQWxCLFVBQW1CLEtBQW1CO1FBQW5CLHNCQUFBLEVBQUEsWUFBbUI7UUFDcEMsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLE9BQU8sR0FBdUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDdEUsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDakMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN2QjtTQUNGO2FBQU07WUFDTCxJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDM0M7SUFDSCxDQUFDO0lBL0RrQixVQUFVO1FBRDlCLE9BQU87T0FDYSxVQUFVLENBZ0U5QjtJQUFELGlCQUFDO0NBaEVELEFBZ0VDLENBaEV1QyxFQUFFLENBQUMsU0FBUyxHQWdFbkQ7a0JBaEVvQixVQUFVIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVG9waWMgfSBmcm9tIFwiQGdhbWVsb290L3RvcGljL3RvcGljX3BiXCI7XHJcbmltcG9ydCBFdmVudEtleSBmcm9tIFwiQGdhbWVsb290L2NsaWVudC1iYXNlLWhhbmRsZXIvYnVpbGQvRXZlbnRLZXlcIjtcclxuaW1wb3J0IENvbm5lY3REZWZpbmUgZnJvbSBcIi4uL0RlZmluZS9Db25uZWN0RGVmaW5lXCI7XHJcbmltcG9ydCBCZW5UYXVIYW5kbGVyIGZyb20gXCIuLi9OZXR3b3JrL0JlblRhdUhhbmRsZXJcIjtcclxuaW1wb3J0IElkZW50aXR5SGFuZGxlciBmcm9tIFwiLi4vTmV0d29yay9JZGVudGl0eUhhbmRsZXJcIjtcclxuaW1wb3J0IE15QWNjb3VudEhhbmRsZXIgZnJvbSBcIi4uL05ldHdvcmsvTXlBY2NvdW50SGFuZGxlclwiO1xyXG4vLyBpbXBvcnQgU291bmRNYW5hZ2VyIGZyb20gXCIuLi9NYW5hZ2Vycy9Tb3VuZE1hbmFnZXJcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHbG9iYWxEYXRhIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICBtYXA6IE1hcDxzdHJpbmcsIGFueT4gPSBudWxsO1xyXG5cclxuICBvbkxvYWQoKSB7XHJcbiAgICB0aGlzLm1hcCA9IG5ldyBNYXA8c3RyaW5nLCBhbnk+KCk7XHJcbiAgICBjYy5nYW1lLmFkZFBlcnNpc3RSb290Tm9kZSh0aGlzLm5vZGUpO1xyXG4gIH1cclxuXHJcbiAgc2V0KGtleSwgdmFsKSB7XHJcbiAgICB0aGlzLm1hcC5zZXQoa2V5LCB2YWwpO1xyXG4gIH1cclxuXHJcbiAgZ2V0KGtleSkge1xyXG4gICAgcmV0dXJuIHRoaXMubWFwLmdldChrZXkpO1xyXG4gIH1cclxuXHJcbiAgY2xlYXIoKSB7XHJcbiAgICB0aGlzLm1hcC5jbGVhcigpO1xyXG4gIH1cclxuXHJcbiAgc2V0QnVuZGxlRGF0YSgpIHtcclxuICAgIHRoaXMuc2V0KEV2ZW50S2V5LkFERFJFU1NfQkVOX1RBVSwgQ29ubmVjdERlZmluZS5hZGRyZXNzQmVuVGF1KTtcclxuICAgIHRoaXMuc2V0KEV2ZW50S2V5LkFERFJFU1NfQkVOX1RBVV9XUywgQ29ubmVjdERlZmluZS5hZGRyZXNzQmVuVGF1V1MpO1xyXG4gICAgdGhpcy5zZXQoRXZlbnRLZXkuQUREUkVTU19TQU5fQkFZLCBDb25uZWN0RGVmaW5lLmFkZHJlc3NTYW5CYXkpO1xyXG4gICAgdGhpcy5zZXQoRXZlbnRLZXkuQUREUkVTU19SRVNPVVJDRSwgQ29ubmVjdERlZmluZS5hZGRyZXNzUmVzb3VyY2VzKTtcclxuICAgIHRoaXMuc2V0KEV2ZW50S2V5LkZCX0FQUF9JRCwgQ29ubmVjdERlZmluZS5mYkFwcElkKTtcclxuXHJcbiAgICB0aGlzLnNldChFdmVudEtleS5HRVRfQkVOVEFVX0hBTkRMRVIsIEJlblRhdUhhbmRsZXIuZ2V0SW5zdGFuY2UoKSk7XHJcbiAgICAvLyB0aGlzLnNldChFdmVudEtleS5HRVRfUE9UVEVSX0hBTkRMRVIsIFBvdHRlckhhbmRsZXIuZ2V0SW5zdGFuY2UoKSk7XHJcbiAgICAvLyB0aGlzLnNldChFdmVudEtleS5HRVRfTUFSS0VUUExBQ0VfSEFORExFUiwgTWFya2V0UGxhY2VIYW5kbGVyLmdldEluc3RhbmNlKCkpO1xyXG4gICAgLy8gdGhpcy5zZXQoRXZlbnRLZXkuR0VUX0xFQURFUl9CT0FSRF9IQU5ETEVSLCBMZWFkZXJCb2FyZEhhbmRsZXIuZ2V0SW5zdGFuY2UoKSk7XHJcbiAgICAvLyB0aGlzLnNldChFdmVudEtleS5HRVRfU09VTkRfTUFOQUdFUiwgU291bmRNYW5hZ2VyLmdldEluc3RhbmNlKCkpO1xyXG4gICAgdGhpcy5zZXQoRXZlbnRLZXkuR0VUX1RPS0VOLCBJZGVudGl0eUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS50b2tlbik7XHJcbiAgICAvLyB0aGlzLnNldChFdmVudEtleS5JU19MT0dfVkVSU0lPTiwgR2FtZVV0aWxzLmlzTG9nVmVyc2lvbigpKTtcclxuXHJcbiAgICB0aGlzLnNldChFdmVudEtleS5HRVRfRlJBTUVfUEFUSCwgTXlBY2NvdW50SGFuZGxlci5nZXRJbnN0YW5jZSgpLkZyYW1lcyk7XHJcbiAgICB0aGlzLnNldChFdmVudEtleS5HRVRfSUNPTl9QQVRILCBNeUFjY291bnRIYW5kbGVyLmdldEluc3RhbmNlKCkuQmFzZXMpO1xyXG4gICAgdGhpcy5zZXQoRXZlbnRLZXkuR0VUX1BMQVlBSCwgTXlBY2NvdW50SGFuZGxlci5nZXRJbnN0YW5jZSgpLlBsYXlhaCk7XHJcbiAgICAvLyB0aGlzLnNldChFdmVudEtleS5HRVRfR0FNRV9JTkZPLCBNYXJrZXRQbGFjZS5nYW1lcyk7XHJcbiAgICBjYy5sb2coXCJzZXRCdW5kbGVEYXRhIFBsYXlhaCBcIiwgTXlBY2NvdW50SGFuZGxlci5nZXRJbnN0YW5jZSgpLlBsYXlhaC50b09iamVjdCgpKTtcclxuICB9XHJcblxyXG4gIHNldFBheW1lbnRCdW5kbGUoYnVuZGxlOiBjYy5Bc3NldE1hbmFnZXIuQnVuZGxlKSB7XHJcbiAgICB0aGlzLnNldChFdmVudEtleS5QQVlNRU5UX0JVTkRMRSwgYnVuZGxlKTtcclxuICB9XHJcblxyXG4gIHNldFRvcEh1QmV0TGV2ZWwodG9waWM6IFRvcGljLCBnYW1lQmV0TGV2ZWw6IG51bWJlcikge1xyXG4gICAgdmFyIG1hcERhdGE6IE1hcDxUb3BpYywgbnVtYmVyPiA9IHRoaXMuZ2V0KEV2ZW50S2V5LlRPUF9IVV9CRVRfTEVWRUwpO1xyXG4gICAgaWYgKG1hcERhdGEpIHtcclxuICAgICAgbWFwRGF0YS5zZXQodG9waWMsIGdhbWVCZXRMZXZlbCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNldChFdmVudEtleS5UT1BfSFVfQkVUX0xFVkVMLCBuZXcgTWFwPFRvcGljLCBudW1iZXI+KCkuc2V0KHRvcGljLCBnYW1lQmV0TGV2ZWwpKTtcclxuICAgIH1cclxuICB9XHJcbiAgY2xlYXJUb3BIdUJldExldmVsKHRvcGljOiBUb3BpYyA9IG51bGwpIHtcclxuICAgIGlmICh0b3BpYykge1xyXG4gICAgICB2YXIgbWFwRGF0YTogTWFwPFRvcGljLCBudW1iZXI+ID0gdGhpcy5nZXQoRXZlbnRLZXkuVE9QX0hVX0JFVF9MRVZFTCk7XHJcbiAgICAgIGlmIChtYXBEYXRhICYmIG1hcERhdGEuaGFzKHRvcGljKSkge1xyXG4gICAgICAgIG1hcERhdGEuZGVsZXRlKHRvcGljKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZXQoRXZlbnRLZXkuVE9QX0hVX0JFVF9MRVZFTCwgbnVsbCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==