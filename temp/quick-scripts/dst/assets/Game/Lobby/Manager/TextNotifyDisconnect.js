
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Manager/TextNotifyDisconnect.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '208d382SrxAb60SZtzad2zN', 'TextNotifyDisconnect');
// Game/Lobby/Manager/TextNotifyDisconnect.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var NotifyInstance_1 = require("../Base/NotifyInstance");
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
// export class TextNotifyData {
//   public onClick: () => void = null;
//   public msg: string = "";
//   public duration: number = 2;
//   public position: Vec3 = null;
//   constructor(msg: string, duration: number = 2, position: Vec3 = new Vec3(0, 0, 0), onClick: () => void = null) {
//     this.msg = msg;
//     this.duration = duration;
//     this.position = position;
//     this.onClick = onClick;
//   }
// }
var TextNotifyDisconnect = /** @class */ (function (_super) {
    __extends(TextNotifyDisconnect, _super);
    function TextNotifyDisconnect() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.content = null;
        _this.rootNode = null;
        _this.listSprFrame = [];
        _this.data = null;
        return _this;
    }
    TextNotifyDisconnect.prototype.onShow = function (data) {
        var _this = this;
        if (data === void 0) { data = null; }
        this.node.active = true;
        this.data = data;
        var notifyData = data;
        if (notifyData.msg != "")
            this.content.string = notifyData.msg;
        this.node.stopAllActions();
        this.node.runAction(cc.sequence(cc.delayTime(notifyData.duration), cc.callFunc(function () {
            _this.onHide();
        })));
    };
    TextNotifyDisconnect.prototype.afterShow = function () { };
    TextNotifyDisconnect.prototype.beforeShow = function () { };
    TextNotifyDisconnect.prototype.beforeClose = function () { };
    TextNotifyDisconnect.prototype.afterClose = function () { };
    TextNotifyDisconnect.prototype.onHide = function () {
        this.node.active = false;
    };
    TextNotifyDisconnect.prototype.onClick = function () {
        if (this.data) {
            if (this.data.onClick)
                this.data.onClick();
        }
    };
    __decorate([
        property(cc.Label)
    ], TextNotifyDisconnect.prototype, "content", void 0);
    __decorate([
        property(cc.Node)
    ], TextNotifyDisconnect.prototype, "rootNode", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], TextNotifyDisconnect.prototype, "listSprFrame", void 0);
    TextNotifyDisconnect = __decorate([
        ccclass
    ], TextNotifyDisconnect);
    return TextNotifyDisconnect;
}(NotifyInstance_1.default));
exports.default = TextNotifyDisconnect;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L01hbmFnZXIvVGV4dE5vdGlmeURpc2Nvbm5lY3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseURBQW9EO0FBR3BELG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGO0FBRTVFLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBQzVDLGdDQUFnQztBQUNoQyx1Q0FBdUM7QUFDdkMsNkJBQTZCO0FBQzdCLGlDQUFpQztBQUNqQyxrQ0FBa0M7QUFDbEMscUhBQXFIO0FBQ3JILHNCQUFzQjtBQUN0QixnQ0FBZ0M7QUFDaEMsZ0NBQWdDO0FBQ2hDLDhCQUE4QjtBQUM5QixNQUFNO0FBQ04sSUFBSTtBQUdKO0lBQWtELHdDQUFjO0lBQWhFO1FBQUEscUVBNENDO1FBMUNDLGFBQU8sR0FBYSxJQUFJLENBQUM7UUFHekIsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixrQkFBWSxHQUFxQixFQUFFLENBQUM7UUFFNUIsVUFBSSxHQUFtQixJQUFJLENBQUM7O0lBa0N0QyxDQUFDO0lBakNRLHFDQUFNLEdBQWIsVUFBYyxJQUFXO1FBQXpCLGlCQWNDO1FBZGEscUJBQUEsRUFBQSxXQUFXO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLFVBQVUsR0FBRyxJQUFzQixDQUFDO1FBQ3hDLElBQUksVUFBVSxDQUFDLEdBQUcsSUFBSSxFQUFFO1lBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLEdBQUcsQ0FBQztRQUMvRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUNqQixFQUFFLENBQUMsUUFBUSxDQUNULEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUNqQyxFQUFFLENBQUMsUUFBUSxDQUFDO1lBQ1YsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxDQUNILENBQ0YsQ0FBQztJQUNKLENBQUM7SUFFUyx3Q0FBUyxHQUFuQixjQUF1QixDQUFDO0lBRWQseUNBQVUsR0FBcEIsY0FBd0IsQ0FBQztJQUVmLDBDQUFXLEdBQXJCLGNBQXlCLENBQUM7SUFFaEIseUNBQVUsR0FBcEIsY0FBd0IsQ0FBQztJQUVsQixxQ0FBTSxHQUFiO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFFTSxzQ0FBTyxHQUFkO1FBQ0UsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ2IsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU87Z0JBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUM1QztJQUNILENBQUM7SUF6Q0Q7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzt5REFDTTtJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzBEQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7OERBQ1c7SUFSakIsb0JBQW9CO1FBRHhDLE9BQU87T0FDYSxvQkFBb0IsQ0E0Q3hDO0lBQUQsMkJBQUM7Q0E1Q0QsQUE0Q0MsQ0E1Q2lELHdCQUFjLEdBNEMvRDtrQkE1Q29CLG9CQUFvQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBOb3RpZnlJbnN0YW5jZSBmcm9tIFwiLi4vQmFzZS9Ob3RpZnlJbnN0YW5jZVwiO1xuaW1wb3J0IFZlYzMgPSBjYy5WZWMzO1xuaW1wb3J0IHsgVGV4dE5vdGlmeURhdGEgfSBmcm9tIFwiLi9UZXh0Tm90aWZ5XCI7XG4vLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcbi8vIGV4cG9ydCBjbGFzcyBUZXh0Tm90aWZ5RGF0YSB7XG4vLyAgIHB1YmxpYyBvbkNsaWNrOiAoKSA9PiB2b2lkID0gbnVsbDtcbi8vICAgcHVibGljIG1zZzogc3RyaW5nID0gXCJcIjtcbi8vICAgcHVibGljIGR1cmF0aW9uOiBudW1iZXIgPSAyO1xuLy8gICBwdWJsaWMgcG9zaXRpb246IFZlYzMgPSBudWxsO1xuLy8gICBjb25zdHJ1Y3Rvcihtc2c6IHN0cmluZywgZHVyYXRpb246IG51bWJlciA9IDIsIHBvc2l0aW9uOiBWZWMzID0gbmV3IFZlYzMoMCwgMCwgMCksIG9uQ2xpY2s6ICgpID0+IHZvaWQgPSBudWxsKSB7XG4vLyAgICAgdGhpcy5tc2cgPSBtc2c7XG4vLyAgICAgdGhpcy5kdXJhdGlvbiA9IGR1cmF0aW9uO1xuLy8gICAgIHRoaXMucG9zaXRpb24gPSBwb3NpdGlvbjtcbi8vICAgICB0aGlzLm9uQ2xpY2sgPSBvbkNsaWNrO1xuLy8gICB9XG4vLyB9XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUZXh0Tm90aWZ5RGlzY29ubmVjdCBleHRlbmRzIE5vdGlmeUluc3RhbmNlIHtcbiAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICBjb250ZW50OiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIHJvb3ROb2RlOiBjYy5Ob2RlID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuU3ByaXRlRnJhbWUpXG4gIGxpc3RTcHJGcmFtZTogY2MuU3ByaXRlRnJhbWVbXSA9IFtdO1xuXG4gIHByaXZhdGUgZGF0YTogVGV4dE5vdGlmeURhdGEgPSBudWxsO1xuICBwdWJsaWMgb25TaG93KGRhdGEgPSBudWxsKSB7XG4gICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgdGhpcy5kYXRhID0gZGF0YTtcbiAgICBsZXQgbm90aWZ5RGF0YSA9IGRhdGEgYXMgVGV4dE5vdGlmeURhdGE7XG4gICAgaWYgKG5vdGlmeURhdGEubXNnICE9IFwiXCIpIHRoaXMuY29udGVudC5zdHJpbmcgPSBub3RpZnlEYXRhLm1zZztcbiAgICB0aGlzLm5vZGUuc3RvcEFsbEFjdGlvbnMoKTtcbiAgICB0aGlzLm5vZGUucnVuQWN0aW9uKFxuICAgICAgY2Muc2VxdWVuY2UoXG4gICAgICAgIGNjLmRlbGF5VGltZShub3RpZnlEYXRhLmR1cmF0aW9uKSxcbiAgICAgICAgY2MuY2FsbEZ1bmMoKCkgPT4ge1xuICAgICAgICAgIHRoaXMub25IaWRlKCk7XG4gICAgICAgIH0pXG4gICAgICApXG4gICAgKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBhZnRlclNob3coKSB7fVxuXG4gIHByb3RlY3RlZCBiZWZvcmVTaG93KCkge31cblxuICBwcm90ZWN0ZWQgYmVmb3JlQ2xvc2UoKSB7fVxuXG4gIHByb3RlY3RlZCBhZnRlckNsb3NlKCkge31cblxuICBwdWJsaWMgb25IaWRlKCkge1xuICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgfVxuXG4gIHB1YmxpYyBvbkNsaWNrKCkge1xuICAgIGlmICh0aGlzLmRhdGEpIHtcbiAgICAgIGlmICh0aGlzLmRhdGEub25DbGljaykgdGhpcy5kYXRhLm9uQ2xpY2soKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==