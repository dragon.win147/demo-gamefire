
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Manager/UIManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '7cc4fAeojdCYrZ0bU7shopQ', 'UIManager');
// Game/Lobby/Manager/UIManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocalBundlePreload = exports.BundlePreloadData = exports.PopupSystemConfigData = exports.PopupConfigData = void 0;
var PopupConfigData = /** @class */ (function () {
    function PopupConfigData(type, name) {
        this.popupType = type;
        this.popupName = name;
    }
    return PopupConfigData;
}());
exports.PopupConfigData = PopupConfigData;
var PopupSystemConfigData = /** @class */ (function () {
    function PopupSystemConfigData(type, name) {
        this.popupType = type;
        this.popupName = name;
    }
    return PopupSystemConfigData;
}());
exports.PopupSystemConfigData = PopupSystemConfigData;
var BundlePreloadData = /** @class */ (function () {
    function BundlePreloadData(topicConst, listPaths) {
        this.topicConst = -1;
        this.listPopupPaths = [];
        this.topicConst = topicConst;
        this.listPopupPaths = listPaths;
    }
    return BundlePreloadData;
}());
exports.BundlePreloadData = BundlePreloadData;
var LocalBundlePreload = /** @class */ (function () {
    function LocalBundlePreload(name) {
        this.bundleName = name;
    }
    return LocalBundlePreload;
}());
exports.LocalBundlePreload = LocalBundlePreload;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIManager = /** @class */ (function (_super) {
    __extends(UIManager, _super);
    function UIManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._persisNotifyRoot = null;
        _this._persistPopupSystemRoot = null;
        _this._persistPopupRoot = null;
        _this.paymentTopUpNode = null;
        _this.paymentWithDrawNode = null;
        _this.screenTran = null;
        _this._screenInstances = new Map();
        _this.elementTran = null;
        _this._elementInstances = new Map();
        _this.popupTran = null;
        _this._popupInstances = new Map();
        _this.notifyTran = null;
        _this._notifyInstances = new Map();
        _this.popupSystemTran = null;
        _this._currentLoading = 0;
        _this._popupSystemInstances = new Map();
        _this.textTempParent = null;
        _this.uiTempParent = null;
        _this.effTempParent = null;
        return _this;
    }
    UIManager_1 = UIManager;
    UIManager.getInstance = function () {
        if (!UIManager_1._instance) {
            var node = new cc.Node("UIManager");
            UIManager_1._instance = node.addComponent(UIManager_1);
            var self = UIManager_1._instance;
            var canvas = cc.director.getScene().getChildByName("Canvas");
            var size = canvas.getContentSize();
            cc.game.addPersistRootNode(self.node);
            self.node.zIndex = LayerHelper_1.default.uiManager;
            //self.setWidget(self.node, self.node.parent);
            // cc.log("UIManager");
            self._persistPopupRoot = new cc.Node("Popup");
            self._persistPopupRoot.setContentSize(size);
            self._persistPopupRoot.setParent(self.node);
            self._persistPopupRoot.setPosition(cc.Vec3.ZERO);
            self.setWidget(self._persistPopupRoot, self.node);
            self._persisNotifyRoot = new cc.Node("Notify");
            self._persisNotifyRoot.setContentSize(size);
            self._persisNotifyRoot.setParent(self.node);
            self._persisNotifyRoot.setPosition(cc.Vec3.ZERO);
            self.setWidget(self._persisNotifyRoot, self.node);
            self._persistPopupSystemRoot = new cc.Node("PopupSystem");
            self._persistPopupSystemRoot.setContentSize(size);
            self._persistPopupSystemRoot.setParent(self.node);
            self._persistPopupSystemRoot.setPosition(cc.Vec3.ZERO);
            self.setWidget(self._persistPopupSystemRoot, self.node);
            // EventManager.on(
            //   EventManager.ON_SCREEN_CHANGE_SIZE,
            //   () => {
            //     self.updateSize();
            //   },
            //   self
            // );
            cc.systemEvent.on(EventKey_1.default.ON_SCREEN_CHANGE_SIZE_EVENT, self.updateSize, self);
        }
        return UIManager_1._instance;
    };
    UIManager.prototype.onDestroy = function () {
        cc.systemEvent.off(EventKey_1.default.ON_SCREEN_CHANGE_SIZE_EVENT, this.updateSize, this);
    };
    UIManager.prototype.updateSize = function () {
        var self = UIManager_1._instance;
        if (self && self.node) {
            var canvas = cc.director.getScene().getChildByName("Canvas");
            if (canvas) {
                var size = canvas.getContentSize();
                self.node.setContentSize(size);
                self.node.setPosition(canvas.getPosition());
                self._persistPopupRoot.setContentSize(size);
                self._persistPopupRoot.setParent(self.node);
                self._persistPopupRoot.setPosition(cc.Vec3.ZERO);
                self._persisNotifyRoot.setContentSize(size);
                self._persisNotifyRoot.setParent(self.node);
                self._persisNotifyRoot.setPosition(cc.Vec3.ZERO);
                self._persistPopupSystemRoot.setContentSize(size);
                self._persistPopupSystemRoot.setParent(self.node);
                self._persistPopupSystemRoot.setPosition(cc.Vec3.ZERO);
            }
        }
    };
    UIManager.prototype.setWidget = function (node, parent) {
        var widget = node.getComponent(cc.Widget);
        if (widget == null)
            widget = node.addComponent(cc.Widget);
        widget.target = parent;
        widget.isAlignLeft = true;
        widget.isAbsoluteRight = true;
        widget.isAbsoluteTop = true;
        widget.isAlignBottom = true;
        widget.left = 0;
        widget.top = 0;
        widget.bottom = 0;
        widget.right = 0;
    };
    Object.defineProperty(UIManager.prototype, "PaymentTopUpObject", {
        get: function () {
            return this.paymentTopUpNode;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "PaymentWithDrawObject", {
        get: function () {
            return this.paymentWithDrawNode;
        },
        enumerable: false,
        configurable: true
    });
    UIManager.prototype.setPaymentTopUpObject = function (node) {
        this.paymentTopUpNode = node;
    };
    UIManager.prototype.setPaymentWithDrawObject = function (node) {
        this.paymentWithDrawNode = node;
    };
    Object.defineProperty(UIManager.prototype, "LayerUpPopup", {
        get: function () {
            return this._persistPopupRoot;
        },
        enumerable: false,
        configurable: true
    });
    UIManager.prototype.init = function () {
        // cc.log("UIManager...... init");
    };
    Object.defineProperty(UIManager.prototype, "currentScreen", {
        get: function () {
            return this._currentScreen;
        },
        set: function (value) {
            this._currentScreen = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "currentElement", {
        get: function () {
            return this._currentHeader;
        },
        set: function (value) {
            this._currentHeader = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "currentPopup", {
        get: function () {
            return this._currentPopup;
        },
        set: function (value) {
            this._currentPopup = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "currentNotify", {
        get: function () {
            return this._currentNotify;
        },
        set: function (value) {
            this._currentNotify = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "currentPopupSystem", {
        get: function () {
            return this._currentPopupSystem;
        },
        set: function (value) {
            this._currentPopupSystem = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIManager.prototype, "currentPopupSystemName", {
        get: function () {
            return this._currentPopupSystemName;
        },
        enumerable: false,
        configurable: true
    });
    UIManager.prototype.openElement = function (type, name, data, parent, onComplete) {
        var _this = this;
        if (data === void 0) { data = null; }
        if (parent === void 0) { parent = null; }
        if (onComplete === void 0) { onComplete = null; }
        var self = UIManager_1._instance;
        self._currentElementName = name;
        parent = parent ? parent : self.elementTran;
        self.getElementInstance(type, name, function (instance) {
            if (self.currentElement == instance) {
                return self.currentElement;
            }
            self.currentElement = instance;
            instance.open(data);
            instance._close = function () {
                self.currentElement = null;
                _this._currentElementName = "";
            };
            if (onComplete)
                onComplete(instance);
        }, parent);
    };
    UIManager.prototype.closeElement = function (type, name) {
        var self = UIManager_1._instance;
        self.getElementInstance(type, name, function (instance) {
            instance.close();
        });
    };
    UIManager.prototype.getElementInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        if (!self._elementInstances)
            self._elementInstances = new Map();
        if (self._elementInstances.has(name) && self._elementInstances.get(name) != null && self._elementInstances.get(name).node != null) {
            var element = self._elementInstances.get(name);
            element.node.parent = null;
            _parent.addChild(element.node);
            onComplete(element);
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/Element/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                _parent.addChild(newNode);
                self._elementInstances.set(name, instance);
                onComplete(instance);
            });
        }
    };
    UIManager.prototype.openScreen = function (type, name, data, parent) {
        if (data === void 0) { data = null; }
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        parent = parent ? parent : self.screenTran;
        self.getSceneInstance(type, name, function (instance) {
            if (self.currentScreen != null) {
                self.currentScreen.close();
            }
            self.currentScreen = instance;
            instance.open(data);
        }, parent);
    };
    UIManager.prototype.getSceneInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        if (!self._screenInstances)
            self._screenInstances = new Map();
        if (self._screenInstances.has(name) && self._screenInstances.get(name) != null && self._screenInstances.get(name).node != null) {
            var scene = self._screenInstances.get(name);
            scene.node.parent = null;
            _parent.addChild(scene.node);
            onComplete(scene);
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/Screen/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                _parent.addChild(newNode);
                self._screenInstances.set(name, instance);
                onComplete(instance);
            });
        }
    };
    UIManager.prototype.preloadListPopup = function (listPopups, onProgress, onComplete) {
        var _this = this;
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (listPopups.length == 0) {
            if (onComplete)
                onComplete();
            return;
        }
        var finish = 0;
        var total = listPopups.length;
        listPopups.forEach(function (data) {
            _this.preLoadPopup(data.popupType, data.popupName, function (err, item) {
                if (err) {
                    // cc.log(err);
                }
                finish++;
                if (onProgress) {
                    onProgress(finish, total, item);
                }
                if (finish == total) {
                    if (onComplete)
                        onComplete();
                }
            });
        });
    };
    UIManager.prototype.preLoadPopup = function (type, name, callback) {
        if (callback === void 0) { callback = null; }
        var self = UIManager_1._instance;
        if (!self._popupInstances)
            self._popupInstances = new Map();
        if (self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null) {
            if (callback) {
                callback(null, self._popupInstances.get(name));
            }
            return;
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/Popup/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                self._popupInstances.set(name, instance);
                if (callback) {
                    callback(null, self._popupInstances.get(name));
                }
            });
        }
    };
    UIManager.prototype.preloadListPopupSystem = function (listPopups, onProgress, onComplete) {
        var _this = this;
        if (onProgress === void 0) { onProgress = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (listPopups.length == 0) {
            if (onComplete)
                onComplete();
            return;
        }
        var finish = 0;
        var total = listPopups.length;
        listPopups.forEach(function (data) {
            _this.preLoadPopupSystem(data.popupType, data.popupName, function (err, item) {
                if (err) {
                    // cc.log(err);
                }
                finish++;
                if (onProgress) {
                    onProgress(finish, total, item);
                }
                if (finish == total) {
                    if (onComplete)
                        onComplete();
                }
            });
        });
    };
    UIManager.prototype.preLoadPopupSystem = function (type, name, callback) {
        if (callback === void 0) { callback = null; }
        var self = UIManager_1._instance;
        if (!self._popupSystemInstances)
            self._popupSystemInstances = new Map();
        if (self._popupSystemInstances.has(name) && self._popupSystemInstances.get(name) != null && self._popupSystemInstances.get(name).node != null) {
            if (callback) {
                callback(null, self._popupSystemInstances.get(name));
            }
            return;
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/PopupSystem/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                self._popupSystemInstances.set(name, instance);
                if (callback) {
                    callback(null, self._popupSystemInstances.get(name));
                }
            });
        }
    };
    UIManager.prototype.openPopup = function (type, name, data, onComplete, parent, showLoadingPopUp) {
        if (data === void 0) { data = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (parent === void 0) { parent = null; }
        if (showLoadingPopUp === void 0) { showLoadingPopUp = true; }
        var self = UIManager_1._instance;
        parent = parent ? parent : self.popupTran;
        if (name == self._currentPopupName && self._currentPopup.node) {
            if (onComplete)
                onComplete(self._currentPopup);
            return;
        }
        self._currentPopupName = name;
        self.getPopupInstance(type, name, function (instance) {
            if (self.currentPopup == instance) {
                if (onComplete)
                    onComplete(self.currentPopup);
                return;
            }
            self.currentPopup = instance;
            instance.open(data);
            instance._close = function () {
                self.currentPopup = null;
                self._currentPopupName = "";
            };
            if (onComplete)
                onComplete(self.currentPopup);
        }, parent, showLoadingPopUp);
    };
    UIManager.prototype.openPopupWithErrorHandler = function (type, name, data, parent, showLoadingPopUp) {
        if (data === void 0) { data = null; }
        if (parent === void 0) { parent = null; }
        if (showLoadingPopUp === void 0) { showLoadingPopUp = true; }
        var self = UIManager_1._instance;
        parent = parent ? parent : self.popupTran;
        if (name == self._currentPopupName && self._currentPopup != null && self._currentPopup.node) {
            return new Promise(function (resolve) {
                resolve(self._currentPopup);
            });
        }
        self._currentPopupName = name;
        return self
            .getPopupInstanceWithError(type, name, parent, showLoadingPopUp)
            .then(function (instance) {
            if (self.currentPopup == instance) {
                return new Promise(function (resolve) {
                    resolve(self.currentPopup);
                });
            }
            self.currentPopup = instance;
            instance.open(data);
            instance._close = function () {
                self.currentPopup = null;
                self._currentPopupName = "";
            };
            return instance;
        })
            .catch(function (error) {
            if (showLoadingPopUp) {
                self._currentPopupName = "";
                UIManager_1.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
            }
            // UIManager.getInstance().openPopupSystem(WarningPopupSystem, GameDefine.WarningPopupSystem, PortalText.POR_MESS_NETWORK_DISCONNECT);
        });
    };
    UIManager.prototype.hasPopupInstance = function (name) {
        return UIManager_1._instance._popupInstances.has(name);
    };
    UIManager.prototype.closePopup = function (type, name) {
        var self = UIManager_1._instance;
        self.getPopupInstance(type, name, function (instance) {
            instance.closeInstance();
        });
    };
    UIManager.prototype.getPopupInstance = function (type, name, onComplete, parent, showLoadingPopUp) {
        var _this = this;
        if (parent === void 0) { parent = null; }
        if (showLoadingPopUp === void 0) { showLoadingPopUp = true; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        if (!self._popupInstances)
            self._popupInstances = new Map();
        if (self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null) {
            var popup = self._popupInstances.get(name);
            popup.node.parent = null;
            _parent.addChild(popup.node);
            onComplete(popup);
        }
        else {
            if (showLoadingPopUp) {
                self.openPopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, true, null, null, function (popupLoading) {
                    _this.loadPopUpPrefab(type, name, onComplete, _parent);
                });
            }
            else {
                this.loadPopUpPrefab(type, name, onComplete, _parent);
            }
        }
    };
    UIManager.prototype.getPopupInstanceWithError = function (type, name, parent, showLoadingPopUp) {
        if (parent === void 0) { parent = null; }
        if (showLoadingPopUp === void 0) { showLoadingPopUp = true; }
        return __awaiter(this, void 0, void 0, function () {
            var self, _parent, popup_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        self = UIManager_1._instance;
                        _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
                        if (!self._popupInstances)
                            self._popupInstances = new Map();
                        if (!(self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null)) return [3 /*break*/, 1];
                        popup_1 = self._popupInstances.get(name);
                        popup_1.node.parent = null;
                        _parent.addChild(popup_1.node);
                        return [2 /*return*/, new Promise(function (resolve) {
                                resolve(popup_1);
                            })];
                    case 1:
                        if (!showLoadingPopUp) return [3 /*break*/, 3];
                        return [4 /*yield*/, self.openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, true, null, null)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, this.loadPopUpPrefabWithErrorHandler(type, name, _parent)];
                    case 3: return [2 /*return*/, this.loadPopUpPrefabWithErrorHandler(type, name, _parent)];
                }
            });
        });
    };
    UIManager.prototype.loadPopUpPrefab = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        ResourceManager_1.default.loadPrefab("Prefabs/Popup/" + name, function (prefab) {
            var newNode = cc.instantiate(prefab);
            var instance = newNode.getComponent(type);
            parent.addChild(newNode);
            self._popupInstances.set(name, instance);
            self.closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
            onComplete(instance);
        });
    };
    UIManager.prototype.loadPopUpPrefabWithErrorHandler = function (type, name, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        return ResourceManager_1.default.loadPrefabWithErrorHandler("Prefabs/Popup/" + name).then(function (prefab) {
            var newNode = cc.instantiate(prefab);
            var instance = newNode.getComponent(type);
            parent.addChild(newNode);
            self._popupInstances.set(name, instance);
            self.closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
            return instance;
        });
    };
    UIManager.prototype.openNotify = function (type, name, data, parent) {
        if (data === void 0) { data = null; }
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        parent = parent ? parent : self.notifyTran;
        if (name == self._currentNotifyName)
            return;
        self._currentNotifyName = name;
        self.getNotifyInstance(type, name, function (instance) {
            if (self.currentNotify == instance)
                return;
            self.currentNotify = instance;
            instance.open(data);
            instance._close = function () {
                self.currentNotify = null;
                self._currentNotifyName = "";
            };
        }, parent);
    };
    UIManager.prototype.getNotifyInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        if (name == GameDefine_1.default.TextNotify)
            _parent = self._persisNotifyRoot;
        if (!self._notifyInstances)
            self._notifyInstances = new Map();
        if (self._notifyInstances.has(name) && self._notifyInstances.get(name) != null && self._notifyInstances.get(name).node != null) {
            var notify = self._notifyInstances.get(name);
            if (name == GameDefine_1.default.TextNotify) {
            }
            else {
                notify.node.parent = null;
                _parent.addChild(notify.node);
            }
            onComplete(notify);
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/Notify/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                _parent.addChild(newNode);
                self._notifyInstances.set(name, instance);
                onComplete(instance);
            });
        }
    };
    UIManager.prototype.openPopupSystem = function (type, name, data, onYes, onNo, onComplete, parent) {
        if (data === void 0) { data = null; }
        if (onYes === void 0) { onYes = null; }
        if (onNo === void 0) { onNo = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        // self.popupSystemTran = cc.director.getScene().getComponentInChildren(MiniGameManager)?.popupSystemTran;
        parent = parent ? parent : self.popupSystemTran;
        if (name == GameDefine_1.default.LoadingPopupSystem) {
            self._currentLoading++;
        }
        if (name == self._currentPopupSystemName) {
            return;
        }
        if (self._currentPopupSystem && self._currentPopupSystem.node) {
            if (onComplete)
                onComplete(self._currentPopupSystem);
            return;
        }
        self._currentPopupSystemName = name;
        self.getPopupSystemInstance(type, name, function (instance) {
            if (self._currentPopupSystem == instance) {
                if (onComplete)
                    onComplete(self._currentPopupSystem);
                return;
            }
            self._currentPopupSystem = instance;
            instance.open(data, onYes, onNo);
            instance._close = function () {
                self._currentPopupSystem = null;
                self._currentPopupSystemName = "";
            };
            if (onComplete)
                onComplete(instance);
        }, parent);
    };
    UIManager.prototype.openPopupSystemV2 = function (type, name, data, onYes, onNo, parent) {
        if (data === void 0) { data = null; }
        if (onYes === void 0) { onYes = null; }
        if (onNo === void 0) { onNo = null; }
        if (parent === void 0) { parent = null; }
        return new Promise(function (resolve) {
            var self = UIManager_1._instance;
            // self.popupSystemTran = cc.director.getScene().getComponentInChildren(MiniGameManager)?.popupSystemTran;
            parent = parent ? parent : self.popupSystemTran;
            if (name == GameDefine_1.default.LoadingPopupSystem) {
                self._currentLoading++;
            }
            if (name == self._currentPopupSystemName && self._currentPopupSystem && self._currentPopupSystem.node) {
                resolve(self._currentPopupSystem);
                return;
            }
            self._currentPopupSystemName = name;
            self.getPopupSystemInstance(type, name, function (instance) {
                if (self._currentPopupSystem == instance) {
                    resolve(self._currentPopupSystem);
                    return;
                }
                self._currentPopupSystem = instance;
                instance.open(data, onYes, onNo);
                instance._close = function () {
                    self._currentPopupSystem = null;
                    self._currentPopupSystemName = "";
                };
                resolve(instance);
            }, parent);
        });
    };
    UIManager.prototype.closeAllPopupSystem = function () {
        var self = UIManager_1._instance;
        self._popupSystemInstances.forEach(function (p) {
            if (p != null && p.node != null)
                p.closeInstance();
            self._currentLoading = 0;
            self._currentPopupSystemName = "";
        });
    };
    UIManager.prototype.closeAllPopup = function (shouldCloseProfilePopup) {
        if (shouldCloseProfilePopup === void 0) { shouldCloseProfilePopup = true; }
        var self = UIManager_1._instance;
        self._popupInstances.forEach(function (p, name) {
            if (!shouldCloseProfilePopup) {
            }
            if (p != null && p.node != null)
                p.closeInstance();
            self._currentPopupName = "";
        });
        self.closeAllPopupSystem();
    };
    UIManager.prototype.closePaymentPopup = function () {
        if (this.paymentWithDrawNode) {
            if (this.paymentWithDrawNode.active)
                this.paymentWithDrawNode.active = false;
        }
        if (this.paymentTopUpNode) {
            if (this.paymentTopUpNode.active)
                this.paymentTopUpNode.active = false;
        }
    };
    UIManager.prototype.closePopupSystem = function (type, name) {
        var self = UIManager_1._instance;
        if (name == GameDefine_1.default.LoadingPopupSystem) {
            self._currentLoading--;
            if (self._currentLoading < 0)
                self._currentLoading = 0;
        }
        self.getPopupSystemInstance(type, name, function (instance) {
            if (self._currentLoading == 0) {
                instance.closeInstance();
                self._currentPopupSystemName = "";
            }
        });
    };
    UIManager.prototype.getPopupSystemInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        _parent = self._persistPopupSystemRoot;
        if (!self._popupSystemInstances)
            self._popupSystemInstances = new Map();
        if (self._popupSystemInstances.has(name) && self._popupSystemInstances.get(name) != null && self._popupSystemInstances.get(name).node != null) {
            var popup = self._popupSystemInstances.get(name);
            popup.node.parent = null;
            _parent.addChild(popup.node);
            onComplete(popup);
        }
        else {
            ResourceManager_1.default.loadPrefab("Prefabs/PopupSystem/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                _parent.addChild(newNode);
                self._popupSystemInstances.set(name, instance);
                onComplete(instance);
            });
        }
    };
    UIManager.prototype.openActionElementPopup = function (type, name, data, onComplete, parent) {
        if (data === void 0) { data = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (parent === void 0) { parent = null; }
        // cc.log("open action popup");
        var self = UIManager_1._instance;
        parent = parent ? parent : self.popupTran;
        // if (name == self._currentPopupName) {
        //   if (onComplete) onComplete(self._currentPopup);
        //   return;
        // }
        self._currentPopupName = name;
        self.getActionElementPopupInstance(type, name, function (instance) {
            if (self.currentPopup == instance) {
                if (onComplete)
                    onComplete(self.currentPopup);
                return;
            }
            self.currentPopup = instance;
            instance.open(data);
            instance._close = function () {
                self.currentPopup = null;
                self._currentPopupName = "";
            };
            if (onComplete)
                onComplete(self.currentPopup);
        }, parent);
    };
    UIManager.prototype.closeActionElementPopup = function (type, name) {
        var self = UIManager_1._instance;
        self.getActionElementPopupInstance(type, name, function (instance) {
            instance.closeInstance();
        });
    };
    UIManager.prototype.getActionElementPopupInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = UIManager_1._instance;
        var _parent = parent == null ? cc.director.getScene().getChildByName("Canvas") : parent;
        if (!self._popupInstances)
            self._popupInstances = new Map();
        if (self._popupInstances.has(name) && self._popupInstances.get(name) != null && self._popupInstances.get(name).node != null) {
            var popup = self._popupInstances.get(name);
            popup.node.parent = null;
            _parent.addChild(popup.node);
            onComplete(popup);
        }
        else {
            // self.openPopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem, true, null, null, (popupLoading) => {
            ResourceManager_1.default.loadPrefab("Prefabs/Popup/" + name, function (prefab) {
                var newNode = cc.instantiate(prefab);
                var instance = newNode.getComponent(type);
                _parent.addChild(newNode);
                self._popupInstances.set(name, instance);
                // self.closePopupSystem(LoadingPopupSystem, GameDefine.LoadingPopupSystem);
                onComplete(instance);
            });
            // });
        }
    };
    UIManager.prototype.setParentText = function (node, parent) {
        if (!this.textTempParent) {
            this.textTempParent = new cc.Node("textTemp");
        }
        else {
            this.textTempParent = new cc.Node("textTemp");
        }
    };
    UIManager.prototype.setParentUI = function (node) {
        var parent = new cc.Node("ui");
    };
    UIManager.prototype.setParentEff = function (node) {
        var parent = new cc.Node("eff");
    };
    var UIManager_1;
    __decorate([
        property(cc.Node)
    ], UIManager.prototype, "screenTran", void 0);
    __decorate([
        property(cc.Node)
    ], UIManager.prototype, "elementTran", void 0);
    __decorate([
        property(cc.Node)
    ], UIManager.prototype, "popupTran", void 0);
    __decorate([
        property(cc.Node)
    ], UIManager.prototype, "notifyTran", void 0);
    __decorate([
        property(cc.Node)
    ], UIManager.prototype, "popupSystemTran", void 0);
    UIManager = UIManager_1 = __decorate([
        ccclass
    ], UIManager);
    return UIManager;
}(cc.Component));
exports.default = UIManager;
var GameDefine_1 = require("./../Define/GameDefine");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var LayerHelper_1 = require("../Utils/LayerHelper");
var ResourceManager_1 = require("./ResourceManager");
var EventKey_1 = require("@gameloot/client-base-handler/build/EventKey");

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L01hbmFnZXIvVUlNYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtJQUlFLHlCQUFZLElBQTBCLEVBQUUsSUFBWTtRQUNsRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDO0lBQ0gsc0JBQUM7QUFBRCxDQVJBLEFBUUMsSUFBQTtBQVJZLDBDQUFlO0FBVTVCO0lBSUUsK0JBQVksSUFBZ0MsRUFBRSxJQUFZO1FBQ3hELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQ3hCLENBQUM7SUFDSCw0QkFBQztBQUFELENBUkEsQUFRQyxJQUFBO0FBUlksc0RBQXFCO0FBVWxDO0lBSUUsMkJBQVksVUFBVSxFQUFFLFNBQW1CO1FBSHBDLGVBQVUsR0FBVyxDQUFDLENBQUMsQ0FBQztRQUN4QixtQkFBYyxHQUFhLEVBQUUsQ0FBQztRQUduQyxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLFNBQVMsQ0FBQztJQUNsQyxDQUFDO0lBQ0gsd0JBQUM7QUFBRCxDQVJBLEFBUUMsSUFBQTtBQVJZLDhDQUFpQjtBQVU5QjtJQUVFLDRCQUFZLElBQVk7UUFDdEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDekIsQ0FBQztJQUNILHlCQUFDO0FBQUQsQ0FMQSxBQUtDLElBQUE7QUFMWSxnREFBa0I7QUFPekIsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBdUMsNkJBQVk7SUFBbkQ7UUFBQSxxRUF3eUJDO1FBN3NCUyx1QkFBaUIsR0FBWSxJQUFJLENBQUM7UUFDbEMsNkJBQXVCLEdBQVksSUFBSSxDQUFDO1FBQ3hDLHVCQUFpQixHQUFZLElBQUksQ0FBQztRQUNsQyxzQkFBZ0IsR0FBWSxJQUFJLENBQUM7UUFDakMseUJBQW1CLEdBQVksSUFBSSxDQUFDO1FBa0M1QyxnQkFBVSxHQUFZLElBQUksQ0FBQztRQUVuQixzQkFBZ0IsR0FBZ0MsSUFBSSxHQUFHLEVBQTBCLENBQUM7UUFZMUYsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFFcEIsdUJBQWlCLEdBQWlDLElBQUksR0FBRyxFQUEyQixDQUFDO1FBVzdGLGVBQVMsR0FBWSxJQUFJLENBQUM7UUFFbEIscUJBQWUsR0FBK0IsSUFBSSxHQUFHLEVBQXlCLENBQUM7UUFhdkYsZ0JBQVUsR0FBWSxJQUFJLENBQUM7UUFFbkIsc0JBQWdCLEdBQWdDLElBQUksR0FBRyxFQUEwQixDQUFDO1FBZ0IxRixxQkFBZSxHQUFZLElBQUksQ0FBQztRQUN4QixxQkFBZSxHQUFXLENBQUMsQ0FBQztRQUM1QiwyQkFBcUIsR0FBcUMsSUFBSSxHQUFHLEVBQStCLENBQUM7UUF5bEJqRyxvQkFBYyxHQUFZLElBQUksQ0FBQztRQUMvQixrQkFBWSxHQUFZLElBQUksQ0FBQztRQUM3QixtQkFBYSxHQUFZLElBQUksQ0FBQzs7SUFjeEMsQ0FBQztrQkF4eUJvQixTQUFTO0lBR3JCLHFCQUFXLEdBQWxCO1FBQ0UsSUFBSSxDQUFDLFdBQVMsQ0FBQyxTQUFTLEVBQUU7WUFDeEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3BDLFdBQVMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFTLENBQUMsQ0FBQztZQUNuRCxJQUFJLElBQUksR0FBRyxXQUFTLENBQUMsU0FBUyxDQUFDO1lBRS9CLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdELElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUVuQyxFQUFFLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxxQkFBVyxDQUFDLFNBQVMsQ0FBQztZQUN6Qyw4Q0FBOEM7WUFDOUMsdUJBQXVCO1lBRXZCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRWxELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDL0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRWxELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDMUQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXhELG1CQUFtQjtZQUNuQix3Q0FBd0M7WUFDeEMsWUFBWTtZQUNaLHlCQUF5QjtZQUN6QixPQUFPO1lBQ1AsU0FBUztZQUNULEtBQUs7WUFFTCxFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDaEY7UUFDRCxPQUFPLFdBQVMsQ0FBQyxTQUFTLENBQUM7SUFDN0IsQ0FBQztJQUVTLDZCQUFTLEdBQW5CO1FBQ0UsRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2xGLENBQUM7SUFFTyw4QkFBVSxHQUFsQjtRQUNFLElBQUksSUFBSSxHQUFHLFdBQVMsQ0FBQyxTQUFTLENBQUM7UUFDL0IsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNyQixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM3RCxJQUFJLE1BQU0sRUFBRTtnQkFDVixJQUFJLElBQUksR0FBRyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBRW5DLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztnQkFFNUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFakQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFakQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN4RDtTQUNGO0lBQ0gsQ0FBQztJQUVPLDZCQUFTLEdBQWpCLFVBQWtCLElBQWEsRUFBRSxNQUFlO1FBQzlDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFDLElBQUksTUFBTSxJQUFJLElBQUk7WUFBRSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUQsTUFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDdkIsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDMUIsTUFBTSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDOUIsTUFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDNUIsTUFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDNUIsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7UUFDaEIsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDZixNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNsQixNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztJQUNuQixDQUFDO0lBUUQsc0JBQVcseUNBQWtCO2FBQTdCO1lBQ0UsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDL0IsQ0FBQzs7O09BQUE7SUFFRCxzQkFBVyw0Q0FBcUI7YUFBaEM7WUFDRSxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUNsQyxDQUFDOzs7T0FBQTtJQUVNLHlDQUFxQixHQUE1QixVQUE2QixJQUFhO1FBQ3hDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7SUFDL0IsQ0FBQztJQUVNLDRDQUF3QixHQUEvQixVQUFnQyxJQUFhO1FBQzNDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7SUFDbEMsQ0FBQztJQUVELHNCQUFXLG1DQUFZO2FBQXZCO1lBQ0UsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDaEMsQ0FBQzs7O09BQUE7SUFFTSx3QkFBSSxHQUFYO1FBQ0Usa0NBQWtDO0lBQ3BDLENBQUM7SUFFRCxzQkFBVyxvQ0FBYTthQUF4QjtZQUNFLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUM3QixDQUFDO2FBQ0QsVUFBeUIsS0FBSztZQUM1QixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUM5QixDQUFDOzs7T0FIQTtJQVlELHNCQUFXLHFDQUFjO2FBQXpCO1lBQ0UsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQzdCLENBQUM7YUFDRCxVQUEwQixLQUFLO1lBQzdCLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzlCLENBQUM7OztPQUhBO0lBV0Qsc0JBQVcsbUNBQVk7YUFBdkI7WUFDRSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDNUIsQ0FBQzthQUNELFVBQXdCLEtBQUs7WUFDM0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDN0IsQ0FBQzs7O09BSEE7SUFhRCxzQkFBVyxvQ0FBYTthQUF4QjtZQUNFLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUM3QixDQUFDO2FBQ0QsVUFBeUIsS0FBSztZQUM1QixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUM5QixDQUFDOzs7T0FIQTtJQVlELHNCQUFXLHlDQUFrQjthQUE3QjtZQUNFLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDO1FBQ2xDLENBQUM7YUFDRCxVQUE4QixLQUFLO1lBQ2pDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDbkMsQ0FBQzs7O09BSEE7SUFLRCxzQkFBVyw2Q0FBc0I7YUFBakM7WUFDRSxPQUFPLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztRQUN0QyxDQUFDOzs7T0FBQTtJQU9NLCtCQUFXLEdBQWxCLFVBQW1CLElBQTRCLEVBQUUsSUFBWSxFQUFFLElBQVcsRUFBRSxNQUFzQixFQUFFLFVBQXFDO1FBQXpJLGlCQXVCQztRQXZCOEQscUJBQUEsRUFBQSxXQUFXO1FBQUUsdUJBQUEsRUFBQSxhQUFzQjtRQUFFLDJCQUFBLEVBQUEsaUJBQXFDO1FBQ3ZJLElBQUksSUFBSSxHQUFHLFdBQVMsQ0FBQyxTQUFTLENBQUM7UUFFL0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUNoQyxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDNUMsSUFBSSxDQUFDLGtCQUFrQixDQUNyQixJQUFJLEVBQ0osSUFBSSxFQUNKLFVBQUMsUUFBUTtZQUNQLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxRQUFRLEVBQUU7Z0JBQ25DLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQzthQUM1QjtZQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDO1lBQy9CLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIsUUFBUSxDQUFDLE1BQU0sR0FBRztnQkFDaEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7Z0JBQzNCLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUM7WUFDaEMsQ0FBQyxDQUFDO1lBQ0YsSUFBSSxVQUFVO2dCQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN2QyxDQUFDLEVBQ0QsTUFBTSxDQUNQLENBQUM7SUFDSixDQUFDO0lBRU0sZ0NBQVksR0FBbkIsVUFBb0IsSUFBNEIsRUFBRSxJQUFZO1FBQzVELElBQUksSUFBSSxHQUFHLFdBQVMsQ0FBQyxTQUFTLENBQUM7UUFFL0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBQyxRQUFRO1lBQzNDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNuQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxzQ0FBa0IsR0FBekIsVUFBMEIsSUFBNEIsRUFBRSxJQUFZLEVBQUUsVUFBOEIsRUFBRSxNQUFzQjtRQUF0Qix1QkFBQSxFQUFBLGFBQXNCO1FBQzFILElBQUksSUFBSSxHQUFHLFdBQVMsQ0FBQyxTQUFTLENBQUM7UUFDL0IsSUFBTSxPQUFPLEdBQUcsTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUMxRixJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQjtZQUFFLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLEdBQUcsRUFBMkIsQ0FBQztRQUN6RixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO1lBQ2pJLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0MsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzNCLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9CLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNyQjthQUFNO1lBQ0wseUJBQWUsQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxFQUFFLFVBQVUsTUFBTTtnQkFDcEUsSUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQXVCLENBQUM7Z0JBQzdELElBQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzVDLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUUzQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdkIsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFTSw4QkFBVSxHQUFqQixVQUFrQixJQUEyQixFQUFFLElBQVksRUFBRSxJQUFXLEVBQUUsTUFBc0I7UUFBbkMscUJBQUEsRUFBQSxXQUFXO1FBQUUsdUJBQUEsRUFBQSxhQUFzQjtRQUM5RixJQUFJLElBQUksR0FBRyxXQUFTLENBQUMsU0FBUyxDQUFDO1FBQy9CLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUMzQyxJQUFJLENBQUMsZ0JBQWdCLENBQ25CLElBQUksRUFDSixJQUFJLEVBQ0osVUFBQyxRQUFRO1lBQ1AsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksRUFBRTtnQkFDOUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUM1QjtZQUNELElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDO1lBRTlCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEIsQ0FBQyxFQUNELE1BQU0sQ0FDUCxDQUFDO0lBQ0osQ0FBQztJQUVNLG9DQUFnQixHQUF2QixVQUF3QixJQUEyQixFQUFFLElBQVksRUFBRSxVQUE4QyxFQUFFLE1BQXNCO1FBQXRCLHVCQUFBLEVBQUEsYUFBc0I7UUFDdkksSUFBSSxJQUFJLEdBQUcsV0FBUyxDQUFDLFNBQVMsQ0FBQztRQUMvQixJQUFNLE9BQU8sR0FBRyxNQUFNLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzFGLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCO1lBQUUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksR0FBRyxFQUEwQixDQUFDO1FBQ3RGLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7WUFDOUgsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDekIsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDN0IsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25CO2FBQU07WUFDTCx5QkFBZSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLEVBQUUsVUFBVSxNQUFNO2dCQUNuRSxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBdUIsQ0FBQztnQkFDM0QsSUFBSSxRQUFRLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDMUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBRTFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN2QixDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUVNLG9DQUFnQixHQUF2QixVQUF3QixVQUE2QixFQUFFLFVBQStFLEVBQUUsVUFBNkI7UUFBckssaUJBcUJDO1FBckJzRCwyQkFBQSxFQUFBLGlCQUErRTtRQUFFLDJCQUFBLEVBQUEsaUJBQTZCO1FBQ25LLElBQUksVUFBVSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDMUIsSUFBSSxVQUFVO2dCQUFFLFVBQVUsRUFBRSxDQUFDO1lBQzdCLE9BQU87U0FDUjtRQUNELElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNmLElBQUksS0FBSyxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUM7UUFDOUIsVUFBVSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7WUFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsVUFBQyxHQUFHLEVBQUUsSUFBbUI7Z0JBQ3pFLElBQUksR0FBRyxFQUFFO29CQUNQLGVBQWU7aUJBQ2hCO2dCQUNELE1BQU0sRUFBRSxDQUFDO2dCQUNULElBQUksVUFBVSxFQUFFO29CQUNkLFVBQVUsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO2lCQUNqQztnQkFDRCxJQUFJLE1BQU0sSUFBSSxLQUFLLEVBQUU7b0JBQ25CLElBQUksVUFBVTt3QkFBRSxVQUFVLEVBQUUsQ0FBQztpQkFDOUI7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLGdDQUFZLEdBQW5CLFVBQW9CLElBQTBCLEVBQUUsSUFBWSxFQUFFLFFBQW1EO1FBQW5ELHlCQUFBLEVBQUEsZUFBbUQ7UUFDL0csSUFBSSxJQUFJLEdBQUcsV0FBUyxDQUFDLFNBQVMsQ0FBQztRQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWU7WUFBRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksR0FBRyxFQUF5QixDQUFDO1FBQ25GLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7WUFDM0gsSUFBSSxRQUFRLEVBQUU7Z0JBQ1osUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQ2hEO1lBQ0QsT0FBTztTQUNSO2FBQU07WUFDTCx5QkFBZSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLEVBQUUsVUFBVSxNQUFNO2dCQUNsRSxJQUFNLE9BQU8sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBdUIsQ0FBQztnQkFDN0QsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUN6QyxJQUFJLFFBQVEsRUFBRTtvQkFDWixRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ2hEO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFTSwwQ0FBc0IsR0FBN0IsVUFDRSxVQUFtQyxFQUNuQyxVQUFxRixFQUNyRixVQUE2QjtRQUgvQixpQkF5QkM7UUF2QkMsMkJBQUEsRUFBQSxpQkFBcUY7UUFDckYsMkJBQUEsRUFBQSxpQkFBNkI7UUFFN0IsSUFBSSxVQUFVLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUMxQixJQUFJLFVBQVU7Z0JBQUUsVUFBVSxFQUFFLENBQUM7WUFDN0IsT0FBTztTQUNSO1FBQ0QsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ2YsSUFBSSxLQUFLLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQztRQUM5QixVQUFVLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtZQUN0QixLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLFVBQUMsR0FBRyxFQUFFLElBQXlCO2dCQUNyRixJQUFJLEdBQUcsRUFBRTtvQkFDUCxlQUFlO2lCQUNoQjtnQkFDRCxNQUFNLEVBQUUsQ0FBQztnQkFDVCxJQUFJLFVBQVUsRUFBRTtvQkFDZCxVQUFVLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDakM7Z0JBQ0QsSUFBSSxNQUFNLElBQUksS0FBSyxFQUFFO29CQUNuQixJQUFJLFVBQVU7d0JBQUUsVUFBVSxFQUFFLENBQUM7aUJBQzlCO1lBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxzQ0FBa0IsR0FBekIsVUFBMEIsSUFBZ0MsRUFBRSxJQUFZLEVBQUUsUUFBeUQ7UUFBekQseUJBQUEsRUFBQSxlQUF5RDtRQUNqSSxJQUFJLElBQUksR0FBRyxXQUFTLENBQUMsU0FBUyxDQUFDO1FBRS9CLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCO1lBQUUsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksR0FBRyxFQUErQixDQUFDO1FBQ3JHLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7WUFDN0ksSUFBSSxRQUFRLEVBQUU7Z0JBQ1osUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDdEQ7WUFDRCxPQUFPO1NBQ1I7YUFBTTtZQUNMLHlCQUFlLENBQUMsVUFBVSxDQUFDLHNCQUFzQixHQUFHLElBQUksRUFBRSxVQUFVLE1BQU07Z0JBQ3hFLElBQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUF1QixDQUFDO2dCQUM3RCxJQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1QyxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDL0MsSUFBSSxRQUFRLEVBQUU7b0JBQ1osUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ3REO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFTSw2QkFBUyxHQUFoQixVQUNFLElBQTBCLEVBQzFCLElBQVksRUFDWixJQUFXLEVBQ1gsVUFBb0QsRUFDcEQsTUFBc0IsRUFDdEIsZ0JBQWdDO1FBSGhDLHFCQUFBLEVBQUEsV0FBVztRQUNYLDJCQUFBLEVBQUEsaUJBQW9EO1FBQ3BELHVCQUFBLEVBQUEsYUFBc0I7UUFDdEIsaUNBQUEsRUFBQSx1QkFBZ0M7UUFFaEMsSUFBSSxJQUFJLEdBQUcsV0FBUyxDQUFDLFNBQVMsQ0FBQztRQUMvQixNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDMUMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFO1lBQzdELElBQUksVUFBVTtnQkFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQy9DLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDOUIsSUFBSSxDQUFDLGdCQUFnQixDQUNuQixJQUFJLEVBQ0osSUFBSSxFQUNKLFVBQUMsUUFBUTtZQUNQLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxRQUFRLEVBQUU7Z0JBQ2pDLElBQUksVUFBVTtvQkFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUM5QyxPQUFPO2FBQ1I7WUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQztZQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLFFBQVEsQ0FBQyxNQUFNLEdBQUc7Z0JBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1lBQzlCLENBQUMsQ0FBQztZQUNGLElBQUksVUFBVTtnQkFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2hELENBQUMsRUFDRCxNQUFNLEVBQ04sZ0JBQWdCLENBQ2pCLENBQUM7SUFDSixDQUFDO0lBRU0sNkNBQXlCLEdBQWhDLFVBQWlDLElBQTBCLEVBQUUsSUFBWSxFQUFFLElBQVcsRUFBRSxNQUFzQixFQUFFLGdCQUFnQztRQUFyRSxxQkFBQSxFQUFBLFdBQVc7UUFBRSx1QkFBQSxFQUFBLGFBQXNCO1FBQUUsaUNBQUEsRUFBQSx1QkFBZ0M7UUFDOUksSUFBSSxJQUFJLEdBQUcsV0FBUyxDQUFDLFNBQVMsQ0FBQztRQUMvQixNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDMUMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFO1lBQzNGLE9BQU8sSUFBSSxPQUFPLENBQWdCLFVBQUMsT0FBTztnQkFDeEMsT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztTQUNKO1FBRUQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUM5QixPQUFPLElBQUk7YUFDUix5QkFBeUIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsQ0FBQzthQUMvRCxJQUFJLENBQUMsVUFBQyxRQUF1QjtZQUM1QixJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksUUFBUSxFQUFFO2dCQUNqQyxPQUFPLElBQUksT0FBTyxDQUFnQixVQUFDLE9BQU87b0JBQ3hDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQzdCLENBQUMsQ0FBQyxDQUFDO2FBQ0o7WUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQztZQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLFFBQVEsQ0FBQyxNQUFNLEdBQUc7Z0JBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1lBQzlCLENBQUMsQ0FBQztZQUVGLE9BQU8sUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxVQUFDLEtBQUs7WUFDWCxJQUFJLGdCQUFnQixFQUFFO2dCQUNwQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO2dCQUM1QixXQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsNEJBQWtCLEVBQUUsb0JBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2FBQzdGO1lBQ0Qsc0lBQXNJO1FBQ3hJLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLG9DQUFnQixHQUF2QixVQUF3QixJQUFZO1FBQ2xDLE9BQU8sV0FBUyxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFTSw4QkFBVSxHQUFqQixVQUFrQixJQUEwQixFQUFFLElBQVk7UUFDeEQsSUFBSSxJQUFJLEdBQUcsV0FBUyxDQUFDLFNBQVMsQ0FBQztRQUMvQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxVQUFDLFFBQVE7WUFDekMsUUFBUSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLG9DQUFnQixHQUF2QixVQUF3QixJQUEwQixFQUFFLElBQVksRUFBRSxVQUE2QyxFQUFFLE1BQXNCLEVBQUUsZ0JBQWdDO1FBQXpLLGlCQWtCQztRQWxCZ0gsdUJBQUEsRUFBQSxhQUFzQjtRQUFFLGlDQUFBLEVBQUEsdUJBQWdDO1FBQ3ZLLElBQUksSUFBSSxHQUFHLFdBQVMsQ0FBQyxTQUFTLENBQUM7UUFDL0IsSUFBTSxPQUFPLEdBQUcsTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUMxRixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWU7WUFBRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksR0FBRyxFQUF5QixDQUFDO1FBQ25GLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7WUFDM0gsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0MsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzdCLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNuQjthQUFNO1lBQ0wsSUFBSSxnQkFBZ0IsRUFBRTtnQkFDcEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyw0QkFBa0IsRUFBRSxvQkFBVSxDQUFDLGtCQUFrQixFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLFVBQUMsWUFBWTtvQkFDckcsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDeEQsQ0FBQyxDQUFDLENBQUM7YUFDSjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2FBQ3ZEO1NBQ0Y7SUFDSCxDQUFDO0lBRVksNkNBQXlCLEdBQXRDLFVBQXVDLElBQTBCLEVBQUUsSUFBWSxFQUFFLE1BQXNCLEVBQUUsZ0JBQWdDO1FBQXhELHVCQUFBLEVBQUEsYUFBc0I7UUFBRSxpQ0FBQSxFQUFBLHVCQUFnQzs7Ozs7O3dCQUNuSSxJQUFJLEdBQUcsV0FBUyxDQUFDLFNBQVMsQ0FBQzt3QkFDekIsT0FBTyxHQUFHLE1BQU0sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7d0JBQzFGLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZTs0QkFBRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksR0FBRyxFQUF5QixDQUFDOzZCQUMvRSxDQUFBLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFBLEVBQXZILHdCQUF1SDt3QkFDckgsVUFBUSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDM0MsT0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO3dCQUN6QixPQUFPLENBQUMsUUFBUSxDQUFDLE9BQUssQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDN0Isc0JBQU8sSUFBSSxPQUFPLENBQWdCLFVBQUMsT0FBTztnQ0FDeEMsT0FBTyxDQUFDLE9BQUssQ0FBQyxDQUFDOzRCQUNqQixDQUFDLENBQUMsRUFBQzs7NkJBRUMsZ0JBQWdCLEVBQWhCLHdCQUFnQjt3QkFDbEIscUJBQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLDRCQUFrQixFQUFFLG9CQUFVLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsRUFBQTs7d0JBQWpHLFNBQWlHLENBQUM7d0JBQ2xHLHNCQUFPLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxFQUFDOzRCQUVqRSxzQkFBTyxJQUFJLENBQUMsK0JBQStCLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsRUFBQzs7OztLQUd0RTtJQUVPLG1DQUFlLEdBQXZCLFVBQXdCLElBQTBCLEVBQUUsSUFBWSxFQUFFLFVBQTZDLEVBQUUsTUFBc0I7UUFBdEIsdUJBQUEsRUFBQSxhQUFzQjtRQUNySSxJQUFJLElBQUksR0FBRyxXQUFTLENBQUMsU0FBUyxDQUFDO1FBQy9CLHlCQUFlLENBQUMsVUFBVSxDQUFDLGdCQUFnQixHQUFHLElBQUksRUFBRSxVQUFVLE1BQU07WUFDbEUsSUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQXVCLENBQUM7WUFDN0QsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsNEJBQWtCLEVBQUUsb0JBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3pFLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN2QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxtREFBK0IsR0FBdkMsVUFBd0MsSUFBMEIsRUFBRSxJQUFZLEVBQUUsTUFBc0I7UUFBdEIsdUJBQUEsRUFBQSxhQUFzQjtRQUN0RyxJQUFJLElBQUksR0FBRyxXQUFTLENBQUMsU0FBUyxDQUFDO1FBQy9CLE9BQU8seUJBQWUsQ0FBQywwQkFBMEIsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxNQUFNO1lBQzlGLElBQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUF1QixDQUFDO1lBQzdELElBQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDNUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLDRCQUFrQixFQUFFLG9CQUFVLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUN6RSxPQUFPLFFBQVEsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSw4QkFBVSxHQUFqQixVQUFrQixJQUEyQixFQUFFLElBQVksRUFBRSxJQUFXLEVBQUUsTUFBc0I7UUFBbkMscUJBQUEsRUFBQSxXQUFXO1FBQUUsdUJBQUEsRUFBQSxhQUFzQjtRQUM5RixJQUFJLElBQUksR0FBRyxXQUFTLENBQUMsU0FBUyxDQUFDO1FBQy9CLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUMzQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsa0JBQWtCO1lBQUUsT0FBTztRQUM1QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsQ0FDcEIsSUFBSSxFQUNKLElBQUksRUFDSixVQUFDLFFBQVE7WUFDUCxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksUUFBUTtnQkFBRSxPQUFPO1lBRTNDLElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDO1lBQzlCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIsUUFBUSxDQUFDLE1BQU0sR0FBRztnQkFDaEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7WUFDL0IsQ0FBQyxDQUFDO1FBQ0osQ0FBQyxFQUNELE1BQU0sQ0FDUCxDQUFDO0lBQ0osQ0FBQztJQUVNLHFDQUFpQixHQUF4QixVQUF5QixJQUEyQixFQUFFLElBQVksRUFBRSxVQUE4QyxFQUFFLE1BQXNCO1FBQXRCLHVCQUFBLEVBQUEsYUFBc0I7UUFDeEksSUFBSSxJQUFJLEdBQUcsV0FBUyxDQUFDLFNBQVMsQ0FBQztRQUMvQixJQUFJLE9BQU8sR0FBRyxNQUFNLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQ3hGLElBQUksSUFBSSxJQUFJLG9CQUFVLENBQUMsVUFBVTtZQUFFLE9BQU8sR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFFcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0I7WUFBRSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxHQUFHLEVBQTBCLENBQUM7UUFDdEYsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtZQUM5SCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzdDLElBQUksSUFBSSxJQUFJLG9CQUFVLENBQUMsVUFBVSxFQUFFO2FBQ2xDO2lCQUFNO2dCQUNMLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDMUIsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDL0I7WUFDRCxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDcEI7YUFBTTtZQUNMLHlCQUFlLENBQUMsVUFBVSxDQUFDLGlCQUFpQixHQUFHLElBQUksRUFBRSxVQUFVLE1BQU07Z0JBQ25FLElBQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUF1QixDQUFDO2dCQUM3RCxJQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1QyxPQUFPLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUMxQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDMUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0lBRU0sbUNBQWUsR0FBdEIsVUFDRSxJQUFnQyxFQUNoQyxJQUFZLEVBQ1osSUFBVyxFQUNYLEtBQXdCLEVBQ3hCLElBQXVCLEVBQ3ZCLFVBQTBELEVBQzFELE1BQXNCO1FBSnRCLHFCQUFBLEVBQUEsV0FBVztRQUNYLHNCQUFBLEVBQUEsWUFBd0I7UUFDeEIscUJBQUEsRUFBQSxXQUF1QjtRQUN2QiwyQkFBQSxFQUFBLGlCQUEwRDtRQUMxRCx1QkFBQSxFQUFBLGFBQXNCO1FBRXRCLElBQUksSUFBSSxHQUFHLFdBQVMsQ0FBQyxTQUFTLENBQUM7UUFDL0IsMEdBQTBHO1FBQzFHLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUVoRCxJQUFJLElBQUksSUFBSSxvQkFBVSxDQUFDLGtCQUFrQixFQUFFO1lBQ3pDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUN4QjtRQUNELElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtZQUN4QyxPQUFPO1NBQ1I7UUFDRCxJQUFJLElBQUksQ0FBQyxtQkFBbUIsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFO1lBQzdELElBQUksVUFBVTtnQkFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDckQsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQztRQUNwQyxJQUFJLENBQUMsc0JBQXNCLENBQ3pCLElBQUksRUFDSixJQUFJLEVBQ0osVUFBQyxRQUFRO1lBQ1AsSUFBSSxJQUFJLENBQUMsbUJBQW1CLElBQUksUUFBUSxFQUFFO2dCQUN4QyxJQUFJLFVBQVU7b0JBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUNyRCxPQUFPO2FBQ1I7WUFFRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsUUFBUSxDQUFDO1lBQ3BDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNqQyxRQUFRLENBQUMsTUFBTSxHQUFHO2dCQUNoQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO2dCQUNoQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsRUFBRSxDQUFDO1lBQ3BDLENBQUMsQ0FBQztZQUNGLElBQUksVUFBVTtnQkFBRSxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdkMsQ0FBQyxFQUNELE1BQU0sQ0FDUCxDQUFDO0lBQ0osQ0FBQztJQUVNLHFDQUFpQixHQUF4QixVQUNFLElBQWdDLEVBQ2hDLElBQVksRUFDWixJQUFXLEVBQ1gsS0FBd0IsRUFDeEIsSUFBdUIsRUFDdkIsTUFBc0I7UUFIdEIscUJBQUEsRUFBQSxXQUFXO1FBQ1gsc0JBQUEsRUFBQSxZQUF3QjtRQUN4QixxQkFBQSxFQUFBLFdBQXVCO1FBQ3ZCLHVCQUFBLEVBQUEsYUFBc0I7UUFFdEIsT0FBTyxJQUFJLE9BQU8sQ0FBc0IsVUFBQyxPQUFPO1lBQzlDLElBQUksSUFBSSxHQUFHLFdBQVMsQ0FBQyxTQUFTLENBQUM7WUFDL0IsMEdBQTBHO1lBQzFHLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUVoRCxJQUFJLElBQUksSUFBSSxvQkFBVSxDQUFDLGtCQUFrQixFQUFFO2dCQUN6QyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7YUFDeEI7WUFFRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsdUJBQXVCLElBQUksSUFBSSxDQUFDLG1CQUFtQixJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUU7Z0JBQ3JHLE9BQU8sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDbEMsT0FBTzthQUNSO1lBRUQsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQztZQUNwQyxJQUFJLENBQUMsc0JBQXNCLENBQ3pCLElBQUksRUFDSixJQUFJLEVBQ0osVUFBQyxRQUFRO2dCQUNQLElBQUksSUFBSSxDQUFDLG1CQUFtQixJQUFJLFFBQVEsRUFBRTtvQkFDeEMsT0FBTyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO29CQUNsQyxPQUFPO2lCQUNSO2dCQUVELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxRQUFRLENBQUM7Z0JBQ3BDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDakMsUUFBUSxDQUFDLE1BQU0sR0FBRztvQkFDaEIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztvQkFDaEMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLEVBQUUsQ0FBQztnQkFDcEMsQ0FBQyxDQUFDO2dCQUNGLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNwQixDQUFDLEVBQ0QsTUFBTSxDQUNQLENBQUM7UUFDSixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSx1Q0FBbUIsR0FBMUI7UUFDRSxJQUFJLElBQUksR0FBRyxXQUFTLENBQUMsU0FBUyxDQUFDO1FBQy9CLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDO1lBQ25DLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUk7Z0JBQUUsQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ25ELElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxFQUFFLENBQUM7UUFDcEMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0saUNBQWEsR0FBcEIsVUFBcUIsdUJBQXVDO1FBQXZDLHdDQUFBLEVBQUEsOEJBQXVDO1FBQzFELElBQUksSUFBSSxHQUFHLFdBQVMsQ0FBQyxTQUFTLENBQUM7UUFDL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDLEVBQUUsSUFBSTtZQUNuQyxJQUFJLENBQUMsdUJBQXVCLEVBQUU7YUFDN0I7WUFDRCxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJO2dCQUFFLENBQUMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUNuRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELHFDQUFpQixHQUFqQjtRQUNFLElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzVCLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU07Z0JBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDOUU7UUFDRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN6QixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNO2dCQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ3hFO0lBQ0gsQ0FBQztJQUVNLG9DQUFnQixHQUF2QixVQUF3QixJQUFnQyxFQUFFLElBQVk7UUFDcEUsSUFBSSxJQUFJLEdBQUcsV0FBUyxDQUFDLFNBQVMsQ0FBQztRQUMvQixJQUFJLElBQUksSUFBSSxvQkFBVSxDQUFDLGtCQUFrQixFQUFFO1lBQ3pDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN2QixJQUFJLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQztnQkFBRSxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQztTQUN4RDtRQUVELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLFVBQUMsUUFBUTtZQUMvQyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxFQUFFO2dCQUM3QixRQUFRLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxFQUFFLENBQUM7YUFDbkM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSwwQ0FBc0IsR0FBN0IsVUFBOEIsSUFBZ0MsRUFBRSxJQUFZLEVBQUUsVUFBbUQsRUFBRSxNQUFzQjtRQUF0Qix1QkFBQSxFQUFBLGFBQXNCO1FBQ3ZKLElBQUksSUFBSSxHQUFHLFdBQVMsQ0FBQyxTQUFTLENBQUM7UUFDL0IsSUFBSSxPQUFPLEdBQUcsTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUV4RixPQUFPLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDO1FBRXZDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCO1lBQUUsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksR0FBRyxFQUErQixDQUFDO1FBQ3JHLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7WUFDN0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNqRCxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDekIsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDN0IsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25CO2FBQU07WUFDTCx5QkFBZSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLEVBQUUsVUFBVSxNQUFNO2dCQUN4RSxJQUFNLE9BQU8sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBdUIsQ0FBQztnQkFDN0QsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBRS9DLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN2QixDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUVNLDBDQUFzQixHQUE3QixVQUE4QixJQUEwQixFQUFFLElBQVksRUFBRSxJQUFXLEVBQUUsVUFBMEQsRUFBRSxNQUFzQjtRQUEvRixxQkFBQSxFQUFBLFdBQVc7UUFBRSwyQkFBQSxFQUFBLGlCQUEwRDtRQUFFLHVCQUFBLEVBQUEsYUFBc0I7UUFDckssK0JBQStCO1FBQy9CLElBQUksSUFBSSxHQUFHLFdBQVMsQ0FBQyxTQUFTLENBQUM7UUFDL0IsTUFBTSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFDLHdDQUF3QztRQUN4QyxvREFBb0Q7UUFDcEQsWUFBWTtRQUNaLElBQUk7UUFDSixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQzlCLElBQUksQ0FBQyw2QkFBNkIsQ0FDaEMsSUFBSSxFQUNKLElBQUksRUFDSixVQUFDLFFBQVE7WUFDUCxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksUUFBUSxFQUFFO2dCQUNqQyxJQUFJLFVBQVU7b0JBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDOUMsT0FBTzthQUNSO1lBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUM7WUFDN0IsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixRQUFRLENBQUMsTUFBTSxHQUFHO2dCQUNoQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztnQkFDekIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztZQUM5QixDQUFDLENBQUM7WUFDRixJQUFJLFVBQVU7Z0JBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNoRCxDQUFDLEVBQ0QsTUFBTSxDQUNQLENBQUM7SUFDSixDQUFDO0lBRU0sMkNBQXVCLEdBQTlCLFVBQStCLElBQTBCLEVBQUUsSUFBWTtRQUNyRSxJQUFJLElBQUksR0FBRyxXQUFTLENBQUMsU0FBUyxDQUFDO1FBQy9CLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLFVBQUMsUUFBUTtZQUN0RCxRQUFRLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0saURBQTZCLEdBQXBDLFVBQXFDLElBQTBCLEVBQUUsSUFBWSxFQUFFLFVBQTZDLEVBQUUsTUFBc0I7UUFBdEIsdUJBQUEsRUFBQSxhQUFzQjtRQUNsSixJQUFJLElBQUksR0FBRyxXQUFTLENBQUMsU0FBUyxDQUFDO1FBQy9CLElBQU0sT0FBTyxHQUFHLE1BQU0sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDMUYsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlO1lBQUUsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLEdBQUcsRUFBeUIsQ0FBQztRQUNuRixJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO1lBQzNILElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUN6QixPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3QixVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbkI7YUFBTTtZQUNMLGdIQUFnSDtZQUNoSCx5QkFBZSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLEVBQUUsVUFBVSxNQUFNO2dCQUNsRSxJQUFNLE9BQU8sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBdUIsQ0FBQztnQkFDN0QsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUN6Qyw0RUFBNEU7Z0JBQzVFLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN2QixDQUFDLENBQUMsQ0FBQztZQUNILE1BQU07U0FDUDtJQUNILENBQUM7SUFLTSxpQ0FBYSxHQUFwQixVQUFxQixJQUFhLEVBQUUsTUFBZTtRQUNqRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN4QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUMvQzthQUFNO1lBQ0wsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDL0M7SUFDSCxDQUFDO0lBQ00sK0JBQVcsR0FBbEIsVUFBbUIsSUFBYTtRQUM5QixJQUFJLE1BQU0sR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUNNLGdDQUFZLEdBQW5CLFVBQW9CLElBQWE7UUFDL0IsSUFBSSxNQUFNLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xDLENBQUM7O0lBdHFCRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2lEQUNTO0lBYzNCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7a0RBQ1U7SUFhNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDUTtJQWUxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2lEQUNTO0lBa0IzQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3NEQUNjO0lBN0xiLFNBQVM7UUFEN0IsT0FBTztPQUNhLFNBQVMsQ0F3eUI3QjtJQUFELGdCQUFDO0NBeHlCRCxBQXd5QkMsQ0F4eUJzQyxFQUFFLENBQUMsU0FBUyxHQXd5QmxEO2tCQXh5Qm9CLFNBQVM7QUE4eUI5QixxREFBZ0Q7QUFDaEQsa0VBQTZEO0FBRTdELG9EQUErQztBQUMvQyxxREFBZ0Q7QUFDaEQseUVBQW9FIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFBvcHVwQ29uZmlnRGF0YSB7XHJcbiAgcHVibGljIHBvcHVwVHlwZTogdHlwZW9mIFBvcHVwSW5zdGFuY2U7XHJcbiAgcHVibGljIHBvcHVwTmFtZTogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3Rvcih0eXBlOiB0eXBlb2YgUG9wdXBJbnN0YW5jZSwgbmFtZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnBvcHVwVHlwZSA9IHR5cGU7XHJcbiAgICB0aGlzLnBvcHVwTmFtZSA9IG5hbWU7XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgUG9wdXBTeXN0ZW1Db25maWdEYXRhIHtcclxuICBwdWJsaWMgcG9wdXBUeXBlOiB0eXBlb2YgUG9wdXBTeXN0ZW1JbnN0YW5jZTtcclxuICBwdWJsaWMgcG9wdXBOYW1lOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHR5cGU6IHR5cGVvZiBQb3B1cFN5c3RlbUluc3RhbmNlLCBuYW1lOiBzdHJpbmcpIHtcclxuICAgIHRoaXMucG9wdXBUeXBlID0gdHlwZTtcclxuICAgIHRoaXMucG9wdXBOYW1lID0gbmFtZTtcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBCdW5kbGVQcmVsb2FkRGF0YSB7XHJcbiAgcHVibGljIHRvcGljQ29uc3Q6IG51bWJlciA9IC0xO1xyXG4gIHB1YmxpYyBsaXN0UG9wdXBQYXRoczogc3RyaW5nW10gPSBbXTtcclxuXHJcbiAgY29uc3RydWN0b3IodG9waWNDb25zdCwgbGlzdFBhdGhzOiBzdHJpbmdbXSkge1xyXG4gICAgdGhpcy50b3BpY0NvbnN0ID0gdG9waWNDb25zdDtcclxuICAgIHRoaXMubGlzdFBvcHVwUGF0aHMgPSBsaXN0UGF0aHM7XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgTG9jYWxCdW5kbGVQcmVsb2FkIHtcclxuICBwdWJsaWMgYnVuZGxlTmFtZTogc3RyaW5nO1xyXG4gIGNvbnN0cnVjdG9yKG5hbWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5idW5kbGVOYW1lID0gbmFtZTtcclxuICB9XHJcbn1cclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSU1hbmFnZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gIHN0YXRpYyBfaW5zdGFuY2U6IFVJTWFuYWdlcjtcclxuXHJcbiAgc3RhdGljIGdldEluc3RhbmNlKCk6IFVJTWFuYWdlciB7XHJcbiAgICBpZiAoIVVJTWFuYWdlci5faW5zdGFuY2UpIHtcclxuICAgICAgbGV0IG5vZGUgPSBuZXcgY2MuTm9kZShcIlVJTWFuYWdlclwiKTtcclxuICAgICAgVUlNYW5hZ2VyLl9pbnN0YW5jZSA9IG5vZGUuYWRkQ29tcG9uZW50KFVJTWFuYWdlcik7XHJcbiAgICAgIGxldCBzZWxmID0gVUlNYW5hZ2VyLl9pbnN0YW5jZTtcclxuXHJcbiAgICAgIGxldCBjYW52YXMgPSBjYy5kaXJlY3Rvci5nZXRTY2VuZSgpLmdldENoaWxkQnlOYW1lKFwiQ2FudmFzXCIpO1xyXG4gICAgICBsZXQgc2l6ZSA9IGNhbnZhcy5nZXRDb250ZW50U2l6ZSgpO1xyXG5cclxuICAgICAgY2MuZ2FtZS5hZGRQZXJzaXN0Um9vdE5vZGUoc2VsZi5ub2RlKTtcclxuICAgICAgc2VsZi5ub2RlLnpJbmRleCA9IExheWVySGVscGVyLnVpTWFuYWdlcjtcclxuICAgICAgLy9zZWxmLnNldFdpZGdldChzZWxmLm5vZGUsIHNlbGYubm9kZS5wYXJlbnQpO1xyXG4gICAgICAvLyBjYy5sb2coXCJVSU1hbmFnZXJcIik7XHJcblxyXG4gICAgICBzZWxmLl9wZXJzaXN0UG9wdXBSb290ID0gbmV3IGNjLk5vZGUoXCJQb3B1cFwiKTtcclxuICAgICAgc2VsZi5fcGVyc2lzdFBvcHVwUm9vdC5zZXRDb250ZW50U2l6ZShzaXplKTtcclxuICAgICAgc2VsZi5fcGVyc2lzdFBvcHVwUm9vdC5zZXRQYXJlbnQoc2VsZi5ub2RlKTtcclxuICAgICAgc2VsZi5fcGVyc2lzdFBvcHVwUm9vdC5zZXRQb3NpdGlvbihjYy5WZWMzLlpFUk8pO1xyXG4gICAgICBzZWxmLnNldFdpZGdldChzZWxmLl9wZXJzaXN0UG9wdXBSb290LCBzZWxmLm5vZGUpO1xyXG5cclxuICAgICAgc2VsZi5fcGVyc2lzTm90aWZ5Um9vdCA9IG5ldyBjYy5Ob2RlKFwiTm90aWZ5XCIpO1xyXG4gICAgICBzZWxmLl9wZXJzaXNOb3RpZnlSb290LnNldENvbnRlbnRTaXplKHNpemUpO1xyXG4gICAgICBzZWxmLl9wZXJzaXNOb3RpZnlSb290LnNldFBhcmVudChzZWxmLm5vZGUpO1xyXG4gICAgICBzZWxmLl9wZXJzaXNOb3RpZnlSb290LnNldFBvc2l0aW9uKGNjLlZlYzMuWkVSTyk7XHJcbiAgICAgIHNlbGYuc2V0V2lkZ2V0KHNlbGYuX3BlcnNpc05vdGlmeVJvb3QsIHNlbGYubm9kZSk7XHJcblxyXG4gICAgICBzZWxmLl9wZXJzaXN0UG9wdXBTeXN0ZW1Sb290ID0gbmV3IGNjLk5vZGUoXCJQb3B1cFN5c3RlbVwiKTtcclxuICAgICAgc2VsZi5fcGVyc2lzdFBvcHVwU3lzdGVtUm9vdC5zZXRDb250ZW50U2l6ZShzaXplKTtcclxuICAgICAgc2VsZi5fcGVyc2lzdFBvcHVwU3lzdGVtUm9vdC5zZXRQYXJlbnQoc2VsZi5ub2RlKTtcclxuICAgICAgc2VsZi5fcGVyc2lzdFBvcHVwU3lzdGVtUm9vdC5zZXRQb3NpdGlvbihjYy5WZWMzLlpFUk8pO1xyXG4gICAgICBzZWxmLnNldFdpZGdldChzZWxmLl9wZXJzaXN0UG9wdXBTeXN0ZW1Sb290LCBzZWxmLm5vZGUpO1xyXG5cclxuICAgICAgLy8gRXZlbnRNYW5hZ2VyLm9uKFxyXG4gICAgICAvLyAgIEV2ZW50TWFuYWdlci5PTl9TQ1JFRU5fQ0hBTkdFX1NJWkUsXHJcbiAgICAgIC8vICAgKCkgPT4ge1xyXG4gICAgICAvLyAgICAgc2VsZi51cGRhdGVTaXplKCk7XHJcbiAgICAgIC8vICAgfSxcclxuICAgICAgLy8gICBzZWxmXHJcbiAgICAgIC8vICk7XHJcblxyXG4gICAgICBjYy5zeXN0ZW1FdmVudC5vbihFdmVudEtleS5PTl9TQ1JFRU5fQ0hBTkdFX1NJWkVfRVZFTlQsIHNlbGYudXBkYXRlU2l6ZSwgc2VsZik7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gVUlNYW5hZ2VyLl9pbnN0YW5jZTtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBvbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICBjYy5zeXN0ZW1FdmVudC5vZmYoRXZlbnRLZXkuT05fU0NSRUVOX0NIQU5HRV9TSVpFX0VWRU5ULCB0aGlzLnVwZGF0ZVNpemUsIHRoaXMpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSB1cGRhdGVTaXplKCkge1xyXG4gICAgbGV0IHNlbGYgPSBVSU1hbmFnZXIuX2luc3RhbmNlO1xyXG4gICAgaWYgKHNlbGYgJiYgc2VsZi5ub2RlKSB7XHJcbiAgICAgIGxldCBjYW52YXMgPSBjYy5kaXJlY3Rvci5nZXRTY2VuZSgpLmdldENoaWxkQnlOYW1lKFwiQ2FudmFzXCIpO1xyXG4gICAgICBpZiAoY2FudmFzKSB7XHJcbiAgICAgICAgbGV0IHNpemUgPSBjYW52YXMuZ2V0Q29udGVudFNpemUoKTtcclxuXHJcbiAgICAgICAgc2VsZi5ub2RlLnNldENvbnRlbnRTaXplKHNpemUpO1xyXG4gICAgICAgIHNlbGYubm9kZS5zZXRQb3NpdGlvbihjYW52YXMuZ2V0UG9zaXRpb24oKSk7XHJcblxyXG4gICAgICAgIHNlbGYuX3BlcnNpc3RQb3B1cFJvb3Quc2V0Q29udGVudFNpemUoc2l6ZSk7XHJcbiAgICAgICAgc2VsZi5fcGVyc2lzdFBvcHVwUm9vdC5zZXRQYXJlbnQoc2VsZi5ub2RlKTtcclxuICAgICAgICBzZWxmLl9wZXJzaXN0UG9wdXBSb290LnNldFBvc2l0aW9uKGNjLlZlYzMuWkVSTyk7XHJcblxyXG4gICAgICAgIHNlbGYuX3BlcnNpc05vdGlmeVJvb3Quc2V0Q29udGVudFNpemUoc2l6ZSk7XHJcbiAgICAgICAgc2VsZi5fcGVyc2lzTm90aWZ5Um9vdC5zZXRQYXJlbnQoc2VsZi5ub2RlKTtcclxuICAgICAgICBzZWxmLl9wZXJzaXNOb3RpZnlSb290LnNldFBvc2l0aW9uKGNjLlZlYzMuWkVSTyk7XHJcblxyXG4gICAgICAgIHNlbGYuX3BlcnNpc3RQb3B1cFN5c3RlbVJvb3Quc2V0Q29udGVudFNpemUoc2l6ZSk7XHJcbiAgICAgICAgc2VsZi5fcGVyc2lzdFBvcHVwU3lzdGVtUm9vdC5zZXRQYXJlbnQoc2VsZi5ub2RlKTtcclxuICAgICAgICBzZWxmLl9wZXJzaXN0UG9wdXBTeXN0ZW1Sb290LnNldFBvc2l0aW9uKGNjLlZlYzMuWkVSTyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgc2V0V2lkZ2V0KG5vZGU6IGNjLk5vZGUsIHBhcmVudDogY2MuTm9kZSkge1xyXG4gICAgbGV0IHdpZGdldCA9IG5vZGUuZ2V0Q29tcG9uZW50KGNjLldpZGdldCk7XHJcbiAgICBpZiAod2lkZ2V0ID09IG51bGwpIHdpZGdldCA9IG5vZGUuYWRkQ29tcG9uZW50KGNjLldpZGdldCk7XHJcbiAgICB3aWRnZXQudGFyZ2V0ID0gcGFyZW50O1xyXG4gICAgd2lkZ2V0LmlzQWxpZ25MZWZ0ID0gdHJ1ZTtcclxuICAgIHdpZGdldC5pc0Fic29sdXRlUmlnaHQgPSB0cnVlO1xyXG4gICAgd2lkZ2V0LmlzQWJzb2x1dGVUb3AgPSB0cnVlO1xyXG4gICAgd2lkZ2V0LmlzQWxpZ25Cb3R0b20gPSB0cnVlO1xyXG4gICAgd2lkZ2V0LmxlZnQgPSAwO1xyXG4gICAgd2lkZ2V0LnRvcCA9IDA7XHJcbiAgICB3aWRnZXQuYm90dG9tID0gMDtcclxuICAgIHdpZGdldC5yaWdodCA9IDA7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9wZXJzaXNOb3RpZnlSb290OiBjYy5Ob2RlID0gbnVsbDtcclxuICBwcml2YXRlIF9wZXJzaXN0UG9wdXBTeXN0ZW1Sb290OiBjYy5Ob2RlID0gbnVsbDtcclxuICBwcml2YXRlIF9wZXJzaXN0UG9wdXBSb290OiBjYy5Ob2RlID0gbnVsbDtcclxuICBwcml2YXRlIHBheW1lbnRUb3BVcE5vZGU6IGNjLk5vZGUgPSBudWxsO1xyXG4gIHByaXZhdGUgcGF5bWVudFdpdGhEcmF3Tm9kZTogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gIHB1YmxpYyBnZXQgUGF5bWVudFRvcFVwT2JqZWN0KCkge1xyXG4gICAgcmV0dXJuIHRoaXMucGF5bWVudFRvcFVwTm9kZTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXQgUGF5bWVudFdpdGhEcmF3T2JqZWN0KCkge1xyXG4gICAgcmV0dXJuIHRoaXMucGF5bWVudFdpdGhEcmF3Tm9kZTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZXRQYXltZW50VG9wVXBPYmplY3Qobm9kZTogY2MuTm9kZSkge1xyXG4gICAgdGhpcy5wYXltZW50VG9wVXBOb2RlID0gbm9kZTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZXRQYXltZW50V2l0aERyYXdPYmplY3Qobm9kZTogY2MuTm9kZSkge1xyXG4gICAgdGhpcy5wYXltZW50V2l0aERyYXdOb2RlID0gbm9kZTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXQgTGF5ZXJVcFBvcHVwKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX3BlcnNpc3RQb3B1cFJvb3Q7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgaW5pdCgpIHtcclxuICAgIC8vIGNjLmxvZyhcIlVJTWFuYWdlci4uLi4uLiBpbml0XCIpO1xyXG4gIH1cclxuICBfY3VycmVudFNjcmVlbjogU2NyZWVuSW5zdGFuY2U7XHJcbiAgcHVibGljIGdldCBjdXJyZW50U2NyZWVuKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX2N1cnJlbnRTY3JlZW47XHJcbiAgfVxyXG4gIHB1YmxpYyBzZXQgY3VycmVudFNjcmVlbih2YWx1ZSkge1xyXG4gICAgdGhpcy5fY3VycmVudFNjcmVlbiA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgc2NyZWVuVHJhbjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gIHByaXZhdGUgX3NjcmVlbkluc3RhbmNlczogTWFwPHN0cmluZywgU2NyZWVuSW5zdGFuY2U+ID0gbmV3IE1hcDxzdHJpbmcsIFNjcmVlbkluc3RhbmNlPigpO1xyXG5cclxuICBfY3VycmVudEhlYWRlcjogRWxlbWVudEluc3RhbmNlO1xyXG4gIF9jdXJyZW50RWxlbWVudE5hbWU6IHN0cmluZztcclxuICBwdWJsaWMgZ2V0IGN1cnJlbnRFbGVtZW50KCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX2N1cnJlbnRIZWFkZXI7XHJcbiAgfVxyXG4gIHB1YmxpYyBzZXQgY3VycmVudEVsZW1lbnQodmFsdWUpIHtcclxuICAgIHRoaXMuX2N1cnJlbnRIZWFkZXIgPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gIGVsZW1lbnRUcmFuOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgcHJpdmF0ZSBfZWxlbWVudEluc3RhbmNlczogTWFwPHN0cmluZywgRWxlbWVudEluc3RhbmNlPiA9IG5ldyBNYXA8c3RyaW5nLCBFbGVtZW50SW5zdGFuY2U+KCk7XHJcbiAgX2N1cnJlbnRQb3B1cDogUG9wdXBJbnN0YW5jZTtcclxuICBfY3VycmVudFBvcHVwTmFtZTogc3RyaW5nO1xyXG4gIHB1YmxpYyBnZXQgY3VycmVudFBvcHVwKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX2N1cnJlbnRQb3B1cDtcclxuICB9XHJcbiAgcHVibGljIHNldCBjdXJyZW50UG9wdXAodmFsdWUpIHtcclxuICAgIHRoaXMuX2N1cnJlbnRQb3B1cCA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgcG9wdXBUcmFuOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgcHJpdmF0ZSBfcG9wdXBJbnN0YW5jZXM6IE1hcDxzdHJpbmcsIFBvcHVwSW5zdGFuY2U+ID0gbmV3IE1hcDxzdHJpbmcsIFBvcHVwSW5zdGFuY2U+KCk7XHJcblxyXG4gIF9jdXJyZW50Tm90aWZ5OiBQb3B1cEluc3RhbmNlO1xyXG4gIF9jdXJyZW50Tm90aWZ5TmFtZTogc3RyaW5nO1xyXG5cclxuICBwdWJsaWMgZ2V0IGN1cnJlbnROb3RpZnkoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5fY3VycmVudE5vdGlmeTtcclxuICB9XHJcbiAgcHVibGljIHNldCBjdXJyZW50Tm90aWZ5KHZhbHVlKSB7XHJcbiAgICB0aGlzLl9jdXJyZW50Tm90aWZ5ID0gdmFsdWU7XHJcbiAgfVxyXG5cclxuICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICBub3RpZnlUcmFuOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgcHJpdmF0ZSBfbm90aWZ5SW5zdGFuY2VzOiBNYXA8c3RyaW5nLCBOb3RpZnlJbnN0YW5jZT4gPSBuZXcgTWFwPHN0cmluZywgTm90aWZ5SW5zdGFuY2U+KCk7XHJcblxyXG4gIF9jdXJyZW50UG9wdXBTeXN0ZW06IFBvcHVwU3lzdGVtSW5zdGFuY2U7XHJcbiAgX2N1cnJlbnRQb3B1cFN5c3RlbU5hbWU6IHN0cmluZztcclxuICBwdWJsaWMgZ2V0IGN1cnJlbnRQb3B1cFN5c3RlbSgpIHtcclxuICAgIHJldHVybiB0aGlzLl9jdXJyZW50UG9wdXBTeXN0ZW07XHJcbiAgfVxyXG4gIHB1YmxpYyBzZXQgY3VycmVudFBvcHVwU3lzdGVtKHZhbHVlKSB7XHJcbiAgICB0aGlzLl9jdXJyZW50UG9wdXBTeXN0ZW0gPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXQgY3VycmVudFBvcHVwU3lzdGVtTmFtZSgpIHtcclxuICAgIHJldHVybiB0aGlzLl9jdXJyZW50UG9wdXBTeXN0ZW1OYW1lO1xyXG4gIH1cclxuXHJcbiAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgcG9wdXBTeXN0ZW1UcmFuOiBjYy5Ob2RlID0gbnVsbDtcclxuICBwcml2YXRlIF9jdXJyZW50TG9hZGluZzogbnVtYmVyID0gMDtcclxuICBwcml2YXRlIF9wb3B1cFN5c3RlbUluc3RhbmNlczogTWFwPHN0cmluZywgUG9wdXBTeXN0ZW1JbnN0YW5jZT4gPSBuZXcgTWFwPHN0cmluZywgUG9wdXBTeXN0ZW1JbnN0YW5jZT4oKTtcclxuXHJcbiAgcHVibGljIG9wZW5FbGVtZW50KHR5cGU6IHR5cGVvZiBFbGVtZW50SW5zdGFuY2UsIG5hbWU6IHN0cmluZywgZGF0YSA9IG51bGwsIHBhcmVudDogY2MuTm9kZSA9IG51bGwsIG9uQ29tcGxldGU6IChpbnN0YW5jZSkgPT4gdm9pZCA9IG51bGwpOiB2b2lkIHtcclxuICAgIGxldCBzZWxmID0gVUlNYW5hZ2VyLl9pbnN0YW5jZTtcclxuXHJcbiAgICBzZWxmLl9jdXJyZW50RWxlbWVudE5hbWUgPSBuYW1lO1xyXG4gICAgcGFyZW50ID0gcGFyZW50ID8gcGFyZW50IDogc2VsZi5lbGVtZW50VHJhbjtcclxuICAgIHNlbGYuZ2V0RWxlbWVudEluc3RhbmNlKFxyXG4gICAgICB0eXBlLFxyXG4gICAgICBuYW1lLFxyXG4gICAgICAoaW5zdGFuY2UpID0+IHtcclxuICAgICAgICBpZiAoc2VsZi5jdXJyZW50RWxlbWVudCA9PSBpbnN0YW5jZSkge1xyXG4gICAgICAgICAgcmV0dXJuIHNlbGYuY3VycmVudEVsZW1lbnQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzZWxmLmN1cnJlbnRFbGVtZW50ID0gaW5zdGFuY2U7XHJcbiAgICAgICAgaW5zdGFuY2Uub3BlbihkYXRhKTtcclxuICAgICAgICBpbnN0YW5jZS5fY2xvc2UgPSAoKSA9PiB7XHJcbiAgICAgICAgICBzZWxmLmN1cnJlbnRFbGVtZW50ID0gbnVsbDtcclxuICAgICAgICAgIHRoaXMuX2N1cnJlbnRFbGVtZW50TmFtZSA9IFwiXCI7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZShpbnN0YW5jZSk7XHJcbiAgICAgIH0sXHJcbiAgICAgIHBhcmVudFxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBjbG9zZUVsZW1lbnQodHlwZTogdHlwZW9mIEVsZW1lbnRJbnN0YW5jZSwgbmFtZTogc3RyaW5nKSB7XHJcbiAgICBsZXQgc2VsZiA9IFVJTWFuYWdlci5faW5zdGFuY2U7XHJcblxyXG4gICAgc2VsZi5nZXRFbGVtZW50SW5zdGFuY2UodHlwZSwgbmFtZSwgKGluc3RhbmNlKSA9PiB7XHJcbiAgICAgIGluc3RhbmNlLmNsb3NlKCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRFbGVtZW50SW5zdGFuY2UodHlwZTogdHlwZW9mIEVsZW1lbnRJbnN0YW5jZSwgbmFtZTogc3RyaW5nLCBvbkNvbXBsZXRlOiAoaW5zdGFuY2UpID0+IHZvaWQsIHBhcmVudDogY2MuTm9kZSA9IG51bGwpIHtcclxuICAgIGxldCBzZWxmID0gVUlNYW5hZ2VyLl9pbnN0YW5jZTtcclxuICAgIGNvbnN0IF9wYXJlbnQgPSBwYXJlbnQgPT0gbnVsbCA/IGNjLmRpcmVjdG9yLmdldFNjZW5lKCkuZ2V0Q2hpbGRCeU5hbWUoXCJDYW52YXNcIikgOiBwYXJlbnQ7XHJcbiAgICBpZiAoIXNlbGYuX2VsZW1lbnRJbnN0YW5jZXMpIHNlbGYuX2VsZW1lbnRJbnN0YW5jZXMgPSBuZXcgTWFwPHN0cmluZywgRWxlbWVudEluc3RhbmNlPigpO1xyXG4gICAgaWYgKHNlbGYuX2VsZW1lbnRJbnN0YW5jZXMuaGFzKG5hbWUpICYmIHNlbGYuX2VsZW1lbnRJbnN0YW5jZXMuZ2V0KG5hbWUpICE9IG51bGwgJiYgc2VsZi5fZWxlbWVudEluc3RhbmNlcy5nZXQobmFtZSkubm9kZSAhPSBudWxsKSB7XHJcbiAgICAgIGxldCBlbGVtZW50ID0gc2VsZi5fZWxlbWVudEluc3RhbmNlcy5nZXQobmFtZSk7XHJcbiAgICAgIGVsZW1lbnQubm9kZS5wYXJlbnQgPSBudWxsO1xyXG4gICAgICBfcGFyZW50LmFkZENoaWxkKGVsZW1lbnQubm9kZSk7XHJcbiAgICAgIG9uQ29tcGxldGUoZWxlbWVudCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBSZXNvdXJjZU1hbmFnZXIubG9hZFByZWZhYihcIlByZWZhYnMvRWxlbWVudC9cIiArIG5hbWUsIGZ1bmN0aW9uIChwcmVmYWIpIHtcclxuICAgICAgICBjb25zdCBuZXdOb2RlID0gY2MuaW5zdGFudGlhdGUocHJlZmFiKSBhcyB1bmtub3duIGFzIGNjLk5vZGU7XHJcbiAgICAgICAgY29uc3QgaW5zdGFuY2UgPSBuZXdOb2RlLmdldENvbXBvbmVudCh0eXBlKTtcclxuICAgICAgICBfcGFyZW50LmFkZENoaWxkKG5ld05vZGUpO1xyXG4gICAgICAgIHNlbGYuX2VsZW1lbnRJbnN0YW5jZXMuc2V0KG5hbWUsIGluc3RhbmNlKTtcclxuXHJcbiAgICAgICAgb25Db21wbGV0ZShpbnN0YW5jZSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIG9wZW5TY3JlZW4odHlwZTogdHlwZW9mIFNjcmVlbkluc3RhbmNlLCBuYW1lOiBzdHJpbmcsIGRhdGEgPSBudWxsLCBwYXJlbnQ6IGNjLk5vZGUgPSBudWxsKSB7XHJcbiAgICBsZXQgc2VsZiA9IFVJTWFuYWdlci5faW5zdGFuY2U7XHJcbiAgICBwYXJlbnQgPSBwYXJlbnQgPyBwYXJlbnQgOiBzZWxmLnNjcmVlblRyYW47XHJcbiAgICBzZWxmLmdldFNjZW5lSW5zdGFuY2UoXHJcbiAgICAgIHR5cGUsXHJcbiAgICAgIG5hbWUsXHJcbiAgICAgIChpbnN0YW5jZSkgPT4ge1xyXG4gICAgICAgIGlmIChzZWxmLmN1cnJlbnRTY3JlZW4gIT0gbnVsbCkge1xyXG4gICAgICAgICAgc2VsZi5jdXJyZW50U2NyZWVuLmNsb3NlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNlbGYuY3VycmVudFNjcmVlbiA9IGluc3RhbmNlO1xyXG5cclxuICAgICAgICBpbnN0YW5jZS5vcGVuKGRhdGEpO1xyXG4gICAgICB9LFxyXG4gICAgICBwYXJlbnRcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0U2NlbmVJbnN0YW5jZSh0eXBlOiB0eXBlb2YgU2NyZWVuSW5zdGFuY2UsIG5hbWU6IHN0cmluZywgb25Db21wbGV0ZTogKGluc3RhbmNlOiBTY3JlZW5JbnN0YW5jZSkgPT4gdm9pZCwgcGFyZW50OiBjYy5Ob2RlID0gbnVsbCkge1xyXG4gICAgbGV0IHNlbGYgPSBVSU1hbmFnZXIuX2luc3RhbmNlO1xyXG4gICAgY29uc3QgX3BhcmVudCA9IHBhcmVudCA9PSBudWxsID8gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShcIkNhbnZhc1wiKSA6IHBhcmVudDtcclxuICAgIGlmICghc2VsZi5fc2NyZWVuSW5zdGFuY2VzKSBzZWxmLl9zY3JlZW5JbnN0YW5jZXMgPSBuZXcgTWFwPHN0cmluZywgU2NyZWVuSW5zdGFuY2U+KCk7XHJcbiAgICBpZiAoc2VsZi5fc2NyZWVuSW5zdGFuY2VzLmhhcyhuYW1lKSAmJiBzZWxmLl9zY3JlZW5JbnN0YW5jZXMuZ2V0KG5hbWUpICE9IG51bGwgJiYgc2VsZi5fc2NyZWVuSW5zdGFuY2VzLmdldChuYW1lKS5ub2RlICE9IG51bGwpIHtcclxuICAgICAgbGV0IHNjZW5lID0gc2VsZi5fc2NyZWVuSW5zdGFuY2VzLmdldChuYW1lKTtcclxuICAgICAgc2NlbmUubm9kZS5wYXJlbnQgPSBudWxsO1xyXG4gICAgICBfcGFyZW50LmFkZENoaWxkKHNjZW5lLm5vZGUpO1xyXG4gICAgICBvbkNvbXBsZXRlKHNjZW5lKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIFJlc291cmNlTWFuYWdlci5sb2FkUHJlZmFiKFwiUHJlZmFicy9TY3JlZW4vXCIgKyBuYW1lLCBmdW5jdGlvbiAocHJlZmFiKSB7XHJcbiAgICAgICAgdmFyIG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpIGFzIHVua25vd24gYXMgY2MuTm9kZTtcclxuICAgICAgICB2YXIgaW5zdGFuY2UgPSBuZXdOb2RlLmdldENvbXBvbmVudCh0eXBlKTtcclxuICAgICAgICBfcGFyZW50LmFkZENoaWxkKG5ld05vZGUpO1xyXG4gICAgICAgIHNlbGYuX3NjcmVlbkluc3RhbmNlcy5zZXQobmFtZSwgaW5zdGFuY2UpO1xyXG5cclxuICAgICAgICBvbkNvbXBsZXRlKGluc3RhbmNlKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgcHJlbG9hZExpc3RQb3B1cChsaXN0UG9wdXBzOiBQb3B1cENvbmZpZ0RhdGFbXSwgb25Qcm9ncmVzczogKGZpbmlzaDogbnVtYmVyLCB0b3RhbDogbnVtYmVyLCBpdGVtOiBQb3B1cEluc3RhbmNlKSA9PiB2b2lkID0gbnVsbCwgb25Db21wbGV0ZTogKCkgPT4gdm9pZCA9IG51bGwpIHtcclxuICAgIGlmIChsaXN0UG9wdXBzLmxlbmd0aCA9PSAwKSB7XHJcbiAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKCk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGxldCBmaW5pc2ggPSAwO1xyXG4gICAgbGV0IHRvdGFsID0gbGlzdFBvcHVwcy5sZW5ndGg7XHJcbiAgICBsaXN0UG9wdXBzLmZvckVhY2goKGRhdGEpID0+IHtcclxuICAgICAgdGhpcy5wcmVMb2FkUG9wdXAoZGF0YS5wb3B1cFR5cGUsIGRhdGEucG9wdXBOYW1lLCAoZXJyLCBpdGVtOiBQb3B1cEluc3RhbmNlKSA9PiB7XHJcbiAgICAgICAgaWYgKGVycikge1xyXG4gICAgICAgICAgLy8gY2MubG9nKGVycik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZpbmlzaCsrO1xyXG4gICAgICAgIGlmIChvblByb2dyZXNzKSB7XHJcbiAgICAgICAgICBvblByb2dyZXNzKGZpbmlzaCwgdG90YWwsIGl0ZW0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZmluaXNoID09IHRvdGFsKSB7XHJcbiAgICAgICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBwcmVMb2FkUG9wdXAodHlwZTogdHlwZW9mIFBvcHVwSW5zdGFuY2UsIG5hbWU6IHN0cmluZywgY2FsbGJhY2s6IChlcnIsIGl0ZW06IFBvcHVwSW5zdGFuY2UpID0+IHZvaWQgPSBudWxsKSB7XHJcbiAgICBsZXQgc2VsZiA9IFVJTWFuYWdlci5faW5zdGFuY2U7XHJcbiAgICBpZiAoIXNlbGYuX3BvcHVwSW5zdGFuY2VzKSBzZWxmLl9wb3B1cEluc3RhbmNlcyA9IG5ldyBNYXA8c3RyaW5nLCBQb3B1cEluc3RhbmNlPigpO1xyXG4gICAgaWYgKHNlbGYuX3BvcHVwSW5zdGFuY2VzLmhhcyhuYW1lKSAmJiBzZWxmLl9wb3B1cEluc3RhbmNlcy5nZXQobmFtZSkgIT0gbnVsbCAmJiBzZWxmLl9wb3B1cEluc3RhbmNlcy5nZXQobmFtZSkubm9kZSAhPSBudWxsKSB7XHJcbiAgICAgIGlmIChjYWxsYmFjaykge1xyXG4gICAgICAgIGNhbGxiYWNrKG51bGwsIHNlbGYuX3BvcHVwSW5zdGFuY2VzLmdldChuYW1lKSk7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgUmVzb3VyY2VNYW5hZ2VyLmxvYWRQcmVmYWIoXCJQcmVmYWJzL1BvcHVwL1wiICsgbmFtZSwgZnVuY3Rpb24gKHByZWZhYikge1xyXG4gICAgICAgIGNvbnN0IG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpIGFzIHVua25vd24gYXMgY2MuTm9kZTtcclxuICAgICAgICBjb25zdCBpbnN0YW5jZSA9IG5ld05vZGUuZ2V0Q29tcG9uZW50KHR5cGUpO1xyXG4gICAgICAgIHNlbGYuX3BvcHVwSW5zdGFuY2VzLnNldChuYW1lLCBpbnN0YW5jZSk7XHJcbiAgICAgICAgaWYgKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgICBjYWxsYmFjayhudWxsLCBzZWxmLl9wb3B1cEluc3RhbmNlcy5nZXQobmFtZSkpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgcHJlbG9hZExpc3RQb3B1cFN5c3RlbShcclxuICAgIGxpc3RQb3B1cHM6IFBvcHVwU3lzdGVtQ29uZmlnRGF0YVtdLFxyXG4gICAgb25Qcm9ncmVzczogKGZpbmlzaDogbnVtYmVyLCB0b3RhbDogbnVtYmVyLCBpdGVtOiBQb3B1cFN5c3RlbUluc3RhbmNlKSA9PiB2b2lkID0gbnVsbCxcclxuICAgIG9uQ29tcGxldGU6ICgpID0+IHZvaWQgPSBudWxsXHJcbiAgKSB7XHJcbiAgICBpZiAobGlzdFBvcHVwcy5sZW5ndGggPT0gMCkge1xyXG4gICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZSgpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBsZXQgZmluaXNoID0gMDtcclxuICAgIGxldCB0b3RhbCA9IGxpc3RQb3B1cHMubGVuZ3RoO1xyXG4gICAgbGlzdFBvcHVwcy5mb3JFYWNoKChkYXRhKSA9PiB7XHJcbiAgICAgIHRoaXMucHJlTG9hZFBvcHVwU3lzdGVtKGRhdGEucG9wdXBUeXBlLCBkYXRhLnBvcHVwTmFtZSwgKGVyciwgaXRlbTogUG9wdXBTeXN0ZW1JbnN0YW5jZSkgPT4ge1xyXG4gICAgICAgIGlmIChlcnIpIHtcclxuICAgICAgICAgIC8vIGNjLmxvZyhlcnIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmaW5pc2grKztcclxuICAgICAgICBpZiAob25Qcm9ncmVzcykge1xyXG4gICAgICAgICAgb25Qcm9ncmVzcyhmaW5pc2gsIHRvdGFsLCBpdGVtKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGZpbmlzaCA9PSB0b3RhbCkge1xyXG4gICAgICAgICAgaWYgKG9uQ29tcGxldGUpIG9uQ29tcGxldGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgcHJlTG9hZFBvcHVwU3lzdGVtKHR5cGU6IHR5cGVvZiBQb3B1cFN5c3RlbUluc3RhbmNlLCBuYW1lOiBzdHJpbmcsIGNhbGxiYWNrOiAoZXJyLCBpdGVtOiBQb3B1cFN5c3RlbUluc3RhbmNlKSA9PiB2b2lkID0gbnVsbCkge1xyXG4gICAgbGV0IHNlbGYgPSBVSU1hbmFnZXIuX2luc3RhbmNlO1xyXG5cclxuICAgIGlmICghc2VsZi5fcG9wdXBTeXN0ZW1JbnN0YW5jZXMpIHNlbGYuX3BvcHVwU3lzdGVtSW5zdGFuY2VzID0gbmV3IE1hcDxzdHJpbmcsIFBvcHVwU3lzdGVtSW5zdGFuY2U+KCk7XHJcbiAgICBpZiAoc2VsZi5fcG9wdXBTeXN0ZW1JbnN0YW5jZXMuaGFzKG5hbWUpICYmIHNlbGYuX3BvcHVwU3lzdGVtSW5zdGFuY2VzLmdldChuYW1lKSAhPSBudWxsICYmIHNlbGYuX3BvcHVwU3lzdGVtSW5zdGFuY2VzLmdldChuYW1lKS5ub2RlICE9IG51bGwpIHtcclxuICAgICAgaWYgKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgY2FsbGJhY2sobnVsbCwgc2VsZi5fcG9wdXBTeXN0ZW1JbnN0YW5jZXMuZ2V0KG5hbWUpKTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm47XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBSZXNvdXJjZU1hbmFnZXIubG9hZFByZWZhYihcIlByZWZhYnMvUG9wdXBTeXN0ZW0vXCIgKyBuYW1lLCBmdW5jdGlvbiAocHJlZmFiKSB7XHJcbiAgICAgICAgY29uc3QgbmV3Tm9kZSA9IGNjLmluc3RhbnRpYXRlKHByZWZhYikgYXMgdW5rbm93biBhcyBjYy5Ob2RlO1xyXG4gICAgICAgIGNvbnN0IGluc3RhbmNlID0gbmV3Tm9kZS5nZXRDb21wb25lbnQodHlwZSk7XHJcbiAgICAgICAgc2VsZi5fcG9wdXBTeXN0ZW1JbnN0YW5jZXMuc2V0KG5hbWUsIGluc3RhbmNlKTtcclxuICAgICAgICBpZiAoY2FsbGJhY2spIHtcclxuICAgICAgICAgIGNhbGxiYWNrKG51bGwsIHNlbGYuX3BvcHVwU3lzdGVtSW5zdGFuY2VzLmdldChuYW1lKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvcGVuUG9wdXAoXHJcbiAgICB0eXBlOiB0eXBlb2YgUG9wdXBJbnN0YW5jZSxcclxuICAgIG5hbWU6IHN0cmluZyxcclxuICAgIGRhdGEgPSBudWxsLFxyXG4gICAgb25Db21wbGV0ZTogKGluc3RhbmNlOiBQb3B1cEluc3RhbmNlKSA9PiB2b2lkID0gbnVsbCxcclxuICAgIHBhcmVudDogY2MuTm9kZSA9IG51bGwsXHJcbiAgICBzaG93TG9hZGluZ1BvcFVwOiBib29sZWFuID0gdHJ1ZVxyXG4gICkge1xyXG4gICAgbGV0IHNlbGYgPSBVSU1hbmFnZXIuX2luc3RhbmNlO1xyXG4gICAgcGFyZW50ID0gcGFyZW50ID8gcGFyZW50IDogc2VsZi5wb3B1cFRyYW47XHJcbiAgICBpZiAobmFtZSA9PSBzZWxmLl9jdXJyZW50UG9wdXBOYW1lICYmIHNlbGYuX2N1cnJlbnRQb3B1cC5ub2RlKSB7XHJcbiAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKHNlbGYuX2N1cnJlbnRQb3B1cCk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHNlbGYuX2N1cnJlbnRQb3B1cE5hbWUgPSBuYW1lO1xyXG4gICAgc2VsZi5nZXRQb3B1cEluc3RhbmNlKFxyXG4gICAgICB0eXBlLFxyXG4gICAgICBuYW1lLFxyXG4gICAgICAoaW5zdGFuY2UpID0+IHtcclxuICAgICAgICBpZiAoc2VsZi5jdXJyZW50UG9wdXAgPT0gaW5zdGFuY2UpIHtcclxuICAgICAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKHNlbGYuY3VycmVudFBvcHVwKTtcclxuICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHNlbGYuY3VycmVudFBvcHVwID0gaW5zdGFuY2U7XHJcbiAgICAgICAgaW5zdGFuY2Uub3BlbihkYXRhKTtcclxuICAgICAgICBpbnN0YW5jZS5fY2xvc2UgPSAoKSA9PiB7XHJcbiAgICAgICAgICBzZWxmLmN1cnJlbnRQb3B1cCA9IG51bGw7XHJcbiAgICAgICAgICBzZWxmLl9jdXJyZW50UG9wdXBOYW1lID0gXCJcIjtcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKHNlbGYuY3VycmVudFBvcHVwKTtcclxuICAgICAgfSxcclxuICAgICAgcGFyZW50LFxyXG4gICAgICBzaG93TG9hZGluZ1BvcFVwXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9wZW5Qb3B1cFdpdGhFcnJvckhhbmRsZXIodHlwZTogdHlwZW9mIFBvcHVwSW5zdGFuY2UsIG5hbWU6IHN0cmluZywgZGF0YSA9IG51bGwsIHBhcmVudDogY2MuTm9kZSA9IG51bGwsIHNob3dMb2FkaW5nUG9wVXA6IGJvb2xlYW4gPSB0cnVlKSB7XHJcbiAgICBsZXQgc2VsZiA9IFVJTWFuYWdlci5faW5zdGFuY2U7XHJcbiAgICBwYXJlbnQgPSBwYXJlbnQgPyBwYXJlbnQgOiBzZWxmLnBvcHVwVHJhbjtcclxuICAgIGlmIChuYW1lID09IHNlbGYuX2N1cnJlbnRQb3B1cE5hbWUgJiYgc2VsZi5fY3VycmVudFBvcHVwICE9IG51bGwgJiYgc2VsZi5fY3VycmVudFBvcHVwLm5vZGUpIHtcclxuICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPFBvcHVwSW5zdGFuY2U+KChyZXNvbHZlKSA9PiB7XHJcbiAgICAgICAgcmVzb2x2ZShzZWxmLl9jdXJyZW50UG9wdXApO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzZWxmLl9jdXJyZW50UG9wdXBOYW1lID0gbmFtZTtcclxuICAgIHJldHVybiBzZWxmXHJcbiAgICAgIC5nZXRQb3B1cEluc3RhbmNlV2l0aEVycm9yKHR5cGUsIG5hbWUsIHBhcmVudCwgc2hvd0xvYWRpbmdQb3BVcClcclxuICAgICAgLnRoZW4oKGluc3RhbmNlOiBQb3B1cEluc3RhbmNlKSA9PiB7XHJcbiAgICAgICAgaWYgKHNlbGYuY3VycmVudFBvcHVwID09IGluc3RhbmNlKSB7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2U8UG9wdXBJbnN0YW5jZT4oKHJlc29sdmUpID0+IHtcclxuICAgICAgICAgICAgcmVzb2x2ZShzZWxmLmN1cnJlbnRQb3B1cCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHNlbGYuY3VycmVudFBvcHVwID0gaW5zdGFuY2U7XHJcbiAgICAgICAgaW5zdGFuY2Uub3BlbihkYXRhKTtcclxuICAgICAgICBpbnN0YW5jZS5fY2xvc2UgPSAoKSA9PiB7XHJcbiAgICAgICAgICBzZWxmLmN1cnJlbnRQb3B1cCA9IG51bGw7XHJcbiAgICAgICAgICBzZWxmLl9jdXJyZW50UG9wdXBOYW1lID0gXCJcIjtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gaW5zdGFuY2U7XHJcbiAgICAgIH0pXHJcbiAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICBpZiAoc2hvd0xvYWRpbmdQb3BVcCkge1xyXG4gICAgICAgICAgc2VsZi5fY3VycmVudFBvcHVwTmFtZSA9IFwiXCI7XHJcbiAgICAgICAgICBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5jbG9zZVBvcHVwU3lzdGVtKExvYWRpbmdQb3B1cFN5c3RlbSwgR2FtZURlZmluZS5Mb2FkaW5nUG9wdXBTeXN0ZW0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5vcGVuUG9wdXBTeXN0ZW0oV2FybmluZ1BvcHVwU3lzdGVtLCBHYW1lRGVmaW5lLldhcm5pbmdQb3B1cFN5c3RlbSwgUG9ydGFsVGV4dC5QT1JfTUVTU19ORVRXT1JLX0RJU0NPTk5FQ1QpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBoYXNQb3B1cEluc3RhbmNlKG5hbWU6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIFVJTWFuYWdlci5faW5zdGFuY2UuX3BvcHVwSW5zdGFuY2VzLmhhcyhuYW1lKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBjbG9zZVBvcHVwKHR5cGU6IHR5cGVvZiBQb3B1cEluc3RhbmNlLCBuYW1lOiBzdHJpbmcpIHtcclxuICAgIGxldCBzZWxmID0gVUlNYW5hZ2VyLl9pbnN0YW5jZTtcclxuICAgIHNlbGYuZ2V0UG9wdXBJbnN0YW5jZSh0eXBlLCBuYW1lLCAoaW5zdGFuY2UpID0+IHtcclxuICAgICAgaW5zdGFuY2UuY2xvc2VJbnN0YW5jZSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0UG9wdXBJbnN0YW5jZSh0eXBlOiB0eXBlb2YgUG9wdXBJbnN0YW5jZSwgbmFtZTogc3RyaW5nLCBvbkNvbXBsZXRlOiAoaW5zdGFuY2U6IFBvcHVwSW5zdGFuY2UpID0+IHZvaWQsIHBhcmVudDogY2MuTm9kZSA9IG51bGwsIHNob3dMb2FkaW5nUG9wVXA6IGJvb2xlYW4gPSB0cnVlKSB7XHJcbiAgICBsZXQgc2VsZiA9IFVJTWFuYWdlci5faW5zdGFuY2U7XHJcbiAgICBjb25zdCBfcGFyZW50ID0gcGFyZW50ID09IG51bGwgPyBjYy5kaXJlY3Rvci5nZXRTY2VuZSgpLmdldENoaWxkQnlOYW1lKFwiQ2FudmFzXCIpIDogcGFyZW50O1xyXG4gICAgaWYgKCFzZWxmLl9wb3B1cEluc3RhbmNlcykgc2VsZi5fcG9wdXBJbnN0YW5jZXMgPSBuZXcgTWFwPHN0cmluZywgUG9wdXBJbnN0YW5jZT4oKTtcclxuICAgIGlmIChzZWxmLl9wb3B1cEluc3RhbmNlcy5oYXMobmFtZSkgJiYgc2VsZi5fcG9wdXBJbnN0YW5jZXMuZ2V0KG5hbWUpICE9IG51bGwgJiYgc2VsZi5fcG9wdXBJbnN0YW5jZXMuZ2V0KG5hbWUpLm5vZGUgIT0gbnVsbCkge1xyXG4gICAgICBsZXQgcG9wdXAgPSBzZWxmLl9wb3B1cEluc3RhbmNlcy5nZXQobmFtZSk7XHJcbiAgICAgIHBvcHVwLm5vZGUucGFyZW50ID0gbnVsbDtcclxuICAgICAgX3BhcmVudC5hZGRDaGlsZChwb3B1cC5ub2RlKTtcclxuICAgICAgb25Db21wbGV0ZShwb3B1cCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAoc2hvd0xvYWRpbmdQb3BVcCkge1xyXG4gICAgICAgIHNlbGYub3BlblBvcHVwU3lzdGVtKExvYWRpbmdQb3B1cFN5c3RlbSwgR2FtZURlZmluZS5Mb2FkaW5nUG9wdXBTeXN0ZW0sIHRydWUsIG51bGwsIG51bGwsIChwb3B1cExvYWRpbmcpID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZFBvcFVwUHJlZmFiKHR5cGUsIG5hbWUsIG9uQ29tcGxldGUsIF9wYXJlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMubG9hZFBvcFVwUHJlZmFiKHR5cGUsIG5hbWUsIG9uQ29tcGxldGUsIF9wYXJlbnQpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgYXN5bmMgZ2V0UG9wdXBJbnN0YW5jZVdpdGhFcnJvcih0eXBlOiB0eXBlb2YgUG9wdXBJbnN0YW5jZSwgbmFtZTogc3RyaW5nLCBwYXJlbnQ6IGNjLk5vZGUgPSBudWxsLCBzaG93TG9hZGluZ1BvcFVwOiBib29sZWFuID0gdHJ1ZSkge1xyXG4gICAgbGV0IHNlbGYgPSBVSU1hbmFnZXIuX2luc3RhbmNlO1xyXG4gICAgY29uc3QgX3BhcmVudCA9IHBhcmVudCA9PSBudWxsID8gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShcIkNhbnZhc1wiKSA6IHBhcmVudDtcclxuICAgIGlmICghc2VsZi5fcG9wdXBJbnN0YW5jZXMpIHNlbGYuX3BvcHVwSW5zdGFuY2VzID0gbmV3IE1hcDxzdHJpbmcsIFBvcHVwSW5zdGFuY2U+KCk7XHJcbiAgICBpZiAoc2VsZi5fcG9wdXBJbnN0YW5jZXMuaGFzKG5hbWUpICYmIHNlbGYuX3BvcHVwSW5zdGFuY2VzLmdldChuYW1lKSAhPSBudWxsICYmIHNlbGYuX3BvcHVwSW5zdGFuY2VzLmdldChuYW1lKS5ub2RlICE9IG51bGwpIHtcclxuICAgICAgbGV0IHBvcHVwID0gc2VsZi5fcG9wdXBJbnN0YW5jZXMuZ2V0KG5hbWUpO1xyXG4gICAgICBwb3B1cC5ub2RlLnBhcmVudCA9IG51bGw7XHJcbiAgICAgIF9wYXJlbnQuYWRkQ2hpbGQocG9wdXAubm9kZSk7XHJcbiAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxQb3B1cEluc3RhbmNlPigocmVzb2x2ZSkgPT4ge1xyXG4gICAgICAgIHJlc29sdmUocG9wdXApO1xyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmIChzaG93TG9hZGluZ1BvcFVwKSB7XHJcbiAgICAgICAgYXdhaXQgc2VsZi5vcGVuUG9wdXBTeXN0ZW1WMihMb2FkaW5nUG9wdXBTeXN0ZW0sIEdhbWVEZWZpbmUuTG9hZGluZ1BvcHVwU3lzdGVtLCB0cnVlLCBudWxsLCBudWxsKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5sb2FkUG9wVXBQcmVmYWJXaXRoRXJyb3JIYW5kbGVyKHR5cGUsIG5hbWUsIF9wYXJlbnQpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmxvYWRQb3BVcFByZWZhYldpdGhFcnJvckhhbmRsZXIodHlwZSwgbmFtZSwgX3BhcmVudCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgbG9hZFBvcFVwUHJlZmFiKHR5cGU6IHR5cGVvZiBQb3B1cEluc3RhbmNlLCBuYW1lOiBzdHJpbmcsIG9uQ29tcGxldGU6IChpbnN0YW5jZTogUG9wdXBJbnN0YW5jZSkgPT4gdm9pZCwgcGFyZW50OiBjYy5Ob2RlID0gbnVsbCkge1xyXG4gICAgbGV0IHNlbGYgPSBVSU1hbmFnZXIuX2luc3RhbmNlO1xyXG4gICAgUmVzb3VyY2VNYW5hZ2VyLmxvYWRQcmVmYWIoXCJQcmVmYWJzL1BvcHVwL1wiICsgbmFtZSwgZnVuY3Rpb24gKHByZWZhYikge1xyXG4gICAgICBjb25zdCBuZXdOb2RlID0gY2MuaW5zdGFudGlhdGUocHJlZmFiKSBhcyB1bmtub3duIGFzIGNjLk5vZGU7XHJcbiAgICAgIGNvbnN0IGluc3RhbmNlID0gbmV3Tm9kZS5nZXRDb21wb25lbnQodHlwZSk7XHJcbiAgICAgIHBhcmVudC5hZGRDaGlsZChuZXdOb2RlKTtcclxuICAgICAgc2VsZi5fcG9wdXBJbnN0YW5jZXMuc2V0KG5hbWUsIGluc3RhbmNlKTtcclxuICAgICAgc2VsZi5jbG9zZVBvcHVwU3lzdGVtKExvYWRpbmdQb3B1cFN5c3RlbSwgR2FtZURlZmluZS5Mb2FkaW5nUG9wdXBTeXN0ZW0pO1xyXG4gICAgICBvbkNvbXBsZXRlKGluc3RhbmNlKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBsb2FkUG9wVXBQcmVmYWJXaXRoRXJyb3JIYW5kbGVyKHR5cGU6IHR5cGVvZiBQb3B1cEluc3RhbmNlLCBuYW1lOiBzdHJpbmcsIHBhcmVudDogY2MuTm9kZSA9IG51bGwpIHtcclxuICAgIGxldCBzZWxmID0gVUlNYW5hZ2VyLl9pbnN0YW5jZTtcclxuICAgIHJldHVybiBSZXNvdXJjZU1hbmFnZXIubG9hZFByZWZhYldpdGhFcnJvckhhbmRsZXIoXCJQcmVmYWJzL1BvcHVwL1wiICsgbmFtZSkudGhlbihmdW5jdGlvbiAocHJlZmFiKSB7XHJcbiAgICAgIGNvbnN0IG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpIGFzIHVua25vd24gYXMgY2MuTm9kZTtcclxuICAgICAgY29uc3QgaW5zdGFuY2UgPSBuZXdOb2RlLmdldENvbXBvbmVudCh0eXBlKTtcclxuICAgICAgcGFyZW50LmFkZENoaWxkKG5ld05vZGUpO1xyXG4gICAgICBzZWxmLl9wb3B1cEluc3RhbmNlcy5zZXQobmFtZSwgaW5zdGFuY2UpO1xyXG4gICAgICBzZWxmLmNsb3NlUG9wdXBTeXN0ZW0oTG9hZGluZ1BvcHVwU3lzdGVtLCBHYW1lRGVmaW5lLkxvYWRpbmdQb3B1cFN5c3RlbSk7XHJcbiAgICAgIHJldHVybiBpbnN0YW5jZTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9wZW5Ob3RpZnkodHlwZTogdHlwZW9mIE5vdGlmeUluc3RhbmNlLCBuYW1lOiBzdHJpbmcsIGRhdGEgPSBudWxsLCBwYXJlbnQ6IGNjLk5vZGUgPSBudWxsKSB7XHJcbiAgICBsZXQgc2VsZiA9IFVJTWFuYWdlci5faW5zdGFuY2U7XHJcbiAgICBwYXJlbnQgPSBwYXJlbnQgPyBwYXJlbnQgOiBzZWxmLm5vdGlmeVRyYW47XHJcbiAgICBpZiAobmFtZSA9PSBzZWxmLl9jdXJyZW50Tm90aWZ5TmFtZSkgcmV0dXJuO1xyXG4gICAgc2VsZi5fY3VycmVudE5vdGlmeU5hbWUgPSBuYW1lO1xyXG4gICAgc2VsZi5nZXROb3RpZnlJbnN0YW5jZShcclxuICAgICAgdHlwZSxcclxuICAgICAgbmFtZSxcclxuICAgICAgKGluc3RhbmNlKSA9PiB7XHJcbiAgICAgICAgaWYgKHNlbGYuY3VycmVudE5vdGlmeSA9PSBpbnN0YW5jZSkgcmV0dXJuO1xyXG5cclxuICAgICAgICBzZWxmLmN1cnJlbnROb3RpZnkgPSBpbnN0YW5jZTtcclxuICAgICAgICBpbnN0YW5jZS5vcGVuKGRhdGEpO1xyXG4gICAgICAgIGluc3RhbmNlLl9jbG9zZSA9ICgpID0+IHtcclxuICAgICAgICAgIHNlbGYuY3VycmVudE5vdGlmeSA9IG51bGw7XHJcbiAgICAgICAgICBzZWxmLl9jdXJyZW50Tm90aWZ5TmFtZSA9IFwiXCI7XHJcbiAgICAgICAgfTtcclxuICAgICAgfSxcclxuICAgICAgcGFyZW50XHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldE5vdGlmeUluc3RhbmNlKHR5cGU6IHR5cGVvZiBOb3RpZnlJbnN0YW5jZSwgbmFtZTogc3RyaW5nLCBvbkNvbXBsZXRlOiAoaW5zdGFuY2U6IE5vdGlmeUluc3RhbmNlKSA9PiB2b2lkLCBwYXJlbnQ6IGNjLk5vZGUgPSBudWxsKSB7XHJcbiAgICBsZXQgc2VsZiA9IFVJTWFuYWdlci5faW5zdGFuY2U7XHJcbiAgICBsZXQgX3BhcmVudCA9IHBhcmVudCA9PSBudWxsID8gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShcIkNhbnZhc1wiKSA6IHBhcmVudDtcclxuICAgIGlmIChuYW1lID09IEdhbWVEZWZpbmUuVGV4dE5vdGlmeSkgX3BhcmVudCA9IHNlbGYuX3BlcnNpc05vdGlmeVJvb3Q7XHJcblxyXG4gICAgaWYgKCFzZWxmLl9ub3RpZnlJbnN0YW5jZXMpIHNlbGYuX25vdGlmeUluc3RhbmNlcyA9IG5ldyBNYXA8c3RyaW5nLCBOb3RpZnlJbnN0YW5jZT4oKTtcclxuICAgIGlmIChzZWxmLl9ub3RpZnlJbnN0YW5jZXMuaGFzKG5hbWUpICYmIHNlbGYuX25vdGlmeUluc3RhbmNlcy5nZXQobmFtZSkgIT0gbnVsbCAmJiBzZWxmLl9ub3RpZnlJbnN0YW5jZXMuZ2V0KG5hbWUpLm5vZGUgIT0gbnVsbCkge1xyXG4gICAgICBsZXQgbm90aWZ5ID0gc2VsZi5fbm90aWZ5SW5zdGFuY2VzLmdldChuYW1lKTtcclxuICAgICAgaWYgKG5hbWUgPT0gR2FtZURlZmluZS5UZXh0Tm90aWZ5KSB7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbm90aWZ5Lm5vZGUucGFyZW50ID0gbnVsbDtcclxuICAgICAgICBfcGFyZW50LmFkZENoaWxkKG5vdGlmeS5ub2RlKTtcclxuICAgICAgfVxyXG4gICAgICBvbkNvbXBsZXRlKG5vdGlmeSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBSZXNvdXJjZU1hbmFnZXIubG9hZFByZWZhYihcIlByZWZhYnMvTm90aWZ5L1wiICsgbmFtZSwgZnVuY3Rpb24gKHByZWZhYikge1xyXG4gICAgICAgIGNvbnN0IG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpIGFzIHVua25vd24gYXMgY2MuTm9kZTtcclxuICAgICAgICBjb25zdCBpbnN0YW5jZSA9IG5ld05vZGUuZ2V0Q29tcG9uZW50KHR5cGUpO1xyXG4gICAgICAgIF9wYXJlbnQuYWRkQ2hpbGQobmV3Tm9kZSk7XHJcbiAgICAgICAgc2VsZi5fbm90aWZ5SW5zdGFuY2VzLnNldChuYW1lLCBpbnN0YW5jZSk7XHJcbiAgICAgICAgb25Db21wbGV0ZShpbnN0YW5jZSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIG9wZW5Qb3B1cFN5c3RlbShcclxuICAgIHR5cGU6IHR5cGVvZiBQb3B1cFN5c3RlbUluc3RhbmNlLFxyXG4gICAgbmFtZTogc3RyaW5nLFxyXG4gICAgZGF0YSA9IG51bGwsXHJcbiAgICBvblllczogKCkgPT4gdm9pZCA9IG51bGwsXHJcbiAgICBvbk5vOiAoKSA9PiB2b2lkID0gbnVsbCxcclxuICAgIG9uQ29tcGxldGU6IChpbnN0YW5jZTogUG9wdXBTeXN0ZW1JbnN0YW5jZSkgPT4gdm9pZCA9IG51bGwsXHJcbiAgICBwYXJlbnQ6IGNjLk5vZGUgPSBudWxsXHJcbiAgKSB7XHJcbiAgICBsZXQgc2VsZiA9IFVJTWFuYWdlci5faW5zdGFuY2U7XHJcbiAgICAvLyBzZWxmLnBvcHVwU3lzdGVtVHJhbiA9IGNjLmRpcmVjdG9yLmdldFNjZW5lKCkuZ2V0Q29tcG9uZW50SW5DaGlsZHJlbihNaW5pR2FtZU1hbmFnZXIpPy5wb3B1cFN5c3RlbVRyYW47XHJcbiAgICBwYXJlbnQgPSBwYXJlbnQgPyBwYXJlbnQgOiBzZWxmLnBvcHVwU3lzdGVtVHJhbjtcclxuXHJcbiAgICBpZiAobmFtZSA9PSBHYW1lRGVmaW5lLkxvYWRpbmdQb3B1cFN5c3RlbSkge1xyXG4gICAgICBzZWxmLl9jdXJyZW50TG9hZGluZysrO1xyXG4gICAgfVxyXG4gICAgaWYgKG5hbWUgPT0gc2VsZi5fY3VycmVudFBvcHVwU3lzdGVtTmFtZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAoc2VsZi5fY3VycmVudFBvcHVwU3lzdGVtICYmIHNlbGYuX2N1cnJlbnRQb3B1cFN5c3RlbS5ub2RlKSB7XHJcbiAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKHNlbGYuX2N1cnJlbnRQb3B1cFN5c3RlbSk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHNlbGYuX2N1cnJlbnRQb3B1cFN5c3RlbU5hbWUgPSBuYW1lO1xyXG4gICAgc2VsZi5nZXRQb3B1cFN5c3RlbUluc3RhbmNlKFxyXG4gICAgICB0eXBlLFxyXG4gICAgICBuYW1lLFxyXG4gICAgICAoaW5zdGFuY2UpID0+IHtcclxuICAgICAgICBpZiAoc2VsZi5fY3VycmVudFBvcHVwU3lzdGVtID09IGluc3RhbmNlKSB7XHJcbiAgICAgICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZShzZWxmLl9jdXJyZW50UG9wdXBTeXN0ZW0pO1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc2VsZi5fY3VycmVudFBvcHVwU3lzdGVtID0gaW5zdGFuY2U7XHJcbiAgICAgICAgaW5zdGFuY2Uub3BlbihkYXRhLCBvblllcywgb25Obyk7XHJcbiAgICAgICAgaW5zdGFuY2UuX2Nsb3NlID0gKCkgPT4ge1xyXG4gICAgICAgICAgc2VsZi5fY3VycmVudFBvcHVwU3lzdGVtID0gbnVsbDtcclxuICAgICAgICAgIHNlbGYuX2N1cnJlbnRQb3B1cFN5c3RlbU5hbWUgPSBcIlwiO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgaWYgKG9uQ29tcGxldGUpIG9uQ29tcGxldGUoaW5zdGFuY2UpO1xyXG4gICAgICB9LFxyXG4gICAgICBwYXJlbnRcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb3BlblBvcHVwU3lzdGVtVjIoXHJcbiAgICB0eXBlOiB0eXBlb2YgUG9wdXBTeXN0ZW1JbnN0YW5jZSxcclxuICAgIG5hbWU6IHN0cmluZyxcclxuICAgIGRhdGEgPSBudWxsLFxyXG4gICAgb25ZZXM6ICgpID0+IHZvaWQgPSBudWxsLFxyXG4gICAgb25ObzogKCkgPT4gdm9pZCA9IG51bGwsXHJcbiAgICBwYXJlbnQ6IGNjLk5vZGUgPSBudWxsXHJcbiAgKTogUHJvbWlzZTxQb3B1cFN5c3RlbUluc3RhbmNlPiB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2U8UG9wdXBTeXN0ZW1JbnN0YW5jZT4oKHJlc29sdmUpID0+IHtcclxuICAgICAgbGV0IHNlbGYgPSBVSU1hbmFnZXIuX2luc3RhbmNlO1xyXG4gICAgICAvLyBzZWxmLnBvcHVwU3lzdGVtVHJhbiA9IGNjLmRpcmVjdG9yLmdldFNjZW5lKCkuZ2V0Q29tcG9uZW50SW5DaGlsZHJlbihNaW5pR2FtZU1hbmFnZXIpPy5wb3B1cFN5c3RlbVRyYW47XHJcbiAgICAgIHBhcmVudCA9IHBhcmVudCA/IHBhcmVudCA6IHNlbGYucG9wdXBTeXN0ZW1UcmFuO1xyXG5cclxuICAgICAgaWYgKG5hbWUgPT0gR2FtZURlZmluZS5Mb2FkaW5nUG9wdXBTeXN0ZW0pIHtcclxuICAgICAgICBzZWxmLl9jdXJyZW50TG9hZGluZysrO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAobmFtZSA9PSBzZWxmLl9jdXJyZW50UG9wdXBTeXN0ZW1OYW1lICYmIHNlbGYuX2N1cnJlbnRQb3B1cFN5c3RlbSAmJiBzZWxmLl9jdXJyZW50UG9wdXBTeXN0ZW0ubm9kZSkge1xyXG4gICAgICAgIHJlc29sdmUoc2VsZi5fY3VycmVudFBvcHVwU3lzdGVtKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHNlbGYuX2N1cnJlbnRQb3B1cFN5c3RlbU5hbWUgPSBuYW1lO1xyXG4gICAgICBzZWxmLmdldFBvcHVwU3lzdGVtSW5zdGFuY2UoXHJcbiAgICAgICAgdHlwZSxcclxuICAgICAgICBuYW1lLFxyXG4gICAgICAgIChpbnN0YW5jZSkgPT4ge1xyXG4gICAgICAgICAgaWYgKHNlbGYuX2N1cnJlbnRQb3B1cFN5c3RlbSA9PSBpbnN0YW5jZSkge1xyXG4gICAgICAgICAgICByZXNvbHZlKHNlbGYuX2N1cnJlbnRQb3B1cFN5c3RlbSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBzZWxmLl9jdXJyZW50UG9wdXBTeXN0ZW0gPSBpbnN0YW5jZTtcclxuICAgICAgICAgIGluc3RhbmNlLm9wZW4oZGF0YSwgb25ZZXMsIG9uTm8pO1xyXG4gICAgICAgICAgaW5zdGFuY2UuX2Nsb3NlID0gKCkgPT4ge1xyXG4gICAgICAgICAgICBzZWxmLl9jdXJyZW50UG9wdXBTeXN0ZW0gPSBudWxsO1xyXG4gICAgICAgICAgICBzZWxmLl9jdXJyZW50UG9wdXBTeXN0ZW1OYW1lID0gXCJcIjtcclxuICAgICAgICAgIH07XHJcbiAgICAgICAgICByZXNvbHZlKGluc3RhbmNlKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHBhcmVudFxyXG4gICAgICApO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xvc2VBbGxQb3B1cFN5c3RlbSgpIHtcclxuICAgIGxldCBzZWxmID0gVUlNYW5hZ2VyLl9pbnN0YW5jZTtcclxuICAgIHNlbGYuX3BvcHVwU3lzdGVtSW5zdGFuY2VzLmZvckVhY2goKHApID0+IHtcclxuICAgICAgaWYgKHAgIT0gbnVsbCAmJiBwLm5vZGUgIT0gbnVsbCkgcC5jbG9zZUluc3RhbmNlKCk7XHJcbiAgICAgIHNlbGYuX2N1cnJlbnRMb2FkaW5nID0gMDtcclxuICAgICAgc2VsZi5fY3VycmVudFBvcHVwU3lzdGVtTmFtZSA9IFwiXCI7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBjbG9zZUFsbFBvcHVwKHNob3VsZENsb3NlUHJvZmlsZVBvcHVwOiBib29sZWFuID0gdHJ1ZSkge1xyXG4gICAgbGV0IHNlbGYgPSBVSU1hbmFnZXIuX2luc3RhbmNlO1xyXG4gICAgc2VsZi5fcG9wdXBJbnN0YW5jZXMuZm9yRWFjaCgocCwgbmFtZSkgPT4ge1xyXG4gICAgICBpZiAoIXNob3VsZENsb3NlUHJvZmlsZVBvcHVwKSB7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHAgIT0gbnVsbCAmJiBwLm5vZGUgIT0gbnVsbCkgcC5jbG9zZUluc3RhbmNlKCk7XHJcbiAgICAgIHNlbGYuX2N1cnJlbnRQb3B1cE5hbWUgPSBcIlwiO1xyXG4gICAgfSk7XHJcbiAgICBzZWxmLmNsb3NlQWxsUG9wdXBTeXN0ZW0oKTtcclxuICB9XHJcblxyXG4gIGNsb3NlUGF5bWVudFBvcHVwKCkge1xyXG4gICAgaWYgKHRoaXMucGF5bWVudFdpdGhEcmF3Tm9kZSkge1xyXG4gICAgICBpZiAodGhpcy5wYXltZW50V2l0aERyYXdOb2RlLmFjdGl2ZSkgdGhpcy5wYXltZW50V2l0aERyYXdOb2RlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMucGF5bWVudFRvcFVwTm9kZSkge1xyXG4gICAgICBpZiAodGhpcy5wYXltZW50VG9wVXBOb2RlLmFjdGl2ZSkgdGhpcy5wYXltZW50VG9wVXBOb2RlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGNsb3NlUG9wdXBTeXN0ZW0odHlwZTogdHlwZW9mIFBvcHVwU3lzdGVtSW5zdGFuY2UsIG5hbWU6IHN0cmluZykge1xyXG4gICAgbGV0IHNlbGYgPSBVSU1hbmFnZXIuX2luc3RhbmNlO1xyXG4gICAgaWYgKG5hbWUgPT0gR2FtZURlZmluZS5Mb2FkaW5nUG9wdXBTeXN0ZW0pIHtcclxuICAgICAgc2VsZi5fY3VycmVudExvYWRpbmctLTtcclxuICAgICAgaWYgKHNlbGYuX2N1cnJlbnRMb2FkaW5nIDwgMCkgc2VsZi5fY3VycmVudExvYWRpbmcgPSAwO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbGYuZ2V0UG9wdXBTeXN0ZW1JbnN0YW5jZSh0eXBlLCBuYW1lLCAoaW5zdGFuY2UpID0+IHtcclxuICAgICAgaWYgKHNlbGYuX2N1cnJlbnRMb2FkaW5nID09IDApIHtcclxuICAgICAgICBpbnN0YW5jZS5jbG9zZUluc3RhbmNlKCk7XHJcbiAgICAgICAgc2VsZi5fY3VycmVudFBvcHVwU3lzdGVtTmFtZSA9IFwiXCI7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldFBvcHVwU3lzdGVtSW5zdGFuY2UodHlwZTogdHlwZW9mIFBvcHVwU3lzdGVtSW5zdGFuY2UsIG5hbWU6IHN0cmluZywgb25Db21wbGV0ZTogKGluc3RhbmNlOiBQb3B1cFN5c3RlbUluc3RhbmNlKSA9PiB2b2lkLCBwYXJlbnQ6IGNjLk5vZGUgPSBudWxsKSB7XHJcbiAgICBsZXQgc2VsZiA9IFVJTWFuYWdlci5faW5zdGFuY2U7XHJcbiAgICBsZXQgX3BhcmVudCA9IHBhcmVudCA9PSBudWxsID8gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShcIkNhbnZhc1wiKSA6IHBhcmVudDtcclxuXHJcbiAgICBfcGFyZW50ID0gc2VsZi5fcGVyc2lzdFBvcHVwU3lzdGVtUm9vdDtcclxuXHJcbiAgICBpZiAoIXNlbGYuX3BvcHVwU3lzdGVtSW5zdGFuY2VzKSBzZWxmLl9wb3B1cFN5c3RlbUluc3RhbmNlcyA9IG5ldyBNYXA8c3RyaW5nLCBQb3B1cFN5c3RlbUluc3RhbmNlPigpO1xyXG4gICAgaWYgKHNlbGYuX3BvcHVwU3lzdGVtSW5zdGFuY2VzLmhhcyhuYW1lKSAmJiBzZWxmLl9wb3B1cFN5c3RlbUluc3RhbmNlcy5nZXQobmFtZSkgIT0gbnVsbCAmJiBzZWxmLl9wb3B1cFN5c3RlbUluc3RhbmNlcy5nZXQobmFtZSkubm9kZSAhPSBudWxsKSB7XHJcbiAgICAgIGxldCBwb3B1cCA9IHNlbGYuX3BvcHVwU3lzdGVtSW5zdGFuY2VzLmdldChuYW1lKTtcclxuICAgICAgcG9wdXAubm9kZS5wYXJlbnQgPSBudWxsO1xyXG4gICAgICBfcGFyZW50LmFkZENoaWxkKHBvcHVwLm5vZGUpO1xyXG4gICAgICBvbkNvbXBsZXRlKHBvcHVwKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIFJlc291cmNlTWFuYWdlci5sb2FkUHJlZmFiKFwiUHJlZmFicy9Qb3B1cFN5c3RlbS9cIiArIG5hbWUsIGZ1bmN0aW9uIChwcmVmYWIpIHtcclxuICAgICAgICBjb25zdCBuZXdOb2RlID0gY2MuaW5zdGFudGlhdGUocHJlZmFiKSBhcyB1bmtub3duIGFzIGNjLk5vZGU7XHJcbiAgICAgICAgY29uc3QgaW5zdGFuY2UgPSBuZXdOb2RlLmdldENvbXBvbmVudCh0eXBlKTtcclxuICAgICAgICBfcGFyZW50LmFkZENoaWxkKG5ld05vZGUpO1xyXG4gICAgICAgIHNlbGYuX3BvcHVwU3lzdGVtSW5zdGFuY2VzLnNldChuYW1lLCBpbnN0YW5jZSk7XHJcblxyXG4gICAgICAgIG9uQ29tcGxldGUoaW5zdGFuY2UpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvcGVuQWN0aW9uRWxlbWVudFBvcHVwKHR5cGU6IHR5cGVvZiBQb3B1cEluc3RhbmNlLCBuYW1lOiBzdHJpbmcsIGRhdGEgPSBudWxsLCBvbkNvbXBsZXRlOiAoaW5zdGFuY2U6IFBvcHVwU3lzdGVtSW5zdGFuY2UpID0+IHZvaWQgPSBudWxsLCBwYXJlbnQ6IGNjLk5vZGUgPSBudWxsKSB7XHJcbiAgICAvLyBjYy5sb2coXCJvcGVuIGFjdGlvbiBwb3B1cFwiKTtcclxuICAgIGxldCBzZWxmID0gVUlNYW5hZ2VyLl9pbnN0YW5jZTtcclxuICAgIHBhcmVudCA9IHBhcmVudCA/IHBhcmVudCA6IHNlbGYucG9wdXBUcmFuO1xyXG4gICAgLy8gaWYgKG5hbWUgPT0gc2VsZi5fY3VycmVudFBvcHVwTmFtZSkge1xyXG4gICAgLy8gICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZShzZWxmLl9jdXJyZW50UG9wdXApO1xyXG4gICAgLy8gICByZXR1cm47XHJcbiAgICAvLyB9XHJcbiAgICBzZWxmLl9jdXJyZW50UG9wdXBOYW1lID0gbmFtZTtcclxuICAgIHNlbGYuZ2V0QWN0aW9uRWxlbWVudFBvcHVwSW5zdGFuY2UoXHJcbiAgICAgIHR5cGUsXHJcbiAgICAgIG5hbWUsXHJcbiAgICAgIChpbnN0YW5jZSkgPT4ge1xyXG4gICAgICAgIGlmIChzZWxmLmN1cnJlbnRQb3B1cCA9PSBpbnN0YW5jZSkge1xyXG4gICAgICAgICAgaWYgKG9uQ29tcGxldGUpIG9uQ29tcGxldGUoc2VsZi5jdXJyZW50UG9wdXApO1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc2VsZi5jdXJyZW50UG9wdXAgPSBpbnN0YW5jZTtcclxuICAgICAgICBpbnN0YW5jZS5vcGVuKGRhdGEpO1xyXG4gICAgICAgIGluc3RhbmNlLl9jbG9zZSA9ICgpID0+IHtcclxuICAgICAgICAgIHNlbGYuY3VycmVudFBvcHVwID0gbnVsbDtcclxuICAgICAgICAgIHNlbGYuX2N1cnJlbnRQb3B1cE5hbWUgPSBcIlwiO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgaWYgKG9uQ29tcGxldGUpIG9uQ29tcGxldGUoc2VsZi5jdXJyZW50UG9wdXApO1xyXG4gICAgICB9LFxyXG4gICAgICBwYXJlbnRcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xvc2VBY3Rpb25FbGVtZW50UG9wdXAodHlwZTogdHlwZW9mIFBvcHVwSW5zdGFuY2UsIG5hbWU6IHN0cmluZykge1xyXG4gICAgbGV0IHNlbGYgPSBVSU1hbmFnZXIuX2luc3RhbmNlO1xyXG4gICAgc2VsZi5nZXRBY3Rpb25FbGVtZW50UG9wdXBJbnN0YW5jZSh0eXBlLCBuYW1lLCAoaW5zdGFuY2UpID0+IHtcclxuICAgICAgaW5zdGFuY2UuY2xvc2VJbnN0YW5jZSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0QWN0aW9uRWxlbWVudFBvcHVwSW5zdGFuY2UodHlwZTogdHlwZW9mIFBvcHVwSW5zdGFuY2UsIG5hbWU6IHN0cmluZywgb25Db21wbGV0ZTogKGluc3RhbmNlOiBQb3B1cEluc3RhbmNlKSA9PiB2b2lkLCBwYXJlbnQ6IGNjLk5vZGUgPSBudWxsKSB7XHJcbiAgICBsZXQgc2VsZiA9IFVJTWFuYWdlci5faW5zdGFuY2U7XHJcbiAgICBjb25zdCBfcGFyZW50ID0gcGFyZW50ID09IG51bGwgPyBjYy5kaXJlY3Rvci5nZXRTY2VuZSgpLmdldENoaWxkQnlOYW1lKFwiQ2FudmFzXCIpIDogcGFyZW50O1xyXG4gICAgaWYgKCFzZWxmLl9wb3B1cEluc3RhbmNlcykgc2VsZi5fcG9wdXBJbnN0YW5jZXMgPSBuZXcgTWFwPHN0cmluZywgUG9wdXBJbnN0YW5jZT4oKTtcclxuICAgIGlmIChzZWxmLl9wb3B1cEluc3RhbmNlcy5oYXMobmFtZSkgJiYgc2VsZi5fcG9wdXBJbnN0YW5jZXMuZ2V0KG5hbWUpICE9IG51bGwgJiYgc2VsZi5fcG9wdXBJbnN0YW5jZXMuZ2V0KG5hbWUpLm5vZGUgIT0gbnVsbCkge1xyXG4gICAgICBsZXQgcG9wdXAgPSBzZWxmLl9wb3B1cEluc3RhbmNlcy5nZXQobmFtZSk7XHJcbiAgICAgIHBvcHVwLm5vZGUucGFyZW50ID0gbnVsbDtcclxuICAgICAgX3BhcmVudC5hZGRDaGlsZChwb3B1cC5ub2RlKTtcclxuICAgICAgb25Db21wbGV0ZShwb3B1cCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyBzZWxmLm9wZW5Qb3B1cFN5c3RlbShMb2FkaW5nUG9wdXBTeXN0ZW0sIEdhbWVEZWZpbmUuTG9hZGluZ1BvcHVwU3lzdGVtLCB0cnVlLCBudWxsLCBudWxsLCAocG9wdXBMb2FkaW5nKSA9PiB7XHJcbiAgICAgIFJlc291cmNlTWFuYWdlci5sb2FkUHJlZmFiKFwiUHJlZmFicy9Qb3B1cC9cIiArIG5hbWUsIGZ1bmN0aW9uIChwcmVmYWIpIHtcclxuICAgICAgICBjb25zdCBuZXdOb2RlID0gY2MuaW5zdGFudGlhdGUocHJlZmFiKSBhcyB1bmtub3duIGFzIGNjLk5vZGU7XHJcbiAgICAgICAgY29uc3QgaW5zdGFuY2UgPSBuZXdOb2RlLmdldENvbXBvbmVudCh0eXBlKTtcclxuICAgICAgICBfcGFyZW50LmFkZENoaWxkKG5ld05vZGUpO1xyXG4gICAgICAgIHNlbGYuX3BvcHVwSW5zdGFuY2VzLnNldChuYW1lLCBpbnN0YW5jZSk7XHJcbiAgICAgICAgLy8gc2VsZi5jbG9zZVBvcHVwU3lzdGVtKExvYWRpbmdQb3B1cFN5c3RlbSwgR2FtZURlZmluZS5Mb2FkaW5nUG9wdXBTeXN0ZW0pO1xyXG4gICAgICAgIG9uQ29tcGxldGUoaW5zdGFuY2UpO1xyXG4gICAgICB9KTtcclxuICAgICAgLy8gfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHRleHRUZW1wUGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuICBwcml2YXRlIHVpVGVtcFBhcmVudDogY2MuTm9kZSA9IG51bGw7XHJcbiAgcHJpdmF0ZSBlZmZUZW1wUGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuICBwdWJsaWMgc2V0UGFyZW50VGV4dChub2RlOiBjYy5Ob2RlLCBwYXJlbnQ6IGNjLk5vZGUpIHtcclxuICAgIGlmICghdGhpcy50ZXh0VGVtcFBhcmVudCkge1xyXG4gICAgICB0aGlzLnRleHRUZW1wUGFyZW50ID0gbmV3IGNjLk5vZGUoXCJ0ZXh0VGVtcFwiKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudGV4dFRlbXBQYXJlbnQgPSBuZXcgY2MuTm9kZShcInRleHRUZW1wXCIpO1xyXG4gICAgfVxyXG4gIH1cclxuICBwdWJsaWMgc2V0UGFyZW50VUkobm9kZTogY2MuTm9kZSkge1xyXG4gICAgbGV0IHBhcmVudCA9IG5ldyBjYy5Ob2RlKFwidWlcIik7XHJcbiAgfVxyXG4gIHB1YmxpYyBzZXRQYXJlbnRFZmYobm9kZTogY2MuTm9kZSkge1xyXG4gICAgbGV0IHBhcmVudCA9IG5ldyBjYy5Ob2RlKFwiZWZmXCIpO1xyXG4gIH1cclxufVxyXG5cclxuaW1wb3J0IFNjcmVlbkluc3RhbmNlIGZyb20gXCIuLi9CYXNlL1NjcmVlbkluc3RhbmNlXCI7XHJcbmltcG9ydCBQb3B1cEluc3RhbmNlIGZyb20gXCIuLi9CYXNlL1BvcHVwSW5zdGFuY2VcIjtcclxuaW1wb3J0IFBvcHVwU3lzdGVtSW5zdGFuY2UgZnJvbSBcIi4uL0Jhc2UvUG9wdXBTeXN0ZW1JbnN0YW5jZVwiO1xyXG5pbXBvcnQgRWxlbWVudEluc3RhbmNlIGZyb20gXCIuLi9CYXNlL0VsZW1lbnRJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2FtZURlZmluZSBmcm9tIFwiLi8uLi9EZWZpbmUvR2FtZURlZmluZVwiO1xyXG5pbXBvcnQgTG9hZGluZ1BvcHVwU3lzdGVtIGZyb20gXCIuLi9Qb3B1cC9Mb2FkaW5nUG9wdXBTeXN0ZW1cIjtcclxuaW1wb3J0IE5vdGlmeUluc3RhbmNlIGZyb20gXCIuLi9CYXNlL05vdGlmeUluc3RhbmNlXCI7XHJcbmltcG9ydCBMYXllckhlbHBlciBmcm9tIFwiLi4vVXRpbHMvTGF5ZXJIZWxwZXJcIjtcclxuaW1wb3J0IFJlc291cmNlTWFuYWdlciBmcm9tIFwiLi9SZXNvdXJjZU1hbmFnZXJcIjtcclxuaW1wb3J0IEV2ZW50S2V5IGZyb20gXCJAZ2FtZWxvb3QvY2xpZW50LWJhc2UtaGFuZGxlci9idWlsZC9FdmVudEtleVwiO1xyXG4iXX0=