
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Manager/ResourceManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8656cWMFyBO660KqJ9Bdch7', 'ResourceManager');
// Game/Lobby/Manager/ResourceManager.ts

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameDefine_1 = require("../../../Common/Scripts/Defines/GameDefine");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ResourceManager = /** @class */ (function () {
    function ResourceManager() {
    }
    Object.defineProperty(ResourceManager, "bundle", {
        get: function () {
            return cc.assetManager.getBundle(GameDefine_1.default.PortalBundle);
        },
        enumerable: false,
        configurable: true
    });
    ResourceManager.loadPrefab = function (path, onComplete) {
        if (onComplete === void 0) { onComplete = null; }
        cc.resources.load(path, cc.Prefab, function (err, prefab) {
            if (err) {
                cc.log(err);
                return;
            }
            if (onComplete)
                onComplete(prefab);
        });
    };
    ResourceManager.loadPrefabWithErrorHandler = function (path) {
        return new Promise(function (resolve, reject) {
            cc.resources.load(path, function (err, prefab) {
                if (err) {
                    cc.log(err);
                    reject(err);
                    return;
                }
                resolve(prefab);
            });
        });
    };
    ResourceManager.loadSound = function (path, onComplete) {
        if (onComplete === void 0) { onComplete = null; }
        cc.resources.load(path, cc.AudioClip, function (err, sound) {
            if (err) {
                cc.log(err);
                return;
            }
            if (onComplete)
                onComplete(sound);
        });
    };
    ResourceManager = __decorate([
        ccclass
    ], ResourceManager);
    return ResourceManager;
}());
exports.default = ResourceManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L01hbmFnZXIvUmVzb3VyY2VNYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUVBQW9FO0FBRTlELElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQUE7SUFzQ0EsQ0FBQztJQXJDQyxzQkFBa0IseUJBQU07YUFBeEI7WUFDRSxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLG9CQUFVLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDNUQsQ0FBQzs7O09BQUE7SUFFYSwwQkFBVSxHQUF4QixVQUF5QixJQUFZLEVBQUUsVUFBOEM7UUFBOUMsMkJBQUEsRUFBQSxpQkFBOEM7UUFDbkYsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBVSxHQUFHLEVBQUUsTUFBaUI7WUFDakUsSUFBSSxHQUFHLEVBQUU7Z0JBQ1AsRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDWixPQUFPO2FBQ1I7WUFDRCxJQUFJLFVBQVU7Z0JBQUUsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVhLDBDQUEwQixHQUF4QyxVQUF5QyxJQUFZO1FBQ25ELE9BQU8sSUFBSSxPQUFPLENBQXdCLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDeEQsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQVUsR0FBRyxFQUFFLE1BQU07Z0JBQzNDLElBQUksR0FBRyxFQUFFO29CQUNQLEVBQUUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ1osTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNaLE9BQU87aUJBQ1I7Z0JBRUQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2xCLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRWEseUJBQVMsR0FBdkIsVUFBd0IsSUFBWSxFQUFFLFVBQWdEO1FBQWhELDJCQUFBLEVBQUEsaUJBQWdEO1FBQ3BGLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLFVBQVUsR0FBRyxFQUFFLEtBQW1CO1lBQ3RFLElBQUksR0FBRyxFQUFFO2dCQUNQLEVBQUUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ1osT0FBTzthQUNSO1lBQ0QsSUFBSSxVQUFVO2dCQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFyQ2tCLGVBQWU7UUFEbkMsT0FBTztPQUNhLGVBQWUsQ0FzQ25DO0lBQUQsc0JBQUM7Q0F0Q0QsQUFzQ0MsSUFBQTtrQkF0Q29CLGVBQWUiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgR2FtZURlZmluZSBmcm9tIFwiLi4vLi4vLi4vQ29tbW9uL1NjcmlwdHMvRGVmaW5lcy9HYW1lRGVmaW5lXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZXNvdXJjZU1hbmFnZXIge1xuICBwdWJsaWMgc3RhdGljIGdldCBidW5kbGUoKTogY2MuQXNzZXRNYW5hZ2VyLkJ1bmRsZSB7XG4gICAgcmV0dXJuIGNjLmFzc2V0TWFuYWdlci5nZXRCdW5kbGUoR2FtZURlZmluZS5Qb3J0YWxCdW5kbGUpO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBsb2FkUHJlZmFiKHBhdGg6IHN0cmluZywgb25Db21wbGV0ZTogKHByZWZhYjogY2MuUHJlZmFiKSA9PiB2b2lkID0gbnVsbCkge1xuICAgIGNjLnJlc291cmNlcy5sb2FkKHBhdGgsIGNjLlByZWZhYiwgZnVuY3Rpb24gKGVyciwgcHJlZmFiOiBjYy5QcmVmYWIpIHtcbiAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgY2MubG9nKGVycik7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGlmIChvbkNvbXBsZXRlKSBvbkNvbXBsZXRlKHByZWZhYik7XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGxvYWRQcmVmYWJXaXRoRXJyb3JIYW5kbGVyKHBhdGg6IHN0cmluZyk6IFByb21pc2U8Y2MuQXNzZXQgfCBjYy5Bc3NldFtdPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPGNjLkFzc2V0IHwgY2MuQXNzZXRbXT4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgY2MucmVzb3VyY2VzLmxvYWQocGF0aCwgZnVuY3Rpb24gKGVyciwgcHJlZmFiKSB7XG4gICAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgICBjYy5sb2coZXJyKTtcbiAgICAgICAgICByZWplY3QoZXJyKTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICByZXNvbHZlKHByZWZhYik7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgbG9hZFNvdW5kKHBhdGg6IHN0cmluZywgb25Db21wbGV0ZTogKHNvdW5kOiBjYy5BdWRpb0NsaXApID0+IHZvaWQgPSBudWxsKSB7XG4gICAgY2MucmVzb3VyY2VzLmxvYWQocGF0aCwgY2MuQXVkaW9DbGlwLCBmdW5jdGlvbiAoZXJyLCBzb3VuZDogY2MuQXVkaW9DbGlwKSB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIGNjLmxvZyhlcnIpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBpZiAob25Db21wbGV0ZSkgb25Db21wbGV0ZShzb3VuZCk7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==