
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Manager/TextNotify.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b9856sUrmpKQaj+DaSEBfj0', 'TextNotify');
// Game/Lobby/Manager/TextNotify.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TextNotifyData = void 0;
var NotifyInstance_1 = require("../Base/NotifyInstance");
var Vec3 = cc.Vec3;
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var TextNotifyData = /** @class */ (function () {
    function TextNotifyData(msg, duration, position, onClick) {
        if (duration === void 0) { duration = 2; }
        if (position === void 0) { position = new Vec3(0, 0, 0); }
        if (onClick === void 0) { onClick = null; }
        this.onClick = null;
        this.msg = "";
        this.duration = 2;
        this.position = null;
        this.msg = msg;
        this.duration = duration;
        this.position = position;
        this.onClick = onClick;
    }
    return TextNotifyData;
}());
exports.TextNotifyData = TextNotifyData;
var TextNotify = /** @class */ (function (_super) {
    __extends(TextNotify, _super);
    function TextNotify() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.content = null;
        _this.rootNode = null;
        _this.listSprFrame = [];
        _this.data = null;
        return _this;
    }
    TextNotify.prototype.onShow = function (data) {
        var _this = this;
        this.data = data;
        var notifyData = data;
        this.content.string = notifyData.msg;
        cc.Tween.stopAllByTarget(this.node);
        cc.tween(this.node)
            .delay(notifyData.duration)
            .call(function () {
            _this.closeInstance();
        })
            .start();
        // let urlFont = "Fonts/MyriadPro-Regular";
        // cc.resources.load(urlFont, cc.Font, function (err, font) {
        //     if (!err && font) {
        //       this.content.font = font;
        //     }
        //   }.bind(this)
        // );
        this.content.node.color = new cc.Color(255, 255, 255);
        this.rootNode.getComponent(cc.Layout).paddingTop = 14;
        this.rootNode.getComponent(cc.Layout).paddingBottom = 14;
        this.rootNode.getComponent(cc.Sprite).spriteFrame = this.listSprFrame[0];
    };
    TextNotify.prototype.afterShow = function () { };
    TextNotify.prototype.beforeShow = function () { };
    TextNotify.prototype.beforeClose = function () { };
    TextNotify.prototype.afterClose = function () { };
    TextNotify.prototype.onClick = function () {
        if (this.data) {
            if (this.data.onClick)
                this.data.onClick();
        }
    };
    __decorate([
        property(cc.Label)
    ], TextNotify.prototype, "content", void 0);
    __decorate([
        property(cc.Node)
    ], TextNotify.prototype, "rootNode", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], TextNotify.prototype, "listSprFrame", void 0);
    TextNotify = __decorate([
        ccclass
    ], TextNotify);
    return TextNotify;
}(NotifyInstance_1.default));
exports.default = TextNotify;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L01hbmFnZXIvVGV4dE5vdGlmeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseURBQW9EO0FBQ3BELElBQU8sSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUM7QUFDdEIsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7QUFFNUUsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFDNUM7SUFLRSx3QkFBWSxHQUFXLEVBQUUsUUFBb0IsRUFBRSxRQUFrQyxFQUFFLE9BQTBCO1FBQXBGLHlCQUFBLEVBQUEsWUFBb0I7UUFBRSx5QkFBQSxFQUFBLGVBQXFCLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUFFLHdCQUFBLEVBQUEsY0FBMEI7UUFKdEcsWUFBTyxHQUFlLElBQUksQ0FBQztRQUMzQixRQUFHLEdBQVcsRUFBRSxDQUFDO1FBQ2pCLGFBQVEsR0FBVyxDQUFDLENBQUM7UUFDckIsYUFBUSxHQUFTLElBQUksQ0FBQztRQUUzQixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNmLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0lBQ3pCLENBQUM7SUFDSCxxQkFBQztBQUFELENBWEEsQUFXQyxJQUFBO0FBWFksd0NBQWM7QUFjM0I7SUFBd0MsOEJBQWM7SUFBdEQ7UUFBQSxxRUFpREM7UUEvQ0MsYUFBTyxHQUFhLElBQUksQ0FBQztRQUd6QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLGtCQUFZLEdBQXFCLEVBQUUsQ0FBQztRQUU1QixVQUFJLEdBQW1CLElBQUksQ0FBQzs7SUF1Q3RDLENBQUM7SUF0Q1csMkJBQU0sR0FBaEIsVUFBaUIsSUFBSTtRQUFyQixpQkF1QkM7UUF0QkMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxVQUFVLEdBQUcsSUFBc0IsQ0FBQztRQUN4QyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUMsR0FBRyxDQUFDO1FBQ3JDLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDaEIsS0FBSyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7YUFDMUIsSUFBSSxDQUFDO1lBQ0osS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsQ0FBQzthQUNELEtBQUssRUFBRSxDQUFDO1FBRVgsMkNBQTJDO1FBQzNDLDZEQUE2RDtRQUM3RCwwQkFBMEI7UUFDMUIsa0NBQWtDO1FBQ2xDLFFBQVE7UUFDUixpQkFBaUI7UUFDakIsS0FBSztRQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUN0RCxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN6RCxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDM0UsQ0FBQztJQUVTLDhCQUFTLEdBQW5CLGNBQXVCLENBQUM7SUFFZCwrQkFBVSxHQUFwQixjQUF3QixDQUFDO0lBRWYsZ0NBQVcsR0FBckIsY0FBeUIsQ0FBQztJQUVoQiwrQkFBVSxHQUFwQixjQUF3QixDQUFDO0lBRWxCLDRCQUFPLEdBQWQ7UUFDRSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDYixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTztnQkFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQzVDO0lBQ0gsQ0FBQztJQTlDRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOytDQUNNO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0RBQ087SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQztvREFDVztJQVJqQixVQUFVO1FBRDlCLE9BQU87T0FDYSxVQUFVLENBaUQ5QjtJQUFELGlCQUFDO0NBakRELEFBaURDLENBakR1Qyx3QkFBYyxHQWlEckQ7a0JBakRvQixVQUFVIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IE5vdGlmeUluc3RhbmNlIGZyb20gXCIuLi9CYXNlL05vdGlmeUluc3RhbmNlXCI7XG5pbXBvcnQgVmVjMyA9IGNjLlZlYzM7XG4vLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcbmV4cG9ydCBjbGFzcyBUZXh0Tm90aWZ5RGF0YSB7XG4gIHB1YmxpYyBvbkNsaWNrOiAoKSA9PiB2b2lkID0gbnVsbDtcbiAgcHVibGljIG1zZzogc3RyaW5nID0gXCJcIjtcbiAgcHVibGljIGR1cmF0aW9uOiBudW1iZXIgPSAyO1xuICBwdWJsaWMgcG9zaXRpb246IFZlYzMgPSBudWxsO1xuICBjb25zdHJ1Y3Rvcihtc2c6IHN0cmluZywgZHVyYXRpb246IG51bWJlciA9IDIsIHBvc2l0aW9uOiBWZWMzID0gbmV3IFZlYzMoMCwgMCwgMCksIG9uQ2xpY2s6ICgpID0+IHZvaWQgPSBudWxsKSB7XG4gICAgdGhpcy5tc2cgPSBtc2c7XG4gICAgdGhpcy5kdXJhdGlvbiA9IGR1cmF0aW9uO1xuICAgIHRoaXMucG9zaXRpb24gPSBwb3NpdGlvbjtcbiAgICB0aGlzLm9uQ2xpY2sgPSBvbkNsaWNrO1xuICB9XG59XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUZXh0Tm90aWZ5IGV4dGVuZHMgTm90aWZ5SW5zdGFuY2Uge1xuICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gIGNvbnRlbnQ6IGNjLkxhYmVsID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgcm9vdE5vZGU6IGNjLk5vZGUgPSBudWxsO1xuXG4gIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcbiAgbGlzdFNwckZyYW1lOiBjYy5TcHJpdGVGcmFtZVtdID0gW107XG5cbiAgcHJpdmF0ZSBkYXRhOiBUZXh0Tm90aWZ5RGF0YSA9IG51bGw7XG4gIHByb3RlY3RlZCBvblNob3coZGF0YSkge1xuICAgIHRoaXMuZGF0YSA9IGRhdGE7XG4gICAgbGV0IG5vdGlmeURhdGEgPSBkYXRhIGFzIFRleHROb3RpZnlEYXRhO1xuICAgIHRoaXMuY29udGVudC5zdHJpbmcgPSBub3RpZnlEYXRhLm1zZztcbiAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYXJnZXQodGhpcy5ub2RlKTtcbiAgICBjYy50d2Vlbih0aGlzLm5vZGUpXG4gICAgICAuZGVsYXkobm90aWZ5RGF0YS5kdXJhdGlvbilcbiAgICAgIC5jYWxsKCgpID0+IHtcbiAgICAgICAgdGhpcy5jbG9zZUluc3RhbmNlKCk7XG4gICAgICB9KVxuICAgICAgLnN0YXJ0KCk7XG5cbiAgICAvLyBsZXQgdXJsRm9udCA9IFwiRm9udHMvTXlyaWFkUHJvLVJlZ3VsYXJcIjtcbiAgICAvLyBjYy5yZXNvdXJjZXMubG9hZCh1cmxGb250LCBjYy5Gb250LCBmdW5jdGlvbiAoZXJyLCBmb250KSB7XG4gICAgLy8gICAgIGlmICghZXJyICYmIGZvbnQpIHtcbiAgICAvLyAgICAgICB0aGlzLmNvbnRlbnQuZm9udCA9IGZvbnQ7XG4gICAgLy8gICAgIH1cbiAgICAvLyAgIH0uYmluZCh0aGlzKVxuICAgIC8vICk7XG4gICAgdGhpcy5jb250ZW50Lm5vZGUuY29sb3IgPSBuZXcgY2MuQ29sb3IoMjU1LCAyNTUsIDI1NSk7XG4gICAgdGhpcy5yb290Tm9kZS5nZXRDb21wb25lbnQoY2MuTGF5b3V0KS5wYWRkaW5nVG9wID0gMTQ7XG4gICAgdGhpcy5yb290Tm9kZS5nZXRDb21wb25lbnQoY2MuTGF5b3V0KS5wYWRkaW5nQm90dG9tID0gMTQ7XG4gICAgdGhpcy5yb290Tm9kZS5nZXRDb21wb25lbnQoY2MuU3ByaXRlKS5zcHJpdGVGcmFtZSA9IHRoaXMubGlzdFNwckZyYW1lWzBdO1xuICB9XG5cbiAgcHJvdGVjdGVkIGFmdGVyU2hvdygpIHt9XG5cbiAgcHJvdGVjdGVkIGJlZm9yZVNob3coKSB7fVxuXG4gIHByb3RlY3RlZCBiZWZvcmVDbG9zZSgpIHt9XG5cbiAgcHJvdGVjdGVkIGFmdGVyQ2xvc2UoKSB7fVxuXG4gIHB1YmxpYyBvbkNsaWNrKCkge1xuICAgIGlmICh0aGlzLmRhdGEpIHtcbiAgICAgIGlmICh0aGlzLmRhdGEub25DbGljaykgdGhpcy5kYXRhLm9uQ2xpY2soKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==