
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Manager/NetworkManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '59efcxXUClF+6cTLHjkNntH', 'NetworkManager');
// Game/Lobby/Manager/NetworkManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var PortalText_1 = require("../Utils/PortalText");
var GameDefine_1 = require("../Define/GameDefine");
var EnumGame_1 = require("../Utils/EnumGame");
var BenTauHandler_1 = require("../Network/BenTauHandler");
var IdentityHandler_1 = require("../Network/IdentityHandler");
var WalletHandler_1 = require("../Network/WalletHandler");
var LayerHelper_1 = require("../Utils/LayerHelper");
var GameUtils_1 = require("../Utils/GameUtils");
var EventBusManager_1 = require("../Event/EventBusManager");
var EventManager_1 = require("../Event/EventManager");
var UIManager_1 = require("./UIManager");
var TextNotify_1 = require("../Manager/TextNotify");
var TextNotifyDisconnect_1 = require("../Manager/TextNotifyDisconnect");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var RetryPopupSystem_1 = require("../Popup/RetryPopupSystem");
var WarningPopupSystem_1 = require("../Popup/WarningPopupSystem");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var NetworkManager = /** @class */ (function (_super) {
    __extends(NetworkManager, _super);
    function NetworkManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.textNotifyDisconnect = null;
        _this.retryPopupSystem = null;
        return _this;
    }
    NetworkManager.prototype.onLoad = function () {
        cc.game.addPersistRootNode(this.node);
        this.node.zIndex = LayerHelper_1.default.netWorkManager;
        var resourceLocalPaths = [
        // chỉ thêm chứ không xóa nha
        ];
        cc.resources.preload(resourceLocalPaths, cc.Prefab, function (err, prefab) {
            if (err) {
                return;
            }
        });
    };
    NetworkManager.prototype.start = function () {
        var _this = this;
        EventManager_1.default.on(EventManager_1.default.ON_DISCONNECT, function (code, isRetry, messError) {
            if (code === void 0) { code = 0; }
            if (isRetry === void 0) { isRetry = false; }
            cc.log("ON_DISCONNECT code = " + code + " isRetry = " + isRetry);
            // if (UserManager.getInstance().isLogin) {
            //   this.backToPortal(code, isRetry, messError);
            // }
            UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, messError);
        }, this);
        EventManager_1.default.on(EventManager_1.default.ON_PING_FAIL, function () {
            cc.log("Ping Fail");
            var duration = 3;
            var textData = new TextNotify_1.TextNotifyData(PortalText_1.default.POR_NOTIFY_PING_FAIL, duration);
            if (_this.textNotifyDisconnect) {
                _this.textNotifyDisconnect.node.active = true;
                _this.textNotifyDisconnect.onShow(textData);
            }
            _this.scheduleOnce(function () {
                if (BenTauHandler_1.default.getInstance().client) {
                    UIManager_1.default.getInstance()
                        .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, true, null, null)
                        .then(function () {
                        var _a;
                        cc.log("onClose ON_PING_FAIL ");
                        // BenTauHandler.getInstance().client?.close(CodeSocket.RETRY, "PING FAIL");
                        (_a = BenTauHandler_1.default.getInstance().client) === null || _a === void 0 ? void 0 : _a.reConnect(EnumGame_1.CodeSocket.RETRY, "PING FAIL");
                        EventBusManager_1.default.onPingError();
                    });
                }
            }, duration);
        }, this);
        EventManager_1.default.on(EventManager_1.default.ON_PING_OK, function () {
            cc.log("Ping OK");
        }, this);
        // cc.game.on(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
        // cc.game.on(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
        EventManager_1.default.on(EventManager_1.default.ON_RECONNECTED, function () {
            WalletHandler_1.default.getInstance().onMyWalletRequest(function (response) { });
        }, this);
        EventManager_1.default.on(EventManager_1.default.ON_SHOW_RETRY_POPUP, function () {
            cc.log("this.textNotifyDisconnect " + _this.textNotifyDisconnect);
            if (_this.textNotifyDisconnect) {
                _this.textNotifyDisconnect.node.active = false;
            }
            if (_this.retryPopupSystem) {
                _this.retryPopupSystem.node.active = true;
                _this.retryPopupSystem.onShow();
            }
        }, this);
        if (!GameUtils_1.default.isStatusPing) {
            EventManager_1.default.fire(EventManager_1.default.ON_PING_FAIL);
        }
    };
    // protected onChangeTabHide() {
    //   cc.log("ON_HIDE_GAME ");
    //   EventManager.fire(EventManager.ON_HIDE_GAME);
    // }
    // protected onChangeTabShow() {
    //   cc.log("ON_SHOW_GAME");
    //   EventManager.fire(EventManager.ON_SHOW_GAME);
    //   if (GameUtils.isNative() && UserManager.getInstance().isLogin) {
    //     WalletHandler.getInstance().onMyWalletRequest((response) => {
    //       //EventManager.fire(EventManager.onUpdateBalance, response.getWallet().getCurrentBalance());
    //     });
    //   }
    // }
    /**
     * Ví dụ cách sử dụng để lấy bến tàu và token
     *let networkHandler = cc.director.getScene().getChildByName("NetworkManager");
      let bentauHandle = networkHandler.getComponent("NetworkManager").getBenTauHandler();
      SicboMessageHandler.benTauHandler = bentauHandle;
     * @returns
     */
    NetworkManager.prototype.getBenTauHandler = function () {
        return BenTauHandler_1.default.getInstance();
    };
    NetworkManager.prototype.getToken = function () {
        return IdentityHandler_1.default.getInstance().token;
    };
    NetworkManager.prototype.onDestroy = function () {
        // cc.game.off(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
        // cc.game.off(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
    };
    __decorate([
        property(TextNotifyDisconnect_1.default)
    ], NetworkManager.prototype, "textNotifyDisconnect", void 0);
    __decorate([
        property(RetryPopupSystem_1.default)
    ], NetworkManager.prototype, "retryPopupSystem", void 0);
    NetworkManager = __decorate([
        ccclass
    ], NetworkManager);
    return NetworkManager;
}(cc.Component));
exports.default = NetworkManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L01hbmFnZXIvTmV0d29ya01hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0Esa0RBQTZDO0FBQzdDLG1EQUE4QztBQUM5Qyw4Q0FBeUQ7QUFDekQsMERBQXFEO0FBQ3JELDhEQUF5RDtBQUV6RCwwREFBcUQ7QUFDckQsb0RBQStDO0FBQy9DLGdEQUEyQztBQUMzQyw0REFBdUQ7QUFDdkQsc0RBQWlEO0FBQ2pELHlDQUFvQztBQUNwQyxvREFBdUQ7QUFDdkQsd0VBQW1FO0FBQ25FLGtFQUE2RDtBQUM3RCw4REFBeUQ7QUFDekQsa0VBQTZEO0FBRXZELElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQTRDLGtDQUFZO0lBQXhEO1FBQUEscUVBc0lDO1FBcElDLDBCQUFvQixHQUF5QixJQUFJLENBQUM7UUFHbEQsc0JBQWdCLEdBQXFCLElBQUksQ0FBQzs7SUFpSTVDLENBQUM7SUEvSEMsK0JBQU0sR0FBTjtRQUNFLEVBQUUsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLHFCQUFXLENBQUMsY0FBYyxDQUFDO1FBQzlDLElBQUksa0JBQWtCLEdBQWE7UUFDakMsNkJBQTZCO1NBQzlCLENBQUM7UUFDRixFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLENBQUMsTUFBTSxFQUFFLFVBQUMsR0FBRyxFQUFFLE1BQU07WUFDOUQsSUFBSSxHQUFHLEVBQUU7Z0JBQ1AsT0FBTzthQUNSO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsOEJBQUssR0FBTDtRQUFBLGlCQThFQztRQTdFQyxzQkFBWSxDQUFDLEVBQUUsQ0FDYixzQkFBWSxDQUFDLGFBQWEsRUFDMUIsVUFBQyxJQUFnQixFQUFFLE9BQXdCLEVBQUUsU0FBaUI7WUFBN0QscUJBQUEsRUFBQSxRQUFnQjtZQUFFLHdCQUFBLEVBQUEsZUFBd0I7WUFDekMsRUFBRSxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLEdBQUcsYUFBYSxHQUFHLE9BQU8sQ0FBQyxDQUFDO1lBQ2pFLDJDQUEyQztZQUMzQyxpREFBaUQ7WUFDakQsSUFBSTtZQUNKLG1CQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsZUFBZSxDQUFDLDRCQUFrQixFQUFFLG9CQUFVLENBQUMsa0JBQWtCLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDeEcsQ0FBQyxFQUNELElBQUksQ0FDTCxDQUFDO1FBRUYsc0JBQVksQ0FBQyxFQUFFLENBQ2Isc0JBQVksQ0FBQyxZQUFZLEVBQ3pCO1lBQ0UsRUFBRSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNwQixJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUM7WUFDakIsSUFBSSxRQUFRLEdBQW1CLElBQUksMkJBQWMsQ0FBQyxvQkFBVSxDQUFDLG9CQUFvQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBRTdGLElBQUksS0FBSSxDQUFDLG9CQUFvQixFQUFFO2dCQUM3QixLQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQzdDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDNUM7WUFFRCxLQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNoQixJQUFJLHVCQUFhLENBQUMsV0FBVyxFQUFFLENBQUMsTUFBTSxFQUFFO29CQUN0QyxtQkFBUyxDQUFDLFdBQVcsRUFBRTt5QkFDcEIsaUJBQWlCLENBQUMsNEJBQWtCLEVBQUUsb0JBQVUsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQzt5QkFDdEYsSUFBSSxDQUFDOzt3QkFDSixFQUFFLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7d0JBQ2hDLDRFQUE0RTt3QkFDNUUsTUFBQSx1QkFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sMENBQUUsU0FBUyxDQUFDLHFCQUFVLENBQUMsS0FBSyxFQUFFLFdBQVcsRUFBRTt3QkFDN0UseUJBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDaEMsQ0FBQyxDQUFDLENBQUM7aUJBQ047WUFDSCxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDZixDQUFDLEVBQ0QsSUFBSSxDQUNMLENBQUM7UUFFRixzQkFBWSxDQUFDLEVBQUUsQ0FDYixzQkFBWSxDQUFDLFVBQVUsRUFDdkI7WUFDRSxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3BCLENBQUMsRUFDRCxJQUFJLENBQ0wsQ0FBQztRQUVGLDhEQUE4RDtRQUM5RCw4REFBOEQ7UUFFOUQsc0JBQVksQ0FBQyxFQUFFLENBQ2Isc0JBQVksQ0FBQyxjQUFjLEVBQzNCO1lBQ0UsdUJBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFDLFFBQVEsSUFBTSxDQUFDLENBQUMsQ0FBQztRQUNsRSxDQUFDLEVBQ0QsSUFBSSxDQUNMLENBQUM7UUFFRixzQkFBWSxDQUFDLEVBQUUsQ0FDYixzQkFBWSxDQUFDLG1CQUFtQixFQUNoQztZQUNFLEVBQUUsQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEdBQUcsS0FBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDakUsSUFBSSxLQUFJLENBQUMsb0JBQW9CLEVBQUU7Z0JBQzdCLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQzthQUMvQztZQUNELElBQUksS0FBSSxDQUFDLGdCQUFnQixFQUFFO2dCQUN6QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ3pDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUNoQztRQUNILENBQUMsRUFDRCxJQUFJLENBQ0wsQ0FBQztRQUVGLElBQUksQ0FBQyxtQkFBUyxDQUFDLFlBQVksRUFBRTtZQUMzQixzQkFBWSxDQUFDLElBQUksQ0FBQyxzQkFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQzlDO0lBQ0gsQ0FBQztJQUVELGdDQUFnQztJQUNoQyw2QkFBNkI7SUFDN0Isa0RBQWtEO0lBQ2xELElBQUk7SUFFSixnQ0FBZ0M7SUFDaEMsNEJBQTRCO0lBQzVCLGtEQUFrRDtJQUNsRCxxRUFBcUU7SUFDckUsb0VBQW9FO0lBQ3BFLHFHQUFxRztJQUNyRyxVQUFVO0lBQ1YsTUFBTTtJQUNOLElBQUk7SUFFSjs7Ozs7O09BTUc7SUFFSCx5Q0FBZ0IsR0FBaEI7UUFDRSxPQUFPLHVCQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDckMsQ0FBQztJQUVELGlDQUFRLEdBQVI7UUFDRSxPQUFPLHlCQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDO0lBQzdDLENBQUM7SUFFRCxrQ0FBUyxHQUFUO1FBQ0UsK0RBQStEO1FBQy9ELCtEQUErRDtJQUNqRSxDQUFDO0lBbklEO1FBREMsUUFBUSxDQUFDLDhCQUFvQixDQUFDO2dFQUNtQjtJQUdsRDtRQURDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQzs0REFDZTtJQUx2QixjQUFjO1FBRGxDLE9BQU87T0FDYSxjQUFjLENBc0lsQztJQUFELHFCQUFDO0NBdElELEFBc0lDLENBdEkyQyxFQUFFLENBQUMsU0FBUyxHQXNJdkQ7a0JBdElvQixjQUFjIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVG9waWMgfSBmcm9tIFwiQGdhbWVsb290L3RvcGljL3RvcGljX3BiXCI7XG5pbXBvcnQgUG9ydGFsVGV4dCBmcm9tIFwiLi4vVXRpbHMvUG9ydGFsVGV4dFwiO1xuaW1wb3J0IEdhbWVEZWZpbmUgZnJvbSBcIi4uL0RlZmluZS9HYW1lRGVmaW5lXCI7XG5pbXBvcnQgeyBDb2RlU29ja2V0LCBHYW1lVHlwZSB9IGZyb20gXCIuLi9VdGlscy9FbnVtR2FtZVwiO1xuaW1wb3J0IEJlblRhdUhhbmRsZXIgZnJvbSBcIi4uL05ldHdvcmsvQmVuVGF1SGFuZGxlclwiO1xuaW1wb3J0IElkZW50aXR5SGFuZGxlciBmcm9tIFwiLi4vTmV0d29yay9JZGVudGl0eUhhbmRsZXJcIjtcbmltcG9ydCBTdGFiaWxpemVDb25uZWN0aW9uIGZyb20gXCIuLi9OZXR3b3JrL1N0YWJpbGl6ZUNvbm5lY3Rpb25cIjtcbmltcG9ydCBXYWxsZXRIYW5kbGVyIGZyb20gXCIuLi9OZXR3b3JrL1dhbGxldEhhbmRsZXJcIjtcbmltcG9ydCBMYXllckhlbHBlciBmcm9tIFwiLi4vVXRpbHMvTGF5ZXJIZWxwZXJcIjtcbmltcG9ydCBHYW1lVXRpbHMgZnJvbSBcIi4uL1V0aWxzL0dhbWVVdGlsc1wiO1xuaW1wb3J0IEV2ZW50QnVzTWFuYWdlciBmcm9tIFwiLi4vRXZlbnQvRXZlbnRCdXNNYW5hZ2VyXCI7XG5pbXBvcnQgRXZlbnRNYW5hZ2VyIGZyb20gXCIuLi9FdmVudC9FdmVudE1hbmFnZXJcIjtcbmltcG9ydCBVSU1hbmFnZXIgZnJvbSBcIi4vVUlNYW5hZ2VyXCI7XG5pbXBvcnQgeyBUZXh0Tm90aWZ5RGF0YSB9IGZyb20gXCIuLi9NYW5hZ2VyL1RleHROb3RpZnlcIjtcbmltcG9ydCBUZXh0Tm90aWZ5RGlzY29ubmVjdCBmcm9tIFwiLi4vTWFuYWdlci9UZXh0Tm90aWZ5RGlzY29ubmVjdFwiO1xuaW1wb3J0IExvYWRpbmdQb3B1cFN5c3RlbSBmcm9tIFwiLi4vUG9wdXAvTG9hZGluZ1BvcHVwU3lzdGVtXCI7XG5pbXBvcnQgUmV0cnlQb3B1cFN5c3RlbSBmcm9tIFwiLi4vUG9wdXAvUmV0cnlQb3B1cFN5c3RlbVwiO1xuaW1wb3J0IFdhcm5pbmdQb3B1cFN5c3RlbSBmcm9tIFwiLi4vUG9wdXAvV2FybmluZ1BvcHVwU3lzdGVtXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBOZXR3b3JrTWFuYWdlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gIEBwcm9wZXJ0eShUZXh0Tm90aWZ5RGlzY29ubmVjdClcbiAgdGV4dE5vdGlmeURpc2Nvbm5lY3Q6IFRleHROb3RpZnlEaXNjb25uZWN0ID0gbnVsbDtcblxuICBAcHJvcGVydHkoUmV0cnlQb3B1cFN5c3RlbSlcbiAgcmV0cnlQb3B1cFN5c3RlbTogUmV0cnlQb3B1cFN5c3RlbSA9IG51bGw7XG5cbiAgb25Mb2FkKCkge1xuICAgIGNjLmdhbWUuYWRkUGVyc2lzdFJvb3ROb2RlKHRoaXMubm9kZSk7XG4gICAgdGhpcy5ub2RlLnpJbmRleCA9IExheWVySGVscGVyLm5ldFdvcmtNYW5hZ2VyO1xuICAgIGxldCByZXNvdXJjZUxvY2FsUGF0aHM6IHN0cmluZ1tdID0gW1xuICAgICAgLy8gY2jhu4kgdGjDqm0gY2jhu6kga2jDtG5nIHjDs2EgbmhhXG4gICAgXTtcbiAgICBjYy5yZXNvdXJjZXMucHJlbG9hZChyZXNvdXJjZUxvY2FsUGF0aHMsIGNjLlByZWZhYiwgKGVyciwgcHJlZmFiKSA9PiB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuICBzdGFydCgpIHtcbiAgICBFdmVudE1hbmFnZXIub24oXG4gICAgICBFdmVudE1hbmFnZXIuT05fRElTQ09OTkVDVCxcbiAgICAgIChjb2RlOiBudW1iZXIgPSAwLCBpc1JldHJ5OiBib29sZWFuID0gZmFsc2UsIG1lc3NFcnJvcjogc3RyaW5nKSA9PiB7XG4gICAgICAgIGNjLmxvZyhcIk9OX0RJU0NPTk5FQ1QgY29kZSA9IFwiICsgY29kZSArIFwiIGlzUmV0cnkgPSBcIiArIGlzUmV0cnkpO1xuICAgICAgICAvLyBpZiAoVXNlck1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5pc0xvZ2luKSB7XG4gICAgICAgIC8vICAgdGhpcy5iYWNrVG9Qb3J0YWwoY29kZSwgaXNSZXRyeSwgbWVzc0Vycm9yKTtcbiAgICAgICAgLy8gfVxuICAgICAgICBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5vcGVuUG9wdXBTeXN0ZW0oV2FybmluZ1BvcHVwU3lzdGVtLCBHYW1lRGVmaW5lLldhcm5pbmdQb3B1cFN5c3RlbSwgbWVzc0Vycm9yKTtcbiAgICAgIH0sXG4gICAgICB0aGlzXG4gICAgKTtcblxuICAgIEV2ZW50TWFuYWdlci5vbihcbiAgICAgIEV2ZW50TWFuYWdlci5PTl9QSU5HX0ZBSUwsXG4gICAgICAoKSA9PiB7XG4gICAgICAgIGNjLmxvZyhcIlBpbmcgRmFpbFwiKTtcbiAgICAgICAgbGV0IGR1cmF0aW9uID0gMztcbiAgICAgICAgbGV0IHRleHREYXRhOiBUZXh0Tm90aWZ5RGF0YSA9IG5ldyBUZXh0Tm90aWZ5RGF0YShQb3J0YWxUZXh0LlBPUl9OT1RJRllfUElOR19GQUlMLCBkdXJhdGlvbik7XG5cbiAgICAgICAgaWYgKHRoaXMudGV4dE5vdGlmeURpc2Nvbm5lY3QpIHtcbiAgICAgICAgICB0aGlzLnRleHROb3RpZnlEaXNjb25uZWN0Lm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgICB0aGlzLnRleHROb3RpZnlEaXNjb25uZWN0Lm9uU2hvdyh0ZXh0RGF0YSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XG4gICAgICAgICAgaWYgKEJlblRhdUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5jbGllbnQpIHtcbiAgICAgICAgICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpXG4gICAgICAgICAgICAgIC5vcGVuUG9wdXBTeXN0ZW1WMihMb2FkaW5nUG9wdXBTeXN0ZW0sIEdhbWVEZWZpbmUuTG9hZGluZ1BvcHVwU3lzdGVtLCB0cnVlLCBudWxsLCBudWxsKVxuICAgICAgICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgY2MubG9nKFwib25DbG9zZSBPTl9QSU5HX0ZBSUwgXCIpO1xuICAgICAgICAgICAgICAgIC8vIEJlblRhdUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5jbGllbnQ/LmNsb3NlKENvZGVTb2NrZXQuUkVUUlksIFwiUElORyBGQUlMXCIpO1xuICAgICAgICAgICAgICAgIEJlblRhdUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5jbGllbnQ/LnJlQ29ubmVjdChDb2RlU29ja2V0LlJFVFJZLCBcIlBJTkcgRkFJTFwiKTtcbiAgICAgICAgICAgICAgICBFdmVudEJ1c01hbmFnZXIub25QaW5nRXJyb3IoKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LCBkdXJhdGlvbik7XG4gICAgICB9LFxuICAgICAgdGhpc1xuICAgICk7XG5cbiAgICBFdmVudE1hbmFnZXIub24oXG4gICAgICBFdmVudE1hbmFnZXIuT05fUElOR19PSyxcbiAgICAgICgpID0+IHtcbiAgICAgICAgY2MubG9nKFwiUGluZyBPS1wiKTtcbiAgICAgIH0sXG4gICAgICB0aGlzXG4gICAgKTtcblxuICAgIC8vIGNjLmdhbWUub24oY2MuZ2FtZS5FVkVOVF9ISURFLCB0aGlzLm9uQ2hhbmdlVGFiSGlkZSwgdGhpcyk7XG4gICAgLy8gY2MuZ2FtZS5vbihjYy5nYW1lLkVWRU5UX1NIT1csIHRoaXMub25DaGFuZ2VUYWJTaG93LCB0aGlzKTtcblxuICAgIEV2ZW50TWFuYWdlci5vbihcbiAgICAgIEV2ZW50TWFuYWdlci5PTl9SRUNPTk5FQ1RFRCxcbiAgICAgICgpID0+IHtcbiAgICAgICAgV2FsbGV0SGFuZGxlci5nZXRJbnN0YW5jZSgpLm9uTXlXYWxsZXRSZXF1ZXN0KChyZXNwb25zZSkgPT4ge30pO1xuICAgICAgfSxcbiAgICAgIHRoaXNcbiAgICApO1xuXG4gICAgRXZlbnRNYW5hZ2VyLm9uKFxuICAgICAgRXZlbnRNYW5hZ2VyLk9OX1NIT1dfUkVUUllfUE9QVVAsXG4gICAgICAoKSA9PiB7XG4gICAgICAgIGNjLmxvZyhcInRoaXMudGV4dE5vdGlmeURpc2Nvbm5lY3QgXCIgKyB0aGlzLnRleHROb3RpZnlEaXNjb25uZWN0KTtcbiAgICAgICAgaWYgKHRoaXMudGV4dE5vdGlmeURpc2Nvbm5lY3QpIHtcbiAgICAgICAgICB0aGlzLnRleHROb3RpZnlEaXNjb25uZWN0Lm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMucmV0cnlQb3B1cFN5c3RlbSkge1xuICAgICAgICAgIHRoaXMucmV0cnlQb3B1cFN5c3RlbS5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgICAgdGhpcy5yZXRyeVBvcHVwU3lzdGVtLm9uU2hvdygpO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgdGhpc1xuICAgICk7XG5cbiAgICBpZiAoIUdhbWVVdGlscy5pc1N0YXR1c1BpbmcpIHtcbiAgICAgIEV2ZW50TWFuYWdlci5maXJlKEV2ZW50TWFuYWdlci5PTl9QSU5HX0ZBSUwpO1xuICAgIH1cbiAgfVxuXG4gIC8vIHByb3RlY3RlZCBvbkNoYW5nZVRhYkhpZGUoKSB7XG4gIC8vICAgY2MubG9nKFwiT05fSElERV9HQU1FIFwiKTtcbiAgLy8gICBFdmVudE1hbmFnZXIuZmlyZShFdmVudE1hbmFnZXIuT05fSElERV9HQU1FKTtcbiAgLy8gfVxuXG4gIC8vIHByb3RlY3RlZCBvbkNoYW5nZVRhYlNob3coKSB7XG4gIC8vICAgY2MubG9nKFwiT05fU0hPV19HQU1FXCIpO1xuICAvLyAgIEV2ZW50TWFuYWdlci5maXJlKEV2ZW50TWFuYWdlci5PTl9TSE9XX0dBTUUpO1xuICAvLyAgIGlmIChHYW1lVXRpbHMuaXNOYXRpdmUoKSAmJiBVc2VyTWFuYWdlci5nZXRJbnN0YW5jZSgpLmlzTG9naW4pIHtcbiAgLy8gICAgIFdhbGxldEhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5vbk15V2FsbGV0UmVxdWVzdCgocmVzcG9uc2UpID0+IHtcbiAgLy8gICAgICAgLy9FdmVudE1hbmFnZXIuZmlyZShFdmVudE1hbmFnZXIub25VcGRhdGVCYWxhbmNlLCByZXNwb25zZS5nZXRXYWxsZXQoKS5nZXRDdXJyZW50QmFsYW5jZSgpKTtcbiAgLy8gICAgIH0pO1xuICAvLyAgIH1cbiAgLy8gfVxuXG4gIC8qKlxuICAgKiBWw60gZOG7pSBjw6FjaCBz4butIGThu6VuZyDEkeG7gyBs4bqleSBi4bq/biB0w6B1IHbDoCB0b2tlblxuICAgKmxldCBuZXR3b3JrSGFuZGxlciA9IGNjLmRpcmVjdG9yLmdldFNjZW5lKCkuZ2V0Q2hpbGRCeU5hbWUoXCJOZXR3b3JrTWFuYWdlclwiKTtcbiAgICBsZXQgYmVudGF1SGFuZGxlID0gbmV0d29ya0hhbmRsZXIuZ2V0Q29tcG9uZW50KFwiTmV0d29ya01hbmFnZXJcIikuZ2V0QmVuVGF1SGFuZGxlcigpO1xuICAgIFNpY2JvTWVzc2FnZUhhbmRsZXIuYmVuVGF1SGFuZGxlciA9IGJlbnRhdUhhbmRsZTtcbiAgICogQHJldHVybnMgXG4gICAqL1xuXG4gIGdldEJlblRhdUhhbmRsZXIoKSB7XG4gICAgcmV0dXJuIEJlblRhdUhhbmRsZXIuZ2V0SW5zdGFuY2UoKTtcbiAgfVxuXG4gIGdldFRva2VuKCkge1xuICAgIHJldHVybiBJZGVudGl0eUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS50b2tlbjtcbiAgfVxuXG4gIG9uRGVzdHJveSgpIHtcbiAgICAvLyBjYy5nYW1lLm9mZihjYy5nYW1lLkVWRU5UX0hJREUsIHRoaXMub25DaGFuZ2VUYWJIaWRlLCB0aGlzKTtcbiAgICAvLyBjYy5nYW1lLm9mZihjYy5nYW1lLkVWRU5UX1NIT1csIHRoaXMub25DaGFuZ2VUYWJTaG93LCB0aGlzKTtcbiAgfVxufVxuIl19