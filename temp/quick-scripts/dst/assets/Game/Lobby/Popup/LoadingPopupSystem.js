
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Popup/LoadingPopupSystem.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a43b7LxEXtHeLNnkRjoYdA7', 'LoadingPopupSystem');
// Portal/Common/Scripts/Managers/UIManager/PopupSystem/LoadingPopupSystem.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var PopupSystemInstance_1 = require("../Base/PopupSystemInstance");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var LoadingPopupSystem = /** @class */ (function (_super) {
    __extends(LoadingPopupSystem, _super);
    function LoadingPopupSystem() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoadingPopupSystem.prototype.onShow = function (data) {
        var _this = this;
        // cc.error("show loading popup system");
        if (this.background) {
            this.background.node.active = false;
            cc.Tween.stopAllByTarget(this.background.node);
            cc.tween(this.background.node)
                .delay(1)
                .call(function () {
                _this.background.node.active = true;
            })
                .start();
        }
    };
    LoadingPopupSystem.prototype.afterShow = function () { };
    LoadingPopupSystem.prototype.beforeShow = function () { };
    LoadingPopupSystem.prototype.beforeClose = function () { };
    LoadingPopupSystem.prototype.afterClose = function () { };
    LoadingPopupSystem.prototype.onDisable = function () {
        cc.Tween.stopAllByTarget(this.background.node);
    };
    LoadingPopupSystem = __decorate([
        ccclass
    ], LoadingPopupSystem);
    return LoadingPopupSystem;
}(PopupSystemInstance_1.default));
exports.default = LoadingPopupSystem;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9Qb3J0YWwvQ29tbW9uL1NjcmlwdHMvTWFuYWdlcnMvVUlNYW5hZ2VyL1BvcHVwU3lzdGVtL0xvYWRpbmdQb3B1cFN5c3RlbS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxtRUFBOEQ7QUFFeEQsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBZ0Qsc0NBQW1CO0lBQW5FOztJQTBCQSxDQUFDO0lBekJXLG1DQUFNLEdBQWhCLFVBQWlCLElBQUk7UUFBckIsaUJBWUM7UUFYQyx5Q0FBeUM7UUFDekMsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEMsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMvQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO2lCQUMzQixLQUFLLENBQUMsQ0FBQyxDQUFDO2lCQUNSLElBQUksQ0FBQztnQkFDSixLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ3JDLENBQUMsQ0FBQztpQkFDRCxLQUFLLEVBQUUsQ0FBQztTQUNaO0lBQ0gsQ0FBQztJQUVTLHNDQUFTLEdBQW5CLGNBQXVCLENBQUM7SUFFZCx1Q0FBVSxHQUFwQixjQUF3QixDQUFDO0lBRWYsd0NBQVcsR0FBckIsY0FBeUIsQ0FBQztJQUVoQix1Q0FBVSxHQUFwQixjQUF3QixDQUFDO0lBRXpCLHNDQUFTLEdBQVQ7UUFDRSxFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUF6QmtCLGtCQUFrQjtRQUR0QyxPQUFPO09BQ2Esa0JBQWtCLENBMEJ0QztJQUFELHlCQUFDO0NBMUJELEFBMEJDLENBMUIrQyw2QkFBbUIsR0EwQmxFO2tCQTFCb0Isa0JBQWtCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFBvcHVwU3lzdGVtSW5zdGFuY2UgZnJvbSBcIi4uL0Jhc2UvUG9wdXBTeXN0ZW1JbnN0YW5jZVwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTG9hZGluZ1BvcHVwU3lzdGVtIGV4dGVuZHMgUG9wdXBTeXN0ZW1JbnN0YW5jZSB7XG4gIHByb3RlY3RlZCBvblNob3coZGF0YSkge1xuICAgIC8vIGNjLmVycm9yKFwic2hvdyBsb2FkaW5nIHBvcHVwIHN5c3RlbVwiKTtcbiAgICBpZiAodGhpcy5iYWNrZ3JvdW5kKSB7XG4gICAgICB0aGlzLmJhY2tncm91bmQubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLmJhY2tncm91bmQubm9kZSk7XG4gICAgICBjYy50d2Vlbih0aGlzLmJhY2tncm91bmQubm9kZSlcbiAgICAgICAgLmRlbGF5KDEpXG4gICAgICAgIC5jYWxsKCgpID0+IHtcbiAgICAgICAgICB0aGlzLmJhY2tncm91bmQubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB9KVxuICAgICAgICAuc3RhcnQoKTtcbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgYWZ0ZXJTaG93KCkge31cblxuICBwcm90ZWN0ZWQgYmVmb3JlU2hvdygpIHt9XG5cbiAgcHJvdGVjdGVkIGJlZm9yZUNsb3NlKCkge31cblxuICBwcm90ZWN0ZWQgYWZ0ZXJDbG9zZSgpIHt9XG5cbiAgb25EaXNhYmxlKCkge1xuICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLmJhY2tncm91bmQubm9kZSk7XG4gIH1cbn1cbiJdfQ==