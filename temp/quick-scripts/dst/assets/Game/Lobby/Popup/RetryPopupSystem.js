
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Popup/RetryPopupSystem.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cd84aa3oT5DMLy5VsQ995UZ', 'RetryPopupSystem');
// Game/Lobby/Popup/RetryPopupSystem.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventManager_1 = require("../Event/EventManager");
var PopupSystemInstance_1 = require("../Base/PopupSystemInstance");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var RetryPopupSystem = /** @class */ (function (_super) {
    __extends(RetryPopupSystem, _super);
    function RetryPopupSystem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.content = null;
        _this.limitReconnectTime = 3;
        _this.timeToReconnect = 3;
        _this.timeDelayConfirm = 3;
        return _this;
    }
    RetryPopupSystem.prototype.onShow = function (data) {
        if (data === void 0) { data = null; }
        this.node.active = true;
    };
    RetryPopupSystem.prototype.onClickCancel = function () {
        cc.log("onClickCancel");
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, 0, false);
        this.onHide();
    };
    RetryPopupSystem.prototype.onClickRetry = function () {
        cc.log("onClickRetry");
        EventManager_1.default.fire(EventManager_1.default.ON_PING_FAIL, 0, true);
        this.onHide();
    };
    RetryPopupSystem.prototype.onHide = function () {
        this.node.active = false;
    };
    RetryPopupSystem.prototype.afterShow = function () { };
    RetryPopupSystem.prototype.beforeShow = function () { };
    RetryPopupSystem.prototype.beforeClose = function () { };
    RetryPopupSystem.prototype.afterClose = function () { };
    __decorate([
        property(cc.Label)
    ], RetryPopupSystem.prototype, "content", void 0);
    __decorate([
        property(cc.Integer)
    ], RetryPopupSystem.prototype, "limitReconnectTime", void 0);
    __decorate([
        property(cc.Integer)
    ], RetryPopupSystem.prototype, "timeToReconnect", void 0);
    __decorate([
        property(cc.Integer)
    ], RetryPopupSystem.prototype, "timeDelayConfirm", void 0);
    RetryPopupSystem = __decorate([
        ccclass
    ], RetryPopupSystem);
    return RetryPopupSystem;
}(PopupSystemInstance_1.default));
exports.default = RetryPopupSystem;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L1BvcHVwL1JldHJ5UG9wdXBTeXN0ZW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsc0RBQWlEO0FBQ2pELG1FQUE4RDtBQUV4RCxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUE4QyxvQ0FBbUI7SUFBakU7UUFBQSxxRUFzQ0M7UUFwQ0MsYUFBTyxHQUFhLElBQUksQ0FBQztRQUd6Qix3QkFBa0IsR0FBVyxDQUFDLENBQUM7UUFHL0IscUJBQWUsR0FBVyxDQUFDLENBQUM7UUFHNUIsc0JBQWdCLEdBQVcsQ0FBQyxDQUFDOztJQTJCL0IsQ0FBQztJQTFCUSxpQ0FBTSxHQUFiLFVBQWMsSUFBVztRQUFYLHFCQUFBLEVBQUEsV0FBVztRQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDMUIsQ0FBQztJQUNNLHdDQUFhLEdBQXBCO1FBQ0UsRUFBRSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN4QixzQkFBWSxDQUFDLElBQUksQ0FBQyxzQkFBWSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFFTSx1Q0FBWSxHQUFuQjtRQUNFLEVBQUUsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDdkIsc0JBQVksQ0FBQyxJQUFJLENBQUMsc0JBQVksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNoQixDQUFDO0lBRU0saUNBQU0sR0FBYjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBRVMsb0NBQVMsR0FBbkIsY0FBdUIsQ0FBQztJQUVkLHFDQUFVLEdBQXBCLGNBQXdCLENBQUM7SUFFZixzQ0FBVyxHQUFyQixjQUF5QixDQUFDO0lBRWhCLHFDQUFVLEdBQXBCLGNBQXdCLENBQUM7SUFuQ3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7cURBQ007SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztnRUFDVTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOzZEQUNPO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7OERBQ1E7SUFYVixnQkFBZ0I7UUFEcEMsT0FBTztPQUNhLGdCQUFnQixDQXNDcEM7SUFBRCx1QkFBQztDQXRDRCxBQXNDQyxDQXRDNkMsNkJBQW1CLEdBc0NoRTtrQkF0Q29CLGdCQUFnQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBFdmVudE1hbmFnZXIgZnJvbSBcIi4uL0V2ZW50L0V2ZW50TWFuYWdlclwiO1xuaW1wb3J0IFBvcHVwU3lzdGVtSW5zdGFuY2UgZnJvbSBcIi4uL0Jhc2UvUG9wdXBTeXN0ZW1JbnN0YW5jZVwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmV0cnlQb3B1cFN5c3RlbSBleHRlbmRzIFBvcHVwU3lzdGVtSW5zdGFuY2Uge1xuICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gIGNvbnRlbnQ6IGNjLkxhYmVsID0gbnVsbDtcblxuICBAcHJvcGVydHkoY2MuSW50ZWdlcilcbiAgbGltaXRSZWNvbm5lY3RUaW1lOiBudW1iZXIgPSAzO1xuXG4gIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxuICB0aW1lVG9SZWNvbm5lY3Q6IG51bWJlciA9IDM7XG5cbiAgQHByb3BlcnR5KGNjLkludGVnZXIpXG4gIHRpbWVEZWxheUNvbmZpcm06IG51bWJlciA9IDM7XG4gIHB1YmxpYyBvblNob3coZGF0YSA9IG51bGwpIHtcbiAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgfVxuICBwdWJsaWMgb25DbGlja0NhbmNlbCgpIHtcbiAgICBjYy5sb2coXCJvbkNsaWNrQ2FuY2VsXCIpO1xuICAgIEV2ZW50TWFuYWdlci5maXJlKEV2ZW50TWFuYWdlci5PTl9ESVNDT05ORUNULCAwLCBmYWxzZSk7XG4gICAgdGhpcy5vbkhpZGUoKTtcbiAgfVxuXG4gIHB1YmxpYyBvbkNsaWNrUmV0cnkoKSB7XG4gICAgY2MubG9nKFwib25DbGlja1JldHJ5XCIpO1xuICAgIEV2ZW50TWFuYWdlci5maXJlKEV2ZW50TWFuYWdlci5PTl9QSU5HX0ZBSUwsIDAsIHRydWUpO1xuICAgIHRoaXMub25IaWRlKCk7XG4gIH1cblxuICBwdWJsaWMgb25IaWRlKCkge1xuICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgfVxuXG4gIHByb3RlY3RlZCBhZnRlclNob3coKSB7fVxuXG4gIHByb3RlY3RlZCBiZWZvcmVTaG93KCkge31cblxuICBwcm90ZWN0ZWQgYmVmb3JlQ2xvc2UoKSB7fVxuXG4gIHByb3RlY3RlZCBhZnRlckNsb3NlKCkge31cbn1cbiJdfQ==