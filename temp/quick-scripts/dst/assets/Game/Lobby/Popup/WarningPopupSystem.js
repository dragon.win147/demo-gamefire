
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Popup/WarningPopupSystem.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '87989taWgxHQqEp4D3evvrf', 'WarningPopupSystem');
// Portal/Common/Scripts/Managers/UIManager/PopupSystem/WarningPopupSystem.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var PopupSystemInstance_1 = require("../Base/PopupSystemInstance");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var WarningPopupSystem = /** @class */ (function (_super) {
    __extends(WarningPopupSystem, _super);
    function WarningPopupSystem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.content = null;
        return _this;
    }
    WarningPopupSystem.prototype.onShow = function (data) {
        this.content.string = data;
    };
    WarningPopupSystem.prototype.afterShow = function () { };
    WarningPopupSystem.prototype.beforeShow = function () { };
    WarningPopupSystem.prototype.beforeClose = function () { };
    WarningPopupSystem.prototype.afterClose = function () { };
    __decorate([
        property(cc.Label)
    ], WarningPopupSystem.prototype, "content", void 0);
    WarningPopupSystem = __decorate([
        ccclass
    ], WarningPopupSystem);
    return WarningPopupSystem;
}(PopupSystemInstance_1.default));
exports.default = WarningPopupSystem;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9Qb3J0YWwvQ29tbW9uL1NjcmlwdHMvTWFuYWdlcnMvVUlNYW5hZ2VyL1BvcHVwU3lzdGVtL1dhcm5pbmdQb3B1cFN5c3RlbS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjtBQUNsRixtRUFBOEQ7QUFFeEQsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBZ0Qsc0NBQW1CO0lBQW5FO1FBQUEscUVBZUM7UUFiQyxhQUFPLEdBQWEsSUFBSSxDQUFDOztJQWEzQixDQUFDO0lBWFcsbUNBQU0sR0FBaEIsVUFBaUIsSUFBSTtRQUNuQixJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDN0IsQ0FBQztJQUVTLHNDQUFTLEdBQW5CLGNBQXVCLENBQUM7SUFFZCx1Q0FBVSxHQUFwQixjQUF3QixDQUFDO0lBRWYsd0NBQVcsR0FBckIsY0FBeUIsQ0FBQztJQUVoQix1Q0FBVSxHQUFwQixjQUF3QixDQUFDO0lBWnpCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7dURBQ007SUFGTixrQkFBa0I7UUFEdEMsT0FBTztPQUNhLGtCQUFrQixDQWV0QztJQUFELHlCQUFDO0NBZkQsQUFlQyxDQWYrQyw2QkFBbUIsR0FlbEU7a0JBZm9CLGtCQUFrQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuaW1wb3J0IFBvcHVwU3lzdGVtSW5zdGFuY2UgZnJvbSBcIi4uL0Jhc2UvUG9wdXBTeXN0ZW1JbnN0YW5jZVwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgV2FybmluZ1BvcHVwU3lzdGVtIGV4dGVuZHMgUG9wdXBTeXN0ZW1JbnN0YW5jZSB7XG4gIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgY29udGVudDogY2MuTGFiZWwgPSBudWxsO1xuXG4gIHByb3RlY3RlZCBvblNob3coZGF0YSkge1xuICAgIHRoaXMuY29udGVudC5zdHJpbmcgPSBkYXRhO1xuICB9XG5cbiAgcHJvdGVjdGVkIGFmdGVyU2hvdygpIHt9XG5cbiAgcHJvdGVjdGVkIGJlZm9yZVNob3coKSB7fVxuXG4gIHByb3RlY3RlZCBiZWZvcmVDbG9zZSgpIHt9XG5cbiAgcHJvdGVjdGVkIGFmdGVyQ2xvc2UoKSB7fVxufVxuIl19