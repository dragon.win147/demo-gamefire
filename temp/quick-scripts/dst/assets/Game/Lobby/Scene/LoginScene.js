
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Scene/LoginScene.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cc34foUFw1BFZHh4LKERe8O', 'LoginScene');
// Game/Lobby/Scene/LoginScene.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BenTauHandler_1 = require("./../Network/BenTauHandler");
var IdentityHandler_1 = require("./../Network/IdentityHandler");
var MyAccountHandler_1 = require("./../Network/MyAccountHandler");
var EventBusManager_1 = require("./../Event/EventBusManager");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var LoginGame = /** @class */ (function (_super) {
    __extends(LoginGame, _super);
    function LoginGame() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoginGame.prototype.start = function () {
        EventBusManager_1.default.onRegisterEvent();
    };
    LoginGame.prototype.onClickLogin = function () {
        this.sendLogin("dev111", "123123");
    };
    LoginGame.prototype.sendLogin = function (userName, password) {
        var self = this;
        IdentityHandler_1.default.getInstance().onLogin(userName, password, function (response) {
            cc.log("onLoginSuccess", response);
            MyAccountHandler_1.default.getInstance().onGetAvatarList(function (res) {
                self.getProfile(function () {
                    if (MyAccountHandler_1.default.getInstance().Playah) {
                        BenTauHandler_1.default.getInstance().init(function () {
                            var GLOBAL_DATA_NAME = "GlobalData";
                            var globalNode = cc.director.getScene().getChildByName(GLOBAL_DATA_NAME);
                            var globalComp = globalNode.getComponent(GLOBAL_DATA_NAME);
                            globalComp.setBundleData();
                            cc.director.loadScene("SicboLoadingScene");
                        });
                    }
                });
            });
        }, null);
    };
    LoginGame.prototype.getProfile = function (onSuccess) {
        if (onSuccess === void 0) { onSuccess = null; }
        var self = this;
        MyAccountHandler_1.default.getInstance().getProfile(function (profile) {
            if (!self)
                return;
            if (onSuccess)
                onSuccess();
        });
    };
    LoginGame = __decorate([
        ccclass
    ], LoginGame);
    return LoginGame;
}(cc.Component));
exports.default = LoginGame;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L1NjZW5lL0xvZ2luU2NlbmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYsNERBQXVEO0FBQ3ZELGdFQUEyRDtBQUMzRCxrRUFBNkQ7QUFFN0QsOERBQXlEO0FBQ25ELElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXVDLDZCQUFZO0lBQW5EOztJQXlDQSxDQUFDO0lBeENDLHlCQUFLLEdBQUw7UUFDRSx5QkFBZSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ3BDLENBQUM7SUFFRCxnQ0FBWSxHQUFaO1FBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVPLDZCQUFTLEdBQWpCLFVBQWtCLFFBQWdCLEVBQUUsUUFBZ0I7UUFDbEQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLHlCQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUNuQyxRQUFRLEVBQ1IsUUFBUSxFQUNSLFVBQUMsUUFBUTtZQUNQLEVBQUUsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDbkMsMEJBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsZUFBZSxDQUFDLFVBQUMsR0FBRztnQkFDakQsSUFBSSxDQUFDLFVBQVUsQ0FBQztvQkFDZCxJQUFJLDBCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sRUFBRTt3QkFDekMsdUJBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUM7NEJBQy9CLElBQUksZ0JBQWdCLEdBQVcsWUFBWSxDQUFDOzRCQUM1QyxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOzRCQUN6RSxJQUFJLFVBQVUsR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLENBQUM7NEJBQzNELFVBQVUsQ0FBQyxhQUFhLEVBQUUsQ0FBQzs0QkFDM0IsRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQzt3QkFDN0MsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsRUFDRCxJQUFJLENBQ0wsQ0FBQztJQUNKLENBQUM7SUFFTSw4QkFBVSxHQUFqQixVQUFrQixTQUE0QjtRQUE1QiwwQkFBQSxFQUFBLGdCQUE0QjtRQUM1QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsMEJBQWdCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLFVBQUMsT0FBcUI7WUFDOUQsSUFBSSxDQUFDLElBQUk7Z0JBQUUsT0FBTztZQUNsQixJQUFJLFNBQVM7Z0JBQUUsU0FBUyxFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBeENrQixTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBeUM3QjtJQUFELGdCQUFDO0NBekNELEFBeUNDLENBekNzQyxFQUFFLENBQUMsU0FBUyxHQXlDbEQ7a0JBekNvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBCZW5UYXVIYW5kbGVyIGZyb20gXCIuLy4uL05ldHdvcmsvQmVuVGF1SGFuZGxlclwiO1xuaW1wb3J0IElkZW50aXR5SGFuZGxlciBmcm9tIFwiLi8uLi9OZXR3b3JrL0lkZW50aXR5SGFuZGxlclwiO1xuaW1wb3J0IE15QWNjb3VudEhhbmRsZXIgZnJvbSBcIi4vLi4vTmV0d29yay9NeUFjY291bnRIYW5kbGVyXCI7XG5pbXBvcnQgeyBQcm9maWxlUmVwbHkgfSBmcm9tIFwiQGdhbWVsb290L215YWNjb3VudC9teWFjY291bnRfcGJcIjtcbmltcG9ydCBFdmVudEJ1c01hbmFnZXIgZnJvbSBcIi4vLi4vRXZlbnQvRXZlbnRCdXNNYW5hZ2VyXCI7XG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTG9naW5HYW1lIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgc3RhcnQoKSB7XG4gICAgRXZlbnRCdXNNYW5hZ2VyLm9uUmVnaXN0ZXJFdmVudCgpO1xuICB9XG5cbiAgb25DbGlja0xvZ2luKCkge1xuICAgIHRoaXMuc2VuZExvZ2luKFwiZGV2MTExXCIsIFwiMTIzMTIzXCIpO1xuICB9XG5cbiAgcHJpdmF0ZSBzZW5kTG9naW4odXNlck5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZykge1xuICAgIGxldCBzZWxmID0gdGhpcztcbiAgICBJZGVudGl0eUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5vbkxvZ2luKFxuICAgICAgdXNlck5hbWUsXG4gICAgICBwYXNzd29yZCxcbiAgICAgIChyZXNwb25zZSkgPT4ge1xuICAgICAgICBjYy5sb2coXCJvbkxvZ2luU3VjY2Vzc1wiLCByZXNwb25zZSk7XG4gICAgICAgIE15QWNjb3VudEhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5vbkdldEF2YXRhckxpc3QoKHJlcykgPT4ge1xuICAgICAgICAgIHNlbGYuZ2V0UHJvZmlsZSgoKSA9PiB7XG4gICAgICAgICAgICBpZiAoTXlBY2NvdW50SGFuZGxlci5nZXRJbnN0YW5jZSgpLlBsYXlhaCkge1xuICAgICAgICAgICAgICBCZW5UYXVIYW5kbGVyLmdldEluc3RhbmNlKCkuaW5pdCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgbGV0IEdMT0JBTF9EQVRBX05BTUU6IHN0cmluZyA9IFwiR2xvYmFsRGF0YVwiO1xuICAgICAgICAgICAgICAgIGxldCBnbG9iYWxOb2RlID0gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShHTE9CQUxfREFUQV9OQU1FKTtcbiAgICAgICAgICAgICAgICBsZXQgZ2xvYmFsQ29tcCA9IGdsb2JhbE5vZGUuZ2V0Q29tcG9uZW50KEdMT0JBTF9EQVRBX05BTUUpO1xuICAgICAgICAgICAgICAgIGdsb2JhbENvbXAuc2V0QnVuZGxlRGF0YSgpO1xuICAgICAgICAgICAgICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZShcIlNpY2JvTG9hZGluZ1NjZW5lXCIpO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICB9LFxuICAgICAgbnVsbFxuICAgICk7XG4gIH1cblxuICBwdWJsaWMgZ2V0UHJvZmlsZShvblN1Y2Nlc3M6ICgpID0+IHZvaWQgPSBudWxsKSB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIE15QWNjb3VudEhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5nZXRQcm9maWxlKChwcm9maWxlOiBQcm9maWxlUmVwbHkpID0+IHtcbiAgICAgIGlmICghc2VsZikgcmV0dXJuO1xuICAgICAgaWYgKG9uU3VjY2Vzcykgb25TdWNjZXNzKCk7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==