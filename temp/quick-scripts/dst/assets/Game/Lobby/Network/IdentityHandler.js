
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Network/IdentityHandler.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e77afE0OfJPKIudqWWOfMBP', 'IdentityHandler');
// Game/Lobby/Network/IdentityHandler.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// import UserManager from "../Managers/UserManager";
var empty_pb_1 = require("google-protobuf/google/protobuf/empty_pb");
var MyidServiceClientPb_1 = require("@greyhole/myid/MyidServiceClientPb");
var myid_pb_1 = require("@greyhole/myid/myid_pb");
var myid_code_pb_1 = require("@greyhole/myid/myid_code_pb");
var HandlerClientBase_1 = require("@gameloot/client-base-handler/build/HandlerClientBase");
var UIManager_1 = require("../Manager/UIManager");
var GameDefine_1 = require("../Define/GameDefine");
var TextNotify_1 = require("../Manager/TextNotify");
var ConnectDefine_1 = require("../Define/ConnectDefine");
var EventManager_1 = require("../Event/EventManager");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var WarningPopupSystem_1 = require("../Popup/WarningPopupSystem");
var PortalText_1 = require("../Utils/PortalText");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var IdentityHandler = /** @class */ (function (_super) {
    __extends(IdentityHandler, _super);
    function IdentityHandler() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.onHandleChangePassSucceed = null;
        _this.onHandleLoginSucceed = null;
        _this.onVerifyOTPFalse = null;
        _this.backtoLoginScene = null;
        _this.getNewCaptcha = null;
        _this.closeLoginPopup = null;
        _this.client = null;
        return _this;
    }
    IdentityHandler_1 = IdentityHandler;
    IdentityHandler.getInstance = function () {
        if (!IdentityHandler_1.instance) {
            IdentityHandler_1.instance = new IdentityHandler_1();
            IdentityHandler_1.instance.client = new MyidServiceClientPb_1.MyIDClient(ConnectDefine_1.default.addressSanBay, {}, null);
        }
        return IdentityHandler_1.instance;
    };
    IdentityHandler.destroy = function () {
        IdentityHandler_1.instance = null;
    };
    Object.defineProperty(IdentityHandler.prototype, "metaData", {
        get: function () {
            return {
                Authorization: "Bearer " + IdentityHandler_1.getInstance().token,
            };
        },
        enumerable: false,
        configurable: true
    });
    IdentityHandler.prototype.showLoading = function (callback) {
        UIManager_1.default.getInstance()
            .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, false, null, null)
            .then(function () {
            callback();
        });
    };
    IdentityHandler.prototype.hideLoading = function () {
        UIManager_1.default.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
    };
    IdentityHandler.prototype.onShowErr = function (str, onClick) {
        var code = parseInt(str.split("-")[0]);
        if (!isNaN(code)) {
            var messError = "";
            switch (code) {
                case myid_code_pb_1.Code.INTERNAL:
                case myid_code_pb_1.Code.UNAVAILABLE:
                    messError = PortalText_1.default.POR_MESS_INTERNAL;
                    UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, messError, onClick, onClick);
                    return;
            }
        }
        UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, str, onClick, onClick);
    };
    IdentityHandler.prototype.onDisconnect = function (CodeSocket, messError) {
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, CodeSocket, false, messError);
    };
    IdentityHandler.prototype.onLogin = function (username, password, response, onErr) {
        if (onErr === void 0) { onErr = null; }
        console.log("onLogin " + username + "," + password);
        // this.onHandleLoginSucceed = response;
        var request = new myid_pb_1.SignInV2Request();
        var myId = new myid_pb_1.SignInV2Request.MyID();
        myId.setUsername(username);
        myId.setPassword(password);
        request.setMyId(myId);
        request.setDeviceId("");
        request.setDeviceName("");
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.signInV2(request, self.metaData, function (err, res) {
                if (err && onErr)
                    onErr();
                cc.log("res", res === null || res === void 0 ? void 0 : res.toObject());
                // if (res?.toObject().hasOwnProperty('confirmOtp')) {
                // if (res?.toObject().confirmOtp) {
                //   if (res.getConfirmOtp().getWaiting() > 0) {
                //     let message = GameUtils.FormatString(PortalText.POR_NOTIFY_WARNING_RESET_OTP, res.getConfirmOtp().getWaiting());
                //     UIManager.getInstance().openNotify(TextNotify, GameDefine.TextNotify, new TextNotifyData(message));
                //     self.onSendReply(err, res, msgId);
                //     return;
                //   }
                //   let data = new RequireOTPData();
                //   data.username = username;
                //   data.password = password;
                //   data.requireOTPType = REQUIREOTP.LOGIN;
                //   data.otpId = res.getConfirmOtp().getSuccess().getId();
                //   data.timeRecallOTP = res.getConfirmOtp().getSuccess().getExpiry();
                //   UIManager.getInstance().openPopupWithErrorHandler(InputOTPPopup, "InputOTPPopup", data, UIManager.getInstance().LayerUpPopup, false);
                //   self.onSendReply(err, res, msgId);
                //   return;
                // }
                self.handleResponseLogin(err, res === null || res === void 0 ? void 0 : res.getTokenInfo(), msgId, username);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            if (res === null || res === void 0 ? void 0 : res.toObject().confirmOtp) {
                return;
            }
            response && response(res);
        }, null, true);
    };
    IdentityHandler.prototype.handleResponseLogin = function (err, tokenInfo, msgId, username) {
        cc.log("handleResponseLogin");
        if (err != null) {
            // console.log("err " + err);
        }
        else {
            // Todo in request logic
            if (tokenInfo) {
                this.token = tokenInfo.getAccessToken();
                // UserManager.getInstance().userData.displayName = "";
                // UserManager.getInstance().userData.userId = tokenInfo.getSafeId();
                // UserManager.getInstance().saveInfoLogin(tokenInfo.getIdToken());
                // UserManager.getInstance().setUserName(tokenInfo.getUsername());
                // console.log("AccessToken " + this.token);
                // console.log("IdToken " + tokenInfo.getIdToken());
                // cc.director.loadScene("SplashScene", () => {
                //   MarketPlace.showPromotionPopup = true;
                // });
            }
            else {
                //TODO : fix tạm thời cho trường hợp native vì 'grpcWeb.Error' trên native return null
                var error = {
                    code: 2,
                    metadata: null,
                    name: "",
                    message: "",
                };
                err = error;
            }
        }
        this.onSendReply(err, tokenInfo, msgId);
    };
    IdentityHandler.prototype.getAccessToken = function (idToken, response) {
        var _this = this;
        var request = new myid_pb_1.CreateAccessTokenRequest();
        request.setIdToken(idToken);
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.createAccessToken(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            _this.token = res.getAccessToken();
            cc.log("this.token", _this.token);
            // UserManager.getInstance().saveInfoLogin(idToken);
            response(res);
        }, null, false);
    };
    IdentityHandler.prototype.verifyAccessToken = function (token, response, onError) {
        if (onError === void 0) { onError = null; }
        var request = new empty_pb_1.Empty();
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.me(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, onError, false, false, 5);
    };
    IdentityHandler.prototype.verifyTokenReconnect = function (onSuccess, onError) {
        if (onSuccess === void 0) { onSuccess = null; }
        if (onError === void 0) { onError = null; }
        // cc.log("verifyTokenReconnect token = " + this.token);
        if (this.token != "") {
            this.verifyAccessToken(this.token, function (response) {
                cc.log("verifyTokenReconnect success");
                onSuccess && onSuccess();
            }, function (msgError, code) {
                cc.log("verifyTokenReconnect onFail");
                onError && onError(msgError, code);
            });
        }
    };
    IdentityHandler.prototype.me = function (response) {
        var _this = this;
        var request = new empty_pb_1.Empty();
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.me(request, _this.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, response, null, false);
    };
    IdentityHandler.prototype.handleCustomError = function (errCode, err) {
        if (errCode == myid_code_pb_1.Code.UNKNOWN)
            return;
        if (errCode == myid_code_pb_1.Code.OK)
            return;
        var messError = "";
        UIManager_1.default.getInstance().openNotify(TextNotify_1.default, GameDefine_1.default.TextNotify, new TextNotify_1.TextNotifyData(messError, 2));
    };
    var IdentityHandler_1;
    IdentityHandler.deviceIdKey = "DEVICE_ID";
    IdentityHandler = IdentityHandler_1 = __decorate([
        ccclass
    ], IdentityHandler);
    return IdentityHandler;
}(HandlerClientBase_1.default));
exports.default = IdentityHandler;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L05ldHdvcmsvSWRlbnRpdHlIYW5kbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHFEQUFxRDtBQUNyRCxxRUFBaUU7QUFDakUsMEVBQWdFO0FBRWhFLGtEQUE0STtBQUM1SSw0REFBbUQ7QUFDbkQsMkZBQXNGO0FBRXRGLGtEQUE2QztBQUM3QyxtREFBOEM7QUFDOUMsb0RBQW1FO0FBQ25FLHlEQUFvRDtBQUNwRCxzREFBaUQ7QUFFakQsa0VBQTZEO0FBQzdELGtFQUE2RDtBQUM3RCxrREFBNkM7QUFFdkMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBNkMsbUNBQWlCO0lBQTlEO1FBQUEscUVBOE5DO1FBM05RLCtCQUF5QixHQUFrQixJQUFJLENBQUM7UUFDaEQsMEJBQW9CLEdBQWtCLElBQUksQ0FBQztRQUMzQyxzQkFBZ0IsR0FBZSxJQUFJLENBQUM7UUFDcEMsc0JBQWdCLEdBQWUsSUFBSSxDQUFDO1FBYXBDLG1CQUFhLEdBQWUsSUFBSSxDQUFDO1FBRWpDLHFCQUFlLEdBQWUsSUFBSSxDQUFDO1FBd0NsQyxZQUFNLEdBQWUsSUFBSSxDQUFDOztJQWlLcEMsQ0FBQzt3QkE5Tm9CLGVBQWU7SUFRM0IsMkJBQVcsR0FBbEI7UUFDRSxJQUFJLENBQUMsaUJBQWUsQ0FBQyxRQUFRLEVBQUU7WUFDN0IsaUJBQWUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxpQkFBZSxFQUFFLENBQUM7WUFDakQsaUJBQWUsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksZ0NBQVUsQ0FBQyx1QkFBYSxDQUFDLGFBQWEsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDekY7UUFDRCxPQUFPLGlCQUFlLENBQUMsUUFBUSxDQUFDO0lBQ2xDLENBQUM7SUFFTSx1QkFBTyxHQUFkO1FBQ0UsaUJBQWUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQ2xDLENBQUM7SUFLRCxzQkFBWSxxQ0FBUTthQUFwQjtZQUNFLE9BQU87Z0JBQ0wsYUFBYSxFQUFFLFNBQVMsR0FBRyxpQkFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUs7YUFDL0QsQ0FBQztRQUNKLENBQUM7OztPQUFBO0lBRVMscUNBQVcsR0FBckIsVUFBc0IsUUFBa0I7UUFDdEMsbUJBQVMsQ0FBQyxXQUFXLEVBQUU7YUFDcEIsaUJBQWlCLENBQUMsNEJBQWtCLEVBQUUsb0JBQVUsQ0FBQyxrQkFBa0IsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQzthQUN2RixJQUFJLENBQUM7WUFDSixRQUFRLEVBQUUsQ0FBQztRQUNiLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVTLHFDQUFXLEdBQXJCO1FBQ0UsbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyw0QkFBa0IsRUFBRSxvQkFBVSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDOUYsQ0FBQztJQUVTLG1DQUFTLEdBQW5CLFVBQW9CLEdBQVcsRUFBRSxPQUFvQjtRQUNuRCxJQUFJLElBQUksR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBUyxDQUFDO1FBQy9DLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDaEIsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ25CLFFBQVEsSUFBSSxFQUFFO2dCQUNaLEtBQUssbUJBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ25CLEtBQUssbUJBQUksQ0FBQyxXQUFXO29CQUNuQixTQUFTLEdBQUcsb0JBQVUsQ0FBQyxpQkFBaUIsQ0FBQztvQkFDekMsbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxlQUFlLENBQUMsNEJBQWtCLEVBQUUsb0JBQVUsQ0FBQyxrQkFBa0IsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO29CQUN4SCxPQUFPO2FBQ1Y7U0FDRjtRQUVELG1CQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsZUFBZSxDQUFDLDRCQUFrQixFQUFFLG9CQUFVLENBQUMsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNwSCxDQUFDO0lBRVMsc0NBQVksR0FBdEIsVUFBdUIsVUFBc0IsRUFBRSxTQUFpQjtRQUM5RCxzQkFBWSxDQUFDLElBQUksQ0FBQyxzQkFBWSxDQUFDLGFBQWEsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFJRCxpQ0FBTyxHQUFQLFVBQVEsUUFBZ0IsRUFBRSxRQUFnQixFQUFFLFFBQXlDLEVBQUUsS0FBd0I7UUFBeEIsc0JBQUEsRUFBQSxZQUF3QjtRQUM3RyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxRQUFRLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxDQUFDO1FBQ3BELHdDQUF3QztRQUN4QyxJQUFJLE9BQU8sR0FBRyxJQUFJLHlCQUFlLEVBQUUsQ0FBQztRQUNwQyxJQUFJLElBQUksR0FBRyxJQUFJLHlCQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNCLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEIsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN4QixPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzFCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDL0IsSUFBSSxXQUFXLEdBQUc7WUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBQyxHQUFxQixFQUFFLEdBQWdCO2dCQUNuRixJQUFJLEdBQUcsSUFBSSxLQUFLO29CQUFFLEtBQUssRUFBRSxDQUFDO2dCQUMxQixFQUFFLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsUUFBUSxHQUFHLENBQUM7Z0JBQy9CLHNEQUFzRDtnQkFFdEQsb0NBQW9DO2dCQUNwQyxnREFBZ0Q7Z0JBQ2hELHVIQUF1SDtnQkFDdkgsMEdBQTBHO2dCQUMxRyx5Q0FBeUM7Z0JBQ3pDLGNBQWM7Z0JBQ2QsTUFBTTtnQkFDTixxQ0FBcUM7Z0JBQ3JDLDhCQUE4QjtnQkFDOUIsOEJBQThCO2dCQUM5Qiw0Q0FBNEM7Z0JBQzVDLDJEQUEyRDtnQkFDM0QsdUVBQXVFO2dCQUV2RSwwSUFBMEk7Z0JBQzFJLHVDQUF1QztnQkFDdkMsWUFBWTtnQkFDWixJQUFJO2dCQUNKLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsR0FBRyxhQUFILEdBQUcsdUJBQUgsR0FBRyxDQUFFLFlBQVksSUFBSSxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDdEUsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFDRixJQUFJLENBQUMsYUFBYSxDQUNoQixLQUFLLEVBQ0wsV0FBVyxFQUNYLFVBQUMsR0FBZ0I7WUFDZixJQUFJLEdBQUcsYUFBSCxHQUFHLHVCQUFILEdBQUcsQ0FBRSxRQUFRLEdBQUcsVUFBVSxFQUFFO2dCQUM5QixPQUFPO2FBQ1I7WUFDRCxRQUFRLElBQUksUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFDRCxJQUFJLEVBQ0osSUFBSSxDQUNMLENBQUM7SUFDSixDQUFDO0lBRU8sNkNBQW1CLEdBQTNCLFVBQTRCLEdBQXFCLEVBQUUsU0FBb0IsRUFBRSxLQUFhLEVBQUUsUUFBZ0I7UUFDdEcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzlCLElBQUksR0FBRyxJQUFJLElBQUksRUFBRTtZQUNmLDZCQUE2QjtTQUM5QjthQUFNO1lBQ0wsd0JBQXdCO1lBQ3hCLElBQUksU0FBUyxFQUFFO2dCQUNiLElBQUksQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUN4Qyx1REFBdUQ7Z0JBQ3ZELHFFQUFxRTtnQkFDckUsbUVBQW1FO2dCQUNuRSxrRUFBa0U7Z0JBQ2xFLDRDQUE0QztnQkFDNUMsb0RBQW9EO2dCQUNwRCwrQ0FBK0M7Z0JBQy9DLDJDQUEyQztnQkFDM0MsTUFBTTthQUNQO2lCQUFNO2dCQUNMLHNGQUFzRjtnQkFDdEYsSUFBSSxLQUFLLEdBQXFCO29CQUM1QixJQUFJLEVBQUUsQ0FBQztvQkFDUCxRQUFRLEVBQUUsSUFBSTtvQkFDZCxJQUFJLEVBQUUsRUFBRTtvQkFDUixPQUFPLEVBQUUsRUFBRTtpQkFDWixDQUFDO2dCQUNGLEdBQUcsR0FBRyxLQUFLLENBQUM7YUFDYjtTQUNGO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRCx3Q0FBYyxHQUFkLFVBQWUsT0FBZSxFQUFFLFFBQW9EO1FBQXBGLGlCQXVCQztRQXRCQyxJQUFJLE9BQU8sR0FBRyxJQUFJLGtDQUF3QixFQUFFLENBQUM7UUFDN0MsT0FBTyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUU1QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQy9CLElBQUksV0FBVyxHQUFHO1lBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBQyxHQUFHLEVBQUUsR0FBRztnQkFDN0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLGFBQWEsQ0FDaEIsS0FBSyxFQUNMLFdBQVcsRUFDWCxVQUFDLEdBQTJCO1lBQzFCLEtBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ2xDLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNqQyxvREFBb0Q7WUFDcEQsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hCLENBQUMsRUFDRCxJQUFJLEVBQ0osS0FBSyxDQUNOLENBQUM7SUFDSixDQUFDO0lBRUQsMkNBQWlCLEdBQWpCLFVBQWtCLEtBQWEsRUFBRSxRQUFxQyxFQUFFLE9BQXNEO1FBQXRELHdCQUFBLEVBQUEsY0FBc0Q7UUFDNUgsSUFBSSxPQUFPLEdBQUcsSUFBSSxnQkFBSyxFQUFFLENBQUM7UUFFMUIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMvQixJQUFJLFdBQVcsR0FBRztZQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFHO2dCQUM5QyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFFRCw4Q0FBb0IsR0FBcEIsVUFBcUIsU0FBMEIsRUFBRSxPQUFxRDtRQUFqRiwwQkFBQSxFQUFBLGdCQUEwQjtRQUFFLHdCQUFBLEVBQUEsY0FBcUQ7UUFDcEcsd0RBQXdEO1FBQ3hELElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGlCQUFpQixDQUNwQixJQUFJLENBQUMsS0FBSyxFQUNWLFVBQUMsUUFBUTtnQkFDUCxFQUFFLENBQUMsR0FBRyxDQUFDLDhCQUE4QixDQUFDLENBQUM7Z0JBQ3ZDLFNBQVMsSUFBSSxTQUFTLEVBQUUsQ0FBQztZQUMzQixDQUFDLEVBQ0QsVUFBQyxRQUFnQixFQUFFLElBQVU7Z0JBQzNCLEVBQUUsQ0FBQyxHQUFHLENBQUMsNkJBQTZCLENBQUMsQ0FBQztnQkFDdEMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDckMsQ0FBQyxDQUNGLENBQUM7U0FDSDtJQUNILENBQUM7SUFFRCw0QkFBRSxHQUFGLFVBQUcsUUFBcUM7UUFBeEMsaUJBWUM7UUFYQyxJQUFJLE9BQU8sR0FBRyxJQUFJLGdCQUFLLEVBQUUsQ0FBQztRQUUxQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRS9CLElBQUksV0FBVyxHQUFHO1lBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxLQUFJLENBQUMsUUFBUSxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUc7Z0JBQzlDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNwQyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFRCwyQ0FBaUIsR0FBakIsVUFBa0IsT0FBZSxFQUFFLEdBQVc7UUFDNUMsSUFBSSxPQUFPLElBQUksbUJBQUksQ0FBQyxPQUFPO1lBQUUsT0FBTztRQUNwQyxJQUFJLE9BQU8sSUFBSSxtQkFBSSxDQUFDLEVBQUU7WUFBRSxPQUFPO1FBQy9CLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNuQixtQkFBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxvQkFBVSxFQUFFLG9CQUFVLENBQUMsVUFBVSxFQUFFLElBQUksMkJBQWMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMxRyxDQUFDOztJQTNOdUIsMkJBQVcsR0FBRyxXQUFXLENBQUM7SUFGL0IsZUFBZTtRQURuQyxPQUFPO09BQ2EsZUFBZSxDQThObkM7SUFBRCxzQkFBQztDQTlORCxBQThOQyxDQTlONEMsMkJBQWlCLEdBOE43RDtrQkE5Tm9CLGVBQWUiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpbXBvcnQgVXNlck1hbmFnZXIgZnJvbSBcIi4uL01hbmFnZXJzL1VzZXJNYW5hZ2VyXCI7XHJcbmltcG9ydCB7IEVtcHR5IH0gZnJvbSBcImdvb2dsZS1wcm90b2J1Zi9nb29nbGUvcHJvdG9idWYvZW1wdHlfcGJcIjtcclxuaW1wb3J0IHsgTXlJRENsaWVudCB9IGZyb20gXCJAZ3JleWhvbGUvbXlpZC9NeWlkU2VydmljZUNsaWVudFBiXCI7XHJcbmltcG9ydCAqIGFzIGdycGNXZWIgZnJvbSBcImdycGMtd2ViXCI7XHJcbmltcG9ydCB7IENyZWF0ZUFjY2Vzc1Rva2VuUmVwbHksIENyZWF0ZUFjY2Vzc1Rva2VuUmVxdWVzdCwgTWVSZXBseSwgU2lnbkluUmVwbHksIFNpZ25JblYyUmVxdWVzdCwgVG9rZW5JbmZvIH0gZnJvbSBcIkBncmV5aG9sZS9teWlkL215aWRfcGJcIjtcclxuaW1wb3J0IHsgQ29kZSB9IGZyb20gXCJAZ3JleWhvbGUvbXlpZC9teWlkX2NvZGVfcGJcIjtcclxuaW1wb3J0IEhhbmRsZXJDbGllbnRCYXNlIGZyb20gXCJAZ2FtZWxvb3QvY2xpZW50LWJhc2UtaGFuZGxlci9idWlsZC9IYW5kbGVyQ2xpZW50QmFzZVwiO1xyXG5cclxuaW1wb3J0IFVJTWFuYWdlciBmcm9tIFwiLi4vTWFuYWdlci9VSU1hbmFnZXJcIjtcclxuaW1wb3J0IEdhbWVEZWZpbmUgZnJvbSBcIi4uL0RlZmluZS9HYW1lRGVmaW5lXCI7XHJcbmltcG9ydCBUZXh0Tm90aWZ5LCB7IFRleHROb3RpZnlEYXRhIH0gZnJvbSBcIi4uL01hbmFnZXIvVGV4dE5vdGlmeVwiO1xyXG5pbXBvcnQgQ29ubmVjdERlZmluZSBmcm9tIFwiLi4vRGVmaW5lL0Nvbm5lY3REZWZpbmVcIjtcclxuaW1wb3J0IEV2ZW50TWFuYWdlciBmcm9tIFwiLi4vRXZlbnQvRXZlbnRNYW5hZ2VyXCI7XHJcbmltcG9ydCB7IENvZGVTb2NrZXQgfSBmcm9tIFwiLi4vVXRpbHMvRW51bUdhbWVcIjtcclxuaW1wb3J0IExvYWRpbmdQb3B1cFN5c3RlbSBmcm9tIFwiLi4vUG9wdXAvTG9hZGluZ1BvcHVwU3lzdGVtXCI7XHJcbmltcG9ydCBXYXJuaW5nUG9wdXBTeXN0ZW0gZnJvbSBcIi4uL1BvcHVwL1dhcm5pbmdQb3B1cFN5c3RlbVwiO1xyXG5pbXBvcnQgUG9ydGFsVGV4dCBmcm9tIFwiLi4vVXRpbHMvUG9ydGFsVGV4dFwiO1xyXG5cclxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIElkZW50aXR5SGFuZGxlciBleHRlbmRzIEhhbmRsZXJDbGllbnRCYXNlIHtcclxuICBwcm90ZWN0ZWQgc3RhdGljIGluc3RhbmNlOiBJZGVudGl0eUhhbmRsZXI7XHJcbiAgcHJpdmF0ZSBzdGF0aWMgcmVhZG9ubHkgZGV2aWNlSWRLZXkgPSBcIkRFVklDRV9JRFwiO1xyXG4gIHB1YmxpYyBvbkhhbmRsZUNoYW5nZVBhc3NTdWNjZWVkOiAocmVzKSA9PiB2b2lkID0gbnVsbDtcclxuICBwdWJsaWMgb25IYW5kbGVMb2dpblN1Y2NlZWQ6IChyZXMpID0+IHZvaWQgPSBudWxsO1xyXG4gIHB1YmxpYyBvblZlcmlmeU9UUEZhbHNlOiAoKSA9PiB2b2lkID0gbnVsbDtcclxuICBwdWJsaWMgYmFja3RvTG9naW5TY2VuZTogKCkgPT4gdm9pZCA9IG51bGw7XHJcblxyXG4gIHN0YXRpYyBnZXRJbnN0YW5jZSgpOiBJZGVudGl0eUhhbmRsZXIge1xyXG4gICAgaWYgKCFJZGVudGl0eUhhbmRsZXIuaW5zdGFuY2UpIHtcclxuICAgICAgSWRlbnRpdHlIYW5kbGVyLmluc3RhbmNlID0gbmV3IElkZW50aXR5SGFuZGxlcigpO1xyXG4gICAgICBJZGVudGl0eUhhbmRsZXIuaW5zdGFuY2UuY2xpZW50ID0gbmV3IE15SURDbGllbnQoQ29ubmVjdERlZmluZS5hZGRyZXNzU2FuQmF5LCB7fSwgbnVsbCk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gSWRlbnRpdHlIYW5kbGVyLmluc3RhbmNlO1xyXG4gIH1cclxuXHJcbiAgc3RhdGljIGRlc3Ryb3koKSB7XHJcbiAgICBJZGVudGl0eUhhbmRsZXIuaW5zdGFuY2UgPSBudWxsO1xyXG4gIH1cclxuICBwdWJsaWMgZ2V0TmV3Q2FwdGNoYTogKCkgPT4gdm9pZCA9IG51bGw7XHJcblxyXG4gIHB1YmxpYyBjbG9zZUxvZ2luUG9wdXA6ICgpID0+IHZvaWQgPSBudWxsO1xyXG4gIHB1YmxpYyB0b2tlbjogc3RyaW5nO1xyXG4gIHByaXZhdGUgZ2V0IG1ldGFEYXRhKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgQXV0aG9yaXphdGlvbjogXCJCZWFyZXIgXCIgKyBJZGVudGl0eUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS50b2tlbixcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgc2hvd0xvYWRpbmcoY2FsbGJhY2s6IEZ1bmN0aW9uKSB7XHJcbiAgICBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKVxyXG4gICAgICAub3BlblBvcHVwU3lzdGVtVjIoTG9hZGluZ1BvcHVwU3lzdGVtLCBHYW1lRGVmaW5lLkxvYWRpbmdQb3B1cFN5c3RlbSwgZmFsc2UsIG51bGwsIG51bGwpXHJcbiAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICBjYWxsYmFjaygpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBoaWRlTG9hZGluZygpIHtcclxuICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpLmNsb3NlUG9wdXBTeXN0ZW0oTG9hZGluZ1BvcHVwU3lzdGVtLCBHYW1lRGVmaW5lLkxvYWRpbmdQb3B1cFN5c3RlbSk7XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgb25TaG93RXJyKHN0cjogc3RyaW5nLCBvbkNsaWNrPzogKCkgPT4gdm9pZCkge1xyXG4gICAgbGV0IGNvZGUgPSBwYXJzZUludChzdHIuc3BsaXQoXCItXCIpWzBdKSBhcyBDb2RlO1xyXG4gICAgaWYgKCFpc05hTihjb2RlKSkge1xyXG4gICAgICBsZXQgbWVzc0Vycm9yID0gXCJcIjtcclxuICAgICAgc3dpdGNoIChjb2RlKSB7XHJcbiAgICAgICAgY2FzZSBDb2RlLklOVEVSTkFMOlxyXG4gICAgICAgIGNhc2UgQ29kZS5VTkFWQUlMQUJMRTpcclxuICAgICAgICAgIG1lc3NFcnJvciA9IFBvcnRhbFRleHQuUE9SX01FU1NfSU5URVJOQUw7XHJcbiAgICAgICAgICBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5vcGVuUG9wdXBTeXN0ZW0oV2FybmluZ1BvcHVwU3lzdGVtLCBHYW1lRGVmaW5lLldhcm5pbmdQb3B1cFN5c3RlbSwgbWVzc0Vycm9yLCBvbkNsaWNrLCBvbkNsaWNrKTtcclxuICAgICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpLm9wZW5Qb3B1cFN5c3RlbShXYXJuaW5nUG9wdXBTeXN0ZW0sIEdhbWVEZWZpbmUuV2FybmluZ1BvcHVwU3lzdGVtLCBzdHIsIG9uQ2xpY2ssIG9uQ2xpY2spO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIG9uRGlzY29ubmVjdChDb2RlU29ja2V0OiBDb2RlU29ja2V0LCBtZXNzRXJyb3I6IHN0cmluZykge1xyXG4gICAgRXZlbnRNYW5hZ2VyLmZpcmUoRXZlbnRNYW5hZ2VyLk9OX0RJU0NPTk5FQ1QsIENvZGVTb2NrZXQsIGZhbHNlLCBtZXNzRXJyb3IpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjbGllbnQ6IE15SURDbGllbnQgPSBudWxsO1xyXG5cclxuICBvbkxvZ2luKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcsIHJlc3BvbnNlOiAocmVzcG9uc2U6IFNpZ25JblJlcGx5KSA9PiB2b2lkLCBvbkVycjogKCkgPT4gdm9pZCA9IG51bGwpIHtcclxuICAgIGNvbnNvbGUubG9nKFwib25Mb2dpbiBcIiArIHVzZXJuYW1lICsgXCIsXCIgKyBwYXNzd29yZCk7XHJcbiAgICAvLyB0aGlzLm9uSGFuZGxlTG9naW5TdWNjZWVkID0gcmVzcG9uc2U7XHJcbiAgICBsZXQgcmVxdWVzdCA9IG5ldyBTaWduSW5WMlJlcXVlc3QoKTtcclxuICAgIGxldCBteUlkID0gbmV3IFNpZ25JblYyUmVxdWVzdC5NeUlEKCk7XHJcbiAgICBteUlkLnNldFVzZXJuYW1lKHVzZXJuYW1lKTtcclxuICAgIG15SWQuc2V0UGFzc3dvcmQocGFzc3dvcmQpO1xyXG4gICAgcmVxdWVzdC5zZXRNeUlkKG15SWQpO1xyXG4gICAgcmVxdWVzdC5zZXREZXZpY2VJZChcIlwiKTtcclxuICAgIHJlcXVlc3Quc2V0RGV2aWNlTmFtZShcIlwiKTtcclxuICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgIHZhciBtc2dJZCA9IHNlbGYuZ2V0VW5pcXVlSWQoKTtcclxuICAgIGxldCBzZW5kUmVxdWVzdCA9ICgpID0+IHtcclxuICAgICAgc2VsZi5jbGllbnQuc2lnbkluVjIocmVxdWVzdCwgc2VsZi5tZXRhRGF0YSwgKGVycjogZ3JwY1dlYi5ScGNFcnJvciwgcmVzOiBTaWduSW5SZXBseSkgPT4ge1xyXG4gICAgICAgIGlmIChlcnIgJiYgb25FcnIpIG9uRXJyKCk7XHJcbiAgICAgICAgY2MubG9nKFwicmVzXCIsIHJlcz8udG9PYmplY3QoKSk7XHJcbiAgICAgICAgLy8gaWYgKHJlcz8udG9PYmplY3QoKS5oYXNPd25Qcm9wZXJ0eSgnY29uZmlybU90cCcpKSB7XHJcblxyXG4gICAgICAgIC8vIGlmIChyZXM/LnRvT2JqZWN0KCkuY29uZmlybU90cCkge1xyXG4gICAgICAgIC8vICAgaWYgKHJlcy5nZXRDb25maXJtT3RwKCkuZ2V0V2FpdGluZygpID4gMCkge1xyXG4gICAgICAgIC8vICAgICBsZXQgbWVzc2FnZSA9IEdhbWVVdGlscy5Gb3JtYXRTdHJpbmcoUG9ydGFsVGV4dC5QT1JfTk9USUZZX1dBUk5JTkdfUkVTRVRfT1RQLCByZXMuZ2V0Q29uZmlybU90cCgpLmdldFdhaXRpbmcoKSk7XHJcbiAgICAgICAgLy8gICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpLm9wZW5Ob3RpZnkoVGV4dE5vdGlmeSwgR2FtZURlZmluZS5UZXh0Tm90aWZ5LCBuZXcgVGV4dE5vdGlmeURhdGEobWVzc2FnZSkpO1xyXG4gICAgICAgIC8vICAgICBzZWxmLm9uU2VuZFJlcGx5KGVyciwgcmVzLCBtc2dJZCk7XHJcbiAgICAgICAgLy8gICAgIHJldHVybjtcclxuICAgICAgICAvLyAgIH1cclxuICAgICAgICAvLyAgIGxldCBkYXRhID0gbmV3IFJlcXVpcmVPVFBEYXRhKCk7XHJcbiAgICAgICAgLy8gICBkYXRhLnVzZXJuYW1lID0gdXNlcm5hbWU7XHJcbiAgICAgICAgLy8gICBkYXRhLnBhc3N3b3JkID0gcGFzc3dvcmQ7XHJcbiAgICAgICAgLy8gICBkYXRhLnJlcXVpcmVPVFBUeXBlID0gUkVRVUlSRU9UUC5MT0dJTjtcclxuICAgICAgICAvLyAgIGRhdGEub3RwSWQgPSByZXMuZ2V0Q29uZmlybU90cCgpLmdldFN1Y2Nlc3MoKS5nZXRJZCgpO1xyXG4gICAgICAgIC8vICAgZGF0YS50aW1lUmVjYWxsT1RQID0gcmVzLmdldENvbmZpcm1PdHAoKS5nZXRTdWNjZXNzKCkuZ2V0RXhwaXJ5KCk7XHJcblxyXG4gICAgICAgIC8vICAgVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkub3BlblBvcHVwV2l0aEVycm9ySGFuZGxlcihJbnB1dE9UUFBvcHVwLCBcIklucHV0T1RQUG9wdXBcIiwgZGF0YSwgVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkuTGF5ZXJVcFBvcHVwLCBmYWxzZSk7XHJcbiAgICAgICAgLy8gICBzZWxmLm9uU2VuZFJlcGx5KGVyciwgcmVzLCBtc2dJZCk7XHJcbiAgICAgICAgLy8gICByZXR1cm47XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIHNlbGYuaGFuZGxlUmVzcG9uc2VMb2dpbihlcnIsIHJlcz8uZ2V0VG9rZW5JbmZvKCksIG1zZ0lkLCB1c2VybmFtZSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuICAgIHRoaXMub25TZW5kUmVxdWVzdChcclxuICAgICAgbXNnSWQsXHJcbiAgICAgIHNlbmRSZXF1ZXN0LFxyXG4gICAgICAocmVzOiBTaWduSW5SZXBseSkgPT4ge1xyXG4gICAgICAgIGlmIChyZXM/LnRvT2JqZWN0KCkuY29uZmlybU90cCkge1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXNwb25zZSAmJiByZXNwb25zZShyZXMpO1xyXG4gICAgICB9LFxyXG4gICAgICBudWxsLFxyXG4gICAgICB0cnVlXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBoYW5kbGVSZXNwb25zZUxvZ2luKGVycjogZ3JwY1dlYi5ScGNFcnJvciwgdG9rZW5JbmZvOiBUb2tlbkluZm8sIG1zZ0lkOiBzdHJpbmcsIHVzZXJuYW1lOiBzdHJpbmcpIHtcclxuICAgIGNjLmxvZyhcImhhbmRsZVJlc3BvbnNlTG9naW5cIik7XHJcbiAgICBpZiAoZXJyICE9IG51bGwpIHtcclxuICAgICAgLy8gY29uc29sZS5sb2coXCJlcnIgXCIgKyBlcnIpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gVG9kbyBpbiByZXF1ZXN0IGxvZ2ljXHJcbiAgICAgIGlmICh0b2tlbkluZm8pIHtcclxuICAgICAgICB0aGlzLnRva2VuID0gdG9rZW5JbmZvLmdldEFjY2Vzc1Rva2VuKCk7XHJcbiAgICAgICAgLy8gVXNlck1hbmFnZXIuZ2V0SW5zdGFuY2UoKS51c2VyRGF0YS5kaXNwbGF5TmFtZSA9IFwiXCI7XHJcbiAgICAgICAgLy8gVXNlck1hbmFnZXIuZ2V0SW5zdGFuY2UoKS51c2VyRGF0YS51c2VySWQgPSB0b2tlbkluZm8uZ2V0U2FmZUlkKCk7XHJcbiAgICAgICAgLy8gVXNlck1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5zYXZlSW5mb0xvZ2luKHRva2VuSW5mby5nZXRJZFRva2VuKCkpO1xyXG4gICAgICAgIC8vIFVzZXJNYW5hZ2VyLmdldEluc3RhbmNlKCkuc2V0VXNlck5hbWUodG9rZW5JbmZvLmdldFVzZXJuYW1lKCkpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKFwiQWNjZXNzVG9rZW4gXCIgKyB0aGlzLnRva2VuKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhcIklkVG9rZW4gXCIgKyB0b2tlbkluZm8uZ2V0SWRUb2tlbigpKTtcclxuICAgICAgICAvLyBjYy5kaXJlY3Rvci5sb2FkU2NlbmUoXCJTcGxhc2hTY2VuZVwiLCAoKSA9PiB7XHJcbiAgICAgICAgLy8gICBNYXJrZXRQbGFjZS5zaG93UHJvbW90aW9uUG9wdXAgPSB0cnVlO1xyXG4gICAgICAgIC8vIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIC8vVE9ETyA6IGZpeCB04bqhbSB0aOG7nWkgY2hvIHRyxrDhu51uZyBo4bujcCBuYXRpdmUgdsOsICdncnBjV2ViLkVycm9yJyB0csOqbiBuYXRpdmUgcmV0dXJuIG51bGxcclxuICAgICAgICBsZXQgZXJyb3I6IGdycGNXZWIuUnBjRXJyb3IgPSB7XHJcbiAgICAgICAgICBjb2RlOiAyLFxyXG4gICAgICAgICAgbWV0YWRhdGE6IG51bGwsXHJcbiAgICAgICAgICBuYW1lOiBcIlwiLFxyXG4gICAgICAgICAgbWVzc2FnZTogXCJcIixcclxuICAgICAgICB9O1xyXG4gICAgICAgIGVyciA9IGVycm9yO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICB0aGlzLm9uU2VuZFJlcGx5KGVyciwgdG9rZW5JbmZvLCBtc2dJZCk7XHJcbiAgfVxyXG5cclxuICBnZXRBY2Nlc3NUb2tlbihpZFRva2VuOiBzdHJpbmcsIHJlc3BvbnNlOiAocmVzcG9uc2U6IENyZWF0ZUFjY2Vzc1Rva2VuUmVwbHkpID0+IHZvaWQpIHtcclxuICAgIGxldCByZXF1ZXN0ID0gbmV3IENyZWF0ZUFjY2Vzc1Rva2VuUmVxdWVzdCgpO1xyXG4gICAgcmVxdWVzdC5zZXRJZFRva2VuKGlkVG9rZW4pO1xyXG5cclxuICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgIHZhciBtc2dJZCA9IHNlbGYuZ2V0VW5pcXVlSWQoKTtcclxuICAgIGxldCBzZW5kUmVxdWVzdCA9ICgpID0+IHtcclxuICAgICAgc2VsZi5jbGllbnQuY3JlYXRlQWNjZXNzVG9rZW4ocmVxdWVzdCwgc2VsZi5tZXRhRGF0YSwgKGVyciwgcmVzKSA9PiB7XHJcbiAgICAgICAgc2VsZi5vblNlbmRSZXBseShlcnIsIHJlcywgbXNnSWQpO1xyXG4gICAgICB9KTtcclxuICAgIH07XHJcbiAgICB0aGlzLm9uU2VuZFJlcXVlc3QoXHJcbiAgICAgIG1zZ0lkLFxyXG4gICAgICBzZW5kUmVxdWVzdCxcclxuICAgICAgKHJlczogQ3JlYXRlQWNjZXNzVG9rZW5SZXBseSkgPT4ge1xyXG4gICAgICAgIHRoaXMudG9rZW4gPSByZXMuZ2V0QWNjZXNzVG9rZW4oKTtcclxuICAgICAgICBjYy5sb2coXCJ0aGlzLnRva2VuXCIsIHRoaXMudG9rZW4pO1xyXG4gICAgICAgIC8vIFVzZXJNYW5hZ2VyLmdldEluc3RhbmNlKCkuc2F2ZUluZm9Mb2dpbihpZFRva2VuKTtcclxuICAgICAgICByZXNwb25zZShyZXMpO1xyXG4gICAgICB9LFxyXG4gICAgICBudWxsLFxyXG4gICAgICBmYWxzZVxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHZlcmlmeUFjY2Vzc1Rva2VuKHRva2VuOiBzdHJpbmcsIHJlc3BvbnNlOiAocmVzcG9uc2U6IE1lUmVwbHkpID0+IHZvaWQsIG9uRXJyb3I6IChtc2dFcnJvcjogc3RyaW5nLCBjb2RlOiBDb2RlKSA9PiB2b2lkID0gbnVsbCkge1xyXG4gICAgbGV0IHJlcXVlc3QgPSBuZXcgRW1wdHkoKTtcclxuXHJcbiAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICB2YXIgbXNnSWQgPSBzZWxmLmdldFVuaXF1ZUlkKCk7XHJcbiAgICBsZXQgc2VuZFJlcXVlc3QgPSAoKSA9PiB7XHJcbiAgICAgIHNlbGYuY2xpZW50Lm1lKHJlcXVlc3QsIHNlbGYubWV0YURhdGEsIChlcnIsIHJlcykgPT4ge1xyXG4gICAgICAgIHNlbGYub25TZW5kUmVwbHkoZXJyLCByZXMsIG1zZ0lkKTtcclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG4gICAgdGhpcy5vblNlbmRSZXF1ZXN0KG1zZ0lkLCBzZW5kUmVxdWVzdCwgcmVzcG9uc2UsIG9uRXJyb3IsIGZhbHNlLCBmYWxzZSwgNSk7XHJcbiAgfVxyXG5cclxuICB2ZXJpZnlUb2tlblJlY29ubmVjdChvblN1Y2Nlc3M6IEZ1bmN0aW9uID0gbnVsbCwgb25FcnJvcjogKG1zZ0Vycm9yOiBzdHJpbmcsIGNvZGU6IGFueSkgPT4gdm9pZCA9IG51bGwpIHtcclxuICAgIC8vIGNjLmxvZyhcInZlcmlmeVRva2VuUmVjb25uZWN0IHRva2VuID0gXCIgKyB0aGlzLnRva2VuKTtcclxuICAgIGlmICh0aGlzLnRva2VuICE9IFwiXCIpIHtcclxuICAgICAgdGhpcy52ZXJpZnlBY2Nlc3NUb2tlbihcclxuICAgICAgICB0aGlzLnRva2VuLFxyXG4gICAgICAgIChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgY2MubG9nKFwidmVyaWZ5VG9rZW5SZWNvbm5lY3Qgc3VjY2Vzc1wiKTtcclxuICAgICAgICAgIG9uU3VjY2VzcyAmJiBvblN1Y2Nlc3MoKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIChtc2dFcnJvcjogc3RyaW5nLCBjb2RlOiBDb2RlKSA9PiB7XHJcbiAgICAgICAgICBjYy5sb2coXCJ2ZXJpZnlUb2tlblJlY29ubmVjdCBvbkZhaWxcIik7XHJcbiAgICAgICAgICBvbkVycm9yICYmIG9uRXJyb3IobXNnRXJyb3IsIGNvZGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG1lKHJlc3BvbnNlOiAocmVzcG9uc2U6IE1lUmVwbHkpID0+IHZvaWQpIHtcclxuICAgIGxldCByZXF1ZXN0ID0gbmV3IEVtcHR5KCk7XHJcblxyXG4gICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgdmFyIG1zZ0lkID0gc2VsZi5nZXRVbmlxdWVJZCgpO1xyXG5cclxuICAgIGxldCBzZW5kUmVxdWVzdCA9ICgpID0+IHtcclxuICAgICAgc2VsZi5jbGllbnQubWUocmVxdWVzdCwgdGhpcy5tZXRhRGF0YSwgKGVyciwgcmVzKSA9PiB7XHJcbiAgICAgICAgc2VsZi5vblNlbmRSZXBseShlcnIsIHJlcywgbXNnSWQpO1xyXG4gICAgICB9KTtcclxuICAgIH07XHJcbiAgICB0aGlzLm9uU2VuZFJlcXVlc3QobXNnSWQsIHNlbmRSZXF1ZXN0LCByZXNwb25zZSwgbnVsbCwgZmFsc2UpO1xyXG4gIH1cclxuXHJcbiAgaGFuZGxlQ3VzdG9tRXJyb3IoZXJyQ29kZTogbnVtYmVyLCBlcnI6IHN0cmluZykge1xyXG4gICAgaWYgKGVyckNvZGUgPT0gQ29kZS5VTktOT1dOKSByZXR1cm47XHJcbiAgICBpZiAoZXJyQ29kZSA9PSBDb2RlLk9LKSByZXR1cm47XHJcbiAgICBsZXQgbWVzc0Vycm9yID0gXCJcIjtcclxuICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpLm9wZW5Ob3RpZnkoVGV4dE5vdGlmeSwgR2FtZURlZmluZS5UZXh0Tm90aWZ5LCBuZXcgVGV4dE5vdGlmeURhdGEobWVzc0Vycm9yLCAyKSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==