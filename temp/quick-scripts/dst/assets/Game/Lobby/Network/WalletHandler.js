
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Network/WalletHandler.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9ec8dOswhxGea0zXXZbZNG5', 'WalletHandler');
// Game/Lobby/Network/WalletHandler.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var client_base_handler_1 = require("@gameloot/client-base-handler");
var WalletHandler = /** @class */ (function (_super) {
    __extends(WalletHandler, _super);
    function WalletHandler() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.client = null;
        _this.onReceiveSendEvent = null;
        _this.onReceiveJoinEvent = null;
        _this.onReceiveLeaveEvent = null;
        _this.onHandleWithDrawFromVaultErr = null;
        _this.onHandleWithDrawFromVaultSucceed = null;
        _this.onVerifyOTPFalse = null;
        _this.gamePlayings = new Map();
        _this.listTransactionID = [];
        _this.max = 50;
        _this._currentBalance = 0;
        _this._currentVaultBalance = 0;
        _this._currentMainBalance = 0;
        _this._currentPromotionBalance = 0;
        _this._currentTransaction = 0;
        return _this;
    }
    Object.defineProperty(WalletHandler.prototype, "currentBalance", {
        get: function () {
            return this._currentBalance;
        },
        enumerable: false,
        configurable: true
    });
    WalletHandler.prototype.setCurrentMainBalance = function (balance) {
        this._currentMainBalance = balance;
    };
    Object.defineProperty(WalletHandler.prototype, "CurrentMainBalance", {
        get: function () {
            return this._currentMainBalance;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WalletHandler.prototype, "CurrentPromotionBalance", {
        get: function () {
            return this._currentPromotionBalance;
        },
        enumerable: false,
        configurable: true
    });
    WalletHandler.prototype.setCurrentPromotionBalance = function (balance) {
        this._currentPromotionBalance = balance;
    };
    WalletHandler.prototype.setCurrentVaultBalance = function (balance) {
        this._currentVaultBalance = balance;
    };
    Object.defineProperty(WalletHandler.prototype, "currentVaultBalance", {
        get: function () {
            return this._currentVaultBalance;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WalletHandler.prototype, "currentTransaction", {
        get: function () {
            return this._currentTransaction;
        },
        enumerable: false,
        configurable: true
    });
    WalletHandler.getInstance = function () {
        if (!WalletHandler.instance) {
            // cc.log("new getInstance(): WalletHandler");
            WalletHandler.instance = new WalletHandler(BenTauHandler_1.default.getInstance());
            WalletHandler.instance.client = new MywalletServiceClientPb_1.MyWalletClient(ConnectDefine_1.default.addressSanBay, {}, null);
            WalletHandler.instance.topic = topic_pb_1.Topic.MY_WALLET;
            WalletHandler.instance.onSubscribe();
        }
        return WalletHandler.instance;
    };
    WalletHandler.destroy = function () {
        if (WalletHandler.instance)
            WalletHandler.instance.onUnSubscribe();
        WalletHandler.instance = null;
    };
    Object.defineProperty(WalletHandler.prototype, "metaData", {
        get: function () {
            return {
                Authorization: "Bearer " + IdentityHandler_1.default.getInstance().token,
            };
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WalletHandler.prototype, "metaDataWithOTP", {
        get: function () {
            var _a;
            return _a = {
                    Authorization: "Bearer " + IdentityHandler_1.default.getInstance().token
                },
                _a["x-otp-id"] = this._xOTPId,
                _a["x-otp"] = this.OTPId,
                _a;
        },
        enumerable: false,
        configurable: true
    });
    WalletHandler.prototype.setOTPID = function (id) {
        this.OTPId = id;
    };
    WalletHandler.prototype.showLoading = function (callback) {
        UIManager_1.default.getInstance()
            .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, false, null, null)
            .then(function () {
            callback();
        });
    };
    WalletHandler.prototype.hideLoading = function () {
        UIManager_1.default.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
    };
    WalletHandler.prototype.onShowErr = function (str, onClick) {
        UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, str, onClick, onClick);
    };
    WalletHandler.prototype.onDisconnect = function (CodeSocket, messError) {
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, CodeSocket, false, messError);
    };
    WalletHandler.prototype.onMyWalletRequest = function (response) {
        var _this = this;
        if (response === void 0) { response = null; }
        var request = new google_protobuf_empty_pb.Empty();
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.me(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            var walletAmount = res.getWallet().getCurrentBalance();
            var vaultWalletAmount = res.getWallet().getBalanceInVault();
            var mainBalance = res.getWallet().getCurrentMainBalance();
            var promotionBalance = res.getWallet().getCurrentPromotionBalance();
            var transaction = res.getLastTxId();
            _this.setCurrentBalance(walletAmount, transaction);
            _this.setCurrentVaultBalance(vaultWalletAmount);
            _this.setCurrentMainBalance(mainBalance);
            _this.setCurrentPromotionBalance(promotionBalance);
            response(res);
        }, null, false);
    };
    WalletHandler.prototype.putToVault = function (amount, response) {
        var _this = this;
        if (response === void 0) { response = null; }
        var request = new mywallet_pb_1.PutToVaultRequest();
        request.setAmount(amount);
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.putToVault(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            var walletAmount = res.getWallet().getCurrentBalance();
            var vaultWalletAmount = res.getWallet().getBalanceInVault();
            var mainBalance = res.getWallet().getCurrentMainBalance();
            var promotionBalance = res.getWallet().getCurrentPromotionBalance();
            var transaction = res.getChange().getLastTxId();
            _this.setCurrentBalance(walletAmount, transaction);
            _this.setCurrentVaultBalance(vaultWalletAmount);
            _this.setCurrentMainBalance(mainBalance);
            _this.setCurrentPromotionBalance(promotionBalance);
            response(res);
        }, null, false);
    };
    WalletHandler.prototype.onReceive = function (msg) {
        _super.prototype.onReceive.call(this, msg);
        var change = mywallet_change_pb_1.Change.deserializeBinary(msg.getPayload_asU8());
        var topicReceive = change.getTopic();
        //console.log("wallet topic " + topicReceive + " amount change " + change.getAmount() + " amount new " + change.getNewBalance() + " " + change.getHappenedAt());
        if (!this.gamePlayings.has(topicReceive)) {
            this.addMoney(change.getAmount(), change.getLastTxId());
        }
    };
    WalletHandler.prototype.onEnableGame = function (topic) {
        if (!this.gamePlayings.has(topic)) {
            //cc.log("Wallet onEnableGame ............." + topic);
            this.gamePlayings.set(topic, 0);
        }
    };
    WalletHandler.prototype.onDisableGame = function (topic) {
        if (this.gamePlayings.has(topic)) {
            //cc.log("Wallet onDisableGame ............." + topic);
            this.gamePlayings.delete(topic);
        }
        if (this.gamePlayings.size == 0) {
            this.onMyWalletRequest(function () { });
        }
    };
    WalletHandler.prototype.onReConnect = function (onComplete) {
        if (onComplete === void 0) { onComplete = null; }
        //BenTauWSHandler.getInstance().onReConnect();
    };
    WalletHandler.prototype.setCurrentBalance = function (value, transaction) {
        //cc.log("Update setCurrentBalance " + value + " " + transaction);
        this._currentBalance = value;
        this._currentTransaction = transaction;
        EventManager_1.default.fire(EventManager_1.default.onUpdateBalance, this._currentBalance);
        EventBusManager_1.default.onFireUpdateBalance(this._currentBalance);
    };
    //TODO lay balance theo revision
    WalletHandler.prototype.getLatestBalance = function (callback) {
        if (callback === void 0) { callback = null; }
        // cc.log("callback 1 " + (callback != null))
        this.onMyWalletRequest(function (resp) {
            // cc.log("callback " + (callback != null))
            if (callback) {
                callback(resp);
            }
        });
    };
    WalletHandler.prototype.addMoney = function (amount, transaction) {
        var allowAddMoney = true;
        //cc.log("wallet " + amount + " " + transaction);
        this.listTransactionID.forEach(function (saveID) {
            if (saveID == transaction) {
                allowAddMoney = false;
                return;
            }
        });
        if (allowAddMoney) {
            if (transaction > 0) {
                // cheat ==0 for FISH temp
                if (transaction <= this._currentTransaction)
                    return;
                this.listTransactionID.push(transaction);
            }
            this._currentBalance += amount;
            EventManager_1.default.fire(EventManager_1.default.onUpdateAddBalance, amount);
            EventBusManager_1.default.onFireAddBalance(amount, this._currentBalance);
            if (this.listTransactionID.length > this.max) {
                this.listTransactionID.shift();
            }
        }
    };
    WalletHandler.prototype.handleCustomError = function (errCode, err) {
        //cc.log("handleCustomError " + errCode);
        if (errCode == mywallet_code_pb_1.Code.OK)
            return;
        if (errCode == mywallet_code_pb_1.Code.UNKNOWN)
            return;
        var messError = "";
        switch (errCode) {
            case mywallet_code_pb_1.Code.INSUFFICIENT_BALANCE:
                messError = PortalText_1.default.POR_MESS_INSUFFICIENT_BALANCE;
                break;
            default:
                messError = errCode + " - " + err;
                break;
        }
        UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, messError, function () { });
    };
    return WalletHandler;
}(client_base_handler_1.HandlerClientBenTauBase));
exports.default = WalletHandler;
var ConnectDefine_1 = require("../Define/ConnectDefine");
var IdentityHandler_1 = require("./IdentityHandler");
var mywallet_pb_1 = require("@gameloot/mywallet/mywallet_pb");
var MywalletServiceClientPb_1 = require("@gameloot/mywallet/MywalletServiceClientPb");
var EventManager_1 = require("../Event/EventManager");
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var mywallet_code_pb_1 = require("@gameloot/mywallet/mywallet_code_pb");
var UIManager_1 = require("../Manager/UIManager");
var WarningPopupSystem_1 = require("../Popup/WarningPopupSystem");
var BenTauHandler_1 = require("./BenTauHandler");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var GameDefine_1 = require("../Define/GameDefine");
var EventBusManager_1 = require("../Event/EventBusManager");
var PortalText_1 = require("../Utils/PortalText");
var mywallet_change_pb_1 = require("@gameloot/mywallet/mywallet_change_pb");

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L05ldHdvcmsvV2FsbGV0SGFuZGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxxRUFBd0U7QUFFeEU7SUFBMkMsaUNBQXVCO0lBQWxFO1FBQUEscUVBc1FDO1FBblFTLFlBQU0sR0FBbUIsSUFBSSxDQUFDO1FBRS9CLHdCQUFrQixHQUFrRCxJQUFJLENBQUM7UUFFekUsd0JBQWtCLEdBQXlCLElBQUksQ0FBQztRQUVoRCx5QkFBbUIsR0FBeUIsSUFBSSxDQUFDO1FBRWpELGtDQUE0QixHQUFlLElBQUksQ0FBQztRQUVoRCxzQ0FBZ0MsR0FBa0IsSUFBSSxDQUFDO1FBRXZELHNCQUFnQixHQUFlLElBQUksQ0FBQztRQUVuQyxrQkFBWSxHQUF1QixJQUFJLEdBQUcsRUFBaUIsQ0FBQztRQUU1RCx1QkFBaUIsR0FBYSxFQUFFLENBQUM7UUFDakMsU0FBRyxHQUFXLEVBQUUsQ0FBQztRQUVqQixxQkFBZSxHQUFXLENBQUMsQ0FBQztRQUM1QiwwQkFBb0IsR0FBVyxDQUFDLENBQUM7UUFDakMseUJBQW1CLEdBQVcsQ0FBQyxDQUFDO1FBQ2hDLDhCQUF3QixHQUFXLENBQUMsQ0FBQztRQTBCckMseUJBQW1CLEdBQVcsQ0FBQyxDQUFDOztJQW1OMUMsQ0FBQztJQXpPQyxzQkFBVyx5Q0FBYzthQUF6QjtZQUNFLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUM5QixDQUFDOzs7T0FBQTtJQUNPLDZDQUFxQixHQUE3QixVQUE4QixPQUFlO1FBQzNDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxPQUFPLENBQUM7SUFDckMsQ0FBQztJQUNELHNCQUFXLDZDQUFrQjthQUE3QjtZQUNFLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDO1FBQ2xDLENBQUM7OztPQUFBO0lBQ0Qsc0JBQVcsa0RBQXVCO2FBQWxDO1lBQ0UsT0FBTyxJQUFJLENBQUMsd0JBQXdCLENBQUM7UUFDdkMsQ0FBQzs7O09BQUE7SUFDTyxrREFBMEIsR0FBbEMsVUFBbUMsT0FBZTtRQUNoRCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsT0FBTyxDQUFDO0lBQzFDLENBQUM7SUFFTyw4Q0FBc0IsR0FBOUIsVUFBK0IsT0FBZTtRQUM1QyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsT0FBTyxDQUFDO0lBQ3RDLENBQUM7SUFDRCxzQkFBVyw4Q0FBbUI7YUFBOUI7WUFDRSxPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztRQUNuQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFXLDZDQUFrQjthQUE3QjtZQUNFLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDO1FBQ2xDLENBQUM7OztPQUFBO0lBRU0seUJBQVcsR0FBbEI7UUFDRSxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRTtZQUMzQiw4Q0FBOEM7WUFDOUMsYUFBYSxDQUFDLFFBQVEsR0FBRyxJQUFJLGFBQWEsQ0FBQyx1QkFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDeEUsYUFBYSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSx3Q0FBYyxDQUFDLHVCQUFhLENBQUMsYUFBYSxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMxRixhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxnQkFBSyxDQUFDLFNBQVMsQ0FBQztZQUMvQyxhQUFhLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3RDO1FBQ0QsT0FBTyxhQUFhLENBQUMsUUFBUSxDQUFDO0lBQ2hDLENBQUM7SUFFTSxxQkFBTyxHQUFkO1FBQ0UsSUFBSSxhQUFhLENBQUMsUUFBUTtZQUFFLGFBQWEsQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDbkUsYUFBYSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDaEMsQ0FBQztJQUlELHNCQUFZLG1DQUFRO2FBQXBCO1lBQ0UsT0FBTztnQkFDTCxhQUFhLEVBQUUsU0FBUyxHQUFHLHlCQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSzthQUMvRCxDQUFDO1FBQ0osQ0FBQzs7O09BQUE7SUFFRCxzQkFBWSwwQ0FBZTthQUEzQjs7WUFDRTtvQkFDRSxhQUFhLEVBQUUsU0FBUyxHQUFHLHlCQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSzs7Z0JBQzlELEdBQUMsVUFBVSxJQUFHLElBQUksQ0FBQyxPQUFPO2dCQUMxQixHQUFDLE9BQU8sSUFBRyxJQUFJLENBQUMsS0FBSzttQkFDckI7UUFDSixDQUFDOzs7T0FBQTtJQUVNLGdDQUFRLEdBQWYsVUFBZ0IsRUFBVTtRQUN4QixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRVMsbUNBQVcsR0FBckIsVUFBc0IsUUFBa0I7UUFDdEMsbUJBQVMsQ0FBQyxXQUFXLEVBQUU7YUFDcEIsaUJBQWlCLENBQUMsNEJBQWtCLEVBQUUsb0JBQVUsQ0FBQyxrQkFBa0IsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQzthQUN2RixJQUFJLENBQUM7WUFDSixRQUFRLEVBQUUsQ0FBQztRQUNiLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVTLG1DQUFXLEdBQXJCO1FBQ0UsbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyw0QkFBa0IsRUFBRSxvQkFBVSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDOUYsQ0FBQztJQUVTLGlDQUFTLEdBQW5CLFVBQW9CLEdBQVcsRUFBRSxPQUFvQjtRQUNuRCxtQkFBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLGVBQWUsQ0FBQyw0QkFBa0IsRUFBRSxvQkFBVSxDQUFDLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDcEgsQ0FBQztJQUVTLG9DQUFZLEdBQXRCLFVBQXVCLFVBQXNCLEVBQUUsU0FBaUI7UUFDOUQsc0JBQVksQ0FBQyxJQUFJLENBQUMsc0JBQVksQ0FBQyxhQUFhLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBRUQseUNBQWlCLEdBQWpCLFVBQWtCLFFBQTRDO1FBQTlELGlCQTRCQztRQTVCaUIseUJBQUEsRUFBQSxlQUE0QztRQUM1RCxJQUFJLE9BQU8sR0FBRyxJQUFJLHdCQUF3QixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25ELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDL0IsSUFBSSxXQUFXLEdBQUc7WUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBQyxHQUFHLEVBQUUsR0FBRztnQkFDOUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRUYsSUFBSSxDQUFDLGFBQWEsQ0FDaEIsS0FBSyxFQUNMLFdBQVcsRUFDWCxVQUFDLEdBQVk7WUFDWCxJQUFJLFlBQVksR0FBRyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztZQUN2RCxJQUFJLGlCQUFpQixHQUFHLEdBQUcsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQzVELElBQUksV0FBVyxHQUFHLEdBQUcsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1lBQzFELElBQUksZ0JBQWdCLEdBQUcsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLDBCQUEwQixFQUFFLENBQUM7WUFDcEUsSUFBSSxXQUFXLEdBQUcsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3BDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDbEQsS0FBSSxDQUFDLHNCQUFzQixDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDL0MsS0FBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3hDLEtBQUksQ0FBQywwQkFBMEIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ2xELFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNoQixDQUFDLEVBQ0QsSUFBSSxFQUNKLEtBQUssQ0FDTixDQUFDO0lBQ0osQ0FBQztJQUVELGtDQUFVLEdBQVYsVUFBVyxNQUFjLEVBQUUsUUFBb0Q7UUFBL0UsaUJBNkJDO1FBN0IwQix5QkFBQSxFQUFBLGVBQW9EO1FBQzdFLElBQUksT0FBTyxHQUFHLElBQUksK0JBQWlCLEVBQUUsQ0FBQztRQUN0QyxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDL0IsSUFBSSxXQUFXLEdBQUc7WUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBQyxHQUFHLEVBQUUsR0FBRztnQkFDdEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRUYsSUFBSSxDQUFDLGFBQWEsQ0FDaEIsS0FBSyxFQUNMLFdBQVcsRUFDWCxVQUFDLEdBQW9CO1lBQ25CLElBQUksWUFBWSxHQUFHLEdBQUcsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQ3ZELElBQUksaUJBQWlCLEdBQUcsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFDNUQsSUFBSSxXQUFXLEdBQUcsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixFQUFFLENBQUM7WUFDMUQsSUFBSSxnQkFBZ0IsR0FBRyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUMsMEJBQTBCLEVBQUUsQ0FBQztZQUNwRSxJQUFJLFdBQVcsR0FBRyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDaEQsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksRUFBRSxXQUFXLENBQUMsQ0FBQztZQUNsRCxLQUFJLENBQUMsc0JBQXNCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUMvQyxLQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDeEMsS0FBSSxDQUFDLDBCQUEwQixDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDbEQsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hCLENBQUMsRUFDRCxJQUFJLEVBQ0osS0FBSyxDQUNOLENBQUM7SUFDSixDQUFDO0lBRUQsaUNBQVMsR0FBVCxVQUFVLEdBQWtCO1FBQzFCLGlCQUFNLFNBQVMsWUFBQyxHQUFHLENBQUMsQ0FBQztRQUNyQixJQUFNLE1BQU0sR0FBRywyQkFBTSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1FBQy9ELElBQUksWUFBWSxHQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNyQyxnS0FBZ0s7UUFDaEssSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxFQUFFLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1NBQ3pEO0lBQ0gsQ0FBQztJQUVELG9DQUFZLEdBQVosVUFBYSxLQUFZO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNqQyxzREFBc0Q7WUFDdEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2pDO0lBQ0gsQ0FBQztJQUVELHFDQUFhLEdBQWIsVUFBYyxLQUFZO1FBQ3hCLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDaEMsdURBQXVEO1lBQ3ZELElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2pDO1FBQ0QsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxDQUFDLEVBQUU7WUFDL0IsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQU8sQ0FBQyxDQUFDLENBQUM7U0FDbEM7SUFDSCxDQUFDO0lBRUQsbUNBQVcsR0FBWCxVQUFZLFVBQTZCO1FBQTdCLDJCQUFBLEVBQUEsaUJBQTZCO1FBQ3ZDLDhDQUE4QztJQUNoRCxDQUFDO0lBRU8seUNBQWlCLEdBQXpCLFVBQTBCLEtBQWEsRUFBRSxXQUFtQjtRQUMxRCxrRUFBa0U7UUFDbEUsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFdBQVcsQ0FBQztRQUN2QyxzQkFBWSxDQUFDLElBQUksQ0FBQyxzQkFBWSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDdEUseUJBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVELGdDQUFnQztJQUN6Qix3Q0FBZ0IsR0FBdkIsVUFBd0IsUUFBZTtRQUFmLHlCQUFBLEVBQUEsZUFBZTtRQUNyQyw2Q0FBNkM7UUFDN0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQUMsSUFBSTtZQUMxQiwyQ0FBMkM7WUFDM0MsSUFBSSxRQUFRLEVBQUU7Z0JBQ1osUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2hCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sZ0NBQVEsR0FBZixVQUFnQixNQUFjLEVBQUUsV0FBbUI7UUFDakQsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLGlEQUFpRDtRQUNqRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLFVBQUMsTUFBTTtZQUNwQyxJQUFJLE1BQU0sSUFBSSxXQUFXLEVBQUU7Z0JBQ3pCLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLE9BQU87YUFDUjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxhQUFhLEVBQUU7WUFDakIsSUFBSSxXQUFXLEdBQUcsQ0FBQyxFQUFFO2dCQUNuQiwwQkFBMEI7Z0JBQzFCLElBQUksV0FBVyxJQUFJLElBQUksQ0FBQyxtQkFBbUI7b0JBQUUsT0FBTztnQkFDcEQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUMxQztZQUNELElBQUksQ0FBQyxlQUFlLElBQUksTUFBTSxDQUFDO1lBQy9CLHNCQUFZLENBQUMsSUFBSSxDQUFDLHNCQUFZLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDM0QseUJBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQy9ELElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFO2dCQUM1QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDaEM7U0FDRjtJQUNILENBQUM7SUFFRCx5Q0FBaUIsR0FBakIsVUFBa0IsT0FBYSxFQUFFLEdBQVc7UUFDMUMseUNBQXlDO1FBQ3pDLElBQUksT0FBTyxJQUFJLHVCQUFJLENBQUMsRUFBRTtZQUFFLE9BQU87UUFDL0IsSUFBSSxPQUFPLElBQUksdUJBQUksQ0FBQyxPQUFPO1lBQUUsT0FBTztRQUNwQyxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDbkIsUUFBUSxPQUFPLEVBQUU7WUFDZixLQUFLLHVCQUFJLENBQUMsb0JBQW9CO2dCQUM1QixTQUFTLEdBQUcsb0JBQVUsQ0FBQyw2QkFBNkIsQ0FBQztnQkFDckQsTUFBTTtZQUNSO2dCQUNFLFNBQVMsR0FBRyxPQUFPLEdBQUcsS0FBSyxHQUFHLEdBQUcsQ0FBQztnQkFDbEMsTUFBTTtTQUNUO1FBQ0QsbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxlQUFlLENBQUMsNEJBQWtCLEVBQUUsb0JBQVUsQ0FBQyxrQkFBa0IsRUFBRSxTQUFTLEVBQUUsY0FBTyxDQUFDLENBQUMsQ0FBQztJQUNsSCxDQUFDO0lBQ0gsb0JBQUM7QUFBRCxDQXRRQSxBQXNRQyxDQXRRMEMsNkNBQXVCLEdBc1FqRTs7QUFDRCx5REFBb0Q7QUFDcEQscURBQWdEO0FBQ2hELDhEQUFtTDtBQUNuTCxzRkFBNEU7QUFDNUUsc0RBQWlEO0FBRWpELHFEQUFpRDtBQUNqRCxtRkFBcUY7QUFDckYsd0VBQTJEO0FBQzNELGtEQUE2QztBQUM3QyxrRUFBNkQ7QUFDN0QsaURBQTRDO0FBQzVDLGtFQUE2RDtBQUU3RCxtREFBOEM7QUFDOUMsNERBQXVEO0FBQ3ZELGtEQUE2QztBQUM3Qyw0RUFBK0QiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIYW5kbGVyQ2xpZW50QmVuVGF1QmFzZSB9IGZyb20gXCJAZ2FtZWxvb3QvY2xpZW50LWJhc2UtaGFuZGxlclwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgV2FsbGV0SGFuZGxlciBleHRlbmRzIEhhbmRsZXJDbGllbnRCZW5UYXVCYXNlIHtcclxuICBwcm90ZWN0ZWQgc3RhdGljIGluc3RhbmNlOiBXYWxsZXRIYW5kbGVyO1xyXG5cclxuICBwcml2YXRlIGNsaWVudDogTXlXYWxsZXRDbGllbnQgPSBudWxsO1xyXG5cclxuICBwdWJsaWMgb25SZWNlaXZlU2VuZEV2ZW50OiAodXNlcklkOiBzdHJpbmcsIHNlbmRNZXNzYWdlOiBzdHJpbmcpID0+IHZvaWQgPSBudWxsO1xyXG5cclxuICBwdWJsaWMgb25SZWNlaXZlSm9pbkV2ZW50OiAoZXZ0OiBFdmVudCkgPT4gdm9pZCA9IG51bGw7XHJcblxyXG4gIHB1YmxpYyBvblJlY2VpdmVMZWF2ZUV2ZW50OiAoZXZ0OiBFdmVudCkgPT4gdm9pZCA9IG51bGw7XHJcblxyXG4gIHB1YmxpYyBvbkhhbmRsZVdpdGhEcmF3RnJvbVZhdWx0RXJyOiAoKSA9PiB2b2lkID0gbnVsbDtcclxuXHJcbiAgcHVibGljIG9uSGFuZGxlV2l0aERyYXdGcm9tVmF1bHRTdWNjZWVkOiAocmVzKSA9PiB2b2lkID0gbnVsbDtcclxuXHJcbiAgcHVibGljIG9uVmVyaWZ5T1RQRmFsc2U6ICgpID0+IHZvaWQgPSBudWxsO1xyXG5cclxuICBwcml2YXRlIGdhbWVQbGF5aW5nczogTWFwPFRvcGljLCBudW1iZXI+ID0gbmV3IE1hcDxUb3BpYywgbnVtYmVyPigpO1xyXG5cclxuICBwcml2YXRlIGxpc3RUcmFuc2FjdGlvbklEOiBudW1iZXJbXSA9IFtdO1xyXG4gIHByaXZhdGUgbWF4OiBudW1iZXIgPSA1MDtcclxuXHJcbiAgcHJpdmF0ZSBfY3VycmVudEJhbGFuY2U6IG51bWJlciA9IDA7XHJcbiAgcHJpdmF0ZSBfY3VycmVudFZhdWx0QmFsYW5jZTogbnVtYmVyID0gMDtcclxuICBwcml2YXRlIF9jdXJyZW50TWFpbkJhbGFuY2U6IG51bWJlciA9IDA7XHJcbiAgcHJpdmF0ZSBfY3VycmVudFByb21vdGlvbkJhbGFuY2U6IG51bWJlciA9IDA7XHJcbiAgcHJpdmF0ZSBfeE9UUElkOiBzdHJpbmc7XHJcbiAgcHJpdmF0ZSBPVFBJZDogc3RyaW5nO1xyXG4gIHByaXZhdGUgX3RpbWVSZWNhbGxPVFA6IG51bWJlcjtcclxuICBwdWJsaWMgZ2V0IGN1cnJlbnRCYWxhbmNlKCk6IG51bWJlciB7XHJcbiAgICByZXR1cm4gdGhpcy5fY3VycmVudEJhbGFuY2U7XHJcbiAgfVxyXG4gIHByaXZhdGUgc2V0Q3VycmVudE1haW5CYWxhbmNlKGJhbGFuY2U6IG51bWJlcikge1xyXG4gICAgdGhpcy5fY3VycmVudE1haW5CYWxhbmNlID0gYmFsYW5jZTtcclxuICB9XHJcbiAgcHVibGljIGdldCBDdXJyZW50TWFpbkJhbGFuY2UoKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLl9jdXJyZW50TWFpbkJhbGFuY2U7XHJcbiAgfVxyXG4gIHB1YmxpYyBnZXQgQ3VycmVudFByb21vdGlvbkJhbGFuY2UoKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLl9jdXJyZW50UHJvbW90aW9uQmFsYW5jZTtcclxuICB9XHJcbiAgcHJpdmF0ZSBzZXRDdXJyZW50UHJvbW90aW9uQmFsYW5jZShiYWxhbmNlOiBudW1iZXIpIHtcclxuICAgIHRoaXMuX2N1cnJlbnRQcm9tb3Rpb25CYWxhbmNlID0gYmFsYW5jZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc2V0Q3VycmVudFZhdWx0QmFsYW5jZShiYWxhbmNlOiBudW1iZXIpIHtcclxuICAgIHRoaXMuX2N1cnJlbnRWYXVsdEJhbGFuY2UgPSBiYWxhbmNlO1xyXG4gIH1cclxuICBwdWJsaWMgZ2V0IGN1cnJlbnRWYXVsdEJhbGFuY2UoKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLl9jdXJyZW50VmF1bHRCYWxhbmNlO1xyXG4gIH1cclxuICBwcml2YXRlIF9jdXJyZW50VHJhbnNhY3Rpb246IG51bWJlciA9IDA7XHJcbiAgcHVibGljIGdldCBjdXJyZW50VHJhbnNhY3Rpb24oKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0aGlzLl9jdXJyZW50VHJhbnNhY3Rpb247XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgZ2V0SW5zdGFuY2UoKTogV2FsbGV0SGFuZGxlciB7XHJcbiAgICBpZiAoIVdhbGxldEhhbmRsZXIuaW5zdGFuY2UpIHtcclxuICAgICAgLy8gY2MubG9nKFwibmV3IGdldEluc3RhbmNlKCk6IFdhbGxldEhhbmRsZXJcIik7XHJcbiAgICAgIFdhbGxldEhhbmRsZXIuaW5zdGFuY2UgPSBuZXcgV2FsbGV0SGFuZGxlcihCZW5UYXVIYW5kbGVyLmdldEluc3RhbmNlKCkpO1xyXG4gICAgICBXYWxsZXRIYW5kbGVyLmluc3RhbmNlLmNsaWVudCA9IG5ldyBNeVdhbGxldENsaWVudChDb25uZWN0RGVmaW5lLmFkZHJlc3NTYW5CYXksIHt9LCBudWxsKTtcclxuICAgICAgV2FsbGV0SGFuZGxlci5pbnN0YW5jZS50b3BpYyA9IFRvcGljLk1ZX1dBTExFVDtcclxuICAgICAgV2FsbGV0SGFuZGxlci5pbnN0YW5jZS5vblN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIFdhbGxldEhhbmRsZXIuaW5zdGFuY2U7XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgZGVzdHJveSgpIHtcclxuICAgIGlmIChXYWxsZXRIYW5kbGVyLmluc3RhbmNlKSBXYWxsZXRIYW5kbGVyLmluc3RhbmNlLm9uVW5TdWJzY3JpYmUoKTtcclxuICAgIFdhbGxldEhhbmRsZXIuaW5zdGFuY2UgPSBudWxsO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGN1ckNoYW5uZWxJZDogc3RyaW5nO1xyXG5cclxuICBwcml2YXRlIGdldCBtZXRhRGF0YSgpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIEF1dGhvcml6YXRpb246IFwiQmVhcmVyIFwiICsgSWRlbnRpdHlIYW5kbGVyLmdldEluc3RhbmNlKCkudG9rZW4sXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXQgbWV0YURhdGFXaXRoT1RQKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgQXV0aG9yaXphdGlvbjogXCJCZWFyZXIgXCIgKyBJZGVudGl0eUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS50b2tlbixcclxuICAgICAgW1wieC1vdHAtaWRcIl06IHRoaXMuX3hPVFBJZCxcclxuICAgICAgW1wieC1vdHBcIl06IHRoaXMuT1RQSWQsXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHNldE9UUElEKGlkOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuT1RQSWQgPSBpZDtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBzaG93TG9hZGluZyhjYWxsYmFjazogRnVuY3Rpb24pIHtcclxuICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpXHJcbiAgICAgIC5vcGVuUG9wdXBTeXN0ZW1WMihMb2FkaW5nUG9wdXBTeXN0ZW0sIEdhbWVEZWZpbmUuTG9hZGluZ1BvcHVwU3lzdGVtLCBmYWxzZSwgbnVsbCwgbnVsbClcclxuICAgICAgLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgIGNhbGxiYWNrKCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIGhpZGVMb2FkaW5nKCkge1xyXG4gICAgVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkuY2xvc2VQb3B1cFN5c3RlbShMb2FkaW5nUG9wdXBTeXN0ZW0sIEdhbWVEZWZpbmUuTG9hZGluZ1BvcHVwU3lzdGVtKTtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBvblNob3dFcnIoc3RyOiBzdHJpbmcsIG9uQ2xpY2s/OiAoKSA9PiB2b2lkKSB7XHJcbiAgICBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5vcGVuUG9wdXBTeXN0ZW0oV2FybmluZ1BvcHVwU3lzdGVtLCBHYW1lRGVmaW5lLldhcm5pbmdQb3B1cFN5c3RlbSwgc3RyLCBvbkNsaWNrLCBvbkNsaWNrKTtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBvbkRpc2Nvbm5lY3QoQ29kZVNvY2tldDogQ29kZVNvY2tldCwgbWVzc0Vycm9yOiBzdHJpbmcpIHtcclxuICAgIEV2ZW50TWFuYWdlci5maXJlKEV2ZW50TWFuYWdlci5PTl9ESVNDT05ORUNULCBDb2RlU29ja2V0LCBmYWxzZSwgbWVzc0Vycm9yKTtcclxuICB9XHJcblxyXG4gIG9uTXlXYWxsZXRSZXF1ZXN0KHJlc3BvbnNlOiAocmVzcG9uc2U6IE1lUmVwbHkpID0+IHZvaWQgPSBudWxsKSB7XHJcbiAgICBsZXQgcmVxdWVzdCA9IG5ldyBnb29nbGVfcHJvdG9idWZfZW1wdHlfcGIuRW1wdHkoKTtcclxuICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgIHZhciBtc2dJZCA9IHNlbGYuZ2V0VW5pcXVlSWQoKTtcclxuICAgIGxldCBzZW5kUmVxdWVzdCA9ICgpID0+IHtcclxuICAgICAgc2VsZi5jbGllbnQubWUocmVxdWVzdCwgc2VsZi5tZXRhRGF0YSwgKGVyciwgcmVzKSA9PiB7XHJcbiAgICAgICAgc2VsZi5vblNlbmRSZXBseShlcnIsIHJlcywgbXNnSWQpO1xyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5vblNlbmRSZXF1ZXN0KFxyXG4gICAgICBtc2dJZCxcclxuICAgICAgc2VuZFJlcXVlc3QsXHJcbiAgICAgIChyZXM6IE1lUmVwbHkpID0+IHtcclxuICAgICAgICBsZXQgd2FsbGV0QW1vdW50ID0gcmVzLmdldFdhbGxldCgpLmdldEN1cnJlbnRCYWxhbmNlKCk7XHJcbiAgICAgICAgbGV0IHZhdWx0V2FsbGV0QW1vdW50ID0gcmVzLmdldFdhbGxldCgpLmdldEJhbGFuY2VJblZhdWx0KCk7XHJcbiAgICAgICAgbGV0IG1haW5CYWxhbmNlID0gcmVzLmdldFdhbGxldCgpLmdldEN1cnJlbnRNYWluQmFsYW5jZSgpO1xyXG4gICAgICAgIGxldCBwcm9tb3Rpb25CYWxhbmNlID0gcmVzLmdldFdhbGxldCgpLmdldEN1cnJlbnRQcm9tb3Rpb25CYWxhbmNlKCk7XHJcbiAgICAgICAgbGV0IHRyYW5zYWN0aW9uID0gcmVzLmdldExhc3RUeElkKCk7XHJcbiAgICAgICAgdGhpcy5zZXRDdXJyZW50QmFsYW5jZSh3YWxsZXRBbW91bnQsIHRyYW5zYWN0aW9uKTtcclxuICAgICAgICB0aGlzLnNldEN1cnJlbnRWYXVsdEJhbGFuY2UodmF1bHRXYWxsZXRBbW91bnQpO1xyXG4gICAgICAgIHRoaXMuc2V0Q3VycmVudE1haW5CYWxhbmNlKG1haW5CYWxhbmNlKTtcclxuICAgICAgICB0aGlzLnNldEN1cnJlbnRQcm9tb3Rpb25CYWxhbmNlKHByb21vdGlvbkJhbGFuY2UpO1xyXG4gICAgICAgIHJlc3BvbnNlKHJlcyk7XHJcbiAgICAgIH0sXHJcbiAgICAgIG51bGwsXHJcbiAgICAgIGZhbHNlXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgcHV0VG9WYXVsdChhbW91bnQ6IG51bWJlciwgcmVzcG9uc2U6IChyZXNwb25zZTogUHV0VG9WYXVsdFJlcGx5KSA9PiB2b2lkID0gbnVsbCkge1xyXG4gICAgbGV0IHJlcXVlc3QgPSBuZXcgUHV0VG9WYXVsdFJlcXVlc3QoKTtcclxuICAgIHJlcXVlc3Quc2V0QW1vdW50KGFtb3VudCk7XHJcbiAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICB2YXIgbXNnSWQgPSBzZWxmLmdldFVuaXF1ZUlkKCk7XHJcbiAgICBsZXQgc2VuZFJlcXVlc3QgPSAoKSA9PiB7XHJcbiAgICAgIHNlbGYuY2xpZW50LnB1dFRvVmF1bHQocmVxdWVzdCwgc2VsZi5tZXRhRGF0YSwgKGVyciwgcmVzKSA9PiB7XHJcbiAgICAgICAgc2VsZi5vblNlbmRSZXBseShlcnIsIHJlcywgbXNnSWQpO1xyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5vblNlbmRSZXF1ZXN0KFxyXG4gICAgICBtc2dJZCxcclxuICAgICAgc2VuZFJlcXVlc3QsXHJcbiAgICAgIChyZXM6IFB1dFRvVmF1bHRSZXBseSkgPT4ge1xyXG4gICAgICAgIGxldCB3YWxsZXRBbW91bnQgPSByZXMuZ2V0V2FsbGV0KCkuZ2V0Q3VycmVudEJhbGFuY2UoKTtcclxuICAgICAgICBsZXQgdmF1bHRXYWxsZXRBbW91bnQgPSByZXMuZ2V0V2FsbGV0KCkuZ2V0QmFsYW5jZUluVmF1bHQoKTtcclxuICAgICAgICBsZXQgbWFpbkJhbGFuY2UgPSByZXMuZ2V0V2FsbGV0KCkuZ2V0Q3VycmVudE1haW5CYWxhbmNlKCk7XHJcbiAgICAgICAgbGV0IHByb21vdGlvbkJhbGFuY2UgPSByZXMuZ2V0V2FsbGV0KCkuZ2V0Q3VycmVudFByb21vdGlvbkJhbGFuY2UoKTtcclxuICAgICAgICBsZXQgdHJhbnNhY3Rpb24gPSByZXMuZ2V0Q2hhbmdlKCkuZ2V0TGFzdFR4SWQoKTtcclxuICAgICAgICB0aGlzLnNldEN1cnJlbnRCYWxhbmNlKHdhbGxldEFtb3VudCwgdHJhbnNhY3Rpb24pO1xyXG4gICAgICAgIHRoaXMuc2V0Q3VycmVudFZhdWx0QmFsYW5jZSh2YXVsdFdhbGxldEFtb3VudCk7XHJcbiAgICAgICAgdGhpcy5zZXRDdXJyZW50TWFpbkJhbGFuY2UobWFpbkJhbGFuY2UpO1xyXG4gICAgICAgIHRoaXMuc2V0Q3VycmVudFByb21vdGlvbkJhbGFuY2UocHJvbW90aW9uQmFsYW5jZSk7XHJcbiAgICAgICAgcmVzcG9uc2UocmVzKTtcclxuICAgICAgfSxcclxuICAgICAgbnVsbCxcclxuICAgICAgZmFsc2VcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBvblJlY2VpdmUobXNnOiBCZW5UYXVNZXNzYWdlKSB7XHJcbiAgICBzdXBlci5vblJlY2VpdmUobXNnKTtcclxuICAgIGNvbnN0IGNoYW5nZSA9IENoYW5nZS5kZXNlcmlhbGl6ZUJpbmFyeShtc2cuZ2V0UGF5bG9hZF9hc1U4KCkpO1xyXG4gICAgbGV0IHRvcGljUmVjZWl2ZSA9IGNoYW5nZS5nZXRUb3BpYygpO1xyXG4gICAgLy9jb25zb2xlLmxvZyhcIndhbGxldCB0b3BpYyBcIiArIHRvcGljUmVjZWl2ZSArIFwiIGFtb3VudCBjaGFuZ2UgXCIgKyBjaGFuZ2UuZ2V0QW1vdW50KCkgKyBcIiBhbW91bnQgbmV3IFwiICsgY2hhbmdlLmdldE5ld0JhbGFuY2UoKSArIFwiIFwiICsgY2hhbmdlLmdldEhhcHBlbmVkQXQoKSk7XHJcbiAgICBpZiAoIXRoaXMuZ2FtZVBsYXlpbmdzLmhhcyh0b3BpY1JlY2VpdmUpKSB7XHJcbiAgICAgIHRoaXMuYWRkTW9uZXkoY2hhbmdlLmdldEFtb3VudCgpLCBjaGFuZ2UuZ2V0TGFzdFR4SWQoKSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvbkVuYWJsZUdhbWUodG9waWM6IFRvcGljKSB7XHJcbiAgICBpZiAoIXRoaXMuZ2FtZVBsYXlpbmdzLmhhcyh0b3BpYykpIHtcclxuICAgICAgLy9jYy5sb2coXCJXYWxsZXQgb25FbmFibGVHYW1lIC4uLi4uLi4uLi4uLi5cIiArIHRvcGljKTtcclxuICAgICAgdGhpcy5nYW1lUGxheWluZ3Muc2V0KHRvcGljLCAwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uRGlzYWJsZUdhbWUodG9waWM6IFRvcGljKSB7XHJcbiAgICBpZiAodGhpcy5nYW1lUGxheWluZ3MuaGFzKHRvcGljKSkge1xyXG4gICAgICAvL2NjLmxvZyhcIldhbGxldCBvbkRpc2FibGVHYW1lIC4uLi4uLi4uLi4uLi5cIiArIHRvcGljKTtcclxuICAgICAgdGhpcy5nYW1lUGxheWluZ3MuZGVsZXRlKHRvcGljKTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmdhbWVQbGF5aW5ncy5zaXplID09IDApIHtcclxuICAgICAgdGhpcy5vbk15V2FsbGV0UmVxdWVzdCgoKSA9PiB7fSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvblJlQ29ubmVjdChvbkNvbXBsZXRlOiAoKSA9PiB2b2lkID0gbnVsbCk6IHZvaWQge1xyXG4gICAgLy9CZW5UYXVXU0hhbmRsZXIuZ2V0SW5zdGFuY2UoKS5vblJlQ29ubmVjdCgpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzZXRDdXJyZW50QmFsYW5jZSh2YWx1ZTogbnVtYmVyLCB0cmFuc2FjdGlvbjogbnVtYmVyKSB7XHJcbiAgICAvL2NjLmxvZyhcIlVwZGF0ZSBzZXRDdXJyZW50QmFsYW5jZSBcIiArIHZhbHVlICsgXCIgXCIgKyB0cmFuc2FjdGlvbik7XHJcbiAgICB0aGlzLl9jdXJyZW50QmFsYW5jZSA9IHZhbHVlO1xyXG4gICAgdGhpcy5fY3VycmVudFRyYW5zYWN0aW9uID0gdHJhbnNhY3Rpb247XHJcbiAgICBFdmVudE1hbmFnZXIuZmlyZShFdmVudE1hbmFnZXIub25VcGRhdGVCYWxhbmNlLCB0aGlzLl9jdXJyZW50QmFsYW5jZSk7XHJcbiAgICBFdmVudEJ1c01hbmFnZXIub25GaXJlVXBkYXRlQmFsYW5jZSh0aGlzLl9jdXJyZW50QmFsYW5jZSk7XHJcbiAgfVxyXG5cclxuICAvL1RPRE8gbGF5IGJhbGFuY2UgdGhlbyByZXZpc2lvblxyXG4gIHB1YmxpYyBnZXRMYXRlc3RCYWxhbmNlKGNhbGxiYWNrID0gbnVsbCkge1xyXG4gICAgLy8gY2MubG9nKFwiY2FsbGJhY2sgMSBcIiArIChjYWxsYmFjayAhPSBudWxsKSlcclxuICAgIHRoaXMub25NeVdhbGxldFJlcXVlc3QoKHJlc3ApID0+IHtcclxuICAgICAgLy8gY2MubG9nKFwiY2FsbGJhY2sgXCIgKyAoY2FsbGJhY2sgIT0gbnVsbCkpXHJcbiAgICAgIGlmIChjYWxsYmFjaykge1xyXG4gICAgICAgIGNhbGxiYWNrKHJlc3ApO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBhZGRNb25leShhbW91bnQ6IG51bWJlciwgdHJhbnNhY3Rpb246IG51bWJlcikge1xyXG4gICAgbGV0IGFsbG93QWRkTW9uZXkgPSB0cnVlO1xyXG4gICAgLy9jYy5sb2coXCJ3YWxsZXQgXCIgKyBhbW91bnQgKyBcIiBcIiArIHRyYW5zYWN0aW9uKTtcclxuICAgIHRoaXMubGlzdFRyYW5zYWN0aW9uSUQuZm9yRWFjaCgoc2F2ZUlEKSA9PiB7XHJcbiAgICAgIGlmIChzYXZlSUQgPT0gdHJhbnNhY3Rpb24pIHtcclxuICAgICAgICBhbGxvd0FkZE1vbmV5ID0gZmFsc2U7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGlmIChhbGxvd0FkZE1vbmV5KSB7XHJcbiAgICAgIGlmICh0cmFuc2FjdGlvbiA+IDApIHtcclxuICAgICAgICAvLyBjaGVhdCA9PTAgZm9yIEZJU0ggdGVtcFxyXG4gICAgICAgIGlmICh0cmFuc2FjdGlvbiA8PSB0aGlzLl9jdXJyZW50VHJhbnNhY3Rpb24pIHJldHVybjtcclxuICAgICAgICB0aGlzLmxpc3RUcmFuc2FjdGlvbklELnB1c2godHJhbnNhY3Rpb24pO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuX2N1cnJlbnRCYWxhbmNlICs9IGFtb3VudDtcclxuICAgICAgRXZlbnRNYW5hZ2VyLmZpcmUoRXZlbnRNYW5hZ2VyLm9uVXBkYXRlQWRkQmFsYW5jZSwgYW1vdW50KTtcclxuICAgICAgRXZlbnRCdXNNYW5hZ2VyLm9uRmlyZUFkZEJhbGFuY2UoYW1vdW50LCB0aGlzLl9jdXJyZW50QmFsYW5jZSk7XHJcbiAgICAgIGlmICh0aGlzLmxpc3RUcmFuc2FjdGlvbklELmxlbmd0aCA+IHRoaXMubWF4KSB7XHJcbiAgICAgICAgdGhpcy5saXN0VHJhbnNhY3Rpb25JRC5zaGlmdCgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBoYW5kbGVDdXN0b21FcnJvcihlcnJDb2RlOiBDb2RlLCBlcnI6IHN0cmluZykge1xyXG4gICAgLy9jYy5sb2coXCJoYW5kbGVDdXN0b21FcnJvciBcIiArIGVyckNvZGUpO1xyXG4gICAgaWYgKGVyckNvZGUgPT0gQ29kZS5PSykgcmV0dXJuO1xyXG4gICAgaWYgKGVyckNvZGUgPT0gQ29kZS5VTktOT1dOKSByZXR1cm47XHJcbiAgICBsZXQgbWVzc0Vycm9yID0gXCJcIjtcclxuICAgIHN3aXRjaCAoZXJyQ29kZSkge1xyXG4gICAgICBjYXNlIENvZGUuSU5TVUZGSUNJRU5UX0JBTEFOQ0U6XHJcbiAgICAgICAgbWVzc0Vycm9yID0gUG9ydGFsVGV4dC5QT1JfTUVTU19JTlNVRkZJQ0lFTlRfQkFMQU5DRTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgZGVmYXVsdDpcclxuICAgICAgICBtZXNzRXJyb3IgPSBlcnJDb2RlICsgXCIgLSBcIiArIGVycjtcclxuICAgICAgICBicmVhaztcclxuICAgIH1cclxuICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpLm9wZW5Qb3B1cFN5c3RlbShXYXJuaW5nUG9wdXBTeXN0ZW0sIEdhbWVEZWZpbmUuV2FybmluZ1BvcHVwU3lzdGVtLCBtZXNzRXJyb3IsICgpID0+IHt9KTtcclxuICB9XHJcbn1cclxuaW1wb3J0IENvbm5lY3REZWZpbmUgZnJvbSBcIi4uL0RlZmluZS9Db25uZWN0RGVmaW5lXCI7XHJcbmltcG9ydCBJZGVudGl0eUhhbmRsZXIgZnJvbSBcIi4vSWRlbnRpdHlIYW5kbGVyXCI7XHJcbmltcG9ydCB7IE1lUmVwbHksIEdldEhpc3RvcnlSZXF1ZXN0LCBHZXRIaXN0b3J5UmVwbHksIFB1dFRvVmF1bHRSZXBseSwgUHV0VG9WYXVsdFJlcXVlc3QsIFdpdGhkcmF3RnJvbVZhdWx0UmVwbHksIFdpdGhkcmF3RnJvbVZhdWx0UmVxdWVzdCB9IGZyb20gXCJAZ2FtZWxvb3QvbXl3YWxsZXQvbXl3YWxsZXRfcGJcIjtcclxuaW1wb3J0IHsgTXlXYWxsZXRDbGllbnQgfSBmcm9tIFwiQGdhbWVsb290L215d2FsbGV0L015d2FsbGV0U2VydmljZUNsaWVudFBiXCI7XHJcbmltcG9ydCBFdmVudE1hbmFnZXIgZnJvbSBcIi4uL0V2ZW50L0V2ZW50TWFuYWdlclwiO1xyXG5pbXBvcnQgeyBCZW5UYXVNZXNzYWdlIH0gZnJvbSBcIkBnYW1lbG9vdC9iZW50YXUvYmVudGF1X3BiXCI7XHJcbmltcG9ydCB7IFRvcGljIH0gZnJvbSBcIkBnYW1lbG9vdC90b3BpYy90b3BpY19wYlwiO1xyXG5pbXBvcnQgKiBhcyBnb29nbGVfcHJvdG9idWZfZW1wdHlfcGIgZnJvbSBcImdvb2dsZS1wcm90b2J1Zi9nb29nbGUvcHJvdG9idWYvZW1wdHlfcGJcIjtcclxuaW1wb3J0IHsgQ29kZSB9IGZyb20gXCJAZ2FtZWxvb3QvbXl3YWxsZXQvbXl3YWxsZXRfY29kZV9wYlwiO1xyXG5pbXBvcnQgVUlNYW5hZ2VyIGZyb20gXCIuLi9NYW5hZ2VyL1VJTWFuYWdlclwiO1xyXG5pbXBvcnQgV2FybmluZ1BvcHVwU3lzdGVtIGZyb20gXCIuLi9Qb3B1cC9XYXJuaW5nUG9wdXBTeXN0ZW1cIjtcclxuaW1wb3J0IEJlblRhdUhhbmRsZXIgZnJvbSBcIi4vQmVuVGF1SGFuZGxlclwiO1xyXG5pbXBvcnQgTG9hZGluZ1BvcHVwU3lzdGVtIGZyb20gXCIuLi9Qb3B1cC9Mb2FkaW5nUG9wdXBTeXN0ZW1cIjtcclxuaW1wb3J0IHsgQ29kZVNvY2tldCB9IGZyb20gXCIuLi9VdGlscy9FbnVtR2FtZVwiO1xyXG5pbXBvcnQgR2FtZURlZmluZSBmcm9tIFwiLi4vRGVmaW5lL0dhbWVEZWZpbmVcIjtcclxuaW1wb3J0IEV2ZW50QnVzTWFuYWdlciBmcm9tIFwiLi4vRXZlbnQvRXZlbnRCdXNNYW5hZ2VyXCI7XHJcbmltcG9ydCBQb3J0YWxUZXh0IGZyb20gXCIuLi9VdGlscy9Qb3J0YWxUZXh0XCI7XHJcbmltcG9ydCB7IENoYW5nZSB9IGZyb20gXCJAZ2FtZWxvb3QvbXl3YWxsZXQvbXl3YWxsZXRfY2hhbmdlX3BiXCI7XHJcbmltcG9ydCBUZXh0Tm90aWZ5LCB7IFRleHROb3RpZnlEYXRhIH0gZnJvbSBcIi4uL01hbmFnZXIvVGV4dE5vdGlmeVwiO1xyXG5pbXBvcnQgR2FtZVV0aWxzIGZyb20gXCIuLi9VdGlscy9HYW1lVXRpbHNcIjtcclxuIl19