
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Network/WebSocketClient/WSClient_WebAndIOS.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e1c50E/a6JNe5Mz0gGC0M05', 'WSClient_WebAndIOS');
// Game/Lobby/Utils/WebSocketClient/WSClient_WebAndIOS.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumGame_1 = require("../../Utils/EnumGame");
var IdentityHandler_1 = require("../../Network/IdentityHandler");
var bentau_pb_1 = require("@gameloot/bentau/bentau_pb");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var WSClient_WebAndIOS = /** @class */ (function (_super) {
    __extends(WSClient_WebAndIOS, _super);
    function WSClient_WebAndIOS() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.websocket = null;
        _this.isError = false;
        return _this;
    }
    WSClient_WebAndIOS.prototype.open = function (address, onOpen, onMessage, onClose, onError, onReconnect) {
        this.isError = false;
        this;
        try {
            this.websocket = new WebSocket(address, [IdentityHandler_1.default.getInstance().token]);
        }
        catch (error) {
            var code = EnumGame_1.CodeSocket.FORCE_CLOSE;
            var reason = "Đường truyền mạng không ổn định!";
            onError && onError(code, reason);
        }
        var self = this;
        this.websocket.binaryType = "arraybuffer";
        this.onClose = onClose;
        this.onReconnect = onReconnect;
        this.websocket.onopen = function () {
            onOpen && onOpen();
        };
        this.websocket.onmessage = function (mgs) {
            var buffer = new Uint8Array(mgs.data);
            var benTauMessage = bentau_pb_1.BenTauMessage.deserializeBinary(buffer);
            // Parse data
            onMessage && onMessage(benTauMessage);
        };
        this.websocket.onclose = function (message) {
            // Hiện tại không cần xử lý case này, case này là hệ thông tự đóng, còn các trường hợp manual close thì đã xử lý.
            //cc.log("System onClose code " + self.isError + " " + message.code + " message " + message.reason + " " + self.idWS);
        };
        this.websocket.onerror = function (message) {
            self.isError = true;
            var code = message.code;
            var reason = message.reason;
            //cc.log("onerror code " + message.code + " message " + message.reason + " " + self.idWS);
            // onError && onError(code, reason);
        };
    };
    WSClient_WebAndIOS.prototype.send = function (data) {
        if (this.websocket.readyState == WebSocket.OPEN)
            this.websocket.send(data);
    };
    WSClient_WebAndIOS.prototype.reConnect = function (code, reason) {
        // //cc.log("reConnect = " + code + " reason = " + reason + " idWS " + this.idWS + " isOpen " + this.websocket.readyState);
        this.onReconnect && this.onReconnect(code, reason);
    };
    WSClient_WebAndIOS.prototype.closeWS = function (code, reason) {
        //cc.log("closeWS = " + code + " reason = " + reason + " idWS " + this.idWS + " isOpen " + this.websocket.readyState);
        if (this.websocket == null || this.websocket.readyState == WebSocket.CLOSED)
            return;
        this.websocket.close(EnumGame_1.CodeSocket.FORCE_CLOSE);
    };
    WSClient_WebAndIOS = __decorate([
        ccclass
    ], WSClient_WebAndIOS);
    return WSClient_WebAndIOS;
}(cc.Component));
exports.default = WSClient_WebAndIOS;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L1V0aWxzL1dlYlNvY2tldENsaWVudC9XU0NsaWVudF9XZWJBbmRJT1MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsaURBQWtEO0FBQ2xELGlFQUE0RDtBQUM1RCx3REFBMkQ7QUFFckQsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBZ0Qsc0NBQVk7SUFBNUQ7UUFBQSxxRUEwREM7UUF2RFMsZUFBUyxHQUFjLElBQUksQ0FBQztRQUM1QixhQUFPLEdBQVksS0FBSyxDQUFDOztJQXNEbkMsQ0FBQztJQXJEUSxpQ0FBSSxHQUFYLFVBQVksT0FBTyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXO1FBQ25FLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQztRQUNMLElBQUk7WUFDRixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksU0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDLHlCQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztTQUNoRjtRQUFDLE9BQU8sS0FBSyxFQUFFO1lBQ2QsSUFBTSxJQUFJLEdBQUcscUJBQVUsQ0FBQyxXQUFXLENBQUM7WUFDcEMsSUFBTSxNQUFNLEdBQUcsa0NBQWtDLENBQUM7WUFDbEQsT0FBTyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDbEM7UUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsYUFBYSxDQUFDO1FBQzFDLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQy9CLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHO1lBQ3RCLE1BQU0sSUFBSSxNQUFNLEVBQUUsQ0FBQztRQUNyQixDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxVQUFVLEdBQUc7WUFDdEMsSUFBSSxNQUFNLEdBQUcsSUFBSSxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RDLElBQUksYUFBYSxHQUFHLHlCQUFhLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDNUQsYUFBYTtZQUNiLFNBQVMsSUFBSSxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDeEMsQ0FBQyxDQUFDO1FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsVUFBVSxPQUFtQjtZQUNwRCxpSEFBaUg7WUFDakgsc0hBQXNIO1FBQ3hILENBQUMsQ0FBQztRQUVGLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHLFVBQVUsT0FBbUI7WUFDcEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDcEIsSUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztZQUMxQixJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO1lBQzlCLDBGQUEwRjtZQUMxRixvQ0FBb0M7UUFDdEMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVNLGlDQUFJLEdBQVgsVUFBWSxJQUFJO1FBQ2QsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsSUFBSSxTQUFTLENBQUMsSUFBSTtZQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFFTSxzQ0FBUyxHQUFoQixVQUFpQixJQUFZLEVBQUUsTUFBYztRQUMzQywySEFBMkg7UUFDM0gsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRU0sb0NBQU8sR0FBZCxVQUFlLElBQVksRUFBRSxNQUFjO1FBQ3pDLHNIQUFzSDtRQUN0SCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxJQUFJLFNBQVMsQ0FBQyxNQUFNO1lBQUUsT0FBTztRQUNwRixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxxQkFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUF6RGtCLGtCQUFrQjtRQUR0QyxPQUFPO09BQ2Esa0JBQWtCLENBMER0QztJQUFELHlCQUFDO0NBMURELEFBMERDLENBMUQrQyxFQUFFLENBQUMsU0FBUyxHQTBEM0Q7a0JBMURvQixrQkFBa0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb2RlU29ja2V0IH0gZnJvbSBcIi4uLy4uL1V0aWxzL0VudW1HYW1lXCI7XG5pbXBvcnQgSWRlbnRpdHlIYW5kbGVyIGZyb20gXCIuLi8uLi9OZXR3b3JrL0lkZW50aXR5SGFuZGxlclwiO1xuaW1wb3J0IHsgQmVuVGF1TWVzc2FnZSB9IGZyb20gXCJAZ2FtZWxvb3QvYmVudGF1L2JlbnRhdV9wYlwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgV1NDbGllbnRfV2ViQW5kSU9TIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgcHJpdmF0ZSBvbkNsb3NlO1xuICBwcml2YXRlIG9uUmVjb25uZWN0O1xuICBwcml2YXRlIHdlYnNvY2tldDogV2ViU29ja2V0ID0gbnVsbDtcbiAgcHJpdmF0ZSBpc0Vycm9yOiBib29sZWFuID0gZmFsc2U7XG4gIHB1YmxpYyBvcGVuKGFkZHJlc3MsIG9uT3Blbiwgb25NZXNzYWdlLCBvbkNsb3NlLCBvbkVycm9yLCBvblJlY29ubmVjdCkge1xuICAgIHRoaXMuaXNFcnJvciA9IGZhbHNlO1xuICAgIHRoaXM7XG4gICAgdHJ5IHtcbiAgICAgIHRoaXMud2Vic29ja2V0ID0gbmV3IFdlYlNvY2tldChhZGRyZXNzLCBbSWRlbnRpdHlIYW5kbGVyLmdldEluc3RhbmNlKCkudG9rZW5dKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgY29uc3QgY29kZSA9IENvZGVTb2NrZXQuRk9SQ0VfQ0xPU0U7XG4gICAgICBjb25zdCByZWFzb24gPSBcIsSQxrDhu51uZyB0cnV54buBbiBt4bqhbmcga2jDtG5nIOG7lW4gxJHhu4tuaCFcIjtcbiAgICAgIG9uRXJyb3IgJiYgb25FcnJvcihjb2RlLCByZWFzb24pO1xuICAgIH1cbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgdGhpcy53ZWJzb2NrZXQuYmluYXJ5VHlwZSA9IFwiYXJyYXlidWZmZXJcIjtcbiAgICB0aGlzLm9uQ2xvc2UgPSBvbkNsb3NlO1xuICAgIHRoaXMub25SZWNvbm5lY3QgPSBvblJlY29ubmVjdDtcbiAgICB0aGlzLndlYnNvY2tldC5vbm9wZW4gPSBmdW5jdGlvbiAoKSB7XG4gICAgICBvbk9wZW4gJiYgb25PcGVuKCk7XG4gICAgfTtcblxuICAgIHRoaXMud2Vic29ja2V0Lm9ubWVzc2FnZSA9IGZ1bmN0aW9uIChtZ3MpIHtcbiAgICAgIHZhciBidWZmZXIgPSBuZXcgVWludDhBcnJheShtZ3MuZGF0YSk7XG4gICAgICB2YXIgYmVuVGF1TWVzc2FnZSA9IEJlblRhdU1lc3NhZ2UuZGVzZXJpYWxpemVCaW5hcnkoYnVmZmVyKTtcbiAgICAgIC8vIFBhcnNlIGRhdGFcbiAgICAgIG9uTWVzc2FnZSAmJiBvbk1lc3NhZ2UoYmVuVGF1TWVzc2FnZSk7XG4gICAgfTtcblxuICAgIHRoaXMud2Vic29ja2V0Lm9uY2xvc2UgPSBmdW5jdGlvbiAobWVzc2FnZTogQ2xvc2VFdmVudCkge1xuICAgICAgLy8gSGnhu4duIHThuqFpIGtow7RuZyBj4bqnbiB44butIGzDvSBjYXNlIG7DoHksIGNhc2UgbsOgeSBsw6AgaOG7hyB0aMO0bmcgdOG7sSDEkcOzbmcsIGPDsm4gY8OhYyB0csaw4budbmcgaOG7o3AgbWFudWFsIGNsb3NlIHRow6wgxJHDoyB44butIGzDvS5cbiAgICAgIC8vY2MubG9nKFwiU3lzdGVtIG9uQ2xvc2UgY29kZSBcIiArIHNlbGYuaXNFcnJvciArIFwiIFwiICsgbWVzc2FnZS5jb2RlICsgXCIgbWVzc2FnZSBcIiArIG1lc3NhZ2UucmVhc29uICsgXCIgXCIgKyBzZWxmLmlkV1MpO1xuICAgIH07XG5cbiAgICB0aGlzLndlYnNvY2tldC5vbmVycm9yID0gZnVuY3Rpb24gKG1lc3NhZ2U6IENsb3NlRXZlbnQpIHtcbiAgICAgIHNlbGYuaXNFcnJvciA9IHRydWU7XG4gICAgICBjb25zdCBjb2RlID0gbWVzc2FnZS5jb2RlO1xuICAgICAgY29uc3QgcmVhc29uID0gbWVzc2FnZS5yZWFzb247XG4gICAgICAvL2NjLmxvZyhcIm9uZXJyb3IgY29kZSBcIiArIG1lc3NhZ2UuY29kZSArIFwiIG1lc3NhZ2UgXCIgKyBtZXNzYWdlLnJlYXNvbiArIFwiIFwiICsgc2VsZi5pZFdTKTtcbiAgICAgIC8vIG9uRXJyb3IgJiYgb25FcnJvcihjb2RlLCByZWFzb24pO1xuICAgIH07XG4gIH1cblxuICBwdWJsaWMgc2VuZChkYXRhKSB7XG4gICAgaWYgKHRoaXMud2Vic29ja2V0LnJlYWR5U3RhdGUgPT0gV2ViU29ja2V0Lk9QRU4pIHRoaXMud2Vic29ja2V0LnNlbmQoZGF0YSk7XG4gIH1cblxuICBwdWJsaWMgcmVDb25uZWN0KGNvZGU6IG51bWJlciwgcmVhc29uOiBzdHJpbmcpIHtcbiAgICAvLyAvL2NjLmxvZyhcInJlQ29ubmVjdCA9IFwiICsgY29kZSArIFwiIHJlYXNvbiA9IFwiICsgcmVhc29uICsgXCIgaWRXUyBcIiArIHRoaXMuaWRXUyArIFwiIGlzT3BlbiBcIiArIHRoaXMud2Vic29ja2V0LnJlYWR5U3RhdGUpO1xuICAgIHRoaXMub25SZWNvbm5lY3QgJiYgdGhpcy5vblJlY29ubmVjdChjb2RlLCByZWFzb24pO1xuICB9XG5cbiAgcHVibGljIGNsb3NlV1MoY29kZTogbnVtYmVyLCByZWFzb246IHN0cmluZykge1xuICAgIC8vY2MubG9nKFwiY2xvc2VXUyA9IFwiICsgY29kZSArIFwiIHJlYXNvbiA9IFwiICsgcmVhc29uICsgXCIgaWRXUyBcIiArIHRoaXMuaWRXUyArIFwiIGlzT3BlbiBcIiArIHRoaXMud2Vic29ja2V0LnJlYWR5U3RhdGUpO1xuICAgIGlmICh0aGlzLndlYnNvY2tldCA9PSBudWxsIHx8IHRoaXMud2Vic29ja2V0LnJlYWR5U3RhdGUgPT0gV2ViU29ja2V0LkNMT1NFRCkgcmV0dXJuO1xuICAgIHRoaXMud2Vic29ja2V0LmNsb3NlKENvZGVTb2NrZXQuRk9SQ0VfQ0xPU0UpO1xuICB9XG59XG4iXX0=