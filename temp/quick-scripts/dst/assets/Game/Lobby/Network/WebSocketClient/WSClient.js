
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Network/WebSocketClient/WSClient.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4b1adCUjshI3rQ2kfSzOd8q', 'WSClient');
// Game/Lobby/Network/WebSocketClient/WSClient.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var WSClient_Android_1 = require("./WSClient_Android");
var WSClient_WebAndIOS_1 = require("./WSClient_WebAndIOS");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var WSClient = /** @class */ (function () {
    function WSClient() {
        this.ws_android_client = new WSClient_Android_1.default();
        this.ws_webios_client = new WSClient_WebAndIOS_1.default();
    }
    WSClient.prototype.open = function (address, onOpen, onMessage, onClose, onError, onReconnect) {
        cc.log("Open new WS");
        if (cc.sys.platform == cc.sys.ANDROID) {
            this.ws_android_client.open(address, onOpen, onMessage, onClose, onError, onReconnect);
        }
        else {
            this.ws_webios_client.open(address, onOpen, onMessage, onClose, onError, onReconnect);
        }
    };
    WSClient.prototype.send = function (data) {
        if (cc.sys.platform == cc.sys.ANDROID) {
            this.ws_android_client.send(data);
        }
        else {
            this.ws_webios_client.send(data);
        }
    };
    WSClient.prototype.close = function (code, reason) {
        cc.log("WSClient close ");
        if (cc.sys.platform == cc.sys.ANDROID) {
            this.ws_android_client.closeWS(code, reason);
        }
        else {
            this.ws_webios_client.closeWS(code, reason);
        }
    };
    WSClient.prototype.reConnect = function (code, reason) {
        cc.log("WSClient reConnect ");
        if (cc.sys.platform == cc.sys.ANDROID) {
            this.ws_android_client.reConnect(code, reason);
        }
        else {
            this.ws_webios_client.reConnect(code, reason);
        }
    };
    WSClient = __decorate([
        ccclass
    ], WSClient);
    return WSClient;
}());
exports.default = WSClient;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L05ldHdvcmsvV2ViU29ja2V0Q2xpZW50L1dTQ2xpZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7QUFFbEYsdURBQWtEO0FBQ2xELDJEQUFzRDtBQUVoRCxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFBO1FBQ1Msc0JBQWlCLEdBQXFCLElBQUksMEJBQWdCLEVBQUUsQ0FBQztRQUM3RCxxQkFBZ0IsR0FBdUIsSUFBSSw0QkFBa0IsRUFBRSxDQUFDO0lBbUN6RSxDQUFDO0lBbENRLHVCQUFJLEdBQVgsVUFBWSxPQUFPLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVc7UUFDbkUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN0QixJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQztTQUN4RjthQUFNO1lBQ0wsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1NBQ3ZGO0lBQ0gsQ0FBQztJQUVNLHVCQUFJLEdBQVgsVUFBWSxJQUFJO1FBQ2QsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRTtZQUNyQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ25DO2FBQU07WUFDTCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2xDO0lBQ0gsQ0FBQztJQUVNLHdCQUFLLEdBQVosVUFBYSxJQUFZLEVBQUUsTUFBYztRQUN2QyxFQUFFLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDMUIsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRTtZQUNyQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztTQUM5QzthQUFNO1lBQ0wsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDN0M7SUFDSCxDQUFDO0lBRU0sNEJBQVMsR0FBaEIsVUFBaUIsSUFBWSxFQUFFLE1BQWM7UUFDM0MsRUFBRSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzlCLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUU7WUFDckMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDaEQ7YUFBTTtZQUNMLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQy9DO0lBQ0gsQ0FBQztJQXBDa0IsUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQXFDNUI7SUFBRCxlQUFDO0NBckNELEFBcUNDLElBQUE7a0JBckNvQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBXU0NsaWVudF9BbmRyb2lkIGZyb20gXCIuL1dTQ2xpZW50X0FuZHJvaWRcIjtcbmltcG9ydCBXU0NsaWVudF9XZWJBbmRJT1MgZnJvbSBcIi4vV1NDbGllbnRfV2ViQW5kSU9TXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBXU0NsaWVudCB7XG4gIHB1YmxpYyB3c19hbmRyb2lkX2NsaWVudDogV1NDbGllbnRfQW5kcm9pZCA9IG5ldyBXU0NsaWVudF9BbmRyb2lkKCk7XG4gIHB1YmxpYyB3c193ZWJpb3NfY2xpZW50OiBXU0NsaWVudF9XZWJBbmRJT1MgPSBuZXcgV1NDbGllbnRfV2ViQW5kSU9TKCk7XG4gIHB1YmxpYyBvcGVuKGFkZHJlc3MsIG9uT3Blbiwgb25NZXNzYWdlLCBvbkNsb3NlLCBvbkVycm9yLCBvblJlY29ubmVjdCkge1xuICAgIGNjLmxvZyhcIk9wZW4gbmV3IFdTXCIpO1xuICAgIGlmIChjYy5zeXMucGxhdGZvcm0gPT0gY2Muc3lzLkFORFJPSUQpIHtcbiAgICAgIHRoaXMud3NfYW5kcm9pZF9jbGllbnQub3BlbihhZGRyZXNzLCBvbk9wZW4sIG9uTWVzc2FnZSwgb25DbG9zZSwgb25FcnJvciwgb25SZWNvbm5lY3QpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLndzX3dlYmlvc19jbGllbnQub3BlbihhZGRyZXNzLCBvbk9wZW4sIG9uTWVzc2FnZSwgb25DbG9zZSwgb25FcnJvciwgb25SZWNvbm5lY3QpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBzZW5kKGRhdGEpIHtcbiAgICBpZiAoY2Muc3lzLnBsYXRmb3JtID09IGNjLnN5cy5BTkRST0lEKSB7XG4gICAgICB0aGlzLndzX2FuZHJvaWRfY2xpZW50LnNlbmQoZGF0YSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMud3Nfd2ViaW9zX2NsaWVudC5zZW5kKGRhdGEpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBjbG9zZShjb2RlOiBudW1iZXIsIHJlYXNvbjogc3RyaW5nKSB7XG4gICAgY2MubG9nKFwiV1NDbGllbnQgY2xvc2UgXCIpO1xuICAgIGlmIChjYy5zeXMucGxhdGZvcm0gPT0gY2Muc3lzLkFORFJPSUQpIHtcbiAgICAgIHRoaXMud3NfYW5kcm9pZF9jbGllbnQuY2xvc2VXUyhjb2RlLCByZWFzb24pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLndzX3dlYmlvc19jbGllbnQuY2xvc2VXUyhjb2RlLCByZWFzb24pO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyByZUNvbm5lY3QoY29kZTogbnVtYmVyLCByZWFzb246IHN0cmluZykge1xuICAgIGNjLmxvZyhcIldTQ2xpZW50IHJlQ29ubmVjdCBcIik7XG4gICAgaWYgKGNjLnN5cy5wbGF0Zm9ybSA9PSBjYy5zeXMuQU5EUk9JRCkge1xuICAgICAgdGhpcy53c19hbmRyb2lkX2NsaWVudC5yZUNvbm5lY3QoY29kZSwgcmVhc29uKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy53c193ZWJpb3NfY2xpZW50LnJlQ29ubmVjdChjb2RlLCByZWFzb24pO1xuICAgIH1cbiAgfVxufVxuIl19