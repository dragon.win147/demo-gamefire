
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Network/WebSocketClient/WSClient_Android.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '635a5J16TxEbru5tJQ+1qFu', 'WSClient_Android');
// Game/Lobby/Utils/WebSocketClient/WSClient_Android.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var bentau_pb_1 = require("@gameloot/bentau/bentau_pb");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var WSClient_Android = /** @class */ (function (_super) {
    __extends(WSClient_Android, _super);
    function WSClient_Android() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.isOpen = false;
        _this.isError = false;
        return _this;
    }
    WSClient_Android.prototype.open = function (address, onOpen, onMessage, onClose, onError, onReconnect) {
        var _this = this;
        console.log("WSClient_Android::receive");
        this.onOpenCb = onOpen;
        this.onMessageCb = onMessage;
        this.onCloseCb = onClose;
        this.onErrorCb = onError;
        this.onReconnect = onReconnect;
        this.isOpen = true;
        this.isError = false;
        // Native.getInstance().call("createWebSocketClient", address, IdentityHandler.getInstance().token);
        cc.systemEvent.on("WSClient_Android_open", function () {
            _this.onOpenCb && _this.onOpenCb();
        });
        cc.systemEvent.on("WSClient_Android_message", function (base64_string) {
            var data = Uint8Array.from(atob(base64_string), function (c) { return c.charCodeAt(0); });
            var benTauMessage = bentau_pb_1.BenTauMessage.deserializeBinary(data);
            _this.onMessageCb && _this.onMessageCb(benTauMessage);
        });
        cc.systemEvent.on("WSClient_Android_close", function (code, reason) {
            // Hiện tại không cần xử lý case này, case này là hệ thông tự đóng, còn các trường hợp manual close thì đã xử lý.
            cc.log("WSClient_Android_close " + code + " " + reason);
            _this.isOpen = false;
            _this.offEvent();
        });
        cc.systemEvent.on("WSClient_Android_error", function (msg) {
            cc.log("WSClient_Android_error " + msg);
            _this.offEvent();
            _this.isError = false;
            // this.onErrorCb && this.onErrorCb(msg);
        });
    };
    WSClient_Android.prototype.send = function (data) {
        cc.log("WSClient_Android send");
        var base64_string = btoa(String.fromCharCode.apply(null, new Uint8Array(data)));
        jsb.reflection.callStaticMethod("org/cocos2dx/javascript/NativeWebSocketClient", "send", "(Ljava/lang/String;)V", base64_string);
    };
    WSClient_Android.prototype.closeWS = function (code, reason) {
        this.offEvent();
        if (!this.isOpen)
            return;
        this.isOpen = false;
        this.onCloseCb && this.onCloseCb(code, reason);
        jsb.reflection.callStaticMethod("org/cocos2dx/javascript/NativeWebSocketClient", "close", "()V");
    };
    WSClient_Android.prototype.reConnect = function (code, reason) {
        // cc.log("reConnect = " + code + " reason = " + reason + " idWS " + this.idWS + " isOpen " + this.websocket.readyState);
        this.onReconnect && this.onReconnect(code, reason);
    };
    WSClient_Android.prototype.offEvent = function () {
        cc.systemEvent.off("WSClient_Android_open");
        cc.systemEvent.off("WSClient_Android_message");
        cc.systemEvent.off("WSClient_Android_close");
        cc.systemEvent.off("WSClient_Android_error");
    };
    WSClient_Android = __decorate([
        ccclass
    ], WSClient_Android);
    return WSClient_Android;
}(cc.Component));
exports.default = WSClient_Android;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L1V0aWxzL1dlYlNvY2tldENsaWVudC9XU0NsaWVudF9BbmRyb2lkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLHdEQUEyRDtBQUVyRCxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUE4QyxvQ0FBWTtJQUExRDtRQUFBLHFFQW1FQztRQTdEUyxZQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2YsYUFBTyxHQUFHLEtBQUssQ0FBQzs7SUE0RDFCLENBQUM7SUEzRFEsK0JBQUksR0FBWCxVQUFZLE9BQU8sRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVztRQUFyRSxpQkFpQ0M7UUFoQ0MsT0FBTyxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO1FBQzdCLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLG9HQUFvRztRQUNwRyxFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyx1QkFBdUIsRUFBRTtZQUN6QyxLQUFJLENBQUMsUUFBUSxJQUFJLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNuQyxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLDBCQUEwQixFQUFFLFVBQUMsYUFBYTtZQUMxRCxJQUFJLElBQUksR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQWYsQ0FBZSxDQUFDLENBQUM7WUFDeEUsSUFBSSxhQUFhLEdBQUcseUJBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxRCxLQUFJLENBQUMsV0FBVyxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyx3QkFBd0IsRUFBRSxVQUFDLElBQUksRUFBRSxNQUFNO1lBQ3ZELGlIQUFpSDtZQUNqSCxFQUFFLENBQUMsR0FBRyxDQUFDLHlCQUF5QixHQUFHLElBQUksR0FBRyxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUM7WUFDeEQsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEIsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsd0JBQXdCLEVBQUUsVUFBQyxHQUFHO1lBQzlDLEVBQUUsQ0FBQyxHQUFHLENBQUMseUJBQXlCLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDeEMsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLHlDQUF5QztRQUMzQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDTSwrQkFBSSxHQUFYLFVBQVksSUFBSTtRQUNkLEVBQUUsQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUNoQyxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoRixHQUFHLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLCtDQUErQyxFQUFFLE1BQU0sRUFBRSx1QkFBdUIsRUFBRSxhQUFhLENBQUMsQ0FBQztJQUNuSSxDQUFDO0lBRU0sa0NBQU8sR0FBZCxVQUFlLElBQVksRUFBRSxNQUFjO1FBQ3pDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU07WUFBRSxPQUFPO1FBQ3pCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDL0MsR0FBRyxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQywrQ0FBK0MsRUFBRSxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDbkcsQ0FBQztJQUVNLG9DQUFTLEdBQWhCLFVBQWlCLElBQVksRUFBRSxNQUFjO1FBQzNDLHlIQUF5SDtRQUN6SCxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFTSxtQ0FBUSxHQUFmO1FBQ0UsRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUM1QyxFQUFFLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1FBQy9DLEVBQUUsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDN0MsRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBbEVrQixnQkFBZ0I7UUFEcEMsT0FBTztPQUNhLGdCQUFnQixDQW1FcEM7SUFBRCx1QkFBQztDQW5FRCxBQW1FQyxDQW5FNkMsRUFBRSxDQUFDLFNBQVMsR0FtRXpEO2tCQW5Fb0IsZ0JBQWdCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IElkZW50aXR5SGFuZGxlciBmcm9tIFwiLi4vLi4vTmV0d29yay9JZGVudGl0eUhhbmRsZXJcIjtcbmltcG9ydCB7IEJlblRhdU1lc3NhZ2UgfSBmcm9tIFwiQGdhbWVsb290L2JlbnRhdS9iZW50YXVfcGJcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFdTQ2xpZW50X0FuZHJvaWQgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICBwcml2YXRlIG9uT3BlbkNiOiBGdW5jdGlvbjtcbiAgcHJpdmF0ZSBvbk1lc3NhZ2VDYjogRnVuY3Rpb247XG4gIHByaXZhdGUgb25DbG9zZUNiOiBGdW5jdGlvbjtcbiAgcHJpdmF0ZSBvbkVycm9yQ2I6IEZ1bmN0aW9uO1xuICBwcml2YXRlIG9uUmVjb25uZWN0OiBGdW5jdGlvbjtcbiAgcHJpdmF0ZSBpc09wZW4gPSBmYWxzZTtcbiAgcHJpdmF0ZSBpc0Vycm9yID0gZmFsc2U7XG4gIHB1YmxpYyBvcGVuKGFkZHJlc3MsIG9uT3Blbiwgb25NZXNzYWdlLCBvbkNsb3NlLCBvbkVycm9yLCBvblJlY29ubmVjdCkge1xuICAgIGNvbnNvbGUubG9nKFwiV1NDbGllbnRfQW5kcm9pZDo6cmVjZWl2ZVwiKTtcbiAgICB0aGlzLm9uT3BlbkNiID0gb25PcGVuO1xuICAgIHRoaXMub25NZXNzYWdlQ2IgPSBvbk1lc3NhZ2U7XG4gICAgdGhpcy5vbkNsb3NlQ2IgPSBvbkNsb3NlO1xuICAgIHRoaXMub25FcnJvckNiID0gb25FcnJvcjtcbiAgICB0aGlzLm9uUmVjb25uZWN0ID0gb25SZWNvbm5lY3Q7XG4gICAgdGhpcy5pc09wZW4gPSB0cnVlO1xuICAgIHRoaXMuaXNFcnJvciA9IGZhbHNlO1xuICAgIC8vIE5hdGl2ZS5nZXRJbnN0YW5jZSgpLmNhbGwoXCJjcmVhdGVXZWJTb2NrZXRDbGllbnRcIiwgYWRkcmVzcywgSWRlbnRpdHlIYW5kbGVyLmdldEluc3RhbmNlKCkudG9rZW4pO1xuICAgIGNjLnN5c3RlbUV2ZW50Lm9uKFwiV1NDbGllbnRfQW5kcm9pZF9vcGVuXCIsICgpID0+IHtcbiAgICAgIHRoaXMub25PcGVuQ2IgJiYgdGhpcy5vbk9wZW5DYigpO1xuICAgIH0pO1xuXG4gICAgY2Muc3lzdGVtRXZlbnQub24oXCJXU0NsaWVudF9BbmRyb2lkX21lc3NhZ2VcIiwgKGJhc2U2NF9zdHJpbmcpID0+IHtcbiAgICAgIHZhciBkYXRhID0gVWludDhBcnJheS5mcm9tKGF0b2IoYmFzZTY0X3N0cmluZyksIChjKSA9PiBjLmNoYXJDb2RlQXQoMCkpO1xuICAgICAgdmFyIGJlblRhdU1lc3NhZ2UgPSBCZW5UYXVNZXNzYWdlLmRlc2VyaWFsaXplQmluYXJ5KGRhdGEpO1xuICAgICAgdGhpcy5vbk1lc3NhZ2VDYiAmJiB0aGlzLm9uTWVzc2FnZUNiKGJlblRhdU1lc3NhZ2UpO1xuICAgIH0pO1xuXG4gICAgY2Muc3lzdGVtRXZlbnQub24oXCJXU0NsaWVudF9BbmRyb2lkX2Nsb3NlXCIsIChjb2RlLCByZWFzb24pID0+IHtcbiAgICAgIC8vIEhp4buHbiB04bqhaSBraMO0bmcgY+G6p24geOG7rSBsw70gY2FzZSBuw6B5LCBjYXNlIG7DoHkgbMOgIGjhu4cgdGjDtG5nIHThu7EgxJHDs25nLCBjw7JuIGPDoWMgdHLGsOG7nW5nIGjhu6NwIG1hbnVhbCBjbG9zZSB0aMOsIMSRw6MgeOG7rSBsw70uXG4gICAgICBjYy5sb2coXCJXU0NsaWVudF9BbmRyb2lkX2Nsb3NlIFwiICsgY29kZSArIFwiIFwiICsgcmVhc29uKTtcbiAgICAgIHRoaXMuaXNPcGVuID0gZmFsc2U7XG4gICAgICB0aGlzLm9mZkV2ZW50KCk7XG4gICAgfSk7XG5cbiAgICBjYy5zeXN0ZW1FdmVudC5vbihcIldTQ2xpZW50X0FuZHJvaWRfZXJyb3JcIiwgKG1zZykgPT4ge1xuICAgICAgY2MubG9nKFwiV1NDbGllbnRfQW5kcm9pZF9lcnJvciBcIiArIG1zZyk7XG4gICAgICB0aGlzLm9mZkV2ZW50KCk7XG4gICAgICB0aGlzLmlzRXJyb3IgPSBmYWxzZTtcbiAgICAgIC8vIHRoaXMub25FcnJvckNiICYmIHRoaXMub25FcnJvckNiKG1zZyk7XG4gICAgfSk7XG4gIH1cbiAgcHVibGljIHNlbmQoZGF0YSkge1xuICAgIGNjLmxvZyhcIldTQ2xpZW50X0FuZHJvaWQgc2VuZFwiKTtcbiAgICB2YXIgYmFzZTY0X3N0cmluZyA9IGJ0b2EoU3RyaW5nLmZyb21DaGFyQ29kZS5hcHBseShudWxsLCBuZXcgVWludDhBcnJheShkYXRhKSkpO1xuICAgIGpzYi5yZWZsZWN0aW9uLmNhbGxTdGF0aWNNZXRob2QoXCJvcmcvY29jb3MyZHgvamF2YXNjcmlwdC9OYXRpdmVXZWJTb2NrZXRDbGllbnRcIiwgXCJzZW5kXCIsIFwiKExqYXZhL2xhbmcvU3RyaW5nOylWXCIsIGJhc2U2NF9zdHJpbmcpO1xuICB9XG5cbiAgcHVibGljIGNsb3NlV1MoY29kZTogbnVtYmVyLCByZWFzb246IHN0cmluZykge1xuICAgIHRoaXMub2ZmRXZlbnQoKTtcbiAgICBpZiAoIXRoaXMuaXNPcGVuKSByZXR1cm47XG4gICAgdGhpcy5pc09wZW4gPSBmYWxzZTtcbiAgICB0aGlzLm9uQ2xvc2VDYiAmJiB0aGlzLm9uQ2xvc2VDYihjb2RlLCByZWFzb24pO1xuICAgIGpzYi5yZWZsZWN0aW9uLmNhbGxTdGF0aWNNZXRob2QoXCJvcmcvY29jb3MyZHgvamF2YXNjcmlwdC9OYXRpdmVXZWJTb2NrZXRDbGllbnRcIiwgXCJjbG9zZVwiLCBcIigpVlwiKTtcbiAgfVxuXG4gIHB1YmxpYyByZUNvbm5lY3QoY29kZTogbnVtYmVyLCByZWFzb246IHN0cmluZykge1xuICAgIC8vIGNjLmxvZyhcInJlQ29ubmVjdCA9IFwiICsgY29kZSArIFwiIHJlYXNvbiA9IFwiICsgcmVhc29uICsgXCIgaWRXUyBcIiArIHRoaXMuaWRXUyArIFwiIGlzT3BlbiBcIiArIHRoaXMud2Vic29ja2V0LnJlYWR5U3RhdGUpO1xuICAgIHRoaXMub25SZWNvbm5lY3QgJiYgdGhpcy5vblJlY29ubmVjdChjb2RlLCByZWFzb24pO1xuICB9XG5cbiAgcHVibGljIG9mZkV2ZW50KCkge1xuICAgIGNjLnN5c3RlbUV2ZW50Lm9mZihcIldTQ2xpZW50X0FuZHJvaWRfb3BlblwiKTtcbiAgICBjYy5zeXN0ZW1FdmVudC5vZmYoXCJXU0NsaWVudF9BbmRyb2lkX21lc3NhZ2VcIik7XG4gICAgY2Muc3lzdGVtRXZlbnQub2ZmKFwiV1NDbGllbnRfQW5kcm9pZF9jbG9zZVwiKTtcbiAgICBjYy5zeXN0ZW1FdmVudC5vZmYoXCJXU0NsaWVudF9BbmRyb2lkX2Vycm9yXCIpO1xuICB9XG59XG4iXX0=