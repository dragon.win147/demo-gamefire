
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Network/StabilizeConnection.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c3b1dBt+lRCVqiRYWgQe6dy', 'StabilizeConnection');
// Game/Lobby/Network/StabilizeConnection.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var bentau_pb_1 = require("@gameloot/bentau/bentau_pb");
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var GameUtils_1 = require("../Utils/GameUtils");
var TimeUtils_1 = require("../Utils/TimeUtils");
var EventManager_1 = require("../Event/EventManager");
var BenTauHandler_1 = require("./BenTauHandler");
var EventBusManager_1 = require("../Event/EventBusManager");
var StabilizeConnection = /** @class */ (function () {
    function StabilizeConnection() {
    }
    StabilizeConnection.update = function (dt) {
        if (!this.start)
            return;
        if (!GameUtils_1.default.isStatusPing)
            return;
        this.currentTime += dt;
        if (this.currentTime > this.TIME_TO_PONG * 1.5) {
            this.currentTime = 0;
            StabilizeConnection.onPingFailure(this.currentTime);
        }
    };
    StabilizeConnection.send = function () {
        var _this = this;
        this.clear();
        this.start = true;
        this.pingDict.clear();
        var ping = new bentau_pb_1.Ping();
        ping.setTopic(GameUtils_1.default.gameTopic);
        var msgId = BenTauHandler_1.default.getInstance().send(topic_pb_1.Topic.PING, "", ping.serializeBinary());
        if (msgId == null) {
            this.continue();
            return;
        }
        BenTauHandler_1.default.getInstance().onSubscribe(topic_pb_1.Topic.PONG, function (msg) {
            _this.onReceive(msg.getMsgId());
        });
        var time = TimeUtils_1.default.getCurrentTimeByMillisecond();
        if (msgId != null) {
            this.pingDict.set(msgId, time);
        }
    };
    StabilizeConnection.onReceive = function (msgId) {
        if (this.pingDict.has(msgId)) {
            var preTimePing = this.pingDict.get(msgId);
            var pingTime = TimeUtils_1.default.getCurrentTimeByMillisecond() - preTimePing;
            this.pingDict.delete(msgId);
            // if (GameUtils.isLogVersion()) {
            //   CheatManager.getInstance().open(PingCheat, "PingCheat", pingTime);
            // }
            EventBusManager_1.default.onPingTime(pingTime);
            this.onPingOk(pingTime);
        }
    };
    StabilizeConnection.clear = function () {
        GameUtils_1.default.isStatusPing = true;
        BenTauHandler_1.default.getInstance().onUnSubscribe(topic_pb_1.Topic.PONG);
        clearTimeout(this.ping);
        this.ping = null;
        this.start = false;
        this.currentTime = 0;
    };
    StabilizeConnection.continue = function () {
        var _this = this;
        if (!GameUtils_1.default.isStatusPing)
            return;
        this.ping = setTimeout(function () { return _this.send(); }, this.TIME_TO_PING);
    };
    StabilizeConnection.onPingOk = function (pingTime) {
        this.continue();
        GameUtils_1.default.isStatusPing = false;
        EventManager_1.default.fire(EventManager_1.default.ON_PING_OK);
    };
    StabilizeConnection.onPingFailure = function (pingTime) {
        cc.log("PING FAILED:" + pingTime + "ms");
        GameUtils_1.default.isStatusPing = false;
        EventManager_1.default.fire(EventManager_1.default.ON_PING_FAIL);
    };
    StabilizeConnection.setTimeOutMsg = function () {
        this.currentTime = 0;
    };
    StabilizeConnection.TIME_TO_PING = 5000;
    StabilizeConnection.TIME_TO_PONG = 3000;
    StabilizeConnection.pingDict = new Map();
    StabilizeConnection.ping = null;
    StabilizeConnection.currentTime = 0;
    StabilizeConnection.start = false;
    return StabilizeConnection;
}());
exports.default = StabilizeConnection;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L05ldHdvcmsvU3RhYmlsaXplQ29ubmVjdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHdEQUFrRDtBQUNsRCxxREFBaUQ7QUFDakQsZ0RBQTJDO0FBQzNDLGdEQUEyQztBQUMzQyxzREFBaUQ7QUFDakQsaURBQTRDO0FBQzVDLDREQUF1RDtBQUN2RDtJQUFBO0lBc0ZBLENBQUM7SUE1RWUsMEJBQU0sR0FBcEIsVUFBcUIsRUFBRTtRQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUs7WUFBRSxPQUFPO1FBQ3hCLElBQUksQ0FBQyxtQkFBUyxDQUFDLFlBQVk7WUFBRSxPQUFPO1FBQ3BDLElBQUksQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFDO1FBQ3ZCLElBQUksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLEdBQUcsRUFBRTtZQUM5QyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztZQUNyQixtQkFBbUIsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3JEO0lBQ0gsQ0FBQztJQUVhLHdCQUFJLEdBQWxCO1FBQUEsaUJBb0JDO1FBbkJDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNiLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFdEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxnQkFBSSxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxtQkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ25DLElBQUksS0FBSyxHQUFHLHVCQUFhLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFLLENBQUMsSUFBSSxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztRQUNyRixJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDakIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLE9BQU87U0FDUjtRQUVELHVCQUFhLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLGdCQUFLLENBQUMsSUFBSSxFQUFFLFVBQUMsR0FBRztZQUN0RCxLQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxJQUFJLEdBQUcsbUJBQVMsQ0FBQywyQkFBMkIsRUFBRSxDQUFDO1FBQ25ELElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtZQUNqQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDaEM7SUFDSCxDQUFDO0lBRWMsNkJBQVMsR0FBeEIsVUFBeUIsS0FBYTtRQUNwQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzVCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzNDLElBQUksUUFBUSxHQUFHLG1CQUFTLENBQUMsMkJBQTJCLEVBQUUsR0FBRyxXQUFXLENBQUM7WUFDckUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFNUIsa0NBQWtDO1lBQ2xDLHVFQUF1RTtZQUN2RSxJQUFJO1lBRUoseUJBQWUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN6QjtJQUNILENBQUM7SUFFYSx5QkFBSyxHQUFuQjtRQUNFLG1CQUFTLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUM5Qix1QkFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLGFBQWEsQ0FBQyxnQkFBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RELFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUVjLDRCQUFRLEdBQXZCO1FBQUEsaUJBR0M7UUFGQyxJQUFJLENBQUMsbUJBQVMsQ0FBQyxZQUFZO1lBQUUsT0FBTztRQUNwQyxJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLElBQUksRUFBRSxFQUFYLENBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVjLDRCQUFRLEdBQXZCLFVBQXdCLFFBQWdCO1FBQ3RDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixtQkFBUyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDL0Isc0JBQVksQ0FBQyxJQUFJLENBQUMsc0JBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRWMsaUNBQWEsR0FBNUIsVUFBNkIsUUFBZ0I7UUFDM0MsRUFBRSxDQUFDLEdBQUcsQ0FBQyxjQUFjLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDO1FBQ3pDLG1CQUFTLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMvQixzQkFBWSxDQUFDLElBQUksQ0FBQyxzQkFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFYSxpQ0FBYSxHQUEzQjtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFwRmEsZ0NBQVksR0FBRyxJQUFJLENBQUM7SUFDcEIsZ0NBQVksR0FBRyxJQUFJLENBQUM7SUFFbkIsNEJBQVEsR0FBd0IsSUFBSSxHQUFHLEVBQWtCLENBQUM7SUFFMUQsd0JBQUksR0FBRyxJQUFJLENBQUM7SUFDWiwrQkFBVyxHQUFHLENBQUMsQ0FBQztJQUNoQix5QkFBSyxHQUFZLEtBQUssQ0FBQztJQThFeEMsMEJBQUM7Q0F0RkQsQUFzRkMsSUFBQTtrQkF0Rm9CLG1CQUFtQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpbmcgfSBmcm9tIFwiQGdhbWVsb290L2JlbnRhdS9iZW50YXVfcGJcIjtcbmltcG9ydCB7IFRvcGljIH0gZnJvbSBcIkBnYW1lbG9vdC90b3BpYy90b3BpY19wYlwiO1xuaW1wb3J0IEdhbWVVdGlscyBmcm9tIFwiLi4vVXRpbHMvR2FtZVV0aWxzXCI7XG5pbXBvcnQgVGltZVV0aWxzIGZyb20gXCIuLi9VdGlscy9UaW1lVXRpbHNcIjtcbmltcG9ydCBFdmVudE1hbmFnZXIgZnJvbSBcIi4uL0V2ZW50L0V2ZW50TWFuYWdlclwiO1xuaW1wb3J0IEJlblRhdUhhbmRsZXIgZnJvbSBcIi4vQmVuVGF1SGFuZGxlclwiO1xuaW1wb3J0IEV2ZW50QnVzTWFuYWdlciBmcm9tIFwiLi4vRXZlbnQvRXZlbnRCdXNNYW5hZ2VyXCI7XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTdGFiaWxpemVDb25uZWN0aW9uIHtcbiAgcHVibGljIHN0YXRpYyBUSU1FX1RPX1BJTkcgPSA1MDAwO1xuICBwdWJsaWMgc3RhdGljIFRJTUVfVE9fUE9ORyA9IDMwMDA7XG5cbiAgcHJpdmF0ZSBzdGF0aWMgcGluZ0RpY3Q6IE1hcDxzdHJpbmcsIG51bWJlcj4gPSBuZXcgTWFwPHN0cmluZywgbnVtYmVyPigpO1xuXG4gIHByaXZhdGUgc3RhdGljIHBpbmcgPSBudWxsO1xuICBwcml2YXRlIHN0YXRpYyBjdXJyZW50VGltZSA9IDA7XG4gIHByaXZhdGUgc3RhdGljIHN0YXJ0OiBib29sZWFuID0gZmFsc2U7XG5cbiAgcHVibGljIHN0YXRpYyB1cGRhdGUoZHQpIHtcbiAgICBpZiAoIXRoaXMuc3RhcnQpIHJldHVybjtcbiAgICBpZiAoIUdhbWVVdGlscy5pc1N0YXR1c1BpbmcpIHJldHVybjtcbiAgICB0aGlzLmN1cnJlbnRUaW1lICs9IGR0O1xuICAgIGlmICh0aGlzLmN1cnJlbnRUaW1lID4gdGhpcy5USU1FX1RPX1BPTkcgKiAxLjUpIHtcbiAgICAgIHRoaXMuY3VycmVudFRpbWUgPSAwO1xuICAgICAgU3RhYmlsaXplQ29ubmVjdGlvbi5vblBpbmdGYWlsdXJlKHRoaXMuY3VycmVudFRpbWUpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgc2VuZCgpIHtcbiAgICB0aGlzLmNsZWFyKCk7XG4gICAgdGhpcy5zdGFydCA9IHRydWU7XG4gICAgdGhpcy5waW5nRGljdC5jbGVhcigpO1xuXG4gICAgbGV0IHBpbmcgPSBuZXcgUGluZygpO1xuICAgIHBpbmcuc2V0VG9waWMoR2FtZVV0aWxzLmdhbWVUb3BpYyk7XG4gICAgbGV0IG1zZ0lkID0gQmVuVGF1SGFuZGxlci5nZXRJbnN0YW5jZSgpLnNlbmQoVG9waWMuUElORywgXCJcIiwgcGluZy5zZXJpYWxpemVCaW5hcnkoKSk7XG4gICAgaWYgKG1zZ0lkID09IG51bGwpIHtcbiAgICAgIHRoaXMuY29udGludWUoKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBCZW5UYXVIYW5kbGVyLmdldEluc3RhbmNlKCkub25TdWJzY3JpYmUoVG9waWMuUE9ORywgKG1zZykgPT4ge1xuICAgICAgdGhpcy5vblJlY2VpdmUobXNnLmdldE1zZ0lkKCkpO1xuICAgIH0pO1xuICAgIGxldCB0aW1lID0gVGltZVV0aWxzLmdldEN1cnJlbnRUaW1lQnlNaWxsaXNlY29uZCgpO1xuICAgIGlmIChtc2dJZCAhPSBudWxsKSB7XG4gICAgICB0aGlzLnBpbmdEaWN0LnNldChtc2dJZCwgdGltZSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgb25SZWNlaXZlKG1zZ0lkOiBzdHJpbmcpIHtcbiAgICBpZiAodGhpcy5waW5nRGljdC5oYXMobXNnSWQpKSB7XG4gICAgICBsZXQgcHJlVGltZVBpbmcgPSB0aGlzLnBpbmdEaWN0LmdldChtc2dJZCk7XG4gICAgICBsZXQgcGluZ1RpbWUgPSBUaW1lVXRpbHMuZ2V0Q3VycmVudFRpbWVCeU1pbGxpc2Vjb25kKCkgLSBwcmVUaW1lUGluZztcbiAgICAgIHRoaXMucGluZ0RpY3QuZGVsZXRlKG1zZ0lkKTtcblxuICAgICAgLy8gaWYgKEdhbWVVdGlscy5pc0xvZ1ZlcnNpb24oKSkge1xuICAgICAgLy8gICBDaGVhdE1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5vcGVuKFBpbmdDaGVhdCwgXCJQaW5nQ2hlYXRcIiwgcGluZ1RpbWUpO1xuICAgICAgLy8gfVxuXG4gICAgICBFdmVudEJ1c01hbmFnZXIub25QaW5nVGltZShwaW5nVGltZSk7XG4gICAgICB0aGlzLm9uUGluZ09rKHBpbmdUaW1lKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGNsZWFyKCkge1xuICAgIEdhbWVVdGlscy5pc1N0YXR1c1BpbmcgPSB0cnVlO1xuICAgIEJlblRhdUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS5vblVuU3Vic2NyaWJlKFRvcGljLlBPTkcpO1xuICAgIGNsZWFyVGltZW91dCh0aGlzLnBpbmcpO1xuICAgIHRoaXMucGluZyA9IG51bGw7XG4gICAgdGhpcy5zdGFydCA9IGZhbHNlO1xuICAgIHRoaXMuY3VycmVudFRpbWUgPSAwO1xuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgY29udGludWUoKSB7XG4gICAgaWYgKCFHYW1lVXRpbHMuaXNTdGF0dXNQaW5nKSByZXR1cm47XG4gICAgdGhpcy5waW5nID0gc2V0VGltZW91dCgoKSA9PiB0aGlzLnNlbmQoKSwgdGhpcy5USU1FX1RPX1BJTkcpO1xuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgb25QaW5nT2socGluZ1RpbWU6IG51bWJlcikge1xuICAgIHRoaXMuY29udGludWUoKTtcbiAgICBHYW1lVXRpbHMuaXNTdGF0dXNQaW5nID0gZmFsc2U7XG4gICAgRXZlbnRNYW5hZ2VyLmZpcmUoRXZlbnRNYW5hZ2VyLk9OX1BJTkdfT0spO1xuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgb25QaW5nRmFpbHVyZShwaW5nVGltZTogbnVtYmVyKSB7XG4gICAgY2MubG9nKFwiUElORyBGQUlMRUQ6XCIgKyBwaW5nVGltZSArIFwibXNcIik7XG4gICAgR2FtZVV0aWxzLmlzU3RhdHVzUGluZyA9IGZhbHNlO1xuICAgIEV2ZW50TWFuYWdlci5maXJlKEV2ZW50TWFuYWdlci5PTl9QSU5HX0ZBSUwpO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBzZXRUaW1lT3V0TXNnKCkge1xuICAgIHRoaXMuY3VycmVudFRpbWUgPSAwO1xuICB9XG59XG4iXX0=