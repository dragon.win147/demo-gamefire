
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Network/BenTauHandler.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '777f0erKmtKIbA3Cqhh1ePA', 'BenTauHandler');
// Game/Lobby/Network/BenTauHandler.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BenTauHandler = /** @class */ (function () {
    function BenTauHandler() {
        this.client = null;
        this._initComplete = null;
        this._timerIntervalUpdate = null;
        this._start = false;
        this.subscribes = new Map();
        this.msgsSend = new Map();
        this.keydefault = "keydefault";
        this.countReconnect = 0;
        this.MAX_COUNT_RECONNECT = 3;
    }
    BenTauHandler.getInstance = function () {
        if (!BenTauHandler.instance) {
            BenTauHandler.instance = new BenTauHandler();
            BenTauHandler.instance.handUpdateTimeOut();
        }
        return BenTauHandler.instance;
    };
    BenTauHandler.prototype.openLoading = function (callback) {
        UIManager_1.default.getInstance()
            .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, false, null, null)
            .then(function () {
            callback();
        });
    };
    BenTauHandler.prototype.closeLoading = function () {
        UIManager_1.default.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
    };
    BenTauHandler.prototype.openRetry = function () {
        EventManager_1.default.fire(EventManager_1.default.ON_SHOW_RETRY_POPUP);
    };
    BenTauHandler.destroy = function () {
        //cc.log("destroy");
        var self = BenTauHandler.instance;
        if (!(self === null || self === void 0 ? void 0 : self.client))
            return;
        self.client.close(EnumGame_1.CodeSocket.FORCE_CLOSE, "");
        clearInterval(self._timerIntervalUpdate);
        BenTauHandler.instance._start = false;
        BenTauHandler.instance = null;
    };
    BenTauHandler.prototype.onSubscribe = function (topic, onReceive, key) {
        if (key === void 0) { key = ""; }
        if (key == "") {
            key = this.keydefault;
        }
        if (this.subscribes.has(topic)) {
            var e = this.subscribes.get(topic);
            e.set(key, onReceive);
            this.subscribes.set(topic, e);
        }
        else {
            var e = new Map();
            e.set(key, onReceive);
            this.subscribes.set(topic, e);
        }
    };
    BenTauHandler.prototype.onUnSubscribe = function (topic, key) {
        if (key === void 0) { key = ""; }
        if (key == "") {
            key = this.keydefault;
        }
        if (this.subscribes.has(topic)) {
            var e = this.subscribes.get(topic);
            if (e.has(key)) {
                e.delete(key);
                this.subscribes.set(topic, e);
            }
        }
    };
    BenTauHandler.prototype.init = function (onComplete) {
        var _this = this;
        if (onComplete === void 0) { onComplete = null; }
        //cc.log("init betau WS");
        var self = BenTauHandler.instance;
        self._initComplete = onComplete;
        this.openLoading(function () {
            //cc.log("Connecting to BenTau");
            if (!(self === null || self === void 0 ? void 0 : self.client)) {
                self.client = new WSClient_1.default();
            }
            self.client.open(ConnectDefine_1.default.addressBenTauWS, self.handleOpen.bind(_this), self.onMessage.bind(_this), self.onClose.bind(_this), self.onError.bind(_this), self.onReconnect.bind(_this));
        });
    };
    BenTauHandler.prototype.openNewWebsocket = function () {
        // console.log("openNewWebsocket");
        var self = BenTauHandler.instance;
        if (!(self === null || self === void 0 ? void 0 : self.client)) {
            self.client = new WSClient_1.default();
        }
        self.client.open(ConnectDefine_1.default.addressBenTauWS, self.handleReconnected.bind(this), self.onMessage.bind(this), self.onClose.bind(this), self.onError.bind(this), self.onReconnect.bind(this));
    };
    BenTauHandler.prototype.cleanConnection = function () { };
    BenTauHandler.prototype.doReconnect = function (onReconnectingFunc, failedReconnectFunc) {
        var _this = this;
        if (onReconnectingFunc === void 0) { onReconnectingFunc = null; }
        if (failedReconnectFunc === void 0) { failedReconnectFunc = null; }
        //cc.log("Reconnecting with api verifyTokenReconnect ");
        // onReconnectingFunc();
        this.countReconnect++;
        IdentityHandler_1.default.getInstance().verifyTokenReconnect(function () {
            BenTauHandler.instance.openNewWebsocket();
        }, function (msgError, code) {
            cc.log("msgError = " + msgError);
            cc.log("code = " + code);
            //cc.log("countReconnect = " + this.countReconnect + " " + this.MAX_COUNT_RECONNECT);
            if (code == code_pb_1.Code.UNKNOWN) {
                // failedReconnectFunc(); // reconnect lại
                if (_this.countReconnect < _this.MAX_COUNT_RECONNECT) {
                    _this.doReconnect();
                }
                else {
                    _this.countReconnect = 0;
                    _this.closeLoading();
                    _this.openRetry();
                }
            }
            else {
                var messError = PortalText_1.default.POR_MESS_ACCOUNT_LOGIN_OTHER_DEVICE;
                EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, EnumGame_1.CodeSocket.KOTIC, false, messError);
            }
        });
    };
    BenTauHandler.prototype.send = function (topic, channelId, payload, reply, onError, loadingBg, loading) {
        var _this = this;
        var _a;
        if (reply === void 0) { reply = null; }
        if (onError === void 0) { onError = null; }
        if (loadingBg === void 0) { loadingBg = true; }
        if (loading === void 0) { loading = true; }
        if (!((_a = BenTauHandler.instance) === null || _a === void 0 ? void 0 : _a.client))
            return null;
        loading = reply != null && loading;
        var request = new bentau_pb_1.BenTauMessage();
        request.setTopic(topic);
        request.setChannelId(channelId);
        var msgId = BenTauHandler.instance.getUniqueId();
        request.setMsgId(msgId);
        // Put  to BenTauMessage
        request.setPayload(payload);
        var sendRequest = function () {
            ////cc.log("sendRequest.........", msgId);
            BenTauHandler.instance.client.send(request.serializeBinary());
        };
        var msg = new build_1.RequestMessage();
        msg.msgId = msgId;
        msg.request = sendRequest;
        msg.response = reply;
        msg.onError = onError;
        msg.onTimeOut = function (msgIdTimeOut) { return _this.onMsgTimeOut(msgIdTimeOut); };
        msg.loading = loading;
        msg.loadingBg = loadingBg;
        this.sendRequest(msg);
        return msgId;
    };
    BenTauHandler.prototype.sendRequest = function (msg) {
        //chỉ những msg nào chờ response mới cache lại
        if (msg.response != null)
            this.msgsSend.set(msg.msgId, msg);
        if (msg.loading) {
            this.openLoading(function () {
                msg.request();
            });
        }
        else {
            msg.request();
        }
    };
    BenTauHandler.prototype.onReceiveMsgReply = function (msgReply) {
        var status = msgReply.getStatus();
        if (status) {
            var code = status.getCode();
            var err = status.getMessage();
            var msgId = msgReply.getMsgId();
            var msgRequest = null;
            var self = BenTauHandler.getInstance();
            if (self.msgsSend.has(msgId)) {
                msgRequest = self.msgsSend.get(msgId);
                self.msgsSend.delete(msgId);
            }
            if (msgRequest) {
                if (msgRequest.loading) {
                    self.closeLoading();
                }
                switch (code) {
                    case code_pb_1.Code.OK:
                        if (msgRequest.response)
                            msgRequest.response(msgReply);
                        break;
                    default:
                        // request fail
                        if (msgRequest.onError)
                            msgRequest.onError(err);
                        break;
                }
            }
        }
    };
    BenTauHandler.prototype.handUpdateTimeOut = function () {
        var _this = this;
        var dt = 500;
        BenTauHandler.instance._timerIntervalUpdate = setInterval(function () { return _this.updateTimeOut(dt); }, dt);
    };
    BenTauHandler.prototype.updateTimeOut = function (dt) {
        var _a;
        if (!((_a = BenTauHandler.instance) === null || _a === void 0 ? void 0 : _a._start))
            return;
        this.msgsSend.forEach(function (msg) {
            msg.update(dt);
        });
        StabilizeConnection_1.default.update(dt);
    };
    BenTauHandler.prototype.onMsgTimeOut = function (msgId) {
        if (this.msgsSend.has(msgId)) {
            var msgTimeOut = this.msgsSend.get(msgId);
            this.msgsSend.delete(msgId);
            if (msgTimeOut.loading) {
                this.closeLoading();
            }
            if (msgTimeOut.onError) {
                msgTimeOut.onError("Request timeout", code_pb_1.Code.UNKNOWN);
            }
        }
    };
    BenTauHandler.prototype.getUniqueId = function () {
        var d = new Date().getTime(); //Timestamp
        var d2 = (performance && performance.now && performance.now() * 1000) || 0; //Time in microseconds since page-load or 0 if unsupported
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            var r = Math.random() * 16; //random number between 0 and 16
            if (d > 0) {
                //Use timestamp until depleted
                r = (d + r) % 16 | 0;
                d = Math.floor(d / 16);
            }
            else {
                //Use microseconds since page-load if supported
                r = (d2 + r) % 16 | 0;
                d2 = Math.floor(d2 / 16);
            }
            return (c === "x" ? r : (r & 0x3) | 0x8).toString(16);
        });
    };
    BenTauHandler.prototype.handleOpen = function () {
        var _a;
        if (!((_a = BenTauHandler.instance) === null || _a === void 0 ? void 0 : _a.client))
            return;
        this.closeLoading();
        if (BenTauHandler.instance._initComplete)
            BenTauHandler.instance._initComplete();
        BenTauHandler.instance._start = true;
        StabilizeConnection_1.default.send();
    };
    BenTauHandler.prototype.handleReconnected = function () {
        var _a;
        if (!((_a = BenTauHandler.instance) === null || _a === void 0 ? void 0 : _a.client))
            return;
        //cc.log("Reconnected to BenTau");
        UIManager_1.default.getInstance().closeAllPopup(false);
        EventManager_1.default.fire(EventManager_1.default.ON_RECONNECTED);
        //reconnect event for card games
        cc.systemEvent.emit("ON_RECONNECTED");
        EventBusManager_1.default.onFireReconnected();
        BenTauHandler.instance._start = true;
        StabilizeConnection_1.default.send();
    };
    BenTauHandler.prototype.onMessage = function (msg) {
        var _a;
        if (!((_a = BenTauHandler.instance) === null || _a === void 0 ? void 0 : _a.client))
            return;
        var topic = msg.getTopic();
        StabilizeConnection_1.default.setTimeOutMsg();
        if (this.subscribes.has(topic)) {
            var e = this.subscribes.get(topic);
            e.forEach(function (f, key) {
                f(msg);
            });
            this.onReceiveMsgReply(msg);
        }
    };
    BenTauHandler.prototype.onError = function (errCode, err) {
        // if (!BenTauHandler.instance?.client) return;
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, EnumGame_1.CodeSocket.FORCE_CLOSE);
    };
    BenTauHandler.prototype.onClose = function (code, reason) {
        //cc.log("Bentau ws onClose " + code + " " + reason);
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, code);
    };
    BenTauHandler.prototype.onReconnect = function (code, reason) {
        //cc.log("Bentau ws onReconnect " + code + " " + reason);
        BenTauHandler.instance.doReconnect();
    };
    return BenTauHandler;
}());
exports.default = BenTauHandler;
var ConnectDefine_1 = require("../Define/ConnectDefine");
var UIManager_1 = require("../Manager/UIManager");
var GameDefine_1 = require("../Define/GameDefine");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var WSClient_1 = require("../Network/WebSocketClient/WSClient");
var StabilizeConnection_1 = require("./StabilizeConnection");
var EventManager_1 = require("../Event/EventManager");
var EnumGame_1 = require("../Utils/EnumGame");
var IdentityHandler_1 = require("./IdentityHandler");
var bentau_pb_1 = require("@gameloot/bentau/bentau_pb");
var code_pb_1 = require("@marketplace/rpc/code_pb");
var build_1 = require("@gameloot/client-base-handler/build");
var EventBusManager_1 = require("../Event/EventBusManager");
var PortalText_1 = require("../Utils/PortalText");

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L05ldHdvcmsvQmVuVGF1SGFuZGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0lBQUE7UUFXUyxXQUFNLEdBQWEsSUFBSSxDQUFDO1FBa0J2QixrQkFBYSxHQUFlLElBQUksQ0FBQztRQUNqQyx5QkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDNUIsV0FBTSxHQUFZLEtBQUssQ0FBQztRQVd4QixlQUFVLEdBQXNDLElBQUksR0FBRyxFQUFnQyxDQUFDO1FBQ3hGLGFBQVEsR0FBZ0MsSUFBSSxHQUFHLEVBQTBCLENBQUM7UUFDMUUsZUFBVSxHQUFXLFlBQVksQ0FBQztRQW9FbEMsbUJBQWMsR0FBVyxDQUFDLENBQUM7UUFDM0Isd0JBQW1CLEdBQVcsQ0FBQyxDQUFDO0lBME0xQyxDQUFDO0lBeFRRLHlCQUFXLEdBQWxCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUU7WUFDM0IsYUFBYSxDQUFDLFFBQVEsR0FBRyxJQUFJLGFBQWEsRUFBRSxDQUFDO1lBQzdDLGFBQWEsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztTQUM1QztRQUNELE9BQU8sYUFBYSxDQUFDLFFBQVEsQ0FBQztJQUNoQyxDQUFDO0lBSU8sbUNBQVcsR0FBbkIsVUFBb0IsUUFBbUI7UUFDckMsbUJBQVMsQ0FBQyxXQUFXLEVBQUU7YUFDcEIsaUJBQWlCLENBQUMsNEJBQWtCLEVBQUUsb0JBQVUsQ0FBQyxrQkFBa0IsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQzthQUN2RixJQUFJLENBQUM7WUFDSixRQUFRLEVBQUUsQ0FBQztRQUNiLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLG9DQUFZLEdBQXBCO1FBQ0UsbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyw0QkFBa0IsRUFBRSxvQkFBVSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDOUYsQ0FBQztJQUVPLGlDQUFTLEdBQWpCO1FBQ0Usc0JBQVksQ0FBQyxJQUFJLENBQUMsc0JBQVksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFLTSxxQkFBTyxHQUFkO1FBQ0Usb0JBQW9CO1FBQ3BCLElBQUksSUFBSSxHQUFHLGFBQWEsQ0FBQyxRQUFRLENBQUM7UUFDbEMsSUFBSSxFQUFDLElBQUksYUFBSixJQUFJLHVCQUFKLElBQUksQ0FBRSxNQUFNLENBQUE7WUFBRSxPQUFPO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHFCQUFVLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzlDLGFBQWEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN6QyxhQUFhLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDdEMsYUFBYSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDaEMsQ0FBQztJQU1ELG1DQUFXLEdBQVgsVUFBWSxLQUFZLEVBQUUsU0FBdUMsRUFBRSxHQUFnQjtRQUFoQixvQkFBQSxFQUFBLFFBQWdCO1FBQ2pGLElBQUksR0FBRyxJQUFJLEVBQUUsRUFBRTtZQUNiLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1NBQ3ZCO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUM5QixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxTQUFTLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDL0I7YUFBTTtZQUNMLElBQUksQ0FBQyxHQUFHLElBQUksR0FBRyxFQUFvQixDQUFDO1lBQ3BDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMvQjtJQUNILENBQUM7SUFFRCxxQ0FBYSxHQUFiLFVBQWMsS0FBWSxFQUFFLEdBQWdCO1FBQWhCLG9CQUFBLEVBQUEsUUFBZ0I7UUFDMUMsSUFBSSxHQUFHLElBQUksRUFBRSxFQUFFO1lBQ2IsR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7U0FDdkI7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzlCLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25DLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDZCxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNkLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQzthQUMvQjtTQUNGO0lBQ0gsQ0FBQztJQUVELDRCQUFJLEdBQUosVUFBSyxVQUE2QjtRQUFsQyxpQkFtQkM7UUFuQkksMkJBQUEsRUFBQSxpQkFBNkI7UUFDaEMsMEJBQTBCO1FBQzFCLElBQUksSUFBSSxHQUFHLGFBQWEsQ0FBQyxRQUFRLENBQUM7UUFDbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUM7UUFFaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUNmLGlDQUFpQztZQUNqQyxJQUFJLEVBQUMsSUFBSSxhQUFKLElBQUksdUJBQUosSUFBSSxDQUFFLE1BQU0sQ0FBQSxFQUFFO2dCQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksa0JBQVEsRUFBRSxDQUFDO2FBQzlCO1lBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQ2QsdUJBQWEsQ0FBQyxlQUFlLEVBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxFQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsRUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLEVBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxFQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FDNUIsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHdDQUFnQixHQUFoQjtRQUNFLG1DQUFtQztRQUNuQyxJQUFJLElBQUksR0FBRyxhQUFhLENBQUMsUUFBUSxDQUFDO1FBQ2xDLElBQUksRUFBQyxJQUFJLGFBQUosSUFBSSx1QkFBSixJQUFJLENBQUUsTUFBTSxDQUFBLEVBQUU7WUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLGtCQUFRLEVBQUUsQ0FBQztTQUM5QjtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUNkLHVCQUFhLENBQUMsZUFBZSxFQUM3QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUNqQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FDNUIsQ0FBQztJQUNKLENBQUM7SUFFRCx1Q0FBZSxHQUFmLGNBQW1CLENBQUM7SUFHcEIsbUNBQVcsR0FBWCxVQUFZLGtCQUFtQyxFQUFFLG1CQUFvQztRQUFyRixpQkEyQkM7UUEzQlcsbUNBQUEsRUFBQSx5QkFBbUM7UUFBRSxvQ0FBQSxFQUFBLDBCQUFvQztRQUNuRix3REFBd0Q7UUFDeEQsd0JBQXdCO1FBQ3hCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0Qix5QkFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLG9CQUFvQixDQUNoRDtZQUNFLGFBQWEsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM1QyxDQUFDLEVBQ0QsVUFBQyxRQUFRLEVBQUUsSUFBSTtZQUNiLEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxDQUFDO1lBQ2pDLEVBQUUsQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQ3pCLHFGQUFxRjtZQUNyRixJQUFJLElBQUksSUFBSSxjQUFJLENBQUMsT0FBTyxFQUFFO2dCQUN4QiwwQ0FBMEM7Z0JBQzFDLElBQUksS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFJLENBQUMsbUJBQW1CLEVBQUU7b0JBQ2xELEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDcEI7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUM7b0JBQ3hCLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQkFDcEIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2lCQUNsQjthQUNGO2lCQUFNO2dCQUNMLElBQUksU0FBUyxHQUFHLG9CQUFVLENBQUMsbUNBQW1DLENBQUM7Z0JBQy9ELHNCQUFZLENBQUMsSUFBSSxDQUFDLHNCQUFZLENBQUMsYUFBYSxFQUFFLHFCQUFVLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQzthQUNuRjtRQUNILENBQUMsQ0FDRixDQUFDO0lBQ0osQ0FBQztJQUVELDRCQUFJLEdBQUosVUFDRSxLQUFZLEVBQ1osU0FBaUIsRUFDakIsT0FBbUIsRUFDbkIsS0FBZ0MsRUFDaEMsT0FBc0QsRUFDdEQsU0FBZ0IsRUFDaEIsT0FBYztRQVBoQixpQkFxQ0M7O1FBakNDLHNCQUFBLEVBQUEsWUFBZ0M7UUFDaEMsd0JBQUEsRUFBQSxjQUFzRDtRQUN0RCwwQkFBQSxFQUFBLGdCQUFnQjtRQUNoQix3QkFBQSxFQUFBLGNBQWM7UUFFZCxJQUFJLFFBQUMsYUFBYSxDQUFDLFFBQVEsMENBQUUsTUFBTSxDQUFBO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFDakQsT0FBTyxHQUFHLEtBQUssSUFBSSxJQUFJLElBQUksT0FBTyxDQUFDO1FBQ25DLElBQUksT0FBTyxHQUFHLElBQUkseUJBQWEsRUFBRSxDQUFDO1FBQ2xDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDeEIsT0FBTyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUVoQyxJQUFJLEtBQUssR0FBRyxhQUFhLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2pELE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFeEIsd0JBQXdCO1FBQ3hCLE9BQU8sQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFNUIsSUFBSSxXQUFXLEdBQUc7WUFDaEIsMENBQTBDO1lBQzFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztRQUNoRSxDQUFDLENBQUM7UUFFRixJQUFJLEdBQUcsR0FBRyxJQUFJLHNCQUFjLEVBQUUsQ0FBQztRQUMvQixHQUFHLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNsQixHQUFHLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQztRQUMxQixHQUFHLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUNyQixHQUFHLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUN0QixHQUFHLENBQUMsU0FBUyxHQUFHLFVBQUMsWUFBWSxJQUFLLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsRUFBL0IsQ0FBK0IsQ0FBQztRQUNsRSxHQUFHLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUN0QixHQUFHLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUUxQixJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVPLG1DQUFXLEdBQW5CLFVBQW9CLEdBQW1CO1FBQ3JDLDhDQUE4QztRQUM5QyxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksSUFBSTtZQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDNUQsSUFBSSxHQUFHLENBQUMsT0FBTyxFQUFFO1lBQ2YsSUFBSSxDQUFDLFdBQVcsQ0FBQztnQkFDZixHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2Y7SUFDSCxDQUFDO0lBRUQseUNBQWlCLEdBQWpCLFVBQWtCLFFBQXVCO1FBQ3ZDLElBQUksTUFBTSxHQUFHLFFBQVEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNsQyxJQUFJLE1BQU0sRUFBRTtZQUNWLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUM1QixJQUFJLEdBQUcsR0FBRyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUM7WUFFOUIsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hDLElBQUksVUFBVSxHQUFtQixJQUFJLENBQUM7WUFDdEMsSUFBSSxJQUFJLEdBQUcsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3ZDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQzVCLFVBQVUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDN0I7WUFDRCxJQUFJLFVBQVUsRUFBRTtnQkFDZCxJQUFJLFVBQVUsQ0FBQyxPQUFPLEVBQUU7b0JBQ3RCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDckI7Z0JBQ0QsUUFBUSxJQUFJLEVBQUU7b0JBQ1osS0FBSyxjQUFJLENBQUMsRUFBRTt3QkFDVixJQUFJLFVBQVUsQ0FBQyxRQUFROzRCQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ3ZELE1BQU07b0JBQ1I7d0JBQ0UsZUFBZTt3QkFDZixJQUFJLFVBQVUsQ0FBQyxPQUFPOzRCQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ2hELE1BQU07aUJBQ1Q7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUVPLHlDQUFpQixHQUF6QjtRQUFBLGlCQUdDO1FBRkMsSUFBSSxFQUFFLEdBQUcsR0FBRyxDQUFDO1FBQ2IsYUFBYSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsR0FBRyxXQUFXLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEVBQXRCLENBQXNCLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDOUYsQ0FBQztJQUVPLHFDQUFhLEdBQXJCLFVBQXNCLEVBQUU7O1FBQ3RCLElBQUksUUFBQyxhQUFhLENBQUMsUUFBUSwwQ0FBRSxNQUFNLENBQUE7WUFBRSxPQUFPO1FBQzVDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsR0FBRztZQUN4QixHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsNkJBQW1CLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFTyxvQ0FBWSxHQUFwQixVQUFxQixLQUFLO1FBQ3hCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDNUIsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUIsSUFBSSxVQUFVLENBQUMsT0FBTyxFQUFFO2dCQUN0QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDckI7WUFDRCxJQUFJLFVBQVUsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3RCLFVBQVUsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsY0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ3JEO1NBQ0Y7SUFDSCxDQUFDO0lBRVMsbUNBQVcsR0FBckI7UUFDRSxJQUFJLENBQUMsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsV0FBVztRQUN6QyxJQUFJLEVBQUUsR0FBRyxDQUFDLFdBQVcsSUFBSSxXQUFXLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQywwREFBMEQ7UUFDdEksT0FBTyxzQ0FBc0MsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQztZQUN4RSxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsZ0NBQWdDO1lBQzVELElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDVCw4QkFBOEI7Z0JBQzlCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNyQixDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7YUFDeEI7aUJBQU07Z0JBQ0wsK0NBQStDO2dCQUMvQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDdEIsRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO2FBQzFCO1lBQ0QsT0FBTyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3hELENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLGtDQUFVLEdBQWxCOztRQUNFLElBQUksUUFBQyxhQUFhLENBQUMsUUFBUSwwQ0FBRSxNQUFNLENBQUE7WUFBRSxPQUFPO1FBQzVDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixJQUFJLGFBQWEsQ0FBQyxRQUFRLENBQUMsYUFBYTtZQUFFLGFBQWEsQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDakYsYUFBYSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3JDLDZCQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFTyx5Q0FBaUIsR0FBekI7O1FBQ0UsSUFBSSxRQUFDLGFBQWEsQ0FBQyxRQUFRLDBDQUFFLE1BQU0sQ0FBQTtZQUFFLE9BQU87UUFDNUMsa0NBQWtDO1FBQ2xDLG1CQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdDLHNCQUFZLENBQUMsSUFBSSxDQUFDLHNCQUFZLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDL0MsZ0NBQWdDO1FBQ2hDLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDdEMseUJBQWUsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3BDLGFBQWEsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNyQyw2QkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRU8saUNBQVMsR0FBakIsVUFBa0IsR0FBa0I7O1FBQ2xDLElBQUksUUFBQyxhQUFhLENBQUMsUUFBUSwwQ0FBRSxNQUFNLENBQUE7WUFBRSxPQUFPO1FBQzVDLElBQUksS0FBSyxHQUFHLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMzQiw2QkFBbUIsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNwQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzlCLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25DLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDLEVBQUUsR0FBRztnQkFDZixDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDVCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUM3QjtJQUNILENBQUM7SUFFRCwrQkFBTyxHQUFQLFVBQVEsT0FBTyxFQUFFLEdBQUc7UUFDbEIsK0NBQStDO1FBQy9DLHNCQUFZLENBQUMsSUFBSSxDQUFDLHNCQUFZLENBQUMsYUFBYSxFQUFFLHFCQUFVLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQUVPLCtCQUFPLEdBQWYsVUFBZ0IsSUFBWSxFQUFFLE1BQWM7UUFDMUMscURBQXFEO1FBQ3JELHNCQUFZLENBQUMsSUFBSSxDQUFDLHNCQUFZLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFTyxtQ0FBVyxHQUFuQixVQUFvQixJQUFZLEVBQUUsTUFBYztRQUM5Qyx5REFBeUQ7UUFDekQsYUFBYSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBQ0gsb0JBQUM7QUFBRCxDQTNUQSxBQTJUQyxJQUFBOztBQUVELHlEQUFvRDtBQUNwRCxrREFBNkM7QUFDN0MsbURBQThDO0FBQzlDLGtFQUE2RDtBQUM3RCxnRUFBMkQ7QUFDM0QsNkRBQXdEO0FBQ3hELHNEQUFpRDtBQUNqRCw4Q0FBK0M7QUFDL0MscURBQWdEO0FBQ2hELHdEQUEyRDtBQUUzRCxvREFBZ0Q7QUFDaEQsNkRBQXFFO0FBQ3JFLDREQUF1RDtBQUN2RCxrREFBNkMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCBjbGFzcyBCZW5UYXVIYW5kbGVyIHtcbiAgcHJvdGVjdGVkIHN0YXRpYyBpbnN0YW5jZTogQmVuVGF1SGFuZGxlcjtcblxuICBzdGF0aWMgZ2V0SW5zdGFuY2UoKTogQmVuVGF1SGFuZGxlciB7XG4gICAgaWYgKCFCZW5UYXVIYW5kbGVyLmluc3RhbmNlKSB7XG4gICAgICBCZW5UYXVIYW5kbGVyLmluc3RhbmNlID0gbmV3IEJlblRhdUhhbmRsZXIoKTtcbiAgICAgIEJlblRhdUhhbmRsZXIuaW5zdGFuY2UuaGFuZFVwZGF0ZVRpbWVPdXQoKTtcbiAgICB9XG4gICAgcmV0dXJuIEJlblRhdUhhbmRsZXIuaW5zdGFuY2U7XG4gIH1cblxuICBwdWJsaWMgY2xpZW50OiBXU0NsaWVudCA9IG51bGw7XG5cbiAgcHJpdmF0ZSBvcGVuTG9hZGluZyhjYWxsYmFjaz86IEZ1bmN0aW9uKSB7XG4gICAgVUlNYW5hZ2VyLmdldEluc3RhbmNlKClcbiAgICAgIC5vcGVuUG9wdXBTeXN0ZW1WMihMb2FkaW5nUG9wdXBTeXN0ZW0sIEdhbWVEZWZpbmUuTG9hZGluZ1BvcHVwU3lzdGVtLCBmYWxzZSwgbnVsbCwgbnVsbClcbiAgICAgIC50aGVuKCgpID0+IHtcbiAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBjbG9zZUxvYWRpbmcoKSB7XG4gICAgVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkuY2xvc2VQb3B1cFN5c3RlbShMb2FkaW5nUG9wdXBTeXN0ZW0sIEdhbWVEZWZpbmUuTG9hZGluZ1BvcHVwU3lzdGVtKTtcbiAgfVxuXG4gIHByaXZhdGUgb3BlblJldHJ5KCkge1xuICAgIEV2ZW50TWFuYWdlci5maXJlKEV2ZW50TWFuYWdlci5PTl9TSE9XX1JFVFJZX1BPUFVQKTtcbiAgfVxuXG4gIHByaXZhdGUgX2luaXRDb21wbGV0ZTogKCkgPT4gdm9pZCA9IG51bGw7XG4gIHByaXZhdGUgX3RpbWVySW50ZXJ2YWxVcGRhdGUgPSBudWxsO1xuICBwcml2YXRlIF9zdGFydDogYm9vbGVhbiA9IGZhbHNlO1xuICBzdGF0aWMgZGVzdHJveSgpIHtcbiAgICAvL2NjLmxvZyhcImRlc3Ryb3lcIik7XG4gICAgbGV0IHNlbGYgPSBCZW5UYXVIYW5kbGVyLmluc3RhbmNlO1xuICAgIGlmICghc2VsZj8uY2xpZW50KSByZXR1cm47XG4gICAgc2VsZi5jbGllbnQuY2xvc2UoQ29kZVNvY2tldC5GT1JDRV9DTE9TRSwgXCJcIik7XG4gICAgY2xlYXJJbnRlcnZhbChzZWxmLl90aW1lckludGVydmFsVXBkYXRlKTtcbiAgICBCZW5UYXVIYW5kbGVyLmluc3RhbmNlLl9zdGFydCA9IGZhbHNlO1xuICAgIEJlblRhdUhhbmRsZXIuaW5zdGFuY2UgPSBudWxsO1xuICB9XG5cbiAgcHJpdmF0ZSBzdWJzY3JpYmVzOiBNYXA8VG9waWMsIE1hcDxzdHJpbmcsIEZ1bmN0aW9uPj4gPSBuZXcgTWFwPFRvcGljLCBNYXA8c3RyaW5nLCBGdW5jdGlvbj4+KCk7XG4gIHByaXZhdGUgbXNnc1NlbmQ6IE1hcDxzdHJpbmcsIFJlcXVlc3RNZXNzYWdlPiA9IG5ldyBNYXA8c3RyaW5nLCBSZXF1ZXN0TWVzc2FnZT4oKTtcbiAgcHJpdmF0ZSBrZXlkZWZhdWx0OiBzdHJpbmcgPSBcImtleWRlZmF1bHRcIjtcblxuICBvblN1YnNjcmliZSh0b3BpYzogVG9waWMsIG9uUmVjZWl2ZTogKG1zZzogQmVuVGF1TWVzc2FnZSkgPT4gdm9pZCwga2V5OiBzdHJpbmcgPSBcIlwiKTogdm9pZCB7XG4gICAgaWYgKGtleSA9PSBcIlwiKSB7XG4gICAgICBrZXkgPSB0aGlzLmtleWRlZmF1bHQ7XG4gICAgfVxuICAgIGlmICh0aGlzLnN1YnNjcmliZXMuaGFzKHRvcGljKSkge1xuICAgICAgbGV0IGUgPSB0aGlzLnN1YnNjcmliZXMuZ2V0KHRvcGljKTtcbiAgICAgIGUuc2V0KGtleSwgb25SZWNlaXZlKTtcbiAgICAgIHRoaXMuc3Vic2NyaWJlcy5zZXQodG9waWMsIGUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBsZXQgZSA9IG5ldyBNYXA8c3RyaW5nLCBGdW5jdGlvbj4oKTtcbiAgICAgIGUuc2V0KGtleSwgb25SZWNlaXZlKTtcbiAgICAgIHRoaXMuc3Vic2NyaWJlcy5zZXQodG9waWMsIGUpO1xuICAgIH1cbiAgfVxuXG4gIG9uVW5TdWJzY3JpYmUodG9waWM6IFRvcGljLCBrZXk6IHN0cmluZyA9IFwiXCIpOiB2b2lkIHtcbiAgICBpZiAoa2V5ID09IFwiXCIpIHtcbiAgICAgIGtleSA9IHRoaXMua2V5ZGVmYXVsdDtcbiAgICB9XG4gICAgaWYgKHRoaXMuc3Vic2NyaWJlcy5oYXModG9waWMpKSB7XG4gICAgICBsZXQgZSA9IHRoaXMuc3Vic2NyaWJlcy5nZXQodG9waWMpO1xuICAgICAgaWYgKGUuaGFzKGtleSkpIHtcbiAgICAgICAgZS5kZWxldGUoa2V5KTtcbiAgICAgICAgdGhpcy5zdWJzY3JpYmVzLnNldCh0b3BpYywgZSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaW5pdChvbkNvbXBsZXRlOiAoKSA9PiB2b2lkID0gbnVsbCkge1xuICAgIC8vY2MubG9nKFwiaW5pdCBiZXRhdSBXU1wiKTtcbiAgICBsZXQgc2VsZiA9IEJlblRhdUhhbmRsZXIuaW5zdGFuY2U7XG4gICAgc2VsZi5faW5pdENvbXBsZXRlID0gb25Db21wbGV0ZTtcblxuICAgIHRoaXMub3BlbkxvYWRpbmcoKCkgPT4ge1xuICAgICAgLy9jYy5sb2coXCJDb25uZWN0aW5nIHRvIEJlblRhdVwiKTtcbiAgICAgIGlmICghc2VsZj8uY2xpZW50KSB7XG4gICAgICAgIHNlbGYuY2xpZW50ID0gbmV3IFdTQ2xpZW50KCk7XG4gICAgICB9XG4gICAgICBzZWxmLmNsaWVudC5vcGVuKFxuICAgICAgICBDb25uZWN0RGVmaW5lLmFkZHJlc3NCZW5UYXVXUyxcbiAgICAgICAgc2VsZi5oYW5kbGVPcGVuLmJpbmQodGhpcyksXG4gICAgICAgIHNlbGYub25NZXNzYWdlLmJpbmQodGhpcyksXG4gICAgICAgIHNlbGYub25DbG9zZS5iaW5kKHRoaXMpLFxuICAgICAgICBzZWxmLm9uRXJyb3IuYmluZCh0aGlzKSxcbiAgICAgICAgc2VsZi5vblJlY29ubmVjdC5iaW5kKHRoaXMpXG4gICAgICApO1xuICAgIH0pO1xuICB9XG5cbiAgb3Blbk5ld1dlYnNvY2tldCgpIHtcbiAgICAvLyBjb25zb2xlLmxvZyhcIm9wZW5OZXdXZWJzb2NrZXRcIik7XG4gICAgbGV0IHNlbGYgPSBCZW5UYXVIYW5kbGVyLmluc3RhbmNlO1xuICAgIGlmICghc2VsZj8uY2xpZW50KSB7XG4gICAgICBzZWxmLmNsaWVudCA9IG5ldyBXU0NsaWVudCgpO1xuICAgIH1cbiAgICBzZWxmLmNsaWVudC5vcGVuKFxuICAgICAgQ29ubmVjdERlZmluZS5hZGRyZXNzQmVuVGF1V1MsXG4gICAgICBzZWxmLmhhbmRsZVJlY29ubmVjdGVkLmJpbmQodGhpcyksXG4gICAgICBzZWxmLm9uTWVzc2FnZS5iaW5kKHRoaXMpLFxuICAgICAgc2VsZi5vbkNsb3NlLmJpbmQodGhpcyksXG4gICAgICBzZWxmLm9uRXJyb3IuYmluZCh0aGlzKSxcbiAgICAgIHNlbGYub25SZWNvbm5lY3QuYmluZCh0aGlzKVxuICAgICk7XG4gIH1cblxuICBjbGVhbkNvbm5lY3Rpb24oKSB7fVxuICBwcml2YXRlIGNvdW50UmVjb25uZWN0OiBudW1iZXIgPSAwO1xuICBwcml2YXRlIE1BWF9DT1VOVF9SRUNPTk5FQ1Q6IG51bWJlciA9IDM7XG4gIGRvUmVjb25uZWN0KG9uUmVjb25uZWN0aW5nRnVuYzogRnVuY3Rpb24gPSBudWxsLCBmYWlsZWRSZWNvbm5lY3RGdW5jOiBGdW5jdGlvbiA9IG51bGwpIHtcbiAgICAvL2NjLmxvZyhcIlJlY29ubmVjdGluZyB3aXRoIGFwaSB2ZXJpZnlUb2tlblJlY29ubmVjdCBcIik7XG4gICAgLy8gb25SZWNvbm5lY3RpbmdGdW5jKCk7XG4gICAgdGhpcy5jb3VudFJlY29ubmVjdCsrO1xuICAgIElkZW50aXR5SGFuZGxlci5nZXRJbnN0YW5jZSgpLnZlcmlmeVRva2VuUmVjb25uZWN0KFxuICAgICAgKCkgPT4ge1xuICAgICAgICBCZW5UYXVIYW5kbGVyLmluc3RhbmNlLm9wZW5OZXdXZWJzb2NrZXQoKTtcbiAgICAgIH0sXG4gICAgICAobXNnRXJyb3IsIGNvZGUpID0+IHtcbiAgICAgICAgY2MubG9nKFwibXNnRXJyb3IgPSBcIiArIG1zZ0Vycm9yKTtcbiAgICAgICAgY2MubG9nKFwiY29kZSA9IFwiICsgY29kZSk7XG4gICAgICAgIC8vY2MubG9nKFwiY291bnRSZWNvbm5lY3QgPSBcIiArIHRoaXMuY291bnRSZWNvbm5lY3QgKyBcIiBcIiArIHRoaXMuTUFYX0NPVU5UX1JFQ09OTkVDVCk7XG4gICAgICAgIGlmIChjb2RlID09IENvZGUuVU5LTk9XTikge1xuICAgICAgICAgIC8vIGZhaWxlZFJlY29ubmVjdEZ1bmMoKTsgLy8gcmVjb25uZWN0IGzhuqFpXG4gICAgICAgICAgaWYgKHRoaXMuY291bnRSZWNvbm5lY3QgPCB0aGlzLk1BWF9DT1VOVF9SRUNPTk5FQ1QpIHtcbiAgICAgICAgICAgIHRoaXMuZG9SZWNvbm5lY3QoKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5jb3VudFJlY29ubmVjdCA9IDA7XG4gICAgICAgICAgICB0aGlzLmNsb3NlTG9hZGluZygpO1xuICAgICAgICAgICAgdGhpcy5vcGVuUmV0cnkoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbGV0IG1lc3NFcnJvciA9IFBvcnRhbFRleHQuUE9SX01FU1NfQUNDT1VOVF9MT0dJTl9PVEhFUl9ERVZJQ0U7XG4gICAgICAgICAgRXZlbnRNYW5hZ2VyLmZpcmUoRXZlbnRNYW5hZ2VyLk9OX0RJU0NPTk5FQ1QsIENvZGVTb2NrZXQuS09USUMsIGZhbHNlLCBtZXNzRXJyb3IpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIHNlbmQoXG4gICAgdG9waWM6IFRvcGljLFxuICAgIGNoYW5uZWxJZDogc3RyaW5nLFxuICAgIHBheWxvYWQ6IFVpbnQ4QXJyYXksXG4gICAgcmVwbHk6IChtc2dSZXBseSkgPT4gdm9pZCA9IG51bGwsXG4gICAgb25FcnJvcjogKG1zZ0Vycm9yOiBzdHJpbmcsIGNvZGU6IENvZGUpID0+IHZvaWQgPSBudWxsLFxuICAgIGxvYWRpbmdCZyA9IHRydWUsXG4gICAgbG9hZGluZyA9IHRydWVcbiAgKTogc3RyaW5nIHtcbiAgICBpZiAoIUJlblRhdUhhbmRsZXIuaW5zdGFuY2U/LmNsaWVudCkgcmV0dXJuIG51bGw7XG4gICAgbG9hZGluZyA9IHJlcGx5ICE9IG51bGwgJiYgbG9hZGluZztcbiAgICBsZXQgcmVxdWVzdCA9IG5ldyBCZW5UYXVNZXNzYWdlKCk7XG4gICAgcmVxdWVzdC5zZXRUb3BpYyh0b3BpYyk7XG4gICAgcmVxdWVzdC5zZXRDaGFubmVsSWQoY2hhbm5lbElkKTtcblxuICAgIHZhciBtc2dJZCA9IEJlblRhdUhhbmRsZXIuaW5zdGFuY2UuZ2V0VW5pcXVlSWQoKTtcbiAgICByZXF1ZXN0LnNldE1zZ0lkKG1zZ0lkKTtcblxuICAgIC8vIFB1dCAgdG8gQmVuVGF1TWVzc2FnZVxuICAgIHJlcXVlc3Quc2V0UGF5bG9hZChwYXlsb2FkKTtcblxuICAgIGxldCBzZW5kUmVxdWVzdCA9ICgpID0+IHtcbiAgICAgIC8vLy9jYy5sb2coXCJzZW5kUmVxdWVzdC4uLi4uLi4uLlwiLCBtc2dJZCk7XG4gICAgICBCZW5UYXVIYW5kbGVyLmluc3RhbmNlLmNsaWVudC5zZW5kKHJlcXVlc3Quc2VyaWFsaXplQmluYXJ5KCkpO1xuICAgIH07XG5cbiAgICBsZXQgbXNnID0gbmV3IFJlcXVlc3RNZXNzYWdlKCk7XG4gICAgbXNnLm1zZ0lkID0gbXNnSWQ7XG4gICAgbXNnLnJlcXVlc3QgPSBzZW5kUmVxdWVzdDtcbiAgICBtc2cucmVzcG9uc2UgPSByZXBseTtcbiAgICBtc2cub25FcnJvciA9IG9uRXJyb3I7XG4gICAgbXNnLm9uVGltZU91dCA9IChtc2dJZFRpbWVPdXQpID0+IHRoaXMub25Nc2dUaW1lT3V0KG1zZ0lkVGltZU91dCk7XG4gICAgbXNnLmxvYWRpbmcgPSBsb2FkaW5nO1xuICAgIG1zZy5sb2FkaW5nQmcgPSBsb2FkaW5nQmc7XG5cbiAgICB0aGlzLnNlbmRSZXF1ZXN0KG1zZyk7XG4gICAgcmV0dXJuIG1zZ0lkO1xuICB9XG5cbiAgcHJpdmF0ZSBzZW5kUmVxdWVzdChtc2c6IFJlcXVlc3RNZXNzYWdlKSB7XG4gICAgLy9jaOG7iSBuaOG7r25nIG1zZyBuw6BvIGNo4budIHJlc3BvbnNlIG3hu5tpIGNhY2hlIGzhuqFpXG4gICAgaWYgKG1zZy5yZXNwb25zZSAhPSBudWxsKSB0aGlzLm1zZ3NTZW5kLnNldChtc2cubXNnSWQsIG1zZyk7XG4gICAgaWYgKG1zZy5sb2FkaW5nKSB7XG4gICAgICB0aGlzLm9wZW5Mb2FkaW5nKCgpID0+IHtcbiAgICAgICAgbXNnLnJlcXVlc3QoKTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBtc2cucmVxdWVzdCgpO1xuICAgIH1cbiAgfVxuXG4gIG9uUmVjZWl2ZU1zZ1JlcGx5KG1zZ1JlcGx5OiBCZW5UYXVNZXNzYWdlKTogdm9pZCB7XG4gICAgbGV0IHN0YXR1cyA9IG1zZ1JlcGx5LmdldFN0YXR1cygpO1xuICAgIGlmIChzdGF0dXMpIHtcbiAgICAgIGxldCBjb2RlID0gc3RhdHVzLmdldENvZGUoKTtcbiAgICAgIGxldCBlcnIgPSBzdGF0dXMuZ2V0TWVzc2FnZSgpO1xuXG4gICAgICBsZXQgbXNnSWQgPSBtc2dSZXBseS5nZXRNc2dJZCgpO1xuICAgICAgbGV0IG1zZ1JlcXVlc3Q6IFJlcXVlc3RNZXNzYWdlID0gbnVsbDtcbiAgICAgIGxldCBzZWxmID0gQmVuVGF1SGFuZGxlci5nZXRJbnN0YW5jZSgpO1xuICAgICAgaWYgKHNlbGYubXNnc1NlbmQuaGFzKG1zZ0lkKSkge1xuICAgICAgICBtc2dSZXF1ZXN0ID0gc2VsZi5tc2dzU2VuZC5nZXQobXNnSWQpO1xuICAgICAgICBzZWxmLm1zZ3NTZW5kLmRlbGV0ZShtc2dJZCk7XG4gICAgICB9XG4gICAgICBpZiAobXNnUmVxdWVzdCkge1xuICAgICAgICBpZiAobXNnUmVxdWVzdC5sb2FkaW5nKSB7XG4gICAgICAgICAgc2VsZi5jbG9zZUxvYWRpbmcoKTtcbiAgICAgICAgfVxuICAgICAgICBzd2l0Y2ggKGNvZGUpIHtcbiAgICAgICAgICBjYXNlIENvZGUuT0s6XG4gICAgICAgICAgICBpZiAobXNnUmVxdWVzdC5yZXNwb25zZSkgbXNnUmVxdWVzdC5yZXNwb25zZShtc2dSZXBseSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgLy8gcmVxdWVzdCBmYWlsXG4gICAgICAgICAgICBpZiAobXNnUmVxdWVzdC5vbkVycm9yKSBtc2dSZXF1ZXN0Lm9uRXJyb3IoZXJyKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBoYW5kVXBkYXRlVGltZU91dCgpIHtcbiAgICBsZXQgZHQgPSA1MDA7XG4gICAgQmVuVGF1SGFuZGxlci5pbnN0YW5jZS5fdGltZXJJbnRlcnZhbFVwZGF0ZSA9IHNldEludGVydmFsKCgpID0+IHRoaXMudXBkYXRlVGltZU91dChkdCksIGR0KTtcbiAgfVxuXG4gIHByaXZhdGUgdXBkYXRlVGltZU91dChkdCkge1xuICAgIGlmICghQmVuVGF1SGFuZGxlci5pbnN0YW5jZT8uX3N0YXJ0KSByZXR1cm47XG4gICAgdGhpcy5tc2dzU2VuZC5mb3JFYWNoKChtc2cpID0+IHtcbiAgICAgIG1zZy51cGRhdGUoZHQpO1xuICAgIH0pO1xuICAgIFN0YWJpbGl6ZUNvbm5lY3Rpb24udXBkYXRlKGR0KTtcbiAgfVxuXG4gIHByaXZhdGUgb25Nc2dUaW1lT3V0KG1zZ0lkKSB7XG4gICAgaWYgKHRoaXMubXNnc1NlbmQuaGFzKG1zZ0lkKSkge1xuICAgICAgbGV0IG1zZ1RpbWVPdXQgPSB0aGlzLm1zZ3NTZW5kLmdldChtc2dJZCk7XG4gICAgICB0aGlzLm1zZ3NTZW5kLmRlbGV0ZShtc2dJZCk7XG4gICAgICBpZiAobXNnVGltZU91dC5sb2FkaW5nKSB7XG4gICAgICAgIHRoaXMuY2xvc2VMb2FkaW5nKCk7XG4gICAgICB9XG4gICAgICBpZiAobXNnVGltZU91dC5vbkVycm9yKSB7XG4gICAgICAgIG1zZ1RpbWVPdXQub25FcnJvcihcIlJlcXVlc3QgdGltZW91dFwiLCBDb2RlLlVOS05PV04pO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRVbmlxdWVJZCgpOiBzdHJpbmcge1xuICAgIHZhciBkID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7IC8vVGltZXN0YW1wXG4gICAgdmFyIGQyID0gKHBlcmZvcm1hbmNlICYmIHBlcmZvcm1hbmNlLm5vdyAmJiBwZXJmb3JtYW5jZS5ub3coKSAqIDEwMDApIHx8IDA7IC8vVGltZSBpbiBtaWNyb3NlY29uZHMgc2luY2UgcGFnZS1sb2FkIG9yIDAgaWYgdW5zdXBwb3J0ZWRcbiAgICByZXR1cm4gXCJ4eHh4eHh4eC14eHh4LTR4eHgteXh4eC14eHh4eHh4eHh4eHhcIi5yZXBsYWNlKC9beHldL2csIGZ1bmN0aW9uIChjKSB7XG4gICAgICB2YXIgciA9IE1hdGgucmFuZG9tKCkgKiAxNjsgLy9yYW5kb20gbnVtYmVyIGJldHdlZW4gMCBhbmQgMTZcbiAgICAgIGlmIChkID4gMCkge1xuICAgICAgICAvL1VzZSB0aW1lc3RhbXAgdW50aWwgZGVwbGV0ZWRcbiAgICAgICAgciA9IChkICsgcikgJSAxNiB8IDA7XG4gICAgICAgIGQgPSBNYXRoLmZsb29yKGQgLyAxNik7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvL1VzZSBtaWNyb3NlY29uZHMgc2luY2UgcGFnZS1sb2FkIGlmIHN1cHBvcnRlZFxuICAgICAgICByID0gKGQyICsgcikgJSAxNiB8IDA7XG4gICAgICAgIGQyID0gTWF0aC5mbG9vcihkMiAvIDE2KTtcbiAgICAgIH1cbiAgICAgIHJldHVybiAoYyA9PT0gXCJ4XCIgPyByIDogKHIgJiAweDMpIHwgMHg4KS50b1N0cmluZygxNik7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGhhbmRsZU9wZW4oKSB7XG4gICAgaWYgKCFCZW5UYXVIYW5kbGVyLmluc3RhbmNlPy5jbGllbnQpIHJldHVybjtcbiAgICB0aGlzLmNsb3NlTG9hZGluZygpO1xuICAgIGlmIChCZW5UYXVIYW5kbGVyLmluc3RhbmNlLl9pbml0Q29tcGxldGUpIEJlblRhdUhhbmRsZXIuaW5zdGFuY2UuX2luaXRDb21wbGV0ZSgpO1xuICAgIEJlblRhdUhhbmRsZXIuaW5zdGFuY2UuX3N0YXJ0ID0gdHJ1ZTtcbiAgICBTdGFiaWxpemVDb25uZWN0aW9uLnNlbmQoKTtcbiAgfVxuXG4gIHByaXZhdGUgaGFuZGxlUmVjb25uZWN0ZWQoKSB7XG4gICAgaWYgKCFCZW5UYXVIYW5kbGVyLmluc3RhbmNlPy5jbGllbnQpIHJldHVybjtcbiAgICAvL2NjLmxvZyhcIlJlY29ubmVjdGVkIHRvIEJlblRhdVwiKTtcbiAgICBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5jbG9zZUFsbFBvcHVwKGZhbHNlKTtcbiAgICBFdmVudE1hbmFnZXIuZmlyZShFdmVudE1hbmFnZXIuT05fUkVDT05ORUNURUQpO1xuICAgIC8vcmVjb25uZWN0IGV2ZW50IGZvciBjYXJkIGdhbWVzXG4gICAgY2Muc3lzdGVtRXZlbnQuZW1pdChcIk9OX1JFQ09OTkVDVEVEXCIpO1xuICAgIEV2ZW50QnVzTWFuYWdlci5vbkZpcmVSZWNvbm5lY3RlZCgpO1xuICAgIEJlblRhdUhhbmRsZXIuaW5zdGFuY2UuX3N0YXJ0ID0gdHJ1ZTtcbiAgICBTdGFiaWxpemVDb25uZWN0aW9uLnNlbmQoKTtcbiAgfVxuXG4gIHByaXZhdGUgb25NZXNzYWdlKG1zZzogQmVuVGF1TWVzc2FnZSkge1xuICAgIGlmICghQmVuVGF1SGFuZGxlci5pbnN0YW5jZT8uY2xpZW50KSByZXR1cm47XG4gICAgbGV0IHRvcGljID0gbXNnLmdldFRvcGljKCk7XG4gICAgU3RhYmlsaXplQ29ubmVjdGlvbi5zZXRUaW1lT3V0TXNnKCk7XG4gICAgaWYgKHRoaXMuc3Vic2NyaWJlcy5oYXModG9waWMpKSB7XG4gICAgICBsZXQgZSA9IHRoaXMuc3Vic2NyaWJlcy5nZXQodG9waWMpO1xuICAgICAgZS5mb3JFYWNoKChmLCBrZXkpID0+IHtcbiAgICAgICAgZihtc2cpO1xuICAgICAgfSk7XG4gICAgICB0aGlzLm9uUmVjZWl2ZU1zZ1JlcGx5KG1zZyk7XG4gICAgfVxuICB9XG5cbiAgb25FcnJvcihlcnJDb2RlLCBlcnIpIHtcbiAgICAvLyBpZiAoIUJlblRhdUhhbmRsZXIuaW5zdGFuY2U/LmNsaWVudCkgcmV0dXJuO1xuICAgIEV2ZW50TWFuYWdlci5maXJlKEV2ZW50TWFuYWdlci5PTl9ESVNDT05ORUNULCBDb2RlU29ja2V0LkZPUkNFX0NMT1NFKTtcbiAgfVxuXG4gIHByaXZhdGUgb25DbG9zZShjb2RlOiBudW1iZXIsIHJlYXNvbjogc3RyaW5nKSB7XG4gICAgLy9jYy5sb2coXCJCZW50YXUgd3Mgb25DbG9zZSBcIiArIGNvZGUgKyBcIiBcIiArIHJlYXNvbik7XG4gICAgRXZlbnRNYW5hZ2VyLmZpcmUoRXZlbnRNYW5hZ2VyLk9OX0RJU0NPTk5FQ1QsIGNvZGUpO1xuICB9XG5cbiAgcHJpdmF0ZSBvblJlY29ubmVjdChjb2RlOiBudW1iZXIsIHJlYXNvbjogc3RyaW5nKSB7XG4gICAgLy9jYy5sb2coXCJCZW50YXUgd3Mgb25SZWNvbm5lY3QgXCIgKyBjb2RlICsgXCIgXCIgKyByZWFzb24pO1xuICAgIEJlblRhdUhhbmRsZXIuaW5zdGFuY2UuZG9SZWNvbm5lY3QoKTtcbiAgfVxufVxuXG5pbXBvcnQgQ29ubmVjdERlZmluZSBmcm9tIFwiLi4vRGVmaW5lL0Nvbm5lY3REZWZpbmVcIjtcbmltcG9ydCBVSU1hbmFnZXIgZnJvbSBcIi4uL01hbmFnZXIvVUlNYW5hZ2VyXCI7XG5pbXBvcnQgR2FtZURlZmluZSBmcm9tIFwiLi4vRGVmaW5lL0dhbWVEZWZpbmVcIjtcbmltcG9ydCBMb2FkaW5nUG9wdXBTeXN0ZW0gZnJvbSBcIi4uL1BvcHVwL0xvYWRpbmdQb3B1cFN5c3RlbVwiO1xuaW1wb3J0IFdTQ2xpZW50IGZyb20gXCIuLi9OZXR3b3JrL1dlYlNvY2tldENsaWVudC9XU0NsaWVudFwiO1xuaW1wb3J0IFN0YWJpbGl6ZUNvbm5lY3Rpb24gZnJvbSBcIi4vU3RhYmlsaXplQ29ubmVjdGlvblwiO1xuaW1wb3J0IEV2ZW50TWFuYWdlciBmcm9tIFwiLi4vRXZlbnQvRXZlbnRNYW5hZ2VyXCI7XG5pbXBvcnQgeyBDb2RlU29ja2V0IH0gZnJvbSBcIi4uL1V0aWxzL0VudW1HYW1lXCI7XG5pbXBvcnQgSWRlbnRpdHlIYW5kbGVyIGZyb20gXCIuL0lkZW50aXR5SGFuZGxlclwiO1xuaW1wb3J0IHsgQmVuVGF1TWVzc2FnZSB9IGZyb20gXCJAZ2FtZWxvb3QvYmVudGF1L2JlbnRhdV9wYlwiO1xuaW1wb3J0IHsgVG9waWMgfSBmcm9tIFwiQGdhbWVsb290L3RvcGljL3RvcGljX3BiXCI7XG5pbXBvcnQgeyBDb2RlIH0gZnJvbSBcIkBtYXJrZXRwbGFjZS9ycGMvY29kZV9wYlwiO1xuaW1wb3J0IHsgUmVxdWVzdE1lc3NhZ2UgfSBmcm9tIFwiQGdhbWVsb290L2NsaWVudC1iYXNlLWhhbmRsZXIvYnVpbGRcIjtcbmltcG9ydCBFdmVudEJ1c01hbmFnZXIgZnJvbSBcIi4uL0V2ZW50L0V2ZW50QnVzTWFuYWdlclwiO1xuaW1wb3J0IFBvcnRhbFRleHQgZnJvbSBcIi4uL1V0aWxzL1BvcnRhbFRleHRcIjtcbiJdfQ==