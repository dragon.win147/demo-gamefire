
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Network/MyAccountHandler.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6e60bbi441AiJnCRgu1b/F1', 'MyAccountHandler');
// Game/Lobby/Network/MyAccountHandler.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var MyaccountServiceClientPb_1 = require("@gameloot/myaccount/MyaccountServiceClientPb");
var myaccount_pb_1 = require("@gameloot/myaccount/myaccount_pb");
var myaccount_code_pb_1 = require("@gameloot/myaccount/myaccount_code_pb");
var playah_pb_1 = require("@gameloot/playah/playah_pb");
var build_1 = require("@gameloot/client-base-handler/build");
var empty_pb_1 = require("google-protobuf/google/protobuf/empty_pb");
var PortalText_1 = require("../Utils/PortalText");
var ConnectDefine_1 = require("../Define/ConnectDefine");
var GameDefine_1 = require("../Define/GameDefine");
var EventManager_1 = require("../Event/EventManager");
var UIManager_1 = require("../Manager/UIManager");
var TextNotify_1 = require("../Manager/TextNotify");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var WarningPopupSystem_1 = require("../Popup/WarningPopupSystem");
var BenTauHandler_1 = require("./BenTauHandler");
var IdentityHandler_1 = require("./IdentityHandler");
var MyAccountHandler = /** @class */ (function (_super) {
    __extends(MyAccountHandler, _super);
    function MyAccountHandler() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.client = null;
        _this.bases = new Map();
        _this.frames = new Map();
        _this.playah = null;
        _this.handleErr = null;
        return _this;
    }
    MyAccountHandler.getInstance = function () {
        if (!MyAccountHandler.instance) {
            MyAccountHandler.instance = new MyAccountHandler(BenTauHandler_1.default.getInstance());
            MyAccountHandler.instance.client = new MyaccountServiceClientPb_1.MyAccountClient(ConnectDefine_1.default.addressSanBay, {}, null);
            MyAccountHandler.instance.topic = topic_pb_1.Topic.MY_ACCOUNT_LEVEL_UP;
            MyAccountHandler.instance.onSubscribe();
        }
        return MyAccountHandler.instance;
    };
    MyAccountHandler.destroy = function () {
        if (MyAccountHandler.instance)
            MyAccountHandler.instance.onUnSubscribe();
        MyAccountHandler.instance = null;
    };
    Object.defineProperty(MyAccountHandler.prototype, "Frames", {
        get: function () {
            return this.frames;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MyAccountHandler.prototype, "Bases", {
        get: function () {
            return this.bases;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MyAccountHandler.prototype, "Playah", {
        get: function () {
            return this.playah;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MyAccountHandler.prototype, "metaData", {
        get: function () {
            return {
                Authorization: "Bearer " + IdentityHandler_1.default.getInstance().token,
            };
        },
        enumerable: false,
        configurable: true
    });
    MyAccountHandler.prototype.showLoading = function (callback) {
        UIManager_1.default.getInstance()
            .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, false, null, null)
            .then(function () {
            callback();
        });
    };
    MyAccountHandler.prototype.hideLoading = function () {
        UIManager_1.default.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
    };
    MyAccountHandler.prototype.onShowErr = function (str, onClick) {
        UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, str, onClick, onClick);
    };
    MyAccountHandler.prototype.onDisconnect = function (CodeSocket, messError) {
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, CodeSocket, false, messError);
    };
    MyAccountHandler.prototype.onGetBasePath = function (id) {
        return this.bases.get(id);
    };
    MyAccountHandler.prototype.onGetFramePath = function (id) {
        return this.frames.get(id);
    };
    MyAccountHandler.prototype.onGetAvatarList = function (response) {
        var _this = this;
        if (response === void 0) { response = null; }
        //cc.log("onGetAvatarList");
        var self = this;
        var request = new myaccount_pb_1.ListMyAvatarsRequest();
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.listMyAvatars(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            res.getBasesList().forEach(function (base) {
                _this.bases.set(base.getId(), base.getPath());
            });
            res.getFramesList().forEach(function (frame) {
                _this.frames.set(frame.getId(), frame.getPath());
            });
            if (response)
                response(res);
        }, function () {
            //cc.log("Err");
            if (response)
                response(null);
        }, false);
    };
    MyAccountHandler.prototype.onGetAvatarListWhenSignUp = function (gender, random, limit, response) {
        var _this = this;
        if (random === void 0) { random = true; }
        if (limit === void 0) { limit = 10; }
        if (response === void 0) { response = null; }
        var self = this;
        var request = new myaccount_pb_1.ListBasesRequest();
        request.setGender(gender);
        request.setRandom(random);
        request.setLimit(limit);
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.listBases(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            _this.frames.set(res.getDefaultFrame().getId(), res.getDefaultFrame().getPath());
            res.getBasesList().forEach(function (base) {
                _this.bases.set(base.getId(), base.getPath());
            });
            response(res);
        }, null, false);
    };
    MyAccountHandler.prototype.getProfile = function (response) {
        //cc.log("getProfile " + IdentityHandler.getInstance().token);
        var request = new empty_pb_1.Empty();
        var self = this;
        var msgId = self.getUniqueId();
        var metaData = { Authorization: "Bearer " + IdentityHandler_1.default.getInstance().token };
        var sendRequest = function () {
            self.client.me(request, metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            //cc.log("getProfile done ");
            if (res == null || !(res === null || res === void 0 ? void 0 : res.hasProfile()) || !(res === null || res === void 0 ? void 0 : res.hasAvatar())) {
                // UIManager.getInstance().openUpdateNickNamePopup();
            }
            else {
                // UserManager.getInstance().setUserData(res);
                var playah = new playah_pb_1.Playah();
                playah.setUserId(res.getSafeId());
                playah.setProfile(res.getProfile());
                playah.setAvatar(res.getAvatar());
                self.playah = playah;
            }
            if (response) {
                response(res);
            }
        }, function (err, code) {
            // if (code != Code.UNKNOWN) {
            //   UIManager.getInstance().openUpdateNickNamePopup();
            // }
        }, false);
    };
    MyAccountHandler.prototype.handleCustomError = function (errCode, err) {
        if (errCode == myaccount_code_pb_1.Code.UNKNOWN)
            return;
        if (this.handleErr)
            this.handleErr();
        //cc.log("handleCustomError " + "errCode" + err);
        var errString = "[" + errCode + "] ";
        //cc.log("errcode: ", errCode);
        switch (errCode) {
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_REQUIRE_LENGTH:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_TOO_SHORT;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_OVER_LENGTH:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_TOO_LONG;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_START_SPACE_CHAR:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_INVALID_PREFIX;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_SAME_USERNAME:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_SAME_USERNAME;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_HAS_PROFANITY:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_PROFANITY;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_TOO_SHORT;
                break;
            case myaccount_code_pb_1.Code.MY_ACCOUNT_INVALID_DISPLAY_NAME_HAS_INVALID_CHARACTER:
                errString = PortalText_1.default.POR_NOTIFY_NICKNAME_HAS_BANNED_CHARACTER;
                break;
        }
        UIManager_1.default.getInstance().openNotify(TextNotify_1.default, GameDefine_1.default.TextNotify, new TextNotify_1.TextNotifyData(errString, 2));
    };
    MyAccountHandler.prototype.onReceive = function (msg) {
        var message = myaccount_pb_1.Level.deserializeBinary(msg.getPayload_asU8());
        var level = message.getLevel();
        EventManager_1.default.fire(EventManager_1.default.UPDATE_BADGE_LEVEL_UP, level);
        // UIManager.getInstance().openAnimationLevelUpPopup(level);
    };
    return MyAccountHandler;
}(build_1.HandlerClientBenTauBase));
exports.default = MyAccountHandler;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L05ldHdvcmsvTXlBY2NvdW50SGFuZGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxxREFBaUQ7QUFDakQseUZBQStFO0FBQy9FLGlFQWUwQztBQUMxQywyRUFBNkQ7QUFDN0Qsd0RBQW9EO0FBQ3BELDZEQUE4RTtBQUM5RSxxRUFBaUU7QUFDakUsa0RBQTZDO0FBQzdDLHlEQUFvRDtBQUNwRCxtREFBOEM7QUFFOUMsc0RBQWlEO0FBQ2pELGtEQUE2QztBQUM3QyxvREFBbUU7QUFDbkUsa0VBQTZEO0FBQzdELGtFQUE2RDtBQUM3RCxpREFBNEM7QUFDNUMscURBQWdEO0FBRWhEO0lBQThDLG9DQUF1QjtJQUFyRTtRQUFBLHFFQTRNQztRQTNMUyxZQUFNLEdBQW9CLElBQUksQ0FBQztRQUMvQixXQUFLLEdBQXdCLElBQUksR0FBRyxFQUFrQixDQUFDO1FBQ3ZELFlBQU0sR0FBd0IsSUFBSSxHQUFHLEVBQWtCLENBQUM7UUFDeEQsWUFBTSxHQUFXLElBQUksQ0FBQztRQThDOUIsZUFBUyxHQUFlLElBQUksQ0FBQzs7SUEwSS9CLENBQUM7SUF6TVEsNEJBQVcsR0FBbEI7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFO1lBQzlCLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxJQUFJLGdCQUFnQixDQUFDLHVCQUFhLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUM5RSxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksMENBQWUsQ0FBQyx1QkFBYSxDQUFDLGFBQWEsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDOUYsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxnQkFBSyxDQUFDLG1CQUFtQixDQUFDO1lBQzVELGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN6QztRQUNELE9BQU8sZ0JBQWdCLENBQUMsUUFBUSxDQUFDO0lBQ25DLENBQUM7SUFFTSx3QkFBTyxHQUFkO1FBQ0UsSUFBSSxnQkFBZ0IsQ0FBQyxRQUFRO1lBQUUsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3pFLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDbkMsQ0FBQztJQU1ELHNCQUFJLG9DQUFNO2FBQVY7WUFDRSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDckIsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSxtQ0FBSzthQUFUO1lBQ0UsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3BCLENBQUM7OztPQUFBO0lBQ0Qsc0JBQUksb0NBQU07YUFBVjtZQUNFLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQixDQUFDOzs7T0FBQTtJQUVELHNCQUFZLHNDQUFRO2FBQXBCO1lBQ0UsT0FBTztnQkFDTCxhQUFhLEVBQUUsU0FBUyxHQUFHLHlCQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSzthQUMvRCxDQUFDO1FBQ0osQ0FBQzs7O09BQUE7SUFFUyxzQ0FBVyxHQUFyQixVQUFzQixRQUFrQjtRQUN0QyxtQkFBUyxDQUFDLFdBQVcsRUFBRTthQUNwQixpQkFBaUIsQ0FBQyw0QkFBa0IsRUFBRSxvQkFBVSxDQUFDLGtCQUFrQixFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO2FBQ3ZGLElBQUksQ0FBQztZQUNKLFFBQVEsRUFBRSxDQUFDO1FBQ2IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRVMsc0NBQVcsR0FBckI7UUFDRSxtQkFBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLGdCQUFnQixDQUFDLDRCQUFrQixFQUFFLG9CQUFVLENBQUMsa0JBQWtCLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRVMsb0NBQVMsR0FBbkIsVUFBb0IsR0FBVyxFQUFFLE9BQW9CO1FBQ25ELG1CQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsZUFBZSxDQUFDLDRCQUFrQixFQUFFLG9CQUFVLENBQUMsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNwSCxDQUFDO0lBRVMsdUNBQVksR0FBdEIsVUFBdUIsVUFBc0IsRUFBRSxTQUFpQjtRQUM5RCxzQkFBWSxDQUFDLElBQUksQ0FBQyxzQkFBWSxDQUFDLGFBQWEsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFRCx3Q0FBYSxHQUFiLFVBQWMsRUFBVTtRQUN0QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFRCx5Q0FBYyxHQUFkLFVBQWUsRUFBVTtRQUN2QixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFHRCwwQ0FBZSxHQUFmLFVBQWdCLFFBQXVEO1FBQXZFLGlCQTRCQztRQTVCZSx5QkFBQSxFQUFBLGVBQXVEO1FBQ3JFLDRCQUE0QjtRQUM1QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxtQ0FBb0IsRUFBRSxDQUFDO1FBQ3pDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMvQixJQUFJLFdBQVcsR0FBRztZQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFHO2dCQUN6RCxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFDRixJQUFJLENBQUMsYUFBYSxDQUNoQixLQUFLLEVBQ0wsV0FBVyxFQUNYLFVBQUMsR0FBdUI7WUFDdEIsR0FBRyxDQUFDLFlBQVksRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7Z0JBQzlCLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztZQUMvQyxDQUFDLENBQUMsQ0FBQztZQUNILEdBQUcsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFLO2dCQUNoQyxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEVBQUUsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7WUFDbEQsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLFFBQVE7Z0JBQUUsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLENBQUMsRUFDRDtZQUNFLGdCQUFnQjtZQUNoQixJQUFJLFFBQVE7Z0JBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFDRCxLQUFLLENBQ04sQ0FBQztJQUNKLENBQUM7SUFFRCxvREFBeUIsR0FBekIsVUFBMEIsTUFBYyxFQUFFLE1BQXNCLEVBQUUsS0FBa0IsRUFBRSxRQUFtRDtRQUF6SSxpQkEwQkM7UUExQnlDLHVCQUFBLEVBQUEsYUFBc0I7UUFBRSxzQkFBQSxFQUFBLFVBQWtCO1FBQUUseUJBQUEsRUFBQSxlQUFtRDtRQUN2SSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxPQUFPLEdBQUcsSUFBSSwrQkFBZ0IsRUFBRSxDQUFDO1FBQ3JDLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUIsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxQixPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3hCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMvQixJQUFJLFdBQVcsR0FBRztZQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFHO2dCQUNyRCxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFDRixJQUFJLENBQUMsYUFBYSxDQUNoQixLQUFLLEVBQ0wsV0FBVyxFQUNYLFVBQUMsR0FBbUI7WUFDbEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFLEdBQUcsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1lBQ2hGLEdBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJO2dCQUM5QixLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7WUFDL0MsQ0FBQyxDQUFDLENBQUM7WUFFSCxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEIsQ0FBQyxFQUNELElBQUksRUFDSixLQUFLLENBQ04sQ0FBQztJQUNKLENBQUM7SUFFRCxxQ0FBVSxHQUFWLFVBQVcsUUFBMEM7UUFDbkQsOERBQThEO1FBRTlELElBQUksT0FBTyxHQUFHLElBQUksZ0JBQUssRUFBRSxDQUFDO1FBQzFCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDL0IsSUFBSSxRQUFRLEdBQUcsRUFBRSxhQUFhLEVBQUUsU0FBUyxHQUFHLHlCQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFbEYsSUFBSSxXQUFXLEdBQUc7WUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFHO2dCQUN6QyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsYUFBYSxDQUNoQixLQUFLLEVBQ0wsV0FBVyxFQUNYLFVBQUMsR0FBaUI7WUFDaEIsNkJBQTZCO1lBQzdCLElBQUksR0FBRyxJQUFJLElBQUksSUFBSSxFQUFDLEdBQUcsYUFBSCxHQUFHLHVCQUFILEdBQUcsQ0FBRSxVQUFVLEdBQUUsSUFBSSxFQUFDLEdBQUcsYUFBSCxHQUFHLHVCQUFILEdBQUcsQ0FBRSxTQUFTLEdBQUUsRUFBRTtnQkFDMUQscURBQXFEO2FBQ3REO2lCQUFNO2dCQUNMLDhDQUE4QztnQkFDOUMsSUFBSSxNQUFNLEdBQUcsSUFBSSxrQkFBTSxFQUFFLENBQUM7Z0JBQzFCLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7Z0JBQ2xDLE1BQU0sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7Z0JBQ3BDLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO2FBQ3RCO1lBQ0QsSUFBSSxRQUFRLEVBQUU7Z0JBQ1osUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2Y7UUFDSCxDQUFDLEVBQ0QsVUFBQyxHQUFHLEVBQUUsSUFBVTtZQUNkLDhCQUE4QjtZQUM5Qix1REFBdUQ7WUFDdkQsSUFBSTtRQUNOLENBQUMsRUFDRCxLQUFLLENBQ04sQ0FBQztJQUNKLENBQUM7SUFFRCw0Q0FBaUIsR0FBakIsVUFBa0IsT0FBYSxFQUFFLEdBQVc7UUFDMUMsSUFBSSxPQUFPLElBQUksd0JBQUksQ0FBQyxPQUFPO1lBQUUsT0FBTztRQUNwQyxJQUFJLElBQUksQ0FBQyxTQUFTO1lBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JDLGlEQUFpRDtRQUNqRCxJQUFJLFNBQVMsR0FBRyxHQUFHLEdBQUcsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNyQywrQkFBK0I7UUFDL0IsUUFBUSxPQUFPLEVBQUU7WUFDZixLQUFLLHdCQUFJLENBQUMsOENBQThDO2dCQUN0RCxTQUFTLEdBQUcsb0JBQVUsQ0FBQyw2QkFBNkIsQ0FBQztnQkFDckQsTUFBTTtZQUNSLEtBQUssd0JBQUksQ0FBQywyQ0FBMkM7Z0JBQ25ELFNBQVMsR0FBRyxvQkFBVSxDQUFDLDRCQUE0QixDQUFDO2dCQUNwRCxNQUFNO1lBQ1IsS0FBSyx3QkFBSSxDQUFDLGdEQUFnRDtnQkFDeEQsU0FBUyxHQUFHLG9CQUFVLENBQUMsa0NBQWtDLENBQUM7Z0JBQzFELE1BQU07WUFDUixLQUFLLHdCQUFJLENBQUMsNkNBQTZDO2dCQUNyRCxTQUFTLEdBQUcsb0JBQVUsQ0FBQyxpQ0FBaUMsQ0FBQztnQkFDekQsTUFBTTtZQUNSLEtBQUssd0JBQUksQ0FBQyw2Q0FBNkM7Z0JBQ3JELFNBQVMsR0FBRyxvQkFBVSxDQUFDLDZCQUE2QixDQUFDO2dCQUNyRCxNQUFNO1lBQ1IsS0FBSyx3QkFBSSxDQUFDLCtCQUErQjtnQkFDdkMsU0FBUyxHQUFHLG9CQUFVLENBQUMsNkJBQTZCLENBQUM7Z0JBQ3JELE1BQU07WUFDUixLQUFLLHdCQUFJLENBQUMscURBQXFEO2dCQUM3RCxTQUFTLEdBQUcsb0JBQVUsQ0FBQyx3Q0FBd0MsQ0FBQztnQkFDaEUsTUFBTTtTQUNUO1FBQ0QsbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsb0JBQVUsRUFBRSxvQkFBVSxDQUFDLFVBQVUsRUFBRSxJQUFJLDJCQUFjLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDMUcsQ0FBQztJQUNELG9DQUFTLEdBQVQsVUFBVSxHQUFrQjtRQUMxQixJQUFJLE9BQU8sR0FBRyxvQkFBSyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1FBQzdELElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMvQixzQkFBWSxDQUFDLElBQUksQ0FBQyxzQkFBWSxDQUFDLHFCQUFxQixFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzdELDREQUE0RDtJQUM5RCxDQUFDO0lBQ0gsdUJBQUM7QUFBRCxDQTVNQSxBQTRNQyxDQTVNNkMsK0JBQXVCLEdBNE1wRSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJlblRhdU1lc3NhZ2UgfSBmcm9tIFwiQGdhbWVsb290L2JlbnRhdS9iZW50YXVfcGJcIjtcbmltcG9ydCB7IFRvcGljIH0gZnJvbSBcIkBnYW1lbG9vdC90b3BpYy90b3BpY19wYlwiO1xuaW1wb3J0IHsgTXlBY2NvdW50Q2xpZW50IH0gZnJvbSBcIkBnYW1lbG9vdC9teWFjY291bnQvTXlhY2NvdW50U2VydmljZUNsaWVudFBiXCI7XG5pbXBvcnQge1xuICBBdmF0YXIsXG4gIEdlbmRlcixcbiAgTGV2ZWwsXG4gIExpc3RCYXNlc1JlcGx5LFxuICBMaXN0QmFzZXNSZXF1ZXN0LFxuICBMaXN0TGV2ZWxzUmVwbHksXG4gIExpc3RMZXZlbHNSZXF1ZXN0LFxuICBMaXN0TXlBdmF0YXJzUmVwbHksXG4gIExpc3RNeUF2YXRhcnNSZXF1ZXN0LFxuICBQcm9maWxlUmVwbHksXG4gIFVwZGF0ZUF2YXRhclJlcGx5LFxuICBVcGRhdGVBdmF0YXJSZXF1ZXN0LFxuICBVcGRhdGVQcm9maWxlUmVwbHksXG4gIFVwZGF0ZVByb2ZpbGVSZXF1ZXN0LFxufSBmcm9tIFwiQGdhbWVsb290L215YWNjb3VudC9teWFjY291bnRfcGJcIjtcbmltcG9ydCB7IENvZGUgfSBmcm9tIFwiQGdhbWVsb290L215YWNjb3VudC9teWFjY291bnRfY29kZV9wYlwiO1xuaW1wb3J0IHsgUGxheWFoIH0gZnJvbSBcIkBnYW1lbG9vdC9wbGF5YWgvcGxheWFoX3BiXCI7XG5pbXBvcnQgeyBIYW5kbGVyQ2xpZW50QmVuVGF1QmFzZSB9IGZyb20gXCJAZ2FtZWxvb3QvY2xpZW50LWJhc2UtaGFuZGxlci9idWlsZFwiO1xuaW1wb3J0IHsgRW1wdHkgfSBmcm9tIFwiZ29vZ2xlLXByb3RvYnVmL2dvb2dsZS9wcm90b2J1Zi9lbXB0eV9wYlwiO1xuaW1wb3J0IFBvcnRhbFRleHQgZnJvbSBcIi4uL1V0aWxzL1BvcnRhbFRleHRcIjtcbmltcG9ydCBDb25uZWN0RGVmaW5lIGZyb20gXCIuLi9EZWZpbmUvQ29ubmVjdERlZmluZVwiO1xuaW1wb3J0IEdhbWVEZWZpbmUgZnJvbSBcIi4uL0RlZmluZS9HYW1lRGVmaW5lXCI7XG5pbXBvcnQgeyBDb2RlU29ja2V0IH0gZnJvbSBcIi4uL1V0aWxzL0VudW1HYW1lXCI7XG5pbXBvcnQgRXZlbnRNYW5hZ2VyIGZyb20gXCIuLi9FdmVudC9FdmVudE1hbmFnZXJcIjtcbmltcG9ydCBVSU1hbmFnZXIgZnJvbSBcIi4uL01hbmFnZXIvVUlNYW5hZ2VyXCI7XG5pbXBvcnQgVGV4dE5vdGlmeSwgeyBUZXh0Tm90aWZ5RGF0YSB9IGZyb20gXCIuLi9NYW5hZ2VyL1RleHROb3RpZnlcIjtcbmltcG9ydCBMb2FkaW5nUG9wdXBTeXN0ZW0gZnJvbSBcIi4uL1BvcHVwL0xvYWRpbmdQb3B1cFN5c3RlbVwiO1xuaW1wb3J0IFdhcm5pbmdQb3B1cFN5c3RlbSBmcm9tIFwiLi4vUG9wdXAvV2FybmluZ1BvcHVwU3lzdGVtXCI7XG5pbXBvcnQgQmVuVGF1SGFuZGxlciBmcm9tIFwiLi9CZW5UYXVIYW5kbGVyXCI7XG5pbXBvcnQgSWRlbnRpdHlIYW5kbGVyIGZyb20gXCIuL0lkZW50aXR5SGFuZGxlclwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNeUFjY291bnRIYW5kbGVyIGV4dGVuZHMgSGFuZGxlckNsaWVudEJlblRhdUJhc2Uge1xuICBwcm90ZWN0ZWQgc3RhdGljIGluc3RhbmNlOiBNeUFjY291bnRIYW5kbGVyO1xuXG4gIHN0YXRpYyBnZXRJbnN0YW5jZSgpOiBNeUFjY291bnRIYW5kbGVyIHtcbiAgICBpZiAoIU15QWNjb3VudEhhbmRsZXIuaW5zdGFuY2UpIHtcbiAgICAgIE15QWNjb3VudEhhbmRsZXIuaW5zdGFuY2UgPSBuZXcgTXlBY2NvdW50SGFuZGxlcihCZW5UYXVIYW5kbGVyLmdldEluc3RhbmNlKCkpO1xuICAgICAgTXlBY2NvdW50SGFuZGxlci5pbnN0YW5jZS5jbGllbnQgPSBuZXcgTXlBY2NvdW50Q2xpZW50KENvbm5lY3REZWZpbmUuYWRkcmVzc1NhbkJheSwge30sIG51bGwpO1xuICAgICAgTXlBY2NvdW50SGFuZGxlci5pbnN0YW5jZS50b3BpYyA9IFRvcGljLk1ZX0FDQ09VTlRfTEVWRUxfVVA7XG4gICAgICBNeUFjY291bnRIYW5kbGVyLmluc3RhbmNlLm9uU3Vic2NyaWJlKCk7XG4gICAgfVxuICAgIHJldHVybiBNeUFjY291bnRIYW5kbGVyLmluc3RhbmNlO1xuICB9XG5cbiAgc3RhdGljIGRlc3Ryb3koKSB7XG4gICAgaWYgKE15QWNjb3VudEhhbmRsZXIuaW5zdGFuY2UpIE15QWNjb3VudEhhbmRsZXIuaW5zdGFuY2Uub25VblN1YnNjcmliZSgpO1xuICAgIE15QWNjb3VudEhhbmRsZXIuaW5zdGFuY2UgPSBudWxsO1xuICB9XG4gIHByaXZhdGUgY2xpZW50OiBNeUFjY291bnRDbGllbnQgPSBudWxsO1xuICBwcml2YXRlIGJhc2VzOiBNYXA8bnVtYmVyLCBzdHJpbmc+ID0gbmV3IE1hcDxudW1iZXIsIHN0cmluZz4oKTtcbiAgcHJpdmF0ZSBmcmFtZXM6IE1hcDxudW1iZXIsIHN0cmluZz4gPSBuZXcgTWFwPG51bWJlciwgc3RyaW5nPigpO1xuICBwcml2YXRlIHBsYXlhaDogUGxheWFoID0gbnVsbDtcblxuICBnZXQgRnJhbWVzKCkge1xuICAgIHJldHVybiB0aGlzLmZyYW1lcztcbiAgfVxuICBnZXQgQmFzZXMoKSB7XG4gICAgcmV0dXJuIHRoaXMuYmFzZXM7XG4gIH1cbiAgZ2V0IFBsYXlhaCgpIHtcbiAgICByZXR1cm4gdGhpcy5wbGF5YWg7XG4gIH1cblxuICBwcml2YXRlIGdldCBtZXRhRGF0YSgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgQXV0aG9yaXphdGlvbjogXCJCZWFyZXIgXCIgKyBJZGVudGl0eUhhbmRsZXIuZ2V0SW5zdGFuY2UoKS50b2tlbixcbiAgICB9O1xuICB9XG5cbiAgcHJvdGVjdGVkIHNob3dMb2FkaW5nKGNhbGxiYWNrOiBGdW5jdGlvbikge1xuICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpXG4gICAgICAub3BlblBvcHVwU3lzdGVtVjIoTG9hZGluZ1BvcHVwU3lzdGVtLCBHYW1lRGVmaW5lLkxvYWRpbmdQb3B1cFN5c3RlbSwgZmFsc2UsIG51bGwsIG51bGwpXG4gICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICB9KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBoaWRlTG9hZGluZygpIHtcbiAgICBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5jbG9zZVBvcHVwU3lzdGVtKExvYWRpbmdQb3B1cFN5c3RlbSwgR2FtZURlZmluZS5Mb2FkaW5nUG9wdXBTeXN0ZW0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIG9uU2hvd0VycihzdHI6IHN0cmluZywgb25DbGljaz86ICgpID0+IHZvaWQpIHtcbiAgICBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5vcGVuUG9wdXBTeXN0ZW0oV2FybmluZ1BvcHVwU3lzdGVtLCBHYW1lRGVmaW5lLldhcm5pbmdQb3B1cFN5c3RlbSwgc3RyLCBvbkNsaWNrLCBvbkNsaWNrKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBvbkRpc2Nvbm5lY3QoQ29kZVNvY2tldDogQ29kZVNvY2tldCwgbWVzc0Vycm9yOiBzdHJpbmcpIHtcbiAgICBFdmVudE1hbmFnZXIuZmlyZShFdmVudE1hbmFnZXIuT05fRElTQ09OTkVDVCwgQ29kZVNvY2tldCwgZmFsc2UsIG1lc3NFcnJvcik7XG4gIH1cblxuICBvbkdldEJhc2VQYXRoKGlkOiBudW1iZXIpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmJhc2VzLmdldChpZCk7XG4gIH1cblxuICBvbkdldEZyYW1lUGF0aChpZDogbnVtYmVyKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5mcmFtZXMuZ2V0KGlkKTtcbiAgfVxuXG4gIGhhbmRsZUVycjogKCkgPT4gdm9pZCA9IG51bGw7XG4gIG9uR2V0QXZhdGFyTGlzdChyZXNwb25zZTogKHJlc3BvbnNlOiBMaXN0TXlBdmF0YXJzUmVwbHkpID0+IHZvaWQgPSBudWxsKSB7XG4gICAgLy9jYy5sb2coXCJvbkdldEF2YXRhckxpc3RcIik7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIGxldCByZXF1ZXN0ID0gbmV3IExpc3RNeUF2YXRhcnNSZXF1ZXN0KCk7XG4gICAgdmFyIG1zZ0lkID0gc2VsZi5nZXRVbmlxdWVJZCgpO1xuICAgIGxldCBzZW5kUmVxdWVzdCA9ICgpID0+IHtcbiAgICAgIHNlbGYuY2xpZW50Lmxpc3RNeUF2YXRhcnMocmVxdWVzdCwgc2VsZi5tZXRhRGF0YSwgKGVyciwgcmVzKSA9PiB7XG4gICAgICAgIHNlbGYub25TZW5kUmVwbHkoZXJyLCByZXMsIG1zZ0lkKTtcbiAgICAgIH0pO1xuICAgIH07XG4gICAgdGhpcy5vblNlbmRSZXF1ZXN0KFxuICAgICAgbXNnSWQsXG4gICAgICBzZW5kUmVxdWVzdCxcbiAgICAgIChyZXM6IExpc3RNeUF2YXRhcnNSZXBseSkgPT4ge1xuICAgICAgICByZXMuZ2V0QmFzZXNMaXN0KCkuZm9yRWFjaCgoYmFzZSkgPT4ge1xuICAgICAgICAgIHRoaXMuYmFzZXMuc2V0KGJhc2UuZ2V0SWQoKSwgYmFzZS5nZXRQYXRoKCkpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmVzLmdldEZyYW1lc0xpc3QoKS5mb3JFYWNoKChmcmFtZSkgPT4ge1xuICAgICAgICAgIHRoaXMuZnJhbWVzLnNldChmcmFtZS5nZXRJZCgpLCBmcmFtZS5nZXRQYXRoKCkpO1xuICAgICAgICB9KTtcbiAgICAgICAgaWYgKHJlc3BvbnNlKSByZXNwb25zZShyZXMpO1xuICAgICAgfSxcbiAgICAgICgpID0+IHtcbiAgICAgICAgLy9jYy5sb2coXCJFcnJcIik7XG4gICAgICAgIGlmIChyZXNwb25zZSkgcmVzcG9uc2UobnVsbCk7XG4gICAgICB9LFxuICAgICAgZmFsc2VcbiAgICApO1xuICB9XG5cbiAgb25HZXRBdmF0YXJMaXN0V2hlblNpZ25VcChnZW5kZXI6IEdlbmRlciwgcmFuZG9tOiBib29sZWFuID0gdHJ1ZSwgbGltaXQ6IG51bWJlciA9IDEwLCByZXNwb25zZTogKHJlc3BvbnNlOiBMaXN0QmFzZXNSZXBseSkgPT4gdm9pZCA9IG51bGwpIHtcbiAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgbGV0IHJlcXVlc3QgPSBuZXcgTGlzdEJhc2VzUmVxdWVzdCgpO1xuICAgIHJlcXVlc3Quc2V0R2VuZGVyKGdlbmRlcik7XG4gICAgcmVxdWVzdC5zZXRSYW5kb20ocmFuZG9tKTtcbiAgICByZXF1ZXN0LnNldExpbWl0KGxpbWl0KTtcbiAgICB2YXIgbXNnSWQgPSBzZWxmLmdldFVuaXF1ZUlkKCk7XG4gICAgbGV0IHNlbmRSZXF1ZXN0ID0gKCkgPT4ge1xuICAgICAgc2VsZi5jbGllbnQubGlzdEJhc2VzKHJlcXVlc3QsIHNlbGYubWV0YURhdGEsIChlcnIsIHJlcykgPT4ge1xuICAgICAgICBzZWxmLm9uU2VuZFJlcGx5KGVyciwgcmVzLCBtc2dJZCk7XG4gICAgICB9KTtcbiAgICB9O1xuICAgIHRoaXMub25TZW5kUmVxdWVzdChcbiAgICAgIG1zZ0lkLFxuICAgICAgc2VuZFJlcXVlc3QsXG4gICAgICAocmVzOiBMaXN0QmFzZXNSZXBseSkgPT4ge1xuICAgICAgICB0aGlzLmZyYW1lcy5zZXQocmVzLmdldERlZmF1bHRGcmFtZSgpLmdldElkKCksIHJlcy5nZXREZWZhdWx0RnJhbWUoKS5nZXRQYXRoKCkpO1xuICAgICAgICByZXMuZ2V0QmFzZXNMaXN0KCkuZm9yRWFjaCgoYmFzZSkgPT4ge1xuICAgICAgICAgIHRoaXMuYmFzZXMuc2V0KGJhc2UuZ2V0SWQoKSwgYmFzZS5nZXRQYXRoKCkpO1xuICAgICAgICB9KTtcblxuICAgICAgICByZXNwb25zZShyZXMpO1xuICAgICAgfSxcbiAgICAgIG51bGwsXG4gICAgICBmYWxzZVxuICAgICk7XG4gIH1cblxuICBnZXRQcm9maWxlKHJlc3BvbnNlOiAocmVzcG9uc2U6IFByb2ZpbGVSZXBseSkgPT4gdm9pZCkge1xuICAgIC8vY2MubG9nKFwiZ2V0UHJvZmlsZSBcIiArIElkZW50aXR5SGFuZGxlci5nZXRJbnN0YW5jZSgpLnRva2VuKTtcblxuICAgIGxldCByZXF1ZXN0ID0gbmV3IEVtcHR5KCk7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIHZhciBtc2dJZCA9IHNlbGYuZ2V0VW5pcXVlSWQoKTtcbiAgICBsZXQgbWV0YURhdGEgPSB7IEF1dGhvcml6YXRpb246IFwiQmVhcmVyIFwiICsgSWRlbnRpdHlIYW5kbGVyLmdldEluc3RhbmNlKCkudG9rZW4gfTtcblxuICAgIGxldCBzZW5kUmVxdWVzdCA9ICgpID0+IHtcbiAgICAgIHNlbGYuY2xpZW50Lm1lKHJlcXVlc3QsIG1ldGFEYXRhLCAoZXJyLCByZXMpID0+IHtcbiAgICAgICAgc2VsZi5vblNlbmRSZXBseShlcnIsIHJlcywgbXNnSWQpO1xuICAgICAgfSk7XG4gICAgfTtcblxuICAgIHRoaXMub25TZW5kUmVxdWVzdChcbiAgICAgIG1zZ0lkLFxuICAgICAgc2VuZFJlcXVlc3QsXG4gICAgICAocmVzOiBQcm9maWxlUmVwbHkpID0+IHtcbiAgICAgICAgLy9jYy5sb2coXCJnZXRQcm9maWxlIGRvbmUgXCIpO1xuICAgICAgICBpZiAocmVzID09IG51bGwgfHwgIXJlcz8uaGFzUHJvZmlsZSgpIHx8ICFyZXM/Lmhhc0F2YXRhcigpKSB7XG4gICAgICAgICAgLy8gVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkub3BlblVwZGF0ZU5pY2tOYW1lUG9wdXAoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBVc2VyTWFuYWdlci5nZXRJbnN0YW5jZSgpLnNldFVzZXJEYXRhKHJlcyk7XG4gICAgICAgICAgbGV0IHBsYXlhaCA9IG5ldyBQbGF5YWgoKTtcbiAgICAgICAgICBwbGF5YWguc2V0VXNlcklkKHJlcy5nZXRTYWZlSWQoKSk7XG4gICAgICAgICAgcGxheWFoLnNldFByb2ZpbGUocmVzLmdldFByb2ZpbGUoKSk7XG4gICAgICAgICAgcGxheWFoLnNldEF2YXRhcihyZXMuZ2V0QXZhdGFyKCkpO1xuICAgICAgICAgIHNlbGYucGxheWFoID0gcGxheWFoO1xuICAgICAgICB9XG4gICAgICAgIGlmIChyZXNwb25zZSkge1xuICAgICAgICAgIHJlc3BvbnNlKHJlcyk7XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICAoZXJyLCBjb2RlOiBDb2RlKSA9PiB7XG4gICAgICAgIC8vIGlmIChjb2RlICE9IENvZGUuVU5LTk9XTikge1xuICAgICAgICAvLyAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpLm9wZW5VcGRhdGVOaWNrTmFtZVBvcHVwKCk7XG4gICAgICAgIC8vIH1cbiAgICAgIH0sXG4gICAgICBmYWxzZVxuICAgICk7XG4gIH1cblxuICBoYW5kbGVDdXN0b21FcnJvcihlcnJDb2RlOiBDb2RlLCBlcnI6IHN0cmluZykge1xuICAgIGlmIChlcnJDb2RlID09IENvZGUuVU5LTk9XTikgcmV0dXJuO1xuICAgIGlmICh0aGlzLmhhbmRsZUVycikgdGhpcy5oYW5kbGVFcnIoKTtcbiAgICAvL2NjLmxvZyhcImhhbmRsZUN1c3RvbUVycm9yIFwiICsgXCJlcnJDb2RlXCIgKyBlcnIpO1xuICAgIGxldCBlcnJTdHJpbmcgPSBcIltcIiArIGVyckNvZGUgKyBcIl0gXCI7XG4gICAgLy9jYy5sb2coXCJlcnJjb2RlOiBcIiwgZXJyQ29kZSk7XG4gICAgc3dpdGNoIChlcnJDb2RlKSB7XG4gICAgICBjYXNlIENvZGUuTVlfQUNDT1VOVF9JTlZBTElEX0RJU1BMQVlfTkFNRV9SRVFVSVJFX0xFTkdUSDpcbiAgICAgICAgZXJyU3RyaW5nID0gUG9ydGFsVGV4dC5QT1JfTk9USUZZX05JQ0tOQU1FX1RPT19TSE9SVDtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIENvZGUuTVlfQUNDT1VOVF9JTlZBTElEX0RJU1BMQVlfTkFNRV9PVkVSX0xFTkdUSDpcbiAgICAgICAgZXJyU3RyaW5nID0gUG9ydGFsVGV4dC5QT1JfTk9USUZZX05JQ0tOQU1FX1RPT19MT05HO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgQ29kZS5NWV9BQ0NPVU5UX0lOVkFMSURfRElTUExBWV9OQU1FX1NUQVJUX1NQQUNFX0NIQVI6XG4gICAgICAgIGVyclN0cmluZyA9IFBvcnRhbFRleHQuUE9SX05PVElGWV9OSUNLTkFNRV9JTlZBTElEX1BSRUZJWDtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIENvZGUuTVlfQUNDT1VOVF9JTlZBTElEX0RJU1BMQVlfTkFNRV9TQU1FX1VTRVJOQU1FOlxuICAgICAgICBlcnJTdHJpbmcgPSBQb3J0YWxUZXh0LlBPUl9OT1RJRllfTklDS05BTUVfU0FNRV9VU0VSTkFNRTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIENvZGUuTVlfQUNDT1VOVF9JTlZBTElEX0RJU1BMQVlfTkFNRV9IQVNfUFJPRkFOSVRZOlxuICAgICAgICBlcnJTdHJpbmcgPSBQb3J0YWxUZXh0LlBPUl9OT1RJRllfTklDS05BTUVfUFJPRkFOSVRZO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgQ29kZS5NWV9BQ0NPVU5UX0lOVkFMSURfRElTUExBWV9OQU1FOlxuICAgICAgICBlcnJTdHJpbmcgPSBQb3J0YWxUZXh0LlBPUl9OT1RJRllfTklDS05BTUVfVE9PX1NIT1JUO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgQ29kZS5NWV9BQ0NPVU5UX0lOVkFMSURfRElTUExBWV9OQU1FX0hBU19JTlZBTElEX0NIQVJBQ1RFUjpcbiAgICAgICAgZXJyU3RyaW5nID0gUG9ydGFsVGV4dC5QT1JfTk9USUZZX05JQ0tOQU1FX0hBU19CQU5ORURfQ0hBUkFDVEVSO1xuICAgICAgICBicmVhaztcbiAgICB9XG4gICAgVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkub3Blbk5vdGlmeShUZXh0Tm90aWZ5LCBHYW1lRGVmaW5lLlRleHROb3RpZnksIG5ldyBUZXh0Tm90aWZ5RGF0YShlcnJTdHJpbmcsIDIpKTtcbiAgfVxuICBvblJlY2VpdmUobXNnOiBCZW5UYXVNZXNzYWdlKSB7XG4gICAgbGV0IG1lc3NhZ2UgPSBMZXZlbC5kZXNlcmlhbGl6ZUJpbmFyeShtc2cuZ2V0UGF5bG9hZF9hc1U4KCkpO1xuICAgIGxldCBsZXZlbCA9IG1lc3NhZ2UuZ2V0TGV2ZWwoKTtcbiAgICBFdmVudE1hbmFnZXIuZmlyZShFdmVudE1hbmFnZXIuVVBEQVRFX0JBREdFX0xFVkVMX1VQLCBsZXZlbCk7XG4gICAgLy8gVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkub3BlbkFuaW1hdGlvbkxldmVsVXBQb3B1cChsZXZlbCk7XG4gIH1cbn1cbiJdfQ==