
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Component/ScaleEffectBtnComp.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b7025zYu0pPu4YaC2FstoFh', 'ScaleEffectBtnComp');
// Game/Lobby/Component/ScaleEffectBtnComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ScaleEffectBtnComp = /** @class */ (function (_super) {
    __extends(ScaleEffectBtnComp, _super);
    function ScaleEffectBtnComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.playOnAwake = true;
        _this.delayFirst = 0;
        return _this;
    }
    ScaleEffectBtnComp.prototype.start = function () {
        var _this = this;
        if (this.playOnAwake) {
            this.scaleEffect();
        }
        this.node.on("click", function () {
            cc.Tween.stopAllByTarget(_this.node);
        }, this);
    };
    ScaleEffectBtnComp.prototype.scaleEffect = function () {
        var duration = 0.2;
        var child = this.node;
        var scale = cc.Vec3.ZERO;
        this.node.getScale(scale);
        var offset = 0.1;
        cc.tween(child)
            .sequence(cc.tween(child).delay(this.delayFirst), cc.tween(child).to(duration, { scaleX: scale.x + offset }), cc.tween(child).parallel(cc.tween(child).to(duration, { scale: scale.x }), cc.tween(child).to(duration, { scaleY: scale.y + offset })), cc.tween(child).to(duration, { scale: scale.x }))
            .repeatForever()
            .start();
    };
    ScaleEffectBtnComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.Boolean)
    ], ScaleEffectBtnComp.prototype, "playOnAwake", void 0);
    __decorate([
        property(cc.Float)
    ], ScaleEffectBtnComp.prototype, "delayFirst", void 0);
    ScaleEffectBtnComp = __decorate([
        ccclass
    ], ScaleEffectBtnComp);
    return ScaleEffectBtnComp;
}(cc.Component));
exports.default = ScaleEffectBtnComp;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L0NvbXBvbmVudC9TY2FsZUVmZmVjdEJ0bkNvbXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ00sSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBZ0Qsc0NBQVk7SUFBNUQ7UUFBQSxxRUF1Q0M7UUFwQ0csaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsZ0JBQVUsR0FBVyxDQUFDLENBQUM7O0lBaUMzQixDQUFDO0lBL0JHLGtDQUFLLEdBQUw7UUFBQSxpQkFRQztRQVBHLElBQUcsSUFBSSxDQUFDLFdBQVcsRUFBQztZQUNoQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7U0FDckI7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUM7WUFDakIsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQ3ZDLENBQUMsRUFBQyxJQUFJLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCx3Q0FBVyxHQUFYO1FBQ0ksSUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ25CLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDdEIsSUFBSSxLQUFLLEdBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0IsSUFBSSxNQUFNLEdBQUcsR0FBRyxDQUFDO1FBQ2pCLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO2FBQ1osUUFBUSxDQUNQLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFDdEMsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDLEdBQUcsTUFBTSxFQUFFLENBQUMsRUFDMUQsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLE1BQU0sRUFBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLENBQUMsRUFDckksRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUNqRDthQUNBLGFBQWEsRUFBRTthQUNmLEtBQUssRUFBRSxDQUFDO0lBQ2YsQ0FBQztJQUVTLHNDQUFTLEdBQW5CO1FBQ0ksRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFsQ0Q7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzsyREFDTztJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzBEQUNJO0lBTk4sa0JBQWtCO1FBRHRDLE9BQU87T0FDYSxrQkFBa0IsQ0F1Q3RDO0lBQUQseUJBQUM7Q0F2Q0QsQUF1Q0MsQ0F2QytDLEVBQUUsQ0FBQyxTQUFTLEdBdUMzRDtrQkF2Q29CLGtCQUFrQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTY2FsZUVmZmVjdEJ0bkNvbXAgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KGNjLkJvb2xlYW4pXG4gICAgcGxheU9uQXdha2U6IGJvb2xlYW4gPSB0cnVlO1xuICBcbiAgICBAcHJvcGVydHkoY2MuRmxvYXQpXG4gICAgZGVsYXlGaXJzdDogbnVtYmVyID0gMDtcblxuICAgIHN0YXJ0ICgpIHtcbiAgICAgICAgaWYodGhpcy5wbGF5T25Bd2FrZSl7XG4gICAgICAgICAgICB0aGlzLnNjYWxlRWZmZWN0KClcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubm9kZS5vbihcImNsaWNrXCIsKCk9PntcbiAgICAgICAgICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLm5vZGUpXG4gICAgICAgIH0sdGhpcyk7XG4gICAgfVxuXG4gICAgc2NhbGVFZmZlY3QoKXtcbiAgICAgICAgbGV0IGR1cmF0aW9uID0gMC4yO1xuICAgICAgICBsZXQgY2hpbGQgPSB0aGlzLm5vZGU7XG4gICAgICAgIGxldCBzY2FsZTogY2MuVmVjMyA9IGNjLlZlYzMuWkVSTztcbiAgICAgICAgIHRoaXMubm9kZS5nZXRTY2FsZShzY2FsZSk7XG4gICAgICAgIGxldCBvZmZzZXQgPSAwLjE7XG4gICAgICAgIGNjLnR3ZWVuKGNoaWxkKVxuICAgICAgICAgIC5zZXF1ZW5jZShcbiAgICAgICAgICAgIGNjLnR3ZWVuKGNoaWxkKS5kZWxheSh0aGlzLmRlbGF5Rmlyc3QpLFxuICAgICAgICAgICAgY2MudHdlZW4oY2hpbGQpLnRvKGR1cmF0aW9uLCB7IHNjYWxlWDogc2NhbGUueCArIG9mZnNldCB9KSxcbiAgICAgICAgICAgIGNjLnR3ZWVuKGNoaWxkKS5wYXJhbGxlbChjYy50d2VlbihjaGlsZCkudG8oZHVyYXRpb24sIHsgc2NhbGU6IHNjYWxlLnggfSksIGNjLnR3ZWVuKGNoaWxkKS50byhkdXJhdGlvbiwgeyBzY2FsZVk6c2NhbGUueSArIG9mZnNldCB9KSksXG4gICAgICAgICAgICBjYy50d2VlbihjaGlsZCkudG8oZHVyYXRpb24sIHsgc2NhbGU6IHNjYWxlLnggfSlcbiAgICAgICAgICApXG4gICAgICAgICAgLnJlcGVhdEZvcmV2ZXIoKVxuICAgICAgICAgIC5zdGFydCgpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBvbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLm5vZGUpO1xuICAgIH1cblxufVxuIl19