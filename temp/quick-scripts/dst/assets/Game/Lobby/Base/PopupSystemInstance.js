
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Base/PopupSystemInstance.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'dd9f6nX6PpAzakGwtMF+x/M', 'PopupSystemInstance');
// Portal/Common/Scripts/Managers/UIManager/Base/PopupSystemInstance.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var PopupInstance_1 = require("./PopupInstance");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var PopupSystemInstance = /** @class */ (function (_super) {
    __extends(PopupSystemInstance, _super);
    function PopupSystemInstance() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PopupSystemInstance.prototype.onLoad = function () {
        // if (!this.background) this.background = this.node.getChildByName("darkBackground").getComponent(cc.Widget);
        // if (this.background) {
        //   this.background.isAlignLeft = true;
        //   this.background.isAbsoluteRight = true;
        //   this.background.isAbsoluteTop = true;
        //   this.background.isAlignBottom = true;
        //   this.background.left = -500;
        //   this.background.top = -500;
        //   this.background.bottom = -500;
        //   this.background.right = -500;
        //   this.background.target = cc.director.getScene().getChildByName("Canvas");
        // }
        // this._animation = this.getComponent(cc.Animation);
        this.node.active = false;
    };
    PopupSystemInstance = __decorate([
        ccclass
    ], PopupSystemInstance);
    return PopupSystemInstance;
}(PopupInstance_1.default));
exports.default = PopupSystemInstance;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9Qb3J0YWwvQ29tbW9uL1NjcmlwdHMvTWFuYWdlcnMvVUlNYW5hZ2VyL0Jhc2UvUG9wdXBTeXN0ZW1JbnN0YW5jZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVsRixpREFBNEM7QUFFdEMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBaUQsdUNBQWE7SUFBOUQ7O0lBaUJBLENBQUM7SUFoQkMsb0NBQU0sR0FBTjtRQUNFLDhHQUE4RztRQUM5Ryx5QkFBeUI7UUFDekIsd0NBQXdDO1FBQ3hDLDRDQUE0QztRQUM1QywwQ0FBMEM7UUFDMUMsMENBQTBDO1FBQzFDLGlDQUFpQztRQUNqQyxnQ0FBZ0M7UUFDaEMsbUNBQW1DO1FBQ25DLGtDQUFrQztRQUNsQyw4RUFBOEU7UUFDOUUsSUFBSTtRQUNKLHFEQUFxRDtRQUNyRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQWhCa0IsbUJBQW1CO1FBRHZDLE9BQU87T0FDYSxtQkFBbUIsQ0FpQnZDO0lBQUQsMEJBQUM7Q0FqQkQsQUFpQkMsQ0FqQmdELHVCQUFhLEdBaUI3RDtrQkFqQm9CLG1CQUFtQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgUG9wdXBJbnN0YW5jZSBmcm9tIFwiLi9Qb3B1cEluc3RhbmNlXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQb3B1cFN5c3RlbUluc3RhbmNlIGV4dGVuZHMgUG9wdXBJbnN0YW5jZSB7XG4gIG9uTG9hZCgpIHtcbiAgICAvLyBpZiAoIXRoaXMuYmFja2dyb3VuZCkgdGhpcy5iYWNrZ3JvdW5kID0gdGhpcy5ub2RlLmdldENoaWxkQnlOYW1lKFwiZGFya0JhY2tncm91bmRcIikuZ2V0Q29tcG9uZW50KGNjLldpZGdldCk7XG4gICAgLy8gaWYgKHRoaXMuYmFja2dyb3VuZCkge1xuICAgIC8vICAgdGhpcy5iYWNrZ3JvdW5kLmlzQWxpZ25MZWZ0ID0gdHJ1ZTtcbiAgICAvLyAgIHRoaXMuYmFja2dyb3VuZC5pc0Fic29sdXRlUmlnaHQgPSB0cnVlO1xuICAgIC8vICAgdGhpcy5iYWNrZ3JvdW5kLmlzQWJzb2x1dGVUb3AgPSB0cnVlO1xuICAgIC8vICAgdGhpcy5iYWNrZ3JvdW5kLmlzQWxpZ25Cb3R0b20gPSB0cnVlO1xuICAgIC8vICAgdGhpcy5iYWNrZ3JvdW5kLmxlZnQgPSAtNTAwO1xuICAgIC8vICAgdGhpcy5iYWNrZ3JvdW5kLnRvcCA9IC01MDA7XG4gICAgLy8gICB0aGlzLmJhY2tncm91bmQuYm90dG9tID0gLTUwMDtcbiAgICAvLyAgIHRoaXMuYmFja2dyb3VuZC5yaWdodCA9IC01MDA7XG4gICAgLy8gICB0aGlzLmJhY2tncm91bmQudGFyZ2V0ID0gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShcIkNhbnZhc1wiKTtcbiAgICAvLyB9XG4gICAgLy8gdGhpcy5fYW5pbWF0aW9uID0gdGhpcy5nZXRDb21wb25lbnQoY2MuQW5pbWF0aW9uKTtcbiAgICB0aGlzLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gIH1cbn1cbiJdfQ==