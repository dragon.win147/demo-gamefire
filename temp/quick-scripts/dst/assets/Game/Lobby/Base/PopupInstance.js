
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Base/PopupInstance.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0b361grLhVCgY/G5sbs028I', 'PopupInstance');
// Game/Lobby/Base/PopupInstance.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var PopupInstance = /** @class */ (function (_super) {
    __extends(PopupInstance, _super);
    function PopupInstance() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.background = null;
        _this.panel = null;
        _this.normalPanelSize = 1;
        _this.scaleWhenOpenAndClosePanelSize = 1.05;
        _this._open = false;
        _this._isShowing = false;
        return _this;
    }
    PopupInstance.prototype.onDestroy = function () {
        this.unscheduleAllCallbacks();
    };
    PopupInstance.prototype.onLoad = function () {
        var _a;
        if (!this.background)
            this.background = (_a = this.node.getChildByName("darkBackground")) === null || _a === void 0 ? void 0 : _a.getComponent(cc.Widget);
        if (this.background) {
            this.background.isAlignLeft = true;
            this.background.isAbsoluteRight = true;
            this.background.isAbsoluteTop = true;
            this.background.isAlignBottom = true;
            this.background.left = -500;
            this.background.top = -500;
            this.background.bottom = -500;
            this.background.right = -500;
            this.background.target = cc.director.getScene().getChildByName("Canvas");
        }
        this._animation = this.getComponent(cc.Animation);
        if (!this._isShowing) {
            this.node.active = false;
        }
    };
    PopupInstance.prototype.showPopup = function () {
        var _this = this;
        if (this.panel != null) {
            this._isShowing = true;
            this.panel.scale = 0;
            this.panel.opacity = 0;
            this.node.active = true;
            cc.Tween.stopAllByTarget(this.panel);
            cc.tween(this.panel)
                .to(0, { scale: 0, opacity: 0 })
                .to(0.2, { scale: this.normalPanelSize * this.scaleWhenOpenAndClosePanelSize, opacity: 255 })
                .to(0.1, { scale: this.normalPanelSize })
                .call(function () {
                _this.showDone();
            })
                .start();
        }
        else {
            this.node.active = true;
            if (this._animation != null) {
                this._animation.stop();
                this._animation.play("Show");
            }
            else {
                this.showDone();
            }
        }
    };
    PopupInstance.prototype.hidePopup = function () {
        var _this = this;
        if (this.panel != null) {
            cc.Tween.stopAllByTarget(this.panel);
            cc.tween(this.panel)
                .to(0.2, { scale: this.normalPanelSize * this.scaleWhenOpenAndClosePanelSize, opacity: 0 })
                .call(function () {
                _this._isShowing = false;
                _this.node.active = false;
                _this.closeDone();
            })
                .start();
        }
        else {
            if (this._animation != null) {
                this._animation.stop();
                this._animation.play("Hide");
            }
            else {
                this.closeDone();
            }
        }
    };
    PopupInstance.prototype.open = function (data, onYes, onNo) {
        if (onYes === void 0) { onYes = null; }
        if (onNo === void 0) { onNo = null; }
        this.beforeShow();
        this._open = true;
        this.showPopup();
        this._data = data;
        this._onYes = onYes;
        this._onNo = onNo;
        this.onShow(data);
    };
    PopupInstance.prototype.closeInstance = function () {
        if (!this._open)
            return;
        this._open = false;
        if (this._close)
            this._close();
        this.beforeClose();
        this.hidePopup();
    };
    PopupInstance.prototype.close = function (playSfx) {
        if (playSfx === void 0) { playSfx = true; }
        if (!this._open)
            return;
        this._open = false;
        if (this._close)
            this._close();
        this.beforeClose();
        this.hidePopup();
        // if (this._animation != null) {
        //   this._animation.stop();
        //   this._animation.play("Hide");
        // } else {
        //   this.closeDone();
        // }
    };
    PopupInstance.prototype.onYes = function () {
        if (this._onYes) {
            this._onYes();
        }
        this.close();
    };
    PopupInstance.prototype.onNo = function () {
        if (this._onNo) {
            this._onNo();
        }
        this.close();
    };
    //#region Call From Animation Event
    PopupInstance.prototype.showDone = function () {
        this.afterShow();
    };
    PopupInstance.prototype.closeDone = function () {
        if (this._open == false) {
            this.node.active = false;
            this.afterClose();
        }
    };
    //#endregion
    PopupInstance.prototype.onShow = function (data) { };
    PopupInstance.prototype.afterShow = function () { };
    PopupInstance.prototype.beforeShow = function () { };
    PopupInstance.prototype.beforeClose = function () { };
    PopupInstance.prototype.afterClose = function () { };
    __decorate([
        property(cc.Widget)
    ], PopupInstance.prototype, "background", void 0);
    __decorate([
        property(cc.Node)
    ], PopupInstance.prototype, "panel", void 0);
    __decorate([
        property
    ], PopupInstance.prototype, "normalPanelSize", void 0);
    __decorate([
        property
    ], PopupInstance.prototype, "scaleWhenOpenAndClosePanelSize", void 0);
    PopupInstance = __decorate([
        ccclass
    ], PopupInstance);
    return PopupInstance;
}(cc.Component));
exports.default = PopupInstance;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9HYW1lL0xvYmJ5L0Jhc2UvUG9wdXBJbnN0YW5jZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUU1RSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUEyQyxpQ0FBWTtJQUF2RDtRQUFBLHFFQTRKQztRQTFKQyxnQkFBVSxHQUFjLElBQUksQ0FBQztRQUU3QixXQUFLLEdBQVksSUFBSSxDQUFDO1FBRXRCLHFCQUFlLEdBQVcsQ0FBQyxDQUFDO1FBRTVCLG9DQUE4QixHQUFXLElBQUksQ0FBQztRQUc5QyxXQUFLLEdBQUcsS0FBSyxDQUFDO1FBRWQsZ0JBQVUsR0FBWSxLQUFLLENBQUM7O0lBK0k5QixDQUFDO0lBeklDLGlDQUFTLEdBQVQ7UUFDRSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRUQsOEJBQU0sR0FBTjs7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVU7WUFBRSxJQUFJLENBQUMsVUFBVSxTQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLDBDQUFFLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDNUcsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDdkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztZQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQztZQUM1QixJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQztZQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsQ0FBQztZQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDLEdBQUcsQ0FBQztZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUMxRTtRQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQzFCO0lBQ0gsQ0FBQztJQUNELGlDQUFTLEdBQVQ7UUFBQSxpQkF3QkM7UUF2QkMsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksRUFBRTtZQUN0QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUN4QixFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUNqQixFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUM7aUJBQy9CLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsOEJBQThCLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxDQUFDO2lCQUM1RixFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztpQkFDeEMsSUFBSSxDQUFDO2dCQUNKLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNsQixDQUFDLENBQUM7aUJBQ0QsS0FBSyxFQUFFLENBQUM7U0FDWjthQUFNO1lBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ3hCLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLEVBQUU7Z0JBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzlCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNqQjtTQUNGO0lBQ0gsQ0FBQztJQUNELGlDQUFTLEdBQVQ7UUFBQSxpQkFtQkM7UUFsQkMsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksRUFBRTtZQUN0QixFQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUNqQixFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLDhCQUE4QixFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztpQkFDMUYsSUFBSSxDQUFDO2dCQUNKLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO2dCQUN4QixLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUNuQixDQUFDLENBQUM7aUJBQ0QsS0FBSyxFQUFFLENBQUM7U0FDWjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksRUFBRTtnQkFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDOUI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQ2xCO1NBQ0Y7SUFDSCxDQUFDO0lBRU0sNEJBQUksR0FBWCxVQUFZLElBQUksRUFBRSxLQUF3QixFQUFFLElBQXVCO1FBQWpELHNCQUFBLEVBQUEsWUFBd0I7UUFBRSxxQkFBQSxFQUFBLFdBQXVCO1FBQ2pFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNsQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQixDQUFDO0lBRU0scUNBQWEsR0FBcEI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUs7WUFBRSxPQUFPO1FBQ3hCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksSUFBSSxDQUFDLE1BQU07WUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRVMsNkJBQUssR0FBZixVQUFnQixPQUF1QjtRQUF2Qix3QkFBQSxFQUFBLGNBQXVCO1FBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSztZQUFFLE9BQU87UUFDeEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxJQUFJLENBQUMsTUFBTTtZQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLGlDQUFpQztRQUNqQyw0QkFBNEI7UUFDNUIsa0NBQWtDO1FBQ2xDLFdBQVc7UUFDWCxzQkFBc0I7UUFDdEIsSUFBSTtJQUNOLENBQUM7SUFFTSw2QkFBSyxHQUFaO1FBQ0UsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ2Y7UUFDRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDZixDQUFDO0lBRU0sNEJBQUksR0FBWDtRQUNFLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNkLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUNkO1FBQ0QsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2YsQ0FBQztJQUVELG1DQUFtQztJQUM1QixnQ0FBUSxHQUFmO1FBQ0UsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFTSxpQ0FBUyxHQUFoQjtRQUNFLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxLQUFLLEVBQUU7WUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztTQUNuQjtJQUNILENBQUM7SUFFRCxZQUFZO0lBRUYsOEJBQU0sR0FBaEIsVUFBaUIsSUFBSSxJQUFHLENBQUM7SUFFZixpQ0FBUyxHQUFuQixjQUF1QixDQUFDO0lBRWQsa0NBQVUsR0FBcEIsY0FBd0IsQ0FBQztJQUVmLG1DQUFXLEdBQXJCLGNBQXlCLENBQUM7SUFFaEIsa0NBQVUsR0FBcEIsY0FBd0IsQ0FBQztJQXpKekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztxREFDUztJQUU3QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNJO0lBRXRCO1FBREMsUUFBUTswREFDbUI7SUFFNUI7UUFEQyxRQUFRO3lFQUNxQztJQVIzQixhQUFhO1FBRGpDLE9BQU87T0FDYSxhQUFhLENBNEpqQztJQUFELG9CQUFDO0NBNUpELEFBNEpDLENBNUowQyxFQUFFLENBQUMsU0FBUyxHQTRKdEQ7a0JBNUpvQixhQUFhIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQb3B1cEluc3RhbmNlIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgQHByb3BlcnR5KGNjLldpZGdldClcbiAgYmFja2dyb3VuZDogY2MuV2lkZ2V0ID0gbnVsbDtcbiAgQHByb3BlcnR5KGNjLk5vZGUpXG4gIHBhbmVsOiBjYy5Ob2RlID0gbnVsbDtcbiAgQHByb3BlcnR5XG4gIG5vcm1hbFBhbmVsU2l6ZTogbnVtYmVyID0gMTtcbiAgQHByb3BlcnR5XG4gIHNjYWxlV2hlbk9wZW5BbmRDbG9zZVBhbmVsU2l6ZTogbnVtYmVyID0gMS4wNTtcblxuICBfYW5pbWF0aW9uOiBjYy5BbmltYXRpb247XG4gIF9vcGVuID0gZmFsc2U7XG4gIF9kYXRhO1xuICBfaXNTaG93aW5nOiBib29sZWFuID0gZmFsc2U7XG5cbiAgX29uWWVzOiAoKSA9PiB2b2lkO1xuICBfb25ObzogKCkgPT4gdm9pZDtcbiAgX2Nsb3NlOiAoKSA9PiB2b2lkO1xuXG4gIG9uRGVzdHJveSgpIHtcbiAgICB0aGlzLnVuc2NoZWR1bGVBbGxDYWxsYmFja3MoKTtcbiAgfVxuXG4gIG9uTG9hZCgpIHtcbiAgICBpZiAoIXRoaXMuYmFja2dyb3VuZCkgdGhpcy5iYWNrZ3JvdW5kID0gdGhpcy5ub2RlLmdldENoaWxkQnlOYW1lKFwiZGFya0JhY2tncm91bmRcIik/LmdldENvbXBvbmVudChjYy5XaWRnZXQpO1xuICAgIGlmICh0aGlzLmJhY2tncm91bmQpIHtcbiAgICAgIHRoaXMuYmFja2dyb3VuZC5pc0FsaWduTGVmdCA9IHRydWU7XG4gICAgICB0aGlzLmJhY2tncm91bmQuaXNBYnNvbHV0ZVJpZ2h0ID0gdHJ1ZTtcbiAgICAgIHRoaXMuYmFja2dyb3VuZC5pc0Fic29sdXRlVG9wID0gdHJ1ZTtcbiAgICAgIHRoaXMuYmFja2dyb3VuZC5pc0FsaWduQm90dG9tID0gdHJ1ZTtcbiAgICAgIHRoaXMuYmFja2dyb3VuZC5sZWZ0ID0gLTUwMDtcbiAgICAgIHRoaXMuYmFja2dyb3VuZC50b3AgPSAtNTAwO1xuICAgICAgdGhpcy5iYWNrZ3JvdW5kLmJvdHRvbSA9IC01MDA7XG4gICAgICB0aGlzLmJhY2tncm91bmQucmlnaHQgPSAtNTAwO1xuICAgICAgdGhpcy5iYWNrZ3JvdW5kLnRhcmdldCA9IGNjLmRpcmVjdG9yLmdldFNjZW5lKCkuZ2V0Q2hpbGRCeU5hbWUoXCJDYW52YXNcIik7XG4gICAgfVxuICAgIHRoaXMuX2FuaW1hdGlvbiA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkFuaW1hdGlvbik7XG4gICAgaWYgKCF0aGlzLl9pc1Nob3dpbmcpIHtcbiAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG4gIH1cbiAgc2hvd1BvcHVwKCkge1xuICAgIGlmICh0aGlzLnBhbmVsICE9IG51bGwpIHtcbiAgICAgIHRoaXMuX2lzU2hvd2luZyA9IHRydWU7XG4gICAgICB0aGlzLnBhbmVsLnNjYWxlID0gMDtcbiAgICAgIHRoaXMucGFuZWwub3BhY2l0eSA9IDA7XG4gICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgIGNjLlR3ZWVuLnN0b3BBbGxCeVRhcmdldCh0aGlzLnBhbmVsKTtcbiAgICAgIGNjLnR3ZWVuKHRoaXMucGFuZWwpXG4gICAgICAgIC50bygwLCB7IHNjYWxlOiAwLCBvcGFjaXR5OiAwIH0pXG4gICAgICAgIC50bygwLjIsIHsgc2NhbGU6IHRoaXMubm9ybWFsUGFuZWxTaXplICogdGhpcy5zY2FsZVdoZW5PcGVuQW5kQ2xvc2VQYW5lbFNpemUsIG9wYWNpdHk6IDI1NSB9KVxuICAgICAgICAudG8oMC4xLCB7IHNjYWxlOiB0aGlzLm5vcm1hbFBhbmVsU2l6ZSB9KVxuICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgdGhpcy5zaG93RG9uZSgpO1xuICAgICAgICB9KVxuICAgICAgICAuc3RhcnQoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICBpZiAodGhpcy5fYW5pbWF0aW9uICE9IG51bGwpIHtcbiAgICAgICAgdGhpcy5fYW5pbWF0aW9uLnN0b3AoKTtcbiAgICAgICAgdGhpcy5fYW5pbWF0aW9uLnBsYXkoXCJTaG93XCIpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5zaG93RG9uZSgpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuICBoaWRlUG9wdXAoKSB7XG4gICAgaWYgKHRoaXMucGFuZWwgIT0gbnVsbCkge1xuICAgICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRoaXMucGFuZWwpO1xuICAgICAgY2MudHdlZW4odGhpcy5wYW5lbClcbiAgICAgICAgLnRvKDAuMiwgeyBzY2FsZTogdGhpcy5ub3JtYWxQYW5lbFNpemUgKiB0aGlzLnNjYWxlV2hlbk9wZW5BbmRDbG9zZVBhbmVsU2l6ZSwgb3BhY2l0eTogMCB9KVxuICAgICAgICAuY2FsbCgoKSA9PiB7XG4gICAgICAgICAgdGhpcy5faXNTaG93aW5nID0gZmFsc2U7XG4gICAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgIHRoaXMuY2xvc2VEb25lKCk7XG4gICAgICAgIH0pXG4gICAgICAgIC5zdGFydCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAodGhpcy5fYW5pbWF0aW9uICE9IG51bGwpIHtcbiAgICAgICAgdGhpcy5fYW5pbWF0aW9uLnN0b3AoKTtcbiAgICAgICAgdGhpcy5fYW5pbWF0aW9uLnBsYXkoXCJIaWRlXCIpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5jbG9zZURvbmUoKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwdWJsaWMgb3BlbihkYXRhLCBvblllczogKCkgPT4gdm9pZCA9IG51bGwsIG9uTm86ICgpID0+IHZvaWQgPSBudWxsKSB7XG4gICAgdGhpcy5iZWZvcmVTaG93KCk7XG4gICAgdGhpcy5fb3BlbiA9IHRydWU7XG4gICAgdGhpcy5zaG93UG9wdXAoKTtcbiAgICB0aGlzLl9kYXRhID0gZGF0YTtcbiAgICB0aGlzLl9vblllcyA9IG9uWWVzO1xuICAgIHRoaXMuX29uTm8gPSBvbk5vO1xuICAgIHRoaXMub25TaG93KGRhdGEpO1xuICB9XG5cbiAgcHVibGljIGNsb3NlSW5zdGFuY2UoKSB7XG4gICAgaWYgKCF0aGlzLl9vcGVuKSByZXR1cm47XG4gICAgdGhpcy5fb3BlbiA9IGZhbHNlO1xuICAgIGlmICh0aGlzLl9jbG9zZSkgdGhpcy5fY2xvc2UoKTtcbiAgICB0aGlzLmJlZm9yZUNsb3NlKCk7XG4gICAgdGhpcy5oaWRlUG9wdXAoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBjbG9zZShwbGF5U2Z4OiBib29sZWFuID0gdHJ1ZSkge1xuICAgIGlmICghdGhpcy5fb3BlbikgcmV0dXJuO1xuICAgIHRoaXMuX29wZW4gPSBmYWxzZTtcbiAgICBpZiAodGhpcy5fY2xvc2UpIHRoaXMuX2Nsb3NlKCk7XG4gICAgdGhpcy5iZWZvcmVDbG9zZSgpO1xuICAgIHRoaXMuaGlkZVBvcHVwKCk7XG4gICAgLy8gaWYgKHRoaXMuX2FuaW1hdGlvbiAhPSBudWxsKSB7XG4gICAgLy8gICB0aGlzLl9hbmltYXRpb24uc3RvcCgpO1xuICAgIC8vICAgdGhpcy5fYW5pbWF0aW9uLnBsYXkoXCJIaWRlXCIpO1xuICAgIC8vIH0gZWxzZSB7XG4gICAgLy8gICB0aGlzLmNsb3NlRG9uZSgpO1xuICAgIC8vIH1cbiAgfVxuXG4gIHB1YmxpYyBvblllcygpIHtcbiAgICBpZiAodGhpcy5fb25ZZXMpIHtcbiAgICAgIHRoaXMuX29uWWVzKCk7XG4gICAgfVxuICAgIHRoaXMuY2xvc2UoKTtcbiAgfVxuXG4gIHB1YmxpYyBvbk5vKCkge1xuICAgIGlmICh0aGlzLl9vbk5vKSB7XG4gICAgICB0aGlzLl9vbk5vKCk7XG4gICAgfVxuICAgIHRoaXMuY2xvc2UoKTtcbiAgfVxuXG4gIC8vI3JlZ2lvbiBDYWxsIEZyb20gQW5pbWF0aW9uIEV2ZW50XG4gIHB1YmxpYyBzaG93RG9uZSgpIHtcbiAgICB0aGlzLmFmdGVyU2hvdygpO1xuICB9XG5cbiAgcHVibGljIGNsb3NlRG9uZSgpIHtcbiAgICBpZiAodGhpcy5fb3BlbiA9PSBmYWxzZSkge1xuICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgdGhpcy5hZnRlckNsb3NlKCk7XG4gICAgfVxuICB9XG5cbiAgLy8jZW5kcmVnaW9uXG5cbiAgcHJvdGVjdGVkIG9uU2hvdyhkYXRhKSB7fVxuXG4gIHByb3RlY3RlZCBhZnRlclNob3coKSB7fVxuXG4gIHByb3RlY3RlZCBiZWZvcmVTaG93KCkge31cblxuICBwcm90ZWN0ZWQgYmVmb3JlQ2xvc2UoKSB7fVxuXG4gIHByb3RlY3RlZCBhZnRlckNsb3NlKCkge31cbn1cbiJdfQ==