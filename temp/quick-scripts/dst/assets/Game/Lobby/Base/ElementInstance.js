
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Base/ElementInstance.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'df83f2AzbJGgb9XLI+x77cV', 'ElementInstance');
// Portal/Common/Scripts/Managers/UIManager/Base/ElementInstance.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ElementInstance = /** @class */ (function (_super) {
    __extends(ElementInstance, _super);
    function ElementInstance() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._open = false;
        return _this;
    }
    ElementInstance.prototype.onLoad = function () {
        this._animation = this.getComponent(cc.Animation);
        this.node.active = true;
    };
    ElementInstance.prototype.open = function (data, onYes, onNo) {
        if (onYes === void 0) { onYes = null; }
        if (onNo === void 0) { onNo = null; }
        this.beforeShow();
        this._open = true;
        this.node.active = true;
        this._data = data;
        this._onYes = onYes;
        this._onNo = onNo;
        this.onShow(data);
        if (this._animation != null) {
            this._animation.play("Show");
        }
        else {
            this.showDone();
        }
    };
    ElementInstance.prototype.close = function () {
        this._open = false;
        this._close();
        this.beforeClose();
        if (this._animation != null) {
            this._animation.play("Hide");
        }
        else {
            this.closeDone();
        }
    };
    ElementInstance.prototype.onYes = function () {
        if (this._onYes) {
            this._onYes();
        }
        this.close();
    };
    ElementInstance.prototype.onNo = function () {
        if (this._onNo) {
            this._onNo();
        }
        this.close();
    };
    //#region Call From Animation Event
    ElementInstance.prototype.showDone = function () {
        this.afterShow();
    };
    ElementInstance.prototype.closeDone = function () {
        if (this._open == false) {
            this.node.active = false;
            this.afterClose();
        }
    };
    //#endregion
    ElementInstance.prototype.onShow = function (data) { };
    ElementInstance.prototype.afterShow = function () { };
    ElementInstance.prototype.beforeShow = function () { };
    ElementInstance.prototype.beforeClose = function () { };
    ElementInstance.prototype.afterClose = function () { };
    ElementInstance = __decorate([
        ccclass
    ], ElementInstance);
    return ElementInstance;
}(cc.Component));
exports.default = ElementInstance;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9Qb3J0YWwvQ29tbW9uL1NjcmlwdHMvTWFuYWdlcnMvVUlNYW5hZ2VyL0Jhc2UvRWxlbWVudEluc3RhbmNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRTVFLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQTZDLG1DQUFZO0lBQXpEO1FBQUEscUVBNkVDO1FBM0VDLFdBQUssR0FBRyxLQUFLLENBQUM7O0lBMkVoQixDQUFDO0lBcEVDLGdDQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBRU0sOEJBQUksR0FBWCxVQUFZLElBQUksRUFBRSxLQUF3QixFQUFFLElBQXVCO1FBQWpELHNCQUFBLEVBQUEsWUFBd0I7UUFBRSxxQkFBQSxFQUFBLFdBQXVCO1FBQ2pFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO1lBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzlCO2FBQU07WUFDTCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7SUFDSCxDQUFDO0lBRU0sK0JBQUssR0FBWjtRQUNFLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNkLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO1lBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzlCO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDbEI7SUFDSCxDQUFDO0lBRU0sK0JBQUssR0FBWjtRQUNFLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNmLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUNmO1FBQ0QsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2YsQ0FBQztJQUVNLDhCQUFJLEdBQVg7UUFDRSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDZCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDZDtRQUNELElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFFRCxtQ0FBbUM7SUFDNUIsa0NBQVEsR0FBZjtRQUNFLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRU0sbUNBQVMsR0FBaEI7UUFDRSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUN6QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDbkI7SUFDSCxDQUFDO0lBRUQsWUFBWTtJQUVGLGdDQUFNLEdBQWhCLFVBQWlCLElBQUksSUFBRyxDQUFDO0lBRWYsbUNBQVMsR0FBbkIsY0FBdUIsQ0FBQztJQUVkLG9DQUFVLEdBQXBCLGNBQXdCLENBQUM7SUFFZixxQ0FBVyxHQUFyQixjQUF5QixDQUFDO0lBRWhCLG9DQUFVLEdBQXBCLGNBQXdCLENBQUM7SUE1RU4sZUFBZTtRQURuQyxPQUFPO09BQ2EsZUFBZSxDQTZFbkM7SUFBRCxzQkFBQztDQTdFRCxBQTZFQyxDQTdFNEMsRUFBRSxDQUFDLFNBQVMsR0E2RXhEO2tCQTdFb0IsZUFBZSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRWxlbWVudEluc3RhbmNlIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgX2FuaW1hdGlvbjogY2MuQW5pbWF0aW9uO1xuICBfb3BlbiA9IGZhbHNlO1xuICBfZGF0YTtcblxuICBfb25ZZXM6ICgpID0+IHZvaWQ7XG4gIF9vbk5vOiAoKSA9PiB2b2lkO1xuICBfY2xvc2U6ICgpID0+IHZvaWQ7XG5cbiAgb25Mb2FkKCkge1xuICAgIHRoaXMuX2FuaW1hdGlvbiA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkFuaW1hdGlvbik7XG4gICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gIH1cblxuICBwdWJsaWMgb3BlbihkYXRhLCBvblllczogKCkgPT4gdm9pZCA9IG51bGwsIG9uTm86ICgpID0+IHZvaWQgPSBudWxsKSB7XG4gICAgdGhpcy5iZWZvcmVTaG93KCk7XG4gICAgdGhpcy5fb3BlbiA9IHRydWU7XG4gICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgdGhpcy5fZGF0YSA9IGRhdGE7XG4gICAgdGhpcy5fb25ZZXMgPSBvblllcztcbiAgICB0aGlzLl9vbk5vID0gb25ObztcbiAgICB0aGlzLm9uU2hvdyhkYXRhKTtcbiAgICBpZiAodGhpcy5fYW5pbWF0aW9uICE9IG51bGwpIHtcbiAgICAgIHRoaXMuX2FuaW1hdGlvbi5wbGF5KFwiU2hvd1wiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zaG93RG9uZSgpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBjbG9zZSgpIHtcbiAgICB0aGlzLl9vcGVuID0gZmFsc2U7XG4gICAgdGhpcy5fY2xvc2UoKTtcbiAgICB0aGlzLmJlZm9yZUNsb3NlKCk7XG4gICAgaWYgKHRoaXMuX2FuaW1hdGlvbiAhPSBudWxsKSB7XG4gICAgICB0aGlzLl9hbmltYXRpb24ucGxheShcIkhpZGVcIik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuY2xvc2VEb25lKCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIG9uWWVzKCkge1xuICAgIGlmICh0aGlzLl9vblllcykge1xuICAgICAgdGhpcy5fb25ZZXMoKTtcbiAgICB9XG4gICAgdGhpcy5jbG9zZSgpO1xuICB9XG5cbiAgcHVibGljIG9uTm8oKSB7XG4gICAgaWYgKHRoaXMuX29uTm8pIHtcbiAgICAgIHRoaXMuX29uTm8oKTtcbiAgICB9XG4gICAgdGhpcy5jbG9zZSgpO1xuICB9XG5cbiAgLy8jcmVnaW9uIENhbGwgRnJvbSBBbmltYXRpb24gRXZlbnRcbiAgcHVibGljIHNob3dEb25lKCkge1xuICAgIHRoaXMuYWZ0ZXJTaG93KCk7XG4gIH1cblxuICBwdWJsaWMgY2xvc2VEb25lKCkge1xuICAgIGlmICh0aGlzLl9vcGVuID09IGZhbHNlKSB7XG4gICAgICB0aGlzLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICB0aGlzLmFmdGVyQ2xvc2UoKTtcbiAgICB9XG4gIH1cblxuICAvLyNlbmRyZWdpb25cblxuICBwcm90ZWN0ZWQgb25TaG93KGRhdGEpIHt9XG5cbiAgcHJvdGVjdGVkIGFmdGVyU2hvdygpIHt9XG5cbiAgcHJvdGVjdGVkIGJlZm9yZVNob3coKSB7fVxuXG4gIHByb3RlY3RlZCBiZWZvcmVDbG9zZSgpIHt9XG5cbiAgcHJvdGVjdGVkIGFmdGVyQ2xvc2UoKSB7fVxufVxuIl19