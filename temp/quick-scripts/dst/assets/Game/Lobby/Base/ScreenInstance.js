
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Game/Lobby/Base/ScreenInstance.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'de204veWBZNGJQXahCbRo54', 'ScreenInstance');
// Portal/Common/Scripts/Managers/UIManager/Base/ScreenInstance.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ScreenInstance = /** @class */ (function (_super) {
    __extends(ScreenInstance, _super);
    function ScreenInstance() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._open = false;
        return _this;
    }
    ScreenInstance.prototype.onLoad = function () {
        this._animation = this.getComponent(cc.Animation);
        this.node.active = false;
    };
    ScreenInstance.prototype.open = function (data) {
        this.beforeShow();
        this._open = true;
        this.node.active = true;
        this._data = data;
        this.onShow(data);
        if (this._animation != null) {
            this._animation.play("Show");
        }
        else {
            this.showDone();
        }
    };
    ScreenInstance.prototype.close = function () {
        this._open = false;
        this.beforeClose();
        if (this._animation != null) {
            this._animation.play("Hide");
        }
        else {
            this.closeDone();
        }
    };
    //#region Call From Animation Event
    ScreenInstance.prototype.showDone = function () {
        this.afterShow();
    };
    ScreenInstance.prototype.closeDone = function () {
        if (this._open == false) {
            this.node.active = false;
            this.afterClose();
        }
    };
    //#endregion
    ScreenInstance.prototype.onShow = function (data) { };
    ScreenInstance.prototype.afterShow = function () { };
    ScreenInstance.prototype.beforeShow = function () { };
    ScreenInstance.prototype.beforeClose = function () { };
    ScreenInstance.prototype.afterClose = function () { };
    ScreenInstance = __decorate([
        ccclass
    ], ScreenInstance);
    return ScreenInstance;
}(cc.Component));
exports.default = ScreenInstance;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9Qb3J0YWwvQ29tbW9uL1NjcmlwdHMvTWFuYWdlcnMvVUlNYW5hZ2VyL0Jhc2UvU2NyZWVuSW5zdGFuY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFNUUsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBNEMsa0NBQVk7SUFBeEQ7UUFBQSxxRUF1REM7UUFyREMsV0FBSyxHQUFHLEtBQUssQ0FBQzs7SUFxRGhCLENBQUM7SUFsREMsK0JBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFFTSw2QkFBSSxHQUFYLFVBQVksSUFBSTtRQUNkLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO1lBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzlCO2FBQU07WUFDTCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7SUFDSCxDQUFDO0lBRU0sOEJBQUssR0FBWjtRQUNFLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO1lBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzlCO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDbEI7SUFDSCxDQUFDO0lBRUQsbUNBQW1DO0lBQzVCLGlDQUFRLEdBQWY7UUFDRSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVNLGtDQUFTLEdBQWhCO1FBQ0UsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLEtBQUssRUFBRTtZQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDekIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQ25CO0lBQ0gsQ0FBQztJQUNELFlBQVk7SUFFRiwrQkFBTSxHQUFoQixVQUFpQixJQUFJLElBQUcsQ0FBQztJQUVmLGtDQUFTLEdBQW5CLGNBQXVCLENBQUM7SUFFZCxtQ0FBVSxHQUFwQixjQUF3QixDQUFDO0lBRWYsb0NBQVcsR0FBckIsY0FBeUIsQ0FBQztJQUVoQixtQ0FBVSxHQUFwQixjQUF3QixDQUFDO0lBdEROLGNBQWM7UUFEbEMsT0FBTztPQUNhLGNBQWMsQ0F1RGxDO0lBQUQscUJBQUM7Q0F2REQsQUF1REMsQ0F2RDJDLEVBQUUsQ0FBQyxTQUFTLEdBdUR2RDtrQkF2RG9CLGNBQWMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNjcmVlbkluc3RhbmNlIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgX2FuaW1hdGlvbjogY2MuQW5pbWF0aW9uO1xuICBfb3BlbiA9IGZhbHNlO1xuICBfZGF0YTtcblxuICBvbkxvYWQoKSB7XG4gICAgdGhpcy5fYW5pbWF0aW9uID0gdGhpcy5nZXRDb21wb25lbnQoY2MuQW5pbWF0aW9uKTtcbiAgICB0aGlzLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gIH1cblxuICBwdWJsaWMgb3BlbihkYXRhKSB7XG4gICAgdGhpcy5iZWZvcmVTaG93KCk7XG4gICAgdGhpcy5fb3BlbiA9IHRydWU7XG4gICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgdGhpcy5fZGF0YSA9IGRhdGE7XG4gICAgdGhpcy5vblNob3coZGF0YSk7XG4gICAgaWYgKHRoaXMuX2FuaW1hdGlvbiAhPSBudWxsKSB7XG4gICAgICB0aGlzLl9hbmltYXRpb24ucGxheShcIlNob3dcIik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2hvd0RvbmUoKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgY2xvc2UoKSB7XG4gICAgdGhpcy5fb3BlbiA9IGZhbHNlO1xuICAgIHRoaXMuYmVmb3JlQ2xvc2UoKTtcbiAgICBpZiAodGhpcy5fYW5pbWF0aW9uICE9IG51bGwpIHtcbiAgICAgIHRoaXMuX2FuaW1hdGlvbi5wbGF5KFwiSGlkZVwiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5jbG9zZURvbmUoKTtcbiAgICB9XG4gIH1cblxuICAvLyNyZWdpb24gQ2FsbCBGcm9tIEFuaW1hdGlvbiBFdmVudFxuICBwdWJsaWMgc2hvd0RvbmUoKSB7XG4gICAgdGhpcy5hZnRlclNob3coKTtcbiAgfVxuXG4gIHB1YmxpYyBjbG9zZURvbmUoKSB7XG4gICAgaWYgKHRoaXMuX29wZW4gPT0gZmFsc2UpIHtcbiAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgIHRoaXMuYWZ0ZXJDbG9zZSgpO1xuICAgIH1cbiAgfVxuICAvLyNlbmRyZWdpb25cblxuICBwcm90ZWN0ZWQgb25TaG93KGRhdGEpIHt9XG5cbiAgcHJvdGVjdGVkIGFmdGVyU2hvdygpIHt9XG5cbiAgcHJvdGVjdGVkIGJlZm9yZVNob3coKSB7fVxuXG4gIHByb3RlY3RlZCBiZWZvcmVDbG9zZSgpIHt9XG5cbiAgcHJvdGVjdGVkIGFmdGVyQ2xvc2UoKSB7fVxufVxuIl19