
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/SicboModuleAdapter.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6ba7d+qOqBGma3DDZg1mAyk', 'SicboModuleAdapter');
// SicboModuleAdapter.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sicbov2_code_pb_1 = require("@gameloot/sicbov2/sicbov2_code_pb");
var HandlerClientBase_1 = require("@gameloot/client-base-handler/build/HandlerClientBase");
var EventKey_1 = require("@gameloot/client-base-handler/build/EventKey");
var empty_pb_1 = require("google-protobuf/google/protobuf/empty_pb");
var playah_pb_1 = require("@gameloot/playah/playah_pb");
var bentau_pb_1 = require("@gameloot/bentau/bentau_pb");
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var whatsapp_pb_1 = require("@gameloot/whatsapp/whatsapp_pb");
var MyHistoryServiceClientPb_1 = require("@gameloot/myhistory/MyHistoryServiceClientPb");
var myhistory_pb_1 = require("@gameloot/myhistory/myhistory_pb");
var LeaderboardServiceClientPb_1 = require("@gameloot/leaderboard/LeaderboardServiceClientPb");
var leaderboard_pb_1 = require("@gameloot/leaderboard/leaderboard_pb");
var conveyor_pb_1 = require("@gameloot/conveyor/conveyor_pb");
var Sicbov2ServiceClientPb_1 = require("@gameloot/sicbov2/Sicbov2ServiceClientPb");
var sicbov2_pb_1 = require("@gameloot/sicbov2/sicbov2_pb");
var build_1 = require("@gameloot/client-base-handler/build");
var timestamp_pb_1 = require("google-protobuf/google/protobuf/timestamp_pb");
var sicbov2_pb_2 = require("@gameloot/sicbov2/sicbov2_pb");
var PotterServiceClientPb_1 = require("@gameloot/potter/PotterServiceClientPb");
var potter_pb_1 = require("@gameloot/potter/potter_pb");
var SicboModuleAdapter = /** @class */ (function () {
    function SicboModuleAdapter() {
    }
    SicboModuleAdapter.getAllRefs = function () {
        return {
            Code: sicbov2_code_pb_1.Code, HandlerClientBase: HandlerClientBase_1.default, Empty: empty_pb_1.Empty, Playah: playah_pb_1.Playah,
            BenTauMessage: bentau_pb_1.BenTauMessage, BenTauReply: bentau_pb_1.BenTauReply, Topic: topic_pb_1.Topic,
            HistoryClient: MyHistoryServiceClientPb_1.MyHistoryClient, GetChannelHistoryReply: myhistory_pb_1.GetChannelHistoryReply, GetPersonalHistoryReply: myhistory_pb_1.GetPersonalHistoryReply, GetChannelHistoryRequest: myhistory_pb_1.GetChannelHistoryRequest, GetPersonalHistoryRequest: myhistory_pb_1.GetPersonalHistoryRequest,
            LeaderboardClient: LeaderboardServiceClientPb_1.LeaderboardClient, RankEarningRequest: leaderboard_pb_1.RankEarningRequest, RankEarningReply: leaderboard_pb_1.RankEarningReply, DetailedLeader: leaderboard_pb_1.DetailedLeader, Cutoff: leaderboard_pb_1.Cutoff,
            FileField: conveyor_pb_1.FileField, ResourceField: conveyor_pb_1.ResourceField,
            SicboClient: Sicbov2ServiceClientPb_1.SicboClient,
            BetDoubleReply: sicbov2_pb_1.BetDoubleReply, BetDoubleRequest: sicbov2_pb_1.BetDoubleRequest, BetReply: sicbov2_pb_1.BetReply, BetRequest: sicbov2_pb_1.BetRequest, Channel: sicbov2_pb_1.Channel, Door: sicbov2_pb_1.Door,
            GetOthersReply: sicbov2_pb_1.GetOthersReply, GetOthersRequest: sicbov2_pb_1.GetOthersRequest, LeaveRequest: sicbov2_pb_1.LeaveRequest, Message: sicbov2_pb_1.Message, ReBetReply: sicbov2_pb_1.ReBetReply, AutoJoinReply: sicbov2_pb_2.AutoJoinReply,
            ReBetRequest: sicbov2_pb_1.ReBetRequest, State: sicbov2_pb_1.State, Result: sicbov2_pb_1.Result, Event: sicbov2_pb_1.Event, DoorAndBetAmount: sicbov2_pb_1.DoorAndBetAmount, Status: sicbov2_pb_1.Status, ChannelHistoryMetadata: sicbov2_pb_1.ChannelHistoryMetadata,
            PersonalHistoryMetadata: sicbov2_pb_1.PersonalHistoryMetadata, Item: sicbov2_pb_1.Item, ChipLevels: sicbov2_pb_1.ChipLevels, StageTime: sicbov2_pb_1.StageTime, LeaveReply: sicbov2_pb_1.LeaveReply,
            HandlerClientBenTauBase: build_1.HandlerClientBenTauBase, CodeSocket: build_1.CodeSocket, Timestamp: timestamp_pb_1.Timestamp, EventKey: EventKey_1.default, ListCurrentJackpotsReply: potter_pb_1.ListCurrentJackpotsReply, GetHistoryReply: potter_pb_1.GetHistoryReply, ListCurrentJackpotsRequest: potter_pb_1.ListCurrentJackpotsRequest, GetHistoryRequest: potter_pb_1.GetHistoryRequest, PotterClient: PotterServiceClientPb_1.PotterClient, JackpotHistoryMetadata: sicbov2_pb_1.JackpotHistoryMetadata,
            WhatAppMessage: whatsapp_pb_1.Message,
            HealthCheckRequest: sicbov2_pb_1.HealthCheckRequest, HealthCheckReply: sicbov2_pb_1.HealthCheckReply
        };
        // let moduleAdapter = null;
        // const adapterName = "SicboAdapterImport"
        // try {
        //   moduleAdapter = require(adapterName)[adapterName];
        // } catch {
        //   //cc.log("new token get Chạy lúc gắn vào portal ");
        // }
        // if (moduleAdapter != null) {
        //   //duoc chay luc build
        //   return new moduleAdapter().getAllRefs();
        // }
        // let temp = cc.director.getScene().getChildByName(adapterName)?.getComponent(adapterName)?.getAllRefs();
        // return temp;
    };
    return SicboModuleAdapter;
}());
exports.default = SicboModuleAdapter;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TaWNib01vZHVsZUFkYXB0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxxRUFBeUQ7QUFDekQsMkZBQXNGO0FBQ3RGLHlFQUFvRTtBQUNwRSxxRUFBaUU7QUFDakUsd0RBQW9EO0FBRXBELHdEQUF3RTtBQUN4RSxxREFBaUQ7QUFFakQsOERBQTJFO0FBRTNFLHlGQUFnRztBQUNoRyxpRUFBd0o7QUFFeEosK0ZBQXFGO0FBQ3JGLHVFQUFvSDtBQUVwSCw4REFBMEU7QUFFMUUsbUZBQXVFO0FBQ3ZFLDJEQTBCc0M7QUFFdEMsNkRBQTBGO0FBQzFGLDZFQUF5RTtBQUN6RSwyREFBNkQ7QUFFN0QsZ0ZBQXNFO0FBQ3RFLHdEQUFzSTtBQUV0STtJQUFBO0lBcURBLENBQUM7SUFwRFEsNkJBQVUsR0FBakI7UUFDRSxPQUFPO1lBQ0wsSUFBSSx3QkFBQSxFQUFFLGlCQUFpQiw2QkFBQSxFQUFFLEtBQUssa0JBQUEsRUFBRSxNQUFNLG9CQUFBO1lBQ3RDLGFBQWEsMkJBQUEsRUFBRSxXQUFXLHlCQUFBLEVBQUcsS0FBSyxrQkFBQTtZQUNsQyxhQUFhLDRDQUFBLEVBQUUsc0JBQXNCLHVDQUFBLEVBQUUsdUJBQXVCLHdDQUFBLEVBQUUsd0JBQXdCLHlDQUFBLEVBQUUseUJBQXlCLDBDQUFBO1lBQ25ILGlCQUFpQixnREFBQSxFQUFFLGtCQUFrQixxQ0FBQSxFQUFFLGdCQUFnQixtQ0FBQSxFQUFFLGNBQWMsaUNBQUEsRUFBRSxNQUFNLHlCQUFBO1lBQy9FLFNBQVMseUJBQUEsRUFBRSxhQUFhLDZCQUFBO1lBQ3hCLFdBQVcsc0NBQUE7WUFDWCxjQUFjLDZCQUFBLEVBQUUsZ0JBQWdCLCtCQUFBLEVBQUUsUUFBUSx1QkFBQSxFQUFFLFVBQVUseUJBQUEsRUFBRSxPQUFPLHNCQUFBLEVBQUUsSUFBSSxtQkFBQTtZQUNyRSxjQUFjLDZCQUFBLEVBQUUsZ0JBQWdCLCtCQUFBLEVBQUUsWUFBWSwyQkFBQSxFQUFFLE9BQU8sc0JBQUEsRUFBRSxVQUFVLHlCQUFBLEVBQUUsYUFBYSw0QkFBQTtZQUNsRixZQUFZLDJCQUFBLEVBQUUsS0FBSyxvQkFBQSxFQUFFLE1BQU0scUJBQUEsRUFBRSxLQUFLLG9CQUFBLEVBQUcsZ0JBQWdCLCtCQUFBLEVBQUUsTUFBTSxxQkFBQSxFQUFFLHNCQUFzQixxQ0FBQTtZQUNyRix1QkFBdUIsc0NBQUEsRUFBRSxJQUFJLG1CQUFBLEVBQUUsVUFBVSx5QkFBQSxFQUFFLFNBQVMsd0JBQUEsRUFBRSxVQUFVLHlCQUFBO1lBQ2hFLHVCQUF1QixpQ0FBQSxFQUFFLFVBQVUsb0JBQUEsRUFBRSxTQUFTLDBCQUFBLEVBQUUsUUFBUSxvQkFBQSxFQUFFLHdCQUF3QixzQ0FBQSxFQUFFLGVBQWUsNkJBQUEsRUFBRSwwQkFBMEIsd0NBQUEsRUFBRSxpQkFBaUIsK0JBQUEsRUFBRSxZQUFZLHNDQUFBLEVBQUUsc0JBQXNCLHFDQUFBO1lBQ3hMLGNBQWMsdUJBQUE7WUFDZCxrQkFBa0IsaUNBQUEsRUFBRSxnQkFBZ0IsK0JBQUE7U0FDckMsQ0FBQztRQUVGLDRCQUE0QjtRQUM1QiwyQ0FBMkM7UUFDM0MsUUFBUTtRQUNSLHVEQUF1RDtRQUN2RCxZQUFZO1FBQ1osd0RBQXdEO1FBQ3hELElBQUk7UUFFSiwrQkFBK0I7UUFDL0IsMEJBQTBCO1FBQzFCLDZDQUE2QztRQUM3QyxJQUFJO1FBQ0osMEdBQTBHO1FBQzFHLGVBQWU7SUFDakIsQ0FBQztJQXFCSCx5QkFBQztBQUFELENBckRBLEFBcURDLElBQUEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb2RlIH0gZnJvbSBcIkBnYW1lbG9vdC9zaWNib3YyL3NpY2JvdjJfY29kZV9wYlwiO1xuaW1wb3J0IEhhbmRsZXJDbGllbnRCYXNlIGZyb20gXCJAZ2FtZWxvb3QvY2xpZW50LWJhc2UtaGFuZGxlci9idWlsZC9IYW5kbGVyQ2xpZW50QmFzZVwiO1xuaW1wb3J0IEV2ZW50S2V5IGZyb20gXCJAZ2FtZWxvb3QvY2xpZW50LWJhc2UtaGFuZGxlci9idWlsZC9FdmVudEtleVwiO1xuaW1wb3J0IHsgRW1wdHkgfSBmcm9tIFwiZ29vZ2xlLXByb3RvYnVmL2dvb2dsZS9wcm90b2J1Zi9lbXB0eV9wYlwiO1xuaW1wb3J0IHsgUGxheWFoIH0gZnJvbSBcIkBnYW1lbG9vdC9wbGF5YWgvcGxheWFoX3BiXCI7XG5cbmltcG9ydCB7IEJlblRhdU1lc3NhZ2UsIEJlblRhdVJlcGx5IH0gZnJvbSBcIkBnYW1lbG9vdC9iZW50YXUvYmVudGF1X3BiXCI7XG5pbXBvcnQgeyBUb3BpYyB9IGZyb20gXCJAZ2FtZWxvb3QvdG9waWMvdG9waWNfcGJcIjtcblxuaW1wb3J0IHsgTWVzc2FnZSBhcyBXaGF0QXBwTWVzc2FnZSB9IGZyb20gXCJAZ2FtZWxvb3Qvd2hhdHNhcHAvd2hhdHNhcHBfcGJcIjtcblxuaW1wb3J0IHsgTXlIaXN0b3J5Q2xpZW50IGFzICBIaXN0b3J5Q2xpZW50fSBmcm9tIFwiQGdhbWVsb290L215aGlzdG9yeS9NeUhpc3RvcnlTZXJ2aWNlQ2xpZW50UGJcIjtcbmltcG9ydCB7IEdldENoYW5uZWxIaXN0b3J5UmVwbHksIEdldFBlcnNvbmFsSGlzdG9yeVJlcGx5LCBHZXRDaGFubmVsSGlzdG9yeVJlcXVlc3QsIEdldFBlcnNvbmFsSGlzdG9yeVJlcXVlc3QgfSBmcm9tIFwiQGdhbWVsb290L215aGlzdG9yeS9teWhpc3RvcnlfcGJcIjtcblxuaW1wb3J0IHsgTGVhZGVyYm9hcmRDbGllbnQgfSBmcm9tIFwiQGdhbWVsb290L2xlYWRlcmJvYXJkL0xlYWRlcmJvYXJkU2VydmljZUNsaWVudFBiXCI7XG5pbXBvcnQgeyBSYW5rRWFybmluZ1JlcGx5LCBSYW5rRWFybmluZ1JlcXVlc3QsIERldGFpbGVkTGVhZGVyLCBDdXRvZmYgfSBmcm9tIFwiQGdhbWVsb290L2xlYWRlcmJvYXJkL2xlYWRlcmJvYXJkX3BiXCI7XG5cbmltcG9ydCB7IEZpbGVGaWVsZCwgUmVzb3VyY2VGaWVsZCB9IGZyb20gXCJAZ2FtZWxvb3QvY29udmV5b3IvY29udmV5b3JfcGJcIjtcblxuaW1wb3J0IHsgU2ljYm9DbGllbnQgfSBmcm9tIFwiQGdhbWVsb290L3NpY2JvdjIvU2ljYm92MlNlcnZpY2VDbGllbnRQYlwiO1xuaW1wb3J0IHtcbiAgQmV0RG91YmxlUmVwbHksXG4gIEJldERvdWJsZVJlcXVlc3QsXG4gIEJldFJlcGx5LFxuICBCZXRSZXF1ZXN0LFxuICBDaGFubmVsLFxuICBEb29yLFxuICBHZXRPdGhlcnNSZXBseSxcbiAgR2V0T3RoZXJzUmVxdWVzdCxcbiAgTGVhdmVSZXF1ZXN0LFxuICBNZXNzYWdlLFxuICBSZUJldFJlcGx5LFxuICBSZUJldFJlcXVlc3QsXG4gIFN0YXRlLFxuICBSZXN1bHQsXG4gIEV2ZW50LFxuICBEb29yQW5kQmV0QW1vdW50LFxuICBTdGF0dXMsXG4gIENoYW5uZWxIaXN0b3J5TWV0YWRhdGEsXG4gIFBlcnNvbmFsSGlzdG9yeU1ldGFkYXRhLFxuICBJdGVtLFxuICBDaGlwTGV2ZWxzLFxuICBTdGFnZVRpbWUsXG4gIExlYXZlUmVwbHksXG4gIEphY2twb3RIaXN0b3J5TWV0YWRhdGEsXG4gIEhlYWx0aENoZWNrUmVxdWVzdCwgSGVhbHRoQ2hlY2tSZXBseVxufSBmcm9tIFwiQGdhbWVsb290L3NpY2JvdjIvc2ljYm92Ml9wYlwiO1xuXG5pbXBvcnQgeyBIYW5kbGVyQ2xpZW50QmVuVGF1QmFzZSwgQ29kZVNvY2tldCB9IGZyb20gXCJAZ2FtZWxvb3QvY2xpZW50LWJhc2UtaGFuZGxlci9idWlsZFwiO1xuaW1wb3J0IHsgVGltZXN0YW1wIH0gZnJvbSBcImdvb2dsZS1wcm90b2J1Zi9nb29nbGUvcHJvdG9idWYvdGltZXN0YW1wX3BiXCI7XG5pbXBvcnQgeyBBdXRvSm9pblJlcGx5IH0gZnJvbSAnQGdhbWVsb290L3NpY2JvdjIvc2ljYm92Ml9wYic7XG5cbmltcG9ydCB7IFBvdHRlckNsaWVudCB9IGZyb20gXCJAZ2FtZWxvb3QvcG90dGVyL1BvdHRlclNlcnZpY2VDbGllbnRQYlwiO1xuaW1wb3J0IHsgTGlzdEN1cnJlbnRKYWNrcG90c1JlcGx5LCBMaXN0Q3VycmVudEphY2twb3RzUmVxdWVzdCwgR2V0SGlzdG9yeVJlcGx5LCBHZXRIaXN0b3J5UmVxdWVzdCB9IGZyb20gXCJAZ2FtZWxvb3QvcG90dGVyL3BvdHRlcl9wYlwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTaWNib01vZHVsZUFkYXB0ZXIge1xuICBzdGF0aWMgZ2V0QWxsUmVmcygpIHtcbiAgICByZXR1cm4ge1xuICAgICAgQ29kZSwgSGFuZGxlckNsaWVudEJhc2UsIEVtcHR5LCBQbGF5YWgsXG4gICAgICBCZW5UYXVNZXNzYWdlLCBCZW5UYXVSZXBseSAsIFRvcGljLFxuICAgICAgSGlzdG9yeUNsaWVudCwgR2V0Q2hhbm5lbEhpc3RvcnlSZXBseSwgR2V0UGVyc29uYWxIaXN0b3J5UmVwbHksIEdldENoYW5uZWxIaXN0b3J5UmVxdWVzdCwgR2V0UGVyc29uYWxIaXN0b3J5UmVxdWVzdCxcbiAgICAgIExlYWRlcmJvYXJkQ2xpZW50LCBSYW5rRWFybmluZ1JlcXVlc3QsIFJhbmtFYXJuaW5nUmVwbHksIERldGFpbGVkTGVhZGVyLCBDdXRvZmYsXG4gICAgICBGaWxlRmllbGQsIFJlc291cmNlRmllbGQsIFxuICAgICAgU2ljYm9DbGllbnQsIFxuICAgICAgQmV0RG91YmxlUmVwbHksIEJldERvdWJsZVJlcXVlc3QsIEJldFJlcGx5LCBCZXRSZXF1ZXN0LCBDaGFubmVsLCBEb29yLFxuICAgICAgR2V0T3RoZXJzUmVwbHksIEdldE90aGVyc1JlcXVlc3QsIExlYXZlUmVxdWVzdCwgTWVzc2FnZSwgUmVCZXRSZXBseSwgQXV0b0pvaW5SZXBseSxcbiAgICAgIFJlQmV0UmVxdWVzdCwgU3RhdGUsIFJlc3VsdCwgRXZlbnQsICBEb29yQW5kQmV0QW1vdW50LCBTdGF0dXMsIENoYW5uZWxIaXN0b3J5TWV0YWRhdGEsXG4gICAgICBQZXJzb25hbEhpc3RvcnlNZXRhZGF0YSwgSXRlbSwgQ2hpcExldmVscywgU3RhZ2VUaW1lLCBMZWF2ZVJlcGx5LFxuICAgICAgSGFuZGxlckNsaWVudEJlblRhdUJhc2UsIENvZGVTb2NrZXQsIFRpbWVzdGFtcCwgRXZlbnRLZXksIExpc3RDdXJyZW50SmFja3BvdHNSZXBseSwgR2V0SGlzdG9yeVJlcGx5LCBMaXN0Q3VycmVudEphY2twb3RzUmVxdWVzdCwgR2V0SGlzdG9yeVJlcXVlc3QsIFBvdHRlckNsaWVudCwgSmFja3BvdEhpc3RvcnlNZXRhZGF0YSxcbiAgICAgIFdoYXRBcHBNZXNzYWdlLFxuICAgICAgSGVhbHRoQ2hlY2tSZXF1ZXN0LCBIZWFsdGhDaGVja1JlcGx5XG4gICAgfTtcblxuICAgIC8vIGxldCBtb2R1bGVBZGFwdGVyID0gbnVsbDtcbiAgICAvLyBjb25zdCBhZGFwdGVyTmFtZSA9IFwiU2ljYm9BZGFwdGVySW1wb3J0XCJcbiAgICAvLyB0cnkge1xuICAgIC8vICAgbW9kdWxlQWRhcHRlciA9IHJlcXVpcmUoYWRhcHRlck5hbWUpW2FkYXB0ZXJOYW1lXTtcbiAgICAvLyB9IGNhdGNoIHtcbiAgICAvLyAgIC8vY2MubG9nKFwibmV3IHRva2VuIGdldCBDaOG6oXkgbMO6YyBn4bqvbiB2w6BvIHBvcnRhbCBcIik7XG4gICAgLy8gfVxuXG4gICAgLy8gaWYgKG1vZHVsZUFkYXB0ZXIgIT0gbnVsbCkge1xuICAgIC8vICAgLy9kdW9jIGNoYXkgbHVjIGJ1aWxkXG4gICAgLy8gICByZXR1cm4gbmV3IG1vZHVsZUFkYXB0ZXIoKS5nZXRBbGxSZWZzKCk7XG4gICAgLy8gfVxuICAgIC8vIGxldCB0ZW1wID0gY2MuZGlyZWN0b3IuZ2V0U2NlbmUoKS5nZXRDaGlsZEJ5TmFtZShhZGFwdGVyTmFtZSk/LmdldENvbXBvbmVudChhZGFwdGVyTmFtZSk/LmdldEFsbFJlZnMoKTtcbiAgICAvLyByZXR1cm4gdGVtcDtcbiAgfVxuXG4gIC8vIGdldFByb3RvYnVmZlJlZigpe1xuICAvLyAgICAgcmV0dXJuIHtUaW1lc3RhbXB9XG4gIC8vIH1cblxuICAvLyBnZXRDb252ZXlvclJlZnMoKSB7XG4gIC8vICAgICByZXR1cm4geyBGaWxlRmllbGQsIFJlc291cmNlRmllbGR9O1xuICAvLyB9XG5cbiAgLy8gZ2V0QmVuVGF1UmVmcygpIHtcbiAgLy8gICAgIHJldHVybiB7QmVuVGF1TWVzc2FnZSwgQmVuVGF1UmVwbHkgLCBUb3BpYywgRW1wdHl9O1xuICAvLyB9XG5cbiAgLy8gZ2V0Z2FtZWxvb3RSZWZzKCkge1xuICAvLyAgICAgcmV0dXJuIHtIYW5kbGVyQ2xpZW50QmVuVGF1QmFzZSwgQ29kZVNvY2tldCB9O1xuICAvLyB9XG5cbiAgLy8gZ2V0SGlzdG9yeVJlZigpe1xuICAvLyAgICAgcmV0dXJuIHtIYW5kbGVyQ2xpZW50QmFzZSwgSGlzdG9yeUNsaWVudCwgR2V0Q2hhbm5lbEhpc3RvcnlSZXBseSwgR2V0UGVyc29uYWxIaXN0b3J5UmVwbHksIEdldENoYW5uZWxIaXN0b3J5UmVxdWVzdCwgR2V0UGVyc29uYWxIaXN0b3J5UmVxdWVzdH07XG4gIC8vIH1cbn1cbiJdfQ==