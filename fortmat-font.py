#!/usr/bin/python

import sys
import glob, os 
import shutil
from shutil import copyfile

try: 

	os.mkdir("export")
except OSError:
    # directory already exists
    pass

def exportFont(inFile):

	#get file object
	fileIn = open(inFile, "r")

	fileOut = open("export/"+inFile, "w")
	d = 20
	#read all lines
	lines = fileIn.readlines()

	#traverse through lines one by one
	for line in lines:

		if(line.find("face=")!=-1):
			t1= line[0:line.find("face=")]
			t2= line[line.find("face="):]
			off1 = t2.find("=\"")
			off2 = t2.find("\" ")
			path = t2[off1+2:off2]
			fileOut.write(t1 + t2[0:off1+2]+os.path.split(path)[1]+t2[off2:len(t2)])

			t1 = line[0:line.find("size=")]
			t2 = line[line.find("size="): len(line)]
			off1 = t2.find("=")
			off2 = t2.find(" ")
			num = t2[off1+1:off2]
			
			d = int(num)

			print (d)

		elif(line.find("file=")!=-1):
			t1= line[0:line.find("file=")]
			t2= line[line.find("file="):]
			off1 = t2.find("=\"")
			off2 = t2.find("\" ")
			path = t2[off1+2:off2-1]
			fileOut.write(t1 + t2[0:off1+2]+os.path.split(path)[1]+t2[off2-1:len(t2)])

			copyfile(os.path.split(path)[1], "export/"+os.path.split(path)[1])

		else :
			t1 = line[0:line.find("yoffset=")]
			t2 = line[line.find("yoffset="): len(line)]
			off1 = t2.find("=")
			off2 = t2.find(" ")
			num = t2[off1+1:off2]
			if(len(num)>0):
				num = str(int(num)-d)
			fileOut.write(t1 + t2[0:off1+1]+num+t2[off2:len(t2)])

	#close file
	fileIn.close
	fileOut.close

for file in glob.glob("*.fnt"):
	exportFont(file)

