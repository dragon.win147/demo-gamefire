"use strict";
cc._RF.push(module, '8656cWMFyBO660KqJ9Bdch7', 'ResourceManager');
// Game/Lobby/Manager/ResourceManager.ts

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameDefine_1 = require("../../../Common/Scripts/Defines/GameDefine");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ResourceManager = /** @class */ (function () {
    function ResourceManager() {
    }
    Object.defineProperty(ResourceManager, "bundle", {
        get: function () {
            return cc.assetManager.getBundle(GameDefine_1.default.PortalBundle);
        },
        enumerable: false,
        configurable: true
    });
    ResourceManager.loadPrefab = function (path, onComplete) {
        if (onComplete === void 0) { onComplete = null; }
        cc.resources.load(path, cc.Prefab, function (err, prefab) {
            if (err) {
                cc.log(err);
                return;
            }
            if (onComplete)
                onComplete(prefab);
        });
    };
    ResourceManager.loadPrefabWithErrorHandler = function (path) {
        return new Promise(function (resolve, reject) {
            cc.resources.load(path, function (err, prefab) {
                if (err) {
                    cc.log(err);
                    reject(err);
                    return;
                }
                resolve(prefab);
            });
        });
    };
    ResourceManager.loadSound = function (path, onComplete) {
        if (onComplete === void 0) { onComplete = null; }
        cc.resources.load(path, cc.AudioClip, function (err, sound) {
            if (err) {
                cc.log(err);
                return;
            }
            if (onComplete)
                onComplete(sound);
        });
    };
    ResourceManager = __decorate([
        ccclass
    ], ResourceManager);
    return ResourceManager;
}());
exports.default = ResourceManager;

cc._RF.pop();