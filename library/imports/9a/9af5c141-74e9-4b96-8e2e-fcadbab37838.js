"use strict";
cc._RF.push(module, '9af5cFBdOlLlo4u/K26s3g4', 'SicboChangeTabComp');
// Game/Sicbo_New/Scripts/Components/SicboChangeTabComp.js

"use strict";

var _SicboSoundManager = _interopRequireDefault(require("../Managers/SicboSoundManager"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var customLog = function customLog(logString) {
  cc.log("SicboChangeTabComponent - " + logString);
};

cc.Class({
  "extends": cc.Component,
  properties: {
    timeMinute: 0,
    closeGameEvent: {
      "default": [],
      type: cc.Component.EventHandler
    }
  },
  onDestroy: function onDestroy() {
    cc.game.off(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
    cc.game.off(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    this.timeEnterBg = 0;
    cc.game.on(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
    cc.game.on(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
  },
  onChangeTabHide: function onChangeTabHide() {
    var onmessage = function onmessage(g) {
      var a,
          b,
          c,
          d = Date.now(),
          e = g.data[0],
          f = function f() {
        clearTimeout(a);
        b = Date.now();
        c = b - d;
        d = b;
        postMessage(c);
        a = setTimeout(f, e);
      };

      a = setTimeout(f, e);
    };

    var onmessageFunction = "onmessage = " + onmessage.toString().replace(/\n/g, "");

    if (!cc.sys.isNative && !window.webWorker) {
      this.checkTimerEnterBg(); // cc.game.pause();

      this.onPauseMusic();
      window.webWorker = new Worker(URL.createObjectURL(new Blob([onmessageFunction], {
        type: "text/javascript"
      })));

      window.webWorker.onmessage = function (event) {
        cc.director.mainLoop(); // TweenMax.ticker.tick()
      };

      window.webWorker.postMessage([Math.floor(cc.game.getFrameRate())]);
    }
  },
  onChangeTabShow: function onChangeTabShow() {
    if (!cc.sys.isNative && window.webWorker) {
      cc.game.resume();
      this.onResumeMusic();
      this.timeEnterBg = 0;
      clearInterval(this.timeInterval);
      window.webWorker.terminate();
      delete window.webWorker;
    }
  },
  onPauseMusic: function onPauseMusic() {
    _SicboSoundManager["default"].getInstance().pauseAllEffects();

    _SicboSoundManager["default"].getInstance().setMusicVolume(0, false);

    _SicboSoundManager["default"].getInstance().setSfxVolume(0, false);
  },
  onResumeMusic: function onResumeMusic() {
    _SicboSoundManager["default"].getInstance().resumeAllEffects();

    _SicboSoundManager["default"].getInstance().loadConfigFromFromStorage();
  },
  checkTimerEnterBg: function checkTimerEnterBg() {
    if (this.timeMinute <= 0) return;
    this.timeInterval = setInterval(function () {
      this.timeEnterBg++;

      if (this.timeEnterBg >= this.timeMinute * 60) {
        this.closeGameEvent.forEach(function (element) {
          element.emit();
        });
        this.timeEnterBg = 0;
        clearInterval(this.timeInterval);
      }
    }.bind(this), 1000);
  },
  start: function start() {}
});

cc._RF.pop();