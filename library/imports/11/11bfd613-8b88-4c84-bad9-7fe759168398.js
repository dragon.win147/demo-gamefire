"use strict";
cc._RF.push(module, '11bfdYTi4hMhLrZf+dZFoOY', 'SicboChipSpriteConfig');
// Game/Sicbo_New/Scripts/Configs/SicboChipSpriteConfig.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboChipSpriteConfig = /** @class */ (function (_super) {
    __extends(SicboChipSpriteConfig, _super);
    function SicboChipSpriteConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.normalChips = [];
        _this.disableChips = [];
        _this.miniChips = [];
        return _this;
    }
    SicboChipSpriteConfig_1 = SicboChipSpriteConfig;
    Object.defineProperty(SicboChipSpriteConfig, "Instance", {
        get: function () {
            return SicboChipSpriteConfig_1.instance;
        },
        enumerable: false,
        configurable: true
    });
    SicboChipSpriteConfig.prototype.onLoad = function () {
        SicboChipSpriteConfig_1.instance = this;
    };
    SicboChipSpriteConfig.prototype.onDestroy = function () {
        delete SicboChipSpriteConfig_1.instance;
        SicboChipSpriteConfig_1.instance = null;
    };
    SicboChipSpriteConfig.prototype.getNormalChipSprite = function (spriteName) {
        var rs = null;
        SicboChipSpriteConfig_1.Instance.normalChips.forEach(function (element) {
            if (element.name == spriteName) {
                rs = element;
            }
        });
        return rs;
    };
    SicboChipSpriteConfig.prototype.getDisableChipSprite = function (spriteName) {
        var rs = null;
        SicboChipSpriteConfig_1.Instance.disableChips.forEach(function (element) {
            if (element.name == spriteName) {
                rs = element;
            }
        });
        return rs;
    };
    SicboChipSpriteConfig.prototype.getMiniChipSprite = function (spriteName) {
        var rs = null;
        SicboChipSpriteConfig_1.Instance.miniChips.forEach(function (element) {
            if (element.name == spriteName) {
                rs = element;
            }
        });
        return rs;
    };
    var SicboChipSpriteConfig_1;
    SicboChipSpriteConfig.instance = null;
    __decorate([
        property([cc.SpriteFrame])
    ], SicboChipSpriteConfig.prototype, "normalChips", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], SicboChipSpriteConfig.prototype, "disableChips", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], SicboChipSpriteConfig.prototype, "miniChips", void 0);
    SicboChipSpriteConfig = SicboChipSpriteConfig_1 = __decorate([
        ccclass
    ], SicboChipSpriteConfig);
    return SicboChipSpriteConfig;
}(cc.Component));
exports.default = SicboChipSpriteConfig;

cc._RF.pop();