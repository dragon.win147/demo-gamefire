"use strict";
cc._RF.push(module, '947b6PbhdlBO4MuiMVN64Lg', 'SicboUIManager');
// Game/Sicbo/Scripts/Managers/SicboUIManager.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboUIManager = /** @class */ (function (_super) {
    __extends(SicboUIManager, _super);
    function SicboUIManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popupRoot = null;
        _this.popupPrefabs = [];
        _this.popupMap = new Map();
        _this._popupInstances = new Map();
        return _this;
    }
    SicboUIManager_1 = SicboUIManager;
    SicboUIManager.getInstance = function () {
        if (SicboUIManager_1._instance == null) {
            var canvas = cc.director.getScene().getChildByName("Canvas");
            var node = canvas.getChildByName("SicboUIManager");
            SicboUIManager_1._instance = node.getComponent(SicboUIManager_1);
        }
        return SicboUIManager_1._instance;
    };
    SicboUIManager.prototype.onLoad = function () {
        this.init();
    };
    SicboUIManager.prototype.init = function () {
        var _this = this;
        this.popupPrefabs.forEach(function (popupPrefab) {
            if (popupPrefab) {
                _this.popupMap.set(popupPrefab.name, popupPrefab);
            }
        });
    };
    SicboUIManager.prototype.openPopup = function (type, name, data, onComplete, parent) {
        if (data === void 0) { data = null; }
        if (onComplete === void 0) { onComplete = null; }
        if (parent === void 0) { parent = null; }
        var self = SicboUIManager_1._instance;
        parent = parent ? parent : self.popupRoot;
        if (name == self._currentPopupName && self._currentPopup.node) {
            if (onComplete)
                onComplete(self._currentPopup);
            return;
        }
        self._currentPopupName = name;
        self.getPopupInstance(type, name, function (instance) {
            if (self._currentPopup == instance) {
                if (onComplete)
                    onComplete(self._currentPopup);
                return;
            }
            self._currentPopup = instance;
            instance.open(data);
            instance._close = function () {
                self._currentPopup = null;
                self._currentPopupName = "";
            };
            if (onComplete)
                onComplete(self._currentPopup);
        }, parent);
    };
    SicboUIManager.prototype.hidePopup = function (name) {
        var _a;
        var self = SicboUIManager_1._instance;
        (_a = self._popupInstances.get(name)) === null || _a === void 0 ? void 0 : _a.hidePopup();
    };
    SicboUIManager.prototype.getPopupInstance = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = SicboUIManager_1._instance;
        var _parent = parent == null ? this.node : parent;
        if (self._popupInstances.has(name)) {
            var popup = self._popupInstances.get(name);
            popup.node.parent = null;
            _parent.addChild(popup.node);
            onComplete(popup);
        }
        else {
            this.loadPopUpPrefab(type, name, onComplete, _parent);
        }
    };
    SicboUIManager.prototype.loadPopUpPrefab = function (type, name, onComplete, parent) {
        if (parent === void 0) { parent = null; }
        var self = SicboUIManager_1._instance;
        var prefab = this.popupMap.get(name);
        var newNode = cc.instantiate(prefab);
        var instance = newNode.getComponent(type);
        parent.addChild(newNode);
        self._popupInstances.set(name, instance);
        onComplete(instance);
    };
    SicboUIManager.prototype.onDestroy = function () {
        delete SicboUIManager_1._instance;
        SicboUIManager_1._instance = null;
    };
    var SicboUIManager_1;
    SicboUIManager._instance = null;
    SicboUIManager.SicboInforPopup = "SicboInforPopup";
    SicboUIManager.SicboCannotJoinPopup = "SicboCannotJoinPopup";
    __decorate([
        property(cc.Node)
    ], SicboUIManager.prototype, "popupRoot", void 0);
    __decorate([
        property([cc.Prefab])
    ], SicboUIManager.prototype, "popupPrefabs", void 0);
    SicboUIManager = SicboUIManager_1 = __decorate([
        ccclass
    ], SicboUIManager);
    return SicboUIManager;
}(cc.Component));
exports.default = SicboUIManager;

cc._RF.pop();