"use strict";
cc._RF.push(module, '947b9avXPpP/ZuSNV+4EGFr', 'ScaleNodeComp');
// Game/Sicbo_New/Scripts/Components/ScaleNodeComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode;
var ScaleNodeComp = /** @class */ (function (_super) {
    __extends(ScaleNodeComp, _super);
    function ScaleNodeComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.maxWidth = 20;
        _this.maxScale = 1;
        return _this;
    }
    ScaleNodeComp.prototype.update = function (dt) {
        this.updateScale();
    };
    ScaleNodeComp.prototype.updateScale = function () {
        if (this.node.width <= 0)
            this.node.scale = this.maxScale;
        var scale = Math.min(this.maxScale, this.maxWidth * 1.0 / this.node.width);
        this.node.scale = scale;
    };
    __decorate([
        property()
    ], ScaleNodeComp.prototype, "maxWidth", void 0);
    __decorate([
        property()
    ], ScaleNodeComp.prototype, "maxScale", void 0);
    ScaleNodeComp = __decorate([
        ccclass,
        executeInEditMode
    ], ScaleNodeComp);
    return ScaleNodeComp;
}(cc.Component));
exports.default = ScaleNodeComp;

cc._RF.pop();