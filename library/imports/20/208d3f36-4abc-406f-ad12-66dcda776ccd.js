"use strict";
cc._RF.push(module, '208d382SrxAb60SZtzad2zN', 'TextNotifyDisconnect');
// Game/Lobby/Manager/TextNotifyDisconnect.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var NotifyInstance_1 = require("../Base/NotifyInstance");
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
// export class TextNotifyData {
//   public onClick: () => void = null;
//   public msg: string = "";
//   public duration: number = 2;
//   public position: Vec3 = null;
//   constructor(msg: string, duration: number = 2, position: Vec3 = new Vec3(0, 0, 0), onClick: () => void = null) {
//     this.msg = msg;
//     this.duration = duration;
//     this.position = position;
//     this.onClick = onClick;
//   }
// }
var TextNotifyDisconnect = /** @class */ (function (_super) {
    __extends(TextNotifyDisconnect, _super);
    function TextNotifyDisconnect() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.content = null;
        _this.rootNode = null;
        _this.listSprFrame = [];
        _this.data = null;
        return _this;
    }
    TextNotifyDisconnect.prototype.onShow = function (data) {
        var _this = this;
        if (data === void 0) { data = null; }
        this.node.active = true;
        this.data = data;
        var notifyData = data;
        if (notifyData.msg != "")
            this.content.string = notifyData.msg;
        this.node.stopAllActions();
        this.node.runAction(cc.sequence(cc.delayTime(notifyData.duration), cc.callFunc(function () {
            _this.onHide();
        })));
    };
    TextNotifyDisconnect.prototype.afterShow = function () { };
    TextNotifyDisconnect.prototype.beforeShow = function () { };
    TextNotifyDisconnect.prototype.beforeClose = function () { };
    TextNotifyDisconnect.prototype.afterClose = function () { };
    TextNotifyDisconnect.prototype.onHide = function () {
        this.node.active = false;
    };
    TextNotifyDisconnect.prototype.onClick = function () {
        if (this.data) {
            if (this.data.onClick)
                this.data.onClick();
        }
    };
    __decorate([
        property(cc.Label)
    ], TextNotifyDisconnect.prototype, "content", void 0);
    __decorate([
        property(cc.Node)
    ], TextNotifyDisconnect.prototype, "rootNode", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], TextNotifyDisconnect.prototype, "listSprFrame", void 0);
    TextNotifyDisconnect = __decorate([
        ccclass
    ], TextNotifyDisconnect);
    return TextNotifyDisconnect;
}(NotifyInstance_1.default));
exports.default = TextNotifyDisconnect;

cc._RF.pop();