"use strict";
cc._RF.push(module, 'd0485XgZ0VO1YaTAZlg9tTG', 'SicboJackpotStateComp');
// Game/Sicbo_New/Scripts/Components/States/SicboJackpotStateComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Topic = _a.Topic, State = _a.State, Result = _a.Result, Status = _a.Status, ListCurrentJackpotsReply = _a.ListCurrentJackpotsReply;
var SicboSetting_1 = require("../../Setting/SicboSetting");
var SicboController_1 = require("../../Controllers/SicboController");
var SicboHelper_1 = require("../../Helpers/SicboHelper");
var SicboPortalAdapter_1 = require("../../SicboPortalAdapter");
var SicboGameUtils_1 = require("../../RNGCommons/Utils/SicboGameUtils");
var SicboSkeletonUtils_1 = require("../../RNGCommons/Utils/SicboSkeletonUtils");
var SicboMoneyFormatComp_1 = require("../../RNGCommons/SicboMoneyFormatComp");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboJackpotStateComp = /** @class */ (function (_super) {
    __extends(SicboJackpotStateComp, _super);
    function SicboJackpotStateComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //ANCHOR jackpot stand
        _this.jackpotStandNode = null;
        _this.jackPotStandSkeleton = null;
        _this.jackpotStandValueLabel = null;
        //ANCHOR my jackpot animation
        _this.myJackpotFrontSkeleton = null;
        _this.myJackpotBackSkeleton = null;
        _this.particlePrefab = null;
        _this.jackpotRandomLabel = null;
        _this.jackpotNumberLabel = null;
        _this.otherWinnerNameLabel = null;
        _this.jackpotAnimationStartTime = 0;
        // private jackpotAnimationEndTime = this.jackpotAnimationStartTime + 4;
        _this.isJackpotPlaying = false;
        _this.jackpotScaleSfxStartTime = _this.jackpotAnimationStartTime;
        _this.jackpotScaleSfxStopTime = _this.jackpotScaleSfxStartTime + 1.5;
        _this.jackpotFinalSfxStartTime = _this.jackpotScaleSfxStartTime + 1.5;
        _this.particleDelayTime = 1.8;
        _this.particleInstance = null;
        _this.jackpotValue = 0;
        return _this;
    }
    SicboJackpotStateComp.prototype.setResult = function (result) {
        if (SicboGameUtils_1.default.isNullOrUndefined(result))
            return;
        this.result = result;
    };
    SicboJackpotStateComp.prototype.onLoad = function () {
        this.setMyJackpotAnimActive(false);
        //this.jackpotStandNode.active = true;
        this.playJackpotStandAnimation(true);
        this.setOtherWinnerActive(false);
    };
    SicboJackpotStateComp.prototype.exitState = function (status) {
        this.isJackpotPlaying = false;
        // if(status != Status.RESULTING && status != Status.PAYING) {
        if (status != Status.PAYING) {
            this.setMyJackpotAnimActive(false); //NOTE: do anim jackpot hoi dai
            //this.jackpotStandNode.active = true;
        }
        this.stopParticleFX();
        this.playJackpotStandAnimation(true);
    };
    SicboJackpotStateComp.prototype.updateState = function (state) {
        var _this = this;
        if (this.isJackpotPlaying)
            return;
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        if (this.isUpdateJackpotValue(status) || this.jackpotStandValueLabel.moneyValue == 0) {
            // this.updateJackpot(state.getPot() || 0);// tạm ẩn không cập nhật theo state
            this.updateJackpot(this.jackpotValue || 0); // tạm ẩn không cập nhật theo state
        }
        if (SicboGameUtils_1.default.isNullOrUndefined(this.result))
            return;
        if (!this.result.hasLotteryWinner())
            return;
        // if(status == Status.RESULTING && elapseTime >= this.jackpotAnimationStartTime){
        if (status == Status.PAYING && elapseTime >= this.jackpotAnimationStartTime) {
            this.isJackpotPlaying = true;
            this.otherWinnerNameLabel.string = this.result.getLotteryWinner().getDisplayName();
            if (SicboPortalAdapter_1.default.getInstance().getMyUserId() == this.result.getLotteryWinner().getUserId()) {
                this.setMyJackpotAnimActive(true);
                this.playParticleFx(elapseTime - this.jackpotAnimationStartTime);
                //this.jackpotStandNode.active = false;
                SicboSkeletonUtils_1.default.setAnimation(this.myJackpotFrontSkeleton, "animation", false, elapseTime - this.jackpotAnimationStartTime);
                SicboSkeletonUtils_1.default.setAnimation(this.myJackpotBackSkeleton, "animation", false, elapseTime - this.jackpotAnimationStartTime);
                this.myJackpotBackSkeleton.setCompleteListener(function (_) {
                    _this === null || _this === void 0 ? void 0 : _this.setMyJackpotAnimActive(false);
                });
                // let self = this;
                // cc.tween(self.node)
                //     .delay(self.jackpotAnimationEndTime - elapseTime)
                //     .call(()=>self?.setMyJackpotAnimActive(false))
                //     .start();
                this.jackpotNumberLabel.setMoney(0);
                this.jackpotNumberLabel.runMoneyTo(this.result.getLotteryWinner().getAmount(), 3);
                this.setOtherWinnerActive(false);
                this.myJackpotSfx(SicboSetting_1.SicboSound.SfxJackpotScale, elapseTime, this.jackpotScaleSfxStartTime);
                this.myJackpotSfx(SicboSetting_1.SicboSound.SfxJackpotScale, elapseTime, this.jackpotScaleSfxStopTime, false);
                this.myJackpotSfx(SicboSetting_1.SicboSound.SfxJackpotFinal, elapseTime, this.jackpotFinalSfxStartTime);
                //this.jackpotStandNode.active = true;
                this.setOtherWinnerActive(true);
                this.playJackpotStandAnimation(false);
            }
            else {
                //Other user win jackpot
                this.setMyJackpotAnimActive(false);
                //this.jackpotStandNode.active = true;
                this.setOtherWinnerActive(true);
                this.playJackpotStandAnimation(false);
            }
        }
    };
    SicboJackpotStateComp.prototype.isUpdateJackpotValue = function (status) {
        switch (status) {
            case Status.PAYING:
            case Status.END_BET:
            case Status.RESULTING:
                return false;
            default: return true;
        }
    };
    SicboJackpotStateComp.prototype.handleResponseJackpot = function (res) {
        var _this = this;
        var getJackpotsList = res.getJackpotsList();
        // let mapJackpot = new Map<Topic, ListCurrentJackpotsReply.Jackpot[]>();
        getJackpotsList.forEach(function (jackpot) {
            var topic = jackpot.getTopic();
            if (topic == Topic.SICBO) {
                var jackpots = [jackpot];
                var amountJackpot = jackpots[0].getAmount();
                // this.updateJackpot(amountJackpot);
                _this.jackpotValue = amountJackpot;
                return;
            }
        });
    };
    SicboJackpotStateComp.prototype.updateJackpot = function (value) {
        //cc.log("updateJackpot " + value);
        this.jackpotStandValueLabel.runMoneyTo(value, .2);
        this.jackpotRandomLabel.runMoneyTo(value, .2);
    };
    SicboJackpotStateComp.prototype.myJackpotSfx = function (sfxName, elapseTime, time, isPlay) {
        if (elapseTime === void 0) { elapseTime = 0; }
        if (isPlay === void 0) { isPlay = true; }
        var self = this;
        if (elapseTime > time) {
            if (isPlay) {
                //qua tgian thi skip
                if (SicboHelper_1.default.approximatelyEqual(elapseTime, time))
                    SicboController_1.default.Instance.playSfx(sfxName);
            }
            else
                SicboController_1.default.Instance.stopSfx(sfxName);
        }
        else {
            cc.tween(self.node)
                .delay(time - elapseTime)
                .call(function () {
                if (isPlay)
                    SicboController_1.default.Instance.playSfx(sfxName);
                else
                    SicboController_1.default.Instance.stopSfx(sfxName);
            })
                .start();
        }
    };
    SicboJackpotStateComp.prototype.startState = function (state, totalDuration) {
        this.setResult(state.getResult());
        var status = state.getStatus();
        // if(status != Status.RESULTING && status != Status.PAYING)
        if (status != Status.PAYING)
            this.setOtherWinnerActive(false);
        // if( status == Status.PAYING && this.jackpotStandValueLabel.moneyValue != 0) return;
        // this.jackpotStandValueLabel.setMoney(state.getPot()||0);
        // this.jackpotRandomLabel.setMoney(state.getPot()||0);
    };
    SicboJackpotStateComp.prototype.setMyJackpotAnimActive = function (isActive) {
        if (isActive === void 0) { isActive = true; }
        this.myJackpotFrontSkeleton.node.active = isActive;
        this.myJackpotBackSkeleton.node.active = isActive;
        if (!isActive)
            this.stopParticleFX();
    };
    SicboJackpotStateComp.prototype.playParticleFx = function (elapseTime) {
        this.stopParticleFX();
        this.particleInstance = cc.instantiate(this.particlePrefab);
        this.particleInstance.setParent(this.node);
        this.particleInstance.setPosition(cc.Vec2.ZERO);
        var particles = this.particleInstance.getComponentsInChildren(cc.ParticleSystem3D);
        for (var i = 0; i < particles.length; i++) {
            var particle = particles[i];
            particle.startDelay.constant = Math.max(this.particleDelayTime - elapseTime, 0);
        }
    };
    SicboJackpotStateComp.prototype.stopParticleFX = function () {
        if (this.particleInstance)
            this.particleInstance.destroy();
    };
    SicboJackpotStateComp.prototype.onHide = function () {
        if (this.node != null)
            cc.Tween.stopAllByTarget(this.node);
        this.setMyJackpotAnimActive(false);
        //this.jackpotStandNode.active = true;
        this.isJackpotPlaying = false;
        this.jackPotStandPlayingAnim = null;
        this.setOtherWinnerActive(false);
        SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxJackpotFinal);
        SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxJackpotScale);
    };
    SicboJackpotStateComp.prototype.setOtherWinnerActive = function (active) {
        this.otherWinnerNameLabel.node.parent.active = active;
        this.jackpotStandValueLabel.node.active = !active;
    };
    SicboJackpotStateComp.prototype.playJackpotStandAnimation = function (isIdle) {
        var anim = "no_hu";
        if (isIdle) {
            anim = "idle";
        }
        if (this.jackPotStandPlayingAnim == anim)
            return;
        this.jackPotStandPlayingAnim = anim;
        SicboSkeletonUtils_1.default.setAnimation(this.jackPotStandSkeleton, anim, true);
    };
    SicboJackpotStateComp.prototype.onDestroy = function () {
        if (this.node != null)
            cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.Node)
    ], SicboJackpotStateComp.prototype, "jackpotStandNode", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboJackpotStateComp.prototype, "jackPotStandSkeleton", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboJackpotStateComp.prototype, "jackpotStandValueLabel", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboJackpotStateComp.prototype, "myJackpotFrontSkeleton", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboJackpotStateComp.prototype, "myJackpotBackSkeleton", void 0);
    __decorate([
        property(cc.Prefab)
    ], SicboJackpotStateComp.prototype, "particlePrefab", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboJackpotStateComp.prototype, "jackpotRandomLabel", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboJackpotStateComp.prototype, "jackpotNumberLabel", void 0);
    __decorate([
        property(cc.Label)
    ], SicboJackpotStateComp.prototype, "otherWinnerNameLabel", void 0);
    SicboJackpotStateComp = __decorate([
        ccclass
    ], SicboJackpotStateComp);
    return SicboJackpotStateComp;
}(cc.Component));
exports.default = SicboJackpotStateComp;

cc._RF.pop();