"use strict";
cc._RF.push(module, 'aedc8sS5oRDu4P1mJUv/RM4', 'EventBusManager');
// Game/Lobby/Event/EventBusManager.ts

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var GameDefine_1 = require("../Define/GameDefine");
var WalletHandler_1 = require("../Network/WalletHandler");
var GameUtils_1 = require("../Utils/GameUtils");
var UIManager_1 = require("../Manager/UIManager");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var WarningPopupSystem_1 = require("../Popup/WarningPopupSystem");
var EventKey_1 = require("@gameloot/client-base-handler/build/EventKey");
var TextNotify_1 = require("../Manager/TextNotify");
var PortalText_1 = require("../Utils/PortalText");
var EventManager_1 = require("../Event/EventManager");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
/**
 * Cách bắn event từ project cảu game qua
 * Sample :
 * cc.systemEvent.emit(EventKey.ON_OPEN_POPUP_WARNING_EVENT, "Đây là thông báo lỗi", () => {
      cc.log("open warning popup success");
    });
 *
 *  cc.systemEvent.emit(EventKey.ON_OPEN_POPUP_LOADING_EVENT, () => {
      cc.log("open loading popup success");
    },0);
 *
 *  *  cc.systemEvent.emit(EventKey.ON_GET_CURRENT_BALANCE_EVENT, (currentBalance) => {
      cc.log("Current balance = " + currentBalance);
    });
 *
 *
 */
var EventBusManager = /** @class */ (function () {
    function EventBusManager() {
    }
    EventBusManager_1 = EventBusManager;
    EventBusManager.onRegisterEvent = function () {
        var _this = this;
        //#region ======== popup system ========
        EventBusManager_1.removeAllEvent();
        cc.systemEvent.on(EventKey_1.default.ON_OPEN_POPUP_DISCONNECT_EVENT, function (code, callback) { });
        cc.systemEvent.on(EventKey_1.default.ON_OPEN_POPUP_WARNING_EVENT, function (msg, callback) {
            UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, msg);
            callback && callback();
        });
        //#region ======== popup loading ========
        cc.systemEvent.on(EventKey_1.default.ON_OPEN_POPUP_LOADING_EVENT, function (callback) {
            UIManager_1.default.getInstance()
                .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, false, null, null)
                .then(function () {
                callback && callback();
            });
        });
        cc.systemEvent.on(EventKey_1.default.ON_CLOSE_POPUP_LOADING_EVENT, function () {
            UIManager_1.default.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
        });
        //#region ======== balance ========
        cc.systemEvent.on(EventKey_1.default.ON_ADD_CURRENT_BALANCE_EVENT, function (amount, transaction) {
            WalletHandler_1.default.getInstance().addMoney(amount, transaction);
        });
        cc.systemEvent.on(EventKey_1.default.GET_CURRENT_BALANCE_EVENT, function (callback) {
            cc.log("GET_CURRENT_BALANCE_EVENT");
            var currentBalance = WalletHandler_1.default.getInstance().currentBalance;
            callback && callback(currentBalance);
        });
        cc.systemEvent.on(EventKey_1.default.ON_ENABLE_WALLET_EVENT, function (topic, callback) {
            // console.error(EventKey.ON_ENABLE_WALLET_EVENT, topic);
            WalletHandler_1.default.getInstance().onEnableGame(topic);
            callback && callback();
        });
        cc.systemEvent.on(EventKey_1.default.ON_DISABLE_WALLET_EVENT, function (topic, callback) {
            // console.error(EventKey.ON_DISABLE_WALLET_EVENT, topic);
            WalletHandler_1.default.getInstance().onDisableGame(topic);
            callback && callback();
        });
        cc.systemEvent.on(EventKey_1.default.ON_GET_LATEST_BALANCE_EVENT, function (callback) {
            // console.error(EventKey.ON_GET_LATEST_BALANCE_EVENT);
            WalletHandler_1.default.getInstance().getLatestBalance(callback);
        });
        //#region ======== event system ========
        cc.systemEvent.on(EventKey_1.default.ON_REMOVE_ALL_EVENT, function () {
            _this.removeAllEvent();
        });
        cc.systemEvent.on(EventKey_1.default.ON_REMOVE_EVENT, function (keyEvent) {
            _this.removeEvent(keyEvent);
        });
        cc.systemEvent.on(EventKey_1.default.ON_DISCONNECT_EVENT, function (code, isRetry, msg) {
            EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, code, isRetry, msg);
        });
        // cc.systemEvent.on(EventKey.UPDATE_STATE_TXMN, (state) => {
        //   if (MiniGameManager.getInstance().countDownSicboMiniComp) MiniGameManager.getInstance().countDownSicboMiniComp.updateState(state);
        // });
        cc.systemEvent.on(EventKey_1.default.ON_SET_GAME_TYPE_EVENT, function (topic) {
            if (topic == topic_pb_1.Topic.BAU_CUA)
                GameUtils_1.default.gameTopic = topic;
        });
        cc.systemEvent.on(EventKey_1.default.ON_SHOW_TEXT_NOTIFY, function (notifyText, time) {
            if (time === void 0) { time = 2; }
            UIManager_1.default.getInstance().openNotify(TextNotify_1.default, GameDefine_1.default.TextNotify, new TextNotify_1.TextNotifyData(notifyText, time));
        });
        cc.systemEvent.on(EventKey_1.default.CG_SHOW_TEXT_NOTIFTY_INVETED, function () {
            UIManager_1.default.getInstance().openNotify(TextNotify_1.default, GameDefine_1.default.TextNotify, new TextNotify_1.TextNotifyData("Đã gửi lời mời tới người chơi khác!"));
        });
        cc.systemEvent.on("CG_REPORT_RESULT", function (rs) {
            var textMessage = "";
            switch (rs) {
                case 0: //success
                    textMessage = PortalText_1.default.POR_CARD_REPORT_SUCCESSED;
                    break;
                case 1: //Anonymous cannot report
                    textMessage = PortalText_1.default.POR_CARD_REPORT_ANONYMOUS;
                    break;
                case 2: //Reported
                    textMessage = PortalText_1.default.POR_CARD_REPORT_FAILED_REPORTED;
                    break;
                case 3: //Cannot report, not enough player
                    textMessage = PortalText_1.default.POR_CARD_REPORT_FAILED_NOT_ENOUGH_PLAYER;
                    break;
            }
            UIManager_1.default.getInstance().openNotify(TextNotify_1.default, GameDefine_1.default.TextNotify, new TextNotify_1.TextNotifyData(textMessage));
        });
        //-=============
    };
    //#region ======== network ========
    EventBusManager.onPingTime = function (ping) {
        if (ping === void 0) { ping = 0; }
        cc.systemEvent.emit(EventKey_1.default.ON_PING_TIME, ping);
    };
    EventBusManager.onPingError = function () {
        cc.systemEvent.emit(EventKey_1.default.ON_PING_ERROR);
    };
    EventBusManager.onFireDisconnect = function (msg) {
        if (msg === void 0) { msg = ""; }
        cc.systemEvent.emit(EventKey_1.default.ON_DISCONNECT_EVENT, msg);
    };
    EventBusManager.onFireReconnected = function (msg) {
        if (msg === void 0) { msg = ""; }
        cc.systemEvent.emit(EventKey_1.default.ON_RECONNECTED_EVENT, msg);
    };
    //#region ======== balance ========
    EventBusManager.onFireUpdateBalance = function (balance) {
        cc.systemEvent.emit(EventKey_1.default.ON_UPDATE_BALANCE_EVENT, balance);
    };
    EventBusManager.onFireAddBalance = function (amount, balance) {
        cc.systemEvent.emit(EventKey_1.default.ON_ADD_BALANCE_EVENT, amount, balance);
    };
    EventBusManager.removeAllEvent = function () {
        cc.log("removeAllEvent");
        var propNames = Object.getOwnPropertyNames(EventKey_1.default);
        propNames.forEach(function (propName) {
            cc.systemEvent.off(propName);
        });
        cc.systemEvent.off(EventKey_1.default.ON_OPEN_POPUP_DISCONNECT_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_OPEN_POPUP_CHAT_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_OPEN_POPUP_INFO_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_SOUND_BG_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_SOUND_EFF_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_REMOVE_ALL_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_REMOVE_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_CLOSE_POPUP_LOADING_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_OPEN_POPUP_LOADING_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_OPEN_POPUP_WARNING_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_ADD_CURRENT_BALANCE_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_ENABLE_WALLET_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_DISABLE_WALLET_EVENT);
        cc.systemEvent.off(EventKey_1.default.LISTEN_CHAT_CARD);
        cc.systemEvent.off(EventKey_1.default.LISTEN_CHAT_TABLE);
        cc.systemEvent.off(EventKey_1.default.LISTEN_CHAT_LODE);
        cc.systemEvent.off(EventKey_1.default.LISTEN_CHAT_MINI);
        cc.systemEvent.off(EventKey_1.default.SHOW_CHAT_ERR_LODE);
        cc.systemEvent.off(EventKey_1.default.SHOW_CHAT_ERR_MINI);
        cc.systemEvent.off(EventKey_1.default.SEND_CHAT_MINI);
        cc.systemEvent.off(EventKey_1.default.TURN_ON_COUNT_DOWN_TXMN);
        cc.systemEvent.off(EventKey_1.default.TURN_OFF_COUNT_DOWN_TXMN);
        cc.systemEvent.off(EventKey_1.default.UPDATE_STATE_TXMN);
        cc.systemEvent.off(EventKey_1.default.GET_CURRENT_BALANCE_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_GET_LATEST_BALANCE_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_DISCONNECT_EVENT);
        cc.systemEvent.off(EventKey_1.default.GET_EMOJI);
        cc.systemEvent.off(EventKey_1.default.GET_HISTORY_CHAT_MINI);
        cc.systemEvent.off(EventKey_1.default.SETTING_CHAT);
        cc.systemEvent.off(EventKey_1.default.SETTING_CHAT_MINI);
        cc.systemEvent.off(EventKey_1.default.ON_SET_GAME_TYPE_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_BACK_TO_PORTAL_EVENT);
        //Card games
        cc.systemEvent.off(EventKey_1.default.ON_BACK_TO_CARD_LOBBY_EVENT);
        cc.systemEvent.off(EventKey_1.default.ON_SHOW_TEXT_NOTIFY);
        cc.systemEvent.off(EventKey_1.default.CG_SHOW_TEXT_NOTIFTY_INVETED);
        cc.systemEvent.off(EventKey_1.default.CG_OPEN_SETTING_POPUP);
        cc.systemEvent.off(EventKey_1.default.CG_OPEN_INFO_POPUP);
        cc.systemEvent.off(EventKey_1.default.CG_GET_CHAT_SAMPLE);
        cc.systemEvent.off(EventKey_1.default.CG_OPEN_CONFIRM_START_GAME);
        cc.systemEvent.off(EventKey_1.default.CG_OPEN_SHARE_TABLE_POPUP);
        cc.systemEvent.off(EventKey_1.default.CG_PLAY_SFX_CLICK_BUTTON);
        cc.systemEvent.off(EventKey_1.default.ON_SHOW_EVENT_POPUP_FOR_TXMN);
        cc.systemEvent.off(EventKey_1.default.ON_GET_CHAT_ENABLE);
        cc.systemEvent.off(EventKey_1.default.ON_GET_MINI_CHAT_ENABLE);
        cc.systemEvent.off(EventKey_1.default.ON_OPEN_GIFT_CODE_HISTORY);
        cc.systemEvent.off(EventKey_1.default.ON_PING_TIME);
        cc.systemEvent.off("ON_SET_POS_MINI_GAME_HUB_FOR_CARD_GAME");
        cc.systemEvent.off("CG_REPORT_RESULT");
        cc.systemEvent.off("FIREBASE_TRACKING_EVENT");
    };
    EventBusManager.removeEvent = function (keyEvent) {
        cc.systemEvent.off(keyEvent);
    };
    var EventBusManager_1;
    EventBusManager = EventBusManager_1 = __decorate([
        ccclass
    ], EventBusManager);
    return EventBusManager;
}());
exports.default = EventBusManager;

cc._RF.pop();