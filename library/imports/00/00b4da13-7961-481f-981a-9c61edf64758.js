"use strict";
cc._RF.push(module, '00b4doTeWFIH5ganGHt9kdY', 'SicboBetComp');
// Game/Sicbo_New/Scripts/Components/SicboBetComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Door = _a.Door, State = _a.State, DoorAndBetAmount = _a.DoorAndBetAmount, Status = _a.Status;
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboUIComp_1 = require("./SicboUIComp");
var SicboHelper_1 = require("../Helpers/SicboHelper");
var SicboCoinItemComp_1 = require("./Items/SicboCoinItemComp");
var SicboBetSlotItemComp_1 = require("./Items/SicboBetSlotItemComp");
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboController_1 = require("../Controllers/SicboController");
var SicboScrollViewWithButton_1 = require("../RNGCommons/UIComponents/SicboScrollViewWithButton");
var SicboGameUtils_1 = require("../RNGCommons/Utils/SicboGameUtils");
var SicboCoinType_1 = require("../RNGCommons/SicboCoinType");
var SicboBetComp = /** @class */ (function (_super) {
    __extends(SicboBetComp, _super);
    function SicboBetComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sicboUIComp = null;
        _this.chipsScrollView = null;
        _this.currentSelectedChip = null;
        _this.largeChipPrefab = null;
        _this.rebetButton = null;
        _this.doubleBetButton = null;
        _this.betSlotsNode = null;
        _this.allBetSlots = [];
        _this.miniChipRoot = null;
        _this.miniChipPool = null;
        _this.loseChipTarget = null;
        _this.winChipPos = null;
        _this._disableChip = false;
        _this.chip_data = [];
        _this.chipItems = [];
        //ANCHOR chip stack pos config
        _this.chipStackPosConfig = {
            anyTrippleCloneSpacing: 30,
            singleCloneSpacing: 3,
            smallCloneSpacing: 40,
            bigCloneSpacing: -40,
            cloneSpacing: 4,
            chipSpacing: 4,
            chipStackOffsetRight: new cc.Vec2(-27, 25),
            chipStackOffsetLeft: new cc.Vec2(27, 25),
            anyTripple: [
                {
                    pos: new cc.Vec2(-84, -34),
                    height: 10,
                },
                {
                    pos: new cc.Vec2(-56, -33),
                    height: 10,
                }
            ],
            trippleLeft: [
                {
                    pos: new cc.Vec2(-82, -8),
                    height: 10,
                },
            ],
            trippleRight: [
                {
                    pos: new cc.Vec2(82, -8),
                    height: 10,
                },
            ],
            small: [
                {
                    pos: new cc.Vec2(-75.5, -50),
                    height: 10,
                },
                {
                    pos: new cc.Vec2(-46, -51),
                    height: 10,
                },
            ],
            big: [
                {
                    pos: new cc.Vec2(76.5, -51),
                    height: 10,
                },
                {
                    pos: new cc.Vec2(47, -50),
                    height: 10,
                },
            ],
            four_sum: [
                {
                    pos: new cc.Vec2(-20, -23.5),
                    height: 10,
                },
            ],
            five_sum: [
                {
                    pos: new cc.Vec2(-20, -23.5),
                    height: 10,
                },
            ],
            six_sum: [
                {
                    pos: new cc.Vec2(-23, -23.5),
                    height: 10,
                },
            ],
            seven_sum: [
                {
                    pos: new cc.Vec2(-20, -23.5),
                    height: 10,
                },
            ],
            eight_sum: [
                {
                    pos: new cc.Vec2(-20, -23.5),
                    height: 10,
                },
            ],
            nine_sum: [
                {
                    pos: new cc.Vec2(-19, -23.5),
                    height: 10,
                },
            ],
            ten_sum: [
                {
                    pos: new cc.Vec2(-19, -23.5),
                    height: 10,
                },
            ],
            eleven_sum: [
                {
                    pos: new cc.Vec2(-18, -23.5),
                    height: 10,
                },
            ],
            twelve_sum: [
                {
                    pos: new cc.Vec2(-18, -23.5),
                    height: 10,
                },
            ],
            thirteen_sum: [
                {
                    pos: new cc.Vec2(-16.5, -23.5),
                    height: 10,
                },
            ],
            fourteen_sum: [
                {
                    pos: new cc.Vec2(-16, -23.5),
                    height: 10,
                },
            ],
            fifteen_sum: [
                {
                    pos: new cc.Vec2(-15.5, -23.5),
                    height: 10,
                },
            ],
            sixteen_sum: [
                {
                    pos: new cc.Vec2(-15.5, -23.5),
                    height: 10,
                },
            ],
            seventeen_sum: [
                {
                    pos: new cc.Vec2(-14.5, -23.5),
                    height: 10,
                },
            ],
            single: [
                {
                    pos: new cc.Vec2(-57.5, -22),
                    height: 10,
                },
            ],
        };
        return _this;
    }
    SicboBetComp.prototype.onLoad = function () {
        this.sicboUIComp = this.node.parent.getComponentInChildren(SicboUIComp_1.default);
        this.allBetSlots = this.betSlotsNode.getComponentsInChildren(SicboBetSlotItemComp_1.default);
        this.miniChipPool = new cc.NodePool();
    };
    SicboBetComp.prototype.getDoor = function (door) {
        return this.allBetSlots.find(function (x) { return x.door == door; });
    };
    //SECTION Chip scroll view
    SicboBetComp.prototype.createChipItems = function (data) {
        var _a;
        this.chip_data = data;
        this.createItemChipCoins(data);
        this.selectChip(this.chipItems[0], false);
        this.chipsScrollView.scrollToTopLeft();
        (_a = this.chipsScrollView.getComponent(SicboScrollViewWithButton_1.default)) === null || _a === void 0 ? void 0 : _a.checkScrollViewButton();
    };
    SicboBetComp.prototype.createItemChipCoins = function (data) {
        var _this = this;
        data.forEach(function (chipValue) {
            _this.createItemCoin(chipValue);
        });
    };
    SicboBetComp.prototype.createItemCoin = function (chipValue) {
        var _this = this;
        var coinType = SicboHelper_1.default.convertValueToCoinType(chipValue);
        var newItem = SicboGameUtils_1.default.createItemFromPrefab(SicboCoinItemComp_1.default, this.largeChipPrefab, this.chipsScrollView.content);
        var itemData = new SicboCoinItemComp_1.SicboCoinItemCompData();
        itemData.coinType = coinType;
        itemData.onClick = function (sicboCoinItemComp) {
            _this.selectChip(sicboCoinItemComp);
        };
        newItem.setData(itemData);
        newItem.isDisable = this.disableChip;
        this.chipItems.push(newItem);
    };
    SicboBetComp.prototype.selectChip = function (sicboCoinItemComp, playSfx) {
        if (playSfx === void 0) { playSfx = true; }
        if (this.disableChip)
            return;
        if (this.currentSelectedChip != null)
            this.currentSelectedChip.selectEff(false);
        this.currentSelectedChip = sicboCoinItemComp;
        this.currentSelectedChip.selectEff(true);
        //TODO: change sound khi bam chip binh thuong va disable
        if (playSfx)
            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxClick);
    };
    SicboBetComp.prototype.getSelectedChipType = function () {
        return this.currentSelectedChip.data.coinType;
    };
    SicboBetComp.prototype.onClickChipScrollNext = function () {
        var numOfItem = this.chipsScrollView.content.childrenCount;
        var spacing = this.chipsScrollView.content.width / numOfItem;
        var offset = this.chipsScrollView.getScrollOffset();
        var maxOffset = this.chipsScrollView.getMaxScrollOffset();
        this.chipsScrollView.scrollToPercentHorizontal((Math.abs(offset.x) + spacing) / maxOffset.x, .5);
    };
    SicboBetComp.prototype.onClickChipScrollPre = function () {
        var numOfItem = this.chipsScrollView.content.childrenCount;
        var spacing = this.chipsScrollView.content.width / numOfItem;
        var offset = this.chipsScrollView.getScrollOffset();
        var maxOffset = this.chipsScrollView.getMaxScrollOffset();
        this.chipsScrollView.scrollToPercentHorizontal((Math.abs(offset.x) - spacing) / maxOffset.x, .5);
    };
    SicboBetComp.prototype.scrollToInsideView = function (chip) {
        var _a;
        this.chipsScrollView.stopAutoScroll();
        var pos = SicboGameUtils_1.default.convertToOtherNode(chip, this.chipsScrollView.node);
        if (pos.x >= -230 && pos.x <= 200)
            return; // Inside view
        var offset = this.chipsScrollView.getScrollOffset();
        var maxOffset = this.chipsScrollView.getMaxScrollOffset();
        this.chipsScrollView.scrollToPercentHorizontal((Math.abs(offset.x) + pos.x) / maxOffset.x);
        (_a = this.chipsScrollView.getComponent(SicboScrollViewWithButton_1.default)) === null || _a === void 0 ? void 0 : _a.checkScrollViewButton();
    };
    //!SECTION
    //#region BET IN TABLE
    /**
    * Bam vao o dat cuoc tren ban
        */
    SicboBetComp.prototype.onClickBetSlotItem = function (betSlotItemComp) {
        if (this.disableChip)
            return;
        this.scrollToInsideView(this.currentSelectedChip.node);
        this.sicboUIComp.myUserBet(betSlotItemComp.door, this.currentSelectedChip.data.coinType);
        // this.putMiniChipOnTableImmediately(this.currentSelectedChip.data.coinType, betSlotItemComp);
    };
    SicboBetComp.prototype.createItemCoinMini = function (coinType) {
        var newItem = null;
        if (this.miniChipPool.size() > 0) {
            newItem = this.miniChipPool.get().getComponent("SicboCoinItemComp");
        }
        else {
            newItem = SicboGameUtils_1.default.createItemFromPrefab(SicboCoinItemComp_1.default, this.largeChipPrefab, this.chipsScrollView.content);
        }
        newItem.node.parent = this.miniChipRoot;
        newItem.node.scale = 1;
        newItem.node.opacity = 255;
        var itemData = new SicboCoinItemComp_1.SicboCoinItemCompData();
        itemData.coinType = coinType;
        itemData.isMini = true;
        newItem.setData(itemData);
        // newItem.rotation = -this.randomRange(0, 360);
        //ANCHOR minichip Scale
        // newItem.isOnTable = true;
        return newItem;
    };
    SicboBetComp.prototype.recycleItemCoinMini = function (item) {
        item.opacity = 255;
        this.miniChipPool.put(item);
        cc.Tween.stopAllByTarget(item);
    };
    SicboBetComp.prototype.getRandomPos = function (betSlotItemComp) {
        var betArea = betSlotItemComp.miniChipArea;
        var rs = cc.Vec2.ZERO;
        var offset = cc.Vec2.ZERO; // new cc.Vec2(20, 20);
        if (betSlotItemComp.door == Door.BIG)
            offset = new cc.Vec2(-22, 0); //NOTE for layer fix when throw chip
        if (betSlotItemComp.door == Door.SMALL)
            offset = new cc.Vec2(22, 0); //NOTE for layer fix when throw chip
        var halfW = (betArea.width) / 2;
        var halfH = (betArea.height) / 2;
        rs = new cc.Vec2(this.randomRange(-halfW, halfW) + offset.x, this.randomRange(-halfH, halfH) + offset.y);
        rs.add(betArea.getPosition());
        var rsV3 = SicboGameUtils_1.default.convertToOtherNode(betArea, this.miniChipRoot, new cc.Vec3(rs.x, rs.y, 0));
        rs.x = rsV3.x;
        rs.y = rsV3.y;
        return rs;
    };
    SicboBetComp.prototype.randomRange = function (min, max) {
        return Math.random() * (max - min) + min;
    };
    SicboBetComp.prototype.getMiniChipStaticPos = function (betSlot) {
        var chipSpacing = this.chipStackPosConfig.chipSpacing;
        var count = betSlot.miniChipsOnSlot.length;
        var col = 0;
        var row = count;
        // if (betSlot.door == Door.ANY_TRIPLE) {
        if (this.getChipStackMaxCol(betSlot.door) > 1) {
            var maxHeight0 = this.getChipStackHeight(betSlot.door, 0);
            if (count >= maxHeight0) {
                col = 1;
                row = count - maxHeight0;
            }
        }
        var startPos = this.getChipStackStartPos(betSlot.door, col);
        var y = startPos.y;
        y += row * chipSpacing;
        return SicboGameUtils_1.default.convertToOtherNode2(betSlot.miniChipArea, this.miniChipRoot, new cc.Vec2(startPos.x, y));
    };
    //ANCHOR  putMiniChipOnTableImmediately
    SicboBetComp.prototype.putMiniChipOnTableImmediately = function (coinType, betSlotItemComp) {
        var chip = this.createItemCoinMini(coinType);
        chip.node.setPosition(this.getRandomPos(betSlotItemComp));
        if (this.isStaticSlot(betSlotItemComp.door)) {
            chip.node.setPosition(this.getMiniChipStaticPos(betSlotItemComp));
        }
        else {
            chip.node.setPosition(this.getRandomPos(betSlotItemComp));
        }
        this.changeParent(chip.node, betSlotItemComp.miniChipArea);
        betSlotItemComp.addMiniChipToSlot(chip);
        this.removeRedundancyChip(betSlotItemComp);
    };
    SicboBetComp.prototype.putMiniChipOnTableImmediatelyByValue = function (totalValue, betSlotItemComp) {
        var _this = this;
        if (betSlotItemComp == null) {
            //cc.log("putMiniChipOnTableImmediatelyByValue NO DOOR FOUND");
            return;
        }
        var parsedCoin = SicboHelper_1.default.calculateNumOfChipToMakeValue(totalValue, this.chip_data);
        parsedCoin.forEach(function (num, val) {
            var coinType = SicboHelper_1.default.convertValueToCoinType(val);
            for (var i = 0; i < num; i++) {
                _this.putMiniChipOnTableImmediately(coinType, betSlotItemComp);
            }
        });
    };
    //ANCHOR  throwMiniChipOnTable
    SicboBetComp.prototype.throwMiniChipOnTableByValue = function (totalValue, from, betSlotItemComp) {
        var _this = this;
        if (from === void 0) { from = null; }
        if (betSlotItemComp == null) {
            //cc.log("throwMiniChipOnTableByValue NO DOOR FOUND");
            return;
        }
        var parsedCoin = SicboHelper_1.default.calculateNumOfChipToMakeValue(totalValue, this.chip_data);
        parsedCoin.forEach(function (num, val) {
            // //console.log("val: " + val + " -> " + num);
            var coinType = SicboHelper_1.default.convertValueToCoinType(val);
            for (var i = 0; i < num; i++) {
                _this.throwMiniChipOnTable(coinType, from, betSlotItemComp);
            }
        });
    };
    SicboBetComp.prototype.throwMiniChipOnTable = function (coinType, from, betSlotItemComp) {
        var _this = this;
        if (from === void 0) { from = null; }
        if (from == null)
            from = this.currentSelectedChip.node;
        var chip = this.createItemCoinMini(coinType);
        var startPos = SicboGameUtils_1.default.convertToOtherNode(from, betSlotItemComp.miniChipArea);
        var targetPos = this.getMiniChipStaticPos(betSlotItemComp);
        var randomPos = this.getRandomPos(betSlotItemComp);
        targetPos = SicboGameUtils_1.default.convertToOtherNode2(chip.node.parent, betSlotItemComp.miniChipArea, targetPos);
        randomPos = SicboGameUtils_1.default.convertToOtherNode2(chip.node.parent, betSlotItemComp.miniChipArea, randomPos);
        chip.node.parent = betSlotItemComp.miniChipArea;
        chip.node.setPosition(startPos);
        // chip.node.setSiblingIndex
        betSlotItemComp.addMiniChipToSlot(chip);
        var chipThrowDuration = 0.9;
        var fadeOutDelay = 0.5;
        if (betSlotItemComp.door != Door.BIG && betSlotItemComp.door != Door.SMALL) {
            randomPos = cc.Vec2.ZERO;
            fadeOutDelay = 0;
        }
        chip.node.scale = 1.2;
        chip.node.opacity = 255;
        cc.tween(chip.node)
            .to(chipThrowDuration / 3, { x: randomPos.x, y: randomPos.y, scale: 1 })
            .delay(fadeOutDelay)
            .to(chipThrowDuration / 3, { opacity: 0, x: targetPos.x, y: targetPos.y })
            //.to(0, {x:targetPos.x, y:targetPos.y})
            .to(chipThrowDuration / 3, { opacity: 255 })
            .call(function () {
            _this.removeRedundancyChip(betSlotItemComp);
        })
            .start();
    };
    SicboBetComp.prototype.changeParent = function (child, newParent) {
        SicboHelper_1.default.changeParent(child, newParent);
    };
    //#endregion
    //SECTION WIN - LOSE
    //ANCHOR chip to lose hole animation
    SicboBetComp.prototype.playLoseAnimation = function (winDoors, duration) {
        var _this = this;
        if (duration === void 0) { duration = .75; }
        var tweenParams = {
            position: cc.Vec3.ZERO,
        };
        this.allBetSlots.forEach(function (betSlot) {
            if (winDoors.find(function (x) { return x == betSlot.door; }) == null) {
                var lostChips = betSlot.miniChipsOnSlot;
                var self_1 = _this;
                self_1.tweenChipToAnotherNode(self_1.loseChipTarget, lostChips, duration, 0, tweenParams, true, function (chip) {
                    //ANCHOR SOUND
                    SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxChipFly);
                    self_1.recycleItemCoinMini(chip.node);
                });
                betSlot.clearMiniChipOnSlot();
            }
        });
    };
    //ANCHOR stack chip into column
    SicboBetComp.prototype.playChipStackAnimation = function (winDoors, duration) {
        var _this = this;
        if (duration === void 0) { duration = .5; }
        //cc.log(duration);
        var chipSpacing = this.chipStackPosConfig.chipSpacing;
        this.allBetSlots.forEach(function (betSlot) {
            if (_this.isStaticSlot(betSlot.door))
                return;
            if (winDoors.find(function (x) { return x == betSlot.door; }) != null) {
                var chips = betSlot.miniChipsOnSlot;
                betSlot.chipColCount = 0;
                var count = 0;
                var maxCol = _this.getChipStackMaxCol(betSlot.door);
                for (var col = 0; col < maxCol; col++) {
                    betSlot.chipColCount++;
                    var maxHeight = _this.getChipStackHeight(betSlot.door, col);
                    var startPos = _this.getChipStackStartPos(betSlot.door, col);
                    var offSet = cc.Vec2.ZERO; //this.getChipStackOffset(betSlot.door);
                    var x = startPos.x;
                    var _loop_1 = function (h) {
                        if (chips.length <= 0)
                            return "break";
                        var y = startPos.y;
                        y += h * chipSpacing;
                        var idx = Math.floor(Math.random() * chips.length);
                        var chip = chips[idx];
                        chips.splice(idx, 1);
                        count++;
                        chip.node.setSiblingIndex(count);
                        cc.tween(chip.node)
                            .to(duration, { x: x + offSet.x, y: y + offSet.y })
                            .call(function () {
                            //ANCHOR SOUND
                            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxChipFly);
                            cc.Tween.stopAllByTarget(chip.node);
                        })
                            .start();
                    };
                    for (var h = 0; h < maxHeight; h++) {
                        var state_1 = _loop_1(h);
                        if (state_1 === "break")
                            break;
                    }
                }
                for (var i = 0; i < chips.length; i++) {
                    var chip = chips[i];
                    _this.recycleItemCoinMini(chip.node);
                }
                // betSlot.miniChipsOnSlot = cachedMiniChip;
            }
        });
    };
    SicboBetComp.prototype.getChipStackConfig = function (door) {
        switch (door) {
            case Door.ANY_TRIPLE:
                return this.chipStackPosConfig.anyTripple;
            case Door.BIG:
                return this.chipStackPosConfig.big;
            case Door.SMALL:
                return this.chipStackPosConfig.small;
            case Door.ONE_TRIPLE:
            case Door.TWO_TRIPLE:
            case Door.THREE_TRIPLE:
                return this.chipStackPosConfig.trippleLeft;
            case Door.FOUR_TRIPLE:
            case Door.FIVE_TRIPLE:
            case Door.SIX_TRIPLE:
                return this.chipStackPosConfig.trippleRight;
            case Door.ONE_SINGLE:
            case Door.TWO_SINGLE:
            case Door.THREE_SINGLE:
            case Door.FOUR_SINGLE:
            case Door.FIVE_SINGLE:
            case Door.SIX_SINGLE:
                return this.chipStackPosConfig.single;
            case Door.FOUR_SUM: return this.chipStackPosConfig.four_sum;
            case Door.FIVE_SUM: return this.chipStackPosConfig.five_sum;
            case Door.SIX_SUM: return this.chipStackPosConfig.six_sum;
            case Door.SEVEN_SUM: return this.chipStackPosConfig.seven_sum;
            case Door.EIGHT_SUM: return this.chipStackPosConfig.eight_sum;
            case Door.NINE_SUM: return this.chipStackPosConfig.nine_sum;
            case Door.TEN_SUM: return this.chipStackPosConfig.ten_sum;
            case Door.ELEVEN_SUM: return this.chipStackPosConfig.eleven_sum;
            case Door.TWELVE_SUM: return this.chipStackPosConfig.twelve_sum;
            case Door.THIRTEEN_SUM: return this.chipStackPosConfig.thirteen_sum;
            case Door.FOURTEEN_SUM: return this.chipStackPosConfig.fourteen_sum;
            case Door.FIFTEEN_SUM: return this.chipStackPosConfig.fifteen_sum;
            case Door.SIXTEEN_SUM: return this.chipStackPosConfig.sixteen_sum;
            case Door.SEVENTEEN_SUM: return this.chipStackPosConfig.seventeen_sum;
        }
    };
    SicboBetComp.prototype.getChipStackStartPos = function (door, col) {
        return this.getChipStackConfig(door)[col].pos;
    };
    SicboBetComp.prototype.getChipStackOffset = function (door) {
        return this.isRightChipStack(door) ? this.chipStackPosConfig.chipStackOffsetRight : this.chipStackPosConfig.chipStackOffsetLeft;
    };
    SicboBetComp.prototype.getChipStackHeight = function (door, col) {
        return this.getChipStackConfig(door)[col].height;
    };
    SicboBetComp.prototype.getChipStackMaxCol = function (door) {
        return this.getChipStackConfig(door).length;
    };
    SicboBetComp.prototype.getCloneSpacing = function (door) {
        switch (door) {
            case Door.ANY_TRIPLE:
                return this.chipStackPosConfig.anyTrippleCloneSpacing;
            case Door.ONE_SINGLE:
            case Door.TWO_SINGLE:
            case Door.THREE_SINGLE:
            case Door.FOUR_SINGLE:
            case Door.FIVE_SINGLE:
            case Door.SIX_SINGLE:
                return this.chipStackPosConfig.singleCloneSpacing;
            case Door.BIG: return this.chipStackPosConfig.bigCloneSpacing;
            case Door.SMALL: return this.chipStackPosConfig.smallCloneSpacing;
        }
        return this.isRightChipStack(door) ? -this.chipStackPosConfig.cloneSpacing : this.chipStackPosConfig.cloneSpacing;
    };
    SicboBetComp.prototype.isStaticSlot = function (door) {
        return true;
        switch (door) {
            case Door.ANY_TRIPLE:
            case Door.ONE_TRIPLE:
            case Door.TWO_TRIPLE:
            case Door.THREE_TRIPLE:
            case Door.FOUR_TRIPLE:
            case Door.FIVE_TRIPLE:
            case Door.SIX_TRIPLE:
                return true;
        }
        return false;
    };
    SicboBetComp.prototype.isRightChipStack = function (door) {
        switch (door) {
            case Door.FOUR_TRIPLE:
            case Door.FIVE_TRIPLE:
            case Door.SIX_TRIPLE:
            case Door.BIG:
                return true;
        }
        return false;
    };
    SicboBetComp.prototype.getCloneStackSibling = function (door) {
        return this.isRightChipStack(door) ? 1 : 0;
    };
    //ANCHOR return won chips to slot    
    SicboBetComp.prototype.dealerReturnWinChip = function (winDoors, onFinish) {
        if (onFinish === void 0) { onFinish = null; }
        return this.dealerReturnChipOneByOne(winDoors, .05, .5, onFinish);
    };
    SicboBetComp.prototype.dealerReturnFullStackChip = function (winDoors, duration, onFinish) {
        var _this = this;
        if (onFinish === void 0) { onFinish = null; }
        this.allBetSlots.forEach(function (betSlot) {
            if (winDoors.find(function (x) { return x == betSlot.door; }) != null) {
                var offset = _this.getChipStackOffset(betSlot.door);
                var cloneChips = cc.instantiate(betSlot.miniChipArea);
                var spacing = _this.getCloneSpacing(betSlot.door);
                cloneChips.parent = _this.winChipPos;
                cloneChips.position = new cc.Vec3(offset.x, offset.y, 0);
                betSlot.setStackChipRoot(cloneChips);
                _this.changeParent(cloneChips, betSlot.node);
                cloneChips.setSiblingIndex(_this.getCloneStackSibling(betSlot.door));
                cc.tween(cloneChips)
                    .to(duration, { position: new cc.Vec3(offset.x + spacing, 0, 0) })
                    .call(function () {
                    if (onFinish)
                        onFinish(betSlot.door);
                })
                    .start();
            }
        });
    };
    SicboBetComp.prototype.dealerReturnChipOneByOne = function (winDoors, delayBetweenChipTime, chipFlyTime, onFinish) {
        var _this = this;
        if (delayBetweenChipTime === void 0) { delayBetweenChipTime = .05; }
        if (chipFlyTime === void 0) { chipFlyTime = .5; }
        if (onFinish === void 0) { onFinish = null; }
        var maxEffectTime = 0;
        this.allBetSlots.forEach(function (betSlot) {
            if (winDoors.find(function (x) { return x == betSlot.door; }) != null) {
                var offset = _this.getChipStackOffset(betSlot.door);
                var cloneChips = cc.instantiate(betSlot.miniChipArea);
                var spacing = _this.getCloneSpacing(betSlot.door);
                cloneChips.parent = betSlot.node;
                cloneChips.position = new cc.Vec3(offset.x + spacing, 0, 0);
                betSlot.setStackChipRoot(cloneChips);
                // this.changeParent(cloneChips, betSlot.node);
                cloneChips.setSiblingIndex(_this.getCloneStackSibling(betSlot.door));
                var chipsInStack_1 = cloneChips.children;
                var delay = delayBetweenChipTime;
                var startPos = SicboGameUtils_1.default.convertToOtherNode(_this.winChipPos, cloneChips);
                maxEffectTime = Math.max(maxEffectTime, chipsInStack_1.length * delay + chipFlyTime);
                var _loop_2 = function (i) {
                    var chip = chipsInStack_1[i];
                    var chipPos = chip.position;
                    chip.position = startPos;
                    chip.active = false;
                    cc.tween(_this.node)
                        .delay(i * delay)
                        .call(function () {
                        chip.active = true;
                        cc.tween(chip).to(chipFlyTime, { position: chipPos })
                            .call(function () {
                            //ANCHOR SOUND
                            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxChipFly);
                            if (i == chipsInStack_1.length - 1) {
                                if (onFinish)
                                    onFinish(betSlot.door);
                            }
                        })
                            .start();
                    })
                        .start();
                };
                for (var i = 0; i < chipsInStack_1.length; i++) {
                    _loop_2(i);
                }
            }
        });
        return maxEffectTime;
    };
    //ANCHOR chip to win player animation
    SicboBetComp.prototype.playPayingAnimation = function (winDoor, user, wonAmount, duration) {
        var _this = this;
        if (duration === void 0) { duration = 1; }
        var tweenParams = {
            useBezier: true,
            position: cc.Vec3.ZERO,
        };
        var betSlot = this.getDoor(winDoor);
        var winChips = this.getChipForPaying(wonAmount);
        var tweenChips = [];
        winChips.forEach(function (coinType) {
            var chipComp = _this.createItemCoinMini(coinType);
            chipComp.node.parent = betSlot.miniChipArea;
            chipComp.node.position = cc.Vec3.ZERO;
            tweenChips.push(chipComp);
        });
        var self = this;
        var delayBetweenChipTime = .1;
        self.tweenChipToAnotherNode(user, tweenChips, duration, delayBetweenChipTime, tweenParams, true, function (chip) {
            self.recycleItemCoinMini(chip.node);
        });
        this.clearMiniChipOnSlot(betSlot);
    };
    SicboBetComp.prototype.clearMiniChipOnSlot = function (betSlot) {
        var _this = this;
        betSlot.miniChipsOnSlot.forEach(function (chip) {
            _this.recycleItemCoinMini(chip.node);
        });
        betSlot.clearMiniChipOnSlot();
        betSlot.miniChipsOnStack.forEach(function (chip) {
            _this.recycleItemCoinMini(chip.node);
        });
        betSlot.deleteStackChipRoot();
    };
    SicboBetComp.prototype.destroyAllMiniChip = function () {
        for (var i = 0; i < this.allBetSlots.length; i++) {
            var betSlot = this.allBetSlots[i];
            betSlot.miniChipsOnSlot.forEach(function (chip) {
                var _a;
                (_a = chip.node) === null || _a === void 0 ? void 0 : _a.destroy();
            });
            betSlot.clearMiniChipOnSlot();
            betSlot.miniChipsOnStack.forEach(function (chip) {
                var _a;
                (_a = chip.node) === null || _a === void 0 ? void 0 : _a.destroy();
            });
            betSlot.deleteStackChipRoot();
        }
        this.miniChipPool.clear();
    };
    SicboBetComp.prototype.getChipForPaying = function (wonValue) {
        if (wonValue <= 5000) {
            return [SicboCoinType_1.SicboCoinType.Coin1k, SicboCoinType_1.SicboCoinType.Coin1k, SicboCoinType_1.SicboCoinType.Coin1k];
        }
        else if (wonValue <= 20000) {
            return [SicboCoinType_1.SicboCoinType.Coin5k, SicboCoinType_1.SicboCoinType.Coin5k, SicboCoinType_1.SicboCoinType.Coin5k];
        }
        else if (wonValue <= 100000) {
            return [SicboCoinType_1.SicboCoinType.Coin100k];
        }
        else if (wonValue <= 1000000) {
            return [SicboCoinType_1.SicboCoinType.Coin1M];
        }
        else {
            return [SicboCoinType_1.SicboCoinType.Coin5M, SicboCoinType_1.SicboCoinType.Coin5M, SicboCoinType_1.SicboCoinType.Coin5M];
        }
    };
    SicboBetComp.prototype.removeRedundancyChip = function (betSlot) {
        var _this = this;
        var mergeFrom = -1;
        if (this.isStaticSlot(betSlot.door)) {
            if (this.getChipStackMaxCol(betSlot.door) > 1) {
                mergeFrom = this.getChipStackHeight(betSlot.door, 0);
            }
            else {
                mergeFrom = 0;
            }
            mergeFrom += 5;
        }
        var rc = betSlot.getRedundancyChip(this.isStaticSlot(betSlot.door), mergeFrom);
        rc.forEach(function (chip) {
            _this.recycleItemCoinMini(chip.node);
        });
    };
    SicboBetComp.prototype.tweenChipToAnotherNode = function (targetNode, chipList, duration, delayBetweenChipTime, tweenParams, changeParentToTargetNode, onComplete) {
        var _this = this;
        if (delayBetweenChipTime === void 0) { delayBetweenChipTime = 0; }
        if (changeParentToTargetNode === void 0) { changeParentToTargetNode = true; }
        var _loop_3 = function (i) {
            var chip = chipList[i];
            var cachedParent = chip.node.parent;
            if (chip.node.parent == null) {
                return "continue";
            }
            this_1.changeParent(chip.node, targetNode);
            var tween = null;
            if (tweenParams.useBezier) {
                var startPos = new cc.Vec2(chip.node.position.x, chip.node.position.y);
                var endPos = new cc.Vec2(tweenParams.position.x, tweenParams.position.y);
                tween = cc.tween(chip.node).delay(i * delayBetweenChipTime).bezierTo(duration, startPos, new cc.Vec2(startPos.x, startPos.y + (endPos.y - startPos.y) * 1), endPos);
            }
            else {
                tween = cc.tween(chip.node).delay(i * delayBetweenChipTime).to(duration, tweenParams);
            }
            tween.call(function () {
                if (changeParentToTargetNode == false)
                    _this.changeParent(chip.node, cachedParent);
                if (onComplete != null)
                    onComplete(chip);
            })
                .start();
        };
        var this_1 = this;
        for (var i = 0; i < chipList.length; i++) {
            _loop_3(i);
        }
    };
    //ANCHOR Tween    
    SicboBetComp.prototype.tween = function (node, targetNode, tweenParams, onComplete) {
        var duration = tweenParams.duration || 0;
        var delay = tweenParams.delay || 0;
        var targetPos = tweenParams.positionInTargetNode || cc.Vec2.ZERO;
        var bezierPoint = tweenParams.bezierPoint || cc.Vec2.ZERO;
        targetPos = SicboGameUtils_1.default.convertToOtherNode2(node.parent, targetNode, targetPos);
        var tween = null;
        var startPos = new cc.Vec2(node.position.x, node.position.y);
        var endPos = targetPos;
        if (tweenParams.useBezier) {
            tween = cc.tween(node).delay(delay).bezierTo(duration, startPos, bezierPoint, endPos);
        }
        else {
            tween = cc.tween(node).delay(delay).to(duration, { x: endPos.x, y: endPos.y });
        }
        tween.call(function () {
            onComplete && onComplete();
        })
            .start();
    };
    //!SECTION
    //SECTION update bet number in table  
    SicboBetComp.prototype.updateMyUserBetOnTable = function (userBetAmounts) {
        var _this = this;
        userBetAmounts.forEach(function (betAmount) {
            var door = _this.getDoor(betAmount.getDoor());
            door === null || door === void 0 ? void 0 : door.setMyTotalBet(betAmount.getBetAmount());
        });
    };
    SicboBetComp.prototype.updateTotalBetOnTable = function (totalBetAmounts) {
        var _this = this;
        totalBetAmounts.forEach(function (betAmount) {
            var door = _this.getDoor(betAmount.getDoor());
            door === null || door === void 0 ? void 0 : door.setTotalBet(betAmount.getBetAmount());
        });
    };
    //!SECTION
    //SECTION ISicboState
    SicboBetComp.prototype.exitState = function (status) {
        if (status == Status.PAYING) {
            for (var i = 0; i < this.allBetSlots.length; i++) {
                this.allBetSlots[i].clearMiniChipOnSlot();
                this.allBetSlots[i].resetSlot();
            }
        }
    };
    SicboBetComp.prototype.updateState = function (state) {
        if (state.getStatus() == Status.BETTING || state.getStatus() == Status.END_BET || state.getStatus() == Status.RESULTING || state.getStatus() == Status.PAYING) {
            this.updateTotalBetOnTable(state.getDoorsAndBetAmountsList());
            this.updateMyUserBetOnTable(state.getDoorsAndPlayerBetAmountsList());
        }
        this.setDoubleBetActive(state.getAllowBetDouble());
        this.setRebetActive(state.getAllowReBet());
    };
    SicboBetComp.prototype.startState = function (state, totalDuration) {
        var _this = this;
        var status = state.getStatus();
        this.disableChip = status != Status.BETTING;
        if (status != Status.WAITING && status != Status.FINISHING) {
            state.getDoorsAndBetAmountsList().forEach(function (dab) {
                var doorComp = _this.getDoor(dab.getDoor());
                if (doorComp.miniChipsOnSlot.length == 0)
                    _this.putMiniChipOnTableImmediatelyByValue(dab.getBetAmount(), doorComp);
            });
        }
        if (status == Status.WAITING || status == Status.FINISHING) {
            for (var i = 0; i < this.allBetSlots.length; i++) {
                this.allBetSlots[i].resetSlot();
            }
        }
    };
    Object.defineProperty(SicboBetComp.prototype, "disableChip", {
        //ANCHOR disable chip
        get: function () {
            return this._disableChip;
        },
        set: function (value) {
            var _this = this;
            this._disableChip = value;
            this.chipItems.forEach(function (chip) {
                chip.isDisable = value;
                if (!value) {
                    if (_this.currentSelectedChip != null)
                        _this.selectChip(_this.currentSelectedChip, false);
                    else
                        _this.selectChip(_this.chipItems[0], false);
                }
            });
        },
        enumerable: false,
        configurable: true
    });
    //Quick bet
    SicboBetComp.prototype.setDoubleBetActive = function (isActive) {
        this.doubleBetButton.interactable = isActive;
    };
    SicboBetComp.prototype.setRebetActive = function (isActive) {
        this.rebetButton.interactable = isActive;
    };
    //!SECTION
    SicboBetComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
    };
    SicboBetComp.prototype.onHide = function () {
        cc.Tween.stopAllByTarget(this.node);
        for (var i = 0; i < this.allBetSlots.length; i++) {
            this.allBetSlots[i].resetSlot();
            // this.clearMiniChipOnSlot(this.allBetSlots[i]);
        }
        this.destroyAllMiniChip();
    };
    __decorate([
        property(cc.ScrollView)
    ], SicboBetComp.prototype, "chipsScrollView", void 0);
    __decorate([
        property(cc.Prefab)
    ], SicboBetComp.prototype, "largeChipPrefab", void 0);
    __decorate([
        property(cc.Button)
    ], SicboBetComp.prototype, "rebetButton", void 0);
    __decorate([
        property(cc.Button)
    ], SicboBetComp.prototype, "doubleBetButton", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetComp.prototype, "betSlotsNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetComp.prototype, "miniChipRoot", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetComp.prototype, "loseChipTarget", void 0);
    __decorate([
        property(cc.Node)
    ], SicboBetComp.prototype, "winChipPos", void 0);
    SicboBetComp = __decorate([
        ccclass
    ], SicboBetComp);
    return SicboBetComp;
}(cc.Component));
exports.default = SicboBetComp;

cc._RF.pop();