"use strict";
cc._RF.push(module, 'e1c50E/a6JNe5Mz0gGC0M05', 'WSClient_WebAndIOS');
// Game/Lobby/Utils/WebSocketClient/WSClient_WebAndIOS.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumGame_1 = require("../../Utils/EnumGame");
var IdentityHandler_1 = require("../../Network/IdentityHandler");
var bentau_pb_1 = require("@gameloot/bentau/bentau_pb");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var WSClient_WebAndIOS = /** @class */ (function (_super) {
    __extends(WSClient_WebAndIOS, _super);
    function WSClient_WebAndIOS() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.websocket = null;
        _this.isError = false;
        return _this;
    }
    WSClient_WebAndIOS.prototype.open = function (address, onOpen, onMessage, onClose, onError, onReconnect) {
        this.isError = false;
        this;
        try {
            this.websocket = new WebSocket(address, [IdentityHandler_1.default.getInstance().token]);
        }
        catch (error) {
            var code = EnumGame_1.CodeSocket.FORCE_CLOSE;
            var reason = "Đường truyền mạng không ổn định!";
            onError && onError(code, reason);
        }
        var self = this;
        this.websocket.binaryType = "arraybuffer";
        this.onClose = onClose;
        this.onReconnect = onReconnect;
        this.websocket.onopen = function () {
            onOpen && onOpen();
        };
        this.websocket.onmessage = function (mgs) {
            var buffer = new Uint8Array(mgs.data);
            var benTauMessage = bentau_pb_1.BenTauMessage.deserializeBinary(buffer);
            // Parse data
            onMessage && onMessage(benTauMessage);
        };
        this.websocket.onclose = function (message) {
            // Hiện tại không cần xử lý case này, case này là hệ thông tự đóng, còn các trường hợp manual close thì đã xử lý.
            //cc.log("System onClose code " + self.isError + " " + message.code + " message " + message.reason + " " + self.idWS);
        };
        this.websocket.onerror = function (message) {
            self.isError = true;
            var code = message.code;
            var reason = message.reason;
            //cc.log("onerror code " + message.code + " message " + message.reason + " " + self.idWS);
            // onError && onError(code, reason);
        };
    };
    WSClient_WebAndIOS.prototype.send = function (data) {
        if (this.websocket.readyState == WebSocket.OPEN)
            this.websocket.send(data);
    };
    WSClient_WebAndIOS.prototype.reConnect = function (code, reason) {
        // //cc.log("reConnect = " + code + " reason = " + reason + " idWS " + this.idWS + " isOpen " + this.websocket.readyState);
        this.onReconnect && this.onReconnect(code, reason);
    };
    WSClient_WebAndIOS.prototype.closeWS = function (code, reason) {
        //cc.log("closeWS = " + code + " reason = " + reason + " idWS " + this.idWS + " isOpen " + this.websocket.readyState);
        if (this.websocket == null || this.websocket.readyState == WebSocket.CLOSED)
            return;
        this.websocket.close(EnumGame_1.CodeSocket.FORCE_CLOSE);
    };
    WSClient_WebAndIOS = __decorate([
        ccclass
    ], WSClient_WebAndIOS);
    return WSClient_WebAndIOS;
}(cc.Component));
exports.default = WSClient_WebAndIOS;

cc._RF.pop();