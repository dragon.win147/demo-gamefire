"use strict";
cc._RF.push(module, '8a68bsnskZMI6KaDiaYGseu', 'SicboUserComp');
// Game/Sicbo_New/Scripts/Components/SicboUserComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var Playah = SicboModuleAdapter_1.default.getAllRefs().Playah;
var SicboUserItemComp_1 = require("./Items/SicboUserItemComp");
var SicboPortalAdapter_1 = require("../SicboPortalAdapter");
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboText_1 = require("../Setting/SicboText");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboUserComp = /** @class */ (function (_super) {
    __extends(SicboUserComp, _super);
    function SicboUserComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //MY USER UI
        _this.myUserItem = null;
        _this.otherUserGroup = null;
        _this.remainingUsersLabel = null;
        _this.usersInRoomNode = null;
        _this.usersInRoom = [];
        _this.remainingPlayersCount = 0;
        _this.usersCachePos = [];
        _this.isUserInRoomAnimPlaying = false;
        _this.userCompAnimMap = new Map(); //co data - co slot
        return _this;
    }
    SicboUserComp.prototype.onLoad = function () {
        this.usersInRoom = this.usersInRoomNode.getComponentsInChildren(SicboUserItemComp_1.default);
        for (var i = 0; i < this.usersInRoom.length; i++) {
            this.usersCachePos.push(this.usersInRoom[i].node.getPosition());
            this.userCompAnimMap.set(this.usersInRoom[i], null);
        }
        this.setMyUserItemUI();
    };
    SicboUserComp.prototype.setMyUserItemUI = function () {
        var playah = SicboPortalAdapter_1.default.getInstance().getMyPlayAh();
        this.myUserItem.setData(playah);
    };
    // getMyUserItemComp(): SicboUserItemComp{
    //     return this.myUserItem;
    // }
    SicboUserComp.prototype.findUser = function (userId) {
        var user = this.getUserInRoom(userId);
        if (user == null) {
            var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
            if (myUserId == userId)
                user = this.myUserItem;
            else
                user = this.otherUserGroup;
        }
        return user;
    };
    SicboUserComp.prototype.getUserInRoom = function (userId) {
        return this.usersInRoom.find(function (x) { return x.data != null && x.data.userId == userId; });
    };
    SicboUserComp.prototype.getMyUser = function () {
        return this.myUserItem;
    };
    SicboUserComp.prototype.getOtherUserGroup = function () {
        return this.otherUserGroup;
    };
    SicboUserComp.prototype.playBetAnimation = function (user, slotNode) {
        user.shakeBet(slotNode);
    };
    SicboUserComp.prototype.updateUsersInRoom = function (displayPlayers, remainingPlayersCount) {
        this.remainingPlayersCount = Math.max(remainingPlayersCount, 0);
        this.otherUserGroup.node.active = remainingPlayersCount > 0;
        if (remainingPlayersCount > 999)
            this.remainingUsersLabel.string = SicboText_1.default.lotsOfUserText;
        else
            this.remainingUsersLabel.string = remainingPlayersCount.toString();
        if (this.isSameUserData(displayPlayers))
            return;
        if (this.isUserInRoomAnimPlaying)
            return;
        this.cacheDisplayPlayers = displayPlayers;
        this.playUserInRoomAnimation(displayPlayers);
    };
    SicboUserComp.prototype.playUserInRoomAnimation = function (displayPlayers) {
        var _this = this;
        var animDuration = SicboSetting_1.default.USER_ANIM_DURATION;
        var self = this;
        var animCount = 0;
        this.userCompAnimMap.forEach(function (targetIndex, userComp) {
            _this.userCompAnimMap.set(userComp, null);
        });
        var _loop_1 = function (i) {
            var userId = displayPlayers[i].getUserId();
            var userComp = this_1.usersInRoom.find(function (x) { return x.getUserId() == userId; });
            if (userComp != null) {
                //console.error("anim", userId, displayPlayers[i].getProfile().getDisplayName(), i)
                self.userCompAnimMap.set(userComp, i);
                if (this_1.isUseLinearAnim(userComp.getChatIndex(), i)) {
                    cc.tween(userComp.node).to(animDuration, { x: self.usersCachePos[i].x, y: self.usersCachePos[i].y }, { easing: "cubicInOut" }).start();
                }
                else {
                    var startPos = new cc.Vec2(userComp.node.x, userComp.node.position.y);
                    var endPos = new cc.Vec2(self.usersCachePos[i].x, self.usersCachePos[i].y);
                    cc.tween(userComp.node).bezierTo(animDuration, startPos, new cc.Vec2(0, -170), endPos).start();
                }
                userComp.setData(displayPlayers[i], i);
                animCount++;
            }
        };
        var this_1 = this;
        //console.error("-------------", displayPlayers.length)
        // for (let i = 0; i < displayPlayers.length; i++) {console.error(displayPlayers[i].getUserId(), displayPlayers[i].getProfile().getDisplayName())}
        //user da co - co slot moi
        for (var i = 0; i < displayPlayers.length; i++) {
            _loop_1(i);
        }
        var freeComps = [];
        this.userCompAnimMap.forEach(function (i, userComp) {
            if (i == null)
                freeComps.push(userComp);
        });
        //NO PLAYAH COMP
        for (var i = 0; i < freeComps.length; i++) {
            freeComps[i].setData();
        }
        cc.tween(this.node)
            .delay(animDuration / 2)
            .call(function () {
            var _loop_2 = function (i) {
                var userId = displayPlayers[i].getUserId();
                var userComp = self.usersInRoom.find(function (x) { return x.getUserId() == userId; });
                if (userComp == null) {
                    //console.error("set", userId, displayPlayers[i].getProfile().getDisplayName(), i)
                    userComp = freeComps.pop();
                    if (userComp != null) {
                        userComp.setData(displayPlayers[i], i);
                        cc.tween(userComp.node).to(0, { x: self.usersCachePos[i].x, y: self.usersCachePos[i].y }).start();
                    }
                }
            };
            //NEW Playah
            for (var i = 0; i < displayPlayers.length; i++) {
                _loop_2(i);
            }
        })
            .start();
        //ANIM DELAY
        if (animCount > 0) {
            self.isUserInRoomAnimPlaying = true;
            cc.tween(this.node)
                .delay(animDuration)
                .call(function () {
                self.isUserInRoomAnimPlaying = false;
            })
                .start();
        }
        else {
            self.isUserInRoomAnimPlaying = false;
        }
    };
    SicboUserComp.prototype.isSameUserData = function (displayPlayers) {
        return false;
        // if(this.cacheDisplayPlayers == null || this.cacheDisplayPlayers.length == 0) return false;
        // if(this.cacheDisplayPlayers.length != displayPlayers.length) return false;
        // for (let i = 0; i < displayPlayers.length; i++) {
        //     if(this.cacheDisplayPlayers[i].getUserId() != displayPlayers[i].getUserId()) return false;
        // }
        // return true;
    };
    SicboUserComp.prototype.isUseLinearAnim = function (myIndex, otherIndex) {
        var isSameSide = Math.floor(myIndex / 3) == Math.floor(otherIndex / 3);
        var isSameRow = myIndex % 3 == otherIndex % 3;
        return isSameSide || isSameRow;
    };
    __decorate([
        property(SicboUserItemComp_1.default)
    ], SicboUserComp.prototype, "myUserItem", void 0);
    __decorate([
        property(SicboUserItemComp_1.default)
    ], SicboUserComp.prototype, "otherUserGroup", void 0);
    __decorate([
        property(cc.Label)
    ], SicboUserComp.prototype, "remainingUsersLabel", void 0);
    __decorate([
        property(cc.Node)
    ], SicboUserComp.prototype, "usersInRoomNode", void 0);
    SicboUserComp = __decorate([
        ccclass
    ], SicboUserComp);
    return SicboUserComp;
}(cc.Component));
exports.default = SicboUserComp;

cc._RF.pop();