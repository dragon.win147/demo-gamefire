"use strict";
cc._RF.push(module, 'bf993nd8gRNVK5ma3UZkBY/', 'SicboBaseDropDownOptionDataComp');
// Game/Sicbo_New/Scripts/Components/DropDown/SicboBaseDropDownOptionDataComp.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboBaseDropDownOptionDataComp = /** @class */ (function () {
    function SicboBaseDropDownOptionDataComp(obj) {
        this.optionString = "";
        this.optionSf = null;
        this.clickFunc = null;
        var optionString = obj.optionString, optionSf = obj.optionSf, clickFunc = obj.clickFunc;
        this.optionString = optionString;
        this.optionSf = optionSf;
        this.clickFunc = clickFunc;
    }
    return SicboBaseDropDownOptionDataComp;
}());
exports.default = SicboBaseDropDownOptionDataComp;

cc._RF.pop();