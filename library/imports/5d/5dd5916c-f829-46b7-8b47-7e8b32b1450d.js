"use strict";
cc._RF.push(module, '5dd59Fs+ClGt4tHfosysUUN', 'SicboInforPopup');
// Game/Sicbo_New/Scripts/PopUps/SicboInforPopup.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboPopUpInstance_1 = require("../Managers/SicboPopUpInstance");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboInforPopup = /** @class */ (function (_super) {
    __extends(SicboInforPopup, _super);
    function SicboInforPopup() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scrollview = null;
        _this.infoSprite = null;
        return _this;
    }
    SicboInforPopup.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
    };
    SicboInforPopup.prototype.onShow = function () {
        // this.scrollview.scrollToTop(0);
        // this.infoSprite.spriteFrame = null;
        // let infoResources = SicboController.Instance.getGameInfo()
        // if(infoResources == null) return
        // infoResources.forEach((info) => {
        //   SicboResourceManager.loadRemoteImageWithFileField(this.infoSprite, info);
        // });
    };
    SicboInforPopup.prototype.onButtonClose = function () {
        this.close();
    };
    __decorate([
        property(cc.ScrollView)
    ], SicboInforPopup.prototype, "scrollview", void 0);
    __decorate([
        property(cc.Sprite)
    ], SicboInforPopup.prototype, "infoSprite", void 0);
    SicboInforPopup = __decorate([
        ccclass
    ], SicboInforPopup);
    return SicboInforPopup;
}(SicboPopUpInstance_1.default));
exports.default = SicboInforPopup;

cc._RF.pop();