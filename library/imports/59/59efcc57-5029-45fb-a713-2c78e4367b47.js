"use strict";
cc._RF.push(module, '59efcxXUClF+6cTLHjkNntH', 'NetworkManager');
// Game/Lobby/Manager/NetworkManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var PortalText_1 = require("../Utils/PortalText");
var GameDefine_1 = require("../Define/GameDefine");
var EnumGame_1 = require("../Utils/EnumGame");
var BenTauHandler_1 = require("../Network/BenTauHandler");
var IdentityHandler_1 = require("../Network/IdentityHandler");
var WalletHandler_1 = require("../Network/WalletHandler");
var LayerHelper_1 = require("../Utils/LayerHelper");
var GameUtils_1 = require("../Utils/GameUtils");
var EventBusManager_1 = require("../Event/EventBusManager");
var EventManager_1 = require("../Event/EventManager");
var UIManager_1 = require("./UIManager");
var TextNotify_1 = require("../Manager/TextNotify");
var TextNotifyDisconnect_1 = require("../Manager/TextNotifyDisconnect");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var RetryPopupSystem_1 = require("../Popup/RetryPopupSystem");
var WarningPopupSystem_1 = require("../Popup/WarningPopupSystem");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var NetworkManager = /** @class */ (function (_super) {
    __extends(NetworkManager, _super);
    function NetworkManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.textNotifyDisconnect = null;
        _this.retryPopupSystem = null;
        return _this;
    }
    NetworkManager.prototype.onLoad = function () {
        cc.game.addPersistRootNode(this.node);
        this.node.zIndex = LayerHelper_1.default.netWorkManager;
        var resourceLocalPaths = [
        // chỉ thêm chứ không xóa nha
        ];
        cc.resources.preload(resourceLocalPaths, cc.Prefab, function (err, prefab) {
            if (err) {
                return;
            }
        });
    };
    NetworkManager.prototype.start = function () {
        var _this = this;
        EventManager_1.default.on(EventManager_1.default.ON_DISCONNECT, function (code, isRetry, messError) {
            if (code === void 0) { code = 0; }
            if (isRetry === void 0) { isRetry = false; }
            cc.log("ON_DISCONNECT code = " + code + " isRetry = " + isRetry);
            // if (UserManager.getInstance().isLogin) {
            //   this.backToPortal(code, isRetry, messError);
            // }
            UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, messError);
        }, this);
        EventManager_1.default.on(EventManager_1.default.ON_PING_FAIL, function () {
            cc.log("Ping Fail");
            var duration = 3;
            var textData = new TextNotify_1.TextNotifyData(PortalText_1.default.POR_NOTIFY_PING_FAIL, duration);
            if (_this.textNotifyDisconnect) {
                _this.textNotifyDisconnect.node.active = true;
                _this.textNotifyDisconnect.onShow(textData);
            }
            _this.scheduleOnce(function () {
                if (BenTauHandler_1.default.getInstance().client) {
                    UIManager_1.default.getInstance()
                        .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, true, null, null)
                        .then(function () {
                        var _a;
                        cc.log("onClose ON_PING_FAIL ");
                        // BenTauHandler.getInstance().client?.close(CodeSocket.RETRY, "PING FAIL");
                        (_a = BenTauHandler_1.default.getInstance().client) === null || _a === void 0 ? void 0 : _a.reConnect(EnumGame_1.CodeSocket.RETRY, "PING FAIL");
                        EventBusManager_1.default.onPingError();
                    });
                }
            }, duration);
        }, this);
        EventManager_1.default.on(EventManager_1.default.ON_PING_OK, function () {
            cc.log("Ping OK");
        }, this);
        // cc.game.on(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
        // cc.game.on(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
        EventManager_1.default.on(EventManager_1.default.ON_RECONNECTED, function () {
            WalletHandler_1.default.getInstance().onMyWalletRequest(function (response) { });
        }, this);
        EventManager_1.default.on(EventManager_1.default.ON_SHOW_RETRY_POPUP, function () {
            cc.log("this.textNotifyDisconnect " + _this.textNotifyDisconnect);
            if (_this.textNotifyDisconnect) {
                _this.textNotifyDisconnect.node.active = false;
            }
            if (_this.retryPopupSystem) {
                _this.retryPopupSystem.node.active = true;
                _this.retryPopupSystem.onShow();
            }
        }, this);
        if (!GameUtils_1.default.isStatusPing) {
            EventManager_1.default.fire(EventManager_1.default.ON_PING_FAIL);
        }
    };
    // protected onChangeTabHide() {
    //   cc.log("ON_HIDE_GAME ");
    //   EventManager.fire(EventManager.ON_HIDE_GAME);
    // }
    // protected onChangeTabShow() {
    //   cc.log("ON_SHOW_GAME");
    //   EventManager.fire(EventManager.ON_SHOW_GAME);
    //   if (GameUtils.isNative() && UserManager.getInstance().isLogin) {
    //     WalletHandler.getInstance().onMyWalletRequest((response) => {
    //       //EventManager.fire(EventManager.onUpdateBalance, response.getWallet().getCurrentBalance());
    //     });
    //   }
    // }
    /**
     * Ví dụ cách sử dụng để lấy bến tàu và token
     *let networkHandler = cc.director.getScene().getChildByName("NetworkManager");
      let bentauHandle = networkHandler.getComponent("NetworkManager").getBenTauHandler();
      SicboMessageHandler.benTauHandler = bentauHandle;
     * @returns
     */
    NetworkManager.prototype.getBenTauHandler = function () {
        return BenTauHandler_1.default.getInstance();
    };
    NetworkManager.prototype.getToken = function () {
        return IdentityHandler_1.default.getInstance().token;
    };
    NetworkManager.prototype.onDestroy = function () {
        // cc.game.off(cc.game.EVENT_HIDE, this.onChangeTabHide, this);
        // cc.game.off(cc.game.EVENT_SHOW, this.onChangeTabShow, this);
    };
    __decorate([
        property(TextNotifyDisconnect_1.default)
    ], NetworkManager.prototype, "textNotifyDisconnect", void 0);
    __decorate([
        property(RetryPopupSystem_1.default)
    ], NetworkManager.prototype, "retryPopupSystem", void 0);
    NetworkManager = __decorate([
        ccclass
    ], NetworkManager);
    return NetworkManager;
}(cc.Component));
exports.default = NetworkManager;

cc._RF.pop();