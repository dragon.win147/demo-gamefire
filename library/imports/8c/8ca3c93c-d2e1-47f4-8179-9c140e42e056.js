"use strict";
cc._RF.push(module, '8ca3ck80uFH9IF5nBQOQuBW', 'SicboNative');
// Game/Sicbo_New/Scripts/SicboNative.ts

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboNative = /** @class */ (function () {
    function SicboNative() {
        this.DEFAULT_PACKAGE_NAME = "org/cocos2dx/javascript/";
        SicboNative_1.instance = this;
    }
    SicboNative_1 = SicboNative;
    SicboNative.getInstance = function () {
        if (SicboNative_1.instance == null) {
            SicboNative_1.instance = new SicboNative_1();
        }
        return SicboNative_1.instance;
    };
    /**
     * call jsb with class name is default on native
     * @param arg
     */
    SicboNative.prototype.call = function () {
        var arg = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            arg[_i] = arguments[_i];
        }
        var args = Array.prototype.slice.call(arguments);
        if (cc.sys.os == cc.sys.OS_ANDROID) {
            args.splice(0, 0, "AndroidNativeUtils");
        }
        else if (cc.sys.os == cc.sys.OS_IOS) {
            args.splice(0, 0, "IOSNativeUtils");
        }
        else {
            return;
        }
        this.callClz.apply(this, args);
    };
    /**
     * call jsb with class name is clz
     * @param clz
     * @param funcName
     * @param arg
     */
    SicboNative.prototype.callClz = function (clz, funcName) {
        var arg = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            arg[_i - 2] = arguments[_i];
        }
        //console.log("callClz clz:", clz);
        var args = Array.prototype.slice.call(arguments);
        args.splice(2, 0, "V");
        return this._callClz.apply(this, args);
    };
    SicboNative.prototype._callClz = function (clz, funcName, returnType) {
        //console.log("_callClz clz:", clz);
        var args = Array.prototype.slice.call(arguments);
        args.unshift(this.DEFAULT_PACKAGE_NAME);
        return this.callClzWithPackage.apply(this, args);
    };
    SicboNative.prototype.callClzWithPackage = function (pkg, clz, funcName, returnType) {
        var args = Array.prototype.slice.call(arguments);
        args.splice(0, 4);
        var real_args = [clz, funcName];
        //console.log("clz:", clz);
        //console.log("funcName:", funcName);
        if (cc.sys.os == cc.sys.OS_ANDROID) {
            real_args[0] = pkg + clz;
            real_args[2] = "()" + returnType;
            if (args.length > 0) {
                var sig_1 = "";
                args.forEach(function (v) {
                    switch (typeof v) {
                        case "boolean":
                            sig_1 += "Z";
                            real_args.push(v);
                            break;
                        case "string":
                            sig_1 += "Ljava/lang/String;";
                            real_args.push(v);
                            break;
                        case "number":
                            sig_1 += "D";
                            real_args.push(v);
                            break;
                        case "function":
                            sig_1 += "Ljava/lang/String;";
                            //   real_args.push(this._newCB(v));
                            real_args.push(v);
                            break;
                    }
                });
                real_args[2] = "(" + sig_1 + ")" + returnType;
            }
        }
        else if (cc.sys.os == cc.sys.OS_IOS) {
            if (args.length > 0) {
                for (var i = 0; i < args.length; i++) {
                    var v = args[i];
                    //   if (typeof v == "function") {
                    //     real_args.push(this._newCB(v));
                    //   } else {
                    real_args.push(v);
                    //   }
                    if (i == 0) {
                        funcName += ":";
                    }
                    else {
                        funcName += "arg" + i + ":";
                    }
                }
                real_args[1] = funcName;
            }
        }
        else {
            return;
        }
        //console.log("real_args:", real_args);
        return jsb.reflection.callStaticMethod.apply(jsb.reflection, real_args);
    };
    SicboNative.prototype.getStr = function (clz, funcName) {
        var args = Array.prototype.slice.call(arguments);
        args.splice(2, 0, "Ljava/lang/String;");
        return this._callClz.apply(this, args);
    };
    var SicboNative_1;
    SicboNative = SicboNative_1 = __decorate([
        ccclass
    ], SicboNative);
    return SicboNative;
}());
exports.default = SicboNative;

cc._RF.pop();