"use strict";
cc._RF.push(module, '6f739r/FmxB66CjMAWuN27L', 'SicboInfiniteScrollViewComp');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboInfiniteScrollViewComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboInfiniteScrollViewComp = /** @class */ (function (_super) {
    __extends(SicboInfiniteScrollViewComp, _super);
    function SicboInfiniteScrollViewComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scrollView = null;
        _this.view = null;
        _this.content = null;
        _this.itemSpacing = 100;
        _this.threshold = 200;
        _this.visibleStartY = 0;
        _this.maxItemCanbeLoaded = 0;
        _this.totalItem = 0;
        _this.lastItemIndexes = [];
        _this.nodeArray = [];
        _this.interfaceInstance = null;
        return _this;
    }
    SicboInfiniteScrollViewComp.prototype.setNumberOfItem = function (num, firstCreated) {
        if (firstCreated === void 0) { firstCreated = false; }
        this.content.height = this.itemSpacing * num;
        this.totalItem = num;
        this.clearAll();
        this.nodeArray = new Array(this.totalItem);
        if (!firstCreated) {
            this.checkAutoScrollToEnd();
            // this.checkScrollViewItem();
        }
    };
    SicboInfiniteScrollViewComp.prototype.init = function (interfaceInstance, totalItem) {
        if (interfaceInstance === void 0) { interfaceInstance = null; }
        var scrollViewEventHandler = new cc.Component.EventHandler();
        scrollViewEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        scrollViewEventHandler.component = "SicboInfiniteScrollViewComp"; // This is the code file name
        scrollViewEventHandler.handler = "callback";
        // scrollViewEventHandler.customEventData = "";        
        this.scrollView.scrollEvents.push(scrollViewEventHandler);
        this.visibleStartY = this.content.y;
        this.maxItemCanbeLoaded = Math.ceil((this.view.height + this.threshold * 2) / this.itemSpacing);
        this.interfaceInstance = interfaceInstance;
        this.setNumberOfItem(totalItem, true);
        this.checkScrollViewItem();
    };
    SicboInfiniteScrollViewComp.prototype.firstLoad = function () {
        var start = 0;
        var end = Math.min(start + this.maxItemCanbeLoaded, this.totalItem - 1);
        var arr = Array.from({ length: end + 1 - start }, function (_, i) { return i + start; });
        this.lastItemIndexes = arr;
        for (var n = 0; n < this.lastItemIndexes.length; n++) {
            var i = this.lastItemIndexes[n];
            if (this.nodeArray[i] == null && this.interfaceInstance) {
                var node = this.interfaceInstance.createItem(i);
                this.setItemPosInScrollView(node, i);
                this.nodeArray[i] = node;
            }
        }
    };
    SicboInfiniteScrollViewComp.prototype.callback = function (scrollView, eventType, customEventData) {
        switch (eventType) { //baka mitai
            case cc.ScrollView.EventType.SCROLLING:
                this.checkScrollViewItem();
                ////cc.log("zzzz", this.content.childrenCount);
                break;
            case cc.ScrollView.EventType.SCROLL_TO_BOTTOM:
            case cc.ScrollView.EventType.SCROLL_TO_TOP:
                this.checkScrollViewItem();
                break;
        }
    };
    SicboInfiniteScrollViewComp.prototype.checkScrollViewItem = function (isRefresh) {
        if (isRefresh === void 0) { isRefresh = false; }
        if (this.interfaceInstance == null)
            return;
        var newItemIndexes = this.getItemsToLoad();
        //delete node not in newItemIndexes
        var deleteIndexes = this.lastItemIndexes.filter(function (o1) { return !newItemIndexes.some(function (o2) { return o2 === o1; }); }); //find element in arr1 not in arr 2
        for (var d = 0; d < deleteIndexes.length; d++) {
            var i = deleteIndexes[d];
            if (this.nodeArray[i] != null)
                this.interfaceInstance.destroyItem(this.nodeArray[i]);
            this.nodeArray[i] = null;
        }
        this.lastItemIndexes = newItemIndexes;
        for (var n = 0; n < newItemIndexes.length; n++) {
            var i = newItemIndexes[n];
            if (this.nodeArray[i] == null) {
                var node = this.interfaceInstance.createItem(i);
                this.setItemPosInScrollView(node, i);
                this.nodeArray[i] = node;
            }
            else {
                if (isRefresh) {
                    this.interfaceInstance.refreshItem(this.nodeArray[i], i);
                }
            }
        }
    };
    // refresh(){
    //     for (let n = 0; n < this.lastItemIndexes.length; n++) {
    //         let i = this.lastItemIndexes[n];
    //         if(this.nodeArray[i] !=null && this.interfaceInstance){
    //             this.interfaceInstance.refreshItem(this.nodeArray[i], i);
    //         }
    //     }
    // }
    SicboInfiniteScrollViewComp.prototype.checkAutoScrollToEnd = function () {
        if (this.totalItem <= this.maxItemCanbeLoaded) {
            this.scrollView.scrollToTop(0.5);
            return;
        }
        var needScroll = false;
        var y = this.content.y - this.content.height;
        var yBot = this.visibleStartY - this.view.height;
        needScroll = y > yBot;
        if (needScroll)
            this.scrollView.scrollToBottom(0.5);
        else {
            this.checkScrollViewItem();
        }
    };
    SicboInfiniteScrollViewComp.prototype.setItemPosInScrollView = function (item, index) {
        item.y = -index * this.itemSpacing;
    };
    SicboInfiniteScrollViewComp.prototype.getItemsToLoad = function () {
        var currentY = this.content.y;
        var start = (currentY - this.threshold - this.visibleStartY) / this.itemSpacing;
        start = Math.floor(Math.max(0, start));
        var end = Math.min(start + this.maxItemCanbeLoaded, this.totalItem - 1);
        var arr = Array.from({ length: end + 1 - start }, function (_, i) { return i + start; });
        return arr;
    };
    SicboInfiniteScrollViewComp.prototype.clearAll = function () {
        for (var i = 0; i < this.nodeArray.length; i++) {
            if (this.nodeArray[i] != null && this.interfaceInstance.destroyItem)
                this.interfaceInstance.destroyItem(this.nodeArray[i]);
            this.nodeArray[i] = null;
        }
    };
    __decorate([
        property(cc.ScrollView)
    ], SicboInfiniteScrollViewComp.prototype, "scrollView", void 0);
    __decorate([
        property(cc.Node)
    ], SicboInfiniteScrollViewComp.prototype, "view", void 0);
    __decorate([
        property(cc.Node)
    ], SicboInfiniteScrollViewComp.prototype, "content", void 0);
    __decorate([
        property()
    ], SicboInfiniteScrollViewComp.prototype, "itemSpacing", void 0);
    __decorate([
        property()
    ], SicboInfiniteScrollViewComp.prototype, "threshold", void 0);
    SicboInfiniteScrollViewComp = __decorate([
        ccclass
    ], SicboInfiniteScrollViewComp);
    return SicboInfiniteScrollViewComp;
}(cc.Component));
exports.default = SicboInfiniteScrollViewComp;

cc._RF.pop();