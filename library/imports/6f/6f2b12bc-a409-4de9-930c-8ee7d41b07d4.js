"use strict";
cc._RF.push(module, '6f2b1K8pAlN6ZMMjufUGwfU', 'SicboCoinItemComp');
// Game/Sicbo_New/Scripts/Components/Items/SicboCoinItemComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboCoinItemCompData = void 0;
var SicboChipSpriteConfig_1 = require("../../Configs/SicboChipSpriteConfig");
var SicboHelper_1 = require("../../Helpers/SicboHelper");
var SicboCoinType_1 = require("../../RNGCommons/SicboCoinType");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboCoinItemCompData = /** @class */ (function () {
    function SicboCoinItemCompData() {
        this.coinType = SicboCoinType_1.SicboCoinType.Coin100;
        this.onClick = null;
        this.isMini = false;
    }
    return SicboCoinItemCompData;
}());
exports.SicboCoinItemCompData = SicboCoinItemCompData;
var SicboCoinItemComp = /** @class */ (function (_super) {
    __extends(SicboCoinItemComp, _super);
    function SicboCoinItemComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.icon = null;
        _this.iconDisable = null;
        _this.showEffect = null;
        _this.data = null;
        return _this;
    }
    //   set isOnTable(value: boolean) {    //
    //     this.node.setScale(value? new cc.Vec2(.25, .22): cc.Vec2.ONE);
    //   }
    SicboCoinItemComp.prototype.onLoad = function () {
        // var interval = 5;
        // // Time of repetition
        // var repeat = 4;
        // // Start delay
        // var delay = 2;
        // let self = this;
        // let isDisable = false;
        // self.isDisable = isDisable;
        // this.schedule(function() {
        //     // Here `this` is referring to the component
        //     isDisable = !isDisable;
        //     self.isDisable = isDisable;        
        //     //cc.log("DKM", isDisable);
        // }, interval, repeat, delay);
    };
    Object.defineProperty(SicboCoinItemComp.prototype, "isDisable", {
        set: function (disable) {
            this.icon.node.active = !disable;
            this.iconDisable.node.active = disable;
            if (disable) {
                this.showEffect.active = false;
                this.node.scale = 1;
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboCoinItemComp.prototype, "rotation", {
        set: function (value) {
            this.icon.node.angle = -value;
        },
        enumerable: false,
        configurable: true
    });
    SicboCoinItemComp.prototype.setData = function (data) {
        this.data = data;
        var spriteName = SicboHelper_1.default.convertCoinTypeToSpriteName(data.coinType, false, this.data.isMini);
        var spriteNameDisable = SicboHelper_1.default.convertCoinTypeToSpriteName(data.coinType, true);
        var self = this;
        var spriteFrame = null;
        if (self.data.isMini)
            spriteFrame = SicboChipSpriteConfig_1.default.Instance.getMiniChipSprite(spriteName);
        else
            spriteFrame = SicboChipSpriteConfig_1.default.Instance.getNormalChipSprite(spriteName);
        self.icon.spriteFrame = spriteFrame;
        if (this.data.isMini) {
            var button = this.getComponent(cc.Button);
            button.enabled = false;
            this.iconDisable.node.active = false;
        }
        else {
            self.iconDisable.spriteFrame = SicboChipSpriteConfig_1.default.Instance.getDisableChipSprite(spriteNameDisable);
            this.iconDisable.node.active = true;
        }
    };
    SicboCoinItemComp.prototype.onClickItem = function () {
        if (this.data.onClick) {
            this.data.onClick(this);
        }
    };
    SicboCoinItemComp.prototype.selectEff = function (isSelect) {
        cc.tween(this.node).to(.05, {
            // y: isSelect?25:0, 
            scale: isSelect ? 1.1 : 1
        })
            .start();
        this.showEffect.active = isSelect;
    };
    SicboCoinItemComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.Sprite)
    ], SicboCoinItemComp.prototype, "icon", void 0);
    __decorate([
        property(cc.Sprite)
    ], SicboCoinItemComp.prototype, "iconDisable", void 0);
    __decorate([
        property(cc.Node)
    ], SicboCoinItemComp.prototype, "showEffect", void 0);
    SicboCoinItemComp = __decorate([
        ccclass
    ], SicboCoinItemComp);
    return SicboCoinItemComp;
}(cc.Component));
exports.default = SicboCoinItemComp;

cc._RF.pop();