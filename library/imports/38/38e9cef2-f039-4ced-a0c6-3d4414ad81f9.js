"use strict";
cc._RF.push(module, '38e9c7y8DlM7aDGPUQUrYH5', 'SicboLoadingBackgroundAnimation');
// Game/Sicbo_New/Scripts/Components/SicboLoadingBackgroundAnimation.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboLoadingBackgroundAnimation = /** @class */ (function (_super) {
    __extends(SicboLoadingBackgroundAnimation, _super);
    function SicboLoadingBackgroundAnimation() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.OnBeginLoadResources = null;
        _this.OnCompleteLoadResources = null;
        return _this;
    }
    SicboLoadingBackgroundAnimation.prototype.start = function () {
        this.OnBeginLoadResources && this.OnBeginLoadResources();
    };
    SicboLoadingBackgroundAnimation.prototype.playFinishAnim = function (onPlayCompleted) {
        onPlayCompleted && onPlayCompleted();
    };
    SicboLoadingBackgroundAnimation.prototype.onSetProgress = function (fill) { };
    SicboLoadingBackgroundAnimation = __decorate([
        ccclass
    ], SicboLoadingBackgroundAnimation);
    return SicboLoadingBackgroundAnimation;
}(cc.Component));
exports.default = SicboLoadingBackgroundAnimation;

cc._RF.pop();