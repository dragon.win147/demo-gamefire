"use strict";
cc._RF.push(module, '50345ucc0NOfoRgTEOcmgYI', 'SicboText');
// Game/Sicbo_New/Scripts/Setting/SicboText.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var Code = SicboModuleAdapter_1.default.getAllRefs().Code;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboText = /** @class */ (function () {
    function SicboText() {
    }
    Object.defineProperty(SicboText, "handlerDefaultErrMsg", {
        get: function () {
            return "Có lỗi xảy ra xin vui lòng thử lại!";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "sessionCopyMessage", {
        get: function () {
            return "Sao Chép Thành Công Mã MD5";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "loadingMsg", {
        get: function () {
            return "Loading... {0}%";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "lotsOfUserText", {
        get: function () {
            return "999+";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "sessionIdText", {
        get: function () {
            return "#{0}";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "leaderboardUpdateTime", {
        get: function () {
            return "{0} phút";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "noLotteryValue", {
        get: function () {
            return "-";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "sessionDataBetHistoryDetail", {
        get: function () {
            return "#{0} ({1})";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "sessionLastFormat", {
        get: function () {
            return "Phiên gần đây nhất (#{0}) -";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "valueLastFormat", {
        get: function () {
            return "{0} {1}: ({2}-{3}-{4})";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "getCannotJoinMessageWhenNotIngame", {
        get: function () {
            return "Game Đang Tiến Hành Nâng Cấp Hệ Thống.\nQuý khách vui lòng quay lại sau!";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "getCannotJoinMessageWhenIngame", {
        get: function () {
            return "Xin lỗi quý khách, hiện tại hệ thống đang gặp sự cố.\nChúng tôi sẽ cố gắng khắc phục trong thời gian sớm nhất.\nChân thành xin lỗi vì sự bất tiện này.";
        },
        enumerable: false,
        configurable: true
    });
    SicboText.getErrorMessage = function (errorCode) {
        switch (errorCode) {
            case Code.SICBO_BET_LIMIT_REACHED:
                // return "Quý Khách Đã Đặt Mức Cược Tối Đa Cho Tụ Này";
                return "Số Tiền Đặt Cược Không Thể Vượt Mức Quy Định Của Tụ!";
            case Code.SICBO_BET_NOT_ENOUGH_MONEY:
                return "Số Dư Của Quý Khách Không Đủ Để Đặt Cược";
            default:
                return null;
        }
    };
    Object.defineProperty(SicboText, "waitingRemainingTimeMsg", {
        get: function () {
            return "Ván mới bắt đầu sau {0}s";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "kickMsg", {
        get: function () {
            return "Xin Mời Quý Khách Đặt Cược\nĐể Tiếp Tục Tham Gia Bàn Chơi.";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "leaveBookingActiveMsg", {
        get: function () {
            return "Quý Khách sẽ rời phòng sau khi ván chơi kết thúc.";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SicboText, "leaveBookingDeactiveMsg", {
        get: function () {
            return "Quý Khách sẽ tiếp tục chơi.";
        },
        enumerable: false,
        configurable: true
    });
    SicboText = __decorate([
        ccclass
    ], SicboText);
    return SicboText;
}());
exports.default = SicboText;

cc._RF.pop();