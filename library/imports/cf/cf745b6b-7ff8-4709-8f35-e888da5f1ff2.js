"use strict";
cc._RF.push(module, 'cf745trf/hHCY816IjaXx/y', 'SicboAvatarWinMoneyComp');
// Game/Sicbo_New/Scripts/Components/SicboAvatarWinMoneyComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboMoneyFormatComp_1 = require("../RNGCommons/SicboMoneyFormatComp");
var SicboSkeletonUtils_1 = require("../RNGCommons/Utils/SicboSkeletonUtils");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboAvatarWinMoneyComp = /** @class */ (function (_super) {
    __extends(SicboAvatarWinMoneyComp, _super);
    function SicboAvatarWinMoneyComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.animationBack = null;
        _this.animationFront = null;
        _this.money = null;
        _this.backAnimation = "win_avata";
        _this.frontAnimation = "win_avata2";
        _this.showTime = 1;
        _this.outTime = .5;
        _this.isShowing = false;
        return _this;
    }
    SicboAvatarWinMoneyComp.prototype.onLoad = function () {
        this.animationBack.node.active = false;
        this.animationFront.node.active = false;
        this.isShowing = false;
    };
    SicboAvatarWinMoneyComp.prototype.show = function (money) {
        if (this.isShowing)
            return;
        this.isShowing = true;
        this.animationBack.node.active = true;
        this.animationFront.node.active = true;
        this.money.setMoney(money, true);
        SicboSkeletonUtils_1.default.setAnimation(this.animationBack, this.backAnimation, false);
        SicboSkeletonUtils_1.default.setAnimation(this.animationFront, this.frontAnimation, false);
        this.money.node.opacity = 255;
        var self = this;
        cc.tween(self.money.node)
            .delay(self.showTime)
            .to(self.outTime, { opacity: 0 })
            .call(function () { return self.isShowing = false; })
            .start();
    };
    SicboAvatarWinMoneyComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.money.node);
    };
    __decorate([
        property(sp.Skeleton)
    ], SicboAvatarWinMoneyComp.prototype, "animationBack", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboAvatarWinMoneyComp.prototype, "animationFront", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboAvatarWinMoneyComp.prototype, "money", void 0);
    SicboAvatarWinMoneyComp = __decorate([
        ccclass
    ], SicboAvatarWinMoneyComp);
    return SicboAvatarWinMoneyComp;
}(cc.Component));
exports.default = SicboAvatarWinMoneyComp;

cc._RF.pop();