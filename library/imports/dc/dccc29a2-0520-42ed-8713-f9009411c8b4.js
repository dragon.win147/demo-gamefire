"use strict";
cc._RF.push(module, 'dccc2miBSBC7YcT+QCUEci0', 'LayerHelper');
// Portal/Common/Scripts/Helpers/LayerHelper.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LayerHelper = /** @class */ (function () {
    function LayerHelper() {
    }
    LayerHelper.miniGameManager = 1;
    LayerHelper.uiManager = 2;
    LayerHelper.cheatManager = 3;
    LayerHelper.netWorkManager = 4;
    return LayerHelper;
}());
exports.default = LayerHelper;

cc._RF.pop();