"use strict";
cc._RF.push(module, 'c7a3f7TS39LboAh+JP12jnl', 'SicboReplayAnimationOnActiveComp');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboReplayAnimationOnActiveComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboSkeletonUtils_1 = require("../Utils/SicboSkeletonUtils");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboReplayAnimationOnActiveComp = /** @class */ (function (_super) {
    __extends(SicboReplayAnimationOnActiveComp, _super);
    function SicboReplayAnimationOnActiveComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.skeleton = null;
        _this.animation = null;
        _this.animationName = "";
        _this.loop = true;
        return _this;
    }
    SicboReplayAnimationOnActiveComp.prototype.onEnable = function () {
        if (this.skeleton != null)
            SicboSkeletonUtils_1.default.setAnimation(this.skeleton, this.animationName, this.loop);
        if (this.animation != null)
            this.animation.play(this.animationName, 0);
    };
    __decorate([
        property(sp.Skeleton)
    ], SicboReplayAnimationOnActiveComp.prototype, "skeleton", void 0);
    __decorate([
        property(cc.Animation)
    ], SicboReplayAnimationOnActiveComp.prototype, "animation", void 0);
    __decorate([
        property
    ], SicboReplayAnimationOnActiveComp.prototype, "animationName", void 0);
    __decorate([
        property
    ], SicboReplayAnimationOnActiveComp.prototype, "loop", void 0);
    SicboReplayAnimationOnActiveComp = __decorate([
        ccclass
    ], SicboReplayAnimationOnActiveComp);
    return SicboReplayAnimationOnActiveComp;
}(cc.Component));
exports.default = SicboReplayAnimationOnActiveComp;

cc._RF.pop();