"use strict";
cc._RF.push(module, 'c43164Pn2tJXJvVH3wPdnJm', 'SicboSoundManager');
// Game/Sicbo_New/Scripts/Managers/SicboSoundManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var SicboModuleAdapter_1 = require("../..../../../../../SicboModuleAdapter");
var EventKey = SicboModuleAdapter_1.default.getAllRefs().EventKey;
var SicboLocalStorageManager_1 = require("./SicboLocalStorageManager");
var SicboSoundItem_1 = require("./SicboSoundItem");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboSoundManager = /** @class */ (function (_super) {
    __extends(SicboSoundManager, _super);
    function SicboSoundManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.soundMap = new Map();
        _this.KEY_EFFECT_VOLUME = EventKey.KEY_EFFECT_VOLUME;
        _this.KEY_EFFECT = EventKey.KEY_EFFECT;
        _this.KEY_MUSIC_VOLUME = EventKey.KEY_MUSIC_VOLUME;
        _this.KEY_MUSIC = EventKey.KEY_MUSIC;
        _this.isEnableMusic = true;
        _this.isEnableSfx = true;
        _this.musicVolume = 1;
        _this.sfxVolume = 1;
        return _this;
    }
    SicboSoundManager_1 = SicboSoundManager;
    SicboSoundManager.getInstance = function () {
        if (SicboSoundManager_1.instance == null) {
            var canvas = cc.director.getScene().getChildByName("Canvas");
            var node = canvas.getChildByName("SicboSoundManager");
            SicboSoundManager_1.instance = node.getComponent(SicboSoundManager_1);
        }
        return SicboSoundManager_1.instance;
    };
    SicboSoundManager.prototype.onLoad = function () {
        this.init();
        this.loadConfigFromFromStorage();
    };
    SicboSoundManager.prototype.init = function () {
        var sounds = this.node.getComponentsInChildren(SicboSoundItem_1.default);
        for (var i = 0; i < sounds.length; i++) {
            this.soundMap.set(sounds[i].node.name, sounds[i]);
        }
    };
    SicboSoundManager.prototype.play = function (soundName, from, autoStopDelay) {
        if (from === void 0) { from = 0; }
        if (autoStopDelay === void 0) { autoStopDelay = -1; }
        if (this.soundMap.has(soundName)) {
            this.soundMap.get(soundName).play(from, autoStopDelay);
        }
    };
    SicboSoundManager.prototype.stop = function (soundName) {
        if (this.soundMap.has(soundName)) {
            this.soundMap.get(soundName).stop();
        }
    };
    SicboSoundManager.prototype.loadConfigFromFromStorage = function () {
        this.isEnableMusic = this.getMusicStatusFromStorage();
        this.isEnableSfx = this.getSfxStatusFromStorage();
        this.musicVolume = this.getMusicVolumeFromStorage();
        this.sfxVolume = this.getSfxVolumeFromStorage();
        this.setMusicVolume(this.musicVolume);
        this.setSfxVolume(this.sfxVolume);
    };
    SicboSoundManager.prototype.setMusicStatus = function (isOn) {
        this.isEnableMusic = isOn;
        cc.audioEngine.setMusicVolume(this.MusicVolume);
        this.saveMusicStatusToStorage(isOn);
        cc.systemEvent.emit(EventKey.ON_SOUND_BG_EVENT, isOn);
    };
    SicboSoundManager.prototype.setMusicVolume = function (volume, isSave) {
        if (isSave === void 0) { isSave = true; }
        this.musicVolume = volume;
        cc.audioEngine.setMusicVolume(volume);
        if (!isSave)
            return;
        this.saveMusicVolumeToStorage(volume);
        var isOn = volume > 0;
        this.saveMusicStatusToStorage(isOn);
        cc.systemEvent.emit(EventKey.ON_SOUND_BG_EVENT, isOn);
    };
    Object.defineProperty(SicboSoundManager.prototype, "MusicVolume", {
        get: function () {
            return this.isEnableMusic ? this.musicVolume : 0;
        },
        enumerable: false,
        configurable: true
    });
    SicboSoundManager.prototype.setSfxVolume = function (volume, isSave) {
        if (isSave === void 0) { isSave = true; }
        this.sfxVolume = volume;
        cc.audioEngine.setEffectsVolume(volume);
        if (!isSave)
            return;
        this.saveSfxVolumeToStorage(volume);
        var isOn = this.sfxVolume > 0;
        this.saveSfxStatusToStorage(isOn);
        cc.systemEvent.emit(EventKey.ON_SOUND_EFF_EVENT, isOn);
    };
    SicboSoundManager.prototype.setSfxStatus = function (isOn) {
        this.isEnableSfx = isOn;
        cc.audioEngine.setEffectsVolume(this.SfxVolume);
        this.saveSfxStatusToStorage(isOn);
        cc.systemEvent.emit(EventKey.ON_SOUND_EFF_EVENT, isOn);
    };
    Object.defineProperty(SicboSoundManager.prototype, "SfxVolume", {
        get: function () {
            return this.isEnableSfx ? this.sfxVolume : 0;
        },
        enumerable: false,
        configurable: true
    });
    //SECTION Storage
    SicboSoundManager.prototype.getMusicStatusFromStorage = function () {
        return SicboLocalStorageManager_1.default.internalGetBoolean(this.KEY_MUSIC, true);
    };
    SicboSoundManager.prototype.saveMusicStatusToStorage = function (isOn) {
        SicboLocalStorageManager_1.default.internalSaveBoolean(this.KEY_MUSIC, isOn);
    };
    SicboSoundManager.prototype.getMusicVolumeFromStorage = function () {
        return Number.parseFloat(SicboLocalStorageManager_1.default.internalGetString(this.KEY_MUSIC_VOLUME, "1"));
    };
    SicboSoundManager.prototype.saveMusicVolumeToStorage = function (volume) {
        SicboLocalStorageManager_1.default.internalSaveString(this.KEY_MUSIC_VOLUME, volume.toString());
    };
    SicboSoundManager.prototype.getSfxStatusFromStorage = function () {
        return SicboLocalStorageManager_1.default.internalGetBoolean(this.KEY_EFFECT, true);
    };
    SicboSoundManager.prototype.saveSfxStatusToStorage = function (isOn) {
        SicboLocalStorageManager_1.default.internalSaveBoolean(this.KEY_EFFECT, isOn);
    };
    SicboSoundManager.prototype.getSfxVolumeFromStorage = function () {
        return Number.parseFloat(SicboLocalStorageManager_1.default.internalGetString(this.KEY_EFFECT_VOLUME, "1"));
    };
    SicboSoundManager.prototype.saveSfxVolumeToStorage = function (volume) {
        SicboLocalStorageManager_1.default.internalSaveString(this.KEY_EFFECT_VOLUME, volume.toString());
    };
    //!SECTION
    SicboSoundManager.prototype.pauseAllEffects = function () {
        cc.audioEngine.pauseAllEffects();
    };
    SicboSoundManager.prototype.resumeAllEffects = function () {
        cc.audioEngine.resumeAllEffects();
    };
    SicboSoundManager.prototype.onDestroy = function () {
        cc.audioEngine.stopAllEffects();
        delete SicboSoundManager_1.instance;
        SicboSoundManager_1.instance = null;
    };
    var SicboSoundManager_1;
    SicboSoundManager.instance = null;
    SicboSoundManager = SicboSoundManager_1 = __decorate([
        ccclass
    ], SicboSoundManager);
    return SicboSoundManager;
}(cc.Component));
exports.default = SicboSoundManager;

cc._RF.pop();