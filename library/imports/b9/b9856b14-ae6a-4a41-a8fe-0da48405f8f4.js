"use strict";
cc._RF.push(module, 'b9856sUrmpKQaj+DaSEBfj0', 'TextNotify');
// Game/Lobby/Manager/TextNotify.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TextNotifyData = void 0;
var NotifyInstance_1 = require("../Base/NotifyInstance");
var Vec3 = cc.Vec3;
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var TextNotifyData = /** @class */ (function () {
    function TextNotifyData(msg, duration, position, onClick) {
        if (duration === void 0) { duration = 2; }
        if (position === void 0) { position = new Vec3(0, 0, 0); }
        if (onClick === void 0) { onClick = null; }
        this.onClick = null;
        this.msg = "";
        this.duration = 2;
        this.position = null;
        this.msg = msg;
        this.duration = duration;
        this.position = position;
        this.onClick = onClick;
    }
    return TextNotifyData;
}());
exports.TextNotifyData = TextNotifyData;
var TextNotify = /** @class */ (function (_super) {
    __extends(TextNotify, _super);
    function TextNotify() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.content = null;
        _this.rootNode = null;
        _this.listSprFrame = [];
        _this.data = null;
        return _this;
    }
    TextNotify.prototype.onShow = function (data) {
        var _this = this;
        this.data = data;
        var notifyData = data;
        this.content.string = notifyData.msg;
        cc.Tween.stopAllByTarget(this.node);
        cc.tween(this.node)
            .delay(notifyData.duration)
            .call(function () {
            _this.closeInstance();
        })
            .start();
        // let urlFont = "Fonts/MyriadPro-Regular";
        // cc.resources.load(urlFont, cc.Font, function (err, font) {
        //     if (!err && font) {
        //       this.content.font = font;
        //     }
        //   }.bind(this)
        // );
        this.content.node.color = new cc.Color(255, 255, 255);
        this.rootNode.getComponent(cc.Layout).paddingTop = 14;
        this.rootNode.getComponent(cc.Layout).paddingBottom = 14;
        this.rootNode.getComponent(cc.Sprite).spriteFrame = this.listSprFrame[0];
    };
    TextNotify.prototype.afterShow = function () { };
    TextNotify.prototype.beforeShow = function () { };
    TextNotify.prototype.beforeClose = function () { };
    TextNotify.prototype.afterClose = function () { };
    TextNotify.prototype.onClick = function () {
        if (this.data) {
            if (this.data.onClick)
                this.data.onClick();
        }
    };
    __decorate([
        property(cc.Label)
    ], TextNotify.prototype, "content", void 0);
    __decorate([
        property(cc.Node)
    ], TextNotify.prototype, "rootNode", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], TextNotify.prototype, "listSprFrame", void 0);
    TextNotify = __decorate([
        ccclass
    ], TextNotify);
    return TextNotify;
}(NotifyInstance_1.default));
exports.default = TextNotify;

cc._RF.pop();