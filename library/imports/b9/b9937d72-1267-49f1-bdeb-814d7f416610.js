"use strict";
cc._RF.push(module, 'b99371yEmdJ8b3rgU1/QWYQ', 'ConnectDefine');
// Game/Lobby/Manager/ConnectDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ConnectDefine = /** @class */ (function () {
    function ConnectDefine() {
    }
    ConnectDefine.fbAppId = "";
    ConnectDefine.addressConfig = ["https://firebasestorage.googleapis.com/v0/b/gameeagles-66e75.appspot.com/o/config_stg_default.json?alt=media"];
    ConnectDefine.addressURL = "https://play.henxui.fun/";
    ConnectDefine.addressURLAndParameter = "https://bentau.henxui.fun";
    ConnectDefine.addressSanBay = "https://sanbay.henxui.fun";
    ConnectDefine.addressBenTau = "https://bentau.henxui.fun";
    ConnectDefine.addressBenTauWS = "wss://bentau.henxui.fun/gameloot.bentau.BenTau/WS";
    ConnectDefine.addressResources = "https://hose.henxui.fun";
    return ConnectDefine;
}());
exports.default = ConnectDefine;

cc._RF.pop();