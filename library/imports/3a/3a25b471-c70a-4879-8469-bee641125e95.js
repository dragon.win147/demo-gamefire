"use strict";
cc._RF.push(module, '3a25bRxxwpIeYRpvuZBEl6V', 'SicboButtonSound');
// Game/Sicbo_New/Scripts/Components/SicboButtonSound.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboSoundManager_1 = require("../Managers/SicboSoundManager");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboButtonSound = /** @class */ (function (_super) {
    __extends(SicboButtonSound, _super);
    function SicboButtonSound() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sound = SicboSetting_1.SicboSound.SfxClick;
        return _this;
        // update (dt) {}
    }
    SicboButtonSound.prototype.onLoad = function () {
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node;
        clickEventHandler.component = "SicboButtonSound";
        clickEventHandler.handler = "callback";
        var button = this.node.getComponent(cc.Button);
        if (button)
            button.clickEvents.push(clickEventHandler);
        var toggle = this.node.getComponent(cc.Toggle);
        if (toggle)
            toggle.clickEvents.push(clickEventHandler);
    };
    SicboButtonSound.prototype.callback = function () {
        SicboSoundManager_1.default.getInstance().play(SicboSetting_1.default.getSoundName(this.sound));
    };
    __decorate([
        property({ type: cc.Enum(SicboSetting_1.SicboSound) })
    ], SicboButtonSound.prototype, "sound", void 0);
    SicboButtonSound = __decorate([
        ccclass
    ], SicboButtonSound);
    return SicboButtonSound;
}(cc.Component));
exports.default = SicboButtonSound;

cc._RF.pop();