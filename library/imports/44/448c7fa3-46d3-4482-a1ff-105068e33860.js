"use strict";
cc._RF.push(module, '448c7+jRtNEgqH/EFBo4zhg', 'SicboSoundItem');
// Game/Sicbo_New/Scripts/Managers/SicboSoundItem.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SicboSoundType = void 0;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboSoundType;
(function (SicboSoundType) {
    SicboSoundType[SicboSoundType["Background"] = 0] = "Background";
    SicboSoundType[SicboSoundType["Effect_Single"] = 1] = "Effect_Single";
    SicboSoundType[SicboSoundType["Effect_Multiple"] = 2] = "Effect_Multiple";
    SicboSoundType[SicboSoundType["Effect_Loop_AutoStop"] = 3] = "Effect_Loop_AutoStop"; //loop sfx va tu dong tat sau n giay
})(SicboSoundType = exports.SicboSoundType || (exports.SicboSoundType = {}));
var SicboSoundItem = /** @class */ (function (_super) {
    __extends(SicboSoundItem, _super);
    function SicboSoundItem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.audioSource = null;
        _this.soundType = SicboSoundType.Background;
        _this.autoStopDelay = -1;
        _this.audioId = -1;
        return _this;
    }
    SicboSoundItem.prototype.play = function (from, autoStopDelay) {
        if (from === void 0) { from = 0; }
        if (autoStopDelay === void 0) { autoStopDelay = -1; }
        this.autoStopDelay = autoStopDelay;
        switch (this.soundType) {
            case SicboSoundType.Background:
                if (this.isValidAudioId() && cc.audioEngine.getState(this.audioId) == cc.audioEngine.AudioState.PLAYING)
                    return;
                this.audioId = cc.audioEngine.playMusic(this.audioSource.clip, true);
                break;
            case SicboSoundType.Effect_Single:
                if (this.isValidAudioId() && cc.audioEngine.getState(this.audioId) == cc.audioEngine.AudioState.PLAYING)
                    return;
                this.audioId = cc.audioEngine.playEffect(this.audioSource.clip, false);
                var durationES = cc.audioEngine.getDuration(this.audioId);
                cc.audioEngine.setCurrentTime(this.audioId, Math.min(from, durationES));
                break;
            case SicboSoundType.Effect_Loop_AutoStop:
                if (this.isValidAudioId() && cc.audioEngine.getState(this.audioId) == cc.audioEngine.AudioState.PLAYING)
                    return;
                this.audioId = cc.audioEngine.playEffect(this.audioSource.clip, true);
                var durationLAE = cc.audioEngine.getDuration(this.audioId);
                cc.audioEngine.setCurrentTime(this.audioId, Math.min(from, durationLAE));
                var self_1 = this;
                if (self_1.autoStopDelay > 0) {
                    cc.tween(self_1.node)
                        .delay(self_1.autoStopDelay)
                        .call(function () {
                        self_1.stop();
                    }).start();
                }
                break;
            case SicboSoundType.Effect_Multiple:
                break;
        }
        // //cc.error(this.audioId);
    };
    SicboSoundItem.prototype.stop = function () {
        if (this.isValidAudioId()) {
            cc.audioEngine.stop(this.audioId);
            cc.Tween.stopAllByTarget(this.node);
        }
    };
    SicboSoundItem.prototype.isValidAudioId = function () {
        return this.audioId >= 0;
    };
    SicboSoundItem.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
    };
    __decorate([
        property(cc.AudioSource)
    ], SicboSoundItem.prototype, "audioSource", void 0);
    __decorate([
        property({ type: cc.Enum(SicboSoundType) })
    ], SicboSoundItem.prototype, "soundType", void 0);
    __decorate([
        property()
    ], SicboSoundItem.prototype, "autoStopDelay", void 0);
    SicboSoundItem = __decorate([
        ccclass
    ], SicboSoundItem);
    return SicboSoundItem;
}(cc.Component));
exports.default = SicboSoundItem;

cc._RF.pop();