"use strict";
cc._RF.push(module, '4402e/3cohJMZFPVEmTN6An', 'SicboLatestResultComp');
// Game/Sicbo/Scripts/Components/SicboLatestResultComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboHelper_1 = require("../../../Sicbo/Scripts/Helpers/SicboHelper");
var SicboItemComp_1 = require("./Items/SicboItemComp");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboLatestResultComp = /** @class */ (function (_super) {
    __extends(SicboLatestResultComp, _super);
    function SicboLatestResultComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sumLabel = null;
        _this.typeLabel = null;
        _this.items = [];
        _this.fxNode = null;
        return _this;
    }
    SicboLatestResultComp.prototype.onLoad = function () {
        this.playFx(false);
    };
    Object.defineProperty(SicboLatestResultComp.prototype, "active", {
        set: function (isActive) {
            this.items.forEach(function (item, index) {
                item.node.scale = isActive ? 1 : 0;
            });
            this.typeLabel.node.parent.active = isActive;
        },
        enumerable: false,
        configurable: true
    });
    SicboLatestResultComp.prototype.setData = function (items, isAnimated) {
        if (isAnimated === void 0) { isAnimated = true; }
        var self = this;
        items.forEach(function (item, index) {
            self.items[index].setData(items[index], isAnimated);
        });
        self.sumLabel.string = SicboHelper_1.default.getResultTotalValue(items).toString();
        var typeResult = SicboHelper_1.default.getResultType(items);
        self.typeLabel.string = SicboHelper_1.default.getResultTypeName(typeResult);
    };
    SicboLatestResultComp.prototype.onDestroy = function () {
        var self = this;
        self.items.forEach(function (item, index) {
            if (self.items[index].node)
                cc.Tween.stopAllByTarget(self.items[index].node.parent);
        });
    };
    SicboLatestResultComp.prototype.playFx = function (isPlay) {
        if (isPlay === void 0) { isPlay = true; }
        this.fxNode.active = isPlay;
    };
    __decorate([
        property(cc.Label)
    ], SicboLatestResultComp.prototype, "sumLabel", void 0);
    __decorate([
        property(cc.Label)
    ], SicboLatestResultComp.prototype, "typeLabel", void 0);
    __decorate([
        property([SicboItemComp_1.default])
    ], SicboLatestResultComp.prototype, "items", void 0);
    __decorate([
        property(cc.Node)
    ], SicboLatestResultComp.prototype, "fxNode", void 0);
    SicboLatestResultComp = __decorate([
        ccclass
    ], SicboLatestResultComp);
    return SicboLatestResultComp;
}(cc.Component));
exports.default = SicboLatestResultComp;

cc._RF.pop();