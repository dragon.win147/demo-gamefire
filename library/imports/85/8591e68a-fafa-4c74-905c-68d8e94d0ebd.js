"use strict";
cc._RF.push(module, '8591eaK+vpMdJBcaNjpTQ69', 'GlobalData');
// Game/Lobby/Utils/GlobalData.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventKey_1 = require("@gameloot/client-base-handler/build/EventKey");
var ConnectDefine_1 = require("../Define/ConnectDefine");
var BenTauHandler_1 = require("../Network/BenTauHandler");
var IdentityHandler_1 = require("../Network/IdentityHandler");
var MyAccountHandler_1 = require("../Network/MyAccountHandler");
// import SoundManager from "../Managers/SoundManager";
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GlobalData = /** @class */ (function (_super) {
    __extends(GlobalData, _super);
    function GlobalData() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.map = null;
        return _this;
    }
    GlobalData.prototype.onLoad = function () {
        this.map = new Map();
        cc.game.addPersistRootNode(this.node);
    };
    GlobalData.prototype.set = function (key, val) {
        this.map.set(key, val);
    };
    GlobalData.prototype.get = function (key) {
        return this.map.get(key);
    };
    GlobalData.prototype.clear = function () {
        this.map.clear();
    };
    GlobalData.prototype.setBundleData = function () {
        this.set(EventKey_1.default.ADDRESS_BEN_TAU, ConnectDefine_1.default.addressBenTau);
        this.set(EventKey_1.default.ADDRESS_BEN_TAU_WS, ConnectDefine_1.default.addressBenTauWS);
        this.set(EventKey_1.default.ADDRESS_SAN_BAY, ConnectDefine_1.default.addressSanBay);
        this.set(EventKey_1.default.ADDRESS_RESOURCE, ConnectDefine_1.default.addressResources);
        this.set(EventKey_1.default.FB_APP_ID, ConnectDefine_1.default.fbAppId);
        this.set(EventKey_1.default.GET_BENTAU_HANDLER, BenTauHandler_1.default.getInstance());
        // this.set(EventKey.GET_POTTER_HANDLER, PotterHandler.getInstance());
        // this.set(EventKey.GET_MARKETPLACE_HANDLER, MarketPlaceHandler.getInstance());
        // this.set(EventKey.GET_LEADER_BOARD_HANDLER, LeaderBoardHandler.getInstance());
        // this.set(EventKey.GET_SOUND_MANAGER, SoundManager.getInstance());
        this.set(EventKey_1.default.GET_TOKEN, IdentityHandler_1.default.getInstance().token);
        // this.set(EventKey.IS_LOG_VERSION, GameUtils.isLogVersion());
        this.set(EventKey_1.default.GET_FRAME_PATH, MyAccountHandler_1.default.getInstance().Frames);
        this.set(EventKey_1.default.GET_ICON_PATH, MyAccountHandler_1.default.getInstance().Bases);
        this.set(EventKey_1.default.GET_PLAYAH, MyAccountHandler_1.default.getInstance().Playah);
        // this.set(EventKey.GET_GAME_INFO, MarketPlace.games);
        cc.log("setBundleData Playah ", MyAccountHandler_1.default.getInstance().Playah.toObject());
    };
    GlobalData.prototype.setPaymentBundle = function (bundle) {
        this.set(EventKey_1.default.PAYMENT_BUNDLE, bundle);
    };
    GlobalData.prototype.setTopHuBetLevel = function (topic, gameBetLevel) {
        var mapData = this.get(EventKey_1.default.TOP_HU_BET_LEVEL);
        if (mapData) {
            mapData.set(topic, gameBetLevel);
        }
        else {
            this.set(EventKey_1.default.TOP_HU_BET_LEVEL, new Map().set(topic, gameBetLevel));
        }
    };
    GlobalData.prototype.clearTopHuBetLevel = function (topic) {
        if (topic === void 0) { topic = null; }
        if (topic) {
            var mapData = this.get(EventKey_1.default.TOP_HU_BET_LEVEL);
            if (mapData && mapData.has(topic)) {
                mapData.delete(topic);
            }
        }
        else {
            this.set(EventKey_1.default.TOP_HU_BET_LEVEL, null);
        }
    };
    GlobalData = __decorate([
        ccclass
    ], GlobalData);
    return GlobalData;
}(cc.Component));
exports.default = GlobalData;

cc._RF.pop();