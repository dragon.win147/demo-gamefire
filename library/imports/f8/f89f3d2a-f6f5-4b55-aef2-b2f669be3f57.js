"use strict";
cc._RF.push(module, 'f89f30q9vVLVa7ysvZpvj9X', 'SicboSlotWinMoneyComp');
// Game/Sicbo_New/Scripts/Components/SicboSlotWinMoneyComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboMoneyFormatComp_1 = require("../RNGCommons/SicboMoneyFormatComp");
var SicboSkeletonUtils_1 = require("../RNGCommons/Utils/SicboSkeletonUtils");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboSlotWinMoneyComp = /** @class */ (function (_super) {
    __extends(SicboSlotWinMoneyComp, _super);
    function SicboSlotWinMoneyComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.animation = null;
        _this.money = null;
        _this.showAnimation = "win_in";
        _this.idleAnimation = "win_loop";
        _this.hideAnimation = "win_out";
        _this.inTime = 0;
        _this.idleTime = .5;
        _this.outTime = 5.5 - 1;
        _this.elapseTime = 0;
        _this.isShow = false;
        _this.isIdle = false;
        _this.isHide = false;
        return _this;
    }
    SicboSlotWinMoneyComp.prototype.show = function (money, elapseTime) {
        if (elapseTime === void 0) { elapseTime = 0; }
        this.node.opacity = 0;
        // //cc.log("DKM", elapseTime)    ;
        this.money.setMoney(money);
        this.elapseTime = elapseTime;
    };
    SicboSlotWinMoneyComp.prototype.hide = function () {
        this.node.opacity = 255;
        SicboSkeletonUtils_1.default.setAnimation(this.animation, this.hideAnimation, false, this.elapseTime - this.outTime);
        this.isHide = true;
    };
    SicboSlotWinMoneyComp.prototype.update = function (dt) {
        this.elapseTime += dt;
        if (this.elapseTime > this.outTime && this.isHide == false) {
            this.node.opacity = 255;
            SicboSkeletonUtils_1.default.setAnimation(this.animation, this.hideAnimation, false, this.elapseTime - this.outTime);
            this.isHide = true;
        }
        else if (this.elapseTime > this.idleTime && this.isIdle == false) {
            this.node.opacity = 255;
            SicboSkeletonUtils_1.default.setAnimation(this.animation, this.idleAnimation, true, this.elapseTime - this.idleTime);
            this.isIdle = true;
        }
        else if (this.elapseTime > this.inTime && this.isShow == false) {
            this.node.opacity = 255;
            SicboSkeletonUtils_1.default.setAnimation(this.animation, this.showAnimation, false, this.elapseTime - this.inTime);
            this.isShow = true;
        }
        // if(this.elapseTime> this.outTime + 1) this.node.destroy();
    };
    __decorate([
        property(sp.Skeleton)
    ], SicboSlotWinMoneyComp.prototype, "animation", void 0);
    __decorate([
        property(SicboMoneyFormatComp_1.default)
    ], SicboSlotWinMoneyComp.prototype, "money", void 0);
    SicboSlotWinMoneyComp = __decorate([
        ccclass
    ], SicboSlotWinMoneyComp);
    return SicboSlotWinMoneyComp;
}(cc.Component));
exports.default = SicboSlotWinMoneyComp;

cc._RF.pop();