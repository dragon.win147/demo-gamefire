"use strict";
cc._RF.push(module, '9fbe7jqbpRFTahPcCPJ/MPr', 'SicboPortalAdapter');
// Game/Sicbo_New/Scripts/SicboPortalAdapter.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Playah = _a.Playah, BenTauMessage = _a.BenTauMessage, Topic = _a.Topic, EventKey = _a.EventKey, GetHistoryReply = _a.GetHistoryReply;
var SicboResourceManager_1 = require("./Managers/SicboResourceManager");
var ccclass = cc._decorator.ccclass;
var SicboPortalAdapter = /** @class */ (function () {
    function SicboPortalAdapter() {
        this.isLogVersion = null;
        //SECTION UserManager
        //UserManager.getInstance().userData.userId
        this.getMyUserId = null;
        //UserManager.getInstance().getMyPlayah()
        this.getMyPlayAh = null;
        //!SECTION
        //SECTION MyAccountHandler
        //SicboUserItemComp, SicboLeaderBoardItemComp
        this.onGetBasePath = null; //basepath, avatar icon
        this.onGetFramePath = null; //frame
        //!SECTION
        this.getGameInfo = null;
        //!SECTION
        //SECTION Portal
        //SicboUIComp
        this.showUserProfilePopUp = null;
        //GameUtils.gameType = GameType.Sicbo;
        this.setGameType = null;
        //SicboMessageHandler
        //showLoading
        this.showHandlerLoading = null;
        //hideLoading
        this.hideHandlerLoading = null;
        //onShowErr
        this.onShowHandlerErr = null;
        //onDisconnect
        this.onHandlerDisconnect = null;
        // getBundleInfo(callback?: (bundleInfo) => void) {
        //   cc.systemEvent.emit(EventKey.GET_BUNDLE_INFO, Topic.SICBO, callback);
        // }
    }
    SicboPortalAdapter_1 = SicboPortalAdapter;
    SicboPortalAdapter.getInstance = function () {
        if (!SicboPortalAdapter_1.instance) {
            SicboPortalAdapter_1.instance = new SicboPortalAdapter_1();
            SicboPortalAdapter_1.instance.init();
        }
        return SicboPortalAdapter_1.instance;
    };
    SicboPortalAdapter.destroy = function () {
        delete SicboPortalAdapter_1.instance;
        SicboPortalAdapter_1.instance = null;
    };
    SicboPortalAdapter.getGlobalData = function (key) {
        if (SicboPortalAdapter_1.globalComp == null) {
            var globalNode = cc.director.getScene().getChildByName(SicboPortalAdapter_1.GLOBAL_DATA_NAME);
            SicboPortalAdapter_1.globalComp = globalNode.getComponent(SicboPortalAdapter_1.GLOBAL_DATA_NAME);
        }
        return SicboPortalAdapter_1.globalComp.get(key);
    };
    //!SECTION
    //SECTION Connect Define
    SicboPortalAdapter.addressSanBay = function () {
        return SicboPortalAdapter_1.getGlobalData(EventKey.ADDRESS_SAN_BAY);
    };
    SicboPortalAdapter.addressBenTau = function () {
        return SicboPortalAdapter_1.getGlobalData(EventKey.ADDRESS_BEN_TAU);
    };
    SicboPortalAdapter.addressBenTauWS = function () {
        return SicboPortalAdapter_1.getGlobalData(EventKey.ADDRESS_BEN_TAU_WS);
    };
    SicboPortalAdapter.addressResources = function () {
        return SicboPortalAdapter_1.getGlobalData(EventKey.ADDRESS_RESOURCE);
    };
    //!SECTION
    SicboPortalAdapter.getBenTauHandler = function () {
        return SicboPortalAdapter_1.getGlobalData(EventKey.GET_BENTAU_HANDLER);
    };
    SicboPortalAdapter.getToken = function () {
        return SicboPortalAdapter_1.getGlobalData(EventKey.GET_TOKEN);
    };
    SicboPortalAdapter.getPotterHandler = function () {
        return SicboPortalAdapter_1.getGlobalData(EventKey.GET_POTTER_HANDLER);
    };
    //!SECTION
    SicboPortalAdapter.prototype.init = function () {
        this.registResourceManager();
        this.registUserManager();
        this.registMyAccountHandler();
        this.registPopup();
        this.registPortal();
        this.registMessageHandler();
        this.registWallet();
    };
    SicboPortalAdapter.prototype.registResourceManager = function () {
        SicboResourceManager_1.default.addressResources = SicboPortalAdapter_1.addressResources();
    };
    SicboPortalAdapter.prototype.registUserManager = function () {
        // BCF_EVENT.GET_USER_ID
        this.getMyUserId = function () {
            return SicboPortalAdapter_1.getGlobalData(EventKey.GET_PLAYAH).getUserId();
        };
        // BCF_EVENT.GET_PLAYAH
        this.getMyPlayAh = function () {
            return SicboPortalAdapter_1.getGlobalData(EventKey.GET_PLAYAH);
        };
    };
    SicboPortalAdapter.prototype.registMyAccountHandler = function () {
        // BCF_EVENT.GET_ICON_PATH
        this.onGetBasePath = function (id) {
            return SicboPortalAdapter_1.getGlobalData(EventKey.GET_ICON_PATH).get(id);
        };
        // BCF_EVENT.GET_FRAME_PATH
        this.onGetFramePath = function (id) {
            return SicboPortalAdapter_1.getGlobalData(EventKey.GET_FRAME_PATH).get(id);
        };
    };
    SicboPortalAdapter.prototype.registPopup = function () {
        //BCF_EVENT.GET_GAME_INFO_BY_TOPIC
        this.getGameInfo = function () {
            return SicboPortalAdapter_1.getGlobalData(EventKey.GET_GAME_INFO);
        }; //MarketPlace.getGameInfoByTopic(topic)};
    };
    SicboPortalAdapter.prototype.registPortal = function () {
        // BCF_EVENT.OPEN_USER_INFO_POPUP
        this.showUserProfilePopUp = function (playah, popupBackground) {
            cc.systemEvent.emit(EventKey.ON_OPEN_POPUP_INFO_EVENT, playah, popupBackground);
        };
        //BCF_EVENT.BACK_TO_PORTAL
        this.leaveGame = function () {
            cc.systemEvent.emit(EventKey.ON_BACK_TO_PORTAL_EVENT, Topic.SICBO);
        };
        // BCF_EVENT.SET_GAME_TYPE
        this.setGameType = function () {
            cc.systemEvent.emit(EventKey.ON_SET_GAME_TYPE_EVENT, Topic.SICBO);
        };
        this.isLogVersion = function () {
            return SicboPortalAdapter_1.getGlobalData(EventKey.IS_LOG_VERSION);
        };
    };
    SicboPortalAdapter.prototype.registMessageHandler = function () {
        // BCF_EVENT.HANDLER_SHOW_LOADING_POPUP
        this.showHandlerLoading = function (callback) {
            cc.systemEvent.emit(EventKey.ON_OPEN_POPUP_LOADING_EVENT, callback);
        };
        // BCF_EVENT.HANDLER_HIDE_LOADING_POPUP
        this.hideHandlerLoading = function () {
            cc.systemEvent.emit(EventKey.ON_CLOSE_POPUP_LOADING_EVENT);
        };
        // BCF_EVENT.HANDLER_SHOW_ERR_POPUP
        this.onShowHandlerErr = function (str, onClick) {
            cc.systemEvent.emit(EventKey.ON_OPEN_POPUP_WARNING_EVENT, str, onClick);
        };
        this.onHandlerDisconnect = function (CodeSocket, messError) {
            cc.systemEvent.emit(EventKey.ON_DISCONNECT_EVENT, CodeSocket, false, messError);
        };
    };
    SicboPortalAdapter.prototype.registWallet = function () {
        // BCF_EVENT.WALLET_GET_CURRENT_BALANCE
        this.getCurrentBalance = function (callback) {
            cc.log("registWallet GET_CURRENT_BALANCE_EVENT");
            cc.systemEvent.emit(EventKey.GET_CURRENT_BALANCE_EVENT, callback);
            // return SicboPortalAdapter.getGlobalData(EventKey.GET_CURRENT_BALANCE_EVENT)
        };
        // BCF_EVENT.WALLET_GET_LATEST_BALANCE
        this.getLatestBalance = function () {
            cc.systemEvent.emit(EventKey.ON_GET_LATEST_BALANCE_EVENT);
        };
        // BCF_EVENT.WALLET_ENABLE
        this.enableWallet = function () {
            cc.systemEvent.emit(EventKey.ON_ENABLE_WALLET_EVENT, Topic.SICBO);
        };
        // BCF_EVENT.WALLET_DISABLE
        this.disableWallet = function () {
            cc.systemEvent.emit(EventKey.ON_DISABLE_WALLET_EVENT, Topic.SICBO);
        };
        // BCF_EVENT.WALLET_ADD_MONEY
        this.addMoney = function (amount, transaction) {
            cc.systemEvent.emit(EventKey.ON_ADD_CURRENT_BALANCE_EVENT, amount, transaction);
        };
    };
    var SicboPortalAdapter_1;
    //SECTION GLOBAL DATA
    SicboPortalAdapter.GLOBAL_DATA_NAME = "GlobalData";
    SicboPortalAdapter.globalComp = null;
    SicboPortalAdapter = SicboPortalAdapter_1 = __decorate([
        ccclass
    ], SicboPortalAdapter);
    return SicboPortalAdapter;
}());
exports.default = SicboPortalAdapter;

cc._RF.pop();