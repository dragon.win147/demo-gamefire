"use strict";
cc._RF.push(module, 'ebf91qnCRNIQbM6FVs8N1An', 'SicboAutoCanvasSizeComp');
// Game/Sicbo_New/Scripts/RNGCommons/SicboAutoCanvasSizeComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var EventKey = SicboModuleAdapter_1.default.getAllRefs().EventKey;
var Size = cc.Size;
var ccclass = cc._decorator.ccclass;
var SicboAutoCanvasSizeComp = /** @class */ (function (_super) {
    __extends(SicboAutoCanvasSizeComp, _super);
    function SicboAutoCanvasSizeComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.defaultRatio = 16 / 9;
        _this.defaultRatioMax = 13 / 6;
        _this.defaultRatioMin = 4 / 3;
        _this.startSize = new cc.Size(1280, 720);
        return _this;
    }
    SicboAutoCanvasSizeComp.prototype.onLoad = function () {
        var _this = this;
        var canvas = this.getComponent(cc.Canvas);
        this.startSize = canvas.designResolution;
        this.setFlexibleWebScreen();
        cc.view.setResizeCallback(function () {
            if (_this && _this.node)
                _this.setFlexibleWebScreen();
        });
    };
    SicboAutoCanvasSizeComp.prototype.onDestroy = function () {
        if (this.isWebBuild()) {
            cc.view.setResizeCallback(null);
        }
        this.unscheduleAllCallbacks();
    };
    SicboAutoCanvasSizeComp.prototype.setFlexibleWebScreen = function () {
        var size = cc.view.getFrameSize();
        var w = size.width;
        var h = size.height;
        var ratio = w / h;
        var fitHeight = ratio > this.defaultRatio;
        var max = false;
        if (ratio > this.defaultRatioMax) {
            ratio = this.defaultRatioMax;
            max = true;
        }
        var min = false;
        if (ratio < this.defaultRatioMin) {
            ratio = this.defaultRatioMin;
            min = true;
        }
        var canvas = this.getComponent(cc.Canvas);
        canvas.fitWidth = !fitHeight;
        canvas.fitHeight = fitHeight;
        if (fitHeight) {
            if (this.isWebBuild() && max) {
                canvas.designResolution = new Size(this.startSize.height * this.defaultRatioMax, this.startSize.height);
                canvas.fitWidth = true;
                canvas.fitHeight = true;
            }
            else {
                canvas.designResolution = new Size(this.startSize.width, this.startSize.height);
            }
        }
        else {
            if (this.isWebBuild() && min) {
                canvas.designResolution = new Size(this.startSize.width, this.startSize.width / this.defaultRatioMin);
                canvas.fitWidth = true;
                canvas.fitHeight = true;
            }
            else {
                canvas.designResolution = new Size(this.startSize.width, this.startSize.height);
            }
        }
        this.scheduleOnce(function () {
            cc.systemEvent.emit(EventKey.ON_SCREEN_CHANGE_SIZE_EVENT);
        }, 1 / 60);
    };
    SicboAutoCanvasSizeComp.prototype.isWebBuild = function () {
        return (cc.sys.platform == cc.sys.DESKTOP_BROWSER || cc.sys.platform == cc.sys.MOBILE_BROWSER) && CC_BUILD;
    };
    SicboAutoCanvasSizeComp = __decorate([
        ccclass
    ], SicboAutoCanvasSizeComp);
    return SicboAutoCanvasSizeComp;
}(cc.Component));
exports.default = SicboAutoCanvasSizeComp;

cc._RF.pop();