"use strict";
cc._RF.push(module, '9ec8dOswhxGea0zXXZbZNG5', 'WalletHandler');
// Game/Lobby/Network/WalletHandler.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var client_base_handler_1 = require("@gameloot/client-base-handler");
var WalletHandler = /** @class */ (function (_super) {
    __extends(WalletHandler, _super);
    function WalletHandler() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.client = null;
        _this.onReceiveSendEvent = null;
        _this.onReceiveJoinEvent = null;
        _this.onReceiveLeaveEvent = null;
        _this.onHandleWithDrawFromVaultErr = null;
        _this.onHandleWithDrawFromVaultSucceed = null;
        _this.onVerifyOTPFalse = null;
        _this.gamePlayings = new Map();
        _this.listTransactionID = [];
        _this.max = 50;
        _this._currentBalance = 0;
        _this._currentVaultBalance = 0;
        _this._currentMainBalance = 0;
        _this._currentPromotionBalance = 0;
        _this._currentTransaction = 0;
        return _this;
    }
    Object.defineProperty(WalletHandler.prototype, "currentBalance", {
        get: function () {
            return this._currentBalance;
        },
        enumerable: false,
        configurable: true
    });
    WalletHandler.prototype.setCurrentMainBalance = function (balance) {
        this._currentMainBalance = balance;
    };
    Object.defineProperty(WalletHandler.prototype, "CurrentMainBalance", {
        get: function () {
            return this._currentMainBalance;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WalletHandler.prototype, "CurrentPromotionBalance", {
        get: function () {
            return this._currentPromotionBalance;
        },
        enumerable: false,
        configurable: true
    });
    WalletHandler.prototype.setCurrentPromotionBalance = function (balance) {
        this._currentPromotionBalance = balance;
    };
    WalletHandler.prototype.setCurrentVaultBalance = function (balance) {
        this._currentVaultBalance = balance;
    };
    Object.defineProperty(WalletHandler.prototype, "currentVaultBalance", {
        get: function () {
            return this._currentVaultBalance;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WalletHandler.prototype, "currentTransaction", {
        get: function () {
            return this._currentTransaction;
        },
        enumerable: false,
        configurable: true
    });
    WalletHandler.getInstance = function () {
        if (!WalletHandler.instance) {
            // cc.log("new getInstance(): WalletHandler");
            WalletHandler.instance = new WalletHandler(BenTauHandler_1.default.getInstance());
            WalletHandler.instance.client = new MywalletServiceClientPb_1.MyWalletClient(ConnectDefine_1.default.addressSanBay, {}, null);
            WalletHandler.instance.topic = topic_pb_1.Topic.MY_WALLET;
            WalletHandler.instance.onSubscribe();
        }
        return WalletHandler.instance;
    };
    WalletHandler.destroy = function () {
        if (WalletHandler.instance)
            WalletHandler.instance.onUnSubscribe();
        WalletHandler.instance = null;
    };
    Object.defineProperty(WalletHandler.prototype, "metaData", {
        get: function () {
            return {
                Authorization: "Bearer " + IdentityHandler_1.default.getInstance().token,
            };
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WalletHandler.prototype, "metaDataWithOTP", {
        get: function () {
            var _a;
            return _a = {
                    Authorization: "Bearer " + IdentityHandler_1.default.getInstance().token
                },
                _a["x-otp-id"] = this._xOTPId,
                _a["x-otp"] = this.OTPId,
                _a;
        },
        enumerable: false,
        configurable: true
    });
    WalletHandler.prototype.setOTPID = function (id) {
        this.OTPId = id;
    };
    WalletHandler.prototype.showLoading = function (callback) {
        UIManager_1.default.getInstance()
            .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, false, null, null)
            .then(function () {
            callback();
        });
    };
    WalletHandler.prototype.hideLoading = function () {
        UIManager_1.default.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
    };
    WalletHandler.prototype.onShowErr = function (str, onClick) {
        UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, str, onClick, onClick);
    };
    WalletHandler.prototype.onDisconnect = function (CodeSocket, messError) {
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, CodeSocket, false, messError);
    };
    WalletHandler.prototype.onMyWalletRequest = function (response) {
        var _this = this;
        if (response === void 0) { response = null; }
        var request = new google_protobuf_empty_pb.Empty();
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.me(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            var walletAmount = res.getWallet().getCurrentBalance();
            var vaultWalletAmount = res.getWallet().getBalanceInVault();
            var mainBalance = res.getWallet().getCurrentMainBalance();
            var promotionBalance = res.getWallet().getCurrentPromotionBalance();
            var transaction = res.getLastTxId();
            _this.setCurrentBalance(walletAmount, transaction);
            _this.setCurrentVaultBalance(vaultWalletAmount);
            _this.setCurrentMainBalance(mainBalance);
            _this.setCurrentPromotionBalance(promotionBalance);
            response(res);
        }, null, false);
    };
    WalletHandler.prototype.putToVault = function (amount, response) {
        var _this = this;
        if (response === void 0) { response = null; }
        var request = new mywallet_pb_1.PutToVaultRequest();
        request.setAmount(amount);
        var self = this;
        var msgId = self.getUniqueId();
        var sendRequest = function () {
            self.client.putToVault(request, self.metaData, function (err, res) {
                self.onSendReply(err, res, msgId);
            });
        };
        this.onSendRequest(msgId, sendRequest, function (res) {
            var walletAmount = res.getWallet().getCurrentBalance();
            var vaultWalletAmount = res.getWallet().getBalanceInVault();
            var mainBalance = res.getWallet().getCurrentMainBalance();
            var promotionBalance = res.getWallet().getCurrentPromotionBalance();
            var transaction = res.getChange().getLastTxId();
            _this.setCurrentBalance(walletAmount, transaction);
            _this.setCurrentVaultBalance(vaultWalletAmount);
            _this.setCurrentMainBalance(mainBalance);
            _this.setCurrentPromotionBalance(promotionBalance);
            response(res);
        }, null, false);
    };
    WalletHandler.prototype.onReceive = function (msg) {
        _super.prototype.onReceive.call(this, msg);
        var change = mywallet_change_pb_1.Change.deserializeBinary(msg.getPayload_asU8());
        var topicReceive = change.getTopic();
        //console.log("wallet topic " + topicReceive + " amount change " + change.getAmount() + " amount new " + change.getNewBalance() + " " + change.getHappenedAt());
        if (!this.gamePlayings.has(topicReceive)) {
            this.addMoney(change.getAmount(), change.getLastTxId());
        }
    };
    WalletHandler.prototype.onEnableGame = function (topic) {
        if (!this.gamePlayings.has(topic)) {
            //cc.log("Wallet onEnableGame ............." + topic);
            this.gamePlayings.set(topic, 0);
        }
    };
    WalletHandler.prototype.onDisableGame = function (topic) {
        if (this.gamePlayings.has(topic)) {
            //cc.log("Wallet onDisableGame ............." + topic);
            this.gamePlayings.delete(topic);
        }
        if (this.gamePlayings.size == 0) {
            this.onMyWalletRequest(function () { });
        }
    };
    WalletHandler.prototype.onReConnect = function (onComplete) {
        if (onComplete === void 0) { onComplete = null; }
        //BenTauWSHandler.getInstance().onReConnect();
    };
    WalletHandler.prototype.setCurrentBalance = function (value, transaction) {
        //cc.log("Update setCurrentBalance " + value + " " + transaction);
        this._currentBalance = value;
        this._currentTransaction = transaction;
        EventManager_1.default.fire(EventManager_1.default.onUpdateBalance, this._currentBalance);
        EventBusManager_1.default.onFireUpdateBalance(this._currentBalance);
    };
    //TODO lay balance theo revision
    WalletHandler.prototype.getLatestBalance = function (callback) {
        if (callback === void 0) { callback = null; }
        // cc.log("callback 1 " + (callback != null))
        this.onMyWalletRequest(function (resp) {
            // cc.log("callback " + (callback != null))
            if (callback) {
                callback(resp);
            }
        });
    };
    WalletHandler.prototype.addMoney = function (amount, transaction) {
        var allowAddMoney = true;
        //cc.log("wallet " + amount + " " + transaction);
        this.listTransactionID.forEach(function (saveID) {
            if (saveID == transaction) {
                allowAddMoney = false;
                return;
            }
        });
        if (allowAddMoney) {
            if (transaction > 0) {
                // cheat ==0 for FISH temp
                if (transaction <= this._currentTransaction)
                    return;
                this.listTransactionID.push(transaction);
            }
            this._currentBalance += amount;
            EventManager_1.default.fire(EventManager_1.default.onUpdateAddBalance, amount);
            EventBusManager_1.default.onFireAddBalance(amount, this._currentBalance);
            if (this.listTransactionID.length > this.max) {
                this.listTransactionID.shift();
            }
        }
    };
    WalletHandler.prototype.handleCustomError = function (errCode, err) {
        //cc.log("handleCustomError " + errCode);
        if (errCode == mywallet_code_pb_1.Code.OK)
            return;
        if (errCode == mywallet_code_pb_1.Code.UNKNOWN)
            return;
        var messError = "";
        switch (errCode) {
            case mywallet_code_pb_1.Code.INSUFFICIENT_BALANCE:
                messError = PortalText_1.default.POR_MESS_INSUFFICIENT_BALANCE;
                break;
            default:
                messError = errCode + " - " + err;
                break;
        }
        UIManager_1.default.getInstance().openPopupSystem(WarningPopupSystem_1.default, GameDefine_1.default.WarningPopupSystem, messError, function () { });
    };
    return WalletHandler;
}(client_base_handler_1.HandlerClientBenTauBase));
exports.default = WalletHandler;
var ConnectDefine_1 = require("../Define/ConnectDefine");
var IdentityHandler_1 = require("./IdentityHandler");
var mywallet_pb_1 = require("@gameloot/mywallet/mywallet_pb");
var MywalletServiceClientPb_1 = require("@gameloot/mywallet/MywalletServiceClientPb");
var EventManager_1 = require("../Event/EventManager");
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var mywallet_code_pb_1 = require("@gameloot/mywallet/mywallet_code_pb");
var UIManager_1 = require("../Manager/UIManager");
var WarningPopupSystem_1 = require("../Popup/WarningPopupSystem");
var BenTauHandler_1 = require("./BenTauHandler");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var GameDefine_1 = require("../Define/GameDefine");
var EventBusManager_1 = require("../Event/EventBusManager");
var PortalText_1 = require("../Utils/PortalText");
var mywallet_change_pb_1 = require("@gameloot/mywallet/mywallet_change_pb");

cc._RF.pop();