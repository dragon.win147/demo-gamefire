"use strict";
cc._RF.push(module, 'df83f2AzbJGgb9XLI+x77cV', 'ElementInstance');
// Portal/Common/Scripts/Managers/UIManager/Base/ElementInstance.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ElementInstance = /** @class */ (function (_super) {
    __extends(ElementInstance, _super);
    function ElementInstance() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._open = false;
        return _this;
    }
    ElementInstance.prototype.onLoad = function () {
        this._animation = this.getComponent(cc.Animation);
        this.node.active = true;
    };
    ElementInstance.prototype.open = function (data, onYes, onNo) {
        if (onYes === void 0) { onYes = null; }
        if (onNo === void 0) { onNo = null; }
        this.beforeShow();
        this._open = true;
        this.node.active = true;
        this._data = data;
        this._onYes = onYes;
        this._onNo = onNo;
        this.onShow(data);
        if (this._animation != null) {
            this._animation.play("Show");
        }
        else {
            this.showDone();
        }
    };
    ElementInstance.prototype.close = function () {
        this._open = false;
        this._close();
        this.beforeClose();
        if (this._animation != null) {
            this._animation.play("Hide");
        }
        else {
            this.closeDone();
        }
    };
    ElementInstance.prototype.onYes = function () {
        if (this._onYes) {
            this._onYes();
        }
        this.close();
    };
    ElementInstance.prototype.onNo = function () {
        if (this._onNo) {
            this._onNo();
        }
        this.close();
    };
    //#region Call From Animation Event
    ElementInstance.prototype.showDone = function () {
        this.afterShow();
    };
    ElementInstance.prototype.closeDone = function () {
        if (this._open == false) {
            this.node.active = false;
            this.afterClose();
        }
    };
    //#endregion
    ElementInstance.prototype.onShow = function (data) { };
    ElementInstance.prototype.afterShow = function () { };
    ElementInstance.prototype.beforeShow = function () { };
    ElementInstance.prototype.beforeClose = function () { };
    ElementInstance.prototype.afterClose = function () { };
    ElementInstance = __decorate([
        ccclass
    ], ElementInstance);
    return ElementInstance;
}(cc.Component));
exports.default = ElementInstance;

cc._RF.pop();