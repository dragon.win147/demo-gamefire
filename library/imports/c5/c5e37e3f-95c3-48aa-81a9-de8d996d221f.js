"use strict";
cc._RF.push(module, 'c5e374/lcNIqoGp3o2ZbSIf', 'SicboPageViewLoopComp');
// Game/Sicbo_New/Scripts/RNGCommons/UIComponents/SicboPageViewLoopComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboPageViewLoopComp = /** @class */ (function (_super) {
    __extends(SicboPageViewLoopComp, _super);
    function SicboPageViewLoopComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.content = null;
        _this.pages = [];
        _this.distanceBetweenPage = 677 + 16;
        _this.moveTime = 0.1;
        _this.currentPage = null;
        _this.idCurrentPage = 0;
        _this.xOriginalPageNode = 0;
        _this.isPageMoving = false;
        _this.nextPage = null;
        _this.nextID = 0;
        return _this;
    }
    SicboPageViewLoopComp.prototype.onLoad = function () {
        this.pages.forEach(function (page) {
            page.active = false;
        });
        this.currentPage = this.pages[0];
        this.idCurrentPage = 0;
        this.currentPage.active = true;
        // this.nextPage = this.pages[1];
        this.xOriginalPageNode = this.currentPage.x;
        // this.distanceBetweenPage += this.xOriginalPageNode;
        // this.nextPage.parent = this.currentPage;
        // this.nextPage.x = this.distanceBetweenPage;
        this.setUpOnTouchPage();
    };
    SicboPageViewLoopComp.prototype.setUpOnTouchPage = function () {
        var _this = this;
        this.pages.forEach(function (page) {
            // page.on(
            //     "touchstart",
            //     (e: EventTouch) => {
            //         if (this.isPageMoving) return;
            //         if (e.target.name == this.currentPage.name) {
            //             this.xPageNode = this.currentPage.x;
            //         }
            //     },
            //     this
            // );
            page.on("touchmove", function (e) {
                if (_this.isPageMoving)
                    return;
                if (e.target.name == _this.currentPage.name) {
                    _this.currentPage.x = _this.currentPage.x + e.getDeltaX();
                    if (_this.currentPage.x > _this.distanceBetweenPage + _this.xOriginalPageNode) {
                        _this.currentPage.x = _this.distanceBetweenPage + _this.xOriginalPageNode;
                    }
                    else if (_this.currentPage.x < -_this.distanceBetweenPage + _this.xOriginalPageNode) {
                        _this.currentPage.x = -_this.distanceBetweenPage + _this.xOriginalPageNode;
                    }
                    if (_this.currentPage.x > _this.xOriginalPageNode) {
                        _this.showNextByPage(true);
                    }
                    else {
                        _this.showNextByPage(false);
                    }
                }
            }, _this);
            page.on("touchcancel", function (e) {
                if (_this.isPageMoving)
                    return;
                if (e.target.name == _this.currentPage.name) {
                    //cc.log("this.current.x " + this.currentPage.x + " " + ((-this.distanceBetweenPage) / 2 + this.xOriginalPageNode));
                    if (_this.currentPage.x > _this.distanceBetweenPage / 2 + _this.xOriginalPageNode) {
                        _this.movePage(false);
                    }
                    else if (_this.currentPage.x < (-_this.distanceBetweenPage) / 2 + _this.xOriginalPageNode) {
                        _this.movePage(true);
                    }
                    else {
                        _this.moveBackToOriginal();
                    }
                }
            }, _this);
            page.on("touchend", function (e) {
                if (_this.isPageMoving)
                    return;
                if (e.target.name == _this.currentPage.name) {
                    //cc.log("this.current.x " + this.currentPage.x + " " + ((-this.distanceBetweenPage) / 2 + this.xOriginalPageNode));
                    if (_this.currentPage.x > _this.distanceBetweenPage / 2 + _this.xOriginalPageNode) {
                        _this.movePage(false);
                    }
                    else if (_this.currentPage.x < (-_this.distanceBetweenPage) / 2 + _this.xOriginalPageNode) {
                        _this.movePage(true);
                    }
                    else {
                        _this.moveBackToOriginal();
                    }
                }
            }, _this);
        });
    };
    SicboPageViewLoopComp.prototype.movePage = function (isMoveLeft) {
        var _this = this;
        this.isPageMoving = true;
        cc.Tween.stopAllByTarget(this.currentPage);
        cc.tween(this.currentPage)
            .to(this.moveTime, { x: isMoveLeft ? -this.distanceBetweenPage + this.xOriginalPageNode : this.distanceBetweenPage + this.xOriginalPageNode })
            .call(function () {
            _this.pages.forEach(function (page, index) {
                if (index != _this.idCurrentPage) {
                    page.parent = _this.content;
                    page.active = false;
                }
            });
            _this.idCurrentPage = _this.nextID;
            _this.nextPage.active = true;
            var tempPage = _this.currentPage;
            _this.currentPage = _this.nextPage;
            _this.nextPage = tempPage;
            _this.nextPage.parent = _this.currentPage;
            _this.currentPage.x = _this.xOriginalPageNode;
            _this.nextPage.x = _this.xOriginalPageNode + _this.distanceBetweenPage;
            _this.isPageMoving = false;
        })
            .start();
    };
    SicboPageViewLoopComp.prototype.moveToFirstPage = function () {
        var _this = this;
        this.pages.forEach(function (page) {
            page.parent = _this.content;
            page.active = false;
        });
        this.currentPage = this.pages[0];
        this.idCurrentPage = 0;
        this.currentPage.active = true;
        this.currentPage.x = 0;
        this.xOriginalPageNode = this.currentPage.x;
    };
    SicboPageViewLoopComp.prototype.moveBackToOriginal = function () {
        var _this = this;
        this.isPageMoving = true;
        cc.Tween.stopAllByTarget(this.currentPage);
        cc.tween(this.currentPage)
            .to(this.moveTime, { x: this.xOriginalPageNode })
            .call(function () {
            _this.isPageMoving = false;
        })
            .start();
    };
    SicboPageViewLoopComp.prototype.showNextByPage = function (isLeft) {
        var _this = this;
        if (isLeft === void 0) { isLeft = true; }
        this.pages.forEach(function (page, index) {
            if (index != _this.idCurrentPage) {
                page.parent = _this.content;
                page.active = false;
            }
        });
        if (isLeft) {
            var id = this.idCurrentPage - 1;
            if (id < 0) {
                id += this.pages.length;
            }
            this.nextID = id;
            this.pages[id].parent = this.currentPage;
            this.pages[id].x = -this.distanceBetweenPage;
            this.pages[id].active = true;
            this.nextPage = this.pages[id];
        }
        else {
            var id = this.idCurrentPage + 1;
            if (id > this.pages.length - 1) {
                id -= this.pages.length;
            }
            this.nextID = id;
            this.pages[id].parent = this.currentPage;
            this.pages[id].x = this.distanceBetweenPage;
            this.pages[id].active = true;
            this.nextPage = this.pages[id];
        }
    };
    SicboPageViewLoopComp.prototype.onNextPage = function () {
        this.showNextByPage(false);
        this.movePage(true);
    };
    SicboPageViewLoopComp.prototype.onPrePage = function () {
        this.showNextByPage(true);
        this.movePage(false);
    };
    __decorate([
        property(cc.Node)
    ], SicboPageViewLoopComp.prototype, "content", void 0);
    __decorate([
        property([cc.Node])
    ], SicboPageViewLoopComp.prototype, "pages", void 0);
    __decorate([
        property
    ], SicboPageViewLoopComp.prototype, "distanceBetweenPage", void 0);
    __decorate([
        property
    ], SicboPageViewLoopComp.prototype, "moveTime", void 0);
    SicboPageViewLoopComp = __decorate([
        ccclass
    ], SicboPageViewLoopComp);
    return SicboPageViewLoopComp;
}(cc.Component));
exports.default = SicboPageViewLoopComp;

cc._RF.pop();