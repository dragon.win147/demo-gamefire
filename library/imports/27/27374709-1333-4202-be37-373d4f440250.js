"use strict";
cc._RF.push(module, '27374cJEzNCAr43Nz1PRAJQ', 'SicboUIComp');
// Game/Sicbo_New/Scripts/Components/SicboUIComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), Playah = _a.Playah, State = _a.State, Status = _a.Status, ListCurrentJackpotsReply = _a.ListCurrentJackpotsReply;
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboUserComp_1 = require("./SicboUserComp");
var SicboBetComp_1 = require("./SicboBetComp");
var SicboController_1 = require("../Controllers/SicboController");
var SicboNotifyComp_1 = require("./SicboNotifyComp");
var SicboHelper_1 = require("../Helpers/SicboHelper");
var SicboSessionInfoComp_1 = require("./SicboSessionInfoComp");
var SicboClockStateComp_1 = require("./States/SicboClockStateComp");
var SicboDealerStateComp_1 = require("./States/SicboDealerStateComp");
var SicboResultStateComp_1 = require("./States/SicboResultStateComp");
var SicboJackpotStateComp_1 = require("./States/SicboJackpotStateComp");
var SicboNotifyStateComp_1 = require("./States/SicboNotifyStateComp");
var SicboPortalAdapter_1 = require("../SicboPortalAdapter");
var SicboGameUtils_1 = require("../RNGCommons/Utils/SicboGameUtils");
var SicboText_1 = require("../Setting/SicboText");
var SicboMenuDetails_1 = require("./SicboMenuDetails");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboUIComp = /** @class */ (function (_super) {
    __extends(SicboUIComp, _super);
    function SicboUIComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //GameBetUIComp {
        //OPEN SCENE FADE OUT EFF
        _this.effectShow = null;
        //USER COMP
        _this.userComp = null;
        _this.chatButton = null;
        _this.shakeBetTarget = null;
        _this.userProfileBackground = null;
        //BET COMP
        _this.betCompNode = null;
        _this.betComp = null;
        _this.sessionComp = null;
        _this.menuDetails = null;
        //ANCHOR State UI
        _this.clock = null;
        _this.dealer = null;
        _this.result = null;
        _this.jackpot = null;
        _this.message = null;
        _this.notifyComp = null;
        _this.chatRoot = null;
        //!SECTION
        _this.stateStatus = Status.STATUS_UNSPECIFIED;
        return _this;
        //!SECTION
    }
    SicboUIComp_1 = SicboUIComp;
    Object.defineProperty(SicboUIComp, "Instance", {
        get: function () {
            return SicboUIComp_1.instance;
        },
        enumerable: false,
        configurable: true
    });
    SicboUIComp.prototype.onLoad = function () {
        SicboUIComp_1.instance = this;
        this.betComp = this.betCompNode.getComponent(SicboBetComp_1.default);
        this.onLoadJackpot();
        this.onSubscribeJackpot();
    };
    SicboUIComp.prototype.onDestroy = function () {
        this.onUnSubscribeJackpot();
        delete SicboUIComp_1.instance;
        SicboUIComp_1.instance = null;
    };
    SicboUIComp.prototype.update = function (dt) {
        // super.update(dt);
    };
    SicboUIComp.prototype.start = function () {
        this.effectShow.node.active = true;
        this.createChipItems(SicboSetting_1.default.getChipLevelList());
        this.showEffectLoadingGame();
        this.preloadInfoSpites();
    };
    SicboUIComp.prototype.preloadInfoSpites = function () {
        // let infoResources = SicboController.Instance.getGameInfo();
        // infoResources.forEach((info) => {
        //   SicboResourceManager.loadRemoteImageWithFileField(null, info);
        // });
    };
    //SECTION BET
    SicboUIComp.prototype.myUserBet = function (door, coinType) {
        var _this = this;
        if (this.stateStatus != Status.BETTING)
            return;
        var coinNumber = SicboHelper_1.default.convertCoinTypeToValue(coinType);
        SicboController_1.default.Instance.sendBet(door, coinType, function () {
            _this.playUserBet(SicboPortalAdapter_1.default.getInstance().getMyUserId(), door, coinNumber);
            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxBet);
        });
    };
    SicboUIComp.prototype.onClickReBet = function () {
        var _this = this;
        SicboController_1.default.Instance.sendReBet(function (dabs) {
            var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
            dabs.forEach(function (dab) {
                _this.playUserBet(myUserId, dab.getDoor(), dab.getBetAmount());
            });
            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxBet);
        });
    };
    SicboUIComp.prototype.onClickDoubleBet = function () {
        var _this = this;
        SicboController_1.default.Instance.sendBetDouble(function (dabs) {
            var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
            dabs.forEach(function (dab) {
                _this.playUserBet(myUserId, dab.getDoor(), dab.getBetAmount());
            });
            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxBet);
        });
    };
    //!SECTION
    SicboUIComp.prototype.setChatEnable = function (isEnable) {
        this.chatButton.node.active = isEnable;
    };
    //ANCHOR effect loading game
    SicboUIComp.prototype.showEffectLoadingGame = function () {
        var _this = this;
        cc.tween(this.effectShow.node)
            .delay(0.3)
            .to(1, {
            opacity: 0,
        })
            .call(function () { return (_this.effectShow.node.active = false); })
            .start();
    };
    //ANCHOR SicboBetComp
    SicboUIComp.prototype.createChipItems = function (chip_level) {
        this.betComp.createChipItems(chip_level);
    };
    //!SECTION
    //ANCHOR SicboUserComp
    SicboUIComp.prototype.findUserComp = function (userId) {
        var user = this.userComp.getUserInRoom(userId);
        if (SicboGameUtils_1.default.isNullOrUndefined(user)) {
            var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
            if (userId == myUserId)
                user = this.userComp.getMyUser();
            else
                user = this.userComp.getOtherUserGroup();
        }
        return user;
    };
    SicboUIComp.prototype.myUserComp = function () {
        return this.userComp.getMyUser();
    };
    SicboUIComp.prototype.updateUsersInRoom = function (displayPlayers, remainingPlayersCount) {
        this.userComp.updateUsersInRoom(displayPlayers, remainingPlayersCount);
    };
    SicboUIComp.prototype.playUserBet = function (userId, door, chip) {
        var userUI = this.userComp.findUser(userId);
        var doorComp = this.betComp.getDoor(door);
        this.userComp.playBetAnimation(userUI, this.shakeBetTarget);
        this.betComp.throwMiniChipOnTableByValue(chip, userUI.betShakeNode, doorComp);
    };
    SicboUIComp.prototype.changeMyUserBalance = function (newBalance) {
        this.userComp.getMyUser().setMoney(newBalance);
    };
    //ANCHOR State UI
    SicboUIComp.prototype.exitState = function (status) {
        this.betComp.exitState(status);
        this.clock.exitState(status);
        this.dealer.exitState(status);
        this.result.exitState(status);
        this.jackpot.exitState(status);
        this.message.exitState(status);
        this.sessionComp.exitState(status);
        this.menuDetails.exitState(status);
    };
    SicboUIComp.prototype.startState = function (state, stateDuration) {
        //cc.log("SICBO STATE", state.getStatus(), stateDuration);
        this.betComp.startState(state, stateDuration);
        this.clock.startState(state, stateDuration);
        this.dealer.startState(state, stateDuration);
        this.result.startState(state, stateDuration);
        this.jackpot.startState(state, stateDuration);
        this.message.startState(state, stateDuration);
        this.sessionComp.startState(state, stateDuration);
        this.menuDetails.startState(state, stateDuration);
        this.stateStatus = state.getStatus();
    };
    SicboUIComp.prototype.updateState = function (state) {
        this.betComp.updateState(state);
        this.clock.updateState(state);
        this.dealer.updateState(state);
        this.result.updateState(state);
        this.jackpot.updateState(state);
        this.message.updateState(state);
        this.sessionComp.updateState(state);
        this.menuDetails.updateState(state);
    };
    SicboUIComp.prototype.onHide = function () {
        this.betComp.onHide();
        this.clock.onHide();
        this.dealer.onHide();
        this.result.onHide();
        this.jackpot.onHide();
        this.message.onHide();
        this.sessionComp.onHide();
        this.menuDetails.onHide();
    };
    SicboUIComp.prototype.onLoadJackpot = function () {
        // SicboPortalAdapter.getPotterHandler().getListCurrentJackpotsRequest((res: InstanceType<typeof ListCurrentJackpotsReply>) => {
        //   this.jackpot.handleResponseJackpot(res);
        // });
    };
    SicboUIComp.prototype.onSubscribeJackpot = function () {
        // SicboPortalAdapter.getPotterHandler().onSubscribeJackpot("SicboUIComp", (res: InstanceType<typeof ListCurrentJackpotsReply>) => {
        //   this.jackpot.handleResponseJackpot(res);
        // });
    };
    SicboUIComp.prototype.onUnSubscribeJackpot = function () {
        // SicboPortalAdapter.getPotterHandler().onUnSubscribeJackpot("SicboUIComp");
    };
    //ANCHOR show Error message
    SicboUIComp.prototype.showNotifyMessage = function (message) {
        this.notifyComp.showSmallNoti(message, 0.1, 2, 0.1);
    };
    SicboUIComp.prototype.showErrMessage = function (errCode, err) {
        // if (errCode == Code.SICBO_BET_FAILED ||
        //   errCode == Code.SICBO_CANNOT_REBET ||
        //   errCode == Code.SICBO_CANNOT_BET_DOUBLE) return;
        var message = SicboText_1.default.getErrorMessage(errCode);
        if (message == null) {
            message = errCode + " - " + err;
            //cc.log("SICBO ERROR", message);
        }
        else {
            this.showNotifyMessage(message);
        }
    };
    //!SECTION
    //SECTION user profile
    SicboUIComp.prototype.showUserProfilePopUp = function (playah) {
        if (playah == null)
            return;
        SicboPortalAdapter_1.default.getInstance().showUserProfilePopUp(playah, this.userProfileBackground);
    };
    var SicboUIComp_1;
    SicboUIComp.instance = null;
    __decorate([
        property(cc.Sprite)
    ], SicboUIComp.prototype, "effectShow", void 0);
    __decorate([
        property(SicboUserComp_1.default)
    ], SicboUIComp.prototype, "userComp", void 0);
    __decorate([
        property(cc.Button)
    ], SicboUIComp.prototype, "chatButton", void 0);
    __decorate([
        property(cc.Node)
    ], SicboUIComp.prototype, "shakeBetTarget", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], SicboUIComp.prototype, "userProfileBackground", void 0);
    __decorate([
        property(cc.Node)
    ], SicboUIComp.prototype, "betCompNode", void 0);
    __decorate([
        property(SicboSessionInfoComp_1.default)
    ], SicboUIComp.prototype, "sessionComp", void 0);
    __decorate([
        property(SicboMenuDetails_1.default)
    ], SicboUIComp.prototype, "menuDetails", void 0);
    __decorate([
        property(SicboClockStateComp_1.default)
    ], SicboUIComp.prototype, "clock", void 0);
    __decorate([
        property(SicboDealerStateComp_1.default)
    ], SicboUIComp.prototype, "dealer", void 0);
    __decorate([
        property(SicboResultStateComp_1.default)
    ], SicboUIComp.prototype, "result", void 0);
    __decorate([
        property(SicboJackpotStateComp_1.default)
    ], SicboUIComp.prototype, "jackpot", void 0);
    __decorate([
        property(SicboNotifyStateComp_1.default)
    ], SicboUIComp.prototype, "message", void 0);
    __decorate([
        property(SicboNotifyComp_1.default)
    ], SicboUIComp.prototype, "notifyComp", void 0);
    __decorate([
        property(cc.Node)
    ], SicboUIComp.prototype, "chatRoot", void 0);
    SicboUIComp = SicboUIComp_1 = __decorate([
        ccclass
    ], SicboUIComp);
    return SicboUIComp;
}(cc.Component));
exports.default = SicboUIComp;

cc._RF.pop();