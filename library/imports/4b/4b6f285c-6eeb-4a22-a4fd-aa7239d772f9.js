"use strict";
cc._RF.push(module, '4b6f2hcbutKIqT9qnI513L5', 'TimeUtils');
// Game/Lobby/Utils/TimeUtils.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var timestamp_pb_1 = require("google-protobuf/google/protobuf/timestamp_pb");
var TimeUtils = /** @class */ (function () {
    function TimeUtils() {
    }
    TimeUtils.getTime1970 = function () {
        var timestamp = new timestamp_pb_1.Timestamp();
        timestamp.fromDate(new Date(1970, 0, 1));
        return timestamp;
    };
    TimeUtils.getTimeCurrentDate = function () {
        var timestamp = new timestamp_pb_1.Timestamp();
        timestamp.fromDate(new Date());
        return timestamp;
    };
    TimeUtils.getTimeFromDate = function (day) {
        var timestamp = new timestamp_pb_1.Timestamp();
        var today = new Date();
        var fromDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() - day, today.getHours(), today.getMinutes());
        timestamp.fromDate(fromDay);
        return timestamp;
    };
    TimeUtils.getCurrentTimeByMillisecond = function () {
        return new Date().getTime();
    };
    TimeUtils.getTimeWithDate = function (year, month, day, hour, minute) {
        if (month === void 0) { month = 1; }
        if (day === void 0) { day = 1; }
        if (hour === void 0) { hour = 0; }
        if (minute === void 0) { minute = 0; }
        var timestamp = new timestamp_pb_1.Timestamp();
        timestamp.fromDate(new Date(year, month - 1, day, hour, minute));
        return timestamp;
    };
    TimeUtils.parseTimeToString = function (date, separateChar) {
        if (separateChar === void 0) { separateChar = "\n"; }
        function pad(n) {
            return n < 10 ? "0" + n : n;
        }
        if (!date)
            date = new Date();
        var year = pad(date.getDate()) + "/" + pad(date.getMonth() + 1) + "/" + date.getFullYear();
        var minute = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
        var hour = date.getHours() + ":" + minute;
        return year + separateChar + hour;
    };
    TimeUtils.parseTimeToDateNotHour = function (date) {
        function pad(n) {
            return n < 10 ? "0" + n : n;
        }
        if (!date)
            date = new Date();
        var year = pad(date.getDate()) + "/" + pad(date.getMonth() + 1) + "/" + date.getFullYear();
        return year;
    };
    TimeUtils.parseTimeToHourNotDate = function (date) {
        function pad(n) {
            return n < 10 ? "0" + n : n;
        }
        if (!date)
            date = new Date();
        var minute = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
        var hour = date.getHours() + "h" + minute;
        return hour;
    };
    TimeUtils.parseTimeToString_v2 = function (date, separateChar) {
        if (separateChar === void 0) { separateChar = " "; }
        function pad(n) {
            return n < 10 ? "0" + n : n;
        }
        if (!date)
            date = new Date();
        var year = pad(date.getDate()) + "/" + pad(date.getMonth() + 1) + "/" + date.getFullYear();
        var minute = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
        var hour = (date.getHours() > 9 ? date.getHours() : "0" + date.getHours()) + ":" + minute;
        return year + separateChar + hour;
    };
    TimeUtils.parseTimeToString_v3 = function (date) {
        function pad(n) {
            return n < 10 ? "0" + n : n;
        }
        if (!date)
            date = new Date();
        var year = pad(date.getDate()) + "-" + pad(date.getMonth() + 1) + "-" + date.getFullYear();
        var minute = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
        var hour = date.getHours() + ":" + minute;
        return hour + " " + year;
    };
    TimeUtils.parseTimeToString_v4 = function (date) {
        var dateString = "";
        switch (date.getDay()) {
            case 0:
                dateString += "CN";
                break;
            case 1:
                dateString += "T2";
                break;
            case 2:
                dateString += "T3";
                break;
            case 3:
                dateString += "T4";
                break;
            case 4:
                dateString += "T5";
                break;
            case 5:
                dateString += "T6";
                break;
            case 6:
                dateString += "T7";
                break;
        }
        dateString += "/" + date.getDate() + "-" + (date.getMonth() + 1) + "[" + date.getHours().toString().padStart(2, "0") + ":" + date.getMinutes().toString().padStart(2, "0") + ":" + date.getSeconds().toString().padStart(2, "0") + "]";
        return dateString;
    };
    TimeUtils.parseTimeToString_v5 = function (date) {
        var dateString = "";
        switch (date.getDay()) {
            case 0:
                dateString += "CN";
                break;
            case 1:
                dateString += "T2";
                break;
            case 2:
                dateString += "T3";
                break;
            case 3:
                dateString += "T4";
                break;
            case 4:
                dateString += "T5";
                break;
            case 5:
                dateString += "T6";
                break;
            case 6:
                dateString += "T7";
                break;
        }
        dateString += "-" + date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear().toString().substring(2) + "[" + date.getHours().toString().padStart(2, "0") + ":" + date.getMinutes().toString().padStart(2, "0") + "]";
        return dateString;
    };
    TimeUtils.parseTimeToStringDMY = function (date) {
        function pad(n) {
            return n < 10 ? "0" + n : n;
        }
        if (!date)
            date = new Date();
        var year = pad(date.getDate()) + "/" + pad(date.getMonth() + 1) + "/" + date.getFullYear();
        return year;
    };
    TimeUtils.parseTimeToStringDMY_2 = function (date) {
        function pad(n) {
            return n < 10 ? "0" + n : n;
        }
        if (!date)
            date = new Date();
        var year = pad(date.getDate()) + "/" + pad(date.getMonth() + 1);
        return year;
    };
    TimeUtils.getMillisecond = function (date) {
        return date.getTime();
    };
    TimeUtils.getSeconds = function (date) {
        return (date.getTime() - date.getTime() % 1000) / 1000;
    };
    return TimeUtils;
}());
exports.default = TimeUtils;

cc._RF.pop();