"use strict";
cc._RF.push(module, '4b8ae3J+clL5IQW6jPjOf3w', 'SicboClockStateComp');
// Game/Sicbo_New/Scripts/Components/States/SicboClockStateComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), State = _a.State, Status = _a.Status;
var SicboSetting_1 = require("../../Setting/SicboSetting");
var SicboController_1 = require("../../Controllers/SicboController");
var SicboSkeletonUtils_1 = require("../../RNGCommons/Utils/SicboSkeletonUtils");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboClockStateComp = /** @class */ (function (_super) {
    __extends(SicboClockStateComp, _super);
    function SicboClockStateComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.countDownText = null;
        _this.ringAnimation = null;
        _this.RING_TIME = 5;
        _this.elapseTime = 0;
        _this.totalDuration = 0;
        // lastRemainingTime = -1;
        _this.isRushSfxPlaying = false;
        _this.isClockPaused = true;
        return _this;
    }
    SicboClockStateComp.prototype.onLoad = function () {
        // this.node.active = false;
        this.stopAnimation();
    };
    SicboClockStateComp.prototype.exitState = function (status) {
        this.stopAnimation();
        // this.node.active = false;
        // this.lastRemainingTime = -1;
        this.isRushSfxPlaying = false;
        if (status == Status.BETTING) {
            // SicboController.Instance.playSfx(SicboSound.SfxStopBetting);
            SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxRush);
        }
    };
    SicboClockStateComp.prototype.updateState = function (state) {
        var status = state.getStatus();
        this.elapseTime = state.getStageTime();
        if (status != Status.BETTING && status != Status.PAYING)
            return;
        var remainingTime = Math.max(Math.ceil((this.totalDuration - this.elapseTime) / 1000), 0);
        this.countDownText.string = remainingTime.toString();
        if (status == Status.BETTING && remainingTime <= this.RING_TIME) {
            // if(remainingTime == 0){
            //     this.stopAnimation();
            //     return;
            // }
            if (this.isRushSfxPlaying == false) {
                SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxRush, true);
                this.isRushSfxPlaying = true;
            }
            if (this.isClockPaused) {
                this.isClockPaused = false;
                // this.ringAnimation.animation = "animation";
                SicboSkeletonUtils_1.default.setAnimation(this.ringAnimation, "animation", true);
            }
            if (this.elapseTime % 1000 == 0) {
                // //cc.log("PlaySfx SfxTictoc");
                // SicboController.Instance.playSfx(SicboSound.SfxTictoc);
                // this.lastRemainingTime = remainingTime;
                cc.tween(this.countDownText.node).to(0.1, { scale: 2 }).to(0.1, { scale: 1 }).start();
            }
        }
    };
    SicboClockStateComp.prototype.startState = function (state, totalDuration) {
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        this.elapseTime = elapseTime * 1000;
        this.totalDuration = totalDuration;
        switch (status) {
            case Status.BETTING:
            case Status.PAYING:
                // this.node.active = true;
                break;
            default:
                // this.node.active = false;
                this.stopAnimation();
                break;
        }
    };
    SicboClockStateComp.prototype.stopAnimation = function () {
        SicboSkeletonUtils_1.default.setAnimation(this.ringAnimation, "idle", true);
        this.isClockPaused = true;
        this.countDownText.node.scale = 1;
        this.countDownText.string = "0";
        // this.ringAnimation.setBonesToSetupPose();
    };
    SicboClockStateComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.countDownText.node);
    };
    SicboClockStateComp.prototype.onHide = function () {
        cc.Tween.stopAllByTarget(this.countDownText.node);
        this.stopAnimation();
        this.isRushSfxPlaying = false;
        SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxRush);
    };
    __decorate([
        property(cc.Label)
    ], SicboClockStateComp.prototype, "countDownText", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboClockStateComp.prototype, "ringAnimation", void 0);
    SicboClockStateComp = __decorate([
        ccclass
    ], SicboClockStateComp);
    return SicboClockStateComp;
}(cc.Component));
exports.default = SicboClockStateComp;

cc._RF.pop();