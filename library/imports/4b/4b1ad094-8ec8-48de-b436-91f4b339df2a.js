"use strict";
cc._RF.push(module, '4b1adCUjshI3rQ2kfSzOd8q', 'WSClient');
// Game/Lobby/Network/WebSocketClient/WSClient.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var WSClient_Android_1 = require("./WSClient_Android");
var WSClient_WebAndIOS_1 = require("./WSClient_WebAndIOS");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var WSClient = /** @class */ (function () {
    function WSClient() {
        this.ws_android_client = new WSClient_Android_1.default();
        this.ws_webios_client = new WSClient_WebAndIOS_1.default();
    }
    WSClient.prototype.open = function (address, onOpen, onMessage, onClose, onError, onReconnect) {
        cc.log("Open new WS");
        if (cc.sys.platform == cc.sys.ANDROID) {
            this.ws_android_client.open(address, onOpen, onMessage, onClose, onError, onReconnect);
        }
        else {
            this.ws_webios_client.open(address, onOpen, onMessage, onClose, onError, onReconnect);
        }
    };
    WSClient.prototype.send = function (data) {
        if (cc.sys.platform == cc.sys.ANDROID) {
            this.ws_android_client.send(data);
        }
        else {
            this.ws_webios_client.send(data);
        }
    };
    WSClient.prototype.close = function (code, reason) {
        cc.log("WSClient close ");
        if (cc.sys.platform == cc.sys.ANDROID) {
            this.ws_android_client.closeWS(code, reason);
        }
        else {
            this.ws_webios_client.closeWS(code, reason);
        }
    };
    WSClient.prototype.reConnect = function (code, reason) {
        cc.log("WSClient reConnect ");
        if (cc.sys.platform == cc.sys.ANDROID) {
            this.ws_android_client.reConnect(code, reason);
        }
        else {
            this.ws_webios_client.reConnect(code, reason);
        }
    };
    WSClient = __decorate([
        ccclass
    ], WSClient);
    return WSClient;
}());
exports.default = WSClient;

cc._RF.pop();