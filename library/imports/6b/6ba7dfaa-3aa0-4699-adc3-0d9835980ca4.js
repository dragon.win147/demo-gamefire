"use strict";
cc._RF.push(module, '6ba7d+qOqBGma3DDZg1mAyk', 'SicboModuleAdapter');
// SicboModuleAdapter.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sicbov2_code_pb_1 = require("@gameloot/sicbov2/sicbov2_code_pb");
var HandlerClientBase_1 = require("@gameloot/client-base-handler/build/HandlerClientBase");
var EventKey_1 = require("@gameloot/client-base-handler/build/EventKey");
var empty_pb_1 = require("google-protobuf/google/protobuf/empty_pb");
var playah_pb_1 = require("@gameloot/playah/playah_pb");
var bentau_pb_1 = require("@gameloot/bentau/bentau_pb");
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var whatsapp_pb_1 = require("@gameloot/whatsapp/whatsapp_pb");
var MyHistoryServiceClientPb_1 = require("@gameloot/myhistory/MyHistoryServiceClientPb");
var myhistory_pb_1 = require("@gameloot/myhistory/myhistory_pb");
var LeaderboardServiceClientPb_1 = require("@gameloot/leaderboard/LeaderboardServiceClientPb");
var leaderboard_pb_1 = require("@gameloot/leaderboard/leaderboard_pb");
var conveyor_pb_1 = require("@gameloot/conveyor/conveyor_pb");
var Sicbov2ServiceClientPb_1 = require("@gameloot/sicbov2/Sicbov2ServiceClientPb");
var sicbov2_pb_1 = require("@gameloot/sicbov2/sicbov2_pb");
var build_1 = require("@gameloot/client-base-handler/build");
var timestamp_pb_1 = require("google-protobuf/google/protobuf/timestamp_pb");
var sicbov2_pb_2 = require("@gameloot/sicbov2/sicbov2_pb");
var PotterServiceClientPb_1 = require("@gameloot/potter/PotterServiceClientPb");
var potter_pb_1 = require("@gameloot/potter/potter_pb");
var SicboModuleAdapter = /** @class */ (function () {
    function SicboModuleAdapter() {
    }
    SicboModuleAdapter.getAllRefs = function () {
        return {
            Code: sicbov2_code_pb_1.Code, HandlerClientBase: HandlerClientBase_1.default, Empty: empty_pb_1.Empty, Playah: playah_pb_1.Playah,
            BenTauMessage: bentau_pb_1.BenTauMessage, BenTauReply: bentau_pb_1.BenTauReply, Topic: topic_pb_1.Topic,
            HistoryClient: MyHistoryServiceClientPb_1.MyHistoryClient, GetChannelHistoryReply: myhistory_pb_1.GetChannelHistoryReply, GetPersonalHistoryReply: myhistory_pb_1.GetPersonalHistoryReply, GetChannelHistoryRequest: myhistory_pb_1.GetChannelHistoryRequest, GetPersonalHistoryRequest: myhistory_pb_1.GetPersonalHistoryRequest,
            LeaderboardClient: LeaderboardServiceClientPb_1.LeaderboardClient, RankEarningRequest: leaderboard_pb_1.RankEarningRequest, RankEarningReply: leaderboard_pb_1.RankEarningReply, DetailedLeader: leaderboard_pb_1.DetailedLeader, Cutoff: leaderboard_pb_1.Cutoff,
            FileField: conveyor_pb_1.FileField, ResourceField: conveyor_pb_1.ResourceField,
            SicboClient: Sicbov2ServiceClientPb_1.SicboClient,
            BetDoubleReply: sicbov2_pb_1.BetDoubleReply, BetDoubleRequest: sicbov2_pb_1.BetDoubleRequest, BetReply: sicbov2_pb_1.BetReply, BetRequest: sicbov2_pb_1.BetRequest, Channel: sicbov2_pb_1.Channel, Door: sicbov2_pb_1.Door,
            GetOthersReply: sicbov2_pb_1.GetOthersReply, GetOthersRequest: sicbov2_pb_1.GetOthersRequest, LeaveRequest: sicbov2_pb_1.LeaveRequest, Message: sicbov2_pb_1.Message, ReBetReply: sicbov2_pb_1.ReBetReply, AutoJoinReply: sicbov2_pb_2.AutoJoinReply,
            ReBetRequest: sicbov2_pb_1.ReBetRequest, State: sicbov2_pb_1.State, Result: sicbov2_pb_1.Result, Event: sicbov2_pb_1.Event, DoorAndBetAmount: sicbov2_pb_1.DoorAndBetAmount, Status: sicbov2_pb_1.Status, ChannelHistoryMetadata: sicbov2_pb_1.ChannelHistoryMetadata,
            PersonalHistoryMetadata: sicbov2_pb_1.PersonalHistoryMetadata, Item: sicbov2_pb_1.Item, ChipLevels: sicbov2_pb_1.ChipLevels, StageTime: sicbov2_pb_1.StageTime, LeaveReply: sicbov2_pb_1.LeaveReply,
            HandlerClientBenTauBase: build_1.HandlerClientBenTauBase, CodeSocket: build_1.CodeSocket, Timestamp: timestamp_pb_1.Timestamp, EventKey: EventKey_1.default, ListCurrentJackpotsReply: potter_pb_1.ListCurrentJackpotsReply, GetHistoryReply: potter_pb_1.GetHistoryReply, ListCurrentJackpotsRequest: potter_pb_1.ListCurrentJackpotsRequest, GetHistoryRequest: potter_pb_1.GetHistoryRequest, PotterClient: PotterServiceClientPb_1.PotterClient, JackpotHistoryMetadata: sicbov2_pb_1.JackpotHistoryMetadata,
            WhatAppMessage: whatsapp_pb_1.Message,
            HealthCheckRequest: sicbov2_pb_1.HealthCheckRequest, HealthCheckReply: sicbov2_pb_1.HealthCheckReply
        };
        // let moduleAdapter = null;
        // const adapterName = "SicboAdapterImport"
        // try {
        //   moduleAdapter = require(adapterName)[adapterName];
        // } catch {
        //   //cc.log("new token get Chạy lúc gắn vào portal ");
        // }
        // if (moduleAdapter != null) {
        //   //duoc chay luc build
        //   return new moduleAdapter().getAllRefs();
        // }
        // let temp = cc.director.getScene().getChildByName(adapterName)?.getComponent(adapterName)?.getAllRefs();
        // return temp;
    };
    return SicboModuleAdapter;
}());
exports.default = SicboModuleAdapter;

cc._RF.pop();