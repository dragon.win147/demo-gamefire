"use strict";
cc._RF.push(module, 'b661ahVspxAsrdgxc1sIhaf', 'GameUtils');
// Game/Lobby/Utils/GameUtils.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Badge = void 0;
var EnumGame_1 = require("../Utils/EnumGame");
var GameDefine_1 = require("../Define/GameDefine");
var topic_pb_1 = require("@gameloot/topic/topic_pb");
var Badge;
(function (Badge) {
    Badge["gold"] = "_gold";
    Badge["platinum"] = "_platinum";
    Badge["diamond"] = "_diamond";
    Badge["ruby"] = "_ruby";
    Badge["emerald"] = "_emerald";
})(Badge = exports.Badge || (exports.Badge = {}));
var GameUtils = /** @class */ (function () {
    function GameUtils() {
    }
    GameUtils.createClickBtnEvent = function (node, componentName, handlerEvent, custom) {
        if (custom === void 0) { custom = ""; }
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = node; //This node is the node to which your event handler code component belongs
        clickEventHandler.component = componentName; //This is the code file name
        clickEventHandler.handler = handlerEvent;
        clickEventHandler.customEventData = custom;
        return clickEventHandler;
    };
    GameUtils.getUrlParameter = function (name, url) {
        name = name.replace("/[[]/", "\\[").replace("/[]]/", "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        var results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    GameUtils.removeParameterUrl = function (sParam, sourceURL) {
        var rtn = sourceURL.split("?")[0], param, params_arr = [], queryString = sourceURL.indexOf("?") !== -1 ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === sParam) {
                    params_arr.splice(i, 1);
                }
            }
            if (params_arr.length)
                rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    };
    GameUtils.removeAllParameterUrl = function (sourceURL) {
        var rtn = sourceURL.split("?")[0], param, params_arr = [], queryString = sourceURL.indexOf("?") !== -1 ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                // if (param === sParam) {
                params_arr.splice(i, 1);
                // }
            }
            if (params_arr.length)
                rtn = rtn + "?" + params_arr.join("&");
        }
        window.history.replaceState(null, null, rtn);
        // return rtn;
    };
    GameUtils.convertToOtherNode = function (fromNode, targetNode, pos) {
        if (pos === void 0) { pos = null; }
        var parent = fromNode;
        if (fromNode.parent)
            parent = fromNode.parent;
        var worldSpace = parent.convertToWorldSpaceAR(fromNode.position);
        if (pos) {
            worldSpace = fromNode.convertToWorldSpaceAR(pos);
        }
        return targetNode.convertToNodeSpaceAR(worldSpace);
    };
    GameUtils.convertToOtherNode2 = function (fromNode, targetNode, pos) {
        if (pos === void 0) { pos = null; }
        var parent = fromNode;
        if (fromNode.parent)
            parent = fromNode.parent;
        var worldSpace = parent.convertToWorldSpaceAR(fromNode.getPosition());
        if (pos) {
            worldSpace = fromNode.convertToWorldSpaceAR(pos);
        }
        return targetNode.convertToNodeSpaceAR(worldSpace);
    };
    GameUtils.rectContainsPoint = function (rect, point) {
        var result = point.x >= rect.origin.x && point.x <= rect.origin.x + rect.size.width && point.y >= rect.origin.y && point.y <= rect.origin.y + rect.size.height;
        return result;
    };
    GameUtils.createItemFromNode = function (item, node, parentNode, insert) {
        if (parentNode === void 0) { parentNode = null; }
        if (insert === void 0) { insert = false; }
        if (!node)
            return null;
        var parent = parentNode ? parentNode : node.parent;
        var newNode = cc.instantiate(node);
        if (insert) {
            parent.insertChild(newNode, 0);
        }
        else {
            parent.addChild(newNode);
        }
        var newItem = newNode.getComponent(item);
        newItem.node.active = true;
        node.active = false;
        return newItem;
    };
    GameUtils.cloneItemFromNode = function (item, node) {
        if (!node)
            return null;
        var newNode = cc.instantiate(node);
        var newItem = newNode.getComponent(item);
        newItem.node.active = true;
        node.active = false;
        return newItem;
    };
    GameUtils.createItemFromPrefab = function (item, prefab, parentNode) {
        if (!prefab)
            return null;
        var node = cc.instantiate(prefab);
        parentNode.addChild(node);
        var newItem = node.getComponent(item);
        newItem.node.active = true;
        return newItem;
    };
    GameUtils.createNodeFromPrefab = function (prefab, parentNode) {
        if (!prefab)
            return null;
        var node = cc.instantiate(prefab);
        parentNode.addChild(node);
        return node;
    };
    GameUtils.formatDate = function (date, format) {
        format = format.replace("%Y", date.getFullYear().toString());
        var month = date.getMonth() + 1;
        format = format.replace("%M", month.toString().length < 2 ? "0" + month.toString() : month.toString());
        format = format.replace("%d", date.getDate().toString().length < 2 ? "0" + date.getDate().toString() : date.getDate().toString());
        format = format.replace("%h", date.getHours().toString().length < 2 ? "0" + date.getHours().toString() : date.getHours().toString());
        format = format.replace("%m", date.getMinutes().toString().length < 2 ? "0" + date.getMinutes().toString() : date.getMinutes().toString());
        format = format.replace("%s", date.getSeconds().toString().length < 2 ? "0" + date.getSeconds().toString() : date.getSeconds().toString());
        return format;
    };
    GameUtils.formatMoreDate = function (startDate, endDate, format) {
        format = format.replace("%Y1", startDate.getFullYear().toString());
        var monthFrom = startDate.getMonth() + 1;
        format = format.replace("%M1", monthFrom.toString().length < 2 ? "0" + monthFrom.toString() : monthFrom.toString());
        format = format.replace("%d1", startDate.getDate().toString().length < 2 ? "0" + startDate.getDate().toString() : startDate.getDate().toString());
        format = format.replace("%h1", startDate.getHours().toString().length < 2 ? "0" + startDate.getHours().toString() : startDate.getHours().toString());
        format = format.replace("%m1", startDate.getMinutes().toString().length < 2 ? "0" + startDate.getMinutes().toString() : startDate.getMinutes().toString());
        // end date
        format = format.replace("%Y2", endDate.getFullYear().toString());
        var monthTo = startDate.getMonth() + 1;
        format = format.replace("%M2", monthTo.toString().length < 2 ? "0" + monthTo.toString() : monthTo.toString());
        format = format.replace("%d2", endDate.getDate().toString().length < 2 ? "0" + endDate.getDate().toString() : endDate.getDate().toString());
        format = format.replace("%h2", endDate.getHours().toString().length < 2 ? "0" + endDate.getHours().toString() : endDate.getHours().toString());
        format = format.replace("%m2", endDate.getMinutes().toString().length < 2 ? "0" + endDate.getMinutes().toString() : endDate.getMinutes().toString());
        return format;
    };
    GameUtils.formatMoneyNumberMyUser = function (money) {
        var s = "$ " + this.numberWithCommas(money);
        return s;
    };
    GameUtils.formatMoneyNumberMyUserNotIcon = function (money) {
        return this.numberWithCommas(money);
    };
    GameUtils.formatMoneyNumberUser = function (money) {
        var s = "$ " + this.formatMoneyNumber(money);
        return s;
    };
    GameUtils.formatMoneyNumber = function (money) {
        var sign = 1;
        var value = money;
        if (money < 0) {
            sign = -1;
            value = value * -1;
        }
        var format = "";
        if (value >= 1000000000.0) {
            value /= 1000000000.0;
            format = "B";
        }
        else if (value >= 1000000.0) {
            value /= 1000000.0;
            format = "M";
        }
        else if (value >= 1000.0) {
            value /= 1000.0;
            format = "K";
        }
        value = (Math.floor(value * 100 + 0.00000001) / 100) * sign;
        return value + format;
    };
    GameUtils.formatMoneyNumber_v2 = function (money) {
        var sign = 1;
        var value = money;
        if (money < 0) {
            sign = -1;
            value = value * -1;
        }
        var format = "";
        if (value >= 1000000000.0) {
            value /= 1000000000.0;
            format = " Tỉ";
        }
        else if (value >= 1000000.0) {
            value /= 1000000.0;
            format = " Triệu";
        }
        else if (value >= 1000.0) {
            value /= 1000.0;
            format = " Ngàn";
        }
        value = (Math.floor(value * 100 + 0.00000001) / 100) * sign;
        return value + format;
    };
    GameUtils.numberWithCommasMoney = function (number) {
        var s = "$ " + this.numberWithCommas(number);
        return s;
    };
    GameUtils.numberWithCommas = function (number) {
        if (number) {
            var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
            result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return result[1] !== "00" ? result.join(",") : result[0];
        }
        return "0";
    };
    GameUtils.numberWithCommasV2 = function (number) {
        if (number) {
            var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
            result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return result[1] !== "00" ? result.join(",") : result[0];
        }
        return "0";
    };
    GameUtils.numberWithDot = function (number) {
        if (number) {
            var result = (number = parseFloat(number)).toFixed(2).toString().split(".");
            result[0] = result[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return result[1] !== "00" ? result.join(",") : result[0];
        }
        return "0";
    };
    GameUtils.formatMoneyWithRange = function (money, shortenStart) {
        if (shortenStart === void 0) { shortenStart = 100000000; }
        var rs = "";
        if (money >= shortenStart)
            rs = GameUtils.formatMoneyNumber(money);
        else
            rs = GameUtils.numberWithCommas(money);
        return rs;
    };
    GameUtils.roundNumber = function (num, roundMin) {
        if (roundMin === void 0) { roundMin = 1000; }
        var temp = parseInt((num / roundMin).toString());
        if (num % roundMin != 0)
            temp += 1;
        return temp * roundMin;
    };
    GameUtils.getRandomInt = function (max, min) {
        if (min === void 0) { min = 0; }
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };
    GameUtils.randomInsideCircle = function (R) {
        if (R === void 0) { R = 1; }
        var r = R * Math.sqrt(Math.random());
        var theta = Math.random() * 2 * Math.PI;
        var x = r * Math.cos(theta);
        var y = r * Math.sin(theta);
        return new cc.Vec2(x, y);
    };
    GameUtils.getRandomFloat = function (max) {
        return Math.random() * max;
    };
    GameUtils.playAnimOnce = function (skeleton, toAnimation, backAnimation, onCompleted) {
        if (onCompleted === void 0) { onCompleted = null; }
        if (skeleton == null || skeleton == undefined)
            return;
        // if (skeleton.getCurrent(0) != null && skeleton.getCurrent(0).animation.name == toAnimation) {
        //   return;
        // }
        skeleton.setCompleteListener(function (trackEntry) {
            if (backAnimation != "" && backAnimation != null)
                skeleton.setAnimation(0, backAnimation, true);
            if (trackEntry.animation && trackEntry.animation.name == toAnimation)
                onCompleted && onCompleted();
        });
        skeleton.setAnimation(0, toAnimation, false);
    };
    GameUtils.sum = function (array) {
        if (array.length <= 0)
            return 0;
        return array.reduce(function (total, val) { return total + val; });
    };
    GameUtils.setContentLabelAutoSize = function (label, content) {
        if (content.length > 30) {
            label.node.width = (30 * label.fontSize) / 2;
            label.overflow = cc.Label.Overflow.RESIZE_HEIGHT;
        }
        else {
            label.overflow = cc.Label.Overflow.NONE;
        }
        label.string = content;
    };
    GameUtils.FormatString = function (str) {
        var val = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            val[_i - 1] = arguments[_i];
        }
        for (var index = 0; index < val.length; index++) {
            str = str.replace("{" + index + "}", val[index].toString());
        }
        return str;
    };
    GameUtils.setPlayEffect = function (nodeEff) {
        if (!nodeEff)
            return;
        nodeEff.getComponentsInChildren(cc.ParticleSystem3D).forEach(function (x) {
            x.stop();
            x.play();
        });
        nodeEff.getComponentsInChildren(cc.Animation).forEach(function (x) {
            x.stop();
            x.play();
        });
        nodeEff.getComponentsInChildren(sp.Skeleton).forEach(function (x) {
            x.setAnimation(0, x.defaultAnimation, x.loop);
        });
    };
    GameUtils.setParentTemp = function (nameTempNode, node, parentOfTemp, zIndex) {
        if (nameTempNode === void 0) { nameTempNode = "Temp"; }
        if (zIndex === void 0) { zIndex = 90; }
        var temp = parentOfTemp.getChildByName(nameTempNode);
        if (!temp) {
            temp = new cc.Node(nameTempNode);
            parentOfTemp.addChild(temp);
            temp.zIndex = zIndex;
            parentOfTemp.sortAllChildren();
        }
        var widget = node.getComponent(cc.Widget);
        if (widget)
            widget.enabled = false;
        var startPos = GameUtils.convertToOtherNode(node.parent, temp, node.position);
        node.setParent(temp);
        node.position = startPos;
        node.zIndex = 0;
    };
    GameUtils.getParentTemp = function (nameTempNode, parentOfTemp, zIndex) {
        if (nameTempNode === void 0) { nameTempNode = "Temp"; }
        if (zIndex === void 0) { zIndex = 80; }
        var temp = parentOfTemp.getChildByName(nameTempNode);
        if (!temp) {
            temp = new cc.Node(nameTempNode);
            parentOfTemp.addChild(temp);
            temp.zIndex = zIndex;
        }
        return temp;
    };
    GameUtils.getTimeSince = function (timeStamp) {
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;
        var elapsed = Date.now() - timeStamp.toDate().getTime();
        cc.log("elapsed", elapsed);
        if (elapsed < msPerMinute) {
            return Math.round(elapsed / 1000) + " giây trước";
        }
        else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + " phút trước";
        }
        else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + " giờ trước";
        }
        else if (elapsed < msPerMonth) {
            return "khoảng " + Math.round(elapsed / msPerDay) + " ngày trước";
        }
        else if (elapsed < msPerYear) {
            return "khoảng " + Math.round(elapsed / msPerMonth) + " tháng trước";
        }
        else {
            return "khoảng " + Math.round(elapsed / msPerYear) + " năm trước";
        }
    };
    GameUtils.getKeyByValue = function (object, value) {
        var key = undefined;
        object.forEach(function (v, k) {
            if (value == v) {
                key = k;
            }
        });
        return key;
    };
    GameUtils.isPlatformIOS = function () {
        return cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD;
    };
    GameUtils.isBrowser = function () {
        return cc.sys.isBrowser;
    };
    GameUtils.isNative = function () {
        return cc.sys.isNative;
    };
    GameUtils.isAndroid = function () {
        return cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID;
    };
    GameUtils.isIOS = function () {
        return cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS;
    };
    GameUtils.isLogVersion = function () {
        return GameDefine_1.default.LOG_VERSION_ENABLE;
    };
    GameUtils.isNullOrUndefined = function (object) {
        return object == null || object == undefined;
    };
    GameUtils.convertLotteryNumberToString = function (lotteryNumber) {
        var lotteryNumberString = lotteryNumber.toString();
        var remainZeroNumber = 6 - lotteryNumberString.length;
        for (var i = 0; i < remainZeroNumber; i++) {
            lotteryNumberString = "0" + lotteryNumberString;
        }
        return lotteryNumberString;
    };
    GameUtils.isWebMobile = function () {
        return cc.sys.isBrowser && cc.sys.isMobile;
    };
    GameUtils.getDeviceId = function () {
        var id = cc.sys.localStorage.getItem(GameUtils.deviceIdKey);
        if (id == null || id == "") {
            var rawId = cc.sys.os +
                cc.sys.osMainVersion +
                cc.sys.language +
                cc.sys.languageCode +
                cc.sys.osVersion +
                cc.sys.isNative +
                cc.sys.windowPixelResolution.height +
                cc.sys.platform +
                cc.sys.windowPixelResolution.width +
                cc.sys.isMobile +
                cc.sys.now();
            id = btoa(rawId);
            cc.sys.localStorage.setItem(GameUtils.deviceIdKey, id);
        }
        GameUtils.deviceID = id;
    };
    GameUtils.getPlatformName = function () {
        if (GameUtils.isWebMobile()) {
            return "WEBM";
        }
        else if (cc.sys.isBrowser) {
            return "WEBPC";
        }
        else if (cc.sys.os == cc.sys.OS_ANDROID) {
            return "ANDROID";
        }
        else if (cc.sys.os == cc.sys.OS_IOS) {
            return "IOS";
        }
        else
            return "UNKNOWN";
    };
    GameUtils.qrToString = function (qrcode) {
        var rs = "";
        for (var row = 0; row < qrcode.getModuleCount(); row++) {
            for (var col = 0; col < qrcode.getModuleCount(); col++) {
                rs = rs + (qrcode.isDark(row, col) ? "1" : "0");
            }
        }
        return rs;
    };
    GameUtils.IS_DEV = false;
    GameUtils.count = 0;
    GameUtils.gameType = EnumGame_1.GameType.BauCua;
    GameUtils.gameTopic = topic_pb_1.Topic.TINA;
    GameUtils.isStatusPing = true;
    GameUtils.isMainScene = true;
    GameUtils.isFirstLoadGame = true;
    GameUtils.deviceName = "";
    GameUtils.deviceID = "";
    GameUtils.utmSource = "";
    GameUtils.deviceIdKey = "DEVICE_ID";
    return GameUtils;
}());
exports.default = GameUtils;

cc._RF.pop();