"use strict";
cc._RF.push(module, 'c0493/P1A5GR54lnExm4v/l', 'SicboDealerStateComp');
// Game/Sicbo_New/Scripts/Components/States/SicboDealerStateComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), State = _a.State, Status = _a.Status;
var SicboGameUtils_1 = require("../../RNGCommons/Utils/SicboGameUtils");
var SicboSkeletonUtils_1 = require("../../RNGCommons/Utils/SicboSkeletonUtils");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboDealerStateComp = /** @class */ (function (_super) {
    __extends(SicboDealerStateComp, _super);
    function SicboDealerStateComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.dealerSkeleton = null;
        _this.skins = ["default"];
        _this.idleArr = ["idle"];
        // private shakeAnimation = "shake";
        // private openAnimation = "open";
        _this.betAnimation = "bet";
        _this.status = Status.STATUS_UNSPECIFIED;
        return _this;
    }
    SicboDealerStateComp.prototype.onLoad = function () {
        this.nextIdleAnimation();
        this.setRandomSkin();
        // SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.openAnimation, false, 100);
    };
    SicboDealerStateComp.prototype.exitState = function (status) { };
    SicboDealerStateComp.prototype.updateState = function (state) { };
    SicboDealerStateComp.prototype.startState = function (state, totalDuration) {
        this.status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        switch (this.status) {
            // case Status.WAITING:                
            //     SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.shakeAnimation, false, elapseTime);
            //     SicboController.Instance.playSfx(SicboSound.SfxDealerShake);
            //     break;
            case Status.BETTING:
                SicboSkeletonUtils_1.default.setAnimation(this.dealerSkeleton, this.betAnimation, false, elapseTime);
                break;
            // case Status.SHOWING_RESULT: 
            //     SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.openAnimation, true);
            default:
                // SicboSkeletonUtils.setAnimation(this.dealerSkeleton, "Waiting Idle", false, elapseTime/1000);
                break;
        }
    };
    SicboDealerStateComp.prototype.update = function (dt) {
        if (this.isAnimationComplete() && this.status != Status.RESULTING)
            this.nextIdleAnimation();
    };
    SicboDealerStateComp.prototype.nextIdleAnimation = function () {
        var idx = SicboGameUtils_1.default.getRandomInt(this.idleArr.length, 0);
        SicboSkeletonUtils_1.default.setAnimation(this.dealerSkeleton, this.idleArr[idx]);
        //cc.log("Dealer Idle");
    };
    SicboDealerStateComp.prototype.isAnimationComplete = function () {
        var trackEntry = this.dealerSkeleton.getCurrent(0);
        // //cc.log("[track %s][isComplete %s]end.", trackEntry.trackIndex, trackEntry.isComplete());
        return trackEntry.isComplete();
    };
    SicboDealerStateComp.prototype.setRandomSkin = function () {
        var idx = SicboGameUtils_1.default.getRandomInt(this.skins.length, 0);
        this.dealerSkeleton.setSkin(this.skins[idx]);
    };
    // setMixAnimation (anim1, anim2, mixTime = 0.5) {
    //     this.dealerSkeleton.setMix(anim1, anim2, mixTime);
    //     this.dealerSkeleton.setMix(anim2, anim1, mixTime);
    // }
    SicboDealerStateComp.prototype.onHide = function () {
        this.nextIdleAnimation();
        this.setRandomSkin();
        // SicboSkeletonUtils.setAnimation(this.dealerSkeleton, this.openAnimation, false, 100);
    };
    __decorate([
        property(sp.Skeleton)
    ], SicboDealerStateComp.prototype, "dealerSkeleton", void 0);
    SicboDealerStateComp = __decorate([
        ccclass
    ], SicboDealerStateComp);
    return SicboDealerStateComp;
}(cc.Component));
exports.default = SicboDealerStateComp;

cc._RF.pop();