"use strict";
cc._RF.push(module, '0697aIKvhxHYZzn8G1dG+8R', 'SicboNotifyStateComp');
// Game/Sicbo_New/Scripts/Components/States/SicboNotifyStateComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), State = _a.State, Status = _a.Status;
var SicboSetting_1 = require("../../Setting/SicboSetting");
var SicboController_1 = require("../../Controllers/SicboController");
var SicboHelper_1 = require("../../Helpers/SicboHelper");
var SicboGameUtils_1 = require("../../RNGCommons/Utils/SicboGameUtils");
var SicboNotifyComp_1 = require("../SicboNotifyComp");
var SicboText_1 = require("../../Setting/SicboText");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboNotifyStateComp = /** @class */ (function (_super) {
    __extends(SicboNotifyStateComp, _super);
    function SicboNotifyStateComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.notifyComp = null;
        _this.elapseTime = 0;
        _this.totalDuration = 0;
        _this.isWaiting = false;
        return _this;
    }
    SicboNotifyStateComp.prototype.exitState = function (status) {
        if (status == Status.FINISHING)
            this.notifyComp.forceHideSmallNoti();
        this.isWaiting = false;
    };
    SicboNotifyStateComp.prototype.updateState = function (state) {
        if (this.isWaiting == false)
            return;
        this.elapseTime = state.getStageTime();
        var remainingTime = Math.max(Math.ceil((this.totalDuration - this.elapseTime) / 1000), 0);
        this.notifyComp.updateSmallNotiMessage(SicboGameUtils_1.default.FormatString(SicboText_1.default.waitingRemainingTimeMsg, remainingTime));
    };
    SicboNotifyStateComp.prototype.startState = function (state, totalDuration) {
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        this.elapseTime = elapseTime * 1000;
        this.totalDuration = totalDuration;
        switch (status) {
            case Status.FINISHING:
                if (SicboSetting_1.default.getStatusDuration(Status.FINISHING) <= 1 * 1000)
                    return;
                this.isWaiting = true;
                var remainingTime = Math.max(Math.floor((this.totalDuration - this.elapseTime) / 1000), 0);
                var showTime = Math.max(Math.min(totalDuration - 1, totalDuration - this.elapseTime - 1), 0);
                this.notifyComp.showSmallNoti(SicboGameUtils_1.default.FormatString(SicboText_1.default.waitingRemainingTimeMsg, remainingTime), .5, showTime, .5, true);
                SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxMessage);
                break;
            case Status.END_BET:
                if (SicboHelper_1.default.approximatelyEqual(this.elapseTime, 0, 100))
                    this.notifyComp.showStopBettingMessage();
                break;
            case Status.BETTING:
                if (SicboHelper_1.default.approximatelyEqual(this.elapseTime, 0, 100))
                    this.notifyComp.showBettingMessage();
                break;
            default:
                break;
        }
    };
    SicboNotifyStateComp.prototype.onHide = function () {
        this.notifyComp.forceHideLargeNoti();
        this.notifyComp.forceHideResultNoti();
        this.notifyComp.forceHideSmallNoti();
    };
    __decorate([
        property(SicboNotifyComp_1.default)
    ], SicboNotifyStateComp.prototype, "notifyComp", void 0);
    SicboNotifyStateComp = __decorate([
        ccclass
    ], SicboNotifyStateComp);
    return SicboNotifyStateComp;
}(cc.Component));
exports.default = SicboNotifyStateComp;

cc._RF.pop();