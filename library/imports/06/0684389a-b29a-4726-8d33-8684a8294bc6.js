"use strict";
cc._RF.push(module, '06843iasppHJo0zhoSoKUvG', 'SicboBaseDropDownItemComp');
// Game/Sicbo_New/Scripts/Components/DropDown/SicboBaseDropDownItemComp.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboBaseDropDownItemComp = /** @class */ (function (_super) {
    __extends(SicboBaseDropDownItemComp, _super);
    function SicboBaseDropDownItemComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = undefined;
        _this.sprite = undefined;
        _this.toggle = undefined;
        return _this;
    }
    __decorate([
        property(cc.Label)
    ], SicboBaseDropDownItemComp.prototype, "label", void 0);
    __decorate([
        property(cc.Sprite)
    ], SicboBaseDropDownItemComp.prototype, "sprite", void 0);
    __decorate([
        property(cc.Toggle)
    ], SicboBaseDropDownItemComp.prototype, "toggle", void 0);
    SicboBaseDropDownItemComp = __decorate([
        ccclass
    ], SicboBaseDropDownItemComp);
    return SicboBaseDropDownItemComp;
}(cc.Component));
exports.default = SicboBaseDropDownItemComp;

cc._RF.pop();