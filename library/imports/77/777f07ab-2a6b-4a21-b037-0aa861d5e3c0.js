"use strict";
cc._RF.push(module, '777f0erKmtKIbA3Cqhh1ePA', 'BenTauHandler');
// Game/Lobby/Network/BenTauHandler.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BenTauHandler = /** @class */ (function () {
    function BenTauHandler() {
        this.client = null;
        this._initComplete = null;
        this._timerIntervalUpdate = null;
        this._start = false;
        this.subscribes = new Map();
        this.msgsSend = new Map();
        this.keydefault = "keydefault";
        this.countReconnect = 0;
        this.MAX_COUNT_RECONNECT = 3;
    }
    BenTauHandler.getInstance = function () {
        if (!BenTauHandler.instance) {
            BenTauHandler.instance = new BenTauHandler();
            BenTauHandler.instance.handUpdateTimeOut();
        }
        return BenTauHandler.instance;
    };
    BenTauHandler.prototype.openLoading = function (callback) {
        UIManager_1.default.getInstance()
            .openPopupSystemV2(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem, false, null, null)
            .then(function () {
            callback();
        });
    };
    BenTauHandler.prototype.closeLoading = function () {
        UIManager_1.default.getInstance().closePopupSystem(LoadingPopupSystem_1.default, GameDefine_1.default.LoadingPopupSystem);
    };
    BenTauHandler.prototype.openRetry = function () {
        EventManager_1.default.fire(EventManager_1.default.ON_SHOW_RETRY_POPUP);
    };
    BenTauHandler.destroy = function () {
        //cc.log("destroy");
        var self = BenTauHandler.instance;
        if (!(self === null || self === void 0 ? void 0 : self.client))
            return;
        self.client.close(EnumGame_1.CodeSocket.FORCE_CLOSE, "");
        clearInterval(self._timerIntervalUpdate);
        BenTauHandler.instance._start = false;
        BenTauHandler.instance = null;
    };
    BenTauHandler.prototype.onSubscribe = function (topic, onReceive, key) {
        if (key === void 0) { key = ""; }
        if (key == "") {
            key = this.keydefault;
        }
        if (this.subscribes.has(topic)) {
            var e = this.subscribes.get(topic);
            e.set(key, onReceive);
            this.subscribes.set(topic, e);
        }
        else {
            var e = new Map();
            e.set(key, onReceive);
            this.subscribes.set(topic, e);
        }
    };
    BenTauHandler.prototype.onUnSubscribe = function (topic, key) {
        if (key === void 0) { key = ""; }
        if (key == "") {
            key = this.keydefault;
        }
        if (this.subscribes.has(topic)) {
            var e = this.subscribes.get(topic);
            if (e.has(key)) {
                e.delete(key);
                this.subscribes.set(topic, e);
            }
        }
    };
    BenTauHandler.prototype.init = function (onComplete) {
        var _this = this;
        if (onComplete === void 0) { onComplete = null; }
        //cc.log("init betau WS");
        var self = BenTauHandler.instance;
        self._initComplete = onComplete;
        this.openLoading(function () {
            //cc.log("Connecting to BenTau");
            if (!(self === null || self === void 0 ? void 0 : self.client)) {
                self.client = new WSClient_1.default();
            }
            self.client.open(ConnectDefine_1.default.addressBenTauWS, self.handleOpen.bind(_this), self.onMessage.bind(_this), self.onClose.bind(_this), self.onError.bind(_this), self.onReconnect.bind(_this));
        });
    };
    BenTauHandler.prototype.openNewWebsocket = function () {
        // console.log("openNewWebsocket");
        var self = BenTauHandler.instance;
        if (!(self === null || self === void 0 ? void 0 : self.client)) {
            self.client = new WSClient_1.default();
        }
        self.client.open(ConnectDefine_1.default.addressBenTauWS, self.handleReconnected.bind(this), self.onMessage.bind(this), self.onClose.bind(this), self.onError.bind(this), self.onReconnect.bind(this));
    };
    BenTauHandler.prototype.cleanConnection = function () { };
    BenTauHandler.prototype.doReconnect = function (onReconnectingFunc, failedReconnectFunc) {
        var _this = this;
        if (onReconnectingFunc === void 0) { onReconnectingFunc = null; }
        if (failedReconnectFunc === void 0) { failedReconnectFunc = null; }
        //cc.log("Reconnecting with api verifyTokenReconnect ");
        // onReconnectingFunc();
        this.countReconnect++;
        IdentityHandler_1.default.getInstance().verifyTokenReconnect(function () {
            BenTauHandler.instance.openNewWebsocket();
        }, function (msgError, code) {
            cc.log("msgError = " + msgError);
            cc.log("code = " + code);
            //cc.log("countReconnect = " + this.countReconnect + " " + this.MAX_COUNT_RECONNECT);
            if (code == code_pb_1.Code.UNKNOWN) {
                // failedReconnectFunc(); // reconnect lại
                if (_this.countReconnect < _this.MAX_COUNT_RECONNECT) {
                    _this.doReconnect();
                }
                else {
                    _this.countReconnect = 0;
                    _this.closeLoading();
                    _this.openRetry();
                }
            }
            else {
                var messError = PortalText_1.default.POR_MESS_ACCOUNT_LOGIN_OTHER_DEVICE;
                EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, EnumGame_1.CodeSocket.KOTIC, false, messError);
            }
        });
    };
    BenTauHandler.prototype.send = function (topic, channelId, payload, reply, onError, loadingBg, loading) {
        var _this = this;
        var _a;
        if (reply === void 0) { reply = null; }
        if (onError === void 0) { onError = null; }
        if (loadingBg === void 0) { loadingBg = true; }
        if (loading === void 0) { loading = true; }
        if (!((_a = BenTauHandler.instance) === null || _a === void 0 ? void 0 : _a.client))
            return null;
        loading = reply != null && loading;
        var request = new bentau_pb_1.BenTauMessage();
        request.setTopic(topic);
        request.setChannelId(channelId);
        var msgId = BenTauHandler.instance.getUniqueId();
        request.setMsgId(msgId);
        // Put  to BenTauMessage
        request.setPayload(payload);
        var sendRequest = function () {
            ////cc.log("sendRequest.........", msgId);
            BenTauHandler.instance.client.send(request.serializeBinary());
        };
        var msg = new build_1.RequestMessage();
        msg.msgId = msgId;
        msg.request = sendRequest;
        msg.response = reply;
        msg.onError = onError;
        msg.onTimeOut = function (msgIdTimeOut) { return _this.onMsgTimeOut(msgIdTimeOut); };
        msg.loading = loading;
        msg.loadingBg = loadingBg;
        this.sendRequest(msg);
        return msgId;
    };
    BenTauHandler.prototype.sendRequest = function (msg) {
        //chỉ những msg nào chờ response mới cache lại
        if (msg.response != null)
            this.msgsSend.set(msg.msgId, msg);
        if (msg.loading) {
            this.openLoading(function () {
                msg.request();
            });
        }
        else {
            msg.request();
        }
    };
    BenTauHandler.prototype.onReceiveMsgReply = function (msgReply) {
        var status = msgReply.getStatus();
        if (status) {
            var code = status.getCode();
            var err = status.getMessage();
            var msgId = msgReply.getMsgId();
            var msgRequest = null;
            var self = BenTauHandler.getInstance();
            if (self.msgsSend.has(msgId)) {
                msgRequest = self.msgsSend.get(msgId);
                self.msgsSend.delete(msgId);
            }
            if (msgRequest) {
                if (msgRequest.loading) {
                    self.closeLoading();
                }
                switch (code) {
                    case code_pb_1.Code.OK:
                        if (msgRequest.response)
                            msgRequest.response(msgReply);
                        break;
                    default:
                        // request fail
                        if (msgRequest.onError)
                            msgRequest.onError(err);
                        break;
                }
            }
        }
    };
    BenTauHandler.prototype.handUpdateTimeOut = function () {
        var _this = this;
        var dt = 500;
        BenTauHandler.instance._timerIntervalUpdate = setInterval(function () { return _this.updateTimeOut(dt); }, dt);
    };
    BenTauHandler.prototype.updateTimeOut = function (dt) {
        var _a;
        if (!((_a = BenTauHandler.instance) === null || _a === void 0 ? void 0 : _a._start))
            return;
        this.msgsSend.forEach(function (msg) {
            msg.update(dt);
        });
        StabilizeConnection_1.default.update(dt);
    };
    BenTauHandler.prototype.onMsgTimeOut = function (msgId) {
        if (this.msgsSend.has(msgId)) {
            var msgTimeOut = this.msgsSend.get(msgId);
            this.msgsSend.delete(msgId);
            if (msgTimeOut.loading) {
                this.closeLoading();
            }
            if (msgTimeOut.onError) {
                msgTimeOut.onError("Request timeout", code_pb_1.Code.UNKNOWN);
            }
        }
    };
    BenTauHandler.prototype.getUniqueId = function () {
        var d = new Date().getTime(); //Timestamp
        var d2 = (performance && performance.now && performance.now() * 1000) || 0; //Time in microseconds since page-load or 0 if unsupported
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            var r = Math.random() * 16; //random number between 0 and 16
            if (d > 0) {
                //Use timestamp until depleted
                r = (d + r) % 16 | 0;
                d = Math.floor(d / 16);
            }
            else {
                //Use microseconds since page-load if supported
                r = (d2 + r) % 16 | 0;
                d2 = Math.floor(d2 / 16);
            }
            return (c === "x" ? r : (r & 0x3) | 0x8).toString(16);
        });
    };
    BenTauHandler.prototype.handleOpen = function () {
        var _a;
        if (!((_a = BenTauHandler.instance) === null || _a === void 0 ? void 0 : _a.client))
            return;
        this.closeLoading();
        if (BenTauHandler.instance._initComplete)
            BenTauHandler.instance._initComplete();
        BenTauHandler.instance._start = true;
        StabilizeConnection_1.default.send();
    };
    BenTauHandler.prototype.handleReconnected = function () {
        var _a;
        if (!((_a = BenTauHandler.instance) === null || _a === void 0 ? void 0 : _a.client))
            return;
        //cc.log("Reconnected to BenTau");
        UIManager_1.default.getInstance().closeAllPopup(false);
        EventManager_1.default.fire(EventManager_1.default.ON_RECONNECTED);
        //reconnect event for card games
        cc.systemEvent.emit("ON_RECONNECTED");
        EventBusManager_1.default.onFireReconnected();
        BenTauHandler.instance._start = true;
        StabilizeConnection_1.default.send();
    };
    BenTauHandler.prototype.onMessage = function (msg) {
        var _a;
        if (!((_a = BenTauHandler.instance) === null || _a === void 0 ? void 0 : _a.client))
            return;
        var topic = msg.getTopic();
        StabilizeConnection_1.default.setTimeOutMsg();
        if (this.subscribes.has(topic)) {
            var e = this.subscribes.get(topic);
            e.forEach(function (f, key) {
                f(msg);
            });
            this.onReceiveMsgReply(msg);
        }
    };
    BenTauHandler.prototype.onError = function (errCode, err) {
        // if (!BenTauHandler.instance?.client) return;
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, EnumGame_1.CodeSocket.FORCE_CLOSE);
    };
    BenTauHandler.prototype.onClose = function (code, reason) {
        //cc.log("Bentau ws onClose " + code + " " + reason);
        EventManager_1.default.fire(EventManager_1.default.ON_DISCONNECT, code);
    };
    BenTauHandler.prototype.onReconnect = function (code, reason) {
        //cc.log("Bentau ws onReconnect " + code + " " + reason);
        BenTauHandler.instance.doReconnect();
    };
    return BenTauHandler;
}());
exports.default = BenTauHandler;
var ConnectDefine_1 = require("../Define/ConnectDefine");
var UIManager_1 = require("../Manager/UIManager");
var GameDefine_1 = require("../Define/GameDefine");
var LoadingPopupSystem_1 = require("../Popup/LoadingPopupSystem");
var WSClient_1 = require("../Network/WebSocketClient/WSClient");
var StabilizeConnection_1 = require("./StabilizeConnection");
var EventManager_1 = require("../Event/EventManager");
var EnumGame_1 = require("../Utils/EnumGame");
var IdentityHandler_1 = require("./IdentityHandler");
var bentau_pb_1 = require("@gameloot/bentau/bentau_pb");
var code_pb_1 = require("@marketplace/rpc/code_pb");
var build_1 = require("@gameloot/client-base-handler/build");
var EventBusManager_1 = require("../Event/EventBusManager");
var PortalText_1 = require("../Utils/PortalText");

cc._RF.pop();