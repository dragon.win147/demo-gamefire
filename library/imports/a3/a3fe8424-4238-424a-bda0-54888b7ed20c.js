"use strict";
cc._RF.push(module, 'a3fe8QkQjhCSr2gVIiLftIM', 'SicboResultStateComp');
// Game/Sicbo_New/Scripts/Components/States/SicboResultStateComp.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboModuleAdapter_1 = require("../../../../../SicboModuleAdapter");
var _a = SicboModuleAdapter_1.default.getAllRefs(), GetChannelHistoryReply = _a.GetChannelHistoryReply, Door = _a.Door, State = _a.State, Result = _a.Result, Status = _a.Status, ChannelHistoryMetadata = _a.ChannelHistoryMetadata;
var SicboSetting_1 = require("../../Setting/SicboSetting");
var SicboDiceConfig_1 = require("../../Configs/SicboDiceConfig");
var SicboController_1 = require("../../Controllers/SicboController");
var SicboHelper_1 = require("../../Helpers/SicboHelper");
var SicboBetComp_1 = require("../SicboBetComp");
var SicboLatestResultComp_1 = require("../SicboLatestResultComp");
var SicboNotifyComp_1 = require("../SicboNotifyComp");
var SicboSlotWinMoneyComp_1 = require("../SicboSlotWinMoneyComp");
var SicboUIComp_1 = require("../SicboUIComp");
var SicboPortalAdapter_1 = require("../../SicboPortalAdapter");
var SicboGameUtils_1 = require("../../RNGCommons/Utils/SicboGameUtils");
var SicboSkeletonUtils_1 = require("../../RNGCommons/Utils/SicboSkeletonUtils");
var _b = cc._decorator, ccclass = _b.ccclass, property = _b.property;
var SicboResultStateComp = /** @class */ (function (_super) {
    __extends(SicboResultStateComp, _super);
    function SicboResultStateComp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.betCompNode = null;
        _this.betComp = null;
        _this.winFxShinnyNode = null;
        _this.chenAnimation = null;
        _this.chenSmallRoot = null;
        _this.chenZoomInRoot = null;
        _this.diceSprites = [];
        _this.latestResultComp = null;
        _this.dealerSkeleton = null;
        _this.slotWinMoneySmallPrefab = null;
        _this.slotWinMoneyLargePrefab = null;
        _this.notifyComp = null;
        _this.winFxSkeletons = [];
        _this.winFxShinnySkeletons = [];
        _this.isWinFxPlaying = false;
        _this.winFxTween = null;
        _this.wonDoors = [];
        _this.wonDoorsFiltered = [];
        _this.items = [];
        _this.chenOpenAnim = "ChenOpen";
        _this.chenIdleAnim = "ChenIdle";
        // private chenCloseAnim = "ChenClose";
        _this.chenRollDiceAnim = "chenRollDice";
        //NOTE startTime tinh tu khi state bat dau
        //ANCHOR Status.RESULTING start time
        _this.chenRollDiceStartTime = 0.1;
        _this.chenRollDiceSfxStartTime = _this.chenRollDiceStartTime + 0.2;
        _this.chenRollDiceSfxStopTime = _this.chenRollDiceSfxStartTime + 0.8;
        _this.chenOpenStartTime = 0.5; //NOTE 1;
        _this.chenOpenSfxStartTime = _this.chenOpenStartTime + 1.5;
        // private chenCloseStartTime = 0;
        _this.chenIdleStartTime = _this.chenOpenStartTime + 5;
        _this.dicesFlyToLatestResultStartTime = _this.chenIdleStartTime + 0.7;
        _this.doorWinFxShinnyStartTime = _this.chenIdleStartTime + 0.5;
        _this.doorWinFxStartTime = _this.chenIdleStartTime + +2.1;
        //ANCHOR Status.PAYING start time
        _this.chipLoseStartTime = 0;
        _this.chipStackStartTime = 1;
        _this.returnChipStartTime = 3 - 1;
        _this.payingStartTime = 6 - 1;
        _this.isPayingPlaying = false;
        //other user group key
        _this.otherGroupKey = "otherGroupKey";
        _this.winMoneyAtSlots = [];
        return _this;
    }
    SicboResultStateComp.prototype.onLoad = function () {
        this.betComp = this.betCompNode.getComponent(SicboBetComp_1.default);
        this.winFxShinnySkeletons = this.winFxShinnyNode.getComponentsInChildren(sp.Skeleton);
        this.winFxSkeletons = this.winFxShinnySkeletons;
        this.hideWinFx();
    };
    //SECTION ISicboState
    SicboResultStateComp.prototype.exitState = function (status) {
        this.isPayingPlaying = false;
    };
    SicboResultStateComp.prototype.updateState = function (state) {
        if (this.isPayingPlaying)
            return;
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        if (status == Status.PAYING && elapseTime >= 0.1) {
            //NOTE server delay 0.1s for generate result
            this.setResult(state.getResult());
            //cc.log("updateState", this.result.toObject());
            this.playPayingAnimation(elapseTime);
            this.isPayingPlaying = true;
        }
    };
    SicboResultStateComp.prototype.setResult = function (result) {
        if (SicboGameUtils_1.default.isNullOrUndefined(result))
            return;
        //cc.log("RESULT1", result.hasPlayerBetTx(), result.toObject());
        this.result = result;
        this.items = result.getItemsList().sort();
        this.wonDoors = result.getDoorsList();
        this.wonDoorsFiltered = this.wonDoors.filter(function (elem, index, self) {
            return index === self.indexOf(elem);
        });
        this.setDiceResult();
    };
    SicboResultStateComp.prototype.startState = function (state, totalDuration) {
        this.curSessionId = state.getTableSessionId();
        var status = state.getStatus();
        var elapseTime = state.getStageTime() / 1000;
        this.setResult(state.getResult());
        //console.error(SicboHelper.StatusToString(status), elapseTime);
        switch (status) {
            case Status.WAITING:
                SicboHelper_1.default.changeParent(this.chenAnimation.node, this.chenZoomInRoot);
                this.chenRollDice(elapseTime);
                this.chenRollDiceSfx(elapseTime);
                this.chenRollDiceSfx(elapseTime, false);
                this.hideWinFx();
                this.latestResultComp.playFx(false);
                this.destroyWinMoneyAtDoor();
                break;
            case Status.RESULTING:
                this.setDiceResult();
                SicboHelper_1.default.changeParent(this.chenAnimation.node, this.chenZoomInRoot);
                this.chenOpen(elapseTime);
                this.chenOpenSfx(elapseTime);
                this.chenIdle(elapseTime);
                this.dicesFlyToLatestResult(elapseTime);
                this.playWinFxShinny(elapseTime);
                this.playWinFx(elapseTime);
                break;
            case Status.PAYING:
                // this.destroyWinMoneyAtDoor();
                // this.statisticComp.showHideSoiCauNewEffect(true);
                SicboHelper_1.default.changeParent(this.chenAnimation.node, this.chenSmallRoot);
                this.chenIdle(100);
                ////console.log("BCF PAYING WinnersList", elapseTime, state.getResult()?.getWinnersList());
                // //console.log("BCF PAYING Receipt", elapseTime, state.getResult()?.getPlayerBetTx());
                this.playWinFx(100);
                this.latestResultComp.playFx();
                // this.playChipInDoorAnimation(elapseTime);
                this.playLoseAnimation(elapseTime);
                this.playChipStackAnimation(elapseTime);
                this.dealerReturnWinChip(elapseTime);
                // this.playPayingAnimation(elapseTime);  //NOTE server delay 0.1s for generate result
                break;
            // case Status.FINISHING:
            //   return;//FIXME
            // this.chenClose(elapseTime);
            // this.statisticComp.showHideSoiCauNewEffect(false);
            default:
                this.hideWinFx();
                this.latestResultComp.playFx(false);
        }
    };
    //!SECTION
    //SECTION Chen
    SicboResultStateComp.prototype.setDiceResult = function () {
        //cc.log("DICES", this.items);
        for (var i = 0; i < this.items.length; i++) {
            var spriteName = SicboHelper_1.default.convertItemToDiceSpriteName(this.items[i]);
            var self = this;
            self.diceSprites[i].spriteFrame = SicboDiceConfig_1.default.Instance.getDice(spriteName);
        }
    };
    SicboResultStateComp.prototype.chenRollDice = function (elapseTime) {
        var _this = this;
        if (elapseTime === void 0) { elapseTime = 0; }
        if (elapseTime > this.chenRollDiceStartTime) {
            this.chenAnimation.play(this.chenRollDiceAnim, elapseTime - this.chenRollDiceStartTime);
            // this.playDealerOpenAnimation();
        }
        else {
            cc.tween(this.node)
                .delay(this.chenRollDiceStartTime - elapseTime)
                .call(function () {
                // this.playDealerOpenAnimation();
                _this.chenAnimation.play(_this.chenRollDiceAnim);
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.chenRollDiceSfx = function (elapseTime, isPlay) {
        if (elapseTime === void 0) { elapseTime = 0; }
        if (isPlay === void 0) { isPlay = true; }
        var time = isPlay ? this.chenRollDiceSfxStartTime : this.chenRollDiceSfxStopTime;
        if (elapseTime > time) {
            if (isPlay)
                SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxDealerShake);
            else
                SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxDealerShake);
        }
        else {
            cc.tween(this.node)
                .delay(time - elapseTime)
                .call(function () {
                if (isPlay)
                    SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxDealerShake);
                else
                    SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxDealerShake);
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.chenOpen = function (elapseTime) {
        var _this = this;
        if (elapseTime === void 0) { elapseTime = 0; }
        if (elapseTime > this.chenOpenStartTime) {
            this.chenAnimation.play(this.chenOpenAnim, elapseTime - this.chenOpenStartTime);
            // this.playDealerOpenAnimation();
        }
        else {
            cc.tween(this.node)
                .delay(this.chenOpenStartTime - elapseTime)
                .call(function () {
                // this.playDealerOpenAnimation();
                _this.chenAnimation.play(_this.chenOpenAnim);
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.chenOpenSfx = function (elapseTime) {
        if (elapseTime === void 0) { elapseTime = 0; }
        var self = this;
        if (elapseTime > this.chenOpenSfxStartTime) {
        }
        else {
            cc.tween(self.node)
                .delay(self.chenOpenSfxStartTime - elapseTime)
                .call(function () {
                SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxResult);
                self.notifyComp.showResultNoti(self.getResultText(self.result.getItemsList()));
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.getResultText = function (items) {
        var total = SicboHelper_1.default.getResultTotalValue(items).toString();
        var typeResult = SicboHelper_1.default.getResultType(items);
        var typeName = SicboHelper_1.default.getResultTypeName(typeResult);
        return total + " điểm - " + typeName;
    };
    // private playDealerOpenAnimation(){
    //   SicboSkeletonUtils.setAnimation(this.dealerSkeleton, "open", true);
    //   //cc.log("Dealer Open");
    // }
    SicboResultStateComp.prototype.chenIdle = function (elapseTime) {
        var _this = this;
        if (elapseTime === void 0) { elapseTime = 0; }
        if (elapseTime > this.chenIdleStartTime) {
            this.chenAnimation.play(this.chenIdleAnim, elapseTime - this.chenIdleStartTime);
        }
        else
            cc.tween(this.node)
                .delay(this.chenIdleStartTime - elapseTime)
                .call(function () {
                _this.chenAnimation.play(_this.chenIdleAnim);
                // SoundManager.getInstance().playEffLocalName(SoundDefine.Sicbo_LidClose, SoundDefine.Sicbo);
            })
                .start();
    };
    // chenClose(elapseTime: number = 0){
    //   if(elapseTime>this.chenCloseStartTime)
    //     this.chenAnimation.play(this.chenCloseAnim, elapseTime - this.chenCloseStartTime);
    //   else
    //     cc.tween(this.node)
    //       .delay(this.chenCloseStartTime - elapseTime)
    //       .call(
    //         ()=>{
    //           this.chenAnimation.play(this.chenCloseAnim);
    //           // SoundManager.getInstance().playEffLocalName(SoundDefine.Sicbo_LidClose, SoundDefine.Sicbo);
    //         }
    //       ).start();
    // }
    SicboResultStateComp.prototype.dicesFlyToLatestResult = function (elapseTime) {
        if (elapseTime === void 0) { elapseTime = 0; }
        if (elapseTime > this.dicesFlyToLatestResultStartTime) {
            // this.statisticComp.reloadLatestRecordWithAnimation(this.result.getItemsList());
            // this.statisticComp.showHideSoiCauNewEffect(true, .5);
            // SicboUIComp.Instance.getHistory(true);
            // SicboController.Instance.playSfx(SicboSound.SfxMessage);
        }
        else {
            var self_1 = this;
            cc.tween(self_1.node)
                .delay(self_1.dicesFlyToLatestResultStartTime - elapseTime)
                .call(function () {
                //cc.log("dicesFlyToLatestResultStartTime", self.dicesFlyToLatestResultStartTime);
                var diceFlyDuration = 0.5;
                var _loop_1 = function (i) {
                    var diceOrigin = self_1.diceSprites[i].node;
                    var dice = cc.instantiate(diceOrigin);
                    dice.setParent(diceOrigin.parent);
                    dice.setPosition(diceOrigin.getPosition());
                    dice.scale = 0.165;
                    SicboHelper_1.default.changeParent(dice, self_1.chenZoomInRoot);
                    var targetPos = self_1.latestResultComp.items[i].node.getPosition();
                    targetPos = SicboGameUtils_1.default.convertToOtherNode2(self_1.latestResultComp.items[i].node, self_1.chenZoomInRoot);
                    cc.tween(dice)
                        .to(diceFlyDuration, {
                        x: targetPos.x,
                        y: targetPos.y,
                        scale: 0.2,
                    })
                        .call(function () {
                        dice.destroy();
                    })
                        .start();
                };
                for (var i = 0; i < self_1.diceSprites.length; i++) {
                    _loop_1(i);
                }
                cc.tween(self_1.node)
                    .delay(diceFlyDuration)
                    .call(function () {
                    self_1.latestResultComp.setData(self_1.result.getItemsList());
                    self_1.latestResultComp.playFx();
                    // self.statisticComp.showHideSoiCauNewEffect(true, .5);
                    SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxMessage);
                    // SicboUIComp.Instance.getHistory(true);
                })
                    .start();
            })
                .start();
        }
    };
    //!SECTION
    //SECTION Door WinFX
    SicboResultStateComp.prototype.playWinFx = function (elapseTime) {
        var _this = this;
        var _a;
        if (elapseTime === void 0) { elapseTime = 0; }
        if (this.wonDoors.length <= 0)
            return;
        if (this.isWinFxPlaying)
            return;
        if (elapseTime > this.doorWinFxStartTime) {
            this.winFx();
        }
        else {
            var self = this;
            (_a = self.winFxTween) === null || _a === void 0 ? void 0 : _a.stop();
            self.winFxTween = cc
                .tween(this.winFxShinnyNode)
                .delay(this.doorWinFxStartTime - elapseTime)
                .call(function () {
                _this.winFx();
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.winFx = function () {
        this.isWinFxPlaying = true;
        for (var index = 0; index < this.winFxSkeletons.length; index++) {
            if (this.wonDoorsFiltered[index] != null) {
                SicboSkeletonUtils_1.default.setAnimation(this.winFxSkeletons[index], this.wonDoorsFiltered[index].toString() + "b", true);
                this.winFxSkeletons[index].node.active = true;
            }
            else {
                this.winFxSkeletons[index].node.active = false;
            }
        }
    };
    SicboResultStateComp.prototype.playWinFxShinny = function (elapseTime) {
        var self = this;
        if (elapseTime > self.doorWinFxShinnyStartTime) {
            self.winFxShinny(elapseTime - self.doorWinFxShinnyStartTime);
        }
        else {
            cc.tween(this.node)
                .delay(this.doorWinFxShinnyStartTime - elapseTime)
                .call(function () {
                self.winFxShinny();
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.winFxShinny = function (startTime) {
        if (startTime === void 0) { startTime = 0; }
        var self = this;
        for (var index = 0; index < self.winFxShinnySkeletons.length; index++) {
            if (self.wonDoorsFiltered[index] != null) {
                SicboSkeletonUtils_1.default.setAnimation(self.winFxShinnySkeletons[index], this.wonDoorsFiltered[index].toString(), false, startTime);
                self.winFxShinnySkeletons[index].node.active = true;
            }
            else {
                self.winFxShinnySkeletons[index].node.active = false;
            }
        }
    };
    SicboResultStateComp.prototype.hideWinFx = function () {
        var _a;
        (_a = this.winFxTween) === null || _a === void 0 ? void 0 : _a.stop();
        cc.Tween.stopAllByTarget(this.winFxShinnyNode);
        for (var index = 0; index < this.winFxSkeletons.length; index++) {
            this.winFxSkeletons[index].node.active = false;
        }
        for (var index = 0; index < this.winFxShinnySkeletons.length; index++) {
            this.winFxShinnySkeletons[index].node.active = false;
        }
        this.isWinFxPlaying = false;
        this.wonDoors = [];
        this.winMoneyAtSlots.forEach(function (element) {
            element.hide();
        });
    };
    //!SECTION
    //SECTION other betComp FX
    //ANCHOR playLoseAnimation
    SicboResultStateComp.prototype.playLoseAnimation = function (elapseTime) {
        var self = this;
        self.elapseTimeCheck(elapseTime, self.chipLoseStartTime, function () {
            self.betComp.playLoseAnimation(self.wonDoorsFiltered, 0);
        }, function () {
            var duration = 0.5;
            self.betComp.playLoseAnimation(self.wonDoorsFiltered, duration);
        });
    };
    SicboResultStateComp.prototype.playChipStackAnimation = function (elapseTime) {
        var self = this;
        self.elapseTimeCheck(elapseTime, self.chipStackStartTime, function () {
            self.betComp.playChipStackAnimation(self.wonDoorsFiltered, 0);
        }, function () {
            var duration = 0.5;
            self.betComp.playChipStackAnimation(self.wonDoorsFiltered, duration);
        });
    };
    //ANCHOR dealerReturnWinChip
    SicboResultStateComp.prototype.dealerReturnWinChip = function (elapseTime) {
        var _this = this;
        var userWinAmount = this.getMyUserWinAmount();
        ////console.log("BCF userWinAmount", userWinAmount);
        var self = this;
        var delayShowInMoney = 0;
        if (elapseTime > this.returnChipStartTime) {
            this.betComp.dealerReturnFullStackChip(this.wonDoorsFiltered, 0);
            delayShowInMoney = 0;
        }
        else {
            cc.tween(this.node)
                .delay(this.returnChipStartTime - elapseTime)
                .call(function () {
                delayShowInMoney = self.betComp.dealerReturnWinChip(self.wonDoorsFiltered) + 0.5;
                cc.tween(_this.node)
                    .delay(delayShowInMoney)
                    .call(function () {
                    userWinAmount.forEach(function (amount, door) {
                        self.showWinMoneyAtDoor(amount, door, elapseTime);
                    });
                    if (userWinAmount.size > 0)
                        SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxWin);
                })
                    .start();
            })
                .start();
        }
    };
    //ANCHOR playPayingAnimation
    SicboResultStateComp.prototype.playPayingAnimation = function (elapseTime) {
        var _this = this;
        //cc.log("SICBO playPayingAnimation", this.result.toObject());
        if (elapseTime > this.payingStartTime) {
            //NOTE k play animation cong tien o avatar khi user vao luc co result, do tien da duoc update o server
            this.result.getWinnersList().forEach(function (record) {
                var user = SicboUIComp_1.default.Instance.findUserComp(record.getUserId());
                var payingMoney = record.getAmount();
                _this.betComp.playPayingAnimation(record.getBet().getDoor(), user.node, payingMoney, 0);
            });
        }
        else {
            var self_2 = this;
            cc.tween(this.node)
                .delay(this.payingStartTime - elapseTime)
                .call(function () {
                self_2.result.getWinnersList().forEach(function (record) {
                    var user = SicboUIComp_1.default.Instance.findUserComp(record.getUserId());
                    self_2.betComp.playPayingAnimation(record.getBet().getDoor(), user.node, record.getAmount());
                });
                var delayShowAvatarWinMoney = 1.05;
                var payingMoney = self_2.result.hasPlayerBetTx() ? self_2.result.getPlayerBetTx().getAmount() : 0; //from chip
                var transaction = self_2.result.hasPlayerBetTx() ? self_2.result.getPlayerBetTx().getLastTxId() : 0;
                if (self_2.result.hasLotteryWinner() && SicboPortalAdapter_1.default.getInstance().getMyUserId() == self_2.result.getLotteryWinner().getUserId()) {
                    //from jackpot
                    payingMoney += self_2.result.getLotteryWinner().getAmount();
                    var jackpotTransaction = self_2.result.hasPlayerJackpotTx() ? self_2.result.getPlayerJackpotTx().getLastTxId() : 0;
                    transaction = Math.max(transaction, jackpotTransaction);
                }
                self_2.payMoneyForMyUser(payingMoney, transaction, delayShowAvatarWinMoney);
                var wonAmountFiltered = self_2.filterWinAmount(self_2.result.getWinnersList(), self_2.result.getLotteryWinner());
                wonAmountFiltered.forEach(function (amount, userId) {
                    var user = SicboUIComp_1.default.Instance.findUserComp(userId);
                    if (self_2.result.hasLotteryWinner() && userId == self_2.result.getLotteryWinner().getUserId())
                        //from jackpot
                        amount += self_2.result.getLotteryWinner().getAmount();
                    cc.tween(_this.node)
                        .delay(delayShowAvatarWinMoney)
                        .call(function () {
                        user.showWinMoney(amount);
                    })
                        .start();
                });
            })
                .start();
        }
    };
    //ANCHOR filterWinAmount
    SicboResultStateComp.prototype.filterWinAmount = function (records, jackpotWinner) {
        var _this = this;
        var result = new Map();
        var myUserId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
        if (jackpotWinner != null) {
            var jackpotUserId = jackpotWinner.getUserId();
            if (myUserId != jackpotUserId) {
                if (!result.has(jackpotUserId))
                    result.set(jackpotUserId, 0);
            }
        }
        records.forEach(function (record) {
            var userId = null;
            if (record.getOthers())
                userId = _this.otherGroupKey;
            else {
                userId = record.getUserId();
            }
            if (userId != myUserId) {
                if (!result.has(userId))
                    result.set(userId, 0);
                result.set(userId, result.get(userId) + record.getAmount());
            }
        });
        return result;
    };
    SicboResultStateComp.prototype.elapseTimeCheck = function (elapseTime, startTime, onPassedElapseTime, onNOTPassElapseTime) {
        if (elapseTime > startTime) {
            if (SicboHelper_1.default.approximatelyEqual(elapseTime - startTime, 0, 0.1)) {
                onNOTPassElapseTime && onNOTPassElapseTime();
            }
            else {
                onPassedElapseTime && onPassedElapseTime();
            }
        }
        else {
            cc.tween(this.node)
                .delay(Math.max(startTime - elapseTime, 0))
                .call(function () {
                onNOTPassElapseTime && onNOTPassElapseTime();
            })
                .start();
        }
    };
    SicboResultStateComp.prototype.payMoneyForMyUser = function (amount, transaction, delay) {
        if (delay === void 0) { delay = 0; }
        if (amount == 0)
            return;
        var user = SicboUIComp_1.default.Instance.myUserComp();
        cc.tween(this.node)
            .delay(delay)
            .call(function () {
            SicboController_1.default.Instance.addMyUserMoney(amount, transaction);
            //cc.log("SICBO RECEIPT", amount);
            user.showWinMoney(amount);
            SicboController_1.default.Instance.playSfx(SicboSetting_1.SicboSound.SfxTotalWin);
        })
            .start();
    };
    //ANCHOR showWinMoneyAtDoor
    SicboResultStateComp.prototype.getMyUserWinAmount = function () {
        var winnerList = this.result.getWinnersList();
        var rs = new Map();
        var userId = SicboPortalAdapter_1.default.getInstance().getMyUserId();
        winnerList.forEach(function (record) {
            if (record.getUserId() == userId) {
                var door = record.getBet().getDoor();
                if (rs.has(door)) {
                    rs.set(door, record.getAmount() + rs.get(door));
                }
                else {
                    rs.set(door, record.getAmount());
                }
            }
        });
        return rs;
    };
    SicboResultStateComp.prototype.showWinMoneyAtDoor = function (money, door, elapseTime) {
        if (elapseTime === void 0) { elapseTime = 0; }
        var d = this.betComp.getDoor(door);
        var prefab = this.slotWinMoneySmallPrefab;
        switch (door) {
            case Door.ANY_TRIPLE:
            case Door.BIG:
            case Door.SMALL:
            case Door.ONE_SINGLE:
            case Door.TWO_TRIPLE:
            case Door.THREE_TRIPLE:
            case Door.FOUR_TRIPLE:
            case Door.FIVE_TRIPLE:
            case Door.SIX_TRIPLE:
            case Door.ONE_SINGLE:
            case Door.TWO_SINGLE:
            case Door.THREE_SINGLE:
            case Door.FOUR_SINGLE:
            case Door.FIVE_SINGLE:
            case Door.SIX_SINGLE:
                prefab = this.slotWinMoneyLargePrefab;
                break;
        }
        var comp = SicboGameUtils_1.default.createItemFromPrefab(SicboSlotWinMoneyComp_1.default, prefab, d.node);
        comp.show(money, elapseTime);
        this.winMoneyAtSlots.push(comp);
    };
    SicboResultStateComp.prototype.destroyWinMoneyAtDoor = function () {
        this.winMoneyAtSlots.forEach(function (element) {
            var _a;
            (_a = element.node) === null || _a === void 0 ? void 0 : _a.destroy();
        });
        this.winMoneyAtSlots.length = 0;
    };
    //!SECTION
    SicboResultStateComp.prototype.updateResultFistTime = function (records) {
        if (SicboGameUtils_1.default.isNullOrUndefined(records)) {
            this.latestResultComp.active = false;
            return;
        }
        var lastRecord = records[0];
        // if(this.updateLastResult == false)
        //   lastRecord = records[1];
        if (SicboGameUtils_1.default.isNullOrUndefined(lastRecord)) {
            this.latestResultComp.active = false;
            return;
        }
        this.latestResultComp.active = true;
        var data = ChannelHistoryMetadata.deserializeBinary(lastRecord.getMetadata_asU8());
        var items = data.getItemsList().sort();
        this.latestResultComp.setData(items, false);
    };
    SicboResultStateComp.prototype.onDestroy = function () {
        cc.Tween.stopAllByTarget(this.node);
        cc.Tween.stopAllByTarget(this.winFxShinnyNode);
    };
    SicboResultStateComp.prototype.onHide = function () {
        SicboController_1.default.Instance.stopSfx(SicboSetting_1.SicboSound.SfxDealerShake);
        cc.Tween.stopAllByTarget(this.node);
        cc.Tween.stopAllByTarget(this.winFxShinnyNode);
        // this.chenClose(100);
        this.hideWinFx();
        this.destroyWinMoneyAtDoor();
        this.isPayingPlaying = false;
    };
    __decorate([
        property(cc.Node)
    ], SicboResultStateComp.prototype, "betCompNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboResultStateComp.prototype, "winFxShinnyNode", void 0);
    __decorate([
        property(cc.Animation)
    ], SicboResultStateComp.prototype, "chenAnimation", void 0);
    __decorate([
        property(cc.Node)
    ], SicboResultStateComp.prototype, "chenSmallRoot", void 0);
    __decorate([
        property(cc.Node)
    ], SicboResultStateComp.prototype, "chenZoomInRoot", void 0);
    __decorate([
        property([cc.Sprite])
    ], SicboResultStateComp.prototype, "diceSprites", void 0);
    __decorate([
        property(SicboLatestResultComp_1.default)
    ], SicboResultStateComp.prototype, "latestResultComp", void 0);
    __decorate([
        property(sp.Skeleton)
    ], SicboResultStateComp.prototype, "dealerSkeleton", void 0);
    __decorate([
        property(cc.Prefab)
    ], SicboResultStateComp.prototype, "slotWinMoneySmallPrefab", void 0);
    __decorate([
        property(cc.Prefab)
    ], SicboResultStateComp.prototype, "slotWinMoneyLargePrefab", void 0);
    __decorate([
        property(SicboNotifyComp_1.default)
    ], SicboResultStateComp.prototype, "notifyComp", void 0);
    SicboResultStateComp = __decorate([
        ccclass
    ], SicboResultStateComp);
    return SicboResultStateComp;
}(cc.Component));
exports.default = SicboResultStateComp;

cc._RF.pop();