"use strict";
cc._RF.push(module, '30fd5I8wj1DW5bHl+VUlTQg', 'SicboCannotJoinPopup');
// Game/Sicbo_New/Scripts/PopUps/SicboCannotJoinPopup.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SicboPortalAdapter_1 = require("../SicboPortalAdapter");
var SicboPopUpInstance_1 = require("../Managers/SicboPopUpInstance");
var SicboSetting_1 = require("../Setting/SicboSetting");
var SicboText_1 = require("../Setting/SicboText");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SicboCannotJoinPopup = /** @class */ (function (_super) {
    __extends(SicboCannotJoinPopup, _super);
    function SicboCannotJoinPopup() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.notIngameMsgLabel = null;
        _this.ingameMsgLabel = null;
        _this.notIngameNode = null;
        _this.ingameNode = null;
        return _this;
    }
    SicboCannotJoinPopup.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        this.notIngameMsgLabel.string = SicboText_1.default.getCannotJoinMessageWhenNotIngame;
        this.ingameMsgLabel.string = SicboText_1.default.getCannotJoinMessageWhenIngame;
    };
    SicboCannotJoinPopup.prototype.onShow = function () {
        var inGame = (cc.director.getScene().name != SicboSetting_1.SicboScene.Loading);
        this.ingameNode.active = inGame;
        this.notIngameNode.active = !inGame;
    };
    SicboCannotJoinPopup.prototype.leave = function () {
        this.close();
        SicboPortalAdapter_1.default.getInstance().leaveGame();
    };
    __decorate([
        property(cc.Label)
    ], SicboCannotJoinPopup.prototype, "notIngameMsgLabel", void 0);
    __decorate([
        property(cc.Label)
    ], SicboCannotJoinPopup.prototype, "ingameMsgLabel", void 0);
    __decorate([
        property(cc.Node)
    ], SicboCannotJoinPopup.prototype, "notIngameNode", void 0);
    __decorate([
        property(cc.Node)
    ], SicboCannotJoinPopup.prototype, "ingameNode", void 0);
    SicboCannotJoinPopup = __decorate([
        ccclass
    ], SicboCannotJoinPopup);
    return SicboCannotJoinPopup;
}(SicboPopUpInstance_1.default));
exports.default = SicboCannotJoinPopup;

cc._RF.pop();