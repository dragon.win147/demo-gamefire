#!/bin/sh
cd `dirname $0`
#MAC PATH
COCOS_EXE_PATH="/Applications/CocosCreator/Creator/2.4.4/CocosCreator.app/Contents/MacOS/CocosCreator"
#WINDOW PATH
#COCOS_EXE_PATH="C:\CocosDashboard_1.0.11\resources\.editors\Creator\2.4.4\CocosCreator.exe"

PLATFORM="web-mobile"
DEBUG="false"

BUILD_FILES="SicboModuleAdapter, Game"
COCOS_FOLDER=`pwd`"/.."
ASSETS_FOLDER=`pwd`"/../assets"
TEMP_FOLDER=`pwd`"/Temp"
PACKAGE_JSON=`pwd`"/../package.json"

GAME_BUNDLE_NAME="SicboBundle"
BUILD_FOLDER=${COCOS_FOLDER}"/build/"${PLATFORM}"/assets"
MAIN_JS_FILE=${BUILD_FOLDER}"/main/index.js"
BUNDLE_JS_FILE=${BUILD_FOLDER}/""${GAME_BUNDLE_NAME}"/index.js"

ZIP_FILE=${GAME_BUNDLE_NAME}".zip"

function Pause(){
	read -p "Press enter to continue"
} 

function LogConfig(){   
	echo "*******************************"
	echo COCOS_EXE_PATH:${COCOS_EXE_PATH}
	echo PLATFORM:${PLATFORM}
	echo DEBUG:${DEBUG}
	echo COCOS_FOLDER:${COCOS_FOLDER}
	echo ASSETS_FOLDER:${ASSETS_FOLDER}
	echo BUILD_FILES:${BUILD_FILES}
	echo TEMP_FOLDER:${TEMP_FOLDER}
	echo "*******************************"
}

function MakeTempFolder(){
	if [ -d "$TEMP_FOLDER" ]; then rm -Rf $TEMP_FOLDER; fi
	mkdir ${TEMP_FOLDER}
}

function MoveUnbuildFileToTempFolder(){
	IFS=', ' read -r -a BUILD_FILE_ARRAY <<< "$BUILD_FILES"

	for file in "$ASSETS_FOLDER"/*; do
		let count="0"
		
		for element in "${BUILD_FILE_ARRAY[@]}"
		do
			if [[ $file == *${element}* ]]; then
				count=${count}"1" #dont know why set value not working, so I use this stupid way. But it works :v
			fi
		done

		if [[ ${#count} == 1 ]]; then
			echo "Move ""$file"" to Temp Folder "
			mv ${file} ${TEMP_FOLDER}
		fi
	done
}

function MoveUnbuildFileBackToAssetFolder(){
	mv  -v "$TEMP_FOLDER"/* ${ASSETS_FOLDER}
}


function RunBuild(){	
	echo "************** BUILDING *****************"
	${COCOS_EXE_PATH} --path ${COCOS_FOLDER} --build "platform=${PLATFORM};debug=${DEBUG}"
}

function CopyIndex(){
	echo "************** COPY INDEX *****************"
	echo Copy:${MAIN_JS_FILE}
	echo To:${BUNDLE_JS_FILE}
	
	echo "" >> ${MAIN_JS_FILE}
	cat ${MAIN_JS_FILE} ${BUNDLE_JS_FILE} > $$.tmp && mv $$.tmp ${BUNDLE_JS_FILE}

	#cat ${MAIN_JS_FILE} >> ${BUNDLE_JS_FILE}
}

function AddPackageJsonLog(){
	echo "************** PACKAGE JSON LOG *****************"
	echo Copy:${PACKAGE_JSON}
	echo To:${BUNDLE_JS_FILE}
	
	echo "" >> ${BUNDLE_JS_FILE}
	echo "window.packageJsonLog=" >> ${BUNDLE_JS_FILE}
	cat ${PACKAGE_JSON} >> ${BUNDLE_JS_FILE}

}

function ZipBundle(){
	cd ${BUILD_FOLDER}
	jar -cfM ${ZIP_FILE} ${GAME_BUNDLE_NAME}
}

function Done(){
	echo "************** BUILD DONE *****************"
	#WINDOW
	#start ${BUILD_FOLDER}

	#MAC
	open ${BUILD_FOLDER}
}

LogConfig
MakeTempFolder
MoveUnbuildFileToTempFolder

RunBuild
MoveUnbuildFileBackToAssetFolder

CopyIndex
#AddPackageJsonLog

ZipBundle

Pause
Done
